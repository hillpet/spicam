/*  (c) Copyright:  2014..2017  Patrn.nl, Confidential Data
 *
 *  Workfile:           graphics.h
 *  Revision:   
 *  Modtime:    
 *
 *  Purpose:            graphics header file
 *
 * 
 * 
 *  Compiler/Assembler: Raspbian Linux GNU gcc
 *  Ext Packages:
 *
 *  Author:             Peter Hillen
 *  Date Created:       29 Apr 2014
 * 
 *  Revisions:
 * 
 *
 * 
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
**/

#ifndef _GRAPHICS_H_
#define _GRAPHICS_H_

#include "vector.h"
//
// Global prototypes
//
int      GRF_Init                   (void);
//
bool     GRF_AugmentMotionVectorFile(MVIMG *, char *, char *, char *, char *);
int      GRF_MotionCreateMask       (MVIMG *, char *, int, int);
void     GRF_MotionDisplay          (char *);
int      GRF_MotionDelta            (void);
int      GRF_MotionDetect           (void);
int      GRF_MotionNormalize        (void);
int      GRF_MotionZoom             (void);

#endif  /*_GRAPHICS_H_ */

