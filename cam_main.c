/*  (c) Copyright:  2017  Patrn, Confidential Data 
 *
 *  Workfile:           cam_main.c
 *  Revision:          
 *  Modtime:           
 *
 *  Purpose:            Raspberry pi
 *
 *
 *
 *  Compiler/Assembler: Raspbian Linux GNU gcc
 *  Ext Packages:
 *
 *  Author:             Peter Hillen
 *  Date Created:       09 Mar 2017
 *
 *  Revisions:
 *  Entry Points:       
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
**/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdarg.h>
#include <stddef.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>
#include <sched.h>
#include <time.h>
#include <errno.h>

#include <common.h>
#include "config.h"
#include "globals.h"
#include "cmd_main.h"
#include "cam_page.h"
#include "cam_html.h"
#include "cam_json.h"
#include "gen_http.h"
#include "gen_func.h"
#include "graphics.h"
//
#include "cam_main.h"

//#define USE_PRINTF
#include <printf.h>

//
// Variables
//
static volatile bool fCamRunning       = TRUE;
static GLOCMD       *pstCmd            = NULL;

//
// Camera parameter list (as defined in cam.defs). Extract what you need here:
//
// iFunction:        Define if a parameter affects certain camera function
//                      JSN_TXT : For JSON API: parameter is formatted as JSON text    ("value" = "xxxxx")
//                      JSN_INT :               parameter is formatted as JSON number  ("value" = xxxxx)
//
//                      CAM_STI : Paramater concerns Snapshot fotos
//                      CAM_TIM :                    Timelapse fotos
//                      CAM_REC :                    Recordings
//                      CAM_STR :                    Streaming video
//                      CAM_PIX :                    Picture file retrieval
//                      CAM_VID :                    Video file retrieval
//                      CAM_PRM :                    Parameter definition
//                      CAM_STP :                    Stopping command-in-progress
//                      CAM_ALL :                    All HTTP commands
//                      CAM____ :                    None of the commands
//
// pcJson:           JSON API parameter name
// pcParm:           HTML API parameter name
// pcOption:         Raspberry Pi command option
// iValueOffset:     Global mmap structure offset of the parameter
// iValueSize:       Global mmap parameter array size
// iChangedOffset:   Global mmap structure offset of the parameter has changed-marker
//
//===============================================================================================
// VLC does not permit to be run as root. To bypass this, hexedit /usr/bin/vlc and replace
//       geteuid
//    by getppid
//===============================================================================================

//
// Local prototypes
//
static pid_t   cam_Deamon                       (void);
static void    cam_Execute                      (void);
static bool    cam_RunCommand                   (void);
static void    cam_SignalCommandHandler         (void);
static void    cam_SignalHttpServer             (void);
static void    cam_HandleGuard                  (void);
//
static void    cam_HandleSignalFromServer       (void);
static void    cam_HandleNotificationFromAlarm  (void);
static void    cam_HandleNotificationFromCmd    (void);
//
static bool    cam_SignalRegister               (sigset_t *);
static void    cam_ReceiveSignalSegmnt          (int);
static void    cam_ReceiveSignalTerm            (int);
static void    cam_ReceiveSignalInt             (int);
static void    cam_ReceiveSignalUser1           (int);
static void    cam_ReceiveSignalUser2           (int);
//
static void    cam_InitSemaphore                (void);
static void    cam_ExitSemaphore                (void);
static void    cam_PostSemaphore                (void);
static int     cam_WaitSemaphore                (int);

/*------  Local functions separator ------------------------------------
__GLOBAL_FUNCTIONS(){};
----------------------------------------------------------------------------*/

// 
// Function:   CAM_Init
// Purpose:    Init the CAM thread
// 
// Parameters:  
// Returns:    Startup code
// 
// 
int CAM_Init(void)
{
   int   iCc=0;

   if( cam_Deamon() > 0) iCc = GLOBAL_CAM_INI;
   //
   // The parent (rpi-main) will return here. The cam_main deamon will 
   // exit upon completion.
   //
   return(iCc);
}

/*------  Local functions separator ------------------------------------
__LOCAL_FUNCTIONS(){};
----------------------------------------------------------------------------*/

//
// Function:   cam_Deamon
// Purpose:    Instantiate a new thread by the rpi_main thread
//
// Parms:      
// Returns:    The pid of the new cam thread
//
static pid_t cam_Deamon(void)
{
   pid_t    tPid;

   tPid = fork();
   //
   switch(tPid)
   {
      default:
         // rpi_main:parent 
         GLOBAL_PidPut(PID_CAMR, tPid);
         break;

      case 0:
         // cam_main:child
         PRINTF("cam-Deamon(): cam-thread" CRLF);
         cam_Execute();
         _exit(0);
         break;

      case -1:
         // Error
         LOG_printf("cam-Deamon(): Error!"CRLF);
         LOG_Report(errno, "RPI", "cam-Deamon(): Error!");
         break;
   }
   return(tPid);
}

// 
// Function:   cam_Execute 
// Purpose:    Run the cam thread
// 
// Parameters:  
// Returns:  
// Note:       This is the main loop for the CAM thread.
//             A SIGUSR1 comes from:
//                o the HTTP server to initiate a command
//                o the COMMAND thread to notify completion of a command
//                o the COMMAND thread to notify completion of an alarm
// 
static void cam_Execute(void)
{
   sigset_t tBlockset;

   if(cam_SignalRegister(&tBlockset) == FALSE)  _exit(1);
   //
   cam_InitSemaphore();
   //
   GEN_Sleep(1000);
   LOG_Report(0, "CAM", "cam_Execute():Init Start....");
   pstCmd = GLOBAL_CommandCopy();
   //
   // Init RaspiCam
   //
   GRF_Init();
   CAM_HtmlInit();
   CAM_JsonInit();
   CAM_InitDynamicPages();
   LOG_Report(0, "CAM", "cam_Execute():Init Dyn pages OK");
   GLOBAL_HostNotification(GLOBAL_CAM_INI);
   //
   LOG_Report(0, "CAM", "cam_Execute():Init Ready");
   PRINTF("cam_Execute():Init OKee" CRLF);
   GLOBAL_PidSaveGuard(PID_CAMR, GLOBAL_CAM_INI);
   //
   while(fCamRunning)
   {
      //
      // The cam mainloop
      //
      switch( cam_WaitSemaphore(1000) )
      {
         case 0:
            //===========================================================================
            // Incoming SIGUSRx signal
            //===========================================================================
            if( GLOBAL_GetSignalNotification(PID_CAMR, GLOBAL_SVR_RUN) ) cam_HandleSignalFromServer();
            if( GLOBAL_GetSignalNotification(PID_CAMR, GLOBAL_CMD_NFY) ) cam_HandleNotificationFromCmd();
            if( GLOBAL_GetSignalNotification(PID_CAMR, GLOBAL_ALM_NFY) ) cam_HandleNotificationFromAlarm();
            if( GLOBAL_GetSignalNotification(PID_CAMR, GLOBAL_GRD_RUN) ) cam_HandleGuard();
            break;

         default:
            break;

         case -1:
            PRINTF("cam_Execute():ERROR cam_WaitSemaphore" CRLF);
            LOG_Report(errno, "CAM", "cam_Execute():ERROR cam_WaitSemaphore");
            fCamRunning = FALSE;
            break;

         case 1:
            //
            // Request timeout
            //
            GLOBAL_PidSetGuard(PID_CAMR);
            break;
      }
   }
   GLOBAL_CommandDestroy(pstCmd);
   cam_ExitSemaphore();
}

// 
// Function:   cam_HandleSignalFromServer
// Purpose:    Handle the signal coming from the http server
// 
// Parameters: 
// Returns:  
//  
static void cam_HandleSignalFromServer()
{
   // DFD-0-3.1 (CAM Rcv SIGUSR1)
   //
   // We are signalled to do something:
   // If the command handler needs to be involved, this is the time to forward the request,
   // else report directly back to the http server.
   //
   PRINTF("cam_Execute():Command from Server" CRLF);
   if( cam_RunCommand() )  
   {
      //
      // Forward the request 
      //
      PRINTF("cam_Execute():Command to CMD" CRLF);
      cam_SignalCommandHandler();
   }
   else
   {
      //
      // Command has already been handled
      //
      PRINTF("cam_Execute():Notify to RPI" CRLF);
      cam_SignalHttpServer();
   }
}

// 
// Function:   cam_HandleNotificationFromCmd
// Purpose:    Handle the notification coming from the command thread
// 
// Parameters: 
// Returns:  
//  
static void cam_HandleNotificationFromCmd()
{
   //
   // The signal comes from he CMD handler.
   // If we have a pending Callback, handle it now
   //
   if(pstCmd->pfCallback) 
   {
      PRINTF("cam_HandleNotificationFromCmd():Notify(CB) from CMD" CRLF);
      pstCmd->pfCallback(pstCmd);
      cam_SignalHttpServer();
   }
}

// 
// Function:   cam_HandleNotificationFromAlarm
// Purpose:    Handle the notification coming from the CMD alarm
// 
// Parameters: 
// Returns:  
//  
static void cam_HandleNotificationFromAlarm()
{
   //
   // The signal comes from he ALARM handler. We need to free the command
   // buffer here, since the RPI server is not aware of any ongoing actions.
   //
   GEN_Free(pstCmd);
}

// 
// Function:   cam_RunCommand
// Purpose:    Extract the command and run the handler for it
// 
// Parameters: 
// Returns:    TRUE  if the command goes up the chain
//             FALSE if this is the end, and we need to notiify the server
// Note:       This is the CAM thread command initiator:
//                o Get the type (HTTP_HTML, HTTP_JSON)
//                o Lookup the command in the correct list
//                o Run the command handler
// 
static bool cam_RunCommand()
{
   bool           fCc=FALSE;
   int            iIdx;
   FTYPE          tType;
   PFCMD          pfAddr;
   const CAMPAGE *pstCam=NULL;
   char          *pcCmd;
   
   GLOBAL_CommandGet(pstCmd);
   GLOBAL_DUMPCOMMAND("cam_RunCommand():PID_CAMR(Get):Execute command from PID_SRVR", pstCmd);
   //
   iIdx = pstMap->G_iCurDynPage;
   //
   // Call the Dyn Page handler function first, then lookup the
   // actual command 
   //
   if(!CAM_DynamicPageHandler(pstCmd, iIdx) ) return(FALSE);
   //
   tType = GEN_GetDynamicPageType(iIdx);        // HTTP_HTML, HTTP_JSON
   //
   // Lookup command
   //
   switch(tType)
   {
      case HTTP_HTML:
         // Use HTML/JS API
         pstCam = CAM_HtmlGetCommands();
         PRINTF1("cam_RunCommand(): %s.html" CRLF, pstMap->G_pcCommand);
         break;
      
      case HTTP_JSON:
         // Use JSON API
         pstCam = CAM_JsonGetCommands();
         PRINTF1("cam_RunCommand(): %s.json" CRLF, pstMap->G_pcCommand);
         break;
      
      default:
         PRINTF1("cam_RunCommand(): %s.other" CRLF, pstMap->G_pcCommand);
         return(FALSE);
   }
   //
   while(TRUE)
   {
      pcCmd = pstCam->pcCommand;
      if(pcCmd == NULL) 
      {
         //
         // Unknown page-command
         //
         PRINTF1("cam_RunCommand(): Unknown page-command %s" CRLF, pstMap->G_pcCommand);
         pstCmd->iError = CAM_STATUS_ERROR;
         pstCmd->iArgs |= CAM_ERR;
         break;
      }
      //
      if(GEN_STRCMPI(pstMap->G_pcCommand, pcCmd) == 0)
      {
         //
         // This request is a dynamic HTTP camera command
         //
         pfAddr = pstCam->pfCmdCb;
         if(pfAddr) 
         {
            pstCmd->iCommand = pstCam->iCommand;
            pstCmd->iArgs    = pstCam->iFunction;
            //
            // Run the command handler: 
            // It will setup a callback function if required. 
            // if CC==TRUE Proceed to signal the command-thread
            // else notify the HTTP server
            //
            fCc = pfAddr(pstCmd);
         }
         break;
      }
      else pstCam++;
   }
   return(fCc);
}

// 
// Function:   cam_SignalCommandHandler
// Purpose:    Signal the command handler
// 
// Parameters: 
// Returns:  
// Note:       Command is the global command from the registered URL
//  
void cam_SignalCommandHandler(void)
{
   //PRINTF4("cam_SignalCommandHandler():Url=%d, Cmd=%s, Args=0x%X, pid=%d" CRLF, 
   //         pstCmd->tUrl, GLOBAL_CommandGetString(pstCmd->iCommand), pstCmd->iArgs, GLOBAL_PidGet(PID_CMND));
   // DFD-0-4.1 (CAM Put stCmd)
   GLOBAL_CommandPutAll(pstCmd);
   GLOBAL_DUMPCOMMAND("cam_SignalCommandHandler():PID_CAMR(PutAll):Signal to PID_CMND", pstCmd);
   //
   // DFD-0-5.1 (CAM Xmt SIGUSR1)
   GLOBAL_Notify(PID_CMND, GLOBAL_CAM_RUN, SIGUSR1);
}

// 
// Function:   cam_SignalHttpServer
// Purpose:    Signal the HTTP server
// 
// Parameters: 
// Returns:  
// Note:       
//  
static void cam_SignalHttpServer()
{
   GLOBAL_CommandPut(pstCmd);
   // DFD-0-9.1 (CAM Xmt SIGUSR1)
   GLOBAL_Notify(PID_SRVR, GLOBAL_CAM_NFY, SIGUSR1);
}

// 
// Function:   cam_HandleGuard
// Purpose:    Respond to the guard query
// 
// Parameters: 
// Returns:    
// Note:       
//
static void cam_HandleGuard(void)
{
   LOG_Report(0, "CAM", "cam_HandleGuard():Host-notification send");
   GLOBAL_HostNotification(GLOBAL_GRD_NFY);
}


/*------  Local functions separator ------------------------------------
__SIGNAL_FUNCTIONS(){};
----------------------------------------------------------------------------*/

// 
// Function:   cam_SignalRegister
// Purpose:    Register all SIGxxx
// 
// Parameters: Blockset
// Returns:    TRUE if delivered
// 
static bool cam_SignalRegister(sigset_t *ptBlockset)
{
  bool fCC = TRUE;

  // SIGSEGV:
  if( signal(SIGSEGV, &cam_ReceiveSignalSegmnt) == SIG_ERR)
  {
     LOG_Report(errno, "CAM", "cam_SignalRegister(): SIGSEGV ERROR");
     fCC = FALSE;
  }
  // SIGUSR1:
  if( signal(SIGUSR1, &cam_ReceiveSignalUser1) == SIG_ERR)
  {
     LOG_Report(errno, "CAM", "cam_SignalRegister(): SIGUSR1 ERROR");
     fCC = FALSE;
  }
  // SIGUSR2:
  if( signal(SIGUSR2, &cam_ReceiveSignalUser2) == SIG_ERR)
  {
     LOG_Report(errno, "CAM", "cam_SignalRegister(): SIGUSR2 ERROR");
     fCC = FALSE;
  }
  // CTL-X handler
  if( signal(SIGTERM, &cam_ReceiveSignalTerm) == SIG_ERR)
  {
     LOG_Report(errno, "CAM", "cam_SignalRegister(): SIGTERM ERROR");
     fCC = FALSE;
  }
  // Ctl-C handler
  if( signal(SIGINT, &cam_ReceiveSignalInt) == SIG_ERR)
  {
     LOG_Report(errno, "CAM", "cam_SignalRegister(): SIGINT ERROR");
     fCC = FALSE;
  }

  if(fCC)
  {
     //
     // Setup the SIGxxx events
     //
     sigemptyset(ptBlockset);
     sigaddset(ptBlockset, SIGSEGV);
     sigaddset(ptBlockset, SIGTERM);
     sigaddset(ptBlockset, SIGINT);
     sigaddset(ptBlockset, SIGUSR1);
     sigaddset(ptBlockset, SIGUSR2);
  }
  return(fCC);
}

//
// Function:   cam_ReceiveSignalSegmnt
// Purpose:    Handle the SIGSEGV
//
// Parms:
// Returns:    TRUE if delivered
// Note:       Do NOT use LOG_xxx() here
//
static void cam_ReceiveSignalSegmnt(int iSignal)
{
   if(fCamRunning)
   {
      fCamRunning = FALSE;
      //
      GEN_SegmentationFault(__FILE__, __LINE__);
      LOG_SegmentationFault(__FILE__, __LINE__);
      //
      // (TRY TO) Cleanup background threads:
      //
      GLOBAL_Notify(PID_HOST, GLOBAL_FLT_NFY, SIGTERM);
   }
   cam_PostSemaphore();
}

// 
// Function:   cam_ReceiveSignalTerm
// Purpose:    Add the SIGTERM command in the buffer (Ctl-X)
// 
// Parameters: 
// Returns:    TRUE if delivered
// 
static void cam_ReceiveSignalTerm(int iSignal)
{
   LOG_Report(0, "CAM", "cam_ReceiveSignalTerm()");
   fCamRunning = FALSE;
   cam_PostSemaphore();
}

//
// Function:   cam_ReceiveSignalInt
// Purpose:    Add the SIGINT command in the buffer (Ctl-C)
//
// Parms:
// Returns:    TRUE if delivered
//
static void cam_ReceiveSignalInt(int iSignal)
{
   LOG_Report(0, "CAM", "cam_ReceiveSignalInt()");
   fCamRunning = FALSE;
   cam_PostSemaphore();
}

//
// Function:   cam_ReceiveSignalUser1
// Purpose:    System callback for SIGUSR1 signal
//
// Parms:
// Returns:    
// Note:       The HTTP server thread will send us a SIGUSR1 to
//             signal a new action
//
static void cam_ReceiveSignalUser1(int iSignal)
{
   // DFD-0-3.1 (CAM Rcv SIGUSR1)
   //LOG_Report(0, "CAM", "cam_ReceiveSignalUser1()");
   cam_PostSemaphore();
}

//
// Function:   cam_ReceiveSignalUser2
// Purpose:    System callback for SIGUSR2 signal
//
// Parms:
// Returns: 
// Note:       Not used
//
static void cam_ReceiveSignalUser2(int iSignal)
{
}

/*------  Local functions separator ------------------------------------
__SEMAPHORE_FUNCTIONS(){};
----------------------------------------------------------------------------*/

//
// Function:   cam_InitSemaphore
// Purpose:    Init the Semaphore
//
// Parms:      
// Returns:    
// Note:       If pshared has the value 0, then the semaphore is shared between the
//             threads of a process, and should be located at some address that is
//             visible to all threads (e.g., a global variable, or a variable
//             allocated dynamically on the heap).
//             If pshared is nonzero, then the semaphore is shared between
//             processes, and should be located in a region of shared memory.
//
//             The sem value is always >= 0:
//                               0 means LOCKED
//                              >0 means UNLOCKED
//             Init(sem 1, VAL)  VAL=1 unlocked
//                               VAL=0 locked
//             Wait(sem)         LOCK:    if(VAL> 0) { VAL--; return;         } 
//                                        if(VAL==0) { Wait till > 0; return; }
//             Post(sem)         UNLOCK:  VAL++
//
static void cam_InitSemaphore(void)
{
   //
   // Init the semaphore as LOCKED
   //
   if(sem_init(&pstMap->G_tSemCam, 1, 0) == -1)
   {
      LOG_Report(errno, "CAM", "cam_InitSemaphore(): sem_init Error");
   }
}

//
// Function:   cam_ExitSemaphore
// Purpose:    Exit the Semaphore
//
// Parms:      
// Returns:    
// Note:       
//
static void cam_ExitSemaphore(void)
{
   if( sem_destroy(&pstMap->G_tSemCam) == -1) LOG_Report(errno, "CAM", "cam_ExitSemaphore():sem_destroy");
}

//
// Function:   cad_PostSemaphore
// Purpose:    Unlock the screen Semaphore
//
// Parms:      Address of semaphore
// Returns:    
// Note:       sem_post() increments (unlocks) the semaphore pointed to by sem. If the semaphore's 
//             value consequently becomes greater than zero, then another process or thread blocked
//             in a sem_wait(3) call will be woken up and proceed to lock the semaphore. 
//
static void cam_PostSemaphore()
{
   int      iCc;

   //PRINTF("cam_PostSemaphore():" CRLF);
   //
   // sem++ (UNLOCK)   
   //
   iCc = sem_post(&pstMap->G_tSemCam);
   if(iCc == -1) LOG_Report(errno, "CAM", "cam_PostSemaphore(): sem_post error");
   //PRINTF("cam_PostSemaphore():Done" CRLF);
}


//
// Function:   cam_WaitSemaphore
// Purpose:    Wait till Semaphore has been unlocked, then LOCK and proceed
//
// Parms:      TIMEOUT (or -1 if wait indefinitely)
// Returns:     0 if sem unlocked
//             +1 if sem timeout
//             -1 on error (errno)
// Note:       sem_wait() tries to decrement (lock) the semaphore. 
//             If the semaphore currently has the value zero, then the call blocks until 
//             either it becomes possible to perform the decrement (the semaphore value rises 
//             above zero) or a signal handler interrupts the call. 
//             If the semaphore's value is greater than zero, then the decrement proceeds, 
//             and the function returns immediately. 
//
//             sem_timedwait() is the same as sem_wait(), except that TIMEOUT specifies a 
//             limit on the amount of time that the call should block if the decrement cannot 
//             be immediately performed. The stTime points to a structure that
//             specifies an absolute timeout in seconds and nanoseconds since the Epoch, 
//             1970-01-01 00:00:00 +0000 (UTC). This structure is defined as follows:
//
//             struct timespec 
//             {
//                time_t tv_sec;      /* Seconds */
//                long   tv_nsec;     /* Nanoseconds [0 .. 999999999] */
//             };
//             If the timeout has already expired by the time of the call, and the semaphore 
//             could not be locked immediately, then sem_timedwait() fails with a timeout error 
//             (errno set to ETIMEDOUT). If the operation can be performed immediately, then 
//             sem_timedwait() never fails with a timeout error, regardless of the value of 
//             TIMEOUT.
//
//
static int cam_WaitSemaphore(int iMsecs)
{
   //
   // if    sem == 0 : wait
   // else  sem-- (LOCK)   
   //
   return(GEN_WaitSemaphore(&pstMap->G_tSemCam, iMsecs));
}

