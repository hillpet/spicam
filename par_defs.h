/*  (c) Copyright:  2017..2018  Patrn, Confidential Data
 *
 *  Workfile:           par_defs.h
 *  Revision:          
 *  Modtime:           
 *
 *  Purpose:            All defines for extracting rpicam data
 *
 *
 *
 *  Compiler/Assembler: Raspbian Linux GNU gcc
 *  Ext Packages:
 *
 *  Author:             Peter Hillen
 *  Date Created:       09 Mar 2017
 *
 *  Revisions:
 *
 *  Macro elements:           Used in:
 *
 *       a  Enum                          cam_main.h              rpi_func.c
 *       b  iFunction         cam_main.c              globals.c   rpi_func.c
 *       c  pcJson            cam_main.c                          rpi_func.c
 *       d  pcHtml            cam_main.c                          rpi_func.c
 *       e  pcOption          cam_main.c
 *       f  iValueOffset      cam_main.c              globals.c   rpi_func.c
 *       g  iValueSize        cam_main.c              globals.c   rpi_func.c
 *       h  iChangedOffset    cam_main.c              globals.c   rpi_func.c
 *       i  Changed                                   globals.c
 *       j  pcDefault                                 globals.c
 *       k  pfHttpFun                                             rpi_func.c
 *
 *
 *
 *
 *
 *
**/

//
// Macro:       a                                              b                                     c                 d        e           f                                     g                  h                                        i   j                        k
//          Cmd-Enum,         _____________________________iFunction____________________________   pcJson,           pcHtml   pcOption iValueOffset                           iValueSize        iChangedOffset                                   pcDefault                 pfHttpFun
//                               Txt/Int Snaps   Tlapse  Recs    Strm    Misc
EXTRACT_PAR(CAM_COMMAND,     (WB|JSN_TXT|CAM____|CAM____|CAM____|CAM____|CAM____|CAM____|CAM_ALL), "Command",        "cmd=",  "",      offsetof(RPIMAP, G_pcCommand),         MAX_PARM_LEN,     offsetof(RPIMAP, G_ubCommandChanged),         0, "",                       NULL)
EXTRACT_PAR(CAM_WIDTH,       (   JSN_TXT|CAM_STI|CAM_TIM|CAM_REC|CAM_STR|CAM_VEC|CAM____|CAM____), "Width",          "w=",    "-w",    offsetof(RPIMAP, G_pcWidth),           MAX_PARM_LEN,     offsetof(RPIMAP, G_ubWidthChanged),           0, "1280",                   NULL)
EXTRACT_PAR(CAM_HEIGHT,      (   JSN_TXT|CAM_STI|CAM_TIM|CAM_REC|CAM_STR|CAM_VEC|CAM____|CAM____), "Height",         "h=",    "-h",    offsetof(RPIMAP, G_pcHeight),          MAX_PARM_LEN,     offsetof(RPIMAP, G_ubHeightChanged),          0, "720",                    NULL)
EXTRACT_PAR(CAM_FLIPH,       (   JSN_TXT|CAM_STI|CAM_TIM|CAM_REC|CAM_STR|CAM_VEC|CAM____|CAM____), "FlipH",          "hf",    "-hf",   offsetof(RPIMAP, G_pcFlipH),           MAX_PARM_LEN,     offsetof(RPIMAP, G_ubFlipHChanged),           0, "1",                      NULL)
EXTRACT_PAR(CAM_FLIPV,       (   JSN_TXT|CAM_STI|CAM_TIM|CAM_REC|CAM_STR|CAM_VEC|CAM____|CAM____), "FlipV",          "vf",    "-vf",   offsetof(RPIMAP, G_pcFlipV),           MAX_PARM_LEN,     offsetof(RPIMAP, G_ubFlipVChanged),           0, "1",                      NULL)
EXTRACT_PAR(CAM_FILEEXT,     (   JSN_TXT|CAM_STI|CAM_TIM|CAM____|CAM____|CAM_VEC|CAM____|CAM____), "Ext",            "e=",    "-e",    offsetof(RPIMAP, G_pcPixFileExt),      MAX_PARM_LEN,     offsetof(RPIMAP, G_ubPixFileExtChanged),      0, "jpg",                    NULL)
EXTRACT_PAR(CAM_FPS,         (   JSN_TXT|CAM____|CAM____|CAM____|CAM_STR|CAM_VEC|CAM____|CAM____), "StreamFps",      "fps=",  "-fps",  offsetof(RPIMAP, G_pcStreamFps),       MAX_PARM_LEN,     offsetof(RPIMAP, G_ubStreamFpsChanged),       1, "25",                     NULL)
EXTRACT_PAR(CAM_DELAY,       (   JSN_TXT|CAM_STI|CAM____|CAM____|CAM____|CAM____|CAM____|CAM____), "Delay",          "ssd=",  "-t",    offsetof(RPIMAP, G_pcPixDelay),        MAX_PARM_LEN,     offsetof(RPIMAP, G_ubPixDelayChanged),        0, "1",                      NULL)
EXTRACT_PAR(CAM_TMRDURATION, (   JSN_TXT|CAM____|CAM_TIM|CAM____|CAM____|CAM____|CAM____|CAM____), "TotalTime",      "dur=",  "-t",    offsetof(RPIMAP, G_pcTimDuration),     MAX_PARM_LEN,     ALWAYS_CHANGED,                               0, "20000",                  NULL)
EXTRACT_PAR(CAM_LAPSE,       (   JSN_TXT|CAM____|CAM_TIM|CAM____|CAM____|CAM____|CAM____|CAM____), "Lapsetime",      "tl=",   "-tl",   offsetof(RPIMAP, G_pcTimelapse),       MAX_PARM_LEN,     ALWAYS_CHANGED,                               0, "5000",                   NULL)
EXTRACT_PAR(CAM_RECDURATION, (   JSN_TXT|CAM____|CAM____|CAM_REC|CAM_STR|CAM_VEC|CAM_PRM|CAM____), "Duration",       "dur=",  "-t",    offsetof(RPIMAP, G_pcRecDuration),     MAX_PARM_LEN,     ALWAYS_CHANGED,                               0, "0",                      NULL)
EXTRACT_PAR(CAM_NOPREVIEW,   (   JSN_TXT|CAM_STI|CAM_TIM|CAM_REC|CAM_STR|CAM____|CAM____|CAM____), "NoPreview",      "nopv",  "-n",    offsetof(RPIMAP, G_pcNoPreview),       MAX_PARM_LEN,     offsetof(RPIMAP, G_ubNoPreviewChanged),       0, "0",                      NULL)
EXTRACT_PAR(CAM_QUALITY,     (   JSN_TXT|CAM_STI|CAM_TIM|CAM_REC|CAM_STR|CAM____|CAM____|CAM____), "Quality",        "q=",    "-q",    offsetof(RPIMAP, G_pcPixQuality),      MAX_PARM_LEN,     offsetof(RPIMAP, G_ubPixQualityChanged),      0, "100",                    NULL)
EXTRACT_PAR(CAM_SHARPNESS,   (   JSN_TXT|CAM_STI|CAM_TIM|CAM_REC|CAM_STR|CAM____|CAM____|CAM____), "Sharpness",      "sh=",   "-sh",   offsetof(RPIMAP, G_pcSharpness),       MAX_PARM_LEN,     offsetof(RPIMAP, G_ubSharpnessChanged),       0, "0",                      NULL)
EXTRACT_PAR(CAM_RECORD,      (WB|JSN_TXT|CAM____|CAM____|CAM____|CAM____|CAM____|CAM____|CAM_ALL), "Recording",      "rec=",  "",      offsetof(RPIMAP, G_pcRecording),       MAX_PARM_LEN,     ALWAYS_CHANGED,                               0, "00:00:00",               NULL)
//
EXTRACT_PAR(CAM_CONTRAST,    (   JSN_TXT|CAM_STI|CAM_TIM|CAM_REC|CAM_STR|CAM____|CAM____|CAM____), "Contrast",       "co=",   "-co",   offsetof(RPIMAP, G_pcContrast),        MAX_PARM_LEN,     offsetof(RPIMAP, G_ubContrastChanged),        0, "0",                      NULL)
EXTRACT_PAR(CAM_BRIGHTNESS,  (   JSN_TXT|CAM_STI|CAM_TIM|CAM_REC|CAM_STR|CAM____|CAM____|CAM____), "Brightness",     "br=",   "-br",   offsetof(RPIMAP, G_pcBrightness),      MAX_PARM_LEN,     offsetof(RPIMAP, G_ubBrightnessChanged),      0, "50",                     NULL)
EXTRACT_PAR(CAM_SATURATION,  (   JSN_TXT|CAM_STI|CAM_TIM|CAM_REC|CAM_STR|CAM____|CAM____|CAM____), "Saturation",     "sa=",   "-sa",   offsetof(RPIMAP, G_pcSaturation),      MAX_PARM_LEN,     offsetof(RPIMAP, G_ubSaturationChanged),      0, "0",                      NULL)
EXTRACT_PAR(CAM_EXPOSURE,    (   JSN_TXT|CAM_STI|CAM_TIM|CAM_REC|CAM_STR|CAM____|CAM____|CAM____), "Exposure",       "ex=",   "-ex",   offsetof(RPIMAP, G_pcExposure),        MAX_PARM_LEN,     offsetof(RPIMAP, G_ubExposureChanged),        0, "1",                      NULL)
EXTRACT_PAR(CAM_AUTOBW,      (   JSN_TXT|CAM_STI|CAM_TIM|CAM_REC|CAM_STR|CAM____|CAM____|CAM____), "AutoWB",         "awb=",  "-awb",  offsetof(RPIMAP, G_pcAutoWB),          MAX_PARM_LEN,     offsetof(RPIMAP, G_ubAutoWBChanged),          0, "1",                      NULL)
EXTRACT_PAR(CAM_IMGEFFECT,   (   JSN_TXT|CAM_STI|CAM_TIM|CAM_REC|CAM_STR|CAM____|CAM____|CAM____), "ImgEffects",     "ifx=",  "-ifx",  offsetof(RPIMAP, G_pcImgEffects),      MAX_PARM_LEN,     offsetof(RPIMAP, G_ubImgEffectsChanged),      0, "0",                      NULL)
EXTRACT_PAR(CAM_COLEFFECT,   (   JSN_TXT|CAM_STI|CAM_TIM|CAM_REC|CAM_STR|CAM____|CAM____|CAM____), "ColEffects",     "cfx=",  "-cfx",  offsetof(RPIMAP, G_pcColEffects),      MAX_PARM_LEN,     offsetof(RPIMAP, G_ubColEffectsChanged),      0, "0",                      NULL)
EXTRACT_PAR(CAM_MMODE,       (   JSN_TXT|CAM_STI|CAM_TIM|CAM_REC|CAM_STR|CAM____|CAM____|CAM____), "MeterMode",      "mm=",   "-mm",   offsetof(RPIMAP, G_pcMeterMode),       MAX_PARM_LEN,     offsetof(RPIMAP, G_ubMeterModeChanged),       0, "0",                      NULL)
EXTRACT_PAR(CAM_ROTATION,    (   JSN_TXT|CAM_STI|CAM_TIM|CAM_REC|CAM_STR|CAM____|CAM____|CAM____), "Rotation",       "rot=",  "-rot",  offsetof(RPIMAP, G_pcRotation),        MAX_PARM_LEN,     offsetof(RPIMAP, G_ubRotationChanged),        0, "0",                      NULL)
EXTRACT_PAR(CAM_STABILITY,   (   JSN_TXT|CAM____|CAM____|CAM_REC|CAM_STR|CAM____|CAM____|CAM____), "VideoStab",      "vstab", "-vs",   offsetof(RPIMAP, G_pcVideoStab),       MAX_PARM_LEN,     offsetof(RPIMAP, G_ubVideoStabChanged),       0, "0",                      NULL)
EXTRACT_PAR(CAM_BANNER,      (   JSN_TXT|CAM_STI|CAM_TIM|CAM_REC|CAM_STR|CAM____|CAM____|CAM____), "RawBanner",      "raw",   "-r",    offsetof(RPIMAP, G_pcRawBanner),       MAX_PARM_LEN,     offsetof(RPIMAP, G_ubRawBannerChanged),       0, "0",                      NULL)
EXTRACT_PAR(CAM_EVCOMP,      (   JSN_TXT|CAM_STI|CAM_TIM|CAM_REC|CAM_STR|CAM____|CAM____|CAM____), "EvCompensation", "evc",   "-ev",   offsetof(RPIMAP, G_pcEvCompensation),  MAX_PARM_LEN,     offsetof(RPIMAP, G_ubEvCompensationChanged),  0, "0",                      NULL)
EXTRACT_PAR(CAM_SSPEED,      (   JSN_TXT|CAM_STI|CAM_TIM|CAM_REC|CAM_STR|CAM____|CAM____|CAM____), "Shutterspeed",   "ss=",   "-ss",   offsetof(RPIMAP, G_pcShutterspeed),    MAX_PARM_LEN,     offsetof(RPIMAP, G_ubShutterspeedChanged),    0, "5000",                   NULL)
//
EXTRACT_PAR(CAM_BITRATE,     (   JSN_TXT|CAM____|CAM____|CAM_REC|CAM_STR|CAM____|CAM____|CAM____), "Bitrate",        "br=",   "-b",    offsetof(RPIMAP, G_pcBitrate),         MAX_PARM_LEN,     offsetof(RPIMAP, G_ubBitrateChanged),         0, "8888",                   NULL)
EXTRACT_PAR(CAM_PROFILE,     (   JSN_TXT|CAM____|CAM____|CAM_REC|CAM_STR|CAM____|CAM____|CAM____), "Profile",        "pf=",   "-pf",   offsetof(RPIMAP, G_pcProfile),         MAX_PARM_LEN,     offsetof(RPIMAP, G_ubProfileChanged),         0, "0",                      NULL)
EXTRACT_PAR(CAM_FULLSCREEN,  (   JSN_TXT|CAM_STI|CAM_TIM|CAM_REC|CAM_STR|CAM____|CAM____|CAM____), "Fullscreen",     "f",     "-f",    offsetof(RPIMAP, G_pcFullscreen),      MAX_PARM_LEN,     offsetof(RPIMAP, G_ubFullscreenChanged),      0, "1",                      NULL)
EXTRACT_PAR(CAM_GOP,         (   JSN_TXT|CAM____|CAM____|CAM_REC|CAM_STR|CAM____|CAM____|CAM____), "Gop",            "gop=",  "-g",    offsetof(RPIMAP, G_pcGop),             MAX_PARM_LEN,     offsetof(RPIMAP, G_ubGopChanged),             0, "10",                     NULL)
EXTRACT_PAR(CAM_ISO,         (   JSN_TXT|CAM_STI|CAM_TIM|CAM_REC|CAM_STR|CAM____|CAM____|CAM____), "Iso",            "iso=",  "-ISO",  offsetof(RPIMAP, G_pcIso),             MAX_PARM_LEN,     offsetof(RPIMAP, G_ubIsoChanged),             0, "200",                    NULL)
EXTRACT_PAR(CAM_OPACITY,     (   JSN_TXT|CAM_STI|CAM_TIM|CAM_REC|CAM_STR|CAM____|CAM____|CAM____), "Opacity",        "op=",   "-op",   offsetof(RPIMAP, G_pcOpacity),         MAX_PARM_LEN,     offsetof(RPIMAP, G_ubOpacityChanged),         0, "0",                      NULL)
EXTRACT_PAR(CAM_PREVIEW,     (   JSN_TXT|CAM_STI|CAM_TIM|CAM_REC|CAM_STR|CAM_VEC|CAM____|CAM____), "Preview",        "pv=",   "-p",    offsetof(RPIMAP, G_pcPreview),         MAX_PARM_LEN,     offsetof(RPIMAP, G_ubPreviewChanged),         0, "1280,0,640,360",         NULL)
EXTRACT_PAR(CAM_ROI,         (   JSN_TXT|CAM_STI|CAM_TIM|CAM_REC|CAM_STR|CAM_VEC|CAM____|CAM____), "Roi",            "roi=",  "-roi",  offsetof(RPIMAP, G_pcRoi),             MAX_PARM_LEN,     offsetof(RPIMAP, G_ubRoiChanged),             0, "0.5,0.5,0.5,0.5",        NULL)
EXTRACT_PAR(CAM_THUMBNAIL,   (   JSN_TXT|CAM_STI|CAM_TIM|CAM_REC|CAM_STR|CAM____|CAM____|CAM____), "Thumbnail",      "th=",   "-th",   offsetof(RPIMAP, G_pcThumbnail),       MAX_PARM_LEN,     offsetof(RPIMAP, G_ubThumbnailChanged),       0, "0,0,100",                NULL)
EXTRACT_PAR(CAM_VERBOSE,     (   JSN_TXT|CAM_STI|CAM_TIM|CAM_REC|CAM_STR|CAM____|CAM____|CAM____), "Verbose",        "v",     "-v",    offsetof(RPIMAP, G_pcVerbose),         MAX_PARM_LEN,     offsetof(RPIMAP, G_ubVerboseChanged),         0, "0",                      NULL)
EXTRACT_PAR(CAM_DEMO,        (   JSN_TXT|CAM_STI|CAM_TIM|CAM_REC|CAM_STR|CAM____|CAM____|CAM____), "Demo",           "demo=", "-d",    offsetof(RPIMAP, G_pcDemo),            MAX_PARM_LEN,     offsetof(RPIMAP, G_ubDemoChanged),            0, "0",                      NULL)
EXTRACT_PAR(CAM_PENC,        (   JSN_TXT|CAM____|CAM____|CAM_REC|CAM_STR|CAM____|CAM____|CAM____), "Penc",           "penc",  "-e",    offsetof(RPIMAP, G_pcPenc),            MAX_PARM_LEN,     offsetof(RPIMAP, G_ubPencChanged),            0, "0",                      NULL)
EXTRACT_PAR(CAM_WRAP,        (   JSN_TXT|CAM____|CAM____|CAM_REC|CAM____|CAM____|CAM____|CAM____), "Wrap",           "wrap=", "-wr",   offsetof(RPIMAP, G_pcWrap),            MAX_PARM_LEN,     offsetof(RPIMAP, G_ubWrapChanged),            0, "3",                      NULL)
EXTRACT_PAR(CAM_SEGMENT,     (   JSN_TXT|CAM____|CAM____|CAM_REC|CAM____|CAM____|CAM____|CAM____), "Segment",        "sgm=",  "-sg",   offsetof(RPIMAP, G_pcSegment),         MAX_PARM_LEN,     offsetof(RPIMAP, G_ubSegmentChanged),         0, "20000",                  NULL)
EXTRACT_PAR(CAM_ANNOTATE,    (   JSN_TXT|CAM____|CAM____|CAM_REC|CAM_STR|CAM____|CAM____|CAM____), "Annotate",       "ann=",  "-a",    offsetof(RPIMAP, G_pcAnnotate),        MAX_PARM_LEN,     offsetof(RPIMAP, G_ubAnnotateChanged),        0, "12",                     NULL)
EXTRACT_PAR(CAM_ANNOTATEEX,  (   JSN_TXT|CAM____|CAM____|CAM_REC|CAM_STR|CAM____|CAM____|CAM____), "AnnotateEx",     "annx=", "-ae",   offsetof(RPIMAP, G_pcAnnotateEx),      MAX_PARM_LEN,     offsetof(RPIMAP, G_ubAnnotateExChanged),      0, "10,0xff,0x808000",       NULL)
   // Misc
EXTRACT_PAR(CAM_NRFILES,     (   JSN_INT|CAM____|CAM____|CAM____|CAM____|CAM____|CAM____|CAM_ALL), "NumFiles",       "dir=",  "",      offsetof(RPIMAP, G_pcNumFiles),        MAX_PARM_LEN,     ALWAYS_CHANGED,                               0, "0",                      NULL)
EXTRACT_PAR(CAM_FILTER,      (   JSN_TXT|CAM____|CAM____|CAM____|CAM_VID|CAM_PIX|CAM____|CAM____), "Filter",         "ftr=",  "",      offsetof(RPIMAP, G_pcFilter),          MAX_PATH_LEN,     ALWAYS_CHANGED,                               0, "*",                      NULL)
EXTRACT_PAR(CAM_STATUS,      (WB|JSN_TXT|CAM____|CAM____|CAM____|CAM____|CAM____|CAM____|CAM_ALL), "Status",         "",      "",      offsetof(RPIMAP, G_pcStatus),          MAX_PARM_LEN,     ALWAYS_CHANGED,                               0, "Cam-Idle",               NULL)
EXTRACT_PAR(CAM_DELETE,      (   JSN_TXT|CAM____|CAM____|CAM____|CAM____|CAM____|CAM____|CAM_ALL), "Delete",         "del=",  "",      offsetof(RPIMAP, G_pcDelete),          MAX_PARM_LEN,     ALWAYS_CHANGED,                               0, "",                       NULL)
EXTRACT_PAR(CAM_TMRNUMBER,   (   JSN_TXT|CAM____|CAM____|CAM____|CAM____|CAM____|CAM____|CAM_ALL), "Number",         "tlnr=", "",      offsetof(RPIMAP, G_pcTimNumber),       MAX_PARM_LEN,     offsetof(RPIMAP, G_ubTimNumberChanged),       0, "",                       NULL)
EXTRACT_PAR(CAM_ALM_CMD,     (   JSN_TXT|CAM____|CAM____|CAM____|CAM____|CAM_ALM|CAM____|CAM____), "AlarmCmd",       "acmd=", "",      offsetof(RPIMAP, G_pcAlarmCommand),    MAX_PARM_LEN,     ALWAYS_CHANGED,                               0, "none",                   NULL)
EXTRACT_PAR(CAM_ALM_TIME,    (   JSN_TXT|CAM____|CAM____|CAM____|CAM____|CAM_ALM|CAM____|CAM____), "AlarmTime",      "adat=", "",      offsetof(RPIMAP, G_pcAlarmDandT),      MAX_PARM_LEN,     ALWAYS_CHANGED,                               0, "........_......",        NULL)
EXTRACT_PAR(CAM_ALM_REPEAT,  (WB|JSN_TXT|CAM____|CAM____|CAM____|CAM____|CAM_ALM|CAM____|CAM____), "AlarmRep",       "arep=", "",      offsetof(RPIMAP, G_pcAlarmRep),        MAX_PARM_LEN,     ALWAYS_CHANGED,                               0, "0",                      NULL)
EXTRACT_PAR(CAM_ALM_NUMBER,  (WB|JSN_TXT|CAM____|CAM____|CAM____|CAM____|CAM_ALM|CAM____|CAM____), "AlarmNum",       "anum=", "",      offsetof(RPIMAP, G_pcAlarmNum),        MAX_PARM_LEN,     ALWAYS_CHANGED,                               0, "0",                      NULL)
EXTRACT_PAR(CAM_HOSTNAME,    (   JSN_TXT|CAM____|CAM____|CAM____|CAM____|CAM____|CAM____|CAM_ALL), "Hostname",       "",      "",      offsetof(RPIMAP, G_pcHostname),        MAX_URL_LEN,      ALWAYS_CHANGED,                               0, "",                       NULL)
EXTRACT_PAR(CAM_VERSION,     (WB|JSN_TXT|CAM____|CAM____|CAM____|CAM____|CAM____|CAM____|CAM_ALL), "Version",        "vrs=",  "",      offsetof(RPIMAP, G_pcVersionSw),       MAX_PARM_LEN,     ALWAYS_CHANGED,                               0, VERSION,                  NULL)
   // Motion Detect
EXTRACT_PAR(CAM_MOT_MODE,    (   JSN_TXT|CAM____|CAM____|CAM____|CAM____|CAM_VEC|CAM____|CAM____), "MotionMode",     "mmod=", "",      offsetof(RPIMAP, G_pcDetectMode),      MAX_PARM_LEN,     ALWAYS_CHANGED,                               0, "Off",                    http_MotionChanged)
EXTRACT_PAR(CAM_MOT_TRIGGER1,(   JSN_TXT|CAM____|CAM____|CAM____|CAM____|CAM_VEC|CAM____|CAM____), "MotionTrigger1", "mtr1=", "",      offsetof(RPIMAP, G_pcDetectTrigger1),  MAX_PARM_LEN,     ALWAYS_CHANGED,                               0, "5",                      http_MotionChanged)
EXTRACT_PAR(CAM_MOT_TRIGGER2,(   JSN_TXT|CAM____|CAM____|CAM____|CAM____|CAM_VEC|CAM____|CAM____), "MotionTrigger2", "mtr2=", "",      offsetof(RPIMAP, G_pcDetectTrigger2),  MAX_PARM_LEN,     ALWAYS_CHANGED,                               0, "10",                     http_MotionChanged)
EXTRACT_PAR(CAM_MOT_PIXELS,  (   JSN_TXT|CAM____|CAM____|CAM____|CAM____|CAM_VEC|CAM____|CAM____), "MotionPix",      "mdp=",  "",      offsetof(RPIMAP, G_pcDetectPixel),     MAX_PARM_LEN,     ALWAYS_CHANGED,                               0, "10",                     NULL)
EXTRACT_PAR(CAM_MOT_LEVEL,   (   JSN_TXT|CAM____|CAM____|CAM____|CAM____|CAM_VEC|CAM____|CAM____), "MotionLev",      "mdl=",  "",      offsetof(RPIMAP, G_pcDetectLevel),     MAX_PARM_LEN,     ALWAYS_CHANGED,                               0, "400",                    NULL)
EXTRACT_PAR(CAM_MOT_SEQ,     (WB|JSN_TXT|CAM____|CAM____|CAM____|CAM____|CAM_VEC|CAM____|CAM____), "MotionSeq",      "mds=",  "",      offsetof(RPIMAP, G_pcDetectSeq),       MAX_PARM_LEN,     ALWAYS_CHANGED,                               0, "0",                      NULL)
EXTRACT_PAR(CAM_MOT_COLOR,   (   JSN_TXT|CAM____|CAM____|CAM____|CAM____|CAM_VEC|CAM____|CAM____), "MotionColor",    "mcol=", "",      offsetof(RPIMAP, G_pcDetectColor),     MAX_PARM_LEN,     ALWAYS_CHANGED,                               0, "RGB",                    NULL)
EXTRACT_PAR(CAM_MOT_RED,     (   JSN_TXT|CAM____|CAM____|CAM____|CAM____|CAM_VEC|CAM____|CAM____), "MotionRed",      "mdr=",  "",      offsetof(RPIMAP, G_pcDetectRed),       MAX_PARM_LEN,     ALWAYS_CHANGED,                               0, "213",                    NULL)
EXTRACT_PAR(CAM_MOT_GREEN,   (   JSN_TXT|CAM____|CAM____|CAM____|CAM____|CAM_VEC|CAM____|CAM____), "MotionGrn",      "mdg=",  "",      offsetof(RPIMAP, G_pcDetectGreen),     MAX_PARM_LEN,     ALWAYS_CHANGED,                               0, "715",                    NULL)
EXTRACT_PAR(CAM_MOT_BLUE,    (   JSN_TXT|CAM____|CAM____|CAM____|CAM____|CAM_VEC|CAM____|CAM____), "MotionBlu",      "mdb=",  "",      offsetof(RPIMAP, G_pcDetectBlue),      MAX_PARM_LEN,     ALWAYS_CHANGED,                               0, "72",                     NULL)
EXTRACT_PAR(CAM_MOT_X,       (   JSN_TXT|CAM____|CAM____|CAM____|CAM____|CAM_VEC|CAM____|CAM____), "MotionX",        "mzx=",  "",      offsetof(RPIMAP, G_pcDetectZoomX),     MAX_PARM_LEN,     ALWAYS_CHANGED,                               0, "0",                      NULL)
EXTRACT_PAR(CAM_MOT_Y,       (   JSN_TXT|CAM____|CAM____|CAM____|CAM____|CAM_VEC|CAM____|CAM____), "MotionY",        "mzy=",  "",      offsetof(RPIMAP, G_pcDetectZoomY),     MAX_PARM_LEN,     ALWAYS_CHANGED,                               0, "0",                      NULL)
EXTRACT_PAR(CAM_MOT_W,       (   JSN_TXT|CAM____|CAM____|CAM____|CAM____|CAM_VEC|CAM____|CAM____), "MotionW",        "mzw=",  "",      offsetof(RPIMAP, G_pcDetectZoomW),     MAX_PARM_LEN,     ALWAYS_CHANGED,                               0, "499",                    NULL)
EXTRACT_PAR(CAM_MOT_H,       (   JSN_TXT|CAM____|CAM____|CAM____|CAM____|CAM_VEC|CAM____|CAM____), "MotionH",        "mzh=",  "",      offsetof(RPIMAP, G_pcDetectZoomH),     MAX_PARM_LEN,     ALWAYS_CHANGED,                               0, "399",                    NULL)
EXTRACT_PAR(CAM_MOT_AVG1,    (   JSN_TXT|CAM____|CAM____|CAM____|CAM____|CAM_VEC|CAM____|CAM____), "MotionAvg1",     "ma1=",  "",      offsetof(RPIMAP, G_pcDetectAvg1),      MAX_PARM_LEN,     ALWAYS_CHANGED,                               0, "0",                      NULL)
EXTRACT_PAR(CAM_MOT_AVG2,    (   JSN_TXT|CAM____|CAM____|CAM____|CAM____|CAM_VEC|CAM____|CAM____), "MotionAvg2",     "ma2=",  "",      offsetof(RPIMAP, G_pcDetectAvg2),      MAX_PARM_LEN,     ALWAYS_CHANGED,                               0, "0",                      NULL)
EXTRACT_PAR(CAM_MOT_TINY,    (   JSN_TXT|CAM____|CAM____|CAM____|CAM____|CAM_VEC|CAM____|CAM____), "MotionTiny",     "mvti=", "",      offsetof(RPIMAP, G_pcMvTiny),          MAX_PARM_LEN,     ALWAYS_CHANGED,                               0, "4",                      NULL)
EXTRACT_PAR(CAM_MOT_SMALL,   (   JSN_TXT|CAM____|CAM____|CAM____|CAM____|CAM_VEC|CAM____|CAM____), "MotionSmall",    "mvsm=", "",      offsetof(RPIMAP, G_pcMvSmall),         MAX_PARM_LEN,     ALWAYS_CHANGED,                               0, "8",                      NULL)
EXTRACT_PAR(CAM_MOT_MEDIUM,  (   JSN_TXT|CAM____|CAM____|CAM____|CAM____|CAM_VEC|CAM____|CAM____), "MotionMedium",   "mvme=", "",      offsetof(RPIMAP, G_pcMvMedium),        MAX_PARM_LEN,     ALWAYS_CHANGED,                               0, "12",                     NULL)
EXTRACT_PAR(CAM_MOT_LARGE,   (   JSN_TXT|CAM____|CAM____|CAM____|CAM____|CAM_VEC|CAM____|CAM____), "MotionLarge",    "mvla=", "",      offsetof(RPIMAP, G_pcMvLarge),         MAX_PARM_LEN,     ALWAYS_CHANGED,                               0, "30",                     NULL)
EXTRACT_PAR(CAM_MOT_HUGE,    (   JSN_TXT|CAM____|CAM____|CAM____|CAM____|CAM_VEC|CAM____|CAM____), "MotionHuge",     "mvhu=", "",      offsetof(RPIMAP, G_pcMvHuge),          MAX_PARM_LEN,     ALWAYS_CHANGED,                               0, "50",                     NULL)
EXTRACT_PAR(CAM_MOT_COUNT,   (WB|JSN_INT|CAM____|CAM____|CAM____|CAM____|CAM_VEC|CAM____|CAM____), "MotionCount",    "",      "",      offsetof(RPIMAP, G_pcDetectCount),     MAX_PARM_LEN,     ALWAYS_CHANGED,                               0, "0",                      NULL)
EXTRACT_PAR(CAM_MOT_DEBUG,   (WB|JSN_INT|CAM____|CAM____|CAM____|CAM____|CAM_VEC|CAM____|CAM____), "MotionDebug",    "",      "",      offsetof(RPIMAP, G_pcDetectDebug),     MAX_PARM_LEN,     ALWAYS_CHANGED,                               0, "0",                      http_CollectDebug)
EXTRACT_PAR(CAM_MOT_CLUSTER, (WB|JSN_INT|CAM____|CAM____|CAM____|CAM____|CAM_VEC|CAM____|CAM____), "MotionCluster",  "",      "",      offsetof(RPIMAP, G_pcDetectCluster),   MAX_PARM_LEN,     ALWAYS_CHANGED,                               0, "0",                      NULL)
   // The files :
EXTRACT_PAR(CAM_PIXPATH,     (   JSN_TXT|CAM_STI|CAM_TIM|CAM_REC|CAM_STR|CAM____|CAM_PRM|CAM_STP), "PixPath",        "",      "",      offsetof(RPIMAP, G_pcPixPath),         MAX_PATH_LEN,     offsetof(RPIMAP, G_ubPixPathChanged),         0, RPI_PUBLIC_PIX,           NULL)
EXTRACT_PAR(CAM_VIDEOPATH,   (   JSN_TXT|CAM_STI|CAM_TIM|CAM_REC|CAM_STR|CAM____|CAM_PRM|CAM_STP), "VideoPath",      "",      "",      offsetof(RPIMAP, G_pcVideoPath),       MAX_PATH_LEN,     offsetof(RPIMAP, G_ubVideoPathChanged),       0, RPI_PUBLIC_VIDEO,         NULL)
EXTRACT_PAR(CAM_LASTFILE,    (WB|JSN_TXT|CAM____|CAM____|CAM____|CAM____|CAM____|CAM_PRM|CAM_ALL), "LastFile",       "o=",    "-o",    offsetof(RPIMAP, G_pcLastFile),        MAX_PATH_LEN,     ALWAYS_CHANGED,                               0, "-",                      NULL)
EXTRACT_PAR(CAM_LASTFILESIZE,(WB|JSN_INT|CAM____|CAM____|CAM____|CAM____|CAM____|CAM_PRM|CAM_ALL), "LastFileSize",   "",      "",      offsetof(RPIMAP, G_pcLastFileSize),    MAX_PARM_LEN,     NEVER_CHANGED,                                0, "0",                      NULL)
EXTRACT_PAR(CAM_WWW_DIR,     (   JSN_TXT|CAM_STI|CAM_TIM|CAM_REC|CAM_STR|CAM____|CAM_PRM|CAM____), "WwwDir",         "",      "",      offsetof(RPIMAP, G_pcWwwDir),          MAX_PATH_LEN,     ALWAYS_CHANGED,                               0, RPI_PUBLIC_WWW,           NULL)
EXTRACT_PAR(CAM_RAM_DIR,     (   JSN_TXT|CAM_STI|CAM_TIM|CAM_REC|CAM_STR|CAM____|CAM_PRM|CAM____), "RamDir",         "",      "",      offsetof(RPIMAP, G_pcRamDir),          MAX_PATH_LEN,     ALWAYS_CHANGED,                               0, RPI_RAMFS_DIR,            NULL)
EXTRACT_PAR(CAM_DEFAULT,     (   JSN_TXT|CAM____|CAM____|CAM____|CAM____|CAM____|CAM_PRM|CAM____), "Default",        "",      "",      offsetof(RPIMAP, G_pcDefault),         MAX_PATH_LEN,     ALWAYS_CHANGED,                               0, RPI_PUBLIC_DEFAULT,       NULL)
   // Streamer/server
EXTRACT_PAR(CAM_SADDR,       (WB|JSN_TXT|CAM____|CAM____|CAM____|CAM_STR|CAM____|CAM_PRM|CAM____), "StreamAddr",     "",      "",      offsetof(RPIMAP, G_pcStreamAddr),      MAX_ADDR_LEN,     NEVER_CHANGED,                                0, "",                       NULL)
EXTRACT_PAR(CAM_POP3_USER,   (   JSN_TXT|CAM____|CAM____|CAM____|CAM____|CAM____|CAM_PRM|CAM____), "Pop3User",       "",      "",      offsetof(RPIMAP, G_pcPop3User),        MAX_MAIL_LEN,     NEVER_CHANGED,                                0, "nemsi@ziggo.nl",         http_MailChanged)
EXTRACT_PAR(CAM_POP3_PASS,   (   JSN_TXT|CAM____|CAM____|CAM____|CAM____|CAM____|CAM_PRM|CAM____), "Pop3Pass",       "",      "",      offsetof(RPIMAP, G_pcPop3Pass),        MAX_PASS_LEN,     NEVER_CHANGED,                                0, "",                       http_MailChanged)
EXTRACT_PAR(CAM_POP3_SRVR,   (   JSN_TXT|CAM____|CAM____|CAM____|CAM____|CAM____|CAM_PRM|CAM____), "Pop3Server",     "",      "",      offsetof(RPIMAP, G_pcPop3Server),      MAX_MAIL_LEN,     NEVER_CHANGED,                                0, "pop.ziggo.nl",           http_MailChanged)
EXTRACT_PAR(CAM_POP3_PORT,   (   JSN_INT|CAM____|CAM____|CAM____|CAM____|CAM____|CAM_PRM|CAM____), "Pop3Port",       "",      "",      offsetof(RPIMAP, G_pcPop3Port),        MAX_PARM_LEN,     NEVER_CHANGED,                                0, "110",                    http_MailChanged)
//
EXTRACT_PAR(CAM_SMTP_USER,   (   JSN_TXT|CAM____|CAM____|CAM____|CAM____|CAM____|CAM_PRM|CAM____), "SmtpUser",       "",      "",      offsetof(RPIMAP, G_pcSmtpUser),        MAX_MAIL_LEN,     NEVER_CHANGED,                                0, "nemsi@ziggo.nl",         http_MailChanged)
EXTRACT_PAR(CAM_SMTP_PASS,   (   JSN_TXT|CAM____|CAM____|CAM____|CAM____|CAM____|CAM_PRM|CAM____), "SmtpPass",       "",      "",      offsetof(RPIMAP, G_pcSmtpPass),        MAX_PASS_LEN,     NEVER_CHANGED,                                0, "",                       http_MailChanged)
EXTRACT_PAR(CAM_SMTP_SRVR,   (   JSN_TXT|CAM____|CAM____|CAM____|CAM____|CAM____|CAM_PRM|CAM____), "SmtpServer",     "",      "",      offsetof(RPIMAP, G_pcSmtpServer),      MAX_MAIL_LEN,     NEVER_CHANGED,                                0, "smtp.ziggo.nl",          http_MailChanged)
EXTRACT_PAR(CAM_SMTP_PORT,   (   JSN_INT|CAM____|CAM____|CAM____|CAM____|CAM____|CAM_PRM|CAM____), "SmtpPort",       "",      "",      offsetof(RPIMAP, G_pcSmtpPort),        MAX_PARM_LEN,     NEVER_CHANGED,                                0, "25",                     http_MailChanged)
//
EXTRACT_PAR(CAM_SPORT,       (   JSN_TXT|CAM____|CAM____|CAM____|CAM_STR|CAM____|CAM_PRM|CAM____), "StreamPort",     "",      "",      offsetof(RPIMAP, G_pcStreamPort),      MAX_PARM_LEN,     NEVER_CHANGED,                                0, "5001",                   NULL)
EXTRACT_PAR(CAM_MYIP,        (WB|JSN_TXT|CAM____|CAM____|CAM____|CAM_STR|CAM____|CAM_PRM|CAM_ERR), "MyIpAddr",       "",      "",      offsetof(RPIMAP, G_pcMyIpAddr),        MAX_ADDR_LEN,     NEVER_CHANGED,                                0, "",                       NULL)
EXTRACT_PAR(CAM_IP,          (WB|JSN_TXT|CAM____|CAM____|CAM____|CAM_STR|CAM____|CAM_PRM|CAM_ERR), "IpAddr",         "",      "",      offsetof(RPIMAP, G_pcIpAddr),          MAX_ADDR_LEN,     offsetof(RPIMAP, G_ubIpAddrChanged),          0, "",                       NULL)
EXTRACT_PAR(CAM_WAN,         (   JSN_TXT|CAM____|CAM____|CAM____|CAM____|CAM____|CAM_PRM|CAM____), "WanAddr",        "",      "",      offsetof(RPIMAP, G_pcWanAddr),         MAX_ADDR_LEN,     NEVER_CHANGED,                                0, "82.74.136.237",          NULL)
EXTRACT_PAR(CAM_SRVR_PORT,   (   JSN_TXT|CAM____|CAM____|CAM____|CAM_STR|CAM____|CAM_PRM|CAM_ERR), "ServerPort",     "",      "",      offsetof(RPIMAP, G_pcSrvrPort),        MAX_PARM_LEN,     NEVER_CHANGED,                                0, "80",                     NULL)
EXTRACT_PAR(CAM_KODI_PORT,   (   JSN_TXT|CAM____|CAM____|CAM____|CAM____|CAM____|CAM_PRM|CAM_ERR), "KodiPort",       "",      "",      offsetof(RPIMAP, G_pcKodiPort),        MAX_PARM_LEN,     NEVER_CHANGED,                                0, "8080",                   NULL)
EXTRACT_PAR(CAM_DEBUG_MASK,  (   JSN_TXT|CAM____|CAM____|CAM____|CAM____|CAM____|CAM_PRM|CAM_ERR), "DebugMask",      "",      "",      offsetof(RPIMAP, G_pcDebugMask),       MAX_PARM_LEN,     NEVER_CHANGED,                                0, "0",                      http_CollectDebug )
EXTRACT_PAR(CAM_RCU_KEY,     (WB|JSN_TXT|CAM____|CAM____|CAM____|CAM____|CAM____|CAM____|CAM_PRM), "Key",            "key=",  "",      offsetof(RPIMAP, G_pcRemoteKey),       MAX_PARM_LEN,     NEVER_CHANGED,                                0, "0",                      http_CollectRcuKey)
EXTRACT_PAR(CAM_SEG_FAULT,   (   JSN_TXT|CAM____|CAM____|CAM____|CAM____|CAM____|CAM____|CAM_PRM), "Segv",           "",      "",      offsetof(RPIMAP, G_pcSegFaultFile),    MAX_URL_LEN,      NEVER_CHANGED,                                0, "",                       NULL)
EXTRACT_PAR(CAM_REMOTE_IO,   (WB|JSN_INT|CAM____|CAM____|CAM____|CAM____|CAM____|CAM____|CAM_PRM), "RemIo",          "",      "",      offsetof(RPIMAP, G_pcRemoteIo),        MAX_PARM_LEN,     ALWAYS_CHANGED,                               0, "0",                      NULL)
EXTRACT_PAR(CAM_REMOTE_IP,   (   JSN_TXT|CAM____|CAM____|CAM____|CAM____|CAM____|CAM____|CAM_PRM), "RemIp",          "",      "",      offsetof(RPIMAP, G_pcRemoteIp),        MAX_ADDR_LEN,     ALWAYS_CHANGED,                               0, "192.168.178.205",        NULL)
EXTRACT_PAR(CAM_REMOTE_ON_1, (   JSN_TXT|CAM____|CAM____|CAM____|CAM____|CAM____|CAM____|CAM_PRM), "RemOn1",         "",      "",      offsetof(RPIMAP, G_pcRemoteOn1),       MAX_PARM_LEN,     ALWAYS_CHANGED,                               0, "30",                     NULL)
EXTRACT_PAR(CAM_REMOTE_ON_2, (   JSN_TXT|CAM____|CAM____|CAM____|CAM____|CAM____|CAM____|CAM_PRM), "RemOn2",         "",      "",      offsetof(RPIMAP, G_pcRemoteOn2),       MAX_PARM_LEN,     ALWAYS_CHANGED,                               0, "20",                     NULL)
EXTRACT_PAR(CAM_REMOTE_ON_3, (   JSN_TXT|CAM____|CAM____|CAM____|CAM____|CAM____|CAM____|CAM_PRM), "RemOn3",         "",      "",      offsetof(RPIMAP, G_pcRemoteOn3),       MAX_PARM_LEN,     ALWAYS_CHANGED,                               0, "5",                      NULL)
