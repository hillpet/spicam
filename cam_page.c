/*  (c) Copyright:  2017  Patrn, Confidential Data 
 *
 *  Workfile:           cam_page.c
 *  Revision:          
 *  Modtime:           
 *
 *  Purpose:             Handler functions for the CAM dynamic HTTP pages
 *
 *
 *
 *  Compiler/Assembler: Raspbian Linux GNU gcc
 *  Ext Packages:
 *
 *  Author:             Peter Hillen
 *  Date Created:       16 Mar 2017
 *
 *  Revisions:
 *  Entry Points:       
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
**/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdarg.h>
#include <stddef.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>
#include <sched.h>
#include <time.h>
#include <errno.h>

#include <common.h>
#include "config.h"
#include "globals.h"
#include "gen_http.h"
//
#include "cam_page.h"

//#define USE_PRINTF
#include <printf.h>

//
// Dynamic page handlers
//
static bool cam_DynPageCameraHtml         (GLOCMD *, int);
static bool cam_DynPageCameraJson         (GLOCMD *, int);
static bool cam_DynPageSnapshotHtml       (GLOCMD *, int);
static bool cam_DynPageSnapshotJson       (GLOCMD *, int);
static bool cam_DynPageTimelapseHtml      (GLOCMD *, int);
static bool cam_DynPageTimelapseJson      (GLOCMD *, int);
static bool cam_DynPageRecordHtml         (GLOCMD *, int);
static bool cam_DynPageRecordJson         (GLOCMD *, int);
static bool cam_DynPageStreamHtml         (GLOCMD *, int);
static bool cam_DynPageStreamJson         (GLOCMD *, int);
//
static const CMDIDYN stDynamicPages[] =
{
//    tUrl,    tType,   iTimeout,   iFlags,  pcUrl,   pfDynCb;
#define  EXTRACT_DYN(a,b,c,d,e,f)   {a,b,c,d,e,f},
#include "dyn_cam.h"
#undef EXTRACT_DYN
   {  -1,      -1,      0,          0,       NULL,    NULL  }
};


//
// Local prototypes
//


/*------  Local functions separator ------------------------------------
__GLOBAL_FUNCTIONS(){};
----------------------------------------------------------------------------*/

//
// Function:   CAM_FindDynamicPage
// Purpose:    Search for this dynamic page
//
// Parms:      The page
// Returns:    The index or -1
//
int CAM_FindDynamicPage(const char *pcPage)
{
   int      iIdx=-1, x=0;
   char    *pcDynPage;

   do
   {
      pcDynPage = GEN_GetDynamicPageName(x);
      //PRINTF3("CAM_FindDynamicPage(): %2d [%s] [%s]" CRLF, x, pcPage, pcDynPage);
      if(GEN_STRCMP(pcPage, pcDynPage) == 0)
      {
         //PRINTF2("CAM_FindDynamicPage(): Found [%d]=%s" CRLF, x, pcDynPage);
         iIdx = x;
         break;
      }
      x++;
   }
   while(pcDynPage);
   //
   return(iIdx);
}

// 
// Function:   CAM_InitDynamicPages
// Purpose:    Register dynamic webpages
// 
// Parameters: 
// Returns:    Nr registered
// 
int CAM_InitDynamicPages(void)
{
   int            iNr=0, iPort;
   const CMDIDYN *pstDyn=stDynamicPages;

   iPort = atoi(pstMap->G_pcSrvrPort);
   PRINTF1("CAM_InitDynamicPages(): port=%d" CRLF, iPort);
   //
   while(pstDyn->pcUrl)
   {
      PRINTF1("CAM_InitDynamicPages():%s" CRLF, pstDyn->pcUrl);
      if(pstDyn->iFlags & DYN_FLAG_PORT)
      {
         //
         // Register for this specific port
         //
         GEN_RegisterDynamicPage(pstDyn->tUrl, PID_CAMR, pstDyn->tType, pstDyn->iTimeout, iPort, pstDyn->pcUrl, (PFVOIDPI)pstDyn->pfDynCb);
      }
      else
      {
         //
         // Register for all ports
         //
         GEN_RegisterDynamicPage(pstDyn->tUrl, PID_CAMR, pstDyn->tType, pstDyn->iTimeout,     0, pstDyn->pcUrl, (PFVOIDPI)pstDyn->pfDynCb);
      }
      iNr++;
      pstDyn++;
   }
   return(iNr);
}

// 
// Function:   CAM_DynamicPageHandler
// Purpose:    Execute the dynamic webpage callback
// 
// Parameters: Command struct^, Page index
// Returns:  
// 
bool CAM_DynamicPageHandler(GLOCMD *pstCmd, int iIdx)
{
   bool     fCc=FALSE;
   PFVOIDPI pfDynCb;

   PRINTF1("CAM_DynamicPageHandler():Idx=%d" CRLF, iIdx);
   //
   pfDynCb = GEN_GetDynamicPageCallback(iIdx);
   if(pfDynCb)
   {
      fCc = pfDynCb(pstCmd, iIdx);
   }
   return(fCc);
}

/*------  Local functions separator ------------------------------------
__PAGE_HANDLERS(){};
----------------------------------------------------------------------------*/

//
// Function:   cam_DynPageCameraHtml
// Purpose:    Dynamically created Raspicam page
//
// Parms:      Client
// Returns:    TRUE if OKee
// Note:       
//             This call acts as a router to Linux command shell
//             GET /cmd.html?raspistill&...&...
//               --> system("/opt/vc/bin/raspistill -o file.jpg")
//                   return file.jpg to caller as output
//             
static bool cam_DynPageCameraHtml(GLOCMD *pstCmd, int iIdx)
{
   PRINTF1("cam_DynPageCameraHtml():Idx=%d" CRLF, iIdx);
   return(TRUE);
}

//
// Function:   cam_DynPageCameraJson
// Purpose:    Dynamically created Raspicam (JSON-API) page
//
// Parms:      Command, Dyn Page index
// Returns:    TRUE if OKee
// Note:       
//             This call acts as a router to Linux command shell
//             GET /cam.json?command=snapshot&parm1=xx&parm2=yyy...
//
static bool cam_DynPageCameraJson(GLOCMD *pstCmd, int iIdx)
{
   PRINTF1("cam_DynPageCameraJson():Idx=%d" CRLF, iIdx);
   return(TRUE);
}

//
// Function:   cam_DynPageRecordHtml
// Purpose:    Dynamically created Raspicam (HTML-API) page
//
// Parms:      Command, Dyn Page index
// Returns:    TRUE if OKee
// Note:       
//             
static bool cam_DynPageRecordHtml(GLOCMD *pstCmd, int iIdx)
{
   PRINTF1("cam_DynPageRecordHtml():Idx=%d" CRLF, iIdx);
   return(TRUE);
}

//
// Function:   cam_DynPageRecordJson
// Purpose:    Dynamically created Raspicam (JSON-API) page
//
// Parms:      Command, Dyn Page index
// Returns:    TRUE if OKee
// Note:       
//             
static bool cam_DynPageRecordJson(GLOCMD *pstCmd, int iIdx)
{
   PRINTF1("cam_DynPageRecordJson():Idx=%d" CRLF, iIdx);
   return(TRUE);
}

//
// Function:   cam_DynPageSnapshotHtml
// Purpose:    Dynamically created Raspicam (HTML-API) page
//
// Parms:      Command, Dyn Page index
// Returns:    TRUE if OKee
// Note:       
//             
//             
static bool cam_DynPageSnapshotHtml(GLOCMD *pstCmd, int iIdx)
{
   PRINTF1("cam_DynPageSnapshotHtml():Idx=%d" CRLF, iIdx);
   return(TRUE);
}

//
// Function:   cam_DynPageSnapshotJson
// Purpose:    Dynamically created Raspicam (JSON-API) page
//
// Parms:      Command, Dyn Page index
// Returns:    TRUE if OKee
// Note:       
//             
//             
static bool cam_DynPageSnapshotJson(GLOCMD *pstCmd, int iIdx)
{
   PRINTF1("cam_DynPageSnapshotJson(): Idx=%d" CRLF, iIdx);
   return(TRUE);
}

//
// Function:   cam_DynPageStreamHtml
// Purpose:    Dynamically created Raspicam (HTML-API) page
//
// Parms:      Command, Dyn Page index
// Returns:    TRUE if OKee
// Note:       
//             
static bool cam_DynPageStreamHtml(GLOCMD *pstCmd, int iIdx)
{
   PRINTF1("cam_DynPageStreamHtml():Idx=%d" CRLF, iIdx);
   return(TRUE);
}

//
// Function:   cam_DynPageStreamJson
// Purpose:    Dynamically created Raspicam (JSON-API) page
//
// Parms:      Command, Dyn Page index
// Returns:    TRUE if OKee
// Note:       
//             
static bool cam_DynPageStreamJson(GLOCMD *pstCmd, int iIdx)
{
   PRINTF1("cam_DynPageStreamJson():Idx=%d" CRLF, iIdx);
   return(TRUE);
}

//
// Function:   cam_DynPageTimelapseHtml
// Purpose:    Dynamically created Raspicam (HTML-API) page
//
// Parms:      Command, Dyn Page index
// Returns:    TRUE if OKee
// Note:       
//             
static bool cam_DynPageTimelapseHtml(GLOCMD *pstCmd, int iIdx)
{
   PRINTF1("cam_DynPageTimelapseHtml():Idx=%d" CRLF, iIdx);
   return(TRUE);
}

//
// Function:   cam_DynPageTimelapseJson
// Purpose:    Dynamically created Raspicam (JSON-API) page
//
// Parms:      Command, Dyn Page index
// Returns:    TRUE if OKee
// Note:       
//             
static bool cam_DynPageTimelapseJson(GLOCMD *pstCmd, int iIdx)
{
   PRINTF1("cam_DynPageTimelapseJson():Idx=%d" CRLF, iIdx);
   return(TRUE);
}
