/*  (c) Copyright:  2017  Patrn.nl, Confidential Data
 *
 *  Workfile:           vector.h
 *  Revision:  
 *  Modtime:   
 *
 *  Purpose:            motion vector detection header file
 *
 *
 *
 *  Compiler/Assembler: Raspbian Linux GNU gcc
 *  Ext Packages:
 *
 *  Author:             Peter Hillen
 *  Date Created:       24 Feb 2017
 * 
 *  Revisions:
 *
 * 
 * 
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
**/

#ifndef _VECTOR_H_
#define _VECTOR_H_


//
// The motion vector data is piped from raspivid into the PID_UTIL thread.
//
#define MV_FILE_BUFFER_SIZE   250               // MV from file buffer size
#define MACRO_BLOCK_SIZE      16                // Macro block is 16x16 pixels
//
#define NUM_FRAMES            8                 // Number of frames in buffer
#define NUM_BGND_FRAMES       8                 // Number of background frames to average
//
// Histogram for XY and SAD samples:
//
//    Motion Vectors:   Range = 0 ... sqrt(128^2+128^2) = 181.02 = MAX_VECTOR
//    SAD:              Range = 0 ... 65535                      = MAX_SAD
//
#define HISTO_BARS            16                      // Number XY and SAD samples
#define HISTO_BARS_MASK       (HISTO_BARS-1)          // Mask
//
#define MAX_VECTOR            181                     // Max index into histogram =sqrt(128^2+128^2)
#define MAX_SAD               65535                   // Max SAD
#define MAX_VECTOR_INDEX      (MAX_VECTOR/HISTO_BARS) // Max index into histogram =sqrt(128^2+128^2)
#define MAX_WEIGHT_VECTOR     (MAX_VECTOR*MAX_SAD)    // Max weighted vector = sqrt(128^2+128^2) * MaxSAD
#define MAX_SAD_INDEX         (MAX_SAD/HISTO_BARS)    //
#define MOTION_BORDER         'x'                     // Border of histogram
//
typedef enum _mv_sort_keys_
{
   MVKEY_NON_ZERO = 0,              // Non-zero Natural order (all zero vectors at the bottom)
   MVKEY_LH_NAT,                    // Low  --> High Natural order
   MVKEY_HL_NAT,                    // High --> Low  Natural order
   MVKEY_LH_X,                      // Low  --> High X motion vector
   MVKEY_HL_X,                      // High --> Low  X motion vector
   MVKEY_LH_Y,                      // Low  --> High Y motion vector
   MVKEY_HL_Y,                      // High --> Low  Y motion vector
   MVKEY_LH_SAD,                    // Low  --> High Sum of abs diffs
   MVKEY_HL_SAD,                    // High --> Low  Sum of abs diffs
   MVKEY_LH_NMV,                    // Low  --> High Normal motion vectors
   MVKEY_HL_NMV,                    // High --> Low  Normal motion vectors
   MVKEY_LH_WMV,                    // Low  --> High Weight motion vectors
   MVKEY_HL_WMV,                    // High --> Low  Weight motion vectors
   MVKEY_LH_CID,                    // Low  --> High X cluster ID
   MVKEY_HL_CID,                    // High --> Low  X cluster ID
   //
   NUM_MVKEYS
}  MVKEY;

typedef enum _motion_types_
{
   MOTION_NONE = 0,                 // No motion
   MOTION_NOISE,                    // many small clusters, Noise
   MOTION_TINY,                     // few small clusters
   MOTION_SMALL,                    // ..
   MOTION_MEDIUM,                   // ..
   MOTION_LARGE,                    // ..
   MOTION_HUGE,                     // ..
   MOTION_STOPPED,                  // Motion vector stream stopped
   MOTION_ERROR,                    // Error
   //
   NUM_MOTIONS,                     //  Number of motions
}  MOTYPE;

enum
{
   MV_DIR_ZERO    = 0,              // ---
   MV_DIR_W,                        // West
   MV_DIR_S,                        // South
   MV_DIR_SW,                       // South-West
   MV_DIR_E,                        // East
   MV_DIR_NONE_5,                   // ---
   MV_DIR_SE,                       // South-East
   MV_DIR_NONE_7,                   // ---
   MV_DIR_N,                        // North
   MV_DIR_NW,                       // North-West
   MV_DIR_NONE_10,                  // ---
   MV_DIR_NONE_11,                  // ---
   MV_DIR_NE,                       // North-East
   MV_DIR_NONE_13,                  // ---
   MV_DIR_NONE_14,                  // ---
   MV_DIR_NONE_15,                  // ---
   //
   NUM_MV_DIR
};
//
typedef struct _motion_vector_
{
   int8     bVectorX;               // X vector -128..+127
   int8     bVectorY;               // Y vector -128..+127
   u_int16  usSad;                  // Sum of absolute differences
}  MVEC;
//
typedef struct _motion_cluster_
{
   u_int16  usDist;                 // Neighbour distance
   u_int16  usMinNeighbours;        // Min nr of neighbours
   u_int16  usMaxNeighbours;        // Max nr of neighbours
}  MCLUS;

//
typedef struct MVIMG
{
   u_int16  usMinFgndSad;              // Min foreground sad
   u_int16  usMaxFgndSad;              // Max foreground sad
   //
   u_int16  usAvgFgndSad;              // Average Forground  SAD
   u_int16  usAvgBgndSad;              // Average Background SAD
   u_int16  usCurBgndSad;              // Current Background SAD
   u_int16  usMaxBgndSad;              // Maximum Background SAD
   //
   int      iMotionVectorsHor;         // Default number of MVs per screen width
   int      iMotionVectorsVer;         // Default number of MVs per screen height
   int      iNumMotionVectors;         // Total number of MVs (Hor x Ver)
   int      iCalMotionVectors;         // Number of MVs, calculated, non-zero
   int      iMaxNormalVector;          // Max Normal   vector = sqrt(X^2+Y^2)
   int      iMaxWeightVector;          // Max Weigthed vector = sqrt(X^2+Y^2) * SAD
   u_int16  usIdxMaxNormalVector;      // Idx to Max Normal   vector
   u_int16  usIdxMaxWeightVector;      //        Max Weigthed vector
   //
   u_int32  ulSecsMotion;              // System secs at motion detect
   int      iTotCount;                 // Counter total  samples (including masked samples)
   int      iActCount;                 // Counter actual samples (only non-masked  samples)
   MVKEY    tSorted;                   // Sorting order of pusKeys
   MOTYPE   tFrameMotion[NUM_FRAMES];  // Motion in each cached frame
   //
   u_int16  usNrOfClusters;            // Number of Clusters
   u_int16  usMaxClusterSize;          // Max Cluster Size (1..?)
   u_int16  usMaxClusterSizeIdx;       // Max Cluster Size Cluster index
   u_int16  usMaxClusterSizeCid;       // Max Cluster Size Cluster ID
   //
   int      iFrameNr;                  // Frames read
   int      iFrameIdx;                 // Frame index
   MVEC    *pstFrames;                 // Multiple frame buffer
   int     *piVector;                  // Buffer for calculated motion vectors
   u_int16 *pusKeys;                   // Buffer for sorting index for calculated motion vectors
   u_int8  *pubMask;                   // Buffer for imake pixel mask
   u_int16 *pusClusters;               // Buffer for cluster IDs
   char    *pcVirtual;                 // Buffer for virtual sysmbols
}  MVIMG;
//
typedef bool(*PFSORT)(MVIMG *, u_int16);
//
// Global prototypes
//
MVIMG   *VEC_InitMotion                (void);
//
void     VEC_CopyVirtualData           (MVIMG *);
u_int16  VEC_GetClusterSize            (MVIMG *, u_int16);
u_int16  VEC_GetClusterCenter          (MVIMG *, u_int16);
int      VEC_GetClusterStrength        (MVIMG *, u_int16, int, int);
void     VEC_AssignClusterIds          (MVIMG *);
int      VEC_ReadMotionFrame           (MVIMG *, int);
void     VEC_SkipMotionFrame           (MVIMG *, int);
u_int16  VEC_RemoveCluster             (MVIMG *, u_int16);
void     VEC_SetMotionBackground       (MVIMG *, int);
void     VEC_SortMotionVectors         (MVIMG *, MVKEY);
void     VEC_SaveMotionVectors         (MVIMG *, const char *);
//
void     VEC_ExitMotion                (MVIMG *);

#endif  /*_VECTOR_H_ */

