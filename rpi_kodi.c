/*  (c) Copyright:  2017..2018  Patrn, Confidential Data
 *
 *  Workfile:           rpi_kodi.c
 *  Revision:          
 *  Modtime:           
 *
 *  Purpose:            
 *
 *
 *
 *  Compiler/Assembler: Raspbian Linux GNU gcc
 *  Ext Packages:
 *
 *  Author:             Peter Hillen
 *  Date Created:       11 Aug 2017
 *
 *  Revisions:
 *  Entry Points:       
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
**/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdarg.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>
#include <sched.h>
#include <time.h>

#include <common.h>
#include "config.h"
#include "globals.h"
#include "gen_http.h"
#include "gen_func.h"
#include "rpi_func.h"
#include "rpi_page.h"
#include "rpi_kodi.h"

//#define USE_PRINTF
#include <printf.h>

#ifdef  DEBUG_ASSERT                   // defined in makefile
#define LOGNET1(a)                     LOG_debug(DEBUG_NET,a)
#define LOGNET2(a,b)                   LOG_debug(DEBUG_NET,a,b)
//
#define LOGRCU1(a)                     LOG_debug(DEBUG_RCU,a)
#define LOGRCU2(a,b)                   LOG_debug(DEBUG_RCU,a,b)
#define LOGRCU3(a,b,c)                 LOG_debug(DEBUG_RCU,a,b,c)
//
#else
//
#define LOGNET1(a)
#define LOGNET2(a,b)
//
#define LOGRCU1(a)
#define LOGRCU2(a,b)
#define LOGRCU3(a,b,c)
#endif  //DEBUG_ASSERT
//
#ifdef   FEATURE_SHOW_KODI_KEYS
#define  KODI_KEY(x,y,z)                  LOG_printf(x,y,z)
#else    
#define  KODI_KEY(x,y,z)
#endif   //FEATURE_SHOW_KODI_KEYS

typedef bool (*PFJRPC)(NETCL *, int, u_int8);
typedef struct _rcu_jsonrpc_
{
   u_int8      ubKey;
   const char *pcRpc;
   PFJRPC      pfFunc;
}  RCURPC;

//
//
// Local prototypes
//
static void kodi_Execute                  (void);
//
static bool kodi_CallbackConnection       (NETCL *);
static int  kodi_HandleHttpRequest        (NETCL *);
static int  kodi_InitDynamicPages         (void);
static void kodi_HandleGuard              (void);
//
static bool kodi_DynPageKodiRpc           (NETCL *, int);
static bool kodi_DynPageXbmc              (NETCL *, int);
static bool kodi_LookupJsonRpc            (NETCL *, const RCURPC *, char *);
//
static bool kodi_KodiRpcExecuteAction     (NETCL *, int, u_int8);
static bool kodi_KodiRpcGetAction         (NETCL *, int, u_int8);
static bool kodi_KodiRpcGetInputKeys      (NETCL *, int, u_int8);
static bool kodi_KodiRpcGetInputSendText  (NETCL *, int, u_int8);
static bool kodi_KodiRpcGetPing           (NETCL *, int, u_int8);
static bool kodi_KodiRpcGetProperties     (NETCL *, int, u_int8);
static bool kodi_KodiRpcGetPlayers        (NETCL *, int, u_int8);
static bool kodi_KodiRpcGetPlaylistItems  (NETCL *, int, u_int8);
//
static void kodi_SendSignalToHandler      (int);
static bool kodi_SignalRegister           (sigset_t *);
static void kodi_ReceiveSignalSegmnt      (int);
static void kodi_ReceiveSignalInt         (int);
static void kodi_ReceiveSignalTerm        (int);
static void kodi_ReceiveSignalUser1       (int);
static void kodi_ReceiveSignalUser2       (int);
//
#ifdef  FEATURE_KODI_SHOW_PAYLOAD
static void kodi_ShowPayload           (int, char *, int);
#define KODI_SHOW_PAYLOAD(x,y,z)       kodi_ShowPayload(x,y,z)
#else
#define KODI_SHOW_PAYLOAD(x,y,z)
#endif  //FEATURE_KODI_SHOW_PAYLOAD
//
#ifdef  FEATURE_KODI_LOG_PAYLOAD
static void kodi_LogPayload           (const char *, int, char *);
#define KODI_LOG_PAYLOAD(x,y,z)        kodi_LogPayload(x,y,z)
#else
#define KODI_LOG_PAYLOAD(x,y,z)
#endif  //FEATURE_KODI_LOG_PAYLOAD

#define  EXTRACT_DYN(a,b,c,d,e,f)   {a,b,c,d,e,f},
static const NETDYN stDynamicKodiPages[] =
{
//    tUrl,    tType,   iTimeout,   iFlags,  pcUrl,   pfDynCb;
   #include "dyn_rcu.h"
   {  -1,      0,       0,          0,       NULL,    NULL  }
};
#undef EXTRACT_DYN

//
// Global http strings
//
extern const char *pcWebPageTitle;
extern const char *pcHttpResponseHeader;
extern const char *pcWebPageStart;
extern const char *pcWebPageDefault;
extern const char *pcWebPageEnd;

static const RCURPC stKodiJsonRpc[] =
{
   // ubKey       pcRpc                                     pfFunc
   {  RCU_NOKEY,  "Application.GetProperties",              kodi_KodiRpcGetProperties     },
   {  RCU_NOKEY,  "Player.GetActivePlayers",                kodi_KodiRpcGetPlayers        },
   {  RCU_NOKEY,  "JSONRPC.Ping",                           kodi_KodiRpcGetPing           },
   {  RCU_UP,     "Input.Up",                               kodi_KodiRpcGetInputKeys      },
   {  RCU_DOWN,   "Input.Down",                             kodi_KodiRpcGetInputKeys      },
   {  RCU_LEFT,   "Input.Left",                             kodi_KodiRpcGetInputKeys      },
   {  RCU_RIGHT,  "Input.Right",                            kodi_KodiRpcGetInputKeys      },
   {  RCU_OK,     "Input.Select",                           kodi_KodiRpcGetInputKeys      },
   {  RCU_ESC,    "Input.Back",                             kodi_KodiRpcGetInputKeys      },
   {  RCU_POWER,  "Input.ShowOSD",                          kodi_KodiRpcGetInputKeys      },
   {  RCU_MENU,   "Input.Home",                             kodi_KodiRpcGetInputKeys      },
   {  RCU_INFO,   "Input.Info",                             kodi_KodiRpcGetInputKeys      },
   {  RCU_NOKEY,  "Input.SendText",                         kodi_KodiRpcGetInputSendText },
   {  RCU_NOKEY,  "Playlist.GetItems",                      kodi_KodiRpcGetPlaylistItems  },
   {  RCU_NOKEY,  "GUI.ActivateWindows",                    kodi_KodiRpcGetPlayers        },
   //
   {  RCU_NOKEY,  "Input.ExecuteAction",                    kodi_KodiRpcExecuteAction     },
   {  RCU_NOKEY,  NULL,                                     NULL                          }
};
//
static const RCURPC stKodiJsonRpcAction[] =
{
   // ubKey       pcRpc                                     pfFunc
   {  RCU_MENU,  "contextmenu",                             kodi_KodiRpcGetAction         },
   {  RCU_INFO,  "info",                                    kodi_KodiRpcGetAction         },
   {  RCU_POWER, "osd",                                     kodi_KodiRpcGetAction         },
   {  RCU_NOKEY, NULL,                                      NULL                          }
};
//
static bool          fPopKodiRunning=TRUE;
static volatile int  tKodiConnection=KCNX_INIT_CONNECT;


/*------  Local functions separator ------------------------------------
__GLOBAL_FUNCTIONS(){};
----------------------------------------------------------------------------*/

//  
// Function:   KODI_Init
// Purpose:    Init Kodi HTTP RCU
// 
// Parameters: Client 
// Returns:    Startup code
// Note:       
//  
int KODI_Init()
{
   int      iCc=0;
   pid_t    tPid;

   tPid = fork();
   //
   switch(tPid)
   {
      default:
         // parent 
         PRINTF("KODI_Init()" CRLF);
         GLOBAL_PidPut(PID_KODI, tPid);
         iCc = GLOBAL_KOD_INI;
         break;

      case 0:
         // child: let parent setup shop first
         sleep(2);
         kodi_Execute();
         //
         // Exit KODI HTTP RCU
         //
         _exit(0);
         break;

      case -1:
         // Error
         LOG_printf("kodi_Deamon(): Error!"CRLF);
         LOG_Report(errno, "KOD", "kodi_Deamon(): Error!");
         break;
   }
   return(iCc);
}


/*------  Local functions separator ------------------------------------
__LOCAL_FUNCTIONS(){};
----------------------------------------------------------------------------*/

//  
// Function:   kodi_Execute
// Purpose:    Run the Kodi RCU thread
// 
// Parameters:  
// Returns:    On exit
// Note:       
//  
static void kodi_Execute()
{
   sigset_t    tBlockset;
   NETCON     *pstNet=NULL;

   if(kodi_SignalRegister(&tBlockset) == FALSE)  
   {
      _exit(1);
   }
   //
   // Setup connection to KODI RCU (iPad or Android App).
   // If and when we run the Kodi media player, we need to terminate this
   // connection to let Kodi service the RCU requests. When Kodi terminates, 
   // setup the connection again.
   //
   LOG_Report(0, "KOD", "kodi_Execute():Init Start....");
   kodi_InitDynamicPages();
   //
   GLOBAL_PidSaveGuard(PID_KODI, GLOBAL_KOD_INI);
   GLOBAL_HostNotification(GLOBAL_KOD_INI);
   LOG_Report(0, "KOD", "kodi_Execute():Init Ready");
   //
   while(fPopKodiRunning)
   {
      switch(tKodiConnection)
      {
         case KCNX_INIT_CONNECT:
            pstNet = NET_Init(pstMap->G_pcWwwDir, pstMap->G_pcRamDir);
            if( NET_ServerConnect(pstNet, pstMap->G_pcKodiPort, HTTP_PROTOCOL) < 0)
            {
               PRINTF("kodi_Execute():RCU connect failed: exiting" CRLF);
               LOG_Report(errno, "KOD", "kodi_Execute():RCU connect failed: exiting.");
               fPopKodiRunning = FALSE;
            }
            else LOGNET1("kodi_Execute():KCNX_INIT_CONNECT" CRLF);
            tKodiConnection = KCNX_EXEC_CONNECT;
            break;

         case KCNX_EXEC_CONNECT:
            NET_BuildConnectionList(pstNet);
            //
            if(NET_WaitForConnection(pstNet, HTTP_CONNECTION_TIMEOUT_MSECS) > 0)
            {
               LOGNET1("kodi_Execute():Have connection" CRLF);
               //PRINTF("kodi_Execute:Have connection" CRLF);
               //
               // There is data on some of the sockets
               //
               NET_HandleSessions(pstNet, &kodi_CallbackConnection, pstMap->G_pcIpAddr, MAX_ADDR_LEN);
            }
            else
            {
               //
               // No connection so far: handle the rest
               //
               if( GLOBAL_GetSignalNotification(PID_KODI, GLOBAL_GRD_RUN) ) kodi_HandleGuard();
               //
               GLOBAL_PidSetGuard(PID_KODI);
               LOGNET1("kodi_Execute():KCNX_EXEC_CONNECT..." CRLF);
            }
            break;

         case KCNX_INIT_DISCONNECT:
            LOGNET1("kodi_Execute():KCNX_INIT_DISCONNECT" CRLF);
            pstNet = NET_ServerTerminate(pstNet);
            tKodiConnection = KCNX_EXEC_DISCONNECT;
            break;

         case KCNX_EXEC_DISCONNECT:
            //
            // When Kodi exits, restart the RCU App for our usage
            //
            LOGNET1("kodi_Execute():KCNX_EXEC_DISCONNECT" CRLF);
            sleep(5);
            break;

         default:
         case KCNX_EXIT:
            //
            // Close all socket connections
            //
            LOGNET1("kodi_Execute():KCNX_EXIT" CRLF);
            NET_ServerTerminate(pstNet);
            fPopKodiRunning = FALSE;
            break;
      }
   }
   _exit(0);
}

//  
// Function:   kodi_CallbackConnection
// Purpose:    Handle the incoming tcp message from the iPad Kodi RC App
// 
// Parameters: Client 
// Returns:    TRUE if OKee
// Note:       
//  
static bool kodi_CallbackConnection(NETCL *pstCl)
{
   KODI_SHOW_PAYLOAD(NET_DUMP_ASCII, &pstCl->pcUrl[pstCl->iPaylIdx], 0);
   KODI_LOG_PAYLOAD("kodi_CallbackConnection()", pstCl->iPort, &pstCl->pcUrl[pstCl->iPaylIdx]);
   LOGNET1("kodi_CallbackConnection()" CRLF);
   //
   kodi_HandleHttpRequest(pstCl);
   NET_FlushCache(pstCl);
   return(TRUE);
}

//  
//  Function    : kodi_HandleHttpRequest
//  Description : Data came in through the socket: handle it
//  
//  Parameters  : Socket descriptor
//  Returns     : 
//  
static int kodi_HandleHttpRequest(NETCL *pstCl)
{
   int   iCc=0, iIdx;
   FTYPE tType;   

   //PRINTF1("kodi_HandleHttpRequest():%s" CRLF, pstCl->pcUrl);
   //
   // Handle incoming HTTP POST request:
   //
   //   "POST /jsonrpc HTTP/1.1" CR,LF
   //   "Host: 10.0.0.231" CR,LF
   //   "......." CR,LF
   //   "Accept-Encoding: gzip,deflate" CR,LF
   //   "Content-Type: application/json" CR,LF
   //   "Content-Length: 115" CR,LF
   //   CR,LF
   //
   if( (iIdx = HTTP_PageIsDynamic(pstCl)) >= 0 )
   {
      tType = GEN_GetDynamicPageType(iIdx);
      //
      // This page is an internal (DYNAMICALLY build) page
      // Type is HTTP_HTML or HTTP_JSON
      // HTTP_HTML: ...cam.html?xxxx&-p17-p2....
      // HTTP_JSON: ...cam.jsom?command=xxxx&para1=yyyy&....
      //
      RPI_CollectParms(pstCl, tType);
      //
      LOGNET2("kodi_HandleHttpRequest(): Page index=%d" CRLF, iIdx);
      HTTP_DynamicPageHandler(pstCl, iIdx);
   }
   else
   {
      pstMap->G_iCurDynPage = -1;
      PRINTF1("kodi_HandleHttpRequest():STATIC (%s)" CRLF, pstCl->pcUrl);
      HTTP_RequestStaticPage(pstCl);
   }
   return(iCc);
}

// 
// Function:   kodi_InitDynamicPages
// Purpose:    Register dynaic webpages
// 
// Parameters: 
// Returns:    Nr registered
// Note:       
//
static int kodi_InitDynamicPages(void)
{
   int            iNr=0, iPort; 
   const NETDYN  *pstDyn=stDynamicKodiPages;

   iPort = atoi(pstMap->G_pcKodiPort);
   PRINTF1("kodi_InitDynamicPages(): port=%d" CRLF, iPort);
   //
   while(pstDyn->pcUrl)
   {
      PRINTF1("kodi_InitDynamicPages():Url=%s" CRLF, pstDyn->pcUrl);
      if(pstDyn->iFlags & DYN_FLAG_PORT)
      {
         //
         // Register for this specific port
         //
         GEN_RegisterDynamicPage(pstDyn->tUrl, -1, pstDyn->tType, pstDyn->iTimeout, iPort, pstDyn->pcUrl, (PFVOIDPI)pstDyn->pfDynCb);
      }
      else
      {
         //
         // Register for all ports
         //
         GEN_RegisterDynamicPage(pstDyn->tUrl, -1, pstDyn->tType, pstDyn->iTimeout,     0, pstDyn->pcUrl, (PFVOIDPI)pstDyn->pfDynCb);
      }
      iNr++;
      pstDyn++;
   }
   return(iNr);
}

// 
// Function:   kodi_HandleGuard
// Purpose:    Respond to the guard query
// 
// Parameters: 
// Returns:    
// Note:       
//
static void kodi_HandleGuard(void)
{
   LOG_Report(0, "KOD", "kodi_HandleGuard():Host-notification send");
   GLOBAL_HostNotification(GLOBAL_GRD_NFY);
}


/*------  Local functions separator ------------------------------------
__DYN_PAGE_FUNCTIONS_________(){};
----------------------------------------------------------------------------*/

// 
// Function:   kodi_DynPageKodiRpc
// Purpose:    Kodi RCU jsonrpc POST RPC
// 
// Parameters: Client, DynPage index
// Returns:    TRUE if OKee
// Note:       
//
static bool kodi_DynPageKodiRpc(NETCL *pstCl, int iIdx)
{
   bool     fCc=TRUE;
   char    *pcKodiRpc=NULL;
   char    *pcPayload;
   int      iId=0;

   PRINTF("kodi_DynPageKodiRpc():" CRLF);
   //
   switch(GEN_GetDynamicPageType(iIdx))
   {
      default:
         PRINTF("kodi_DynPageKodiRpc():OTHER" CRLF);
         break;

      case HTTP_HTML:
         //
         // Put out the HTML header, start tag and the rest
         //         
         HTTP_ExPageGeneric(pstCl, pcHttpResponseHeader);
         //
         HTTP_ExPageStart(pstCl,   pcWebPageTitle);
         HTTP_ExPageGeneric(pstCl, pcWebPageDefault);
         HTTP_ExPageGeneric(pstCl, pcWebPageEnd);
         PRINTF("kodi_DynPageKodiRpc():HTML" CRLF);
         break;

      case HTTP_JSON:
         //
         // The RPC request came with a session ID. Retrieve it, we need it for the reply
         //
         pcPayload = &pstCl->pcUrl[pstCl->iPaylIdx];
         //
         LOGRCU2("kodi_DynPageKodiRpc(): RPC()=%s" CRLF, pcPayload);
         if( GEN_JsonGetInteger(pcPayload, "id", &iId) )
         {
            PRINTF2("kodi_DynPageKodiRpc():ID=%d %s" CRLF, iId, pcPayload);
            //
            // Lookup JSONRPC from the list
            //
            if( GEN_JsonGetString(pcPayload, "method", &pcKodiRpc) )
            {
               fCc = kodi_LookupJsonRpc(pstCl, stKodiJsonRpc, pcKodiRpc);
            }
            else PRINTF2("kodi_DynPageKodiRpc():Unsupported RPC: ID=%d %s" CRLF, iId, pcPayload);
         }
         else
         {
            PRINTF2("kodi_DynPageKodiRpc():ERROR: ID=%d %s" CRLF, iId, pcPayload);
         }
         break;
   }
   return(fCc);
}

// 
// Function:   kodi_DynPageXbmc
// Purpose:    Dynamically created system page to exit the server
// 
// Parameters: Client, DynPage index
// Returns:    TRUE if OKee
// Note:       
//
static bool kodi_DynPageXbmc(NETCL *pstCl, int iIdx)
{
   KODI_KEY("Key=%2d %s" CRLF, 99, pstMap->G_pcCommand);
   KODI_KEY("Key=%2d %s" CRLF, 99, pstMap->G_pcXbmcCmd);
   //
   // Put out the HTML header, start tag and the rest
   //         
   HTTP_ExPageGeneric(pstCl, pcHttpResponseHeader);
   //
   HTTP_ExPageStart(pstCl,   pcWebPageTitle);
   HTTP_ExPageGeneric(pstCl, pcWebPageDefault);
   HTTP_ExPageGeneric(pstCl, pcWebPageEnd);
   //
   PRINTF("kodi_DynPageXbmc():OKee" CRLF);
   return(TRUE);
}

/*------  Local functions separator ------------------------------------
__KODI_FUNCTIONS_________(){};
----------------------------------------------------------------------------*/

// 
//  Function    : kodi_LookupJsonRpc
//  Description : Lookup the JSONRPC command in the list
// 
//  Parameters  : Client, JSONRPC list, command
//  Returns     : True if found and handled
// 
static bool kodi_LookupJsonRpc(NETCL *pstCl, const RCURPC *pstRpc, char *pcKodiRpc)
{
   bool  fCc=FALSE;
   int   iLen, iId;
   char *pcPayload=&pstCl->pcUrl[pstCl->iPaylIdx];

   if( GEN_JsonGetInteger(pcPayload, "id", &iId) )
   {
      do
      {
         iLen = GEN_STRLEN(pstRpc->pcRpc);
         if(GEN_STRNCMP(pstRpc->pcRpc, pcKodiRpc, iLen) == 0)
         {
            //
            // Kodi RPC found: call handler
            //
            fCc = pstRpc->pfFunc(pstCl, iId, pstRpc->ubKey);
            break;
         }
         pstRpc++;
      }
      while(pstRpc->pcRpc);
   }
   return(fCc);
}

// 
// Function:   kodi_KodiRpcExecuteAction
// Purpose:    Kodi RCU replay to jsonrpc POST
// 
// Parameters: Client, session ID, Key
// Returns:    TRUE if OKee
// Note:       
//
static bool kodi_KodiRpcExecuteAction(NETCL *pstCl, int iId, u_int8 ubKey)
{
   bool  fCc=FALSE;
   char *pcKodiRpc;
   char *pcPayload=&pstCl->pcUrl[pstCl->iPaylIdx];

   LOGRCU3("kodi_KodiRpcExecuteAction(%d):%s" CRLF, ubKey, pcPayload);
   //
   // Lookup JSONRPC from the list
   //
   if( GEN_JsonGetString(pcPayload, "action", &pcKodiRpc) )
   {
      fCc = kodi_LookupJsonRpc(pstCl, stKodiJsonRpcAction, pcKodiRpc);
   }
   else PRINTF2("kodi_KodiRpcExecuteAction():Unsupported RPC: ID=%d %s" CRLF, iId, pcPayload);
   return(fCc);
}

// 
// Function:   kodi_KodiRpcGetAction
// Purpose:    Kodi RCU replay to jsonrpc POST
// 
// Parameters: Client, session ID, Key
// Returns:    TRUE if OKee
// Note:       
//
static bool kodi_KodiRpcGetAction(NETCL *pstCl, int iId, u_int8 ubKey)
{
   bool  fCc;

   KODI_KEY("Key=%2d %s" CRLF, ubKey, &pstCl->pcUrl[pstCl->iPaylIdx]);
   LOGRCU3("kodi_KodiRpcGetAction(%d):%s" CRLF, ubKey, &pstCl->pcUrl[pstCl->iPaylIdx]);
   //
   // Report all valid keys
   //
   if(ubKey != RCU_NOKEY)
   {
      kodi_SendSignalToHandler((int)ubKey);
   }
   fCc = RPI_BuildJsonMessageKodiRpcGetInput(pstCl, iId);
   return(fCc);
}

// 
// Function:   kodi_KodiRpcGetInputKeys
// Purpose:    Kodi RCU replay to jsonrpc POST
// 
// Parameters: Client, session ID, Key
// Returns:    TRUE if OKee
// Note:       
//
static bool kodi_KodiRpcGetInputKeys(NETCL *pstCl, int iId, u_int8 ubKey)
{
   bool  fCc;

   KODI_KEY("Key=%2d %s" CRLF, ubKey, &pstCl->pcUrl[pstCl->iPaylIdx]);
   LOGRCU3("kodi_KodiRpcGetInputKeys(%d):%s" CRLF, ubKey, &pstCl->pcUrl[pstCl->iPaylIdx]);
   //
   // Report all valid keys
   //
   if(ubKey != RCU_NOKEY)
   {
      kodi_SendSignalToHandler((int)ubKey);
   }
   //
   fCc = RPI_BuildJsonMessageKodiRpcGetInput(pstCl, iId);
   return(fCc);
}

// 
// Function:   kodi_KodiRpcGetInputSendText
// Purpose:    Kodi RCU reply to jsonrpc POST
// 
// Parameters: Client, session ID, key
// Returns:    TRUE if OKee
// Note:       
//
static bool kodi_KodiRpcGetInputSendText(NETCL *pstCl, int iId, u_int8 ubKey)
{
   bool  fCc, fDone=FALSE;
   char *pcPayload=&pstCl->pcUrl[pstCl->iPaylIdx];
   char *pcText;

   LOGRCU3("kodi_KodiRpcGetInputSendText(%d):%s" CRLF, ubKey, pcPayload);

   //
   // Lookup JSONRPC from the list
   //
   if( GEN_JsonGetBoolean(pcPayload, "done", &fDone) )
   {
      if(fDone)
      {
         //
         // Correct text entry sequence : {"text":"aapnootmies", "done":true}
         //
         if( GEN_JsonGetString(pcPayload, "text", &pcText) )
         {
            LOGRCU2("kodi_KodiRpcGetInputSendText(RCU_MENU):%s" CRLF, pcText);
            ubKey = RCU_MENU;
         }
         else LOGRCU2("kodi_KodiRpcGetInputSendText():text not found: %s" CRLF, pcPayload);
      }
      else LOGRCU2("kodi_KodiRpcGetInputSendText():New char: %s" CRLF, pcPayload);
   }
   else LOGRCU2("kodi_KodiRpcGetInputSendText():No bool: %s" CRLF, pcPayload);
   //
   // Report all valid keys
   //
   if(ubKey != RCU_NOKEY)
   {
      kodi_SendSignalToHandler((int)ubKey);
   }
   //
   fCc = RPI_BuildJsonMessageKodiRpcGetInput(pstCl, iId);
   return(fCc);
}

// 
// Function:   kodi_KodiRpcGetPing
// Purpose:    Kodi RCU replay to jsonrpc POST
// 
// Parameters: Client, session ID, Key
// Returns:    TRUE if OKee
// Note:       
//
static bool kodi_KodiRpcGetPing(NETCL *pstCl, int iId, u_int8 ubKey)
{
   bool  fCc;

   LOGRCU3("kodi_KodiRpcGetPing(%d):%s" CRLF, ubKey, &pstCl->pcUrl[pstCl->iPaylIdx]);
   fCc = RPI_BuildJsonMessageKodiRpcGetPing(pstCl, iId);
   return(fCc);
}

// 
// Function:   kodi_KodiRpcGetProperties
// Purpose:    Kodi RCU replay to jsonrpc POST
// 
// Parameters: Client, session ID, Key
// Returns:    TRUE if OKee
// Note:       
//
static bool kodi_KodiRpcGetProperties(NETCL *pstCl, int iId, u_int8 ubKey)
{
   bool  fCc;

   LOGRCU3("kodi_KodiRpcGetProperties(%d):%s" CRLF, ubKey, &pstCl->pcUrl[pstCl->iPaylIdx]);
   fCc = RPI_BuildJsonMessageKodiRpcGetProps(pstCl, iId);
   return(fCc);
}

// 
// Function:   kodi_KodiRpcGetPlayers
// Purpose:    Kodi RCU RPC to jsonrpc POST
// 
// Parameters: Client, session ID, Key
// Returns:    TRUE if OKee
// Note:       
//
static bool kodi_KodiRpcGetPlayers(NETCL *pstCl, int iId, u_int8 ubKey)
{
   bool  fCc;

   LOGRCU3("kodi_KodiRpcGetPlayers(%d):%s" CRLF, ubKey, &pstCl->pcUrl[pstCl->iPaylIdx]);
   //
   // Report all valid keys
   //
   if(ubKey != RCU_NOKEY)
   {
      kodi_SendSignalToHandler((int)ubKey);
   }
   fCc = RPI_BuildJsonMessageKodiRpcGetPlayers(pstCl, iId);
   return(fCc);
}

// 
// Function:   kodi_KodiRpcGetPlaylistItems
// Purpose:    Kodi RCU RPC to jsonrpc POST (Playlist.GetItems)
// 
// Parameters: Client, session ID, Key
// Returns:    TRUE if OKee
// Note:       
//
static bool kodi_KodiRpcGetPlaylistItems(NETCL *pstCl, int iId, u_int8 ubKey)
{
   bool  fCc;

   KODI_KEY("Key=%2d %s" CRLF, ubKey, &pstCl->pcUrl[pstCl->iPaylIdx]);
   LOGRCU3("kodi_KodiRpcGetPlaylistItems(%d):%s" CRLF, ubKey, &pstCl->pcUrl[pstCl->iPaylIdx]);
   //
   // Report all valid keys
   //
   if(ubKey != RCU_NOKEY)
   {
      kodi_SendSignalToHandler((int)ubKey);
   }
   fCc = RPI_BuildJsonMessageKodiRpcGetItems(pstCl, iId);
   return(fCc);
}

/*------  Local functions separator ------------------------------------
__SIGNAL_FUNCTIONS(){};
----------------------------------------------------------------------------*/

// 
//  Function    : kodi_SendSignalToHandler
//  Description : Signal the RCU key handler we have a RCU key
// 
//  Parameters  : Data
//  Returns     : 
// 
//  Note        : Uses MMAP shared memory
// 
static void kodi_SendSignalToHandler(int iData)
{
   if(GLOBAL_KeyPut(iData))
   {
      LOG_Report(0, "KOD", "kodi-SendSignalToHandler():Key=%d", iData);
      //
      // Notify the handler we have a RCU key
      //
      PRINTF2("kodi_SendSignalToHandler(): Host(%d), Data=%d" CRLF, GLOBAL_PidGet(PID_HOST), iData);
      GLOBAL_Notify(PID_HOST, GLOBAL_RCU_NFY, SIGUSR1);
   }
   else
   {
      LOG_Report(0, "KOD", "kodi-SendSignalToHandler():Key=%d:BUFFER IS FULL!!", iData);
   }
}

// 
// Function:   kodi_SignalRegister
// Purpose:    Register all SIGxxx
// 
// Parameters: Blockset
// Returns:    TRUE if delivered
// 
static bool kodi_SignalRegister(sigset_t *ptBlockset)
{
  bool fCC = TRUE;

  // SIGSEGV:
  if( signal(SIGSEGV, &kodi_ReceiveSignalSegmnt) == SIG_ERR)
  {
     LOG_Report(errno, "KOD", "kodi_SignalRegister(): SIGSEGV ERROR");
     fCC = FALSE;
  }
  // SIGUSR1:
  if( signal(SIGUSR1, &kodi_ReceiveSignalUser1) == SIG_ERR)
  {
     LOG_Report(errno, "KOD", "kodi_SignalRegister(): SIGUSR1 ERROR");
     fCC = FALSE;
  }
  // SIGUSR2:
  if( signal(SIGUSR2, &kodi_ReceiveSignalUser2) == SIG_ERR)
  {
     LOG_Report(errno, "KOD", "kodi_SignalRegister(): SIGUSR2 ERROR");
     fCC = FALSE;
  }
  // CTL-X handler
  if( signal(SIGTERM, &kodi_ReceiveSignalTerm) == SIG_ERR)
  {
     LOG_Report(errno, "KOD", "kodi_SignalRegister(): SIGTERM ERROR");
     fCC = FALSE;
  }
  // Ctl-C handler
  if( signal(SIGINT, &kodi_ReceiveSignalInt) == SIG_ERR)
  {
     LOG_Report(errno, "KOD", "kodi_SignalRegister(): SIGINT ERROR");
     fCC = FALSE;
  }

  if(fCC)
  {
     //
     // Setup the SIGxxx events
     //
     sigemptyset(ptBlockset);
     sigaddset(ptBlockset, SIGSEGV);
     sigaddset(ptBlockset, SIGTERM);
     sigaddset(ptBlockset, SIGINT);
     sigaddset(ptBlockset, SIGUSR1);
     sigaddset(ptBlockset, SIGUSR2);
  }
  return(fCC);
}

//
// Function:   kodi_ReceiveSignalSegmnt
// Purpose:    Handle the SIGSEGV
//
// Parms:
// Returns:    TRUE if delivered
// Note:       Do NOT use LOG_xxx() here
//
static void kodi_ReceiveSignalSegmnt(int iSignal)
{
   if(fPopKodiRunning)
   {
      fPopKodiRunning = FALSE;
      //
      GEN_SegmentationFault(__FILE__, __LINE__);
      LOG_SegmentationFault(__FILE__, __LINE__);
      //
      // (TRY TO) Cleanup background threads:
      //
      GLOBAL_Notify(PID_HOST, GLOBAL_FLT_NFY, SIGTERM);
   }
}

// 
// Function:   kodi_ReceiveSignalTerm
// Purpose:    Add the SIGTERM command in the buffer (Ctl-X)
// 
// Parameters: 
// Returns:    TRUE if delivered
// 
static void kodi_ReceiveSignalTerm(int iSignal)
{
   LOG_Report(0, "KOD", "kodi_ReceiveSignalTerm()");
   tKodiConnection = KCNX_EXIT;
}

//
// Function:   kodi_ReceiveSignalInt
// Purpose:    System callback for SIGINT (Ctl-C)
//
// Parms:
// Returns:    TRUE if delivered
//
static void kodi_ReceiveSignalInt(int iSignal)
{
   LOG_Report(0, "KOD", "kodi_ReceiveSignalInt()");
   tKodiConnection = KCNX_EXIT;
}

//
// Function:   kodi_ReceiveSignalUser1
// Purpose:    System callback for SIGUSR1 signal
//
// Parms:
// Returns:    
// Note:       Various threads can send us a SIGUSR1. Check the GLOBAL_GetSignalNotification
//             to check from wich source the SIGUSR1 came. 
//
//             (tKodiConnection = KCNX_INIT_CONNECT)
//
static void kodi_ReceiveSignalUser1(int iSignal)
{
}

//
// Function:   kodi_ReceiveSignalUser2
// Purpose:    System callback for SIGUSR2 signal
//
// Parms:
// Returns:    
// Note:       Various threads can send us a SIGUSR2. Check the GLOBAL_GetSignalNotification
//             to check from wich source the SIGUSR2 came. 
//
//             (tKodiConnection = KCNX_INIT_DISCONNECT)
//
static void kodi_ReceiveSignalUser2(int iSignal)
{
}

#ifdef  FEATURE_KODI_SHOW_PAYLOAD
//
//  Function:  kodi_ShowPayload
//  Purpose:   Show the kodi POST http request payload
//             
//  Parms:     mode, Buffer, len
//  Returns:   
//
//
static void kodi_ShowPayload(int iMode, char *pcBuffer, int iLen)
{
   int   iRow=0, iCol=0, iIdx;
   char  pcChar[2];
   char  pcAscii[12];

   if(iMode & NET_DUMP_ASCII)
   {
      if(iLen == 0)
      {
         GEN_Printf("%s" CRLF, pcBuffer);
      }
      else
      {
         iIdx = 0;
         while(iIdx<iLen)
         {
            GEN_Printf("%c", pcBuffer[iIdx++]);
         }
         GEN_Printf(CRLF);
      }
   }
   //
   if(iMode & NET_DUMP_HEX)
   {
      iIdx = 0;
      pcChar[1] = 0;
      //
      GEN_Printf("=======================================================" CRLF);
      while(iIdx < iLen)
      {
         if(iCol == 0) 
         {
            GEN_Printf("   %03d = ", iRow);
         }
         //
         pcChar[0] = pcBuffer[iIdx++];
         GEN_Printf("%02X ", pcChar[0]);
         //
         if( (pcChar[0]==0x0d) || (pcChar[0]==0x0a)) pcAscii[iCol] = '.';
         else                                        pcAscii[iCol] = pcChar[0];
         iCol++;
         //
         if(iCol == 10) 
         {
            pcAscii[iCol] = 0;
            GEN_Printf("   %s\n", pcAscii);
            iCol = 0;
         }
         iRow++;
      }
      //
      pcAscii[iCol] = 0;
      GEN_Printf("   %s\n", pcAscii);
      GEN_Printf("=======================================================" CRLF);
   }
}
#endif   //FEATURE_KODI_SHOW_PAYLOAD

#ifdef FEATURE_KODI_LOG_PAYLOAD
/*
 * Function    : kodi_LogPayload
 * Description : Log the network json payload
 *
 * Parameters  : Title, port, data
 * Returns     : 
 *
 */
static void kodi_LogPayload(const char *pcTitle, int iPort, char *pcData)
{
   LOG_printf("%s(p=%d):Payload=%s" CRLF, pcTitle, iPort, pcData);
}
#endif   //FEATURE_KODI_LOG_PAYLOAD

