/*  (c) Copyright:  2018  Patrn, Confidential Data
 *
 *  Workfile:           cmd_defs.h
 *  Revision:
 *  Modtime:
 *
 *  Purpose:            Extract headerfile for the RPI commands
 *
 *
 *
 *  Compiler/Assembler: Raspbian Linux GNU gcc
 *  Ext Packages:
 *
 *  Author:             Peter Hillen
 *  Date Created:       04 Jul 2018
 *
 *  Revisions:
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 **/

//          iCommand                pcCommand      pcHelp

EXTRACT_CMD(CAM_CMD_IDLE,           "Idle",        "Idle"                                                )
EXTRACT_CMD(CAM_CMD_BUTTONS,        "buttons",     "Request button data"                                 )
EXTRACT_CMD(CAM_CMD_DEFAULTS,       "defaults",    "Restore defaults"                                    )
EXTRACT_CMD(CAM_CMD_FOTOS,          "fotos",       "Request foto  list"                                  )
EXTRACT_CMD(CAM_CMD_PARMS,          "parms",       "Request CAM parameters"                              )
EXTRACT_CMD(CAM_CMD_STREAM,         "stream",      "Start streaming to socket"                           )
EXTRACT_CMD(CAM_CMD_TIMELAPSE,      "timelapse",   "Take timelapse snapshots"                            )
EXTRACT_CMD(CAM_CMD_VIDEOS,         "videos",      "Request video list"                                  )
EXTRACT_CMD(CAM_CMD_REMOTEIO,       "io",          "Remote IO"                                           )
//
EXTRACT_CMD(CAM_CMD_ALM,            "alarm",       "Add alarm"                                           )
EXTRACT_CMD(CAM_CMD_ALM_RECORD,     "arecord",     "Start alarm recording"                               )
EXTRACT_CMD(CAM_CMD_ALM_MOTION,     "amotion",     "Start alarm vector motion detect"                    )
EXTRACT_CMD(CAM_CMD_ALM_SNAP,       "asnap",       "Take alarm snapshot"                                 )
EXTRACT_CMD(CAM_CMD_ALM_WAN,        "awan",        "Verify WAN ip address changes"                       )
EXTRACT_CMD(CAM_CMD_ALM_MDET_MAIL,  "amdmail",     "Send Motion Detect summary mail"                     )
EXTRACT_CMD(CAM_CMD_ALM_STOP,       "astop",       "Stop ongoing recording or streaming"                 )
//
EXTRACT_CMD(CAM_CMD_DEL_FOTOS,      "rm-fotos",    "Delete foto's"                                       )
EXTRACT_CMD(CAM_CMD_DEL_VIDEOS,     "rm-videos",   "Delete video's"                                      )
//
EXTRACT_CMD(CAM_CMD_MAN_RECORD,     "record",      "Start manual recording to file"                      )
EXTRACT_CMD(CAM_CMD_MAN_SNAP,       "snapshot",    "Take manual snapshot"                                )
EXTRACT_CMD(CAM_CMD_MAN_STOP,       "stop",        "Stop ongoing recording or streaming"                 )
//
EXTRACT_CMD(CAM_CMD_M_DELTA,        "mdelta",      "Motion: Determine delta between 2 foto's"            )
EXTRACT_CMD(CAM_CMD_M_DETECT,       "mdetect",     "Motion: start detection"                             )
EXTRACT_CMD(CAM_CMD_M_NORM,         "mnorm",       "Motion: Normalize foto"                              )
EXTRACT_CMD(CAM_CMD_M_PARMS,        "mparms",      "Motion: Request parameters"                          )
EXTRACT_CMD(CAM_CMD_M_SNAP,         "msnap",       "Take motion reference snapshot"                      )
EXTRACT_CMD(CAM_CMD_M_VECTOR,       "mvector",     "Motion: Determine vector movements"                  )
EXTRACT_CMD(CAM_CMD_A_VECTOR,       "avector",     "Motion: Determine vector movements (AUTO Mode)"      )
EXTRACT_CMD(CAM_CMD_M_ZOOM,         "mzoom",       "Motion: Zoom foto"                                   )
EXTRACT_CMD(CAM_CMD_M_TRIGGER,      "mtrigger",    "Motion: Trigger remote IO"                           )
