/*  (c) Copyright:  2017..2018 Patrn, Confidential Data 
 *
 *  Workfile:           rpi_srvr.c
 *  Revision:   
 *  Modtime:    
 *
 *  Purpose:            Dynamic HTTP server
 *
 *
 *
 *  Compiler/Assembler: Platform independent
 *  Ext Packages:
 * 
 *  Author:             Peter Hillen
 *  Date Created:       19 Oct 2017
 *                                   
 *  Revisions:
 *  Entry Points:       SRVR_Init()
 *
 *                      
 *
 *                      
 *                      
 *                      
 *                      
 *
 *                      
 *                      
 *                      
 *                      
 *
 *                      
 *                      
 *                      
 *                      
 *
 *
 *
 *
**/


#include <stdio.h>
#include <string.h>
#include <netinet/in.h>
#include <stdlib.h>
#include <unistd.h>
#include <time.h>
#include <resolv.h>
#include <sys/types.h>
#include <sys/signal.h>
#include <sys/socket.h>
#include <sys/wait.h>
#include <errno.h>

#include <common.h>
#include "config.h"
#include "globals.h"
#include "gen_func.h"
#include "gen_http.h"
#include "rpi_page.h"
#include "rpi_func.h"
#include "rpi_proxy.h"
#include "rpi_srvr.h"

//#define USE_PRINTF
#include <printf.h>

//
// Reference the Host dynamic HTTP pages
//
extern const NETDYN  stHostDynamicPages[];

#ifdef  FEATURE_JSON_SHOW_PAYLOAD
   static void srvr_ShowNetPayload        (const char *, int, char *);
   #define SHOW_NET_PAYLOAD(a,b,c)        srvr_ShowNetPayload(a,b,c)
#else
   #define SHOW_NET_PAYLOAD(a,b,c)
#endif  //FEATURE_JSON_SHOW_PAYLOAD

//
// Local prototypes
//
static void    srvr_Daemon                (void);
static bool    srvr_SignalRegister        (sigset_t *);
static void    srvr_ReceiveSignalInt      (int);
static void    srvr_ReceiveSignalTerm     (int);
static void    srvr_ReceiveSignalUser1    (int);
static void    srvr_ReceiveSignalUser2    (int);
static void    srvr_ReceiveSignalSegmnt   (int);
//
static void    srvr_HandleCompletion      (NETCL *);
static bool    srvr_CallbackConnection    (NETCL *);
//
static void    srvr_NotifyFromButton      (NETCL *);
static void    srvr_NotifyFromCamera      (NETCL *);
static void    srvr_NotifyFromProxy       (NETCL *);
static void    srvr_NotifySpurious        (int);
static void    srvr_HandleGuard           (void);
//
static int     srvr_HandleHttpProxy       (NETCL *);
static int     srvr_HandleHttpReply       (NETCL *);
static int     srvr_HandleHttpReplyHtml   (NETCL *);
static int     srvr_HandleHttpReplyJson   (NETCL *);
static int     srvr_HandleHttpRequest     (NETCL *);
static int     srvr_InitDynamicPages      (int);
static int     srvr_WaitSemaphore         (int);
static void    srvr_PostSemaphore         (void);
//
static GLOCMD *pstCmd          = NULL;
static bool    fSrvrRunning    = TRUE;

/* ======   Local Functions separator ===========================================
___MAIN_FUNCTIONS(){}
==============================================================================*/

//
//  Function:   SRVR_Init
//  Purpose:    HTTP server init
//
//  Parms:      
//  Returns:    Strtup code
//
int SRVR_Init(void)
{
   int      iCc=0;
   pid_t    tPid;

   tPid = fork();
   //
   switch(tPid)
   {
      default:
         // parent 
         PRINTF("SRVR_Init()" CRLF);
         GLOBAL_PidPut(PID_SRVR, tPid);
         iCc = GLOBAL_SVR_INI;
         break;

      case 0:
         // Child: RPI HTTP-server
         GEN_Sleep(2000);
         srvr_Daemon();
         LOG_Report(0, "SVR", "SRVR_Init():Exit normally");
         _exit(0);
         break;

      case -1:
         // Error
         GEN_Printf("SRVR_Init(): Error!"CRLF);
         LOG_Report(errno, "SVR", "SRVR_Init():Exit fork ERROR:");
         _exit(1);
         break;
   }
   return(iCc);
}

/* ======   Local Functions separator ===========================================
___LOCAL_FUNCTIONS(){}
==============================================================================*/

//
//  Function:   srvr_Daemon
//  Purpose:    Run the Raspberry Pi HTTP mini-server as a deamon
//
//  Parms:
//  Returns:
//
static void srvr_Daemon(void)
{
   bool        fRpiIpOkee=FALSE;
   int         iPort, iCc;
   sigset_t    tBlockset;
   NETCON     *pstNet=NULL;

   if(sem_init(&pstMap->G_tSemSvr, 1, 0) == -1) 
   {
      LOG_Report(errno, "SVR", "srvr_Daemon():Exit semaphore ERROR:");
      _exit(1);
   }
   if(srvr_SignalRegister(&tBlockset) == FALSE) 
   {
      LOG_Report(errno, "SVR", "srvr_Daemon():Exit fork ERROR:");
      _exit(1);
   }
   //
   // Set up server socket 
   //
   LOG_Report(0, "SVR", "srvr_Daemon():Init Start....");
   pstNet = NET_Init(pstMap->G_pcWwwDir, pstMap->G_pcRamDir);
   if( NET_ServerConnect(pstNet, pstMap->G_pcSrvrPort, HTTP_PROTOCOL) == -1)
   {
      LOG_printf("srvr_Daemon():Server connect failed: exiting" CRLF);
      LOG_Report(errno, "SVR", "srvr_Daemon():Exit NET connect ERROR:");
      _exit(1);
   }
   //
   pstCmd = GLOBAL_CommandCopy();
   iPort  = atoi(pstMap->G_pcSrvrPort);
   //
   srvr_InitDynamicPages(iPort);
   //
   GLOBAL_GetHostName();
   GLOBAL_PidSaveGuard(PID_SRVR, GLOBAL_SVR_INI);
   GLOBAL_HostNotification(GLOBAL_SVR_INI);
   LOG_Report(0, "SVR", "srvr_Daemon():Init Ready");
   //
   // Wait for data
   //
   while(fSrvrRunning)
   {

      NET_BuildConnectionList(pstNet);
      //
      // Retrieve the IP address for eth0
      //
      if(fRpiIpOkee == FALSE) fRpiIpOkee = NET_GetIpInfo(pstMap->G_pcMyIpAddr, MAX_ADDR_LEN, pstMap->G_pcMyIpIfce, MAX_PARM_LEN);
      if(NET_WaitForConnection(pstNet, HTTP_CONNECTION_TIMEOUT_MSECS) > 0)
      {
         //
         // There is data on some of the sockets
         //
         //PRINTF("Server has data" CRLF);
         iCc = NET_HandleSessions(pstNet, &srvr_CallbackConnection, pstMap->G_pcIpAddr, MAX_ADDR_LEN);
         if(iCc < 0)
         {
            //
            // Unrecoverable (for now) NET_Write error: restart theApp
            //
            LOG_Report(0, "RPI", "srvr_Daemon():NET_HandleSessions error");
            GLOBAL_Notify(PID_HOST, GLOBAL_RPI_RST, SIGTERM);
         }
      }
      else
      {
         //
         // Timeout: Check
         //    - Guard query:    The guard is prompting us to respond because we failed to do it until now.
         //    - Network query:  Check if we have lingering network connections.
         //    - Guard duty:     Reset the sentry marker if it was triggered by the guard.
         //
         if(GLOBAL_GetSignalNotification(PID_SRVR, GLOBAL_GRD_RUN) ) srvr_HandleGuard();
         if(NET_ReportServerStatus(pstNet, NET_CHECK_COUNT) == 0)
         {
            //
            // No connections available: reset all expired ones !
            //
            NET_ReportServerStatus(pstNet, NET_CHECK_LOG|NET_CHECK_FREE);
         }
         GLOBAL_PidSetGuard(PID_SRVR);
      }
   }
   //
   // Close all socket connections
   //
   NET_ServerTerminate(pstNet);
   GLOBAL_CommandDestroy(pstCmd);
   if( sem_destroy(&pstMap->G_tSemSvr) == -1) LOG_Report(errno, "SVR", "srvr_Daemon():sem_destroy");
}

//
//  Function:   srvr_SignalRegister
//  Purpose:    Register all SIGxxx
//
//  Parms:      Blockset
//  Returns:    TRUE if delivered
//
static bool srvr_SignalRegister(sigset_t *ptBlockset)
{
  bool fCC = TRUE;

  // SIGUSR1:
  if( signal(SIGUSR1, &srvr_ReceiveSignalUser1) == SIG_ERR)
  {
     LOG_Report(errno, "SVR", "srvr_SignalRegister(): SIGUSR1 ERROR");
     fCC = FALSE;
  }
  // SIGUSR2:
  if( signal(SIGUSR2, &srvr_ReceiveSignalUser2) == SIG_ERR)
  {
     LOG_Report(errno, "SVR", "srvr_SignalRegister(): SIGUSR2 ERROR");
     fCC = FALSE;
  }
  // CTL-X handler
  if( signal(SIGTERM, &srvr_ReceiveSignalTerm) == SIG_ERR)
  {
     LOG_Report(errno, "SVR", "srvr_SignalRegister(): SIGTERM ERROR");
     fCC = FALSE;
  }
  // Ctl-C handler
  if( signal(SIGINT, &srvr_ReceiveSignalInt) == SIG_ERR)
  {
     LOG_Report(errno, "SVR", "srvr_SignalRegister(): SIGINT ERROR");
     fCC = FALSE;
  }
  // Segmentation fault
  if( signal(SIGSEGV, &srvr_ReceiveSignalSegmnt) == SIG_ERR)
  {
     LOG_Report(errno, "SVR", "srvr_ReceiveSignalSegmnt(): SIGSEGV ERROR");
     fCC = FALSE;
  }

  if(fCC)
  {
     //
     // Setup the SIGxxx events
     //
     sigemptyset(ptBlockset);
     sigaddset(ptBlockset, SIGTERM);
     sigaddset(ptBlockset, SIGINT);
     sigaddset(ptBlockset, SIGUSR1);
     sigaddset(ptBlockset, SIGUSR2);
     sigaddset(ptBlockset, SIGSEGV);
  }
  return(fCC);
}

//
//  Function:   srvr_ReceiveSignalTerm
//  Purpose:    Add the SIGTERM command in the buffer (Ctl-X)
//
//  Parms:
//  Returns:    TRUE if delivered
//
static void srvr_ReceiveSignalTerm(int iSignal)
{
   LOG_Report(0, "SVR", "srvr_ReceiveSignalTerm()");
   fSrvrRunning = FALSE;
   srvr_PostSemaphore();
}

//
//  Function:   srvr_ReceiveSignalInt
//  Purpose:    Add the SIGINT command in the buffer (Ctl-C)
//
//  Parms:
//  Returns:    TRUE if delivered
//
static void srvr_ReceiveSignalInt(int iSignal)
{
   LOG_Report(0, "SVR", "srvr_ReceiveSignalInt()");
   fSrvrRunning = FALSE;
   srvr_PostSemaphore();
}

//
// Function:   srvr_ReceiveSignalSegmnt
// Purpose:    Handle the SIGSEGV
//
// Parms:
// Returns:    TRUE if delivered
// Note:       Do NOT use LOG_xxx() here
//
static void srvr_ReceiveSignalSegmnt(int iSignal)
{
   if(fSrvrRunning)
   {
      fSrvrRunning = FALSE;
      //
      GEN_SegmentationFault(__FILE__, __LINE__);
      LOG_SegmentationFault(__FILE__, __LINE__);
      //
      // (TRY TO) Cleanup background threads:
      //
      GLOBAL_Notify(PID_HOST, GLOBAL_FLT_NFY, SIGTERM);
   }
   srvr_PostSemaphore();
}

//
// Function:   srvr_ReceiveSignalUser1
// Purpose:    SIGUSR1 signal
//
// Parms:
// Returns:    
// Note:       SIGUSR1 is used to 
//
static void srvr_ReceiveSignalUser1(int iSignal)
{
   srvr_PostSemaphore();
}

//
// Function:   srvr_ReceiveSignalUser2
// Purpose:    System callback on receiving the SIGUSR2 signal
//
// Parms:
// Returns:    
// Note:       SIGUSR2 is not used
//
static void srvr_ReceiveSignalUser2(int iSignal)
{
}

/* ======   Local Functions separator ===========================================
___MISC_FUNCTIONS(){}
==============================================================================*/

// 
// Function:   srvr_InitDynamicPages
// Purpose:    Register dynamic webpages
// 
// Parameters: Port
// Returns:    Nr registered
// Note:       
//
static int srvr_InitDynamicPages(int iPort)
{
   int            iNr=0; 
   const NETDYN  *pstDyn=stHostDynamicPages;

   PRINTF1("srvr_InitDynamicPages(): port=%d" CRLF, iPort);
   //
   while(pstDyn->pcUrl)
   {
      PRINTF1("srvr_InitDynamicPages():Url=%s" CRLF, pstDyn->pcUrl);
      if(pstDyn->iFlags & DYN_FLAG_PORT)
      {
         //
         // Register for this specific port
         //
         GEN_RegisterDynamicPage(pstDyn->tUrl, -1, pstDyn->tType, pstDyn->iTimeout, iPort, pstDyn->pcUrl, (PFVOIDPI)pstDyn->pfDynCb);
      }
      else
      {
         //
         // Register for all ports
         //
         GEN_RegisterDynamicPage(pstDyn->tUrl, -1, pstDyn->tType, pstDyn->iTimeout,     0, pstDyn->pcUrl, (PFVOIDPI)pstDyn->pfDynCb);
      }
      iNr++;
      pstDyn++;
   }
   return(iNr);
}

//
// Function:   srvr_PostSemaphore
// Purpose:    Unlock the Server Semaphore
//
// Parms:      
// Returns:    
// Note:       sem_post() increments (unlocks) the semaphore pointed to by sem. If the semaphore's 
//             value consequently becomes greater than zero, then another process or thread blocked
//             in a sem_wait(3) call will be woken up and proceed to lock the semaphore. 
//
static void srvr_PostSemaphore(void)
{
   //
   // sem++ (UNLOCK)   
   //
   if(sem_post(&pstMap->G_tSemSvr) == -1) LOG_Report(errno, "SVR", "srvr_PostSemaphore(): sem_post error");
}

//
// Function:   srvr_WaitSemaphore
// Purpose:    Wait till Semaphore has been unlocked, then LOCK and proceed
//
// Parms:      TIMEOUT (or -1 if wait indefinitely)
// Returns:     0 if sem unlocked
//             +1 if sem timeout
//             -1 on error (errno)
// Note:       sem_wait() tries to decrement (lock) the semaphore. 
//             If the semaphore currently has the value zero, then the call blocks until 
//             either it becomes possible to perform the decrement (the semaphore value rises 
//             above zero) or a signal handler interrupts the call. 
//             If the semaphore's value is greater than zero, then the decrement proceeds, 
//             and the function returns immediately. 
//
//             sem_timedwait() is the same as sem_wait(), except that TIMEOUT specifies a 
//             limit on the amount of time that the call should block if the decrement cannot 
//             be immediately performed. The stTime points to a structure that
//             specifies an absolute timeout in seconds and nanoseconds since the Epoch, 
//             1970-01-01 00:00:00 +0000 (UTC). This structure is defined as follows:
//
//             struct timespec 
//             {
//                time_t tv_sec;      /* Seconds */
//                long   tv_nsec;     /* Nanoseconds [0 .. 999999999] */
//             };
//             If the timeout has already expired by the time of the call, and the semaphore 
//             could not be locked immediately, then sem_timedwait() fails with a timeout error 
//             (errno set to ETIMEDOUT). If the operation can be performed immediately, then 
//             sem_timedwait() never fails with a timeout error, regardless of the value of 
//             TIMEOUT.
//
//
static int srvr_WaitSemaphore(int iMsecs)
{
   //
   // if    sem == 0 : wait
   // else  sem-- (LOCK)   
   //
   return(GEN_WaitSemaphore(&pstMap->G_tSemSvr, iMsecs));
}

// 
// Function:   srvr_HandleGuard
// Purpose:    Respond to the guard query
// 
// Parameters: 
// Returns:    
// Note:       
//
static void srvr_HandleGuard(void)
{
   LOG_Report(0, "SVR", "srvr_HandleGuard():Host-notification send");
   GLOBAL_HostNotification(GLOBAL_GRD_NFY);
}


/* ======   Local Functions separator ===========================================
___SERVER_FUNCTIONS(){}
==============================================================================*/

//  
// Function:      srvr_CallbackConnection
// Description:   Handle the connection list
// 
// Parameters:    Socket descriptor
// Returns:       TRUE if OKee
// Note:          NET_HandleSessions(..) will callback here as soon as a complete
//                HTTP Get/POST request has been acquired through the socket.
//                This Callback will verify if the HTTP request concerns:
//                o a STATIC  page: Try to read it from file and return to the socket
//                o a DYNAMIC page: Lookup the page and forward to the registered handler
//
//                The handler will respond back with a SIGUSR1 signal with the appropriate
//                notification set.
//  
static bool srvr_CallbackConnection(NETCL *pstCl)
{
   bool     fCc=TRUE;
   int      iFlag, iTimeout, iCc;

   //
   // Data came in from the socket: check if we have something to do
   //
   // DFD-0-1.1 (RPI Rcv HTTP)
   SHOW_NET_PAYLOAD("srvr_CallbackConnection()", pstCl->iPort, &pstCl->pcUrl[pstCl->iPaylIdx]);
   //
   if( (iTimeout = srvr_HandleHttpRequest(pstCl)) > 0) 
   {
      //
      // Command has been forwarded to a remote handler. It will SIGUSR1 back through the semaphore,
      // with the correct GLOBAL_xxx_NFY set. Give it until timeout to report back and act accordingly.
      //
      switch( srvr_WaitSemaphore(iTimeout) )
      {
         case 0:
            //
            // Signal from someone: check notification
            //
            if( GLOBAL_GetSignalNotification(PID_SRVR, GLOBAL_BTN_NFY) ) srvr_NotifyFromButton(pstCl);
            if( GLOBAL_GetSignalNotification(PID_SRVR, GLOBAL_PRX_NFY) ) srvr_NotifyFromProxy(pstCl);
            if( GLOBAL_GetSignalNotification(PID_SRVR, GLOBAL_CAM_NFY) ) srvr_NotifyFromCamera(pstCl);
            if( GLOBAL_GetSignalNotification(PID_SRVR, GLOBAL_GRD_RUN) ) srvr_HandleGuard();
            if( (iFlag = GLOBAL_SetSignalNotification(PID_SRVR, 0))    ) srvr_NotifySpurious(iFlag);
            //
            PRINTF("srvr_CallbackConnection():Done" CRLF);
            break;

         default:
            break;

         case -1:
            PRINTF("srvr_CallbackConnection():ERROR srvr_WaitSemaphore" CRLF);
            LOG_Report(errno, "RPI", "srvr_CallbackConnection():ERROR srvr_WaitSemaphore");
            fCc = FALSE;
            break;

         case 1:
            //
            // Timeout: handler has NOT yet completed: 
            //
            GLOBAL_CommandGet(pstCmd);
            GLOBAL_DUMPCOMMAND("srvr_CallbackConnection():PID_SRVR(Get):No reply from handler", pstCmd);
            LOG_Report(errno, "RPI", "srvr_CallbackConnection():sem_timedwait");
            PRINTF1("srvr_CallbackConnection():Timeout(errno=%d)" CRLF, errno);
            //
            // No response from any command handler:
            // Check the completion of the command
            //
            pstCmd->iError = CAM_STATUS_TIMEOUT;
            //
            if( srvr_HandleHttpReply(pstCl) )
            {
               //
               // The command timed out: specific reply has already been send back to the app
               //
            }
            else
            {
               //
               // Nothing specific can be done: generate a generic response
               //
               pstCmd->iError = CAM_STATUS_REJECTED;
               srvr_HandleCompletion(pstCl);
            }
            GEN_Free(pstCmd);
            //PRINTF2("srvr_CallbackConnection():Cmd:free Status=%d, Error=%d" CRLF, pstCmd->iStatus, pstCmd->iError);
            break;
      }
   }
   iCc = NET_FlushCache(pstCl);
   if(iCc < 0)
   {
      //
      // Unrecoverable (for now) NET_Write error: restart theApp
      //
      LOG_Report(0, "RPI", "srvr_CallbackConnection():NET_Flush error");
      GLOBAL_Notify(PID_HOST, GLOBAL_RPI_RST, SIGTERM);
   }
   //
   // Session is finished: close connection
   //
   PRINTF("srvr_CallbackConnection():Ready, socket disconnected." CRLF);
   return(fCc);
}

//  
//  Function    : srvr_NotifyFromButton
//  Description : Signal came back from PID_BUTN: handle the completion code
//  
//  Parameters  : Socket descriptor
//  Returns     : 
//  
static void srvr_NotifyFromButton(NETCL *pstCl)
{
   //
   // PID_BUTN has completed: return it to the server
   // Check the completion of the command
   //
   srvr_HandleCompletion(pstCl);
   GEN_Free(pstCmd);
   PRINTF2("srvr_NotifyFromButton():Cmd:free Status=%d, Error=%d" CRLF, pstCmd->iStatus, pstCmd->iError);
}


//  
//  Function    : srvr_NotifyFromCamera
//  Description : Signal came back from CAM: handle the completion code
//  
//  Parameters  : Socket descriptor
//  Returns     : 
//  
static void srvr_NotifyFromCamera(NETCL *pstCl)
{
   //
   // DFD-0-10.1 (RPI Get stCmd completion)
   // CAM has completed: return it to the server
   // Update activity seconds (Recording/Streaming)
   // Check the completion of the command
   //
   srvr_HandleCompletion(pstCl);
   // DFD-0-10.2 (RPI Put stCmd Free)
   GEN_Free(pstCmd);
   PRINTF2("srvr_NotifyFromCamera():Cmd:free Status=%d, Error=%d" CRLF, pstCmd->iStatus, pstCmd->iError);
}

//  
//  Function    : srvr_NotifyFromProxy
//  Description : Signal came back from proxy: handle the completion code
//  
//  Parameters  : Socket descriptor
//  Returns     : 
//  
static void srvr_NotifyFromProxy(NETCL *pstCl)
{
   PRINTF("srvr_NotifyFromProxy():Proxy NFY" CRLF);
   srvr_HandleHttpProxy(pstCl);
}

//  
//  Function    : srvr_NotifySpurious
//  Description : Some signal came in: handle unsupported nfy
//  
//  Parameters  : Spurious notifications
//  Returns     : 
//  
static void srvr_NotifySpurious(int iFlag)
{
   PRINTF1("srvr_NotifySpurious():Spurious signals 0x%08X" CRLF, iFlag);
   LOG_Report(0, "RPI", "srvr_NotifySpurious():Spurious signal 0x%08X", iFlag);
   // Reset them
   GLOBAL_GetSignalNotification(PID_SRVR, iFlag);
}

//  
//  Function    : srvr_HandleCompletion
//  Description : Signal came back from CAM: handle the completion code
//  
//  Parameters  : Socket descriptor
//  Returns     : Timeout in msecs
//  
static void srvr_HandleCompletion(NETCL *pstCl)
{
   GLOBAL_CommandGet(pstCmd);
   GLOBAL_DUMPCOMMAND("srvr_HandleCompletion():PID_SRVR(Get):Completion from handler", pstCmd);
   //
   switch(pstCmd->iError)
   {
      case CAM_STATUS_TIMEOUT:
         PRINTF("srvr_HandleCompletion():TIMEOUT" CRLF);
         GLOBAL_Status(CAM_STATUS_TIMEOUT);
         RPI_ReportError(pstCl, pstCmd);
         break;

      case CAM_STATUS_ERROR:
         PRINTF("srvr_HandleCompletion():ERROR" CRLF);
         GLOBAL_Status(CAM_STATUS_ERROR);
         RPI_ReportError(pstCl, pstCmd);
         break;

      case CAM_STATUS_REJECTED:
         PRINTF("srvr_HandleCompletion():REJECTED" CRLF);
         GLOBAL_Status(CAM_STATUS_REJECTED);
         RPI_ReportBusy(pstCl, pstCmd);
         break;

      case CAM_STATUS_REDIRECT:
         pstCmd->iError = CAM_STATUS_IDLE;
         //PRINTF("srvr_HandleCompletion():REDIRECTED" CRLF);
         GLOBAL_Status(pstCmd->iStatus);
         HTTP_DynamicPageHandler(pstCl, pstMap->G_iCurDynPage);
         break;

      default:
         //PRINTF("srvr_HandleCompletion():OKee" CRLF);
         GLOBAL_Status(pstCmd->iStatus);
         srvr_HandleHttpReply(pstCl);
         break;
   }
}

//  
//  Function    : srvr_HandleHttpProxy
//  Description : Request has been completed: handle the reply back to the browser/application
//  
//  Parameters  : Socket descriptor
//  Returns     : 
//  
static int srvr_HandleHttpProxy(NETCL *pstCl)
{
   bool     fCc=FALSE;
   FTYPE    tType;

   PRINTF("srvr_HandleHttpProxy()" CRLF);
   tType = GEN_GetDynamicPageType(pstMap->G_iCurDynPage);
   switch(tType)
   {
      default:
      case HTTP_HTML:
         fCc = PROXY_DynPageReplyHtml(pstCl);
         break;

      case HTTP_JSON:
         fCc = PROXY_DynPageReplyJson(pstCl);
         break;
   }
   return(fCc);
}

//  
//  Function    : srvr_HandleHttpReply
//  Description : Request has been completed: handle the reply back to the browser/application
//  
//  Parameters  : Socket descriptor
//  Returns     : 
//  
static int srvr_HandleHttpReply(NETCL *pstCl)
{
   bool     fCc=FALSE;
   FTYPE    tType;

   tType = GEN_GetDynamicPageType(pstMap->G_iCurDynPage);
   switch(tType)
   {
      default:
      case HTTP_HTML:
         fCc = srvr_HandleHttpReplyHtml(pstCl);
         break;

      case HTTP_JSON:
         fCc = srvr_HandleHttpReplyJson(pstCl);
         break;
   }
   return(fCc);
}

//  
//  Function    : srvr_HandleHttpReplyHtml
//  Description : Request has been completed: handle HTML reply banck to the browser/application
//  
//  Parameters  : Socket descriptor
//  Returns     : True if handled
//  
static int srvr_HandleHttpReplyHtml(NETCL *pstCl)
{
   bool     fCc=FALSE;

   switch(pstCmd->iCommand)
   {
      default:
         //PRINTF("srvr_HandleHttpReplyHtml():DEFAULT" CRLF);
         break;

      case CAM_CMD_MAN_SNAP:
         //PRINTF("srvr_HandleHttpReplyHtml():CAM_CMD_MAN_SNAP" CRLF);
         if(pstCmd->iError == CAM_STATUS_TIMEOUT)  fCc = HTTP_HtmlLinkSnapshot(pstCl, "Snapshot");
         else                                      fCc = HTTP_HtmlShowSnapshot(pstCl, "Snapshot");
         fCc = TRUE;
         break;
   }
   return(fCc);
}

//  
//  Function    : srvr_HandleHttpReplyJson
//  Description : Request has been completed: handle JSON reply back to the browser/application
//  
//  Parameters  : Socket descriptor
//  Returns     : True if handled
//  
static int srvr_HandleHttpReplyJson(NETCL *pstCl)
{
   bool     fCc=FALSE;
   
   switch(pstCmd->iCommand)
   {
      default:
         //PRINTF("srvr_HandleHttpReplyJson():DEFAULT" CRLF);
         RPI_BuildJsonMessageBody(pstCl, pstCmd);
         fCc = TRUE;
         break;
   }
   return(fCc);
}

//  
//  Function    : srvr_HandleHttpRequest
//  Description : Data came in through the socket: handle it
//  
//  Parameters  : Socket descriptor
//  Returns     : Timeout in msecs
//  
static int srvr_HandleHttpRequest(NETCL *pstCl)
{
   int      iWait=0, iIdx;
   FTYPE    tType;
   pid_t    tPid;
   int      ePid;

   //PRINTF1("srvr_HandleHttpRequest():%s" CRLF, pstCl->pcUrl);
   //
   // Handle incoming HTTP GET request:
   //
   //   "GET /index.html HTTP/1.1" CR,LF
   //   "Host: 10.0.0.231" CR,LF
   //   "User-Agent: Mozilla/5.0 (Windows; U; WinGecko/20100722 Firefox/3.6.8 (.NET CLR 3.5.30729)" CR,LF
   //   "Accept: image/png,image/*;q=0.8,*/*;q=0.5" CR,LF
   //   "Accept-Encoding: gzip,deflate" CR,LF
   //   "Accept-Charset: ISO-8859-1,utf-8;q=0.7,*;q=0.7" CR,LF
   //   "Keep-Alive: 115" CR,LF
   //   "Connection: keep-alive" CR,LF
   //   CR,LF
   //
   // Check if the requested URL is a dynamically generated page or if it should
   // be on the filesystem.
   //
   if( (iIdx = HTTP_PageIsDynamic(pstCl)) < 0 )
   {
      // DFD-0-3.3 (RPI Xmt STATIC page)
      // This page is a STATIC page (needs to come from the file system)
      //
      pstMap->G_iCurDynPage = -1;
      GLOBAL_CommandPut(pstCmd);
      GLOBAL_DUMPCOMMAND("srvr_HandleHttpRequest():PID_SRVC(put):IsStatic", pstCmd);
      PRINTF1("srvr_HandleHttpRequest():Command to STATIC (%s)" CRLF, pstCl->pcUrl);
      HTTP_RequestStaticPage(pstCl);
   }
   else
   {
      // DFD-0-1.2 (RPI Get stCmd Status)
      // This page is an internal (DYNAMICALLY build) page
      // Check if we can do this ourselves here, or if it need additional
      // handling.
      //
      if( GEN_IsBusy(pstCmd) ) 
      {
         PRINTF1("srvr_HandleHttpRequest():Command (%s) REJECTED" CRLF, pstCl->pcUrl);
         pstCmd->iError = GLOBAL_Status(CAM_STATUS_REJECTED);
         GLOBAL_CommandPut(pstCmd);
         GLOBAL_DUMPCOMMAND("srvr_HandleHttpRequest():PID_SRVC(put):IsBusy", pstCmd);
         RPI_ReportBusy(pstCl, pstCmd);
      }
      else
      {
         //
         // No command in progress: erase previous command (in case we omit a new command)
         //
         GEN_STRCPY(pstMap->G_pcCommand, "");
         pstMap->G_iCurDynPage = iIdx;
         //
         tType        = GEN_GetDynamicPageType(iIdx);
         ePid         = GEN_GetDynamicPagePid(iIdx);
         iWait        = GEN_GetDynamicPageTimeout(iIdx);
         pstCmd->tUrl = GEN_GetDynamicPageUrl(iIdx);
         //
         // Type is HTTP_HTML or HTTP_JSON
         // HTTP_HTML: ...cam.html?xxxx&-p17-p2....
         // HTTP_JSON: ...cam.jsom?command=xxxx&para1=yyyy&....
         //
         tPid = GLOBAL_PidGet(ePid);
         //
         // DFD-0-2.1 (RPI Put G_stMap args)
         //
         RPI_CollectParms(pstCl, tType);
         //
         //PRINTF2("srvr_HandleHttpRequest():Data from socket: [%s]-[%s]" CRLF, pstCl->pcUrl, pstMap->G_pcCommand);
         //PRINTF1("srvr_HandleHttpRequest():%s" CRLF, &pstCl->pcUrl[pstCl->iFileIdx]);
         PRINTF4("srvr_HandleHttpRequest():Dynamic URL:Idx=%d, Pid=%d, Wait=%d, URL=%s" CRLF, iIdx, tPid, iWait, GEN_GetDynamicPageName(iIdx) );
         //
         // Signal the thread to handle it (if it is not us)
         //
         GLOBAL_CommandPut(pstCmd);
         //
         if(tPid > 0)
         {
            GLOBAL_DUMPCOMMAND("srvr_HandleHttpRequest():PID_SRVC(put):To PID", pstCmd);
            PRINTF3("srvr_HandleHttpRequest():Command to Pid=%d, [%s]-[%s]" CRLF, tPid, pstCl->pcUrl, pstMap->G_pcCommand);
            // DFD-0-3.1 (RPI Xmt SIGUSR1 to pid)
            GLOBAL_SetSignalNotification(ePid, GLOBAL_SVR_RUN);
            GEN_SignalPid(tPid, SIGUSR1);
         }
         else
         {
            // DFD-0-3.2 (RPI Xmt Dyn Page to HTTP)
            //
            GLOBAL_DUMPCOMMAND("srvr_HandleHttpRequest():PID_SRVC(put):Selfie", pstCmd);
            if(HTTP_DynamicPageHandler(pstCl, iIdx))
            {
               //
               // Dynamic page returns TRUE if the current page request should complete the HTTP request
               //
               PRINTF2("srvr_HandleHttpRequest():Command to RPI:[%s]-[%s]" CRLF, pstCl->pcUrl, pstMap->G_pcCommand);
            }
            else
            {
               //
               // No reply was set up: send back error
               //
               PRINTF2("srvr_HandleHttpRequest():Command to ERROR:[%s]-[%s]" CRLF, pstCl->pcUrl, pstMap->G_pcCommand);
               pstCmd->iError = GLOBAL_Status(CAM_STATUS_ERROR);
               pstCmd->iArgs |= CAM_ERR;
               GLOBAL_DUMPCOMMAND("srvr_HandleHttpRequest():PID_SRVC(put):Error", pstCmd);
               RPI_ReportError(pstCl, pstCmd);
            }
         }
      }
   }
   return(iWait);
}

#ifdef  FEATURE_JSON_SHOW_PAYLOAD
/*
 * Function    : srvr_ShowNetPayload
 * Description : Print out the network json payload
 *
 * Parameters  : Title, port, data
 * Returns     : 
 *
 */
static void srvr_ShowNetPayload(const char *pcTitle, int iPort, char *pcData)
{
   LOG_printf("%s(p=%d):Payload=%s" CRLF, pcTitle, iPort, pcData);
}
#endif   //FEATURE_JSON_SHOW_PAYLOAD

