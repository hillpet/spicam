/*  (c) Copyright:  2017  Patrn, Confidential Data
 *
 *  Workfile:           gen_func.h
 *  Revision:          
 *  Modtime:           
 *
 *  Purpose:            Headerfile for gen_func functions
 *
 *
 *
 *  Compiler/Assembler: Raspbian Linux GNU gcc
 *  Ext Packages:
 *
 *  Author:             Peter Hillen
 *  Date Created:       09 Mar 2017
 *
 *  Revisions:
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
**/

#ifndef _GEN_FUNC_H_
#define _GEN_FUNC_H_

typedef bool   (*PFDYN)(void *);
//
void  GEN_SegmentationFault         (const char *, int);
bool  GEN_CheckDelete               (void);
void  GEN_Free                      (GLOCMD *);
bool  GEN_IsBusy                    (GLOCMD *);
void  GEN_Signal                    (PIDT, int);
void  GEN_SignalWait                (PIDT, int);


#endif /* _GEN_FUNC_H_ */
