/*  (c) Copyright:  2017  Patrn, Confidential Data
 *
 *  Workfile:           cmd_exec.h
 *  Revision:          
 *  Modtime:           
 *
 *  Purpose:            Headerfile for command cmd_exec.c
 *
 *
 *
 *  Compiler/Assembler: Raspbian Linux GNU gcc
 *  Ext Packages:
 *
 *  Author:             Peter Hillen
 *  Date Created:       16 Apr 2017
 *
 *  Revisions:
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
**/

#ifndef _CMD_EXEC_H_
#define _CMD_EXEC_H_

bool  CMD_BashProcess            (GLOCMD *);
bool  CMD_ExecProcess            (GLOCMD *);

#endif /* _CMD_EXEC_H_ */
