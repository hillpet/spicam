/*  (c) Copyright:  2017..2018  Patrn, Confidential Data
 *
 *  Workfile:           key_defs.h
 *  Revision:          
 *  Modtime:           
 *
 *  Purpose:            Headerfile for RCU keys
 *
 *
 *
 *  Compiler/Assembler: Raspbian Linux GNU gcc
 *  Ext Packages:
 *
 *  Author:             Peter Hillen
 *  Date Created:       10 Sep 2017
 *
 *  Revisions:          06 Jan 2018:Split FP buttons to but_defs.h
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
**/

EXTRACT_KEY(RCU_NOKEY,           "RCU_NOKEY")            // --                --
//
// RCU Keys (IR and HTTP)
//
EXTRACT_KEY(RCU_POWER,           "RCU_POWER")            // STANDBY           RECORD + POWER-Icon
EXTRACT_KEY(RCU_OK,              "RCU_OK")               // ENTER             Ok
EXTRACT_KEY(RCU_UP,              "RCU_UP")               // UP                Up
EXTRACT_KEY(RCU_DOWN,            "RCU_DOWN")             // DOWN              Down
EXTRACT_KEY(RCU_LEFT,            "RCU_LEFT")             // LEFT              Left
EXTRACT_KEY(RCU_RIGHT,           "RCU_RIGHT")            // RIGHT             Right
EXTRACT_KEY(RCU_PLAY,            "RCU_PLAY")             // TV-MODE           Play
EXTRACT_KEY(RCU_STOP,            "RCU_STOP")             // DIM               Stop
EXTRACT_KEY(RCU_EXIT,            "RCU_EXIT")             // --                Exit
EXTRACT_KEY(RCU_PAUSE,           "RCU_PAUSE")            // --                Pause
EXTRACT_KEY(RCU_NEXT,            "RCU_NEXT")             // --                >>|
EXTRACT_KEY(RCU_PREVIOUS,        "RCU_PREVIOUS")         // --                |<<
EXTRACT_KEY(RCU_FORWARD,         "RCU_FORWARD")          // --                >>
EXTRACT_KEY(RCU_REWIND,          "RCU_REWIND")           // --                <<
EXTRACT_KEY(RCU_CHANNELUP,       "RCU_CHANNELUP")        // --                Ch+
EXTRACT_KEY(RCU_CHANNELDOWN,     "RCU_CHANNELDOWN")      // --                Ch-
EXTRACT_KEY(RCU_EPG,             "RCU_EPG")              // --                Guide
EXTRACT_KEY(RCU_INFO,            "RCU_INFO")             // --                Info
EXTRACT_KEY(RCU_MENU,            "RCU_MENU")             // --                Menu
EXTRACT_KEY(RCU_ESC,             "RCU_ESC")              // --                Return/Back
EXTRACT_KEY(RCU_0,               "RCU_0")                // --                0
EXTRACT_KEY(RCU_1,               "RCU_1")                // --                1
EXTRACT_KEY(RCU_2,               "RCU_2")                // --                2
EXTRACT_KEY(RCU_3,               "RCU_3")                // --                3
EXTRACT_KEY(RCU_4,               "RCU_4")                // --                4
EXTRACT_KEY(RCU_5,               "RCU_5")                // --                5
EXTRACT_KEY(RCU_6,               "RCU_6")                // --                6
EXTRACT_KEY(RCU_7,               "RCU_7")                // --                7
EXTRACT_KEY(RCU_8,               "RCU_8")                // --                8
EXTRACT_KEY(RCU_9,               "RCU_9")                // --                9
EXTRACT_KEY(RCU_RED,             "RCU_RED")              // --                Red
EXTRACT_KEY(RCU_BLUE,            "RCU_BLUE")             // --                Blue
EXTRACT_KEY(RCU_GREEN,           "RCU_GREEN")            // --                Green
EXTRACT_KEY(RCU_YELLOW,          "RCU_YELLOW")           // --                Yellow
