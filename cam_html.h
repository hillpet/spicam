/*  (c) Copyright:  2017  Patrn, Confidential Data
 *
 *  Workfile:           cam_html.h
 *  Revision:          
 *  Modtime:           
 *
 *  Purpose:            Headerfile for cam_html.c, the dynamic HTML page handler
 *
 *
 *
 *  Compiler/Assembler: Raspbian Linux GNU gcc
 *  Ext Packages:
 *
 *  Author:             Peter Hillen
 *  Date Created:       17 Mar 2017
 *
 *  Revisions:
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
**/

#ifndef _CAM_HTML_H_
#define _CAM_HTML_H_

//
//
// Global prototypes
//
const CAMPAGE *CAM_HtmlGetCommands           (void);
const char    *CAM_HtmlGetCommandString      (int);
void           CAM_HtmlInit                  (void);

#endif /* _CAM_HTML_H_ */
