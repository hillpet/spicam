/*  (c) Copyright:  2017  Patrn ESS, Confidential Data
 *
 *  Workfile:           cam_html.c
 *  Revision:          
 *  Modtime:           
 *
 *  Purpose:            Raspberry pi dynamic HTML page command handlers
 *
 *
 *
 *  Compiler/Assembler: Raspbian Linux GNU gcc
 *  Ext Packages:
 *
 *  Author:             Peter Hillen
 *  Date Created:       17 Mar 2017
 *
 *  Revisions:
 *  Entry Points:       
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
**/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdarg.h>
#include <stddef.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>
#include <sched.h>
#include <time.h>
#include <errno.h>

#include <common.h>
#include "config.h"
#include "globals.h"
#include "cmd_main.h"
#include "cam_main.h"
#include "cam_page.h"
#include "gen_http.h"
#include "gen_func.h"
#include "graphics.h"
//
#include "cam_html.h"

//#define USE_PRINTF
#include <printf.h>

//
// Camera parameter list (as defined in cam.defs). Extract what you need here:
//
// iFunction:        Define if a parameter affects certain camera function
//                      JSN_TXT : For JSON API: parameter is formatted as JSON text    ("value" = "xxxxx")
//                      JSN_INT :               parameter is formatted as JSON number  ("value" = xxxxx)
//
//                      CAM_STI : Paramater concerns Snapshot fotos
//                      CAM_TIM :                    Timelapse fotos
//                      CAM_REC :                    Recordings
//                      CAM_STR :                    Streaming video
//                      CAM_PIX :                    Picture file retrieval
//                      CAM_VID :                    Video file retrieval
//                      CAM_PRM :                    Parameter definition
//                      CAM_STP :                    Stopping command-in-progress
//                      CAM_ALL :                    All HTTP commands
//                      CAM____ :                    None of the commands
//
// pcJson:           JSON API parameter name
// pcParm:           HTML API parameter name
// pcOption:         Raspberry Pi command option
// iValueOffset:     Global mmap structure offset of the parameter
// iValueSize:       Global mmap parameter array size
// iChangedOffset:   Global mmap structure offset of the parameter has changed-marker
//
// Local prototypes
//
static bool    cam_CallbackHtmlGeneric       (GLOCMD *);

//
// URL command handler functions
//
static bool    cam_HtmlAlarm                 (GLOCMD *);
static bool    cam_HtmlDefaults              (GLOCMD *);
static bool    cam_HtmlDeleteFotos           (GLOCMD *);
static bool    cam_HtmlDeleteVideos          (GLOCMD *);
static bool    cam_HtmlFotos                 (GLOCMD *);
static bool    cam_HtmlParms                 (GLOCMD *);
static bool    cam_HtmlRecord                (GLOCMD *);
static bool    cam_HtmlSnapshot              (GLOCMD *);
static bool    cam_HtmlStop                  (GLOCMD *);
static bool    cam_HtmlStream                (GLOCMD *);
static bool    cam_HtmlTimelapse             (GLOCMD *);
static bool    cam_HtmlVideos                (GLOCMD *);

//
// Supported dynamic HTML functions
//
static const CAMPAGE stHtmlCommands[] =
{
//    iCommand             iFunction   pcCommand         pfCmdCb
   {  CAM_CMD_PARMS,       CAM_PRM,    "parms",          cam_HtmlParms        },      // On top to show correct parms object
   //
   {  CAM_CMD_ALM,         CAM_ALM,    "alarm",          cam_HtmlAlarm        },
   {  CAM_CMD_DEFAULTS,    CAM_DEF,    "defaults",       cam_HtmlDefaults     },
   {  CAM_CMD_DEL_FOTOS,   CAM_RMP,    "rm-fotos",       cam_HtmlDeleteFotos  },
   {  CAM_CMD_DEL_VIDEOS,  CAM_RMV,    "rm-videos",      cam_HtmlDeleteVideos },
   {  CAM_CMD_FOTOS,       CAM_PIX,    "fotos",          cam_HtmlFotos        },
   {  CAM_CMD_MAN_RECORD,  CAM_REC,    "record",         cam_HtmlRecord       },
   {  CAM_CMD_MAN_SNAP,    CAM_STI,    "snapshot",       cam_HtmlSnapshot     },
   {  CAM_CMD_MAN_STOP,    CAM_STP,    "stop",           cam_HtmlStop         },
   {  CAM_CMD_STREAM,      CAM_STR,    "stream",         cam_HtmlStream       },
   {  CAM_CMD_TIMELAPSE,   CAM_TIM,    "timelapse",      cam_HtmlTimelapse    },
   {  CAM_CMD_VIDEOS,      CAM_VID,    "videos",         cam_HtmlVideos       },
   {  0,                   0,          0,                0                    }
};
static const char *pcNoCommand = "----";

//
// Redirect some dyn pages
//
static const char *pcDepricatedFotos   =  "fotos.html";
static const char *pcDepricatedVideos  =  "videos.html";

/*------  Local functions separator ------------------------------------
__GLOBAL_FUNCTIONS(){};
----------------------------------------------------------------------------*/

//
// Function:   CAM_HtmlGetCommands
// Purpose:    Return the list of supported dynamic HTTP HTML commands
//
// Parms:      
// Returns:    The list
//
const CAMPAGE *CAM_HtmlGetCommands()
{
   return(stHtmlCommands);
}

//
// Function:   CAM_HtmlGetCommandString
// Purpose:    Return the HTML command string
//
// Parms:      The command enum CAM_CMD_xxxx
// Returns:    The command name
//
const char *CAM_HtmlGetCommandString(int iCmd)
{
   int   iIdx=0;

   while(stHtmlCommands[iIdx].pcCommand)
   {
      if(iCmd == stHtmlCommands[iIdx].iCommand) return(stHtmlCommands[iIdx].pcCommand);
      iIdx++;
   }
   return(pcNoCommand);
}

//
// Function:   CAM_HtmlInit
// Purpose:    Init dynamic HTTP HTML commands
//
// Parms:      
// Returns:    
//
void CAM_HtmlInit()
{
}

/*------  Local functions separator ------------------------------------
__HTML_PAGES(){};
----------------------------------------------------------------------------*/

//
// Function:   cam_HtmlAlarm
// Purpose:    
//
// Parms:      Global Command struct^
// Returns:    CC
// Note:       if CC==TRUE Proceed to signal the command-thread
//             else notify the HTTP server
//
static bool cam_HtmlAlarm(GLOCMD *pstCmd)
{
   pstCmd->iError = CAM_STATUS_ERROR;
   return(FALSE);
}

//
// Function:   cam_HtmlDefaults
// Purpose:    
//
// Parms:      Global Command struct^
// Returns:    CC
// Note:       if CC==TRUE Proceed to signal the command-thread
//             else notify the HTTP server
//
static bool cam_HtmlDefaults(GLOCMD *pstCmd)
{
   pstCmd->iError = CAM_STATUS_ERROR;
   return(FALSE);
}

//
// Function:   cam_HtmlDeleteFotos
// Purpose:    
//
// Parms:      Global Command struct^
// Returns:    CC
// Note:       if CC==TRUE Proceed to signal the command-thread
//             else notify the HTTP server
//
static bool cam_HtmlDeleteFotos(GLOCMD *pstCmd)
{
   pstCmd->iError = CAM_STATUS_ERROR;
   return(FALSE);
}

//
// Function:   cam_HtmlDeleteVideos
// Purpose:    
//
// Parms:      Global Command struct^
// Returns:    CC
// Note:       if CC==TRUE Proceed to signal the command-thread
//             else notify the HTTP server
//
static bool cam_HtmlDeleteVideos(GLOCMD *pstCmd)
{
   pstCmd->iError = CAM_STATUS_ERROR;
   return(FALSE);
}

//
// Function:   cam_HtmlFotos
// Purpose:    
//
// Parms:      Global Command struct^
// Returns:    CC
// Note:       if CC==TRUE Proceed to signal the command-thread
//             else notify the HTTP server
//
static bool cam_HtmlFotos(GLOCMD *pstCmd)
{
   int      iIdx;

   switch(pstCmd->tUrl)
   {
      case CAM_DYN_HTML_CAM:
         PRINTF("cam_HtmlFotos()" CRLF);
         //
         // Deprecated  cam.html?fotos:
         // trigger to: fotos.html
         //
         iIdx = CAM_FindDynamicPage(pcDepricatedFotos);
         if(iIdx != -1)
         {
            PRINTF1("cam_HtmlFotos():Redirect to i=%d" CRLF, iIdx);
            pstMap->G_iCurDynPage = iIdx;
            pstCmd->iError        = CAM_STATUS_REDIRECT;
         }
         else
         {
            pstCmd->iError = CAM_STATUS_ERROR;
         }
         break;

      default:
         pstCmd->iError = CAM_STATUS_ERROR;
         break;
   }
   return(FALSE);
}

//
// Function:   cam_HtmlParms
// Purpose:    
//
// Parms:      Global Command struct^
// Returns:    CC
// Note:       if CC==TRUE Proceed to signal the command-thread
//             else notify the HTTP server
//
static bool cam_HtmlParms(GLOCMD *pstCmd)
{
   bool           fCc=FALSE;

   switch(pstCmd->tUrl)
   {
      case CAM_DYN_HTML_CAM:
         PRINTF("cam_HtmlParms():" CRLF);
         pstCmd->pfCallback = cam_CallbackHtmlGeneric;
         fCc = TRUE;
         break;

      default:
         pstCmd->iError = CAM_STATUS_ERROR;
         pstCmd->iArgs |= CAM_ERR;
         break;
   }
   return(fCc);
}

//
// Function:   cam_HtmlRecord
// Purpose:    
//
// Parms:      Global Command struct^
// Returns:    CC
// Note:       if CC==TRUE Proceed to signal the command-thread
//             else notify the HTTP server
//
static bool cam_HtmlRecord(GLOCMD *pstCmd)
{
   pstCmd->iError = CAM_STATUS_ERROR;
   return(FALSE);
}

//
// Function:   cam_HtmlSnapshot
// Purpose:    
//
// Parms:      Global Command struct^
// Returns:    CC
// Note:       if CC==TRUE Proceed to signal the command-thread
//             else notify the HTTP server
//
static bool cam_HtmlSnapshot(GLOCMD *pstCmd)
{
   bool     fCc=FALSE;
   u_int32  ulSecs;
   char    *pcFile;

   switch(pstCmd->tUrl)
   {
      case CAM_DYN_HTML_CAM:
         //
         // Build foto output filename "YYYYMMDD_HHMMSS"
         //
         pcFile  = (char *) safemalloc(MAX_PATH_LEN);
         ulSecs = RTC_GetSystemSecs();
         RTC_ConvertDateTime(TIME_FORMAT_YYYY_MM_DD_HH_MM_SS, pcFile, ulSecs);
         GEN_SPRINTF(pstMap->G_pcLastFile, "%s%s%s.%s", pstMap->G_pcWwwDir, pstMap->G_pcPixPath, pcFile, pstMap->G_pcPixFileExt);
         //
         // Collect command and parameters
         //
         LOG_Report(0, "CAM", "cam_HtmlSnapshot(%s)", pstMap->G_pcLastFile);
         GEN_SPRINTF(pstCmd->pcExec, RPI_PIX_EXEC);
         GLOBAL_InsertOptions(pstCmd->pcArgs, MAX_ARG_LEN, CAM_STI);
         PRINTF2("cam_HtmlSnapshot():[%s]:%s" CRLF, pstMap->G_pcLastFile, pstCmd->pcArgs);
         //
         // Spawn execution to CMND thread:
         //
         pstCmd->pfCallback = cam_CallbackHtmlGeneric;
         safefree(pcFile);
         fCc = TRUE;
         break;

      default:
         pstCmd->iError = CAM_STATUS_ERROR;
         pstCmd->iArgs |= CAM_ERR;
         break;
   }
   PRINTF("cam_HtmlSnapshot():Done" CRLF);
   return(fCc);
}

//
// Function:   cam_HtmlStop
// Purpose:    
//
// Parms:      Global Command struct^
// Returns:    CC
// Note:       if CC==TRUE Proceed to signal the command-thread
//             else notify the HTTP server
//
static bool cam_HtmlStop(GLOCMD *pstCmd)
{
   pstCmd->iError = CAM_STATUS_ERROR;
   return(FALSE);
}

//
// Function:   cam_HtmlStream
// Purpose:    
//
// Parms:      Global Command struct^
// Returns:    CC
// Note:       if CC==TRUE Proceed to signal the command-thread
//             else notify the HTTP server
//
static bool cam_HtmlStream(GLOCMD *pstCmd)
{
   pstCmd->iError = CAM_STATUS_ERROR;
   return(FALSE);
}

//
// Function:   cam_HtmlTimelapse
// Purpose:    
//
// Parms:      Global Command struct^
// Returns:    CC
// Note:       if CC==TRUE Proceed to signal the command-thread
//             else notify the HTTP server
//
static bool cam_HtmlTimelapse(GLOCMD *pstCmd)
{
   pstCmd->iError = CAM_STATUS_ERROR;
   return(FALSE);
}

//
// Function:   cam_HtmlVideos
// Purpose:    
//
// Parms:      Global Command struct^
// Returns:    CC
// Note:       if CC==TRUE Proceed to signal the command-thread
//             else notify the HTTP server
//
static bool cam_HtmlVideos(GLOCMD *pstCmd)
{
   int      iIdx;

   switch(pstCmd->tUrl)
   {
      case CAM_DYN_HTML_CAM:
         PRINTF("cam_HtmlVideos()" CRLF);
         //
         // Deprecated  cam.html?videos:
         // trigger to: videos.html
         //
         iIdx = CAM_FindDynamicPage(pcDepricatedVideos);
         if(iIdx != -1)
         {
            PRINTF1("cam_HtmlVideos():Redirect to i=%d" CRLF, iIdx);
            pstMap->G_iCurDynPage = iIdx;
            pstCmd->iError        = CAM_STATUS_REDIRECT;
         }
         else
         {
            pstCmd->iError = CAM_STATUS_ERROR;
         }
         break;

      default:
         pstCmd->iError = CAM_STATUS_ERROR;
         break;
   }
   return(FALSE);
}


/*------  Local functions separator ------------------------------------
__CALLBACK_FUNCTIONS(){};
----------------------------------------------------------------------------*/

//
// Function:   cam_CallbackHtmlGeneric
// Purpose:    Callback HTML: Generic functions
//
// Parms:      
// Returns:    TRUE if OKee
//
static bool cam_CallbackHtmlGeneric(GLOCMD *pstCmd)
{
    pstCmd->pfCallback = NULL;
   //
   // Signal command completion to the initiator
   //
   return(TRUE);
}

