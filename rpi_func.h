/*  (c) Copyright:  2017  Patrn, Confidential Data
 *
 *  Workfile:           rpi_func.h.h
 *  Revision:          
 *  Modtime:           
 *
 *  Purpose:            Headerfile for rpi_func
 *
 *
 *
 *  Compiler/Assembler: Raspbian Linux GNU gcc
 *  Ext Packages:
 *
 *  Author:             Peter Hillen
 *  Date Created:       09 Mar 2017
 *
 *  Revisions:
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
**/

#ifndef _RPI_FUNC_H_
#define _RPI_FUNC_H_

bool        RPI_BuildJsonMessageBody               (NETCL *, GLOCMD *);
bool        RPI_BuildJsonMessageArgs               (NETCL *, int);
bool        RPI_BuildJsonMessageBodyAlarm          (NETCL *, int);
bool        RPI_BuildJsonMessageBodyArray          (NETCL *, int, bool);
bool        RPI_BuildJsonMessageKodiRpcGetInput    (NETCL *, int);
bool        RPI_BuildJsonMessageKodiRpcGetItems    (NETCL *, int);
bool        RPI_BuildJsonMessageKodiRpcGetPing     (NETCL *, int);
bool        RPI_BuildJsonMessageKodiRpcGetProps    (NETCL *, int);
bool        RPI_BuildJsonMessageKodiRpcGetPlayers  (NETCL *, int);
int         RPI_CollectParms                       (NETCL *, FTYPE);
void        RPI_ReportBusy                         (NETCL *, GLOCMD *);
void        RPI_ReportError                        (NETCL *, GLOCMD *);
const char *RPI_GetMotionMode                      (int);
int         RPI_SetMotionMode                      (void);


#endif /* _RPI_FUNC_H_ */
