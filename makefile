#################################################################################
# 
# makefile:
#	$(TARGET) - Raspberry Pi HTTP server for the camera
#
#	Copyright (c) 2012-2018 Peter Hillen
#################################################################################
BOARD=$(shell cat ../../RPIBOARD)
#
ifeq ($(BOARD),1)
#
# RPi#1 (Rev-1): Metal case with CAM
#
	BOARD_OK=yes
	TARGET=rpicam
	COMMON=../common
	DEFS=-D FEATURE_IO_NONE
	DEFS+=-D FEATURE_CAMERA
	OBJM=
	OBJX=
	OBJCAM=cam_main.o cam_page.o cam_html.o cam_json.o graphics.o vector.o
	INCCAM=-I/usr/include/GraphicsMagick
	LIBCAM=-ltiff -ljpeg -ljbig -lz -lfreetype -lm -lgomp -lX11 -lXext -lxml2 -lbz2 -lwmflite -lGraphicsMagickWand -lGraphicsMagick
	LIBM=
	LIBLIRC=
	LIBUSB=
endif

ifeq ($(BOARD),2)
#
# RPi#2 (Rev-1): Black box with PiFace CAD LCD
#
	BOARD_OK=yes
	TARGET=spicam
	COMMON=../common
	DEFS=-D FEATURE_IO_PIFACECAD
	DEFS+=-D FEATURE_USB
	OBJM=cmd_mail.o mail/mail_func.o mail/mail_ssl.o mail/mail_smtp.o mail/mail_pop3.o mail/mail_client.o mail/blowbox.o mail/blowfish.o mail/encode.o mail/crypto.o
	OBJX=gen_usb.o rpi_proxy.o rpi_rcu.o
	OBJCAM=
	INCCAM=
	LIBCAM=
	LIBM=-lssl -lcrypto
	LIBLIRC=-llirc_client 
	LIBUSB=-lhidapi-hidraw -lhidapi-libusb
endif

ifeq ($(BOARD),3)
#
# RPi#3 (Rev-1): PiFace Digital
#
	BOARD_OK=yes
	TARGET=rpicam
	COMMON=../common
	DEFS=-D FEATURE_IO_PIFACE
	OBJM=
	OBJX=
	OBJCAM=
	INCCAM=
	LIBCAM=
	LIBM=
	LIBLIRC=
	LIBUSB=
endif

ifeq ($(BOARD),4)
#
# RPi#4 (Rev-1): 
# 03 Apr 2018: remove png12, jasper, lcms (not needed anymore).
# 19 May 2018: add PiFace Digital IO board
#
	BOARD_OK=yes
	TARGET=spicam
	COMMON=../common
	DEFS=-D FEATURE_IO_PIFACE
	DEFS+=-D FEATURE_CAMERA
	DEFS+=-D FEATURE_USB
	OBJM=cmd_mail.o mail/mail_func.o mail/mail_ssl.o mail/mail_smtp.o mail/mail_pop3.o mail/mail_client.o mail/blowbox.o mail/blowfish.o mail/encode.o mail/crypto.o
	OBJX=gen_usb.o rpi_proxy.o 
	OBJCAM=cam_main.o cam_page.o cam_html.o cam_json.o graphics.o vector.o
	INCCAM=-I/usr/include/GraphicsMagick
	LIBCAM=-ltiff -ljpeg -ljbig -lz -lfreetype -lm -lgomp -lX11 -lXext -lxml2 -lbz2 -lwmflite -lGraphicsMagickWand -lGraphicsMagick
	LIBM=-lssl -lcrypto
	LIBLIRC=
	LIBUSB=-lhidapi-hidraw -lhidapi-libusb
endif

ifeq ($(BOARD),5)
#
# RPi#5 (Rev-1): 
# 03 Apr 2018: remove png12, jasper, lcms (not needed anymore).
# 19 May 2018: add PiFace Digital IO board
# 07 Jun 2018: Remove Camera
# 05 Jul 2018: Use custom IO board with robotdyn relays
# 14 May 2021: Add Camera
#
	BOARD_OK=yes
	TARGET=spicam
	COMMON=../common
	DEFS=-D FEATURE_IO_PATRN
	DEFS+=-D FEATURE_USB
	DEFS+=-D FEATURE_CAMERA
	OBJM=cmd_mail.o mail/mail_func.o mail/mail_ssl.o mail/mail_smtp.o mail/mail_pop3.o mail/mail_client.o mail/blowbox.o mail/blowfish.o mail/encode.o mail/crypto.o
	OBJX=gen_usb.o rpi_proxy.o 
	OBJCAM=cam_main.o cam_page.o cam_html.o cam_json.o graphics.o vector.o
	INCCAM=-I/usr/include/GraphicsMagick
	LIBCAM=-ltiff -ljpeg -ljbig -lz -lfreetype -lm -lgomp -lX11 -lXext -lxml2 -lbz2 -lwmflite -lGraphicsMagickWand -lGraphicsMagick
	LIBM=-lssl -lcrypto
	LIBLIRC=
	LIBUSB=-lhidapi-hidraw -lhidapi-libusb
endif

ifeq ($(BOARD),6)
#
# RPi#6 (Rev-1): Rpi TFT Camera
#
	BOARD_OK=yes
	TARGET=rpicam
	COMMON=../common
	DEFS=-D FEATURE_IO_TFTBUTS
	DEFS+=-D FEATURE_CAMERA
	OBJM=
	OBJX=
	OBJCAM=cam_main.o cam_page.o cam_html.o cam_json.o graphics.o vector.o
	INCCAM=-I/usr/include/GraphicsMagick
	LIBCAM=-ltiff -ljpeg -ljbig -lz -lfreetype -lm -lgomp -lX11 -lXext -lxml2 -lbz2 -lwmflite -lGraphicsMagickWand -lGraphicsMagick
	LIBM=
	LIBLIRC=
	LIBUSB=
endif

ifeq ($(BOARD),7)
#
# RPi#7 (Rev-2): PopKodi and Apache web server
#
	BOARD_OK=yes
	TARGET=popkodi
	COMMON=../common
	DEFS=-D FEATURE_IO_NONE
	OBJM=
	OBJX=
	OBJCAM=
	INCCAM=
	LIBCAM=
	LIBM=
	LIBLIRC=-llirc_client 
	LIBUSB=
endif

ifeq ($(BOARD),8)
#
# RPi#8 (Rev-2): Spicam Pond 
#
	BOARD_OK=yes
	TARGET=spicam
	COMMON=../common
	DEFS=-D FEATURE_IO_NONE
	DEFS+=-D FEATURE_CAMERA
	OBJM=cmd_mail.o mail/mail_func.o mail/mail_ssl.o mail/mail_smtp.o mail/mail_pop3.o mail/mail_client.o mail/blowbox.o mail/blowfish.o mail/encode.o mail/crypto.o
	OBJX=gen_usb.o rpi_proxy.o 
	OBJCAM=cam_main.o cam_page.o cam_html.o cam_json.o graphics.o vector.o
	INCCAM=-I/usr/include/GraphicsMagick
	LIBCAM=-ltiff -ljpeg -ljbig -lz -lfreetype -lm -lgomp -lX11 -lXext -lxml2 -lbz2 -lwmflite -lGraphicsMagickWand -lGraphicsMagick
	LIBM=-lssl -lcrypto
	LIBLIRC=
	LIBUSB=
endif

ifeq ($(BOARD),9)
#
# RPi#9 (Rev-3): VPN Server
#
	BOARD_OK=yes
	TARGET=rpivpn
	COMMON=../common
	DEFS=-D FEATURE_IO_NONE
	OBJM=
	OBJX=rpi_proxy.o rpi_comm.o
	OBJCAM=
	INCCAM=
	LIBCAM=
	LIBM=
	LIBLIRC=
	LIBUSB=
endif

ifeq ($(BOARD),10)
#
# RPi#10 (Rev-3): SpiCam HTTP Server
#
	BOARD_OK=yes
	TARGET=spicam
	COMMON=../common
	DEFS=-D FEATURE_IO_NONE
	DEFS+=-D FEATURE_CAMERA
	DEFS+=-D FEATURE_USB
	OBJM=cmd_mail.o mail/mail_func.o mail/mail_ssl.o mail/mail_smtp.o mail/mail_pop3.o mail/mail_client.o mail/blowbox.o mail/blowfish.o mail/encode.o mail/crypto.o
	OBJX=gen_usb.o rpi_proxy.o 
	OBJCAM=cam_main.o cam_page.o cam_html.o cam_json.o graphics.o vector.o
	INCCAM=-I/usr/include/GraphicsMagick
	LIBCAM=-ltiff -ljpeg -ljbig -lz -lfreetype -lm -lgomp -lX11 -lXext -lxml2 -lbz2 -lwmflite -lGraphicsMagickWand -lGraphicsMagick
	LIBM=-lssl -lcrypto
	LIBLIRC=
	LIBUSB=-lhidapi-hidraw -lhidapi-libusb
endif

ifeq ($(BOARD),11)
#
# RPi#11 (Rev-3+): SpiCam HTTP Server
#
	BOARD_OK=yes
	TARGET=spicam
	COMMON=../common
	DEFS=-D FEATURE_IO_NONE
	DEFS+=-D FEATURE_CAMERA
	DEFS+=-D FEATURE_USB
	OBJM=cmd_mail.o mail/mail_func.o mail/mail_ssl.o mail/mail_smtp.o mail/mail_pop3.o mail/mail_client.o mail/blowbox.o mail/blowfish.o mail/encode.o mail/crypto.o
	OBJX=gen_usb.o rpi_proxy.o 
	OBJCAM=cam_main.o cam_page.o cam_html.o cam_json.o graphics.o vector.o
	INCCAM=-I/usr/include/GraphicsMagick
	LIBCAM=-ltiff -ljpeg -ljbig -lz -lfreetype -lm -lgomp -lX11 -lXext -lxml2 -lbz2 -lwmflite -lGraphicsMagickWand -lGraphicsMagick
	LIBM=-lssl -lcrypto
	LIBLIRC=
	LIBUSB=-lhidapi-hidraw -lhidapi-libusb
endif

ifndef BOARD_OK
#
# All other RPi's: 
#
	TARGET=rpicam
	DEFS=-D FEATURE_IO_NONE
	COMMON=../common
	OBJM=
	OBJX=
	OBJCAM=
	INCCAM=
	LIBCAM=
	LIBM=
	LIBLIRC=
	LIBUSB=
endif

CC		= gcc
CFLAGS	= -O2 -Wall -g
DEFS	+= -D BOARD_RPI$(BOARD)
INCDIR	= -I$(COMMON)
INCDIR	+= -I/opt/vc/include
INCDIR	+= -I/opt/vc/include/interface/vmcs_host/linux
INCDIR	+= -I/opt/vc/include/interface/vcos/pthreads -DRPI=1
INCDIR	+= $(INCCAM)
LDFLGS	= -pthread
DESTDIR	= ./bin
LIBFLGS	= -L/usr/local/lib -L/usr/lib -L/opt/vc/lib -L$(COMMON)
LIBS	= -lrt -lwiringPi -lwiringPiDev
LIBS	+= $(LIBCAM) 
LIBS	+= $(LIBM) 
LIBS	+= $(LIBLIRC) 
LIBS	+= $(LIBUSB)
LIBCOM	= -lcommon
DEPS	= globals.h config.h
OBJ	= rpi_main.o \
			rpi_srvr.o rpi_page.o rpi_json.o rpi_func.o rpi_kodi.o rpi_supp.o \
			cmd_main.o cmd_exec.o cmd_alarm.o btn_main.o btn_button.o cmd_cad.o \
			gen_http.o gen_func.o \
			globals.o 
OBJ		+= $(OBJCAM)
OBJ		+= $(OBJM)
OBJ		+= $(OBJX)

debug:	CFLAGS = -Wall -g
debug:	LIBCOM = -lcommondebug -rdynamic
debug:	DEFS += -D FEATURE_USE_PRINTF -D DEBUG_ASSERT
debug:	all

all: modules $(DESTDIR)/$(TARGET)

modules:
$(DESTDIR)/$(TARGET): $(OBJ)
	$(CC) $(CFLAGS) $(DEFS) -o $@ $(OBJ) $(LIBFLGS) $(LIBS) $(LIBCOM) $(LDFLGS)
	strip $(DESTDIR)/$(TARGET)

sync:
	@echo "RPi-$(TARGET) Board=RPi#$(BOARD) $(DEFS)"
	cp -u /mnt/rpi/softw/$(TARGET)/makefile ./
	cp -u /mnt/rpi/softw/$(TARGET)/rebuild ./
	cp -u /mnt/rpi/softw/$(TARGET)/*.c ./
	cp -u /mnt/rpi/softw/$(TARGET)/*.h ./
	cp -u /mnt/rpi/softw/$(TARGET)/mail/*.c mail/
	cp -u /mnt/rpi/softw/$(TARGET)/mail/*.h mail/
	cp -u /mnt/rpi/softw/$(TARGET)/init/$(TARGET).service ./

forcesync:
	@echo "RPi-$(TARGET) Board=RPi#$(BOARD) $(DEFS)"
	cp /mnt/rpi/softw/$(TARGET)/makefile ./
	cp /mnt/rpi/softw/$(TARGET)/rebuild ./
	cp /mnt/rpi/softw/$(TARGET)/*.c ./
	cp /mnt/rpi/softw/$(TARGET)/*.h ./
	cp /mnt/rpi/softw/$(TARGET)/mail/*.c mail/
	cp /mnt/rpi/softw/$(TARGET)/mail/*.h mail/
	cp /mnt/rpi/softw/$(TARGET)/init/$(TARGET).service ./

error:
	cp proj.err /mnt/rpi/softw/$(TARGET)/proj.err

ziplog:
	@echo "Zip logfile: RPi-$(TARGET) Board=RPi#$(BOARD) $(DEFS)"
	sudo gzip -c /usr/local/share/rpi/$(TARGET).log 2>/dev/null >>/usr/local/share/rpi/$(TARGET)logs.gz
	sudo rm /usr/local/share/rpi/$(TARGET).log
	sudo rm /mnt/rpicache/$(TARGET).log

copylog:
	@echo "Copy logfile to PC: RPi-$(TARGET) Board=RPi#$(BOARD) $(DEFS)"
	sudo cp /mnt/rpicache/$(TARGET).log /mnt/rpi/softw/$(TARGET)/$(TARGET).log

sortlog:
	@echo "Sort and copy logfile to PC: RPi-$(TARGET) Board=RPi#$(BOARD) $(DEFS)"
	sudo cat /mnt/rpicache/$(TARGET).log | sort -n >/mnt/rpicache/$(TARGET)sort.log
	sudo cp /mnt/rpicache/$(TARGET)sort.log /mnt/rpi/softw/$(TARGET)/$(TARGET).log
	sudo rm /mnt/rpicache/$(TARGET)sort.log

dellog:
	@echo "Delete logfiles: RPi-$(TARGET) Board=RPi#$(BOARD) $(DEFS)"
	sudo rm /usr/local/share/rpi/$(TARGET).log
	sudo rm /mnt/rpicache/$(TARGET).log

delmap:
	@echo "Delete MAP file: RPi-$(TARGET) Board=RPi#$(BOARD) $(DEFS)"
	sudo rm /usr/local/share/rpi/$(TARGET).map
	sudo rm /mnt/rpicache/$(TARGET).map

clean:
	@echo "RPi-$(TARGET) Board=RPi#$(BOARD) $(DEFS)"
	rm -rf *.o
	rm -rf mail/*.o
	rm -rf $(DESTDIR)/$(TARGET)

install:
	sudo systemctl stop $(TARGET).service
	sudo cp $(DESTDIR)/$(TARGET) /usr/sbin/$(TARGET)
	sudo systemctl restart $(TARGET).service

restart:
	sudo systemctl stop $(TARGET).service
	sudo systemctl restart $(TARGET).service

setup:
	sudo cp /usr/local/share/rpi/$(TARGET).map /mnt/rpicache/$(TARGET).map
	sudo cp /usr/local/share/rpi/$(TARGET).log /mnt/rpicache/$(TARGET).log
	sudo cp $(DESTDIR)/$(TARGET).service /lib/systemd/system/$(TARGET).service

mail/%.o: mail/%.c  $(DEPS)
	$(CC) -o $@ -c $(CFLAGS) $(DEFS) $(INCDIR) $< 

%.o:%.c  $(DEPS)
	$(CC) -o $@ -c $(CFLAGS) $(DEFS) $(INCDIR) $< 
