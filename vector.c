/*  (c) Copyright:  2017..2018  Patrn.nl, Confidential Data
 *
 *  Workfile:           vector.c
 *  Revision:  
 *  Modtime:   
 *
 *  Purpose:            Interface Motion vector functions
 *
 *
 *
 *  Compiler/Assembler: Raspbian Linux GNU gcc
 *  Ext Packages:
 * 
 *  Author:             Peter Hillen
 *
 *  Entry Points:       VEC_InitMotion
 *                      VEC_CenterOfGravity
 *                      VEC_ReadMotionFrame
 *                      VEC_SetMotionBackground
 *                      VEC_SortMotionVectors
 *                      VEC_ExitMotion
 *
 *                      pstMap->G_stMotionDetect.iDebug:
 *                      ===============================================================================================
 *                      0x0001   1 : T&D-File:CSV : Sorted MV Frames - count, y, x, sad, VectY, VectX, Dir
 *                      0x0002   2 : T&D-File     : Put out MV data
 *                      0x0004   4 : T&D-File:CSV : Unsorted ALL Frames - count, y, x, sad, VectY, VectX, Dir
 *                      0x0008   8 : File         : Read MV frames from binary file
 *                      0x0010  16 : STDOUT       : VirtLCD with MV data
 *                      0x0020  32 : File         : Write all MV structs binary
 *                      0x0040  64 : OneShot      : Exit always after 1 frame
 *                      0x0080 128 : File         : Write Graphics RGB levels
 *                      0x0100 256 : Mask         : Do NOT mask frame
 *
 *  Revisions:
 *    24 Feb 2017 Created
 *    10 Jul 2018 Multiple frames in buffer; Add timestamp to VEC_SaveXxx()
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
**/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <math.h>
#include <unistd.h>
#include <sys/types.h>
#include <errno.h>

#include <common.h>
#include "config.h"
#include "globals.h"
#include "gen_func.h"
//
#include "vector.h"

//#define USE_PRINTF
#include <printf.h>

extern const char   *pcMotionInput;
//
// Local prototypes
//
static u_int16 vec_FindMembers               (MVIMG *, u_int16, u_int16);
static char    vec_GetVirtualData            (MVIMG *, int, int);
static void    vec_PutVirtualData            (MVIMG *, int, int, char);
static int     vec_ReadMotionVectorFrame     (MVIMG *, int);
static u_int16 vec_DetermineDirection        (MVEC *);
//
static bool    vec_SortNonZeroMv             (MVIMG *, u_int16);
static bool    vec_SortLoHiNormalMv          (MVIMG *, u_int16);
static bool    vec_SortHiLoNormalMv          (MVIMG *, u_int16);
static bool    vec_SortLoHiWeightMv          (MVIMG *, u_int16);
static bool    vec_SortHiLoWeightMv          (MVIMG *, u_int16);
static bool    vec_SortLoHiClusterId         (MVIMG *, u_int16);
static bool    vec_SortHiLoClusterId         (MVIMG *, u_int16);
static bool    vec_SortLoHiNatural           (MVIMG *, u_int16);
static bool    vec_SortHiLoNatural           (MVIMG *, u_int16);
static bool    vec_SortLoHiSad               (MVIMG *, u_int16);
static bool    vec_SortHiLoSad               (MVIMG *, u_int16);
static bool    vec_SortLoHiXvector           (MVIMG *, u_int16);
static bool    vec_SortHiLoXvector           (MVIMG *, u_int16);
static bool    vec_SortLoHiYvector           (MVIMG *, u_int16);
static bool    vec_SortHiLoYvector           (MVIMG *, u_int16);
//
// Motion vector init values
// CodeSense does NOT allow this statement: it will terminate parsing after this !
//
//static const MVIMG stMvImgDefault =
//{
//   iMotionVectorsHor:     121,         // w=1920: Default number of MVs per screen width
//   iMotionVectorsVer:      67,         // h=1080: Default number of MVs per screen height
//   usAvgBgndSad:            0,         // Average Background SAD
//   usAvgFgndSad:            0,         // Average Forground  SAD
//   usMaxBgndSad:            0          // Maximum Background SAD
//};
//   GEN_MEMCPY(pstMv, &stMvImgDefault, sizeof(MVIMG));
//
static const char *pcMvDirection[NUM_MV_DIR] = 
{
   "--", " W", " S", "SW", " E", "--", "SE", "--",
   " N", "NW", "--", "--", "NE", "--", "--", "--"
};
//
static const char cVirtualScreenSymbols[HISTO_BARS] = 
{
   '0', '1', '2', '3', '4', '5', '6', '7',
   '8', '9', 'a', 'b', 'c', 'd', 'e', 'f'
};
//
static const char    cVirtualScreenSymbolMasked   = MOTION_BORDER;
static const char    cVirtualScreenSymbolEmpty    = ' ';
//
static const char *pcMotionSort[NUM_MVKEYS]  =
{
   "MVKEY_NON_ZERO",
   "MVKEY_LH_NAT",
   "MVKEY_HL_NAT",
   "MVKEY_LH_X",
   "MVKEY_HL_X",
   "MVKEY_LH_Y",
   "MVKEY_HL_Y",
   "MVKEY_LH_SAD",
   "MVKEY_HL_SAD",
   "MVKEY_LH_NMV",
   "MVKEY_HL_NMV",
   "MVKEY_LH_WMV",
   "MVKEY_HL_WMV",
   "MVKEY_LH_CID",
   "MVKEY_HL_CID"
};
//
//  Function:  VEC_InitMotion
//  Purpose:   Init the motion detect process
//
//  Parms:     Motion vector data
//  Returns:   
//  Note:      Allocate bufffer(s)
//
MVIMG *VEC_InitMotion(void)
{
   int      iSize;
   MVIMG   *pstMv;

   pstMv = (MVIMG *) safemalloc(sizeof(MVIMG));
   //
   // Calculate nr of vectors in this frame : CEILING(NumMacroblocks)+1
   //
   // FROM RASPBERRY PI FORUM, Motion vectors = 
   //    (mb_width+1) * mb_height * 4 bytes
   //
   iSize = atoi(pstMap->G_pcWidth);
   pstMv->iMotionVectorsHor = (iSize / MACRO_BLOCK_SIZE) + 1;
   iSize = atoi(pstMap->G_pcHeight);
   pstMv->iMotionVectorsVer = iSize / MACRO_BLOCK_SIZE;
   //
   pstMv->iNumMotionVectors = pstMv->iMotionVectorsHor * pstMv->iMotionVectorsVer;
   pstMv->iCalMotionVectors = 0;
   pstMv->iFrameIdx         = 0;
   //
   //    Create array of Mv groups
   //    Add each motion vector-SAD weight symbol to the array
   //    Allocate buffer to read NUM_FRAMES frames of MVEC's
   //    Allocate buffer for the calculated motion vectors
   //    Allocate buffer to create an image of motion weight symbols
   //
   pstMv->pstFrames        = (MVEC *)   safemalloc(pstMv->iNumMotionVectors * sizeof(MVEC) * NUM_FRAMES);
   pstMv->piVector         = (int *)    safemalloc(pstMv->iNumMotionVectors * sizeof(int));
   pstMv->pusKeys          = (u_int16 *)safemalloc(pstMv->iNumMotionVectors * sizeof(u_int16));
   pstMv->pusClusters      = (u_int16 *)safemalloc(pstMv->iNumMotionVectors * sizeof(u_int16));
   pstMv->pubMask          = (u_int8 *) safemalloc(pstMv->iNumMotionVectors * sizeof(u_int8));
   pstMv->pcVirtual        = (char *)   safemalloc(pstMv->iNumMotionVectors * sizeof(u_int8));
   
   return(pstMv);
}

//
//  Function:  VEC_CopyVirtualData
//  Purpose:   Copy symbol data to virtual LCD
//
//  Parms:     MV Image
//  Returns:   
//  Note:      
//
void VEC_CopyVirtualData(MVIMG *pstMv)
{
   int      iSrcIdx=0, iDstIdx=0, iMvY;

   for(iMvY=0; iMvY<pstMv->iMotionVectorsVer; iMvY++)
   {
      GEN_MEMCPY(&pstMap->G_stVirtLcd.pcVirtLcd[iDstIdx], &pstMv->pcVirtual[iSrcIdx], pstMv->iMotionVectorsHor);
      iSrcIdx += pstMv->iMotionVectorsHor;
      iDstIdx += LCD_VIRTUAL_COLS;
   }
}

//
//  Function:  VEC_GetClusterSize
//  Purpose:   Get the cluster size of a cluster of MVs
//
//  Parms:     Motion vector data, ClusterId
//  Returns:   Size of the cluster
//  Note:      For each of the [iCalMotionVectors] MVs, check how large its cluster is
//             within the distance [usDist] :
//
//             +---+---+---+---+---+---+---+---+---+           Distance
//      Border | X | X | X | X | X | X | X | X | X | Ex: MV  1  2  3  4
//             +---+---+---+---+---+---+---+---+---+    ---  ------------
//             |   |   |   |   |   |   |   |   |   |     A   3  7  9  10
//             +---+---+---+---+---+---+---+---+---+     B   5  9  10 10
//             |   |   |   | A |   |   |   |   |   |     C   7  9  10 10
//             +---+---+---+---+---+---+---+---+---+     D   4  9  10 10
//             |   |   |   | B | C | D |   |   |   |     E   8  10 10 10
//             +---+---+---+---+---+---+---+---+---+     F   6  10 10 10
//             |   |   |   | j | E | F |   |   |   |     G   6  9  10 10
//             +---+---+---+---+---+---+---+---+---+     H   5  9  10 10
//             |   |   |   |   | G |H  |   |   |   |     I   3  6  9  10
//             +---+---+---+---+---+---+---+---+---+     J   6  10 10 10
//             |   |   |   |   | I |   |   |   |   |
//             +---+---+---+---+---+---+---+---+---+
//             |   |   |   |   |   |   |   |   |   |
//             +---+---+---+---+---+---+---+---+---+
//
//
u_int16 VEC_GetClusterSize(MVIMG *pstMv, u_int16 usCid)
{
   MVKEY    tSaveKey;
   u_int16  usMvKey, usIdx,  usCount=0;

   //
   // Check if list needs resorting
   //
   tSaveKey = pstMv->tSorted;
   VEC_SortMotionVectors(pstMv, MVKEY_HL_NMV);
   //
   for(usIdx=0; usIdx<pstMv->iCalMotionVectors; usIdx++)
   {
      //
      // Get cluster index, check ID match
      //
      usMvKey = pstMv->pusKeys[usIdx];
      //PRINTF2("VEC-CountClusters():Cid=%d, Mv=%d" CRLF, usCid, pstMv->pusClusters[usMvKey]);
      if(pstMv->pusClusters[usMvKey] == usCid) usCount++;
   }
   //
   // Put sorting order back as it was
   //
   VEC_SortMotionVectors(pstMv, tSaveKey);
   return(usCount);
}

//
//  Function:  VEC_GetClusterCenter
//  Purpose:   Get the center coords of a cluster of MVs
//
//  Parms:     Motion vector data, Cid
//  Returns:   Coords Idx
//  Note:      X=CoordIdx%HorMvs
//             Y=CoordIdx/HorMvs, 
//
u_int16 VEC_GetClusterCenter(MVIMG *pstMv, u_int16 usCid)
{
   MVKEY    tSaveKey;
   int      iMvX, iMvXmin, iMvXmax;
   int      iMvY, iMvYmin, iMvYmax;
   u_int16  usMvKey, usIdx;

   //
   // Check if list needs resorting
   //
   tSaveKey = pstMv->tSorted;
   VEC_SortMotionVectors(pstMv, MVKEY_HL_NMV);
   //
   iMvXmin = iMvYmin = pstMv->iNumMotionVectors;
   iMvXmax = iMvYmax = 0;
   //
   for(usIdx=0; usIdx<pstMv->iCalMotionVectors; usIdx++)
   {
      //
      // Get cluster index, check ID match
      //
      usMvKey = pstMv->pusKeys[usIdx];
      //PRINTF2("VEC-GetClusterCenter():Cid=%d, Mv=%d" CRLF, usCid, pstMv->pusClusters[usMvKey]);
      if(pstMv->pusClusters[usMvKey] == usCid)
      {
         //
         // This MV does belong to our cluster
         //
         iMvX = usMvKey % pstMv->iMotionVectorsHor;
         iMvY = usMvKey / pstMv->iMotionVectorsHor;
         //
         if(iMvX < iMvXmin)   iMvXmin = iMvX;
         if(iMvX > iMvXmax)   iMvXmax = iMvX;
         if(iMvY < iMvYmin)   iMvYmin = iMvY;
         if(iMvY > iMvYmax)   iMvYmax = iMvY;
      }
   }
   //
   // Calculate center of gravity
   //
   usMvKey = (((iMvYmin+iMvYmax)/2) * pstMv->iMotionVectorsHor) + ((iMvXmin+iMvXmax)/2);
   //
   // Put sorting order back as it was
   //
   VEC_SortMotionVectors(pstMv, tSaveKey);
   return(usMvKey);
}

//
//  Function:  VEC_GetClusterStrength
//  Purpose:   Determine cluster strength 0..100% of max MV
//
//  Parms:     Motion vector data, Cid, Max vector, Depth of MVs from the top of the cluster
//  Returns:   Average Cluster Strength 0..100% = (100*SUM(MVs)/n)/MVmax
//  Note:      iMvMax could be the trigger vanue (pstMdet->iTrigger2) or the max
//             of this cluster.
//
int VEC_GetClusterStrength(MVIMG *pstMv, u_int16 usCid, int iMvMax, int iNr)
{
   MVKEY    tSaveKey;
   int      iMvCur, iMvSum=0, iNum=0;
   u_int16  usMvKey, usIdx;

   //
   // Check if list needs resorting
   //
   tSaveKey = pstMv->tSorted;
   VEC_SortMotionVectors(pstMv, MVKEY_HL_NMV);
   //
   for(usIdx=0; usIdx<pstMv->iCalMotionVectors; usIdx++)
   {
      //
      // Get cluster index, check ID match
      //
      usMvKey = pstMv->pusKeys[usIdx];
      if(pstMv->pusClusters[usMvKey] == usCid)
      {
         if(iNr)
         {
            //
            // This MV does belong to our cluster:
            // Sum the top <iNr> vectors
            //
            iMvCur = pstMv->piVector[usMvKey];
            if(iMvCur > iMvMax) iMvMax = iMvCur;
            iMvSum += iMvCur;
            iNr--;
            iNum++;
         }
         else break;
      }
   }
   //
   // Calculate cluster strength 0..100% of max MV in this cluster
   //
   if(iNum) 
   {
      iMvSum *= 100;
      iMvSum /= iNum;
      iMvSum += (iMvMax/2);   // Round up
      iMvSum /= iMvMax;
   }
   //
   // Put sorting order back as it was
   //
   VEC_SortMotionVectors(pstMv, tSaveKey);
   return(iMvSum);
}

//
//  Function:  VEC_AssignClusterIds
//  Purpose:   Assign a cluster ID to all members of the same cluster
//
//  Parms:     Motion vector data
//  Returns:   
//  Note:      For each of the [iCalMotionVectors] MVs, check how large its cluster is:
//
//             +---+---+---+---+---+---+---+---+---+           
//      Border | X | X | X | X | X | X | X | X | X | Ex: MV  #Members 
//             +---+---+---+---+---+---+---+---+---+    ---  --------
//             |   |   |   |   |   |   |   |   |   |     A     3 
//             +---+---+---+---+---+---+---+---+---+     B     5 
//             |   |   |   | A |   |   |   |   |   |     C     7 
//             +---+---+---+---+---+---+---+---+---+     D     4 
//             |   |   |   | B | C | D |   |   |   |     E     8 
//             +---+---+---+---+---+---+---+---+---+     F     6 
//             |   |   |   | j | E | F |   |   |   |     G     6 
//             +---+---+---+---+---+---+---+---+---+     H     5 
//             |   |   |   |   | G |H  |   |   |   |     I     3 
//             +---+---+---+---+---+---+---+---+---+     J     6 
//             |   |   |   |   | I |   |   |   |   |
//             +---+---+---+---+---+---+---+---+---+
//             |   |   |   |   |   |   |   |   |   |
//             +---+---+---+---+---+---+---+---+---+
//
void VEC_AssignClusterIds(MVIMG *pstMv)
{
   u_int16  usMvPar, usIdx, usCount, usCountTotal;
   u_int16  usCid=1, usMaxClusterSizeCid=0, usMaxClusterSizeIdx=0, usMaxClusterSize=0, usNrOfClusters=0;

   //
   // Sort MVs if needed
   //
   VEC_SortMotionVectors(pstMv, MVKEY_HL_NMV);
   GEN_MEMSET(pstMv->pusClusters, 0, pstMv->iNumMotionVectors * sizeof(u_int16));
   //
   PRINTF2("VEC-AssignClusterIds():%d MVs (Fr=%d)" CRLF, pstMv->iCalMotionVectors, pstMv->iFrameIdx);
   //
   for(usIdx=0; usIdx<pstMv->iCalMotionVectors; usIdx++)
   {
      usCountTotal = 0;
      //
      // Calc current (parent) MV coords
      // If unallocated member: make it the first one
      //
      usMvPar = pstMv->pusKeys[usIdx];
      //
      if(pstMv->pusClusters[usMvPar] == 0)
      {
         pstMv->pusClusters[usMvPar] = usCid;
         usCountTotal++;
      }
      PRINTF2("VEC-AssignClusterIds():Cluster-ID=%d, Parent=%5d" CRLF, usCid, usMvPar);
      //
      usCount       = vec_FindMembers(pstMv, usMvPar, usCid);
      usCountTotal += usCount;
      if(usCountTotal) 
      {
         usNrOfClusters++;
         //
         // This cluster has members: 
         //    update cluster ID
         //    remember the biggest cluster
         //
         if(usCountTotal > usMaxClusterSize) 
         {
            usMaxClusterSize    = usCountTotal;
            usMaxClusterSizeIdx = usMvPar;
            usMaxClusterSizeCid = usCid;
         }
         PRINTF2("VEC-AssignClusterIds():Cluster-ID=%d has %d members" CRLF, usCid, usCountTotal);
         usCid++;
      }
   }
   pstMv->usNrOfClusters     = usNrOfClusters;
   pstMv->usMaxClusterSize   = usMaxClusterSize;
   pstMv->usMaxClusterSizeIdx= usMaxClusterSizeIdx;
   pstMv->usMaxClusterSizeCid= usMaxClusterSizeCid;
}

//
//  Function:  VEC_ReadMotionFrame
//  Purpose:   Read one single motion frame using the cams motion vectors. The optional mask
//             buffer can be used to mask off areas of no interrest.
//  Parms:     motion vector data, motion type
//  Returns:    0 = No Motion
//             >0 = Total number of vectors showing motion
//             -1 = Stopped
//             -2 = Error
//  Note:      First determine the background SAD (Sum of absolute differences) and save this
//             as a future reference.
//
//             If iFd==0, the data comes from the cache buffer, not from the pipeline !
//
int VEC_ReadMotionFrame(MVIMG *pstMv, int iFd)
{
   int      iNumVectors;
   
   if(pstMv == NULL) return(-1);
   //
   //PRINTF("VEC-ReadMotionFrame():" CRLF);
   iNumVectors = vec_ReadMotionVectorFrame(pstMv, iFd);
   switch(iNumVectors)
   {
      case 0:
         //PRINTF("VEC-ReadMotionFrame():Zero motionframes !" CRLF);
         break;

      case -1:
         PRINTF("VEC-ReadMotionFrame():*EOF*" CRLF);
         break;

      case -2:
         PRINTF("VEC-ReadMotionFrame(): ERROR:Read frame" CRLF);
         break;

      default:
         //PRINTF1("VEC-ReadMotionFrame():%d MVs" CRLF, iNumVectors);
         pstMv->iCalMotionVectors = iNumVectors;
         //
         if(pstMap->G_stMotionDetect.iDebug & MM_DEBUG_ONE_SHOT)
         {
            //
            // STOP One Shot
            //
            iNumVectors = -3; 
         }
         break;
   }
   return(iNumVectors);
}

//
//  Function:  VEC_RemoveCluster
//  Purpose:   Remove a cluster of MVs from the list
//
//  Parms:     Motion vector data, Cluster ID
//  Returns:   Number of MVs removed
//  Note:      
//             
//
u_int16 VEC_RemoveCluster(MVIMG *pstMv, u_int16 usCid)
{
   MVKEY    tSaveKey;
   u_int16  usIdx,  usCount=0;
   u_int16 *pusCid=pstMv->pusClusters;
   MVEC    *pstFrame=&pstMv->pstFrames[pstMv->iNumMotionVectors * pstMv->iFrameIdx];
   //
   // Save current sorting key. The list itself does not need to be sorted
   // (we will traverse the whole list) but at the end all deleted frames
   // need to be discarted.
   //
   tSaveKey = pstMv->tSorted;
   for(usIdx=0; usIdx<pstMv->iNumMotionVectors; usIdx++)
   {
      //
      // check ID match
      //
      if(*pusCid == usCid) 
      {
         // Leave SAD alone: it tells us which frame is a boundary
         // pstFrame[usIdx].usSad = 0;
         pstFrame[usIdx].bVectorX = 0;
         pstFrame[usIdx].bVectorY = 0;
         //
         pstMv->piVector[usIdx]  = 0;
         pstMv->pcVirtual[usIdx] = cVirtualScreenSymbolEmpty;
         usCount++;
      }
      pusCid++;
   }
   //
   // Some MVs are no longer in the list: force resorting
   //
   if(usCount)
   {
      pstMv->usNrOfClusters--;
      pstMv->tSorted = MVKEY_NON_ZERO;
      VEC_SortMotionVectors(pstMv, MVKEY_HL_NMV);
      pstMv->iCalMotionVectors -= usCount;
      //
      // Put sorting order back as it was
      //
      VEC_SortMotionVectors(pstMv, tSaveKey);
   }
   return(usCount);
}

//
//  Function:  VEC_SetMotionBackground
//  Purpose:   Determine current motion background noise
//
//  Parms:     Filedescriptor of the vector stream, motion vector data
//  Returns:   pstMv::Avg Background SAD
//  Note:      First determine the background SAD (Sum of absolute differences) and save this
//             as a future reference.
//
void VEC_SetMotionBackground(MVIMG *pstMv, int iFd)
{
   int   i, iNr, iNoiseSum=0;

   if(pstMv == NULL) return;
   //
   // Fill frames buffer and determine the current background motion noise
   //
   PRINTF("VEC-SetMotionBackground():" CRLF);
   for(i=0; i<NUM_BGND_FRAMES; i++) 
   {
      //
      // Read background SAD : enable reading Bgnd SAD
      //
      iNr = vec_ReadMotionVectorFrame(pstMv, iFd);
      if(iNr < 0) break;
      iNoiseSum += (int)pstMv->usCurBgndSad;
   }
   //
   // Save background CEILING(SAD) as reference
   //
   pstMv->usAvgBgndSad = (u_int16) (iNoiseSum / NUM_BGND_FRAMES);
   PRINTF1("VEC-SetMotionBackground():Avg Bgnd SAD=%d" CRLF, pstMv->usAvgBgndSad);
}

//
//  Function:  VEC_SortMotionVectors
//  Purpose:   Bubble-sort the motion vectors to a key
//
//  Parms:     Motion vector data, sorting key
//  Returns:   
//  Note:      The acquisition of all vectors already sorted them in a sequential
//             order, with all non-zero MVs at the top of the list. This means sorting
//             the vectors again on any other method only needs to happen on the actual
//             non-zero MVs (number = iCalMotionVectors)
//             Resoring on the NON_ZERO key must be done on all MVs (number = iNumMotionVectors)
//
void VEC_SortMotionVectors(MVIMG *pstMv, MVKEY tKey)
{
   bool     fSorting=TRUE;
   PFSORT   pfnSort;
   u_int16  usIdx, usTemp;
   u_int16 *pusKeys=pstMv->pusKeys;
   u_int16  usNrMv=pstMv->iCalMotionVectors;

   if(tKey == pstMv->tSorted) return;
   //
   switch(tKey)
   {
      default:
      case MVKEY_NON_ZERO:
         // Non-zero natural order
         PRINTF("VEC-SortMotionVectors(MVKEY_NON_ZERO)" CRLF);
         pfnSort  = vec_SortNonZeroMv;
         usNrMv   = pstMv->iNumMotionVectors;
         break;

      case MVKEY_LH_NAT:
         // Natural order
         PRINTF("VEC-SortMotionVectors(MVKEY_LH_NAT)" CRLF);
         pfnSort  = vec_SortLoHiNatural;
         fSorting = pfnSort(pstMv, 0);
         break;

      case MVKEY_HL_NAT:
         // Natural order
         PRINTF("VEC-SortMotionVectors(MVKEY_HL_NAT)" CRLF);
         pfnSort  = vec_SortHiLoNatural;
         fSorting = pfnSort(pstMv, 0);
         break;

      case MVKEY_LH_X:
         // X motion vector
         PRINTF("VEC-SortMotionVectors(MVKEY_LH_X)" CRLF);
         pfnSort = vec_SortLoHiXvector;
         break;

      case MVKEY_HL_X:
         // X motion vector
         PRINTF("VEC-SortMotionVectors(MVKEY_HL_X)" CRLF);
         pfnSort = vec_SortHiLoXvector;
         break;

      case MVKEY_LH_Y:
         // Y motion vector
         PRINTF("VEC-SortMotionVectors(MVKEY_LH_Y)" CRLF);
         pfnSort = vec_SortLoHiYvector;
         break;

      case MVKEY_HL_Y:
         // Y motion vector
         PRINTF("VEC-SortMotionVectors(MVKEY_HL_Y)" CRLF);
         pfnSort = vec_SortHiLoYvector;
         break;

      case MVKEY_LH_SAD:
         // Sum of abs diffs
         PRINTF("VEC-SortMotionVectors(MVKEY_LH_SAD)" CRLF);
         pfnSort = vec_SortLoHiSad;
         break;

      case MVKEY_HL_SAD:
         // Sum of abs diffs
         PRINTF("VEC-SortMotionVectors(MVKEY_HL_SAD)" CRLF);
         pfnSort = vec_SortHiLoSad;
         break;

      case MVKEY_LH_NMV:
         // Calculated motion vectors
         PRINTF("VEC-SortMotionVectors(MVKEY_LH_NMV)" CRLF);
         pfnSort = vec_SortLoHiNormalMv;
         break;

      case MVKEY_HL_NMV:
         // Calculated motion vectors
         PRINTF("VEC-SortMotionVectors(MVKEY_HL_NMV)" CRLF);
         pfnSort = vec_SortHiLoNormalMv;
         break;

      case MVKEY_LH_WMV:
         // Weighted Calculated motion vectors
         PRINTF("VEC-SortMotionVectors(MVKEY_LH_WMV)" CRLF);
         pfnSort = vec_SortLoHiWeightMv;
         break;

      case MVKEY_HL_WMV:
         // Weighted Calculated motion vectors
         PRINTF("VEC-SortMotionVectors(MVKEY_HL_WMV)" CRLF);
         pfnSort = vec_SortHiLoWeightMv;
         break;

      case MVKEY_LH_CID:
         // ClusterID
         PRINTF("VEC-SortMotionVectors(MVKEY_LH_CID)" CRLF);
         pfnSort = vec_SortLoHiClusterId;
         break;

      case MVKEY_HL_CID:
         // ClusterID
         PRINTF("VEC-SortMotionVectors(MVKEY_HL_CID)" CRLF);
         pfnSort = vec_SortHiLoClusterId;
         break;
   }
   //
   if(usNrMv)
   {
      usNrMv--;
      while(fSorting)
      {
         //
         // Call sorting function depending the key
         //
         fSorting = FALSE;
         //
         for(usIdx=0; usIdx<usNrMv; usIdx++)
         {
            if(pfnSort(pstMv, usIdx))
            {
               //
               // These two keys have to be swapped
               //
               usTemp           = pusKeys[usIdx+0];
               pusKeys[usIdx+0] = pusKeys[usIdx+1];
               pusKeys[usIdx+1] = usTemp;
               fSorting         = TRUE;
            }
         }
      }
   }
   else PRINTF("VEC-SortMotionVectors():ZERO Members !" CRLF);
   //
   pstMv->tSorted = tKey;
   PRINTF("VEC-SortMotionVectors():Done" CRLF);
}

// 
// Function:   VEC_SaveMotionVectors
// Purpose:    Write current MVs to file
// 
// Parameters: Mv Data, caption
// Returns:    
// Note:       
//
void VEC_SaveMotionVectors(MVIMG *pstMv, const char *pcCaption)
{
   int      iMvX, iMvY;
   u_int16  usCid;
   char     cSymbol;
   FILE    *ptFile=NULL;
   MVEC    *pstFrame=&pstMv->pstFrames[pstMv->iNumMotionVectors * pstMv->iFrameIdx];

   //
   // Run one of the debug modes:
   //
   if(pstMap->G_stMotionDetect.iDebug & MM_DEBUG_FILE)
   {
      int   iWeight, iWeightTopX, iSize;
      char *pcTimeStp;
      char *pcPathname;

      iSize      = RTC_ConvertDateTimeSize(TIME_FORMAT_WW_DD_MM_YYYY_HH_MM_SS);
      pcTimeStp  = safemalloc(iSize+1);
      RTC_ConvertDateTime(TIME_FORMAT_WW_DD_MM_YYYY_HH_MM_SS, pcTimeStp, pstMv->ulSecsMotion);
      //
      // T&D file needed: create it
      //
      pcPathname = safemalloc(MAX_PATH_LEN);
      GEN_SNPRINTF(pcPathname, MAX_PATH_LEN, "%s%s.txt", pstMap->G_pcRamDir, (char *)pcMotionInput);
      ptFile     = safefopen(pcPathname, "a+");
      //
      iWeight    = VEC_GetClusterStrength(pstMv, pstMv->usMaxClusterSizeCid, pstMap->G_stMotionDetect.iTrigger2, pstMv->usMaxClusterSize);
      iWeightTopX= VEC_GetClusterStrength(pstMv, pstMv->usMaxClusterSizeCid, pstMap->G_stMotionDetect.iTrigger2, CLUSTER_STRENGTH_COUNT);
      //
      GEN_FPRINTF(ptFile, "VEC-SaveMotionVectors():%s[%d]:%s-Sorted on %s" CRLF,          pcCaption, pstMv->iFrameIdx, pcTimeStp, pcMotionSort[pstMv->tSorted]);
      GEN_FPRINTF(ptFile, "                      Avg-Bgnd-SAD=%5d Max-Bgnd-SAD=%5d" CRLF, pstMv->usAvgBgndSad, pstMv->usMaxBgndSad);
      GEN_FPRINTF(ptFile, "   Min-Fgnd-SAD=%5d Avg-Fgnd-SAD=%5d Max-Fgnd-SAD=%5d" CRLF,   pstMv->usMinFgndSad, pstMv->usAvgFgndSad, pstMv->usMaxFgndSad);
      GEN_FPRINTF(ptFile, "   Nrm-Vector  =%5d Wgt-Vector  =%d" CRLF,                     pstMv->iMaxNormalVector,  pstMv->iMaxWeightVector);
      GEN_FPRINTF(ptFile, "   Hor-Vectors =%5d Ver-Vectors =%d" CRLF,                     pstMv->iMotionVectorsHor, pstMv->iMotionVectorsVer);
      GEN_FPRINTF(ptFile, "   Cal-Vectors =%5d Tot-Vectors =%d" CRLF,                     pstMv->iCalMotionVectors, pstMv->iNumMotionVectors);
      GEN_FPRINTF(ptFile, "   Tot-Count   =%5d Act-Count   =%d" CRLF,                     pstMv->iTotCount, pstMv->iActCount);
      GEN_FPRINTF(ptFile, "   Clusters    =%5d" CRLF,                                     pstMv->usNrOfClusters);
      GEN_FPRINTF(ptFile, "   Weight TopX =%3d%%  Weight All  =%d%%" CRLF,                iWeightTopX, iWeight);
      GEN_FPRINTF(ptFile, CRLF);
      //
      // Put out CSV : count, y, x, sad, VectY, VectX, Dir, vector
      //
      safefree(pcPathname);
      safefree(pcTimeStp);
   }
   //
   // Write the sorted MV as CSV to file
   //
   if(pstMap->G_stMotionDetect.iDebug & MM_DEBUG_FILE_CSV_SRT)
   {
      int         iMv, iVector, iDir, iWeight;
      u_int16     usIdx;
      u_int16    *pusKeys;
      const char *pcDir;
      MVEC       *pstMvec;

      pusKeys = pstMv->pusKeys;
      //
      // Put out data sorted as is
      //
      for(iMv=0; iMv<pstMv->iNumMotionVectors; iMv++)
      {
         usIdx   = *pusKeys++;
         pstMvec = &pstFrame[usIdx];
         iDir    = vec_DetermineDirection(pstMvec);
         pcDir   = pcMvDirection[iDir];
         //
         if(*pcDir != '-')
         {
            //
            // We have a motion vector:
            // Calc original coords (x,y)
            //
            iMvY    = (int)usIdx / pstMv->iMotionVectorsHor;
            iMvX    = (int)usIdx % pstMv->iMotionVectorsHor;
            iVector = pstMv->piVector[usIdx];
            usCid   = pstMv->pusClusters[usIdx];
            iWeight = iVector * (int)pstMvec->usSad;
            //
            GEN_FPRINTF(ptFile, " %5d, %3d, %3d, %5d, %4d, %4d, %s, %5d, %6d, %d" CRLF, 
                  iMv, iMvY, iMvX, pstMvec->usSad, pstMvec->bVectorY, pstMvec->bVectorX, pcDir, iVector, iWeight, usCid);
         }
      }
   }
   //
   // Write the Virt-LCD to file
   //
   if(pstMap->G_stMotionDetect.iDebug & MM_DEBUG_FILE_CSV_ALL)
   {
      int   i;
      char  cChar;

      GEN_FPRINTF(ptFile, "Virt lines=%3d Cols=%3d" CRLF, LCD_VIRTUAL_ROWS,         LCD_VIRTUAL_COLS);
      GEN_FPRINTF(ptFile, "Mvec lines=%3d Cols=%3d" CRLF, pstMv->iMotionVectorsHor, pstMv->iMotionVectorsVer);
      GEN_FPRINTF(ptFile, CRLF);
      //
      // Put out caption
      //
      cChar='0';
      GEN_FPRINTF(ptFile, "    ");
      for(i=0; i<pstMv->iMotionVectorsHor/10; i++)
      {
         GEN_FPRINTF(ptFile, "%c         ", cChar);
         cChar += 1;
         if(cChar > '9') cChar = '0';
      }
      GEN_FPRINTF(ptFile, CRLF);
      //
      cChar='0';
      GEN_FPRINTF(ptFile, "    ");
      for(i=0; i<pstMv->iMotionVectorsHor; i++)
      {
         GEN_FPRINTF(ptFile, "%c", cChar);
         cChar += 1;
         if(cChar > '9') cChar = '0';
      }
      GEN_FPRINTF(ptFile, CRLF);
      //
      for(iMvY=0; iMvY<pstMv->iMotionVectorsVer; iMvY++)
      {
         GEN_FPRINTF(ptFile, "%02d [", iMvY);
         for(iMvX=0; iMvX<pstMv->iMotionVectorsHor; iMvX++)
         {
            cSymbol = vec_GetVirtualData(pstMv, iMvX, iMvY);
            GEN_FPRINTF(ptFile, "%c", cSymbol);
         }
         GEN_FPRINTF(ptFile, "]" CRLF);
      }
      GEN_FPRINTF(ptFile, CRLF CRLF);
   }
   //
   // Write the ALL unsorted MVs as CSV to file
   //
   if(pstMap->G_stMotionDetect.iDebug & MM_DEBUG_FILE_CSV_UNS)
   {
      int         iMv=0, iVector, iWeight, iDir;
      const char *pcDir;
      int        *piVector=pstMv->piVector;
      u_int16    *pusClusters=pstMv->pusClusters;
      MVEC       *pstMvec=pstFrame;

      GEN_FPRINTF(ptFile, "Motion Count   =%d" CRLF CRLF, pstMap->G_stMotionDetect.iCounter);
      //
      // Put out ALL MV data
      //
      for(iMvY=0; iMvY<pstMv->iMotionVectorsVer; iMvY++)
      {
         for(iMvX=0; iMvX<pstMv->iMotionVectorsHor; iMvX++)
         {
            iDir    = vec_DetermineDirection(pstMvec);
            pcDir   = pcMvDirection[iDir];
            iVector = *piVector++;
            usCid   = *pusClusters++;
            iWeight = iVector * (int)pstFrame->usSad;
            //
            GEN_FPRINTF(ptFile, " %5d, %3d, %3d, %5d, %4d, %4d, %s, %5d, %6d, %d" CRLF, 
                           iMv, iMvY, iMvX, pstFrame->usSad, pstFrame->bVectorY, pstFrame->bVectorX, pcDir, iVector, iWeight, usCid);
            iMv++;
            pstMvec++;
         }
      }
      GEN_FPRINTF(ptFile, "" CRLF);
   }
   //
   // Put Virt-LCD out to STROUT
   //
   if(pstMap->G_stMotionDetect.iDebug & MM_DEBUG_OUT_LCD)
   {
      for(iMvY=0; iMvY<pstMv->iMotionVectorsVer; iMvY++)
      {
         GEN_Printf("[");
         for(iMvX=0; iMvX<pstMv->iMotionVectorsHor; iMvX++)
         {
            cSymbol = vec_GetVirtualData(pstMv, iMvX, iMvY);
            GEN_Printf("%c", cSymbol);
         }
         GEN_Printf("]" CRLF);
      }
   }
   if(ptFile) safefclose(ptFile);
}

//
//  Function:  VEC_SkipMotionFrame
//  Purpose:   Skip a single frame of MV data
//
//  Parms:     Filedescriptor of the vector stream, motion vector data
//  Returns:   
//  Note:      
//
void VEC_SkipMotionFrame(MVIMG *pstMv, int iFd)
{
   int   iMvX, iMvY;
   MVEC  stFrame;

   if(pstMv == NULL) return;
   //
   PRINTF("VEC-SkipMotionFrame():" CRLF);
   pstMv->iFrameNr++;
   //
   for(iMvY=0; iMvY<pstMv->iMotionVectorsVer; iMvY++)
   {
      for(iMvX=0; iMvX<pstMv->iMotionVectorsHor; iMvX++)
      {
         saferead(iFd, &stFrame, sizeof(MVEC));
      }
   }
}

//
//  Function:  VEC_ExitMotion
//  Purpose:   Exit the motion detect process
//
//  Parms:     Motion vector data
//  Returns:   
//  Note:      
//
void VEC_ExitMotion(MVIMG *pstMv)
{
   //
   // Free all motion buffers
   //
   if(pstMv)
   {
      safefree(pstMv->pcVirtual);
      safefree(pstMv->pubMask);
      safefree(pstMv->pusClusters);
      safefree(pstMv->pusKeys);
      safefree(pstMv->piVector);
      safefree(pstMv->pstFrames);
      //
      safefree(pstMv);
   }
}

/*------  Local functions separator ------------------------------------
__Local_Functions_(){};
----------------------------------------------------------------------------*/

//
//  Function:  vec_FindMembers
//  Purpose:   Find all members of the same cluster
//
//  Parms:     Motion vector data, parent idx, cluster ID
//  Returns:   Number of members in this cluster
//  Note:      
//
static u_int16 vec_FindMembers(MVIMG *pstMv, u_int16 usMvPar, u_int16 usCid)
{
   int      iMvParY, iMvParX;
   int      iMvMemY, iMvMemX;
   u_int16  usCount=0, usMvMem, usIdx;
   char     cSymbol;
   u_int16 *pusKeys;

   //
   // Calc Parent MV (X,Y) coords
   //
   iMvParX = usMvPar % pstMv->iMotionVectorsHor;
   iMvParY = usMvPar / pstMv->iMotionVectorsHor;
   //
   pusKeys = pstMv->pusKeys;
   //
   //PRINTF4("vec-FindMember():Cluster-ID=%d, Par=%5d(%3d, %3d)" CRLF, usCid, usMvPar, iMvParX, iMvParY);
   //
   for(usIdx=0; usIdx<pstMv->iCalMotionVectors; usIdx++)
   {
      usMvMem = *pusKeys++;
      iMvMemX = usMvMem % pstMv->iMotionVectorsHor;
      iMvMemY = usMvMem / pstMv->iMotionVectorsHor;
      //
      cSymbol = vec_GetVirtualData(pstMv, iMvMemX, iMvMemY);
      if( (cSymbol != cVirtualScreenSymbolEmpty) && (cSymbol != cVirtualScreenSymbolMasked) ) 
      {
         if( (abs(iMvMemY-iMvParY) <= 1) && (abs(iMvMemX-iMvParX) <= 1) )
         {
            //
            // This MV is a neighbour, and therefor part of the cluster:
            //    o Mark the cluster
            //       = 0 : Init
            //       < 0 : Deleted
            //       > 0 : Marked with ID 1...32767
            //
            // Remember any non-marked cluster member
            //
            if(pstMv->pusClusters[usMvMem] == 0)
            {
               //
               // This is a new member: recursively check its members
               //
               PRINTF4("vec-FindMember():Cluster-ID=%d, Mem=%5d(%3d, %3d)" CRLF, usCid, usMvMem, iMvMemX, iMvMemY);
               pstMv->pusClusters[usMvMem] = usCid;
               usCount++;
               usCount += vec_FindMembers(pstMv, usMvMem, usCid);
            }
         }
      }
      else
      {
         PRINTF4("vec-FindMember():Cluster-ID=%d, Mem=%5d(%3d, %3d) BAD SYMBOL HERE !!!!" CRLF, usCid, usMvMem, iMvMemX, iMvMemY);
      }
   }
   return(usCount);
}

//
//  Function:  vec_GetVirtualData
//  Purpose:   Get virtual data
//
//  Parms:     MV Image, Line, Col
//  Returns:   Data or the border designator
//  Note:      
//
static char vec_GetVirtualData(MVIMG *pstMv, int iCol, int iRow)
{
   int      iMv;

   if(iCol < 0)                 return(MOTION_BORDER);
   if(iRow < 0)                 return(MOTION_BORDER);
   
   if(iCol >= pstMv->iMotionVectorsHor) return(MOTION_BORDER);
   if(iRow >= pstMv->iMotionVectorsVer) return(MOTION_BORDER);
   //
   iMv = (iRow * pstMv->iMotionVectorsHor) + iCol;

   //
   return(pstMv->pcVirtual[iMv]);
}

//
//  Function:  vec_PutVirtualData
//  Purpose:   Put virtual data
//
//  Parms:     MV Image, Line, Col, data
//  Returns:   
//  Note:      
//
static void vec_PutVirtualData(MVIMG *pstMv, int iCol, int iRow, char cData)
{
   int      iMv;

   if(iCol >= pstMv->iMotionVectorsHor) return;
   if(iRow >= pstMv->iMotionVectorsVer) return;
   //
   iMv = (iRow * pstMv->iMotionVectorsHor) + iCol;

   pstMv->pcVirtual[iMv] = cData;
}

//
//  Function:  vec_ReadMotionVectorFrame
//  Purpose:   Read one whole frame into the buffer
//
//  Parms:     File read descr, MV data
//  Returns:   -1 on EOF, -2 on error, else number of motion vectors <> 0
//  Note:      If iFd==0, the data comes from the cache buffer, not from the pipeline !
//
static int vec_ReadMotionVectorFrame(MVIMG *pstMv, int iFd)
{
   u_int16  usMaxBgndSad=0;
   u_int16  usMinFgndSad=65535, usMaxFgndSad=0;
   int      iMaxNormalVector=0;
   int      iMaxWeightVector=0;
   int      iBgndVectors=0, iFgndVectors=0;
   int      iBgndSumSad=0,  iFgndSumSad=0;
   int      iRead, iMvX, iMvY;
   int      iVector, iIdx;
   int     *piVector;
   float    flVector;
   u_int16  usIdxKey=0, usIdxTop, usIdxBot;       
   u_int16  usIdxMaxNormalVector=0, usIdxMaxWeightVector=0;       
   u_int8  *pubMask=pstMv->pubMask;
   MVEC    *pstFrame=&pstMv->pstFrames[pstMv->iNumMotionVectors * pstMv->iFrameIdx];

   //PRINTF1("vec-ReadMotionVectorFrame():Frame=%d" CRLF, pstMv->iFrameNr);
   //
   // Setup frame acquisition
   //
   pstMv->iFrameNr++;
   pstMv->iTotCount = 0;
   pstMv->iActCount = 0;
   pstMv->tSorted   = MVKEY_NON_ZERO;
   piVector         = pstMv->piVector;
   //
   // Build index list for the new frame: All actual vectors at the top, all zero
   // vectors at the bottom: Sorting can be limitted to the actual non-zero vectors
   //
   usIdxTop = 0;
   usIdxBot = pstMv->iNumMotionVectors - 1;
   //
   for(iMvY=0; iMvY<pstMv->iMotionVectorsVer; iMvY++)
   {
      for(iMvX=0; iMvX<pstMv->iMotionVectorsHor; iMvX++)
      {
         if(iFd)  iRead = saferead(iFd, pstFrame, sizeof(MVEC));
         else     iRead = sizeof(MVEC);
         //
         if(iRead == sizeof(MVEC))
         {
            pstMv->iTotCount++;
            //
            // Optionally mask off areas of no interrest
            //
            iVector = 0;
            if(pubMask[usIdxKey])
            {
               pstMv->iActCount++;
               //
               // Only consider those MB who are NOT masked AND have an actual vector
               //
               if(pstFrame->bVectorX || pstFrame->bVectorY)
               {
                  //PRINTF6("vec-ReadMotionVectorFrame():V=%5d : (%4d,%4d) X=%3d Y=%3d SAD=%d" CRLF, pstMv->iTotCount, iMvX, iMvY, pstFrame->bVectorX, pstFrame->bVectorY, pstFrame->usSad);
                  //
                  // Calculate SAD * motion vector
                  // Remember max and current foreground SAD
                  //
                  iFgndSumSad += (int)pstFrame->usSad;
                  iFgndVectors++;
                  if(pstFrame->usSad > usMaxFgndSad) usMaxFgndSad = pstFrame->usSad;
                  if(pstFrame->usSad < usMinFgndSad) usMinFgndSad = pstFrame->usSad;
                  //
                  //  iVector := Normal   vector = sqrt(X^2+Y^2)
                  // flVector := Weigthed vector = sqrt(X^2+Y^2) * SAD
                  //
                  flVector = sqrt(powf((float) pstFrame->bVectorX, 2) + powf((float) pstFrame->bVectorY, 2));
                  iVector  = (int) flVector;
                  iIdx     = iVector / MAX_VECTOR_INDEX;
                  //
                  vec_PutVirtualData(pstMv, iMvX, iMvY, cVirtualScreenSymbols[iIdx]);
                  //
                  flVector *= (float)pstFrame->usSad;
                  //
                  // Save maximum normal   motion vector
                  //              weighted motion vector
                  //
                  if(iVector > iMaxNormalVector)         { iMaxNormalVector = iVector;        usIdxMaxNormalVector = usIdxKey; }
                  if((int) flVector > iMaxWeightVector)  { iMaxWeightVector = (int) flVector; usIdxMaxWeightVector = usIdxKey; }
                  //
                  // Put all actual vectors at the top, but still unsorted !
                  //
                  pstMv->pusKeys[usIdxTop] = usIdxKey++;
                  usIdxTop++;
               }
               else
               {
                  //
                  // MB has not changed.
                  // Remember max and current background SAD
                  //
                  iBgndSumSad += pstFrame->usSad;
                  iBgndVectors++;
                  if(pstFrame->usSad > usMaxBgndSad)  usMaxBgndSad = pstFrame->usSad;
                  vec_PutVirtualData(pstMv, iMvX, iMvY, cVirtualScreenSymbolEmpty);
                  //
                  // Put all zero vectors at the bottom, but still unsorted !
                  //
                  pstMv->pusKeys[usIdxBot] = usIdxKey++;
                  usIdxBot--;
               }
            }
            else
            {
               //
               // This MB is masked and therefor not of interrest: Put the sampla at the bottom of the
               // sorting list
               //
               pstFrame->bVectorX = 0;
               pstFrame->bVectorY = 0;
               vec_PutVirtualData(pstMv, iMvX, iMvY, cVirtualScreenSymbolMasked);
               //
               // Put all zero vectors at the bottom, but still unsorted !
               //
               pstMv->pusKeys[usIdxBot] = usIdxKey++;
               usIdxBot--;
            }
            *piVector++ = iVector;
            pstFrame++;
         }
         else if(iRead == 0)
         {
            PRINTF("vec-ReadMotionVectorFrame():**EOF**" CRLF);
            return(-1);
         }
         else
         {
            PRINTF2("vec-ReadMotionVectorFrame():Error reading Fd %d:%s" CRLF, iFd, strerror(errno));
            return(-2);
         }
      }
   }
   if(iBgndVectors) 
   {
      pstMv->usCurBgndSad = (u_int16) (iBgndSumSad / iBgndVectors);
      pstMv->usMaxBgndSad = usMaxBgndSad;
   }
   if(iFgndVectors) 
   {
      pstMv->usMinFgndSad = usMinFgndSad;
      pstMv->usMaxFgndSad = usMaxFgndSad;
      pstMv->usAvgFgndSad = (u_int16) (iFgndSumSad / iFgndVectors);
   }
   pstMv->iMaxNormalVector     = iMaxNormalVector;
   pstMv->iMaxWeightVector     = iMaxWeightVector;
   pstMv->usIdxMaxNormalVector = usIdxMaxNormalVector;
   pstMv->usIdxMaxWeightVector = usIdxMaxWeightVector;
   //
   //PRINTF1("vec-ReadMotionVectorFrame():Frame read: Total=%d" CRLF, pstMv->iTotCount++);
   return(iFgndVectors);
}

// 
// Function    : vec_DetermineDirection
// Description : Determine the MV direction
//
// Parameters  : 
// Returns     : Direction index 0..15
// 
static u_int16 vec_DetermineDirection(MVEC *pstMvec)
{
   u_int16 usMvDir=0;

   if(pstMvec->bVectorX < 0) usMvDir |= MV_DIR_W; 
   if(pstMvec->bVectorX > 0) usMvDir |= MV_DIR_E; 
   if(pstMvec->bVectorY < 0) usMvDir |= MV_DIR_S; 
   if(pstMvec->bVectorY > 0) usMvDir |= MV_DIR_N;
   //
   return(usMvDir);
}


/*------  Local functions separator ------------------------------------
__Sort_Functions_(){};
----------------------------------------------------------------------------*/

//
//  Function:  vec_SortNonZeroMv
//  Purpose:   Sort on non-zero normal motion vectors (Natural)
//             SQRT(X^2+Y^2)
//  Parms:     Motion vector data, current index
//  Returns:   Result of the sort (needs swap = TRUE)
//  Note:      MVKEY_NON_ZERO
//             This sorting methode will sort ALL MVs !!!
//
static bool vec_SortNonZeroMv(MVIMG *pstMv, u_int16 usIdx)
{
   u_int16  usIdx1, usIdx2;
   u_int16 *pusKeys=&pstMv->pusKeys[usIdx];
   int     *piList = pstMv->piVector;

   usIdx1 = *pusKeys++;
   usIdx2 = *pusKeys;
   //
   return(piList[usIdx1] < piList[usIdx2]);
}

//
//  Function:  vec_SortLoHiNormalMv
//  Purpose:   Sort on normal motion vectors (Asc)
//             SQRT(X^2+Y^2)
//  Parms:     Motion vector data, current index
//  Returns:   Result of the sort (needs swap = TRUE)
//  Note:      MVKEY_LH_NMV
//             This sorting methode will sort only the non-zero MVs !!!
//
static bool vec_SortLoHiNormalMv(MVIMG *pstMv, u_int16 usIdx)
{
   u_int16  usIdx1, usIdx2;
   u_int16 *pusKeys=&pstMv->pusKeys[usIdx];
   int     *piList = pstMv->piVector;

   usIdx1 = *pusKeys++;
   usIdx2 = *pusKeys;
   //
   return(piList[usIdx1] > piList[usIdx2]);
}

//
//  Function:  vec_SortHiLoNormalMv
//  Purpose:   Sort on normal motion vectors (Desc)
//             SQRT(X^2+Y^2)
//  Parms:     Motion vector data, current index
//  Returns:   Result of the sort (needs swap = TRUE)
//  Note:      MVKEY_HL_NMV
//             This sorting methode will sort only the non-zero MVs !!!
//
static bool vec_SortHiLoNormalMv(MVIMG *pstMv, u_int16 usIdx)
{
   u_int16  usIdx1, usIdx2;
   u_int16 *pusKeys=&pstMv->pusKeys[usIdx];
   int     *piList = pstMv->piVector;

   usIdx1 = *pusKeys++;
   usIdx2 = *pusKeys;
   //
   return(piList[usIdx1] < piList[usIdx2]);
}

//
//  Function:  vec_SortLoHiWeightMv
//  Purpose:   Sort on weighted motion vectors (Asc)
//             SAD*SQRT(X^2+Y^2)
//  Parms:     Motion vector data, current index
//  Returns:   Result of the sort (needs swap = TRUE)
//  Note:      MVKEY_LH_NMV
//             This sorting methode will sort only the non-zero MVs !!!
//
static bool vec_SortLoHiWeightMv(MVIMG *pstMv, u_int16 usIdx)
{
   u_int16  usIdx1, usIdx2;
   u_int16 *pusKeys=&pstMv->pusKeys[usIdx];
   int     *piList=pstMv->piVector;
   int      iWeight1, iWeight2;
   MVEC    *pstFrame=&pstMv->pstFrames[pstMv->iNumMotionVectors * pstMv->iFrameIdx];

   usIdx1 = *pusKeys++;
   usIdx2 = *pusKeys;
   //
   iWeight1 = piList[usIdx1] * (int)pstFrame[usIdx1].usSad;
   iWeight2 = piList[usIdx2] * (int)pstFrame[usIdx2].usSad;
   return(iWeight1 > iWeight2);
}

//
//  Function:  vec_SortHiLoWeightMv
//  Purpose:   Sort on weighted motion vectors (Desc)
//             SAD*SQRT(X^2+Y^2)
//  Parms:     Motion vector data, current index
//  Returns:   Result of the sort (needs swap = TRUE)
//  Note:      MVKEY_HL_WMV
//             This sorting methode will sort only the non-zero MVs !!!
//
static bool vec_SortHiLoWeightMv(MVIMG *pstMv, u_int16 usIdx)
{
   u_int16  usIdx1, usIdx2;
   u_int16 *pusKeys=&pstMv->pusKeys[usIdx];
   int     *piList=pstMv->piVector;
   int      iWeight1, iWeight2;
   MVEC    *pstFrame=&pstMv->pstFrames[pstMv->iNumMotionVectors * pstMv->iFrameIdx];

   usIdx1 = *pusKeys++;
   usIdx2 = *pusKeys;
   //
   iWeight1 = piList[usIdx1] * (int)pstFrame[usIdx1].usSad;
   iWeight2 = piList[usIdx2] * (int)pstFrame[usIdx2].usSad;
   return(iWeight1 < iWeight2);
}

//
//  Function:  vec_SortLoHiClusterId
//  Purpose:   Sort on cluster ID
//
//  Parms:     Motion vector data, current index
//  Returns:   Result of the sort (needs swap = TRUE)
//  Note:      MVKEY_LH_CID
//             CID = 0 Should remain at the bottom of the list always !
//             This sorting methode will sort only the non-zero MVs !!!
//
static bool vec_SortLoHiClusterId(MVIMG *pstMv, u_int16 usIdx)
{
   u_int16  usIdx1, usIdx2;
   u_int16 *pusKeys=&pstMv->pusKeys[usIdx];

   usIdx1 = *pusKeys++;
   usIdx2 = *pusKeys;
   //
   if(pstMv->pusClusters[usIdx2] == 0) return(FALSE);
   if(pstMv->pusClusters[usIdx1] == 0) return(TRUE);
   //
   return(pstMv->pusClusters[usIdx1] > pstMv->pusClusters[usIdx2]);
}

//
//  Function:  vec_SortHiLoClusterId
//  Purpose:   Sort on cluster ID
//
//  Parms:     Motion vector data, current index
//  Returns:   Result of the sort (needs swap = TRUE)
//  Note:      MVKEY_HL_CID
//             CID = 0 Should remain at the bottom of the list always !
//             This sorting methode will sort only the non-zero MVs !!!
//
static bool vec_SortHiLoClusterId(MVIMG *pstMv, u_int16 usIdx)
{
   u_int16  usIdx1, usIdx2;
   u_int16 *pusKeys=&pstMv->pusKeys[usIdx];

   usIdx1 = *pusKeys++;
   usIdx2 = *pusKeys;
   //
   return(pstMv->pusClusters[usIdx1] < pstMv->pusClusters[usIdx2]);
}

//
//  Function:  vec_SortLoHiNatural
//  Purpose:   Sort natural order
//
//  Parms:     Motion vector data, current index
//  Returns:   FALSE (list is already OK
//  Note:      MVKEY_LH_NAT
//             This sorting methode will sort only the non-zero MVs !!!
//
static bool vec_SortLoHiNatural(MVIMG *pstMv, u_int16 usIdx)
{
   u_int16  usX, usNum;
   u_int16 *pusKeys=pstMv->pusKeys;

   usNum = pstMv->iNumMotionVectors - usIdx;
   //
   for(usX=0; usX<usNum; usX++)
   {
      *pusKeys++ = usIdx++;
   }
   return(FALSE);
}

//
//  Function:  vec_SortHiLoNatural
//  Purpose:   Sort natural order
//
//  Parms:     Motion vector data, current index
//  Returns:   FALSE (list is already OK
//  Note:      MVKEY_HL_NAT
//             This sorting methode will sort only the non-zero MVs !!!
//
static bool vec_SortHiLoNatural(MVIMG *pstMv, u_int16 usIdx)
{
   u_int16  usX, usNum;
   u_int16 *pusKeys=pstMv->pusKeys;

   usNum = pstMv->iNumMotionVectors - usIdx;
   usIdx = usNum;
   //
   for(usX=0; usX<usNum; usX++)
   {
      *pusKeys++ = --usIdx;
   }
   return(FALSE);
}

//
//  Function:  vec_SortLoHiSad
//  Purpose:   Sort on SAD (Asc)
//
//  Parms:     Motion vector data, current index
//  Returns:   Result of the sort (needs swap = TRUE)
//  Note:      MVKEY_LH_SAD
//             This sorting methode will sort only the non-zero MVs !!!
//
static bool vec_SortLoHiSad(MVIMG *pstMv, u_int16 usIdx)
{
   u_int16  usIdx1, usIdx2;
   u_int16 *pusKeys=&pstMv->pusKeys[usIdx];
   MVEC    *pstFrame=&pstMv->pstFrames[pstMv->iNumMotionVectors * pstMv->iFrameIdx];

   usIdx1 = *pusKeys++;
   usIdx2 = *pusKeys;
   //
   return(pstFrame[usIdx1].usSad > pstFrame[usIdx2].usSad);
}

//
//  Function:  vec_SortHiLoSad
//  Purpose:   Sort on SAD (Desc)
//
//  Parms:     Motion vector data, current index
//  Returns:   Result of the sort (needs swap = TRUE)
//  Note:      MVKEY_HL_SAD
//             This sorting methode will sort only the non-zero MVs !!!
//
static bool vec_SortHiLoSad(MVIMG *pstMv, u_int16 usIdx)
{
   u_int16  usIdx1, usIdx2;
   u_int16 *pusKeys=&pstMv->pusKeys[usIdx];
   MVEC    *pstFrame=&pstMv->pstFrames[pstMv->iNumMotionVectors * pstMv->iFrameIdx];

   usIdx1 = *pusKeys++;
   usIdx2 = *pusKeys;
   //
   return(pstFrame[usIdx1].usSad < pstFrame[usIdx2].usSad);
}

//
//  Function:  vec_SortLoHiXvector
//  Purpose:   Sort on X vector (Asc)
//
//  Parms:     Motion vector data, current index
//  Returns:   Result of the sort (needs swap = TRUE)
//  Note:      MVKEY_LH_X
//             This sorting methode will sort only the non-zero MVs !!!
//
static bool vec_SortLoHiXvector(MVIMG *pstMv, u_int16 usIdx)
{
   u_int16  usIdx1, usIdx2;
   u_int16 *pusKeys=&pstMv->pusKeys[usIdx];
   MVEC    *pstFrame=&pstMv->pstFrames[pstMv->iNumMotionVectors * pstMv->iFrameIdx];

   usIdx1 = *pusKeys++;
   usIdx2 = *pusKeys;
   //
   return(pstFrame[usIdx1].bVectorX > pstFrame[usIdx2].bVectorX);
}

//
//  Function:  vec_SortHiLoXvector
//  Purpose:   Sort on X vector (Desc)
//
//  Parms:     Motion vector data, current index
//  Returns:   Result of the sort (needs swap = TRUE)
//  Note:      MVKEY_HL_X
//             This sorting methode will sort only the non-zero MVs !!!
//
static bool vec_SortHiLoXvector(MVIMG *pstMv, u_int16 usIdx)
{
   u_int16  usIdx1, usIdx2;
   u_int16 *pusKeys=&pstMv->pusKeys[usIdx];
   MVEC    *pstFrame=&pstMv->pstFrames[pstMv->iNumMotionVectors * pstMv->iFrameIdx];

   usIdx1 = *pusKeys++;
   usIdx2 = *pusKeys;
   //
   return(pstFrame[usIdx1].bVectorX < pstFrame[usIdx2].bVectorX);
}

//
//  Function:  vec_SortLoHiYvector
//  Purpose:   Sort on Y vector (Asc)
//
//  Parms:     Motion vector data, current index
//  Returns:   Result of the sort (needs swap = TRUE)
//  Note:      MVKEY_LH_Y
//             This sorting methode will sort only the non-zero MVs !!!
//
static bool vec_SortLoHiYvector(MVIMG *pstMv, u_int16 usIdx)
{
   u_int16  usIdx1, usIdx2;
   u_int16 *pusKeys=&pstMv->pusKeys[usIdx];
   MVEC    *pstFrame=&pstMv->pstFrames[pstMv->iNumMotionVectors * pstMv->iFrameIdx];

   usIdx1 = *pusKeys++;
   usIdx2 = *pusKeys;
   //
   return(pstFrame[usIdx1].bVectorY > pstFrame[usIdx2].bVectorY);
}

//
//  Function:  vec_SortHiLoYvector
//  Purpose:   Sort on Y vector (Desc)
//
//  Parms:     Motion vector data, current index
//  Returns:   Result of the sort (needs swap = TRUE)
//  Note:      MVKEY_HL_Y
//             This sorting methode will sort only the non-zero MVs !!!
//
static bool vec_SortHiLoYvector(MVIMG *pstMv, u_int16 usIdx)
{
   u_int16  usIdx1, usIdx2;
   u_int16 *pusKeys=&pstMv->pusKeys[usIdx];
   MVEC    *pstFrame=&pstMv->pstFrames[pstMv->iNumMotionVectors * pstMv->iFrameIdx];

   usIdx1 = *pusKeys++;
   usIdx2 = *pusKeys;
   //
   return(pstFrame[usIdx1].bVectorY < pstFrame[usIdx2].bVectorY);
}

/*------  Local functions separator ------------------------------------
__Debug_Functions_(){};
----------------------------------------------------------------------------*/


/*------  Local functions separator ------------------------------------
__DELETED_FUNCTIONS_________(){};
----------------------------------------------------------------------------*/

#ifdef COMMENT

//
//  Function:  VEC_CountVectorCluster
//  Purpose:   Count the number of proximate (cluster of) vectors 
//
//  Parms:     Motion vector data, parent vector, max distance (pixels sqare), ptr to index array, array size
//  Returns:   Number of vectors 1...?
//  Note:      Max dist in square pixels: calculate max radius
//             Include this center vector
//
u_int16 VEC_CountVectorCluster(MVIMG *pstMv, u_int16 usCurrIdx, u_int16 usMaxDist, u_int16 *pusProx, u_int16 usNrProx)
{
   int      iMvYc, iMvXc;
   int      iMvYn, iMvXn;
   float    flDist, flMaxDist;
   u_int16  usKey, usTmp, usCurrCount;
   u_int16 *pusKeys;
   u_int16  usNrMv=pstMv->iCalMotionVectors;

   flMaxDist = sqrt( (2 * powf((float)(usMaxDist-1), 2)) ) / 2;
   //
   // Calc current MV coords
   //
   usKey = pstMv->pusKeys[usCurrIdx];
   iMvYc = usKey / pstMv->iMotionVectorsHor;
   iMvXc = usKey % pstMv->iMotionVectorsHor;
   //
   pusKeys     = pstMv->pusKeys;
   usCurrCount = 1;
   //
   for(usTmp=0; usTmp<usNrMv; usTmp++)
   {
      usKey = *pusKeys++;
      if(usKey != pstMv->pusKeys[usCurrIdx])
      {
         //
         // Calc MV coords
         //
         iMvYn = usKey / pstMv->iMotionVectorsHor;
         iMvXn = usKey % pstMv->iMotionVectorsHor;
         //
         flDist = sqrt( powf((float)(iMvYc-iMvYn), 2) + powf((float)(iMvXc-iMvXn), 2) );
         if(flDist <= flMaxDist)
         {
            //
            // This MV is within range of the assumed center: count it and store it (if required)
            //
            usCurrCount++;
            if(pusProx && usNrProx)
            {
               *pusProx++ = usKey;
               usNrProx--; 
            }
         }
         //LOG_printf("VEC-CountVectorCluster():usCurrIdx=%d usNrMv=%d iMvYc=%2d iMvXc=%2d iMvYn=%2d iMvXn=%2d flDist=%2.5f flMaxDist=%2.5f usCurrCount=%d" CRLF, 
         //               usCurrIdx, usNrMv, iMvYc, iMvXc, iMvYn, iMvXn, flDist, flMaxDist, usCurrCount);
      }
      else
      {
         //
         // This is the parent vector, already counted as 1st
         //

         //LOG_printf("VEC-CountVectorCluster():usCurrIdx=%d usNrMv=%d iMvYc=%2d iMvXc=%2d iMvYn=-- iMvXn=-- flDist=--.----- flMaxDist=%2.5f usCurrCount=%d" CRLF, 
         //               usCurrIdx, usNrMv, iMvYc, iMvXc, flMaxDist, usCurrCount);
      }
   }
   return(usCurrCount);
}

//
//  Function:  VEC_RemoveCluster
//  Purpose:   Remove a cluster of MVs from the list
//
//  Parms:     Motion vector data, Index into sort-key list, cluster distance
//  Returns:   
//  Note:      
//             
//
void VEC_RemoveCluster(MVIMG *pstMv, u_int16 usIdx, u_int16 usDist)
{
   u_int16 *pusCluster;
   u_int16  usKey, usNr, usClusterSize, usTmp;
   MVEC    *pstFrame=&pstMv->pstFrames[pstMv->iNumMotionVectors * pstMv->iFrameIdx];

   usClusterSize = usDist * usDist;
   pusCluster    = safemalloc(usClusterSize * sizeof(u_int16));
   usNr          = VEC_CountVectorCluster(pstMv, usIdx, usDist, pusCluster, usClusterSize);
   //
   PRINTF3("VEC-RemoveCluster():Remove %d vectors from cluster %d (Dist=%d)" CRLF, usNr, usIdx, usDist);
   //
   for(usTmp=0; usTmp<usNr; usTmp++)
   {
      usKey = pusCluster[usTmp];
      PRINTF1("VEC-RemoveCluster():Remove vector %d" CRLF, usKey);
      pstFrame[usKey].usSad    = 0;
      pstFrame[usKey].bVectorX = 0;
      pstFrame[usKey].bVectorY = 0;
      pstMv->piVector[usKey]   = 0;
   }
   pstMv->iCalMotionVectors -= usNr;
   safefree(pusCluster);
}

//
//  Function:  VEC_CenterOfGravity
//  Purpose:   Determine center of gravity for the current frame
//
//  Parms:     Motion vector data, max distance (pixels sqare), ptr to nr of close-by MV's
//  Returns:   Index
//  Note:      Max dist in square pixels: calculate max radius
//
u_int16 VEC_CenterOfGravity(MVIMG *pstMv, u_int16 usMaxDist, u_int16 *pusProx)
{
   u_int16  usBestIdx=0, usCurrIdx=0, usCurrCount, usBestCount=0;
   u_int16  usNrMv=pstMv->iCalMotionVectors;

   while(usCurrIdx < usNrMv)
   {
      usCurrCount = VEC_CountVectorCluster(pstMv, usCurrIdx, usMaxDist, NULL, 0);
      if(usCurrCount > usBestCount)
      {
         PRINTF2("VEC-CenterOfGravity():CurrCount=%d > BestCount=%d" CRLF, usCurrCount, usBestCount);
         //
         // This center seems be be better than previous one: save it
         //
         usBestIdx   = usCurrIdx;
         usBestCount = usCurrCount;
      }
      usCurrIdx++;
   }
   if(pusProx) *pusProx = usBestCount;
   //
   return(usBestIdx);
}

//
//  Function:  VEC_RemoveCluster
//  Purpose:   Remove a cluster of MVs from the list
//
//  Parms:     Motion vector data, Cluster ID
//  Returns:   Number of MVs removed
//  Note:      
//             
//
u_int16 VEC_RemoveCluster(MVIMG *pstMv, u_int16 usCid)
{
   MVKEY    tSaveKey;
   u_int16  usMvKey, usIdx,  usCount=0;
   MVEC    *pstFrame=&pstMv->pstFrames[pstMv->iNumMotionVectors * pstMv->iFrameIdx];

   //
   // Check if list needs resorting
   //
   tSaveKey = pstMv->tSorted;
   VEC_SortMotionVectors(pstMv, MVKEY_LH_CID);
   //
   for(usIdx=0; usIdx<pstMv->iCalMotionVectors; usIdx++)
   {
      //
      // Get cluster index, check ID match
      //
      usMvKey = pstMv->pusKeys[usIdx];
      if(pstMv->pusClusters[usMvKey] == usCid) 
      {
         pstFrame[usMvKey].usSad    = 0;
         pstFrame[usMvKey].bVectorX = 0;
         pstFrame[usMvKey].bVectorY = 0;
         //
         pstMv->piVector[usMvKey]   = 0;
         usCount++;
      }
   }
   //
   // Some MVs are no longer in the list: force resorting
   //
   pstMv->iCalMotionVectors -= usCount;
   VEC_SortMotionVectors(pstMv, MVKEY_HL_NMV);
   //
   // Put sorting order back as it was
   //
   VEC_SortMotionVectors(pstMv, tSaveKey);
   return(usCount);
}

//
//  Function:  VEC_CountClusterSize
//  Purpose:   Count the cluster size of a cluster of MVs
//
//  Parms:     Motion vector data, ClusterId
//  Returns:   Size of the cluster
//  Note:      For each of the [iCalMotionVectors] MVs, check how large its cluster is
//
//             +---+---+---+---+---+---+---+---+---+           Distance
//      Border | X | X | X | X | X | X | X | X | X | Ex: MV  1  2  3  4
//             +---+---+---+---+---+---+---+---+---+    ---  ------------
//             |   |   |   |   |   |   |   |   |   |     A   3  7  9  10
//             +---+---+---+---+---+---+---+---+---+     B   5  9  10 10
//             |   |   |   | A |   |   |   |   |   |     C   7  9  10 10
//             +---+---+---+---+---+---+---+---+---+     D   4  9  10 10
//             |   |   |   | B | C | D |   |   |   |     E   8  10 10 10
//             +---+---+---+---+---+---+---+---+---+     F   6  10 10 10
//             |   |   |   | j | E | F |   |   |   |     G   6  9  10 10
//             +---+---+---+---+---+---+---+---+---+     H   5  9  10 10
//             |   |   |   |   | G |H  |   |   |   |     I   3  6  9  10
//             +---+---+---+---+---+---+---+---+---+     J   6  10 10 10
//             |   |   |   |   | I |   |   |   |   |
//             +---+---+---+---+---+---+---+---+---+
//             |   |   |   |   |   |   |   |   |   |
//             +---+---+---+---+---+---+---+---+---+
//
//
u_int16 VEC_CountClusterSize(MVIMG *pstMv, u_int16 usCid)
{
   u_int16  usIdx, usCount=0;
   u_int16 *pusCid=pstMv->pusClusters;

   for(usIdx=0; usIdx<pstMv->iNumMotionVectors; usIdx++)
   {
      //
      // check ID match
      //
      if(*pusCid++ == usCid) usCount++;
   }
   return(usCount);
}



#endif