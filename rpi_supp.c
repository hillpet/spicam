/*  (c) Copyright:  2017  Patrn.nl, Confidential Data 
**
**  $Workfile:          rpi_supp.c
**  $Revision:   
**  $Modtime:    
**
**  Purpose:            RPi support thread
**                      Periodically, the Support thread will check several other threads for
**                      safe operations. If something fails, we have two distict operations:
**                         o Send SIGUSR1 to the rpi_dog thread: this will restart theApp
**                         o Send SIGUSR2 to the rpi_dog thread: this will reboot the system
**
**                      For CMD line operations, the Support thread can be disabled by sending
**                      a SIGUSR2 to this thread (PID_SUPP). SIGUSR1 will enable the function again.
**
**
**
**  Entry Points:       
**
**
**
**
**
 *  Compiler/Assembler: Raspbian Linux
 *  Ext Packages:
**
 *  Author:             Peter Hillen
 *  Date Created:       14 Sep 2017
**
 *  Revisions:
 *    $Log:   $
 *
 *
 **/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <time.h>
#include <errno.h>
#include <resolv.h>
#include <sys/types.h>
#include <sys/signal.h>
#include <sys/socket.h>
#include <sys/wait.h>
#include <signal.h>

#include <common.h>
#include "config.h"
#include "globals.h"
#include "gen_func.h"
#include "rpi_supp.h"

//#define USE_PRINTF
#include <printf.h>

//
// Local prototypes
//
static bool    sup_CheckBackground     (void);
static bool    sup_Check               (pid_t, const char *);
static bool    sup_TerminateUtility    (const char *, int);
//
static void    sup_HandleSupport       (void);
static void    sup_HandleGuard         (void);
static bool    sup_SignalRegister      (sigset_t *);
static void    sup_ReceiveSignalSegmnt (int);
static void    sup_ReceiveSignalInt    (int);
static void    sup_ReceiveSignalUser1  (int);
static void    sup_ReceiveSignalUser2  (int);
static void    sup_ReceiveSignalTerm   (int);
static int     sup_WaitSemaphore       (int);

#define  SUPP_TIMEOUT_FRIENDLY   1000
#define  SUPP_TIMEOUT_FORCED     500
//
// Local variables
//
static bool       fSupRunning   = TRUE;
static bool       fSupEnabled   = TRUE;
//
static const char *pcRaspiStill = RPI_RASPISTILL;
static const char *pcRaspiVid   = RPI_RASPIVID;

//
//  Function:  SUP_InitSupport
//  Purpose:   Init support thread
//
//  Parms:     
//  Returns:   Startup code
//
int SUP_Init()
{
   int      iCc=0;
   pid_t    tPid;

   //==========================================================================
   // Split process : 
   //       Parent  thread returns with tPid of the EXEC thread
   //       Support thread returns with tPid=0
   //
   tPid = fork();
   //==========================================================================
   switch(tPid)
   {
      case 0:
         //
         // Support child
         //
         sup_HandleSupport();
         _exit(0);
         break;

      case -1:
         // Error
         break;
            
      default:
         //
         // This is the parent thread which started the Support process
         //
         PRINTF1("SUP_InitSupport(): Support thread now running: pid=%d" CRLF, tPid);
         GLOBAL_PidPut(PID_SUPP, tPid);
         iCc = GLOBAL_SUP_INI;
         break;
   }
   return(iCc);
}

/*------  Local functions separator ------------------------------------
__Local_functions_(){};
----------------------------------------------------------------------------*/

//
//  Function:  sup_HandleSupport
//  Purpose:   Handle the support of various activities
//
//  Parms:     
//  Returns:   If killed
//  Note:      Support handles:
//                HOST: Main thread         PID_HOST
//                CAMR: Camera handler          CAMR
//                CMND: Command handler         CMND
//                HELP: Apps helper             HELP
//                EMUL: Localhost handler       EMUL
//                LIRC: RCU handler             LIRC
//                UTIL: Utility handler         UTIL
//                FBUF: Apps executioner        FBUF
//                KODI: iPAD Kodi RCU           KODI
//                USBC: USB Comms               USBC
//                USBD: USB Debug               USBD
//
static void sup_HandleSupport(void)
{
   sigset_t tBlockset;

   fSupRunning = TRUE;
   //
   // This is the support thread
   //
   if(sup_SignalRegister(&tBlockset) == FALSE) exit(-1);
   //
   LOG_Report(0, "SUP", "sup_HandleSupport():Init Start....");
   PRINTF("sup_HandleSupport():Run support tasks..." CRLF);
   //
   //  If pshared has the value 0, then the semaphore is shared between the
   //  threads of a process, and should be located at some address that is
   //  visible to all threads (e.g., a global variable, or a variable
   //  allocated dynamically on the heap).
   //  If pshared is nonzero, then the semaphore is shared between
   //  processes, and should be located in a region of shared memory.
   //
   //  The sem value is always >= 0:
   //                    0 means LOCKED
   //                   >0 means UNLOCKED
   //  Init(sem 1, VAL)  VAL=1 unlocked
   //                    VAL=0 locked
   //  Wait(sem)         LOCK:    if(VAL> 0) { VAL--; return;         } 
   //                             if(VAL==0) { Wait till > 0; return; }
   //  Post(sem)         UNLOCK:  VAL++
   //
   //  Init the semaphore as LOCKED
   //
   if(sem_init(&pstMap->G_tSemSup, 1, 0) == -1) 
   {
      LOG_Report(errno, "SUP", "sup_HandleSupport():sem_init");
      PRINTF("sup_HandleSupport():sem_init ERROR");
      fSupRunning = FALSE;
   }
   //
   GLOBAL_PidSaveGuard(PID_SUPP, GLOBAL_SUP_INI);
   GLOBAL_HostNotification(GLOBAL_SUP_INI);
   LOG_Report(0, "SUP", "sup_HandleSupport():Init Ready");
   //
   while(fSupRunning)
   {
      switch( sup_WaitSemaphore(10000) )
      {
         case 0:
            //
            // Signaled: check threads
            //
            if(fSupEnabled)   sup_CheckBackground();
            else              PRINTF("sup_HandleSupport():Disabled !" CRLF);
            break;

         case -1:
            PRINTF("sup_HandleSupport():ERROR sup_WaitSemaphore" CRLF);
            LOG_Report(errno, "SUP", "sup_HandleSupport():ERROR sup_WaitSemaphore");
            break;
         
         default:
            break;

         case 1:
            //
            // Timeout: Run guard duty and check all threads
            //
            if(GLOBAL_GetSignalNotification(PID_SUPP, GLOBAL_GRD_RUN)) sup_HandleGuard();
            if(fSupEnabled)                                            sup_CheckBackground();
            GLOBAL_PidSetGuard(PID_SUPP);
            break;
      }
   }
   //
   // Support thread terminated: Make sure utilities are also terminated
   //
   sup_TerminateUtility(pcRaspiStill, SUPP_TIMEOUT_FORCED);
   sup_TerminateUtility(pcRaspiVid,   SUPP_TIMEOUT_FORCED);
   PRINTF("sup_HandleSupport():Exit" CRLF);
}

// 
// Function:   supp_HandleGuard
// Purpose:    Respond to the guard query
// 
// Parameters: 
// Returns:    
// Note:       
//
static void sup_HandleGuard(void)
{
   LOG_Report(0, "SUP", "supp_HandleGuard():Host-notification send");
   GLOBAL_HostNotification(GLOBAL_GRD_NFY);
}

//  
// Function:      sup_CheckBackground
// Description:   Check essential background threads
// 
// Parameters: 
// Returns:       TRUE if all still OKee
//  
static bool sup_CheckBackground()
{
   bool     fCc=TRUE;
   pid_t    tPid;

   //
   // Check all threads running
   //
   if(!sup_Check(GLOBAL_PidGet(PID_HOST), "HOST:Parent"))
   {
      //
      // Serious problems: the server thread has crashed: restart the App
      // Make sure any raspi-tools are also terminated
      //
      sup_TerminateUtility(pcRaspiStill, SUPP_TIMEOUT_FRIENDLY);
      sup_TerminateUtility(pcRaspiVid,   SUPP_TIMEOUT_FRIENDLY);
      //
      tPid = GEN_GetPidByName(RPI_WATCHDOG);
      GEN_SignalPid(tPid, SIGUSR1);
      return(FALSE);
   }
   if(!sup_Check(GLOBAL_PidGet(PID_CAMR), "CAMR: Camera handler"))
   {
      //
      // Serious problems: the Camera thread has crashed: restart the App
      // Make sure any raspi-tools are also terminated
      //
      sup_TerminateUtility(pcRaspiStill, SUPP_TIMEOUT_FRIENDLY);
      sup_TerminateUtility(pcRaspiVid,   SUPP_TIMEOUT_FRIENDLY);
      //
      GLOBAL_Notify(PID_HOST, GLOBAL_RPI_RST, SIGTERM);
      return(FALSE);
   }
   if(!sup_Check(GLOBAL_PidGet(PID_CMND), "CMND: Command handler"))
   {
      //
      // Serious problems: the Command thread has crashed: restart the App
      // Make sure any raspi-tools are also terminated
      //
      sup_TerminateUtility(pcRaspiStill, SUPP_TIMEOUT_FRIENDLY);
      sup_TerminateUtility(pcRaspiVid,   SUPP_TIMEOUT_FRIENDLY);
      //
      GLOBAL_Notify(PID_HOST, GLOBAL_RPI_RST, SIGTERM);
      return(FALSE);
   }
   if(!sup_Check(GLOBAL_PidGet(PID_HELP), "HELP: Apps helper"))
   {
      //
      // The Helper thread has crashed: 
      // Make sure any raspi-tools are also terminated
      //
      sup_TerminateUtility(pcRaspiStill, SUPP_TIMEOUT_FRIENDLY);
      sup_TerminateUtility(pcRaspiVid,   SUPP_TIMEOUT_FRIENDLY);
      //
      // Make sure the Utility helper is also terminated
      //
      if( GEN_KillProcess(GLOBAL_PidGet(PID_UTIL), DO_FRIENDLY, SUPP_TIMEOUT_FRIENDLY) == 1) 
          GEN_KillProcess(GLOBAL_PidGet(PID_UTIL), NO_FRIENDLY, SUPP_TIMEOUT_FORCED);
   }
   if(!sup_Check(GLOBAL_PidGet(PID_UTIL), "UTIL: Utility handler"))
   {
      //
      // The Utility thread has crashed: 
      // Make sure any raspi-tools are also terminated
      //
      sup_TerminateUtility(pcRaspiStill, SUPP_TIMEOUT_FRIENDLY);
      sup_TerminateUtility(pcRaspiVid,   SUPP_TIMEOUT_FRIENDLY);
      //
      // Make sure the Helper is also terminated
      //
      if( GEN_KillProcess(GLOBAL_PidGet(PID_HELP), DO_FRIENDLY, SUPP_TIMEOUT_FRIENDLY) == 1) 
          GEN_KillProcess(GLOBAL_PidGet(PID_HELP), NO_FRIENDLY, SUPP_TIMEOUT_FORCED);
   }
   //
   // Other threads are report only
   //
   if(!sup_Check(GLOBAL_PidGet(PID_EMUL), "EMUL: Localhost handler"))  fCc = FALSE;
   if(!sup_Check(GLOBAL_PidGet(PID_LIRC), "LIRC: RCU handler"))        fCc = FALSE;
   if(!sup_Check(GLOBAL_PidGet(PID_FBUF), "FBUF: Apps executioner"))   fCc = FALSE;
   //
   return(fCc);
}

//  
// Function:      sup_Check
// Description:   Check essential background thread
// 
// Parameters:    Pid, description
// Returns:       TRUE if thread is running just fine, or it is legally not running 
// Note:          GEN_SignalPid(tPid, 0) does not send the signal, but reports normal errors:
//                On success zero is returned.  On error, -1 is returned, 
//                and errno is set appropriately:
//
//                EINVAL An invalid signal was specified.
//
//                EPERM  The process does not have permission to send the signal to any
//                       of the target processes.
//
//                ESRCH  The process or process group does not exist.  Note that an
//                       existing process might be a zombie, a process that has
//                       terminated execution, but has not yet been wait(2)ed for.
//  
static bool sup_Check(pid_t tPid, const char *pcName)
{
   bool     fCc=FALSE;
   PIDINFO  stInfo;
   PIDST    tState=PIDST_IS_NOT;

   if(tPid)
   {
      //
      // Background thread should be running: check if still there
      //
      switch(GEN_GetPidInfo(&stInfo, tPid, PIDST_STATE))
      {
         case 0:
            //
            // Thread has terminated by itself: report
            //
            GEN_SignalPid(tPid, 0);
            LOG_Report(errno, "SUP", "sup_Check():%s has stopped unexpectedly:", pcName);
            break;

         case 1:
            tState = stInfo.tState;
            switch(tState)
            {
               default:
                  GEN_SignalPid(tPid, 0);
                  LOG_Report(errno, "SUP", "sup_Check():%s has problems:", pcName);
                  break;

               case PIDST_IS_SLEEPING:
               case PIDST_IS_RUNNING:
                  //
                  // Thread is doing fine
                  //
                  fCc = TRUE;
                  break;

               case PIDST_IS_ZOMBIE:
                  GEN_SignalPid(tPid, 0);
                  LOG_Report(errno, "SUP", "sup_Check():%s has gone zombie:", pcName);
                  break;
            }
            break;

         default:
         case -1:
            //
            // Error 
            //
            GEN_SignalPid(tPid, 0);
            LOG_Report(errno, "SUP", "sup_Check():%s reports error:", pcName);
            break;
      }
   }
   else 
   {
      //
      // Thread is legally not active, OKee
      //
      fCc = TRUE;
   }
   return(fCc);
}

//  
// Function:      sup_TerminateUtility
// Description:   Check if utility is running. If so, terminate it.
// 
// Parameters:    Utility, friendly timeout
// Returns:       TRUE if utility was found and terminated
// Note:          
//                
static bool sup_TerminateUtility(const char *pcName, int iTimeout)
{
   bool     fCc=FALSE;
   pid_t    tPid;

   tPid = GEN_GetPidByName(pcName);
   if(tPid)
   {
      //
      // Yes, the utility is running: terminate friendly or by force
      // Report
      //
      sup_Check(tPid, pcName);
      //
      if(iTimeout)
      {
         //
         // Try friendly first
         //
         if( GEN_KillProcess(tPid, DO_FRIENDLY, iTimeout) == 1) 
             GEN_KillProcess(tPid, NO_FRIENDLY, SUPP_TIMEOUT_FORCED);
      }
      else
      {
         GEN_KillProcess(tPid, NO_FRIENDLY, SUPP_TIMEOUT_FORCED);
      }
      //
      fCc = TRUE;
   }
   return(fCc);
}

/*------  Local functions separator ------------------------------------
__Signal_functions_(){};
----------------------------------------------------------------------------*/

//
//  Function:   sup_SignalRegister
//  Purpose:    Register all SIGxxx
//
//  Parms:      Blockset
//  Returns:    TRUE if delivered
//
static bool sup_SignalRegister(sigset_t *ptBlockset)
{
  bool fCC = TRUE;

   if( signal(SIGSEGV, &sup_ReceiveSignalSegmnt) == SIG_ERR)
   {
      LOG_Report(errno, "SUP", "sup_SignalRegister(): SIGSEGV Error");
      fCC = FALSE;
   }
   if( signal(SIGUSR1, &sup_ReceiveSignalUser1) == SIG_ERR)
   {
      LOG_Report(errno, "SUP", "sup_SignalRegister(): SIGUSR1 Error");
      fCC = FALSE;
   }
   if( signal(SIGUSR2, &sup_ReceiveSignalUser2) == SIG_ERR)
   {
      LOG_Report(errno, "SUP", "sup_SignalRegister(): SIGUSR2 Error");
      fCC = FALSE;
   }
  // CTL-X handler
  if( signal(SIGTERM, &sup_ReceiveSignalTerm) == SIG_ERR)
  {
     LOG_Report(errno, "SUP", "sup_SignalRegister(): SIGTERM ERROR");
     fCC = FALSE;
  }
  // Ctl-C handler
  if( signal(SIGINT, &sup_ReceiveSignalInt) == SIG_ERR)
  {
     LOG_Report(errno, "SUP", "sup_SignalRegister(): SIGINT ERROR");
     fCC = FALSE;
  }
  //
  if(fCC)
  {
     //
     // Setup the SIGxxx events
     //
     sigemptyset(ptBlockset);
     sigaddset(ptBlockset, SIGSEGV);
     sigaddset(ptBlockset, SIGUSR1);
     sigaddset(ptBlockset, SIGUSR2);
     sigaddset(ptBlockset, SIGTERM);
     sigaddset(ptBlockset, SIGINT);
  }
  return(fCC);
}

//
// Function:   sup_ReceiveSignalSegmnt
// Purpose:    Handle the SIGSEGV
//
// Parms:
// Returns:    TRUE if delivered
// Note:       Do NOT use LOG_xxx() here
//
static void sup_ReceiveSignalSegmnt(int iSignal)
{
   if(fSupRunning)
   {
      fSupRunning = FALSE;
      //
      GEN_SegmentationFault(__FILE__, __LINE__);
      LOG_SegmentationFault(__FILE__, __LINE__);
      //
      // (TRY TO) Cleanup background threads:
      //
      GLOBAL_Notify(PID_HOST, GLOBAL_FLT_NFY, SIGTERM);
   }
}

//
//  Function:   sup_ReceiveSignalInt
//  Purpose:    Add the SIGINT command in the buffer (Ctl-C)
//
//  Parms:
//  Returns:    TRUE if delivered
//
static void sup_ReceiveSignalInt(int iSignal)
{
   fSupRunning = FALSE;
   PRINTF("sup_ReceiveSignalInt()" CRLF);
   LOG_Report(0, "SUP", "sup_ReceiveSignalInt()");
   if( sem_post(&pstMap->G_tSemSup) == -1) LOG_Report(errno, "SUP", "sup_ReceiveSignalInt():sem_post");
}

//
//  Function:   sup_ReceiveSignalUser1
//  Purpose:    Add the SIGUSR1 command in the buffer. Used to ENABLE the support function
//
//  Parms:
//  Returns:    TRUE if delivered
//
static void sup_ReceiveSignalUser1(int iSignal)
{
   fSupEnabled = TRUE;
   PRINTF("sup_ReceiveSignalUser1(): Enable Support" CRLF);
   LOG_Report(0, "SUP", "sup_ReceiveSignalUser1()");
   //
   if( sem_post(&pstMap->G_tSemSup) == -1) LOG_Report(errno, "SUP", "sup_ReceiveSignalUser1():sem_post");
}

//
//  Function:   sup_ReceiveSignalUser2
//  Purpose:    Add the SIGUSR2 command in the buffer. Used to DISABLE the support function
//
//  Parms:
//  Returns:    TRUE if delivered
//
static void sup_ReceiveSignalUser2(int iSignal)
{
   fSupEnabled = FALSE;
   PRINTF("sup_ReceiveSignalUser1()" CRLF);
   LOG_Report(0, "SUP", "sup_ReceiveSignalUser2(): Disable Support");
   //
   if( sem_post(&pstMap->G_tSemSup) == -1) LOG_Report(errno, "SUP", "sup_ReceiveSignalUser2():sem_post");
}

//
//  Function:   sup_ReceiveSignalTerm
//  Purpose:    Add the SIGTERM command in the buffer (Ctl-X)
//
//  Parms:
//  Returns:    TRUE if delivered
//
static void sup_ReceiveSignalTerm(int iSignal)
{
   fSupRunning = FALSE;
   PRINTF("sup_ReceiveSignalTerm()" CRLF);
   LOG_Report(0, "SUP", "sup_ReceiveSignalTerm()");
   if( sem_post(&pstMap->G_tSemSup) == -1) LOG_Report(errno, "SUP", "sup_ReceiveSignalTerm():sem_post");
}

//
// Function:   sup_WaitSemaphore
// Purpose:    Wait till Semaphore has been unlocked, then LOCK and proceed
//
// Parms:      TIMEOUT (or 0 if no timeout)
// Returns:     0 if sem unlocked
//             +1 if sem timeout
//             -1 on error (errno)
// Note:       sem_wait() tries to decrement (lock) the semaphore. 
//             If the semaphore currently has the value zero, then the call blocks until 
//             either it becomes possible to perform the decrement (the semaphore value rises 
//             above zero) or a signal handler interrupts the call. 
//             If the semaphore's value is greater than zero, then the decrement proceeds, 
//             and the function returns immediately. 
//
//             sem_timedwait() is the same as sem_wait(), except that TIMEOUT specifies a 
//             limit on the amount of time that the call should block if the decrement cannot 
//             be immediately performed. The stTime points to a structure that
//             specifies an absolute timeout in seconds and nanoseconds since the Epoch, 
//             1970-01-01 00:00:00 +0000 (UTC). This structure is defined as follows:
//
//             struct timespec 
//             {
//                time_t tv_sec;      /* Seconds */
//                long   tv_nsec;     /* Nanoseconds [0 .. 999,999,999] */
//             };
//             If the timeout has already expired by the time of the call, and the semaphore 
//             could not be locked immediately, then sem_timedwait() fails with a timeout error 
//             (errno set to ETIMEDOUT). If the operation can be performed immediately, then 
//             sem_timedwait() never fails with a timeout error, regardless of the value of 
//             TIMEOUT.
//
static int sup_WaitSemaphore(int iMsecs)
{
   //
   // if    sem == 0 : wait
   // else  sem-- (LOCK)   
   //
   return(GEN_WaitSemaphore(&pstMap->G_tSemSup, iMsecs));
}

