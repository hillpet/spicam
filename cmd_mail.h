/*  (c) Copyright:  2017  Patrn, Confidential Data
 *
 *  Workfile:           cmd_mail.h
 *  Revision:          
 *  Modtime:           
 *
 *  Purpose:            Headerfile for command cmd_mail.c
 *
 *
 *
 *  Compiler/Assembler: Raspbian Linux GNU gcc
 *  Ext Packages:
 *
 *  Author:             Peter Hillen
 *  Date Created:       27 Nov 2017
 *
 *  Revisions:
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
**/

#ifndef _CMD_MAIL_H_
#define _CMD_MAIL_H_

#include "mail/mail_func.h"

bool CMD_InitMail             (void);
void CMD_SendMail             (MAILSCB);
void CMD_ReceiveMail          (void);
bool CMD_ParameterChanged     (int);

#endif /* _CMD_MAIL_H_ */
