/*  (c) Copyright:  2017  Patrn, Confidential Data
 *
 *  Workfile:           gen_http.h
 *  Revision:          
 *  Modtime:           
 *
 *  Purpose:            Headerfile for gen_http functions
 *
 *
 *
 *  Compiler/Assembler: Raspbian Linux GNU gcc
 *  Ext Packages:
 *
 *  Author:             Peter Hillen
 *  Date Created:       09 Mar 2017
 *
 *  Revisions:
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
**/

#ifndef _GEN_HTTP_H_
#define _GEN_HTTP_H_

#define MAX_DYN_PAGES         64
//
// Dynamic page registered URLs
//
typedef bool(*PFVOIDPI)(void *, int);
//
char    *GEN_FindDynamicPageName       (int);
int      GEN_GetDynamicPageUrl         (int);
char    *GEN_GetDynamicPageName        (int);
int      GEN_GetDynamicPagePid         (int);
int      GEN_GetDynamicPagePort        (int);
int      GEN_GetDynamicPageTimeout     (int);
FTYPE    GEN_GetDynamicPageType        (int);
PFVOIDPI GEN_GetDynamicPageCallback    (int);
//
int      GEN_RegisterDynamicPage       (int, int, FTYPE, int, int, const char *, PFVOIDPI);


#endif /* _GEN_HTTP_H_ */
