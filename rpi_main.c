/*  (c) Copyright:  2012..2018 Patrn, Confidential Data
 *
 *  Workfile:           rpi_main.c
 *  Revision:   
 *  Modtime:    
 *
 *  Purpose:            spicam is a simple dynamic HTTP server used to control the RPi camera.
 *
 *
 *
 *  Compiler/Assembler: Platform independent
 *  Ext Packages:
 * 
 *  Author:             Peter Hillen
 *  Date Created:       17 Jul 2012
 *                                   
 *  Entry Points:       main()
 *
 *  Revisions:
 *	   11 Jun 2018:      HIDUSB has makefile FEATURE_USB
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
**/

/*
**  Client-Server model:
**
**        RPI          CAM                  CMD
**         |            |                          |
**         |            |          +-------+       |
**         |            |          |   0   |       |
**         +----->------+---->-----|  cmd  |--->---+ SIGUSR1
**         |            |          |  data |       |
**         |            |          +-------+       |
**         |            |                        Handle cmd
**         |            |                          |
**         |            |          +-------+       |
**         |            |          |  cc   |---<---+ SIGUSR1
**         +-----<------+----<-----|  cmd  |       |
**         |            |          |  data |       |
**         |            |          +-------+       |
**         |            |                          |
**         |            |                          |
**         |            |          +-------+       |
**         |            |          |  cc   |       |
**         +----->------+---->-----|   0   |--->---+ SIGUSR1
**         |            |          |   0   |       |
**         |            |          +-------+       |
**         |            |                        Cleanup 
**         |            |                          |
**         |            |          +-------+       |
**         |            |          |   0   |---<---+ SIGUSR1
**         +-----<------+----<-----|   0   |       |
**         |            |          |   0   |       |
**         |            |          +-------+       |
**         |            |                          |
**
**
**        RPI                       BROWSER:http exit command
**         |                           |
**         +---------------<-----------+ SIGINT
**         |                           |
**         |                           
**         |                           
**         +---->--- SIGINT  CAM  thread
**         +---->--- SIGINT  CMD  thread
**         +---->--- SIGINT  HELP thread
**         +---->--- SIGINT  ...  thread
**         |                           
**
**         Threads: (assume option -D for background execution):
**
**          HOST : Thread:Startup (exits on -D cmd line arg)
**          CAMR : Thread:CAM
**          CMND : Thread:Command
**          HELP : Thread:Helper
**          KODI : Thread:Kodi RCU
**          PROX : Thread:Proxy server
**          UTIL : Thread:Apps execution
**          LIRC : Thread:Lirc IR remote control deamon
**          STRM : Thread:Streamer deamon
*/

#include <stdio.h>
#include <string.h>
#include <netinet/in.h>
#include <stdlib.h>
#include <unistd.h>
#include <time.h>
#include <resolv.h>
#include <sys/types.h>
#include <sys/signal.h>
#include <sys/socket.h>
#include <sys/wait.h>
#include <errno.h>
#include <execinfo.h>

#include <common.h>
#include "config.h"
#include "globals.h"
#include "rpi_page.h"
#include "rpi_func.h"
#include "rpi_srvr.h"
#include "rpi_kodi.h"
#include "rpi_supp.h"
#include "rpi_proxy.h"
#include "cam_main.h"
#include "cmd_main.h"
#include "cmd_alarm.h"
#include "btn_button.h"
#include "gen_http.h"
#include "gen_func.h"
#include "gen_usb.h"
#include "graphics.h"
//
#include "rpi_main.h"

#define USE_PRINTF
#include <printf.h>


#if defined(__DATE__) && defined(__TIME__)
   static const char RPI_BUILT[] = __DATE__ " " __TIME__;
#else
   static const char RPI_BUILT[] = "Unknown";
#endif

#ifdef   FEATURE_TEST_IO
static void    rpi_TestIo              (void);
#define  RPI_TESTIO()                  rpi_TestIo()
#else  //FEATURE_TEST_IO
#define  RPI_TESTIO()
#endif //FEATURE_TEST_IO

static pid_t   rpi_Deamon              (void);
static void    rpi_Help                (void);
static void    rpi_Version             (void);
//
static bool    rpi_SignalRegister      (sigset_t *);
static void    rpi_ReceiveSignalInt    (int);
static void    rpi_ReceiveSignalTerm   (int);
static void    rpi_ReceiveSignalUser1  (int);
static void    rpi_ReceiveSignalUser2  (int);
static void    rpi_ReceiveSignalSegmnt (int);
//
static int     rpi_Startup             (RPIUSB *);
static void    rpi_GlobalsInit         (RPIMAP *);
static bool    rpi_HandleButton        (void);
static bool    rpi_HandleGuard         (void);
static bool    rpi_WaitStartup         (int);
static bool    rpi_SegmentationFault   (void);
//
static void    rpi_ExitSemaphore       (void);
static void    rpi_InitSemaphore       (void);
static void    rpi_PostSemaphore       (void);
static int     rpi_WaitSemaphore       (int);
//
static bool    fRpiRunning          = TRUE;
static bool    fRpiDeamonMode       = FALSE;
static bool    fRpiEmergencyReboot  = FALSE;
//
// Generic timestamp
//
static char    cDateTime[32];

/* ======   Local Functions separator ===========================================
___MAIN_FUNCTIONS(){}
==============================================================================*/

//
//  Function:   main
//  Purpose:    Main entry point for the Raspberry Pi monitor
//
//  Parms:      Commandline options
//  Returns:    Exit codes
//
int main(int argc, char **argv)
{
   bool        fMapOkee;
   int         iOpt, iAddZero=0, iAddSpare=0;
   int         iCnxs, iGuards, iGuardCount;
   u_int32     ulSecsNow, ulSecsCheck, iGuardsSecs=GUARD_SECS_DEBUG;
   pid_t       tPid;
   sigset_t    tBlockset;
   RPIUSB     *pstHid=NULL;
   
   fMapOkee = GLOBAL_Init();
   //
   LOG_printf("spicam():%s-v%s:Build:%s (%s)" CRLF, GLOBAL_GetHostName(), pstMap->G_pcVersionSw, RPI_BUILT, RPI_IO_BOARD);
   LOG_printf("spicam():Safe memory allocation=%d" CRLF, safemalloccount());
   //
   // If MMAP was restored from file, all is fine. If not, some additional init may be appropriate
   //
   if(!fMapOkee)
   {
      rpi_GlobalsInit(pstMap);
   }
   //
   // NO ongoing recordings
   //
   pstMap->G_ulStartTimestamp = RTC_GetDateTime(NULL);
   pstMap->G_ulRecordingStart = -1;
   //
   while ((iOpt = getopt(argc, argv, "a:z:dk:p:FDhvu")) != -1)
   {
      switch (iOpt)
      {
         case 'a':
            LOG_printf("spicam():Option -a=%s" CRLF, optarg);
            iAddSpare = atoi(optarg);
            break;

         case 'z':
            LOG_printf("spicam():Option -z=%s" CRLF, optarg);
            iAddZero = atoi(optarg);
            break;

         case 'd':
            LOG_printf("spicam():Option -d=%s" CRLF, optarg);
            GEN_STRCPY(pstMap->G_pcDebugMask, optarg);
            GLOBAL_ConvertDebugMask(TRUE);
            break;

         case 'k':
            LOG_printf("spicam():Option -k=%s" CRLF, optarg);
            GEN_STRCPY(pstMap->G_pcKodiPort, optarg);
            break;

         case 'p':
            LOG_printf("spicam():Option -p=%s" CRLF, optarg);
            GEN_STRCPY(pstMap->G_pcSrvrPort, optarg);
            break;

         case 'F':
            LOG_printf("spicam():Option -F" CRLF);
            pstMap->G_fFakeExec = TRUE;
            break;

         case 'D':
            LOG_printf("spicam():Option -D" CRLF);
            LOG_printf("Starting spicam Http server as deamon" CRLF);
            stderr = fopen(RPI_ERROR_PATH, "a+");
            fRpiDeamonMode = TRUE;
            break;

         case 'v':
            LOG_printf("spicam():Option -v" CRLF);
            rpi_Version() ;
            //
            GLOBAL_Close();
            exit(EXIT_SUCCESS);
            break;
   
         case 'h':
            LOG_printf("spicam():Option -h" CRLF);
            rpi_Help();
            GLOBAL_Close();
            exit(EXIT_SUCCESS);
            break;
   
         case 'u':
            LOG_printf("spicam():Option -u" CRLF);
            LOG_printf("spicam():Starting Teensy USB comms and debug channels" CRLF);
            pstHid = USB_GetInfo(0, 0);
            break;

         default:
            LOG_printf("spicam(): Invalid option." CRLF);
            rpi_Help();
            GLOBAL_Close();
            exit(EXIT_SUCCESS);
            break;
      }
   }
   //
   if( (iAddZero>0) || (iAddSpare>0) )
   {
      //
      // CL Option to expand the mmap file: do it and exit
      //
      GLOBAL_ExpandMap(iAddZero, iAddSpare);
      //
      LOG_printf("spicam(): Sizeof int       = %d" CRLF, sizeof(int));
      LOG_printf("spicam(): Sizeof long      = %d" CRLF, sizeof(long));
      LOG_printf("spicam(): Sizeof long long = %d" CRLF, sizeof(long long));
      LOG_printf("spicam(): Sizeof float     = %d" CRLF, sizeof(float));
      LOG_printf("spicam(): Sizeof double    = %d" CRLF, sizeof(double));
      LOG_printf("spicam(): Sizeof RPIMAP    = %d" CRLF, sizeof(RPIMAP));
      LOG_printf("spicam(): Sizeof GLOCMD    = %d" CRLF, sizeof(GLOCMD));
      LOG_printf("spicam(): Sizeof MDET      = %d" CRLF, sizeof(MDET));
      LOG_printf("spicam(): Sizeof GLOG      = %d" CRLF, sizeof(GLOG));
      LOG_printf("spicam(): Expanding done:" CRLF);
      //
      GLOBAL_Close();
      exit(EXIT_SUCCESS);
   }
   //
   // switch thread context, if necessary
   //
   if(fRpiDeamonMode) 
   {
      rpi_Deamon();
      iGuardsSecs = GUARD_SECS;
      //
      // Deamon mode:
      //    the HOST has exited already and we are now HOST and running in the background
      //
   }
   //
   //
   //
   GLOBAL_PidPut(PID_HOST, getpid());
   //
   if( LOG_ReportOpenFile(RPI_PUBLIC_LOG_PATH) == FALSE)
   {
      LOG_Report(errno, "RPI", "main():Log file open error");
      GEN_Printf("LDG: !----------------------------!" CRLF);
      GEN_Printf("LDG: !! Using STDERR as log-file !!" CRLF);
      GEN_Printf("LDG: !----------------------------!" CRLF);
   }
   else
   {
      LOG_printf("spicam():Logfile is %s" CRLF, RPI_PUBLIC_LOG_PATH);
   }
   GLOBAL_DUMPVARS("spicam");
   //
   rpi_InitSemaphore();
   //
   if(rpi_SignalRegister(&tBlockset) == FALSE) 
   {
      GLOBAL_Close();
      exit(EXIT_FAILURE);
   }
   //
   // Set up server socket 
   //
   LOG_Report(0, "RPI", "v%s:Build:%s (%s)", pstMap->G_pcVersionSw, RPI_BUILT, RPI_IO_BOARD);
   //
   if(pstMap->G_fFakeExec)
   {
      LOG_Report(0, "RPI", "Raspberry Pi - Running FAKE raspicam !!");
      LOG_printf("spicam(): Running FAKE raspicam" CRLF);
   }
   //
   //====================================================================================
   // Kickstart all supporting threads. Remember which threads will report back 
   // when their init has completed and we can presume.
   //====================================================================================
   //
   iGuards           = rpi_Startup(pstHid);
   pstMap->G_iGuards = iGuards;
   iGuardCount       = GUARD_COUNT;
   //
   // Sync startup of all threads
   //
   fRpiRunning = rpi_WaitStartup(iGuards);
   iCnxs       = GLOBAL_PidsLog();
   ulSecsCheck = RTC_GetSystemSecs() + iGuardsSecs;
   //
   GLOBAL_PidClearGuards();
   //
   if(fRpiRunning)
   {
      //
      // All threads running: update logfile with PIDs
      //
      LOG_Report(0, "RPI", "%d threads running OKee", iCnxs);
      LOG_printf("spicam():%d threads running OKee" CRLF, iCnxs);
   }
   else
   {
      //
      // Not all threads reported correct Init(): restart if release mode !
      //
      GLOBAL_SetSignalNotification(PID_HOST, GLOBAL_RPI_RST);
      LOG_Report(0, "RPI", "spicam():ERROR: NOT all %d threads are running OKee", iCnxs);
      LOG_printf("spicam():ERROR: NOT all %d threads are running OKee" CRLF, iCnxs);
   }
   //
   // In CMD-Line mode, we do not need the Support thread to check on us
   //
   if(!fRpiDeamonMode) 
   {
      LOG_printf("spicam():NOTICE : ****** Disable Support thread ******" CRLF);
      GEN_Signal(PID_SUPP, SIGUSR2);
   }
   //
   // Wait for signals
   //
/*======   Local Functions separator ===========================================
rpi_Main_Loop(){}
==============================================================================*/

   while(fRpiRunning)
   {
      switch( rpi_WaitSemaphore(2000) )
      {
         case 0:
            //
            // Sem was posted
            //
            if(GLOBAL_GetSignalNotification(PID_HOST, GLOBAL_RPI_END)) fRpiRunning = FALSE;
            if(GLOBAL_GetSignalNotification(PID_HOST, GLOBAL_BTN_NFY)) rpi_HandleButton();
            if(GLOBAL_GetSignalNotification(PID_HOST, GLOBAL_GRD_NFY)) rpi_HandleGuard();
            if(GLOBAL_GetSignalNotification(PID_HOST, GLOBAL_FLT_NFY)) fRpiRunning = rpi_SegmentationFault();
            break;

         case -1:
            //
            // Sem error
            //
            break;

         case 1:
            RPI_TESTIO();
            //
            // Timeout: Check guard at the end of the watch period (each GUARD_SECS secs)
            //
            ulSecsNow = RTC_GetSystemSecs();
            if(ulSecsNow > ulSecsCheck)
            {
               //
               // All active threads need to report back within GUARD_SECS secs.
               // If they didn't the service will be restarted by the WatchDog, 
               // who is triggered by PID_HOST through a SIGUSR1 (Restart) or 
               // SIGUSR2 (Reboot).
               //
               // G_stPidList[i]->pid_t       tPid   : The PID
               //                 int         iFlag  : The thread's guard marker
               //                 const char *pcName : The PID short-name
               //                 const char *pcHelp : The PID long-name
               //
               //
               // Check all threads if it has set its guard marker again
               // to signal it is still alive and kicking
               //
               if(GLOBAL_PidCheckGuards())
               {
                  //
                  // All threads reported back: Restart guards duty
                  //
                  iGuardCount  = GUARD_COUNT;
                  ulSecsCheck += iGuardsSecs;
               }
               else
               {
                  //
                  // Not all threads reported back: if multiple times in a row, take action
                  //
                  if(iGuardCount-- == 0)
                  {
                     //
                     // Problems: restart theApp
                     //
                     LOG_Report(0, "RPI", "rpi-main():Check Guard Error:Signal RPI-Dog to restart theApp!");
                     PRINTF("rpi-main():Check Guard ERROR: Restart theApp !" CRLF);
                     GLOBAL_SetSignalNotification(PID_HOST, GLOBAL_RPI_RST);
                     fRpiRunning = FALSE;
                  }
                  else
                  {
                     //
                     // Not all threads reported back: the missing one(s) were already triggered
                     //
                     RTC_ConvertDateTime(TIME_FORMAT_WW_DD_MM_YYYY_HH_MM_SS, cDateTime, ulSecsNow);
                     PRINTF2("rpi-main():Guard round found problem %d at %s" CRLF, GUARD_COUNT-iGuardCount, cDateTime);
                     LOG_Report(0, "RPI", "Guard round found problem %d at %s", GUARD_COUNT-iGuardCount, cDateTime);
                     ulSecsCheck += GUARD_SECS_RETRY;
                  }
               }
               GLOBAL_PidClearGuards();
               //RTC_ConvertDateTime(TIME_FORMAT_WW_DD_MM_YYYY_HH_MM_SS, cDateTime, ulSecsCheck);
               //PRINTF1("rpi-main():Check Guard: All is well. Next guard round is at %s" CRLF, cDateTime);
            }
            break;
      }
   }
/*======   Local Functions separator ===========================================
rpi_Exit_Loop(){}
==============================================================================*/
   //
   // Exit threads
   //
   RCU_EXIT();
   LCD_EXIT();
   //
   rpi_ExitSemaphore();
   //
   // Exit these threads: 
   //    o close all sockets
   //    o kill all child processes
   //    o clean up all I/O
   //
   LOG_printf("spicam():Go terminate threads..." CRLF);
   //
   // Cleanup background threads:
   //
   iCnxs = GLOBAL_PidsTerminate(2000);
   LOG_printf("spicam():Threads still running after terminate=%d" CRLF, iCnxs);
   //
   USB_ReleaseInfo(pstHid);
   //
   // In CMD-Line mode, we do not need the Watchdog thread to check on us
   // If Daemon mode make sure to get the Watchdog info now, so we can report errors
   //
   if(fRpiDeamonMode) 
   {
      tPid = GEN_GetPidByName(RPI_WATCHDOG);
      if(tPid > 0)
      {
         //
         // Watchdog is OKee:
         // Check how to proceed from here:
         //    o Restart the App
         //    o Reboot the system
         //    o Just terminate the App   
         //
         if(GLOBAL_GetSignalNotification(PID_HOST, GLOBAL_RPI_RST))
         {
            //
            // We need to restart the App: SIGUSR1 the watchdog
            //
            LOG_Report(0, "RPI", "Exit problems:Watchdog RESTARTS theApp !!!");
            GEN_SignalPid(tPid, SIGUSR1);
         }
         else if(GLOBAL_GetSignalNotification(PID_HOST, GLOBAL_RPI_RBT))
         {
            //
            // We need to reboot the system: SIGUSR2 the watchdog
            //
            LOG_Report(0, "RPI", "Exit problems:Watchdog REBOOTS !!!");
            GEN_SignalPid(tPid, SIGUSR2);
         }
      }
      else
      {
         LOG_Report(0, "RPI", "Exit problems and Watchdog not found : EmergencyReboot");
         fRpiEmergencyReboot = TRUE;
      }
   }
   else
   {
      if(GLOBAL_GetSignalNotification(PID_HOST, GLOBAL_RPI_RST)) GEN_Printf("CmdLine-Mode:Skipping RESTART" CRLF);
      if(GLOBAL_GetSignalNotification(PID_HOST, GLOBAL_RPI_RBT)) GEN_Printf("CmdLine-Mode:Skipping REBOOT" CRLF);
   }
   LOG_Report(0, "RPI", "v%s now EXIT", pstMap->G_pcVersionSw);
   LOG_ReportCloseFile();
   //
   //==============================================================================================
   // From here on :
   // CANNOT Use functions which use LOG_Report() anymore
   //==============================================================================================
   GEN_Printf("spicam():Safe memory allocation=%d" CRLF, safemalloccount());
   GLOBAL_Sync();
   GLOBAL_Close();
   //==============================================================================================
   // From here on :
   // CANNOT Use LOG_printf(), PRINTFx() or LOG_Report() due to the use of the G_tmutex !
   //==============================================================================================
   GEN_Printf("spicam():exit(EXIT_SUCCESS)" CRLF);
   //
   fflush(stderr);
   fflush(stdout);
   //
   if(fRpiEmergencyReboot)
   {
      //
      // Abort clean restart/reboot: Just REBOOT
      //
      system("sudo reboot");
      exit(EXIT_FAILURE);
   }
   else exit(EXIT_SUCCESS);
}

/* ======   Local Functions separator ===========================================
___LOCAL_FUNCTIONS(){}
==============================================================================*/


//
// Function:   rpi_Deamon
// Purpose:    Run the Raspberry Pi HTTP mini-server as a deamon, rather than the shelled version
//             
// Parms:       
// Returns:     
//
static pid_t rpi_Deamon(void)
{
   pid_t    tPid;

   tPid = fork();
   //
   switch(tPid)
   {
      default:
         // Parent (Commandline or service) 
         PRINTF("rpi-Deamon(): Parent TERMINATES now." CRLF);
         exit(EXIT_SUCCESS);
         break;

      case 0:
         // Oldest child: RPI HTTP-server
         PRINTF("rpi-Deamon(): spicam running background mode" CRLF);
         sleep(2);
         break;

      case -1:
         // Error
         GEN_Printf("rpi-Deamon(): Error!"CRLF);
         exit(EXIT_FAILURE);
         break;
   }
   return(tPid);
}

//
//  Function:   rpi_Version
//  Purpose:    Show the version string
//
//  Parms:
//  Returns:
//
static void rpi_Version(void)
{
   char     cLog[DATE_TIME_SIZE];

   RTC_GetDateTimeSecs(cLog);
   //
   GEN_Printf(CRLF);
   GEN_Printf("Raspberry Pi: Dynamic HTTP Server v%s" CRLF, pstMap->G_pcVersionSw);
   GEN_Printf("   Time  : %s" CRLF, cLog);
   GEN_Printf("   Built : %s" CRLF, RPI_BUILT);
   GEN_Printf("   Home  : http://www.patrn.nl" CRLF);
   GEN_Printf(CRLF);
   //
   fflush(stdout);
}

//
//  Function:   rpi_Help
//  Purpose:
//
//  Parms:
//  Returns:
//
static void rpi_Help(void)
{
   rpi_Version();
   //
   GEN_Printf("   Sizeof int       = %d" CRLF, sizeof(int));
   GEN_Printf("   Sizeof long      = %d" CRLF, sizeof(long));
   GEN_Printf("   Sizeof long long = %d" CRLF, sizeof(long long));
   GEN_Printf("   Sizeof float     = %d" CRLF, sizeof(float));
   GEN_Printf("   Sizeof double    = %d" CRLF, sizeof(double));
   GEN_Printf("   Sizeof RPIMAP    = %d" CRLF, sizeof(RPIMAP));
   GEN_Printf("   Sizeof GLOCMD    = %d" CRLF, sizeof(GLOCMD));
   GEN_Printf("   Sizeof MDET      = %d" CRLF, sizeof(MDET));
   GEN_Printf("   Sizeof GLOG      = %d" CRLF, sizeof(GLOG));
   GEN_Printf(CRLF);
   //
   GEN_Printf("Usage : spicam [-options] " CRLF);
   GEN_Printf(CRLF);
   GEN_Printf("Available options:" CRLF);
   GEN_Printf("  -dxxx\t\tDebug mask" CRLF);
   GEN_Printf("  -kxxx\t\tKodi RCU Port" CRLF);
   GEN_Printf("  -pxxx\t\tServer Port" CRLF);
   GEN_Printf("  -D\t\tRun spicam Http as background daemon" CRLF);
   GEN_Printf("  -F\t\tUse FAKE raspicam" CRLF);
   GEN_Printf("  -h\t\tThis help" CRLF);
   GEN_Printf("  -v\t\tShow version number" CRLF);
   GEN_Printf(CRLF);
}



//
//  Function:   rpi_SignalRegister
//  Purpose:    Register all SIGxxx
//
//  Parms:      Blockset
//  Returns:    TRUE if delivered
//
static bool rpi_SignalRegister(sigset_t *ptBlockset)
{
  bool fCC = TRUE;

  // SIGUSR1:
  if( signal(SIGUSR1, &rpi_ReceiveSignalUser1) == SIG_ERR)
  {
     LOG_Report(errno, "RPI", "rpi-SignalRegister(): SIGUSR1 ERROR");
     fCC = FALSE;
  }
  // SIGUSR2:
  if( signal(SIGUSR2, &rpi_ReceiveSignalUser2) == SIG_ERR)
  {
     LOG_Report(errno, "RPI", "rpi-SignalRegister(): SIGUSR2 ERROR");
     fCC = FALSE;
  }
  // CTL-X handler
  if( signal(SIGTERM, &rpi_ReceiveSignalTerm) == SIG_ERR)
  {
     LOG_Report(errno, "RPI", "rpi-SignalRegister(): SIGTERM ERROR");
     fCC = FALSE;
  }
  // Ctl-C handler
  if( signal(SIGINT, &rpi_ReceiveSignalInt) == SIG_ERR)
  {
     LOG_Report(errno, "RPI", "rpi-SignalRegister(): SIGINT ERROR");
     fCC = FALSE;
  }
  // Segmentation fault
  if( signal(SIGSEGV, &rpi_ReceiveSignalSegmnt) == SIG_ERR)
  {
     LOG_Report(errno, "RPI", "rpi-ReceiveSignalSegmnt(): SIGSEGV ERROR");
     fCC = FALSE;
  }

  if(fCC)
  {
     //
     // Setup the SIGxxx events
     //
     sigemptyset(ptBlockset);
     sigaddset(ptBlockset, SIGTERM);
     sigaddset(ptBlockset, SIGINT);
     sigaddset(ptBlockset, SIGUSR1);
     sigaddset(ptBlockset, SIGUSR2);
     sigaddset(ptBlockset, SIGSEGV);
  }
  return(fCC);
}

//
//  Function:   rpi_ReceiveSignalTerm
//  Purpose:    Add the SIGTERM command in the buffer (Ctl-X)
//
//  Parms:
//  Returns:    TRUE if delivered
//
static void rpi_ReceiveSignalTerm(int iSignal)
{
   LOG_Report(0, "RPI", "rpi-ReceiveSignalTerm()");
   //
   fRpiRunning = FALSE;
   rpi_PostSemaphore();
}

//
//  Function:   rpi_ReceiveSignalInt
//  Purpose:    Add the SIGINT command in the buffer (Ctl-C)
//
//  Parms:
//  Returns:    TRUE if delivered
//
static void rpi_ReceiveSignalInt(int iSignal)
{
   LOG_Report(0, "RPI", "rpi-ReceiveSignalInt()");
   //
   fRpiRunning = FALSE;
   rpi_PostSemaphore();
}

//
// Function:   rpi_ReceiveSignalSegmnt
// Purpose:    Handle the SIGSEGV
//
// Parms:
// Returns:    TRUE if delivered
// Note:       Do NOT use LOG_xxx() here
//
static void rpi_ReceiveSignalSegmnt(int iSignal)
{
   int   iNr;
   pid_t tPid;

   if(fRpiRunning)
   {
      fRpiRunning = FALSE;
      //
      GLOBAL_SetSignalNotification(PID_HOST, GLOBAL_FLT_NFY);
      GEN_SegmentationFault(__FILE__, __LINE__);
      LOG_SegmentationFault(__FILE__, __LINE__);
      //
      // (TRY TO) Cleanup background threads:
      //
      for(iNr=0; iNr<NUM_PIDT; iNr++)
      {
         tPid = GLOBAL_PidGet(iNr);
         if( (tPid > 0) && (tPid != getpid()) )
         {
            GEN_SignalPid(tPid, SIGINT);
         }
      }
   }
   rpi_PostSemaphore();
}

//
// Function:   rpi_ReceiveSignalUser1
// Purpose:    SIGUSR1 signal
//
// Parms:
// Returns:    
// Note:       SIGUSR1 is used to signal thread comms
//
static void rpi_ReceiveSignalUser1(int iSignal)
{
   rpi_PostSemaphore();
}

//
// Function:   rpi_ReceiveSignalUser2
// Purpose:    System callback on receiving the SIGUSR2 signal
//
// Parms:
// Returns:    
// Note:       SIGUSR2
//
static void rpi_ReceiveSignalUser2(int iSignal)
{
}

/* ======   Local Functions separator ===========================================
___MISC_FUNCTIONS(){}
==============================================================================*/

//
// Function    : rpi_GlobalsInit
// Description : Init all global variables
// 
// Parameters  : MMAP ptr
// Returns     : 
//
static void rpi_GlobalsInit(RPIMAP *pstMap)
{
   //
   // Clear virtual LCD screen, Dynamic Page register and global CMDs
   //
   GEN_MEMSET( pstMap->G_stVirtLcd.pcVirtLcd, ' ', LCD_VIRTUAL_ROWS * LCD_VIRTUAL_COLS);
   //
   pstMap->G_stVirtLcd.ubVirtRows = LCD_VIRTUAL_ROWS;
   pstMap->G_stVirtLcd.ubVirtCols = LCD_VIRTUAL_COLS;
}

//
// Function:   rpi_HandleButton
// Purpose:    Handle the buttons and RCU keys
//
// Parms:      
// Returns:    TRUE if OKee
// Note:       
//
static bool rpi_HandleButton()
{
   bool  fCc=FALSE;
   int   iKey;

   iKey = GLOBAL_KeyGet();
   if(iKey != RCU_NOKEY)
   {
      LOG_Report(0, "RPI", "rpi-HandleButton():Key=%d", iKey);
      fCc = TRUE;
   }
   else
   {
      LOG_Report(0, "RPI", "rpi-HandleButton():Keybuffer is empty");
   }
   return(fCc);
}

#ifdef   FEATURE_TEST_IO
//
//  Function:  rpi_TestIo
//  Purpose:   
//
//  Parms:     
//  Returns:   
//
static void rpi_TestIo()
{
   static int iMask=1;

   int   i, iIn=0, iBt=1;

   for(i=0; i<8; i++)
   {
      LED(i, iMask & iBt);
      iBt = iBt << 1;
      //
      iIn = iIn << 1;
      if(BUTTON(i)) iIn |= 1;
   }
   LOG_printf("rpi-TestIo():Mask  =0x%x" CRLF, iMask);
   LOG_printf("rpi-TestIo():Inputs=0x%x" CRLF, iIn);
   //
   iMask = iMask << 1;
   if(iMask == 0x100) iMask = 1;
}
#endif   //FEATURE_TEST_IO

//
//  Function:  rpi_HandleGuard
//  Purpose:   Handle the guard notification
//
//  Parms:     
//  Returns:   
//  Note:      One or more threads did not respond to the generic guard duty. A signal
//             GLOBAL_GRD_RUN has been send and now replied with GLOBAL_GRD_NFY.
//
static bool rpi_HandleGuard()
{
   LOG_Report(0, "RPI", "rpi-HandleGuard():Notification received");
   return(TRUE);
}

//
//  Function:  rpi_Startup
//  Purpose:   Startup all threads
//
//  Parms:     HID struct (if enabled)
//  Returns:   Init markers
//
static int rpi_Startup(RPIUSB *pstHid)
{
   int   iStup;

   //
   //
   // Start aux threads:
   //    o Http Server
   //    o Camera
   //    o Command handler
   //    o Kodi RCU
   //    o Proxy Server
   //    o Proxy Comms
   //    o Support
   //    o USB Daemon
   //    o USB Debug
   //
   iStup  = SRVR_Init();
   iStup |= CAM_INIT();
   iStup |= CMD_Init();
   iStup |= BTN_Init();
   iStup |= KODI_Init();
   iStup |= PROXY_Init();
   iStup |= SUP_Init();
   //
   // config threads:
   //
   iStup |= RCU_INIT();
   iStup |= LCD_INIT();
   //
   // Setup USB comms to HID clients
   //
   if(pstHid)
   {
      iStup |= USB_Daemon(pstHid);
      iStup |= USB_Debug(pstHid);
   }
   return(iStup);
}

//
//  Function:  rpi_WaitStartup
//  Purpose:   Wait until all threads are doing fine
//
//  Parms:     Initial flags of all threads which started
//  Returns:   True if all OK
//
static bool rpi_WaitStartup(int iInit)
{
   bool     fWaiting=TRUE;
   bool     fCc=FALSE;
   int      iStartup=0, iHalfSecs = 20;

#ifdef FEATURE_LOG_RPI_INIT
   int      iFlag;

   iFlag = GLOBAL_SetSignalNotification(PID_HOST, 0);
   LOG_Report(0, "RPI", "rpi-WaitStartup(): ===:Flag=0x%08X -->0x%08X)", iFlag, iInit);
#endif   //FEATURE_LOG_RPI_INIT

   //
   // Mask only INIT threads
   //
   iInit &= GLOBAL_XXX_INI;
   //
   while(fWaiting)
   {
      switch( GEN_WaitSemaphore(&pstMap->G_tSemRpi, 1000) )
      {
         case 0:
            // Thread signalled: verify completion

#ifdef FEATURE_LOG_RPI_INIT

            iFlag = GLOBAL_SetSignalNotification(PID_HOST, 0);
            LOG_Report(0, "RPI", "rpi-WaitStartup(): ---:Flag=0x%08X)", iFlag);
            //
            if(GLOBAL_GetSignalNotification(PID_HOST, GLOBAL_SVR_INI)) 
            { 
               LOG_Report(0, "RPI", "rpi-WaitStartup():SVR OKee"); 
               iStartup |= GLOBAL_SVR_INI;
               //
               iFlag = GLOBAL_SetSignalNotification(PID_HOST, 0);
               LOG_Report(0, "RPI", "rpi-WaitStartup(): SRV(001):Flag=0x%08X)", iFlag);
            }
            //
            if(GLOBAL_GetSignalNotification(PID_HOST, GLOBAL_CAM_INI)) 
            { 
               LOG_Report(0, "RPI", "rpi-WaitStartup():CAM OKee"); 
               iStartup |= GLOBAL_CAM_INI;
               //
               iFlag = GLOBAL_SetSignalNotification(PID_HOST, 0);
               LOG_Report(0, "RPI", "rpi-WaitStartup(): CAM(002):Flag=0x%08X)", iFlag);
            }
            //
            if(GLOBAL_GetSignalNotification(PID_HOST, GLOBAL_CMD_INI)) 
            { 
               LOG_Report(0, "RPI", "rpi-WaitStartup():CMD OKee"); 
               iStartup |= GLOBAL_CMD_INI;
               //
               iFlag = GLOBAL_SetSignalNotification(PID_HOST, 0);
               LOG_Report(0, "RPI", "rpi-WaitStartup(): CMD(004):Flag=0x%08X)", iFlag);
            }
            //
            if(GLOBAL_GetSignalNotification(PID_HOST, GLOBAL_BTN_INI)) 
            { 
               LOG_Report(0, "RPI", "rpi-WaitStartup():BTN OKee"); 
               iStartup |= GLOBAL_BTN_INI;
               //
               iFlag = GLOBAL_SetSignalNotification(PID_HOST, 0);
               LOG_Report(0, "RPI", "rpi-WaitStartup(): BTN(400):Flag=0x%08X)", iFlag);
            }
            //
            if(GLOBAL_GetSignalNotification(PID_HOST, GLOBAL_KOD_INI)) 
            { 
               LOG_Report(0, "RPI", "rpi-WaitStartup():KOD OKee"); 
               iStartup |= GLOBAL_KOD_INI;
               //
               iFlag = GLOBAL_SetSignalNotification(PID_HOST, 0);
               LOG_Report(0, "RPI", "rpi-WaitStartup(): KOD(008):Flag=0x%08X)", iFlag);
            }
            //
            if(GLOBAL_GetSignalNotification(PID_HOST, GLOBAL_PRX_INI)) 
            { 
               LOG_Report(0, "RPI", "rpi-WaitStartup():PRX OKee"); 
               iStartup |= GLOBAL_PRX_INI;
               //
               iFlag = GLOBAL_SetSignalNotification(PID_HOST, 0);
               LOG_Report(0, "RPI", "rpi-WaitStartup(): PRX(010):Flag=0x%08X)", iFlag);
            }
            //
            if(GLOBAL_GetSignalNotification(PID_HOST, GLOBAL_SUP_INI)) 
            { 
               LOG_Report(0, "RPI", "rpi-WaitStartup():SUP OKee"); 
               iStartup |= GLOBAL_SUP_INI;
               //
               iFlag = GLOBAL_SetSignalNotification(PID_HOST, 0);
               LOG_Report(0, "RPI", "rpi-WaitStartup(): SUP(020):Flag=0x%08X)", iFlag);
            }
            //
            if(GLOBAL_GetSignalNotification(PID_HOST, GLOBAL_UDM_INI)) 
            { 
               LOG_Report(0, "RPI", "rpi-WaitStartup():UDM OKee"); 
               iStartup |= GLOBAL_UDM_INI;
               //
               iFlag = GLOBAL_SetSignalNotification(PID_HOST, 0);
               LOG_Report(0, "RPI", "rpi-WaitStartup(): UDM(040):Flag=0x%08X)", iFlag);
            }
            //
            if(GLOBAL_GetSignalNotification(PID_HOST, GLOBAL_UDB_INI)) 
            { 
               LOG_Report(0, "RPI", "rpi-WaitStartup():UDB OKee"); 
               iStartup |= GLOBAL_UDB_INI;
               //
               iFlag = GLOBAL_SetSignalNotification(PID_HOST, 0);
               LOG_Report(0, "RPI", "rpi-WaitStartup(): UDB(080):Flag=0x%08X)", iFlag);
            }

#else    //FEATURE_LOG_RPI_INIT
            if(GLOBAL_GetSignalNotification(PID_HOST, GLOBAL_SVR_INI)) { LOG_Report(0, "RPI", "rpi-WaitStartup():SVR OKee"); iStartup |= GLOBAL_SVR_INI;}
            if(GLOBAL_GetSignalNotification(PID_HOST, GLOBAL_CAM_INI)) { LOG_Report(0, "RPI", "rpi-WaitStartup():CAM OKee"); iStartup |= GLOBAL_CAM_INI;}
            if(GLOBAL_GetSignalNotification(PID_HOST, GLOBAL_CMD_INI)) { LOG_Report(0, "RPI", "rpi-WaitStartup():CMD OKee"); iStartup |= GLOBAL_CMD_INI;}
            if(GLOBAL_GetSignalNotification(PID_HOST, GLOBAL_BTN_INI)) { LOG_Report(0, "RPI", "rpi-WaitStartup():BTN OKee"); iStartup |= GLOBAL_BTN_INI;}
            if(GLOBAL_GetSignalNotification(PID_HOST, GLOBAL_KOD_INI)) { LOG_Report(0, "RPI", "rpi-WaitStartup():KOD OKee"); iStartup |= GLOBAL_KOD_INI;}
            if(GLOBAL_GetSignalNotification(PID_HOST, GLOBAL_PRX_INI)) { LOG_Report(0, "RPI", "rpi-WaitStartup():PRX OKee"); iStartup |= GLOBAL_PRX_INI;}
            if(GLOBAL_GetSignalNotification(PID_HOST, GLOBAL_SUP_INI)) { LOG_Report(0, "RPI", "rpi-WaitStartup():SUP OKee"); iStartup |= GLOBAL_SUP_INI;}
            if(GLOBAL_GetSignalNotification(PID_HOST, GLOBAL_UDM_INI)) { LOG_Report(0, "RPI", "rpi-WaitStartup():UDM OKee"); iStartup |= GLOBAL_UDM_INI;}
            if(GLOBAL_GetSignalNotification(PID_HOST, GLOBAL_UDB_INI)) { LOG_Report(0, "RPI", "rpi-WaitStartup():UDB OKee"); iStartup |= GLOBAL_UDB_INI;}
#endif   //FEATURE_LOG_RPI_INIT
            
            if(iStartup == iInit)
            {
               LOG_Report(0, "RPI", "rpi-WaitStartup():All inits OKee");
               fWaiting = FALSE;
               fCc      = TRUE;
            }
            else
            {
               PRINTF2("rpi-WaitStartup(): Waiting for 0x%08X (now 0x%08X)" CRLF, iInit, iStartup);
            }
            break;

         case -1:
            // Error
            fWaiting = FALSE;
            break;

         case 1:
            if(iHalfSecs-- == 0)
            {
               // Timeout: problems
               LOG_Report(0, "RPI", "rpi-WaitStartup(): Timeout: Init=0x%08X, Actual==0x%08X", iInit, iStartup);
               PRINTF2("rpi-WaitStartup(): Timeout: Init=0x%08X, Actual==0x%08X" CRLF, iInit, iStartup);
               fWaiting = FALSE;
            }
#ifdef FEATURE_LOG_RPI_INIT
            else
            {
               iFlag = GLOBAL_SetSignalNotification(PID_HOST, 0);
               PRINTF1("rpi-WaitStartup(): TO.:Flag=0x%08X)" CRLF, iFlag);
            }
#endif   //FEATURE_LOG_RPI_INIT
            break;
      }
   }
   return(fCc);
}

//
//  Function:  rpi_SegmentationFault
//  Purpose:   Hndle segmentation fault
//
//  Parms:     
//  Returns:   FALSE (to exit the main loop)
//
static bool rpi_SegmentationFault()
{       
   LOG_Report(0, "RPI", "rpi-SegmentationFault():Pid=%d, %s Line %d" CRLF, 
                     pstMap->G_iSegFaultPid, pstMap->G_pcSegFaultFile, pstMap->G_iSegFaultLine);
   return(FALSE);
}


/*------  Local functions separator ------------------------------------
__SEMAPHORE_FUNCTIONS(){};
----------------------------------------------------------------------------*/

//
// Function:   rpi_InitSemaphore
// Purpose:    Init the Semaphore
//
// Parms:
// Returns:
// Note:       If pshared has the value 0, then the semaphore is shared between the
//             threads of a process, and should be located at some address that is
//             visible to all threads (e.g., a global variable, or a variable
//             allocated dynamically on the heap).
//             If pshared is nonzero, then the semaphore is shared between
//             processes, and should be located in a region of shared memory.
//
//             The sem value is always >= 0:
//                               0 means LOCKED
//                              >0 means UNLOCKED
//             Init(sem 1, VAL)  VAL=1 unlocked
//                               VAL=0 locked
//             Wait(sem)         LOCK:    if(VAL> 0) { VAL--; return;         } 
//                                        if(VAL==0) { Wait till > 0; return; }
//             Post(sem)         UNLOCK:  VAL++
//
static void rpi_InitSemaphore(void)
{
    //
    // Init the semaphore as UNLOCKED
    //
    if(sem_init(&pstMap->G_tSemRpi, 1, 0) == -1)
    {
        LOG_Report(errno, "RPI", "rpi-InitSemaphore(): sem_init Error");
    }
}

//
// Function:   rpi_ExitSemaphore
// Purpose:    Exit the Semaphore
//
// Parms:
// Returns:
// Note:
//
static void rpi_ExitSemaphore(void)
{
    if( sem_destroy(&pstMap->G_tSemRpi) == -1) LOG_Report(errno, "RPI", "rpi-ExitSemaphore():sem_destroy");
}

//
// Function:   rpi_PostSemaphore
// Purpose:    Unlock the Server Semaphore
//
// Parms:      
// Returns:    
// Note:       sem_post() increments (unlocks) the semaphore pointed to by sem. If the semaphore's 
//             value consequently becomes greater than zero, then another process or thread blocked
//             in a sem_wait(3) call will be woken up and proceed to lock the semaphore. 
//
static void rpi_PostSemaphore()
{
   //
   // sem++ (UNLOCK)   
   //
   if(sem_post(&pstMap->G_tSemRpi) == -1) LOG_Report(errno, "RPI", "rpim_PostSemaphore(): sem_post error");
}

//
// Function:   rpi_WaitSemaphore
// Purpose:    Wait till Semaphore has been unlocked, then LOCK and proceed
//
// Parms:      TIMEOUT (or -1 if wait indefinitely)
// Returns:     0 if sem unlocked
//             +1 if sem timeout
//             -1 on error (errno)
// Note:       sem_wait() tries to decrement (lock) the semaphore. 
//             If the semaphore currently has the value zero, then the call blocks until 
//             either it becomes possible to perform the decrement (the semaphore value rises 
//             above zero) or a signal handler interrupts the call. 
//             If the semaphore's value is greater than zero, then the decrement proceeds, 
//             and the function returns immediately. 
//
//             sem_timedwait() is the same as sem_wait(), except that TIMEOUT specifies a 
//             limit on the amount of time that the call should block if the decrement cannot 
//             be immediately performed. The stTime points to a structure that
//             specifies an absolute timeout in seconds and nanoseconds since the Epoch, 
//             1970-01-01 00:00:00 +0000 (UTC). This structure is defined as follows:
//
//             struct timespec 
//             {
//                time_t tv_sec;      /* Seconds */
//                long   tv_nsec;     /* Nanoseconds [0 .. 999999999] */
//             };
//             If the timeout has already expired by the time of the call, and the semaphore 
//             could not be locked immediately, then sem_timedwait() fails with a timeout error 
//             (errno set to ETIMEDOUT). If the operation can be performed immediately, then 
//             sem_timedwait() never fails with a timeout error, regardless of the value of 
//             TIMEOUT.
//
//
static int rpi_WaitSemaphore(int iMsecs)
{
   //
   // if    sem == 0 : wait
   // else  sem-- (LOCK)   
   //
   return(GEN_WaitSemaphore(&pstMap->G_tSemRpi, iMsecs));
}

