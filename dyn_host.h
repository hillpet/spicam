/*  (c) Copyright:  2017  Patrn, Confidential Data
 *
 *  Workfile:           dyn_host.h
 *  Revision:          
 *  Modtime:           
 *
 *  Purpose:            Extract headerfile for RPI dynamic http pages
 *
 *
 *
 *  Compiler/Assembler: Raspbian Linux GNU gcc
 *  Ext Packages:
 *
 *  Author:             Peter Hillen
 *  Date Created:       09 Mar 2017
 *
 *  Revisions:
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
**/

//
// The default (empty) URL page MUST be the first in this list
//
EXTRACT_DYN(RPI_DYN_HTML_EMPTY,    HTTP_HTML,  0, DYN_FLAG_PORT, "",                  http_DynPageEmpty          )
EXTRACT_DYN(RPI_DYN_HTML_INFO,     HTTP_HTML,  0, DYN_FLAG_PORT, "info.html",         http_DynPageDefault        )
EXTRACT_DYN(RPI_DYN_JSON_INFO,     HTTP_JSON,  0, DYN_FLAG_PORT, "info.json",         http_DynPageDefault        )
EXTRACT_DYN(RPI_DYN_JSON_PARMS,    HTTP_JSON,  0, DYN_FLAG_PORT, "parms.json",        http_DynPageParms          )
EXTRACT_DYN(RPI_DYN_HTML_PARMS,    HTTP_HTML,  0, DYN_FLAG_PORT, "parms.html",        http_DynPageParms          )
EXTRACT_DYN(RPI_DYN_JSON_ALARM,    HTTP_JSON,  0, DYN_FLAG_PORT, "alarm.json",        http_DynPageAlarm          )
EXTRACT_DYN(RPI_DYN_HTMP_FOTOS,    HTTP_HTML,  0, DYN_FLAG_PORT, "fotos.html",        http_DynPageFotos          )
EXTRACT_DYN(RPI_DYN_JSON_FOTOS,    HTTP_JSON,  0, DYN_FLAG_PORT, "fotos.json",        http_DynPageFotos          )
EXTRACT_DYN(RPI_DYN_HTMP_VIDEOS,   HTTP_HTML,  0, DYN_FLAG_PORT, "videos.html",       http_DynPageVideos         )
EXTRACT_DYN(RPI_DYN_JSON_VIDEOS,   HTTP_JSON,  0, DYN_FLAG_PORT, "videos.json",       http_DynPageVideos         )
EXTRACT_DYN(RPI_DYN_HTML_CMD,      HTTP_HTML,  0, DYN_FLAG_PORT, "cmd.html",          http_DynPageShell          )
EXTRACT_DYN(RPI_DYN_JSON_CMD,      HTTP_JSON,  0, DYN_FLAG_PORT, "cmd.json",          http_DynPageShell          )
EXTRACT_DYN(RPI_DYN_HTML_LOG,      HTTP_HTML,  0, DYN_FLAG_PORT, "log.html",          http_DynPageLog            )
EXTRACT_DYN(RPI_DYN_SHRT_LOG,      HTTP_HTML,  0, DYN_FLAG_PORT, "log",               http_DynPageLog            )
EXTRACT_DYN(RPI_DYN_JSON_LOG,      HTTP_JSON,  0, DYN_FLAG_PORT, "log.json",          http_DynPageLog            )
EXTRACT_DYN(RPI_DYN_HTML_SCREEN,   HTTP_HTML,  0, DYN_FLAG_PORT, "screen.html",       http_DynPageScreen         )
EXTRACT_DYN(RPI_DYN_SHRT_SCREEN,   HTTP_HTML,  0, DYN_FLAG_PORT, "screen",            http_DynPageScreen         )
EXTRACT_DYN(RPI_DYN_JSON_SCREEN,   HTTP_JSON,  0, DYN_FLAG_PORT, "screen.json",       http_DynPageScreen         )
EXTRACT_DYN(RPI_DYN_HTML_MOTION,   HTTP_HTML,  0, DYN_FLAG_PORT, "motion.html",       http_DynPageMotion         )
EXTRACT_DYN(RPI_DYN_JSON_MOTION,   HTTP_JSON,  0, DYN_FLAG_PORT, "motion.json",       http_DynPageMotion         )
EXTRACT_DYN(RPI_DYN_HTML_EXIT,     HTTP_HTML,  0, DYN_FLAG_NONE, "exit.html",         http_DynPageExit           )
EXTRACT_DYN(RPI_DYN_JSON_EXIT,     HTTP_JSON,  0, DYN_FLAG_PORT, "exit.json",         http_DynPageExit           )
EXTRACT_DYN(RPI_DYN_HTML_MOT,      HTTP_HTML,  0, DYN_FLAG_PORT, "md.html",           http_DynPageMd             )
EXTRACT_DYN(RPI_DYN_SHRT_MOT,      HTTP_HTML,  0, DYN_FLAG_PORT, "md",                http_DynPageMd             )
EXTRACT_DYN(RPI_DYN_JSON_MOT,      HTTP_JSON,  0, DYN_FLAG_PORT, "md.json",           http_DynPageMd             )
