/*  (c) Copyright:  2013..2017  Patrn.nl, Confidential Data
 *
 *  Workfile:           cmd_net.h
 *  Revision:  
 *  Modtime:   
 *
 *  Purpose:            cmd_net header file
 *
 *  Compiler/Assembler: Raspbian Linux GNU gcc
 *  Ext Packages:
 * 
 *  Author:             Peter Hillen
 *  Date Created:       5 Dec 2013
 * 
 *  Revisions:
 *
 * 
 * 
 * 
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
**/

#ifndef _CMD_NET_H_
#define _CMD_NET_H_

//
// Global prototypes
//
pid_t IP_SendToServer         (char *);

#endif  /*_CMD_NET_H_ */

