/*  (c) Copyright:  2012..2108  Patrn, Confidential Data  
 *
 *  $Workfile:          rpi_page.c
 *  $Revision:          $
 *  $Modtime:           $
 *
 *  Purpose:            Dynamic HTTP page for Raspberry pi webserver
 *
 *
 *  Entry Points:       
 *
 *  Compiler/Assembler: gnu gcc
 *  Ext Packages:
 *
 *  Author:             Peter Hillen
 *  Date Created:       16 Jul 2012
 *
 *  Revisions:
 *    $Log:   $
 *
 *
 *
 *
 *
 *
 *
 *
**/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdarg.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/wait.h>

#include <common.h>
#include "config.h"
#include "globals.h"
#include "gen_http.h"
#include "gen_func.h"
#include "cmd_cad.h"
#include "rpi_func.h"
#include "rpi_json.h"
//
#include "rpi_page.h"

//#define USE_PRINTF
#include <printf.h>

//
// Generic HTTP data from common lib
//
extern const char *pcHttpResponseHeader;
extern const char *pcWebPageStart;
extern const char *pcWebPageTitle;
extern const char *pcWebPageDefault;
extern const char *pcWebPageEnd;

//
// Local prototypes
//
static bool    http_DynPageAlarm             (NETCL *, int);
static bool    http_DynPageDefault           (NETCL *, int);
static bool    http_DynPageEmpty             (NETCL *, int);
static bool    http_DynPageExit              (NETCL *, int);
static bool    http_DynPageFotos             (NETCL *, int);
static bool    http_DynPageMotion            (NETCL *, int);
static bool    http_DynPageMd                (NETCL *, int);
static bool    http_DynPageLog               (NETCL *, int);
static bool    http_DynPageParms             (NETCL *, int);
static bool    http_DynPageScreen            (NETCL *, int);
static bool    http_DynPageScreenHtml        (NETCL *, int);
static bool    http_DynPageScreenJson        (NETCL *, int);
static bool    http_DynPageShell             (NETCL *, int);
static bool    http_DynPageShellHtml         (NETCL *, int);
static bool    http_DynPageShellJson         (NETCL *, int);
static bool    http_DynPageVideos            (NETCL *, int);
//
// Local prototypes
//
static void    http_CheckVirtualData         (char *);
static char   *http_ExtractPageName          (NETCL *);

static int     http_ReadTextFile             (FILE *, char *, int);
static bool    http_ShowFiles                (NETCL *, char *, char *, const char *);
//
// Host dynamin HTTP pages
//
#define  EXTRACT_DYN(a,b,c,d,e,f)   {a,b,c,d,e,f},
const NETDYN stHostDynamicPages[] =
{
//    tUrl,    tType,   iTimeout,   iFlags,  pcUrl,   pfDynCb;
   #include "dyn_host.h"
   {  -1,      0,       0,          0,       NULL,    NULL  }
};
#undef EXTRACT_DYN

//
// HTTP web data
//
static const RPINFO stVirtualInfo[] = 
{  // trigger              title                row   col   offs  length
   { "top -",              "",                   2,    0,    6,   80    },
   { "MemTotal",           NULL,                 3,    0,    0,   80    },
   { "MemFree",            NULL,                 4,    0,    0,   80    },
   //
   { NULL,                 NULL,                 0,    0,    0,   0     }
};

//
// Misc local http header strings 
//
static const char *pcWebPageShell            = "RaspberryPi$ %s";
//
static const char *pcWebPageLineBreak        = "<br/>";
static const char *pcWebPagePreStart         = "<div align=\"left\"><pre>";
static const char *pcWebPagePreEnd           = "</pre></div> ";
static const char *pcWebPageShellBad         = "<p>Cmd shell: bad request (errno=%d)</p>" NEWLINE;
static const char *pcWebPageContentBad       = "<p>Raspicam: No content found !</p>" NEWLINE;
static const char *pcWebRequestPicture       = "<img src=\"%s\" width=\"%s\" height=\"%s\"></img>";
//
static const char *pcWebPageFontStart        = "<font face=\"%s\">";
static const char *pcWebPageFontEnd          = "</font>";
//
static const char *pcFotoHeader              = "RaspBerry Pi Camera Foto's";
static const char *pcVideoHeader             = "RaspBerry Pi Camera Video's";
//
// HTTP Response:
//=============================
//    HTTP Response header
//    NEWLINE
//    Optional Message body
//    NEWLINE NEWLINE
//
static const char *pcHttpResponseHeaderJson  = "HTTP/1.0 200 OK" NEWLINE "Content-Type=application/json" NEWLINE NEWLINE;
static const char *pcHttpResponseHeaderBad   = "HTTP/1.0 400 Bad Request" NEWLINE NEWLINE;
//
static const char *pcWebPageEmpty            = "<br/>" NEWLINE \
                                               "Welcome to another PATRN mini HTTP server." NEWLINE \
                                               "<a href=\"http://www.patrn.nl\" target=\"_blank\">Patrn.nl</a>" NEWLINE \
                                               "<br/>" NEWLINE;
//
static const char *pcWebExitPage             = "<br/>" NEWLINE \
                                               "<br/>" NEWLINE \
                                               "Exiting PATRN HTTP server. Good bye !" NEWLINE \
                                               "<br/>" NEWLINE \
                                               "<br/>" NEWLINE;
//
// HTTP table start: Needs border, width(%), colspan, headerstring
//
static const char *pcWebPageTableStart       = "<table align=center border=%d width=\"%d%%\">" NEWLINE \
                                               "<body style=\"text-align:left\">" NEWLINE
                                               "<thead>" NEWLINE \
                                               "<th colspan=%d>%s</th>" NEWLINE;
//
// HTTP table end
//
static const char *pcWebPageTableEnd         = "</tbody></table>" NEWLINE;
//
// HTTP table row: Needs row height
//
static const char *pcWebPageTableRowStart    = "<tr height=%d>" NEWLINE;
//
// HTTP table row end
//
static const char *pcWebPageTableRowEnd      = "</tr>" NEWLINE;
//
// HTTP table cel link data : Needs [cell width,] link, data
//
static const char *pcWebPageText             = "[%s]" NEWLINE;
//
// HTTP table cel data: Needs [cell width,] data
//
static const char *pcWebPageCellData[]       =
{
   "<td>%s</td>" NEWLINE,
   "<td width=\"%d%%\">%s</td>" NEWLINE NEWLINE
};
//
// HTTP table cel link data : Needs [cell width,] link, data
//
static const char *pcWebPageCellDataLink[]   =
{
   "<td><a href=\"%s\">%s</td>" NEWLINE,
   "<td width=\"%d%%\"><a href=\"%s\">%s</td>" NEWLINE
};
//
// HTTP table cel numeric data : Needs [cell width,] data
//
static const char * pcWebPageCellDataNumber[] =
{
   "<td>%d</td>" NEWLINE,
   "<td width=\"%d%%\">%d</td>" NEWLINE
};

/*------  Local functions separator ------------------------------------
__GLOBAL_FUNCTIONS_________(){};
----------------------------------------------------------------------------*/

// 
// Function:   HTTP_DynamicPageHandler
// Purpose:    Execute the dynamic webpage callback
// 
// Parameters: Socket description, DynPage index
// Returns:    TRUE if there is data to be send back
// Note:       
//
bool HTTP_DynamicPageHandler(NETCL *pstCl, int iIdx)
{
   bool     fCc=FALSE;
   PFVOIDPI pfCb;

   PRINTF1("HTTP-DynamicPageHandler():Idx=%d" CRLF, iIdx);
   //
   pfCb = GEN_GetDynamicPageCallback(iIdx);
   if(pfCb)
   {
      fCc = pfCb(pstCl, iIdx);
   }
   return(fCc);
}

// 
// Function:   HTTP_ExPageGeneric
// Purpose:    Send out generic data to the http client
// 
// Parameters: Client
// Returns:    
// Note:       
//
bool HTTP_ExPageGeneric(NETCL *pstCl, const char *pcData)
{
   bool fOKee;

   fOKee = HTTP_BuildGeneric(pstCl, pcData);

   return(fOKee);
}

// 
// Function:   HTTP_ExPageStart
// Purpose:    Starts a web page
// 
// Parameters: Client
// Returns:    
// Note:       
//
bool HTTP_ExPageStart(NETCL *pstCl, const char *pcTitle)
{
   bool fOKee;

   fOKee = HTTP_BuildGeneric(pstCl, pcWebPageStart, pcTitle);

   return(fOKee);
}

//
// Function:   HTTP_PageIsDynamic
// Purpose:    Check if the requested page is dynamic or (file)static
// 
// Parameters: Net client struct
// Returns:    >=0 if requested page is dynamic (and needs construction)
// Note:       
//
int HTTP_PageIsDynamic(NETCL *pstCl)
{
   int   iIdx=0, iLen, iPort;
   char *pcDynName;
   char *pcPagename;

   pcPagename = http_ExtractPageName(pstCl);

   if ( pcPagename == NULL)   return(-1);
   if (*pcPagename == '/')    pcPagename++;
   if (*pcPagename == 0)      return(0);

   do
   {
      if( (pcDynName = GEN_GetDynamicPageName(iIdx)) )
      {
         iLen = GEN_STRLEN(pcPagename);
         PRINTF3("HTTP-PageIsDynamic(): URL=%s:%04d, DynPage=%s" CRLF, pcPagename, pstCl->iPort, pcDynName);
         if(GEN_STRNCMP(pcDynName, pcPagename, iLen) == 0)
         {
            //
            // This request is a dynamic HTTP page: check if port matches !
            //
            iPort = GEN_GetDynamicPagePort(iIdx);
            if( (iPort == 0) || (iPort == pstCl->iPort) ) return(iIdx);
         }
         //
         // No match: keep looking
         //
         iIdx++;
      }
   }
   while(pcDynName);
   return(-1);
}

// 
// Function:   HTTP_HtmlLinkSnapshot
// Purpose:    Build table with link to current snapshot 
// 
// Parameters: Client, header text, snapshot path
// Returns:    TRUE if OKee
// Note:       
//
bool HTTP_HtmlLinkSnapshot(NETCL *pstCl, const char *pcHeader)
{
   HTTP_BuildRespondHeader(pstCl);
   HTTP_BuildStart(pstCl);
   //
   // Build table
   //
   HTTP_BuildTableStart(pstCl, pcHeader, 1, 70, 3);
   //
   HTTP_BuildTableRowStart(pstCl, 6);
   HTTP_BuildTableColumnLink(pstCl, pstMap->G_pcLastFile, 25, pstMap->G_pcLastFile);
   HTTP_BuildTableColumnText(pstCl, "", 15);
   HTTP_BuildTableColumnText(pstCl, "", 30);
   HTTP_BuildTableRowEnd(pstCl);
   HTTP_BuildTableEnd(pstCl);
   HTTP_BuildLineBreaks(pstCl, 1);
   HTTP_BuildEnd(pstCl);
   return(TRUE);
}

// 
// Function:   HTTP_HtmlShowSnapshot
// Purpose:    Build table with current snapshot 
// 
// Parameters: Client, header text
// Returns:    TRUE if OKee
// Note:       
//
bool HTTP_HtmlShowSnapshot(NETCL *pstCl, const char *pcHeader)
{
   //
   // Reply HTTP response header OK
   // Reply msg body : 
   //
   HTTP_BuildRespondHeader(pstCl);        //"HTTP/1.0 200 OK" NEWLINE "Content-Type=text/html" NEWLINE NEWLINE;
   HTTP_BuildStart(pstCl);                // Title - eyecandy
   HTTP_BuildDivStart(pstCl);
   //
   // Send the shell results to the client:
   //    - Command executed
   //    - <img src="__filename__" width="xxx" height="xxx"></img>
   //
   HTTP_BuildGeneric(pstCl, pcWebPageShell, pstMap->G_pcLastFile, pstMap->G_pcStatus);
   HTTP_BuildLineBreaks(pstCl, 2);
   HTTP_BuildGeneric(pstCl, pcWebRequestPicture, pstMap->G_pcLastFile, pstMap->G_pcWidth, pstMap->G_pcHeight);
   HTTP_BuildDivEnd(pstCl);
   HTTP_BuildEnd(pstCl);
   return(TRUE);
}

// 
// Function:   HTTP_SendTextFile
// Purpose:    Send a textfile from fs to socket
// 
// Parameters: Client, filename
// Returns:    TRUE if OKee
// Note:       
//
bool HTTP_SendTextFile(NETCL *pstCl, char *pcFile)
{
   bool     fCc=FALSE;
   int      iRead, iTotal=0;
   FILE    *ptFile;
   char    *pcBuffer;
   char    *pcRead;

   ptFile = fopen(pcFile, "r");

   if(ptFile)
   {
      pcBuffer = safemalloc(RCV_BUFFER_SIZE);
      //
      PRINTF1("HTTP-SendTextFile(): File=<%s>"CRLF, pcFile);
      do
      {
         pcRead = fgets(pcBuffer, RCV_BUFFER_SIZE, ptFile);
         if(pcRead)
         {
            iRead = GEN_STRLEN(pcBuffer);
            PRINTF2("HTTP-SendTextFile(): %s(%d)"CRLF, pcBuffer, iRead);
            HTTP_SendData(pstCl, pcBuffer, iRead);
            iTotal += iRead;
         }
         else
         {
            PRINTF("HTTP-SendTextFile(): EOF" CRLF);
         }
      }
      while(pcRead);
      PRINTF1("HTTP-SendTextFile(): %d bytes written."CRLF, iTotal);
      fclose(ptFile);
      free(pcBuffer);
      fCc = TRUE;
   }
   else
   {
      PRINTF1("HTTP-SendTextFile(): Error open file <%s>"CRLF, pcFile);
   }
   return(fCc);
}

/*------  Local functions separator ------------------------------------
__BUILD_FUNCTIONS_________(){};
----------------------------------------------------------------------------*/

//
// Function:   HTTP_BuildStart
// Purpose:    Put out HTTP page start 
// 
// Parameters: Client
// Returns:    True if okee
// Note:       
//
bool HTTP_BuildStart(NETCL *pstCl)
{
   bool fOKee=TRUE;

   //
   // Put out the HTML header, start tag, title etc
   //
   fOKee &= HTTP_ExPageStart(pstCl,   pcWebPageTitle);
   fOKee &= HTTP_ExPageGeneric(pstCl, pcWebPageDefault);
   fOKee &= HTTP_ExPageGeneric(pstCl, pcWebPageLineBreak);
   return(fOKee);
}

//
// Function:   HTTP_BuildRespondHeader
// Purpose:    Put out HTTP response header
// 
// Parameters: Client
// Returns:    True if okee
// Note:       
//
bool HTTP_BuildRespondHeader(NETCL *pstCl)
{
   return( HTTP_ExPageGeneric(pstCl, pcHttpResponseHeader) );
}

//
// Function:   HTTP_BuildRespondHeaderBad
// Purpose:    Put out HTTP response header Bad Request
// 
// Parameters: Client
// Returns:    True if okee
// Note:       
//
bool HTTP_BuildRespondHeaderBad(NETCL *pstCl)
{
   return( HTTP_ExPageGeneric(pstCl, pcHttpResponseHeaderBad) );
}

//
// Function:   HTTP_BuildRespondHeaderJson
// Purpose:    Put out HTTP page start Json
// 
// Parameters: Client
// Returns:    True if okee
// Note:       
//
bool HTTP_BuildRespondHeaderJson(NETCL *pstCl)
{
   return( HTTP_ExPageGeneric(pstCl, pcHttpResponseHeaderJson) );
}

//
// Function:   HTTP_BuildLineBreaks
// Purpose:    Put out HTTP linebreaks
// 
// Parameters: Client, number of newlines
// Returns:    True if okee
// Note:       
//
bool HTTP_BuildLineBreaks(NETCL *pstCl, int iNr)
{
   bool fOKee=TRUE;

   //
   // Put out the HTML header, start tag, title etc
   //
   while(iNr--)   fOKee &= HTTP_ExPageGeneric(pstCl, pcWebPageLineBreak);
   return(fOKee);
}

//
// Function:   HTTP_BuildDivStart
// Purpose:    Put out HTTP page div/section start 
// 
// Parameters: Client
// Returns:    True if okee
// Note:       
//
bool HTTP_BuildDivStart(NETCL *pstCl)
{
   bool fOKee=TRUE;

   //
   // Put out the HTML div start tag
   //
   fOKee &= HTTP_ExPageGeneric(pstCl, pcWebPagePreStart);
   fOKee &= HTTP_ExPageGeneric(pstCl, pcWebPageLineBreak);
   return(fOKee);
}

//
// Function:   HTTP_BuildDivEnd
// Purpose:    Put out HTTP page div/section end
// 
// Parameters: Client
// Returns:    True if okee
// Note:       
//
bool HTTP_BuildDivEnd(NETCL *pstCl)
{
   bool fOKee=TRUE;

   //
   // Put out the HTML div end tag
   //
   fOKee &= HTTP_ExPageGeneric(pstCl, pcWebPagePreEnd);
   fOKee &= HTTP_ExPageGeneric(pstCl, pcWebPageLineBreak);
   return(fOKee);
}

//
// Function:   HTTP_BuildEnd
// Purpose:    Put out HTTP page end
// 
// Parameters: Client
// Returns:    True if okee
// Note:       
//
bool HTTP_BuildEnd(NETCL *pstCl)
{
   bool fOKee=TRUE;

   //
   // Put out the HTML end
   //
   fOKee &= HTTP_ExPageGeneric(pstCl, pcWebPageLineBreak);
   fOKee &= HTTP_ExPageGeneric(pstCl, pcWebPageEnd);
   return(fOKee);
}

/*------  Local functions separator ------------------------------------
__TABLE_FUNCTIONS_________(){};
----------------------------------------------------------------------------*/

//
// Function:   HTTP_BuildTableStart
// Purpose:    Start a http web page table
// 
// Parameters: Client, page header, border with, table width, number of columns
// Returns:    
// Note:       
//
bool  HTTP_BuildTableStart(NETCL *pstCl, const char *pcPage, u_int16 usBorder, u_int16 usWidth, u_int16 usCols)
{
   bool  fOKee;
   
   fOKee = HTTP_BuildGeneric(pstCl, pcWebPageTableStart, usBorder, usWidth, usCols, pcPage);

   return(fOKee);
}

//
// Function:   HTTP_BuildTableRowStart
// Purpose:    Starts a row in a table
// 
// Parameters: Client, row height
// Returns:    
// Note:       
//
bool  HTTP_BuildTableRowStart(NETCL *pstCl,  u_int16 usHeight)
{
   bool  fOKee;

   fOKee = HTTP_BuildGeneric(pstCl, pcWebPageTableRowStart, usHeight);
   
   return(fOKee);
}

//
// Function:   HTTP_BuildTableRowEnd
// Purpose:    Ends a row
// 
// Parameters: Client
// Returns:    
// Note:       
//
bool  HTTP_BuildTableRowEnd(NETCL *pstCl)
{
   bool fOKee;

   fOKee = HTTP_BuildGeneric(pstCl, pcWebPageTableRowEnd);
   
   return(fOKee);
}

//
// Function:   HTTP_BuildTableColumnText
// Purpose:    Start a generic column
// 
// Parameters: Client, page struct, column text, column width
// Returns:    
// Note:       
//
bool  HTTP_BuildTableColumnText(NETCL *pstCl, char *pcText, u_int16 usWidth)
{
   bool     fOKee;

   if(usWidth) fOKee = HTTP_BuildGeneric(pstCl, pcWebPageCellData[1], usWidth, pcText);
   else        fOKee = HTTP_BuildGeneric(pstCl, pcWebPageCellData[0], pcText);

   return(fOKee);
}

//
// Function:   HTTP_BuildTableColumnLink
// Purpose:    Start a link with a hyperlink
// 
// Parameters: Client, page struct, column text, width, link
// Returns:    
// Note:       
//
bool  HTTP_BuildTableColumnLink(NETCL *pstCl, char *pcText, u_int16 usWidth, char *pcLink)
{
   bool  fOKee;

   if(usWidth) fOKee = HTTP_BuildGeneric(pstCl, pcWebPageCellDataLink[1], usWidth, pcLink, pcText);
   else        fOKee = HTTP_BuildGeneric(pstCl, pcWebPageCellDataLink[0], pcLink, pcText);

   return(fOKee);
}

//
// Function:   HTTP_BuildTableColumnNumber
// Purpose:    Start a column with a number
// 
// Parameters: Client, page struct, number, width
// Returns:    
// Note:       
//
bool  HTTP_BuildTableColumnNumber(NETCL *pstCl, u_int16 usNr, u_int16 usWidth)
{
   bool     fOKee;
   
   if(usWidth) fOKee = HTTP_BuildGeneric(pstCl, pcWebPageCellDataNumber[1], usWidth, usNr);
   else        fOKee = HTTP_BuildGeneric(pstCl, pcWebPageCellDataNumber[0], usNr);

   return(fOKee);
}

//
// Function:   HTTP_BuildTableEnd
// Purpose:    End a http web page table
// 
// Parameters: Client
// Returns:    
// Note:       
//
bool  HTTP_BuildTableEnd(NETCL *pstCl)
{
   bool  fOKee;
   
   fOKee = HTTP_BuildGeneric(pstCl, pcWebPageTableEnd);

   return(fOKee);
}

/*------  Local functions separator ------------------------------------
__PAGE_HANDLERS_________(){};
----------------------------------------------------------------------------*/

// 
// Function:   http_DynPageAlarm
// Purpose:    Dynamically created default page
// 
// Parameters: Client, DynPage index
// Returns:    1 if OKee
// Note:       
//
static bool http_DynPageAlarm(NETCL *pstCl, int iIdx)
{
   bool     fCc=FALSE;

   switch(GEN_GetDynamicPageType(iIdx))
   {
      default:
         break;

      case HTTP_HTML:
         break;

      case HTTP_JSON:
         GEN_STRCPY(pstMap->G_pcCommand, "alarm");
         fCc = RPI_BuildJsonMessageBodyAlarm(pstCl, CAM_ALM);
         break;
   }
   return(fCc);
}

//
// Function:   http_DynPageDefault
// Purpose:    Dynamically created default page
// 
// Parameters: Client, DynPage index
// Returns:    1 if OKee
// Note:       
//
static bool http_DynPageDefault(NETCL *pstCl, int iIdx)
{
   bool     fCc=FALSE;

   switch(GEN_GetDynamicPageType(iIdx))
   {
      default:
         break;

      case HTTP_HTML:
         //
         // Put out the HTML header, start tag and the rest
         //         
         HTTP_ExPageGeneric(pstCl, pcHttpResponseHeader);
         //
         HTTP_ExPageStart(pstCl,   pcWebPageTitle);
         HTTP_ExPageGeneric(pstCl, pcWebPageLineBreak);
         HTTP_ExPageGeneric(pstCl, pcWebPageDefault);
         HTTP_ExPageGeneric(pstCl, pcWebPageEmpty);
         HTTP_ExPageGeneric(pstCl, pcWebPageLineBreak);
         HTTP_ExPageGeneric(pstCl, pcWebPageEnd);
         fCc = TRUE;
         break;

      case HTTP_JSON:
         GEN_STRCPY(pstMap->G_pcCommand, "info");
         fCc = TRUE;
         break;
   }
   return(fCc);
}

//
// Function:   http_DynPageEmpty
// Purpose:    Dynamically created empty page
// 
// Parameters: Client, DynPage index
// Returns:    1 if OKee
// Note:       
//
static bool http_DynPageEmpty(NETCL *pstCl, int iIdx)
{
   int      iSize;
   char    *pcFile;

   //
   // Build default file pathname
   //
   iSize  = (2 * MAX_PATH_LEN) + 1;
   pcFile = safemalloc(iSize);
   //
   GEN_SPRINTF(pcFile, "%s%s", pstMap->G_pcWwwDir, pstMap->G_pcDefault);
   //
   HTTP_ExPageGeneric(pstCl, pcHttpResponseHeader);
   HTTP_ExPageStart(pstCl, pcWebPageTitle);
   //
   // Try to send out the default HTML file. If none exists, revert to default lines
   //
   if(!HTTP_SendTextFile(pstCl, pcFile) )
   {
      //
      // Put out a default reply: 
      //    
      HTTP_ExPageGeneric(pstCl, pcWebPageLineBreak);
      HTTP_ExPageGeneric(pstCl, pcWebPageDefault);
      HTTP_ExPageGeneric(pstCl, pcWebPageEmpty);
      HTTP_ExPageGeneric(pstCl, pcWebPageLineBreak);
   }
   HTTP_ExPageGeneric(pstCl, pcWebPageEnd);
   safefree(pcFile);
   return(TRUE);
}

//
// Function:   http_DynPageExit
// Purpose:    Dynamically created system page to exit the server
// 
// Parameters: Client, DynPage index
// Returns:    TRUE if OKee
// Note:       This call acts as a gracefull exit from the server to the Linux command shell
//
static bool http_DynPageExit(NETCL *pstCl, int iIdx)
{
   bool     fCc=FALSE;

   switch(GEN_GetDynamicPageType(iIdx))
   {
      default:
         break;

      case HTTP_HTML:
         //
         // Put out the HTML header, start tag and the rest
         //         
         HTTP_ExPageGeneric(pstCl, pcHttpResponseHeader);
         //
         HTTP_ExPageStart(pstCl,   pcWebPageTitle);
         HTTP_ExPageGeneric(pstCl, pcWebPageDefault);
         HTTP_ExPageGeneric(pstCl, pcWebExitPage);
         HTTP_ExPageGeneric(pstCl, pcWebPageEnd);
         //
         //PRINTF("http-DynPageExit():OKee" CRLF);
         LOG_Report(0, "DYN", "Exit request from browser");
         //
         // Request our own exit
         //
         GLOBAL_HostNotification(GLOBAL_RPI_END);
         fCc = TRUE;
         break;

      case HTTP_JSON:
         GEN_STRCPY(pstMap->G_pcCommand, "exit");
         break;
   }
   return(fCc);
}

// 
// Function:   http_DynPageFotos
// Purpose:    Dynamically created system page to send back all fotos
// 
// Parameters: Client, DynPage index
// Returns:    TRUE:    pstCl is setup to handle reply correctly
//             FALSE:   Perform a generic error msg to client (HTML or JSON)
// Note:       Called:  NET_xxxx Session callback function:
//                         rpi_CallbackConnection(pstCl)
//                         ....
//                         HTTP_DynamicPageHandler(pstCl)
//
static bool http_DynPageFotos(NETCL *pstCl, int iIdx)
{
   bool     fCc=FALSE;
   int      tUrl;

   switch(GEN_GetDynamicPageType(iIdx))
   {
      default:
         PRINTF1("http-DynPageFotos():??? Index=%d" CRLF, iIdx);
         break;

      case HTTP_HTML:
         fCc = http_ShowFiles(pstCl, pstMap->G_pcPixPath, pstMap->G_pcFilter, pcFotoHeader);
         if(fCc) PRINTF("http-DynPageFotos():HTML OKee" CRLF);
         else    PRINTF("http-DynPageFotos():HTML No files found" CRLF);
         //
         // We're correctly setup for proper reply to client
         //
         fCc = TRUE;
         break;

      case HTTP_JSON:
         PRINTF("http-DynPageFotos():JSON" CRLF);
         GEN_STRCPY(pstMap->G_pcCommand, "fotos");
         //
         tUrl = GEN_GetDynamicPageUrl(iIdx);
         fCc  = RPI_BuildJsonMessageBodyArray(pstCl, tUrl, FALSE);
         break;
   }
   return(fCc);
}

//
// Function:   http_DynPageLog
// Purpose:    Dynamically created system page to show the log file
// 
// Parameters: Client, DynPage index
// Returns:    TRUE if OKee
// Note:       
//
static bool http_DynPageLog(NETCL *pstCl, int iIdx)
{
   bool     fCc=FALSE;

   switch(GEN_GetDynamicPageType(iIdx))
   {
      default:
         break;

      case HTTP_HTML:
         // 
         // Put out the HTML header, start tag and the rest
         //         
         HTTP_ExPageGeneric(pstCl, pcHttpResponseHeader);
         HTTP_ExPageStart(pstCl,   pcWebPageTitle);
         HTTP_ExPageGeneric(pstCl, pcWebPageLineBreak);
         HTTP_ExPageGeneric(pstCl, pcWebPageDefault);
         //
         HTTP_ExPageGeneric(pstCl, pcWebPagePreStart);
         HTTP_ExPageGeneric(pstCl, pcWebPageLineBreak);
         HTTP_SendTextFile(pstCl,  RPI_PUBLIC_LOG_PATH);
         HTTP_ExPageGeneric(pstCl, pcWebPagePreEnd);
         HTTP_ExPageGeneric(pstCl, pcWebPageEnd);
         //
         fCc = TRUE;
         //PRINTF("http-DynPageLog():OKee" CRLF);
         break;

      case HTTP_JSON:
         GEN_STRCPY(pstMap->G_pcCommand, "log");
         break;
   }
   return(fCc);
}

// 
// Function:   http_DynPageMotion
// Purpose:    Dynamically created virtual screen with motion data using JSON reply
// 
// Parameters: Client, DynPage index
// Returns:    TRUE if OKee
// Note:       
//
static bool http_DynPageMotion(NETCL *pstCl, int iIdx)
{
   bool        fCc=FALSE;
   int         iRow;
   RPIDATA    *pstCam=NULL;
   char       *pcTemp;
   char       *pcVirtual;
   VLCD       *pstLcd=&pstMap->G_stVirtLcd;

   switch(GEN_GetDynamicPageType(iIdx))
   {
      default:
      case HTTP_HTML:
         break;

      case HTTP_JSON:
         pcTemp = safemalloc(pstLcd->ubVirtCols+1);
         //
         GEN_STRCPY(pstMap->G_pcCommand, "motion");
         //
         // Put out the contents of the virtual screen 
         //
         pstCam = JSON_InsertParameter(pstCam, "Command", "motion", JE_CURLB|JE_TEXT|JE_COMMA|JE_CRLF);
         pstCam = JSON_InsertInteger(pstCam,   "Counter", pstMap->G_stMotionDetect.iCounter, JE_COMMA|JE_CRLF);
         pstCam = JSON_InsertInteger(pstCam,   "Cluster", pstMap->G_stMotionDetect.iCluster, JE_COMMA|JE_CRLF);
         //
         pstCam = JSON_InsertInteger(pstCam,   "VirtualRows", pstLcd->ubVirtRows, JE_COMMA|JE_CRLF);
         pstCam = JSON_InsertInteger(pstCam,   "VirtualCols", pstLcd->ubVirtCols, JE_COMMA|JE_CRLF);
         pstCam = JSON_InsertParameter(pstCam, "VirtualScreen", "", JE_SQRB|JE_CRLF);
         //
         pcVirtual = pstLcd->pcVirtLcd;
         for(iRow=0; iRow<pstLcd->ubVirtRows; iRow++)
         {
            GEN_MEMCPY(pcTemp, pcVirtual, pstLcd->ubVirtCols);
            pcTemp[pstLcd->ubVirtCols] = 0;
            pstCam = JSON_InsertValue(pstCam, pcTemp, JE_TEXT|JE_COMMA|JE_CRLF);
            pcVirtual += pstLcd->ubVirtCols;
         }
         pstCam = JSON_TerminateArray(pstCam);
         pstCam = JSON_TerminateObject(pstCam);
         fCc    = JSON_RespondObject(pstCl, pstCam);
         //
         // Done with this function: exit
         //
         JSON_ReleaseObject(pstCam);
         //
         //PRINTF("http-DynPageMotion():Done" CRLF);
         safefree(pcTemp);
         break;
   }
   return(fCc);
}

//
// Function:   http_DynPageMd
// Purpose:    Dynamically created system page to show the Motion Detection log file
// 
// Parameters: Client, DynPage index
// Returns:    TRUE if OKee
// Note:       
//
static bool http_DynPageMd(NETCL *pstCl, int iIdx)
{
   bool     fCc=FALSE;

   switch(GEN_GetDynamicPageType(iIdx))
   {
      default:
         break;

      case HTTP_HTML:
         // 
         // Put out the HTML header, start tag and the rest
         //         
         HTTP_ExPageGeneric(pstCl, pcHttpResponseHeader);
         HTTP_ExPageStart(pstCl,   pcWebPageTitle);
         HTTP_ExPageGeneric(pstCl, pcWebPageLineBreak);
         HTTP_ExPageGeneric(pstCl, pcWebPageDefault);
         //
         HTTP_ExPageGeneric(pstCl, pcWebPagePreStart);
         HTTP_ExPageGeneric(pstCl, pcWebPageLineBreak);
         HTTP_SendTextFile(pstCl,  RPI_MREP_PATH);
         HTTP_ExPageGeneric(pstCl, pcWebPagePreEnd);
         HTTP_ExPageGeneric(pstCl, pcWebPageEnd);
         //
         fCc = TRUE;
         //PRINTF("http-DynPageMd():OKee" CRLF);
         break;

      case HTTP_JSON:
         GEN_STRCPY(pstMap->G_pcCommand, "md");
         break;
   }
   return(fCc);
}

// 
// Function:   http_DynPageParms
// Purpose:    Dynamically created parameters page
// 
// Parameters: Client, DynPage index
// Returns:    1 if OKee
// Note:       
//
static bool http_DynPageParms(NETCL *pstCl, int iIdx)
{
   bool     fCc=FALSE;
   GLOCMD  *pstCmd;

   if(pstMap->G_iHttpStatus == CAM_STATUS_ERROR)
   {
      //
      // CAM was in error: restore Global to IDLE
      //
      pstCmd = GLOBAL_CommandCopy();
      PRINTF2("http-DynPageParms():Reset error:Status=%d, Error=%d" CRLF, pstCmd->iStatus, pstCmd->iError);
      pstCmd->iError = CAM_STATUS_IDLE;
      GEN_Free(pstCmd);
      //
      GLOBAL_Status(CAM_STATUS_IDLE);
      GLOBAL_CommandDestroy(pstCmd);
   }
   //
   switch(GEN_GetDynamicPageType(iIdx))
   {
      default:
         break;

      case HTTP_HTML:
         //
         // Put out the HTML header, start tag and the rest
         //
         HTTP_ExPageGeneric(pstCl, pcHttpResponseHeader);
         //
         HTTP_ExPageStart(pstCl,   pcWebPageTitle);
         HTTP_ExPageGeneric(pstCl, pcWebPageLineBreak);
         HTTP_ExPageGeneric(pstCl, pcWebPageDefault);
         HTTP_ExPageGeneric(pstCl, pcWebPageLineBreak);
         HTTP_ExPageGeneric(pstCl, pcWebPageEnd);
         fCc = TRUE;
         break;

      case HTTP_JSON:
         GEN_STRCPY(pstMap->G_pcCommand, "parms");
         fCc = RPI_BuildJsonMessageArgs(pstCl, CAM_ALL);
         break;
   }
   return(fCc);
}

// 
// Function:   http_DynPageShell
// Purpose:    Dynamically created virtual shell
// 
// Parameters: Client, DynPage index
// Returns:    TRUE if OKee
// Note:       
//
static bool http_DynPageShell(NETCL *pstCl, int iIdx)
{
   bool  fCc=FALSE;

   switch(GEN_GetDynamicPageType(iIdx))
   {
      default:
         break;

      case HTTP_HTML:
         fCc = http_DynPageShellHtml(pstCl, iIdx);
         break;

      case HTTP_JSON:
         fCc = http_DynPageShellJson(pstCl, iIdx);
         break;
   }
   return(fCc);
}

// 
// Function:   http_DynPageShellHtml
// Purpose:    Dynamically created virtual screen using HTML reply
// 
// Parameters: Client, DynPage index
// Returns:    TRUE if OKee
// Note:       This call acts as a router to Linux command shell
//             GET /cmd.html?ps&-a&-x blahblah
//                  --> system("ps -a -x ><dir>/<file>")
//                      return <file> to caller as text output from the command
// 
static bool http_DynPageShellHtml(NETCL *pstCl, int iIdx)
{
   bool  fCc=TRUE;
   int   iCc;
   char *pcShell;
   char *pcCmd;
   char *pcTemp;

   pcShell = safemalloc(MAX_CMD_LEN);
   pcCmd   = safemalloc(MAX_CMD_LEN);
   //
   // Collect command and parameters, execute command and return its reply 
   //
   pstCl->iNextIdx = pstCl->iPaylIdx;
   pcTemp = HTTP_CollectGetParameter(pstCl);
   //
   if(pcTemp) PRINTF1("http-DynPageShellHtml():P=<%s>" CRLF, pcTemp);
   else       PRINTF("http-DynPageShellHtml():No Parms" CRLF);
   //
   if(pcTemp)
   {
      //
      // We have a Shell command: collect the whole string
      //
      GEN_SPRINTF(pcShell, "%s", pcTemp);
      do
      {
         GEN_SPRINTF(pcCmd, "%s", pcShell);
         pcTemp = HTTP_CollectGetParameter(pstCl);
         if(pcTemp) GEN_SPRINTF(pcShell, "%s %s", pcCmd, pcTemp);
      }
      while(pcTemp);
      //
      GEN_SPRINTF(pcCmd, "%s", pcShell);
      GEN_SPRINTF(pcShell, "%s >%s%s", pcCmd, RPI_WORK_DIR, RPI_SHELL_FILE);
      PRINTF1("http-DynPageShellHtml():%s" CRLF, pcShell);
      //
      // Execute the Shell command
      //
      LOG_Report(0, "DYN", "Run system call [%s]", pcShell);
      iCc = system(pcShell);
      // 
      //
      // Put out the HTML header, start tag and the rest
      //         
      HTTP_ExPageGeneric(pstCl, pcHttpResponseHeader);
      HTTP_ExPageStart(pstCl,   pcWebPageTitle);
      HTTP_ExPageGeneric(pstCl, pcWebPageLineBreak);
      HTTP_ExPageGeneric(pstCl, pcWebPageDefault);
      //
      //
      if(iCc < 0) 
      {
         LOG_Report(errno, "DYN", "ERROR: Running system call [%s]", pcShell);
         PRINTF2("http-DynPageShellHtml(): Error completion %d (errno=%d)" CRLF, iCc, errno);
         HTTP_BuildGeneric(pstCl, pcWebPageShellBad, errno);
         fCc=FALSE;
      }
      HTTP_ExPageGeneric(pstCl, pcWebPagePreStart);
      //
      // Send the shell results to the client pcWebPageShell
      //
      HTTP_BuildGeneric(pstCl, pcWebPageShell, pcShell);
      HTTP_ExPageGeneric(pstCl, pcWebPageLineBreak);
      HTTP_ExPageGeneric(pstCl, pcWebPageLineBreak);
      GEN_SPRINTF(pcCmd, "%s%s", RPI_WORK_DIR, RPI_SHELL_FILE);
      HTTP_SendTextFile(pstCl, pcCmd);
      HTTP_ExPageGeneric(pstCl, pcWebPagePreEnd);
      HTTP_ExPageGeneric(pstCl, pcWebPageEnd);
   }
   else
   {
      HTTP_ExPageGeneric(pstCl, pcHttpResponseHeaderBad);
      HTTP_ExPageStart(pstCl,   pcWebPageTitle);
      HTTP_ExPageGeneric(pstCl, pcWebPageEnd);
      fCc=FALSE;
   }
   //
   safefree(pcShell);
   safefree(pcCmd);
   //PRINTF("http-DynPageShellHtml():Done" CRLF);
   return(fCc);
}

// 
// Function:   http_DynPageShellJson
// Purpose:    Dynamically created system page using JSON reply
// 
// Parameters: Client, DynPage index
// Returns:    TRUE if OKee
// Note:       This call acts as a router to Linux command shell
//             GET /cmd.json?ps&-a&-x blahblah
//                  --> system("ps -a -x ><dir>/<file>")
// 
static bool http_DynPageShellJson(NETCL *pstCl, int iIdx)
{
   bool     fCc=TRUE;
   int      iCc, iRead=0, iLines=0;
   RPIDATA *pstCam=NULL;
   char    *pcShell;
   char    *pcCmd;
   char    *pcTemp;
   FILE    *ptFile;

   pcShell = safemalloc(MAX_CMD_LEN);
   pcCmd   = safemalloc(MAX_CMD_LEN);
   //
   GEN_STRCPY(pstMap->G_pcCommand, "shell");
   //
   // Collect command and parameters, execute command and return its reply 
   //
   pstCl->iNextIdx = pstCl->iPaylIdx;
   pcTemp = HTTP_CollectGetParameter(pstCl);
   //
   if(pcTemp) PRINTF1("http-DynPageShellJson():P=<%s>" CRLF, pcTemp);
   else       PRINTF("http-DynPageShellJson():No Parms" CRLF);
   //
   if(pcTemp)
   {
      //
      // We have a Shell command: collect the whole string
      //
      GEN_SPRINTF(pcShell, "%s", pcTemp);
      do
      {
         GEN_SPRINTF(pcCmd, "%s", pcShell);
         pcTemp = HTTP_CollectGetParameter(pstCl);
         if(pcTemp) GEN_SPRINTF(pcShell, "%s %s", pcCmd, pcTemp);
      }
      while(pcTemp);
      //
      GEN_SPRINTF(pcCmd, "%s", pcShell);
      GEN_SPRINTF(pcShell, "%s >%s%s", pcCmd, RPI_WORK_DIR, RPI_SHELL_FILE);
      PRINTF1("http-DynPageShellJson():%s" CRLF, pcShell);
      //
      // Execute the Shell command
      //
      //LOG_Report(0, "DYN", "Run [%s]", pcShell);
      iCc = system(pcShell);
      //
      if(iCc < 0) 
      {
         LOG_Report(errno, "DYN", "ERROR: Running system call [%s]", pcShell);
         PRINTF2("http-DynPageShellJson(): Error completion %d (errno=%d)" CRLF, iCc, errno);
         fCc=FALSE;
      }
      else
      {
         GEN_SPRINTF(pcCmd, "%s%s", RPI_WORK_DIR, RPI_SHELL_FILE);
         PRINTF1("http-DynPageShellJson(): Split file in JSON elements : %s" CRLF, pcCmd);
         ptFile = fopen(pcCmd, "r");
         //
         pstCam = JSON_InsertParameter(pstCam, "Command", "shell", JE_CURLB|JE_TEXT|JE_COMMA|JE_CRLF);
         pstCam = JSON_InsertParameter(pstCam, "Filename", pcCmd, JE_TEXT|JE_COMMA|JE_CRLF);
         pstCam = JSON_InsertParameter(pstCam, "Output", "", JE_SQRB|JE_CRLF);
         do
         {
            iRead = http_ReadTextFile(ptFile, pcShell, MAX_CMD_LEN-1);
            if(iRead)
            {
               pstCam = JSON_InsertValue(pstCam, pcShell, JE_TEXT|JE_COMMA|JE_CRLF);
               http_CheckVirtualData(pcShell);
               iLines++;
            }
         }
         while(iRead);
         pstCam = JSON_TerminateArray(pstCam);
         GEN_SPRINTF(pcCmd, "%d", iLines);
         pstCam = JSON_InsertParameter(pstCam, "Lines", pcCmd, JE_COMMA|JE_CRLF);
         pstCam = JSON_TerminateObject(pstCam);
         fCc    = JSON_RespondObject(pstCl, pstCam);
         //
         // Done with this function: exit
         //
         JSON_ReleaseObject(pstCam);
         fclose(ptFile);
      }
   }
   else
   {
      fCc=FALSE;
   }
   //
   safefree(pcShell);
   safefree(pcCmd);
   //PRINTF("http-DynPageShellJson():Done" CRLF);
   return(fCc);
}

// 
// Function:   http_DynPageScreen
// Purpose:    Dynamically created system page
// 
// Parameters: Client, DynPage index
// Returns:    TRUE if OKee
// Note:       This call acts as a router to Linux command shell
//                GET /cmd?ps&-a&-x blahblah
//
//                 --> system("ps -a -x ><dir>/<file>")
//                     return <file> to caller as text output from the command
//
static bool http_DynPageScreen(NETCL *pstCl, int iIdx)
{
   bool  fCc=FALSE;

   switch(GEN_GetDynamicPageType(iIdx))
   {
      default:
         break;

      case HTTP_HTML:
         fCc = http_DynPageScreenHtml(pstCl, iIdx);
         break;

      case HTTP_JSON:
         fCc = http_DynPageScreenJson(pstCl, iIdx);
         break;
   }
   return(fCc);
}

// 
// Function:   http_DynPageScreenHtml
// Purpose:    Dynamically created virtual screen using HTML reply
// 
// Parameters: Client, DynPage index
// Returns:    TRUE if OKee
// Note:       
//
static bool http_DynPageScreenHtml(NETCL *pstCl, int iIdx)
{
   int      iRow;
   char    *pcTemp;
   char    *pcVirtual;
   VLCD    *pstLcd=&pstMap->G_stVirtLcd;

   pcTemp = safemalloc(LCD_VIRTUAL_COLS+1);
   //
   // Put out the HTML header, start tag and the rest
   //         
   HTTP_ExPageGeneric(pstCl, pcHttpResponseHeader);
   HTTP_ExPageStart(pstCl,   pcWebPageTitle);
   HTTP_ExPageGeneric(pstCl, pcWebPageLineBreak);
   HTTP_ExPageGeneric(pstCl, pcWebPageDefault);
   //
   HTTP_BuildGeneric(pstCl,  pcWebPageFontStart, "Courier New");
   HTTP_ExPageGeneric(pstCl, pcWebPagePreStart);
   //
   pcVirtual = pstLcd->pcVirtLcd;
   for(iRow=0; iRow<LCD_VIRTUAL_ROWS; iRow++)
   {
      GEN_MEMCPY(pcTemp, pcVirtual, LCD_VIRTUAL_COLS);
      pcTemp[LCD_VIRTUAL_COLS] = 0;
      HTTP_BuildGeneric(pstCl, pcWebPageText, pcTemp);
      pcVirtual += LCD_VIRTUAL_COLS;
   }
   HTTP_ExPageGeneric(pstCl, pcWebPagePreEnd);
   HTTP_ExPageGeneric(pstCl, pcWebPageFontEnd);
   HTTP_ExPageGeneric(pstCl, pcWebPageEnd);
   //
   safefree(pcTemp);
   //PRINTF("http-DynPageScreenHtml():Done" CRLF);
   return(TRUE);
}

// 
// Function:   http_DynPageScreenJson
// Purpose:    Dynamically created virtual screen using JSON reply
// 
// Parameters: Client, DynPage index
// Returns:    TRUE if OKee
// Note:       
//
static bool http_DynPageScreenJson(NETCL *pstCl, int iIdx)
{
   bool        fCc;
   int         iRow;
   RPIDATA    *pstCam=NULL;
   char       *pcTemp;
   char       *pcVirtual;
   VLCD       *pstLcd=&pstMap->G_stVirtLcd;

   pcTemp = safemalloc(LCD_VIRTUAL_COLS+1);
   //
   GEN_STRCPY(pstMap->G_pcCommand, "screen");
   //
   // Put out the contents of the virtual screen 
   //
   pstCam = JSON_InsertParameter(pstCam, "Command", "screen", JE_CURLB|JE_TEXT|JE_COMMA|JE_CRLF);
   pstCam = JSON_InsertInteger(pstCam, "VirtualRows", LCD_VIRTUAL_ROWS, JE_COMMA|JE_CRLF);
   pstCam = JSON_InsertInteger(pstCam, "VirtualCols", LCD_VIRTUAL_COLS, JE_COMMA|JE_CRLF);
   pstCam = JSON_InsertParameter(pstCam, "Output", "", JE_SQRB|JE_CRLF);
   //
   pcVirtual = pstLcd->pcVirtLcd;
   for(iRow=0; iRow<LCD_VIRTUAL_ROWS; iRow++)
   {
      GEN_MEMCPY(pcTemp, pcVirtual, LCD_VIRTUAL_COLS);
      pcTemp[LCD_VIRTUAL_COLS] = 0;
      pstCam = JSON_InsertValue(pstCam, pcTemp, JE_TEXT|JE_COMMA|JE_CRLF);
      pcVirtual += LCD_VIRTUAL_COLS;
   }
   pstCam = JSON_TerminateArray(pstCam);
   pstCam = JSON_TerminateObject(pstCam);
   fCc    = JSON_RespondObject(pstCl, pstCam);
   //
   // Done with this function: exit
   //
   JSON_ReleaseObject(pstCam);
   //
   //PRINTF("http-DynPageScreenJson():Done" CRLF);
   safefree(pcTemp);
   return(fCc);
}

// 
// Function:   http_DynPageVideos
// Purpose:    Dynamically created system page to send back all videos
// 
// Parameters: Client, DynPage index
// Returns:    TRUE if OKee
// Note:       
//
static bool http_DynPageVideos(NETCL *pstCl, int iIdx)
{
   bool     fCc=FALSE;
   int      tUrl;

   PRINTF("http-DynPageVideos():" CRLF);
   //
   switch(GEN_GetDynamicPageType(iIdx))
   {
      default:
         break;

      case HTTP_HTML:
         fCc = http_ShowFiles(pstCl, pstMap->G_pcVideoPath, pstMap->G_pcFilter, pcVideoHeader);
         break;

      case HTTP_JSON:
         GEN_STRCPY(pstMap->G_pcCommand, "videos");
         //
         tUrl = GEN_GetDynamicPageUrl(iIdx);
         fCc  = RPI_BuildJsonMessageBodyArray(pstCl, tUrl, FALSE);
         break;
   }
   return(fCc);
}

/*------  Local functions separator ------------------------------------
__HTTP_FUNCTIONS_________(){};
----------------------------------------------------------------------------*/

// 
// Function:   http_CheckVirtualData
// Purpose:    Check if this data contains info to put out to the virtual display
// 
// Parameters: Text
// Returns:    
// Note:       
//
static void http_CheckVirtualData(char *pcData)
{
   const RPINFO  *pstInfo=stVirtualInfo;
   int            iOffset, iLength;
   u_int8         ubCol=0;
   char          *pcResult;
   char          *pcTitle;

   PRINTF1("http-CheckVirtualData(): New virtual data <%s>" CRLF, pcData);
   while(pstInfo->pcTrigger)
   {
      PRINTF1("http-CheckVirtualData():Check <%s>" CRLF, pstInfo->pcTrigger);
      if( (pcResult = GEN_STRSTR(pcData, pstInfo->pcTrigger)) != NULL )
      {
         ubCol   = pstInfo->ubCol;
         pcTitle = pstInfo->pcTitle;
         PRINTF3("http-CheckVirtualData():Found: (%2d,%2d) <%s>" CRLF, pstInfo->ubRow, ubCol, pcResult);
         if(pcTitle)
         {
            PRINTF1("http-CheckVirtualData():Title <%s>" CRLF, pcTitle);
            //
            // Put title first
            //
            LCD_TEXT(pstInfo->ubRow, ubCol, pcTitle);
            ubCol += (u_int8)GEN_STRLEN(pcTitle);
         }
         iOffset = pstInfo->iOffset;
         iLength = pstInfo->iLength;
         if( iLength < GEN_STRLEN(&pcResult[iOffset]) )
         {
            //
            // Result string needs to be truncated !
            //
            PRINTF3("http-CheckVirtualData():Truncate L=%d, O=%d <%s>" CRLF, iLength, iOffset, pcResult);
            pcResult[iOffset+iLength] = 0;
         }
         PRINTF3("http-CheckVirtualData():(%2d,%2d) <%s>" CRLF, pstInfo->ubRow, ubCol, &pcResult[iOffset]);
         LCD_TEXT(pstInfo->ubRow, ubCol, &pcResult[iOffset]);
      }
      pstInfo++;
   }
}

// 
// Function:   http_ExtractPageName
// Purpose:    Remove HTTP specific chars 
// 
// Parameters: Net client struct
// Returns:    Filename ptr
// Note:       
//
static char *http_ExtractPageName(NETCL *pstCl)
{
   char *pcPageName = NULL;

   //
   //   "GET /index.html HTTP/1.1"
   //   "Host: 10.0.0.231
   //   "User-Agent: Mozilla/5.0 (Windows; U; WinGecko/20100722 Firefox/3.6.8 (.NET CLR 3.5.30729)
   //   "Accept: image/png,image/*;q=0.8,*/*;q=0.5
   //   "
   //   "Accept-Encoding: gzip,deflate
   //   "Accept-Charset: ISO-8859-1,utf-8;q=0.7,*;q=0.7
   //   "Keep-Alive: 115
   //   "Connection: keep-alive
   //   CR,CR,LF,CR,CR,LF
   //
   //    pcUrl
   //       iFileIdx : index to filename
   //       iExtnIdx : index to extension
   //
   PRINTF2("http-ExtractPageName(%d)=%s" CRLF, pstCl->iFileIdx, pstCl->pcUrl);
   if(pstCl->iFileIdx != -1)
   {
      pcPageName = &pstCl->pcUrl[pstCl->iFileIdx];
      PRINTF2("http-ExtractPageName(%d)=%s" CRLF, pstCl->iFileIdx, pcPageName);
   }
   return(pcPageName);
}

// 
// Function:   http_ReadTextFile
// Purpose:    Read a textfile from fs
// 
// Parameters: Buffer, length
// Returns:    Number read, 0 for EOF
// Note:       
//
static int http_ReadTextFile(FILE *ptFile, char *pcBuffer, int iLength)
{
   int      iRem, iRead=0;
   char    *pcRead;

   if(ptFile)
   {
      pcRead = fgets(pcBuffer, iLength, ptFile);
      if(pcRead)
      {
         iRem  = GEN_RemoveChar(pcRead, 0x0a);
         iRem += GEN_RemoveChar(pcRead, 0x0d);
         iRead = GEN_STRLEN(pcRead);
         //PRINTF3("http-ReadTextFile():(%2d-%2d) %s" CRLF, iRead, iRem, pcRead);
      }
      else
      {
         PRINTF("http-ReadTextFile(): EOF" CRLF);
      }
   }
   else
   {
      PRINTF("http-ReadTextFile(): Error: No open file" CRLF);
   }
   return(iRead);
}

// 
// Function:   http_ShowFiles
// Purpose:    Show files in directory from disk
// 
// Parameters: Client, www dir name, filter, header
// Returns:    TRUE if OKee
// Note:       
//
static bool http_ShowFiles(NETCL *pstCl, char *pcWww, char *pcFilter, const char *pcHeader)
{
   bool     fCc=FALSE;
   int      x, iNum=0;
   char     cForD;
   char    *pcFileName;
   char    *pcFullPath;
   char    *pcFilePath;
   char    *pcTemp;
   void    *pvData;

   pcFileName = (char *) safemalloc(MAX_PATH_LEN);
   pcFilePath = (char *) safemalloc(MAX_PATH_LEN);
   pcFullPath = (char *) safemalloc(MAX_PATH_LEN);
   pcTemp     = (char *) safemalloc(MAX_PATH_LEN);
   //
   GEN_SPRINTF(pcFullPath, "%s%s", pstMap->G_pcWwwDir, pcWww);
   PRINTF2("http-ShowFiles(): Filter[%s]-Path=[%s]" CRLF, pcFilter, pcFullPath);
   //
   pvData = FINFO_GetDirEntries(pcFullPath, pcFilter, &iNum);
   if(iNum > 0)
   {
      HTTP_BuildRespondHeader(pstCl);
      HTTP_BuildStart(pstCl);
      // Build table
      HTTP_BuildTableStart(pstCl, pcHeader, 1, 70, 3);
      //
      for (x=0; x<iNum; x++)
      {
         FINFO_GetFilename(pvData, x, pcFileName, MAX_PATH_LEN);
         GEN_SPRINTF(pcFilePath, "%s/%s", pcFullPath, pcFileName);
         FINFO_GetFileInfo(pcFilePath, FI_FILEDIR, &cForD, 1);
         PRINTF3("http-ShowFiles(): Filter(%s)-File(%c):[%s]" CRLF, pcFilter, cForD, pcFileName);
         if( cForD == 'F')
         {
            GEN_SPRINTF(pcTemp, "%s/%s", pcWww, pcFileName);
            HTTP_BuildTableRowStart(pstCl, 6);
            HTTP_BuildTableColumnLink(pstCl, pcFileName, 25, pcTemp);
            FINFO_GetFileInfo(pcFilePath, FI_SIZE, pcTemp, MAX_PATH_LEN);
            HTTP_BuildTableColumnText(pstCl, pcTemp, 15);
            FINFO_GetFileInfo(pcFilePath, FI_MDATE, pcTemp, MAX_PATH_LEN);
            HTTP_BuildTableColumnText(pstCl, pcTemp, 30);
            HTTP_BuildTableRowEnd(pstCl);
         }
      }
      FINFO_ReleaseDirEntries(pvData, iNum);
      HTTP_BuildTableEnd(pstCl);
      HTTP_BuildLineBreaks(pstCl, 1);
      HTTP_BuildEnd(pstCl);
      fCc = TRUE;
   }
   else
   {
      PRINTF("http-ShowFiles(): No file/dir entries" CRLF);
      HTTP_BuildRespondHeader(pstCl);
      HTTP_BuildStart(pstCl);
      HTTP_BuildGeneric(pstCl, pcWebPageContentBad);
      HTTP_BuildLineBreaks(pstCl, 1);
      HTTP_BuildEnd(pstCl);
   }
   safefree(pcFileName);
   safefree(pcFilePath);
   safefree(pcFullPath);
   safefree(pcTemp);
   return(fCc);
}

