/*  (c) Copyright:  2017  Patrn.nl, Confidential Data
**
**  $Workfile:          gen_usb.h
**  $Revision:   
**  $Modtime:    
**
**  Purpose:            gen_usb.c header file
**
 *  Compiler/Assembler: Raspbian Linux
 *  Ext Packages:
**
 *  Author:             Peter Hillen
 *  Date Created:       5 Oct 2017 (from rpiusb.h)
**
 *  Revisions:
 *    $Log:   $
 *
 *
**/

#ifndef _GEN_USB_H_
#define _GEN_USB_H_

#ifdef FEATURE_USB
#include <hidapi/hidapi.h>
#endif   //FEATURE_USB

#ifdef  DEBUG_ASSERT                   // defined in makefile
//      USB COMMS Log
#define LOGUSB1(a)                  LOG_debug(DEBUG_USB,a)
#define LOGUSB2(a,b)                LOG_debug(DEBUG_USB,a,b)
#define LOGUSB3(a,b,c)              LOG_debug(DEBUG_USB,a,b,c)
//      USB DEBUG Log
#define LOGUSD1(a)                  LOG_debug(DEBUG_USD,a)
#define LOGUSD2(a,b)                LOG_debug(DEBUG_USD,a,b)
#define LOGUSD3(a,b,c)              LOG_debug(DEBUG_USD,a,b,c)
//
#else
//
#define LOG_USB1(a)
#define LOG_USB2(a,b)
#define LOG_USB3(a,b,c)
//
#define LOG_USD1(a)
#define LOG_USD2(a,b)
#define LOG_USD3(a,b,c)
//
#endif  //DEBUG_ASSERT
//
#define  MAX_STR  255
#define  MAX_USB  1024
//
typedef struct _rpiusb_
{
   int               iIndex;              // Device Index
   int               iVendorId;           // Vendor ID
   int               iProductId;          // Product ID
   int               iInterfaceNr;        // Device Interface Number
   char             *pcDevicePath;        // Pathname
   char             *pcManufacturer;      // Manuf string
   char             *pcProduct;           // Product string
   char             *pcSerialNr;          // Serial number string
#ifdef FEATURE_USB
   hid_device       *ptHandle;            // Device handle
#else    //FEATURE_USB
   void             *ptHandle;            // Dummy struct
#endif   //FEATURE_USB
   struct _rpiusb_  *pstNext;             // Next device
}  RPIUSB;

//
// Global prototypes
//
int      USB_Daemon           (RPIUSB *);
int      USB_Debug            (RPIUSB *);
int      USB_PrintInfo        (void);
//
RPIUSB  *USB_GetInfo          (int, int);
RPIUSB  *USB_GetDeviceInfo    (RPIUSB *, int, int, int);
bool     USB_Open             (RPIUSB *);
int      USB_Read             (RPIUSB *, char *, int);
int      USB_Write            (RPIUSB *, char *, int);
bool     USB_Close            (RPIUSB *);
void     USB_ReleaseInfo      (RPIUSB *);

#endif  /*_GEN_USB_H_ */

