/*  (c) Copyright:  2017  Patrn, Confidential Data
 *
 *  Workfile:           rpi_proxy.h
 *  Revision:
 *  Modtime:
 *
 *  Purpose:            Headerfile for rpi_proxy.c
 *
 *
 *
 *  Compiler/Assembler: Raspbian Linux GNU gcc
 *  Ext Packages:
 *
 *  Author:             Peter Hillen
 *  Date Created:       31 Okt 2017
 *
 *  Revisions:
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 **/

#ifndef _RPI_PROXY_H_
#define _RPI_PROXY_H_

#define  PRX_TO               10000
#define  PROXY_LENGTH         1000
//
int  PROXY_Init               (void);
bool PROXY_DynPageReplyHtml   (NETCL *);
bool PROXY_DynPageReplyJson   (NETCL *);

#endif /* _RPI_PROXY_H_ */
