/*  (c) Copyright:  2017  Patrn, Confidential Data
 *
 *  Workfile:           cam_json.h
 *  Revision:          
 *  Modtime:           
 *
 *  Purpose:            Headerfile for cam_html.c, the dynamic JSON page handler
 *
 *
 *
 *  Compiler/Assembler: Raspbian Linux GNU gcc
 *  Ext Packages:
 *
 *  Author:             Peter Hillen
 *  Date Created:       17 Mar 2017
 *
 *  Revisions:
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
**/

#ifndef _CAM_JSON_H_
#define _CAM_JSON_H_

//
//
// Global prototypes
//
const CAMPAGE *CAM_JsonGetCommands           (void);
void           CAM_JsonInit                  (void);


#endif /* _CAM_JSON_H_ */
