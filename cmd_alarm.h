/*  (c) Copyright:  2013..2017  Patrn.nl, Confidential Data
 *
 *  Workfile:           cmd_alarm.h
 *  Revision:           
 *  Modtime:            
 *
 *  Purpose:            cmd_alarm.c header file
 * 
 *
 *
 *  Compiler/Assembler: Raspbian Linux GNU gcc
 *  Ext Packages:
 *
 *  Author:             Peter Hillen
 *  Date Created:       17 Sep 2012
 * 
 *  Revisions:
 * 
 *
 * 
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
**/

#ifndef _CMD_ALARM_H_
#define _CMD_ALARM_H_

typedef struct ALUT
{
   const char *pcCommand;
   bool      (*pfFunc)(ALARM *);
}  ALUT;

//
// Global prototypes
//
ALARM      *ALARM_AddAlarm          ();
int         ALARM_CheckAlarms       ();
void        ALARM_DeleteAlarm       (ALARM *);
ALARM      *ALARM_FindDuplicate     (char *, char *);
bool        ALARM_HandleAlarms      ();
const char *ALARM_GetCommand        (int);
int         ALARM_ListAlarms        (char **);

#endif  /*_CMD_ALARM_H_ */

