/*  (c) Copyright:  2013..2018  Patrn.nl, Confidential Data
 *
 *  Workfile:           cmd_button.c
 *  Revision:   
 *  Modtime:    
 *
 *  Purpose:            Interface Buttons functions
 *
 *
 * 
 *  Compiler/Assembler: Raspbian Linux GNU gcc
 *  Ext Packages:
 * 
 *  Author:             Peter Hillen
 *  Date Created:       28 Dec 2013
 * 
 *  Revisions:          wiringPi
 *  Entry Points:       
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
**/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <sys/types.h>

#include <common.h>
#include "config.h"
#include "globals.h"
#include "gen_func.h"
#include "btn_button.h"

//#define USE_PRINTF
#include <printf.h>


//
// Local prototypes/data
//
static void btn_SetupGpio        (void);
static void btn_SetupSwitches    (const BTNDEF *);

/*
 * Function    : BTN_InitButtons
 * Description : Init FP buttons
 *
 * Parameters  : 
 * Returns     : TRUE if OKee
 *
 */
bool BTN_InitButtons(void)
{
   btn_SetupGpio();
   //
   BTN_ShowButtonSetup("BTN");
   return(TRUE);
}

/*
 * Function    : BTN_RegisterButton
 * Description : Register a button or rcu locical key to a particular function
 *
 * Parameters  : Logical Key, Function
 * Returns     : TRUE if OKee
 *
 */
bool BTN_RegisterButton(u_int8 ubKey, PFKEY pFunc)
{
   bool     fCc=FALSE;
   int      iKey;

   if(ubKey < NUM_RCU_KEYS)
   {
      iKey = BTN_GetButtonIndex(ubKey);
      pstMap->G_stButtons[iKey].pFuncButton = pFunc;
      //
      fCc = TRUE;
   }
   return(fCc);
}


//
//  Function:  BTN_LookupKeyAction
//  Purpose:   Lookup the registered button handler
//
//  Parms:     Logical Button key BUTKEYx
//  Returns:   Handled key or RCU_NOKEY
//
u_int8 BTN_LookupKeyAction(u_int8 ubKey)
{
   u_int8   ubRes=RCU_NOKEY;
   int      x;

   PRINTF1("BTN_LookupKeyAction():Key=%d" CRLF, ubKey);
   //
   for(x=0; x<NUM_RCU_KEYS; x++)
   {
      if(pstMap->G_stButtons[x].ubKey == ubKey)
      {
         if(pstMap->G_stButtons[x].pFuncButton)
         {
            ubRes = pstMap->G_stButtons[x].pFuncButton(ubKey);
            break;
         }
      }
   }
   return(ubRes);
}

/*
 * Function    : BTN_ScanButtons
 * Description : Scan the button status
 *
 * Parameters  : 
 * Returns     : Key of the button with a status change BTN_KEYx or RCU_NOKEY
 *
 *                          +---+---+---+...+---+---+---+---+
 * Note        : ulButton = |   |   |   |...|   |   |   |   |
 *                          +---+---+---+...+---+---+---+---+
 *                           msb                         lsb                 
 *
 *               G_stButtons[X].ulImage
 *               G_stButtons[X].ulDebounce 
 *
 */
u_int8 BTN_ScanButtons(void)
{
   //
   // Check if we have a status change on one of the buttons
   //

#ifdef DIO_USE_WIRINGPI
   int      iIdx;
   u_int8   ubButKey=RCU_NOKEY;
   u_int32  ulButtonImage;
   GLOBUT  *pstBut=pstMap->G_stButtons;

   for(iIdx=0; iIdx<MAX_NUM_BUTTONS; iIdx++)
   {
      if(pstBut->ubId != 255)
      {
         ulButtonImage = pstBut->ulImage << 1;
         if(BUTTON(pstBut->ubId) ) 
         {
            ulButtonImage |= 1;
         }
         //
         // Check if image exceeds the debounce threshold
         //
         if(ulButtonImage != pstBut->ulLastImage) 
         {
            PRINTF2("BTN_ScanButtons(): BUT image[%d] changed:0x%08X" CRLF, pstBut->ubKey, (int)ulButtonImage);
            if(ulButtonImage == pstBut->ulDebounce)
            {
               PRINTF3("BTN_ScanButtons():Idx=%d, But[%d]=Pressed, Image=0x%08X" CRLF, iIdx, pstBut->ubKey, (int)ulButtonImage);
               //
               // Image has changed and matched the debounce threshold
               // Set image
               //
               pstBut->iStatus  = CAM_STATUS_KEYPRESS;
               pstBut->fChanged = TRUE;
               ubButKey         = pstBut->ubKey;
               //
               pstBut->ulLastImage = pstBut->ulImage = 0xffffffff;
               break;
            }
            else if((ulButtonImage & 0x3) == 0)
            {
               if(pstBut->iStatus == CAM_STATUS_KEYPRESS)
               {
                  PRINTF3("BTN_ScanButtons():Idx=%d, But[%d]=Released, Image=0x%08X" CRLF, iIdx, pstBut->ubKey, (int)ulButtonImage);
                  //
                  // Key has been pressed: now released
                  // Fixed release time 0x0000000F = b000...1111
                  //
                  pstBut->iStatus  = CAM_STATUS_KEYRELEASE;
                  pstBut->fChanged = TRUE;
                  ubButKey         = pstBut->ubKey;
               }
               // Reset image
               pstBut->ulLastImage = pstBut->ulImage = 0;
               break;
            }
            pstBut->ulLastImage = ulButtonImage;
         }
         pstBut->ulImage = ulButtonImage;
      }
      pstBut++;
   }
   return(ubButKey);
#else
   //
   // IO : No IO used !
   //
   return(RCU_NOKEY);
#endif
}

/*
 * Function    : BTN_GetButtonIndex
 * Description : Get the index to a button or RCU key
 *
 * Parameters  : RCUKEYS
 * Returns     : Index 0..NUM_RCU_KEYS or -1
 *                where -1 means key NOT_FOUND
 *
 */
int BTN_GetButtonIndex(u_int8 ubKey)
{
   int      x, iIdx=-1;
   GLOBUT  *pstBut=pstMap->G_stButtons;

   for(x=0; x<NUM_RCU_KEYS; x++)
   {
      if(pstBut->ubKey == ubKey)
      {
         iIdx = x;
         PRINTF2("BTN_GetButtonIndex(): Button %d = Index %d" CRLF, ubKey, iIdx);
         break;
      }
      pstBut++;
   }
   return(iIdx);
}

/*
 * Function    : BTN_GetButtonKey
 * Description : Get a button logical key
 *
 * Parameters  : button/IRkey index 0..
 * Returns     : BTN_KEYx
 *
 */
u_int8 BTN_GetButtonKey(int iIdx)
{
   u_int8   ubKey=255;

   if(iIdx < NUM_RCU_KEYS)
   {
      ubKey= pstMap->G_stButtons[iIdx].ubKey;
   }
   else
   {
      PRINTF1("BTN_GetButtonKey(%d):Bad Button !" CRLF, iIdx);
      LOG_Report(0, "BTN", "BTN_GetButtonKey(): ERROR: Bad button");
   }
   return(ubKey);
}

/*
 * Function    : BTN_GetButtonStatus
 * Description : Get a button press/depress
 *
 * Parameters  : button/IRkey index 0..
 * Returns     : CAM_STATUS_xxx Status
 *
 */
int BTN_GetButtonStatus(int iIdx)
{
   int      iCc=CAM_STATUS_IDLE;

   if(iIdx < NUM_RCU_KEYS)
   {
      if(pstMap->G_stButtons[iIdx].fChanged)
      {
         iCc = pstMap->G_stButtons[iIdx].iStatus;
      }
   }
   else
   {
      PRINTF1("BTN_GetButtonStatus(%d):Bad Button !" CRLF, iIdx);
      LOG_Report(0, "BTN", "BTN_GetButtonStatus(): ERROR: Bad button");
   }
   return(iCc);
}

// 
//  Function    : BTN_ShowButtonSetup()
//  Description : Show switch configuration
// 
//  Parameters  : ID
//  Returns     : 
// 
void BTN_ShowButtonSetup(const char *pcId)
{
#ifdef   FEATURE_SHOW_BUTTON_SETUP
   int      iIdx;
   GLOBUT  *pstBut=pstMap->G_stButtons;
   
   for(iIdx=0; iIdx<NUM_RCU_KEYS; iIdx++)
   {
      PRINTF4("BTN_ShowButtonSetup(%s): Index[%d] Id=%d, debounce = 0x%x * 100mSecs" CRLF, pcId, iIdx, pstBut->ubId, (int)pstBut->ulDebounce);
      pstBut++;
   }
#endif
}

/*------  Local functions separator ------------------------------------
__LOCAL_FUNCTIONS_(){};
----------------------------------------------------------------------------*/

/*
 * Function    : btn_SetupGpio()
 * Description : Setup GPIO for various IO alternatives
 *
 * Parameters  : 
 * Returns     : 
 *
 */
static void btn_SetupGpio()
{

#ifdef FEATURE_IO_NONE                                      // We have no Digital I/O
   static const BTNDEF stDefaultButtons[NUM_FP_KEYS] = 
   {
      { 0xff,        0xff,       0                      },  // No discrete keys
      { 0xff,        0xff,       0                      },  // No discrete keys
      { 0xff,        0xff,       0                      },  // No discrete keys
      { 0xff,        0xff,       0                      },  // No discrete keys
      { 0xff,        0xff,       0                      },  // No discrete keys
      { 0xff,        0xff,       0                      },  // No discrete keys
      { 0xff,        0xff,       0                      },  // No discrete keys
      { 0xff,        0xff,       0                      }   // No discrete keys
   };
               
   //=============================================================================
   // Use NO I/O board
   //=============================================================================
   PRINTF("btn_SetupGpio(): NO IO-board Setup" CRLF);
   btn_SetupSwitches(stDefaultButtons);
   //
#endif


#ifdef FEATURE_IO_PATRN                                     // Custom digital I/O
   static const BTNDEF stDefaultButtons[NUM_FP_KEYS] = 
   {
      { BTN_1,       BTN_KEY1,   BUTTON_DEBOUNCE_200    },  // Discrete key : Next
      { BTN_2,       BTN_KEY2,   BUTTON_DEBOUNCE_1000   },  // Discrete key : Execute
      { 0xff,        0xff,       0                      },  // No discrete keys
      { 0xff,        0xff,       0                      },  // No discrete keys
      { 0xff,        0xff,       0                      },  // No discrete keys
      { 0xff,        0xff,       0                      },  // No discrete keys
      { 0xff,        0xff,       0                      },  // No discrete keys
      { 0xff,        0xff,       0                      }   // No discrete keys
   };

   //=============================================================================
   // Used Patrn.nl IO board (wiringPi API)
   //=============================================================================
   PRINTF("btn_SetupGpio(): PatrnBoard Setup" CRLF);
   btn_SetupSwitches(stDefaultButtons);
   //
   if( (wiringPiSetup() == -1) )
   {
      LOG_Report(0, "BTN", "btn_SetupGpio(): ERROR: wiringPiSetup");
      LOG_printf("btn_SetupGpio(): wiringPiSetup failed!" CRLF);
   }
   //
   INP_GPIO(LED_R); OUT_GPIO(LED_R); OUT(LED_R, 0);
   INP_GPIO(LED_Y); OUT_GPIO(LED_Y); OUT(LED_Y, 0);
   INP_GPIO(LED_G); OUT_GPIO(LED_G); OUT(LED_G, 0);
   INP_GPIO(LED_W); OUT_GPIO(LED_W); OUT(LED_W, 0);
   INP_GPIO(REL_1); OUT_GPIO(REL_1); OUT(REL_1, 0);
   INP_GPIO(REL_2); OUT_GPIO(REL_2); OUT(REL_2, 0);
   //
   INP_GPIO(BTN_1);
   INP_GPIO(BTN_2);
   //
#endif


#ifdef FEATURE_IO_GERTBOARD                                 // Use GertBoard digital I/O
   static const BTNDEF stDefaultButtons[NUM_FP_KEYS] = 
   {
      { BTN_1,       BTN_KEY1,   BUTTON_DEBOUNCE_200    },  // Discrete key : Next
      { BTN_2,       BTN_KEY2,   BUTTON_DEBOUNCE_1000   },  // Discrete key : Execute
      { BTN_3,       BTN_KEY3,   BUTTON_DEBOUNCE_200    },  // Discrete key : Previous
      { 0xff,        0xff,       0                      },  // No discrete keys
      { 0xff,        0xff,       0                      },  // No discrete keys
      { 0xff,        0xff,       0                      },  // No discrete keys
      { 0xff,        0xff,       0                      },  // No discrete keys
      { 0xff,        0xff,       0                      }   // No discrete keys
   };

   //=============================================================================
   // Use Gert Board
   //=============================================================================
   PRINTF("btn_SetupGpio(): GertBoard Setup" CRLF);
   btn_SetupSwitches(stDefaultButtons);

   // LEDs test GPIO mapping:
   //         Function            Mode
   // GPIO0=  unused
   // GPIO1=  unused
   // GPIO4=  unused
   // GPIO7=  LED_R               Output
   // GPIO8=  LED_Y               Output
   // GPIO9=  LED_G               Output
   // GPIO10=                     Output
   // GPIO11=                     Output
   // GPIO14= unused (preset to be UART)
   // GPIO15= unused (preset to be UART)
   // GPIO17=                     Output
   // GPIO18=                     Output
   // GPIO21=                     Output
   // GPIO22=                     Output
   // GPIO23= BUTTON-3            Input
   // GPIO24= BUTTON-1            Input
   // GPIO25= BUTTON-2            Input
   //
   //
   INP_GPIO(7);   OUT_GPIO(7);
   INP_GPIO(8);   OUT_GPIO(8);
   INP_GPIO(9);   OUT_GPIO(9);
   INP_GPIO(10);  OUT_GPIO(10);
   INP_GPIO(11);  OUT_GPIO(11);
   // 14 and 15 are already set to UART mode
   // by Linux. Best if we don't touch them
   INP_GPIO(17);  OUT_GPIO(17);
   INP_GPIO(18);  OUT_GPIO(18);
   INP_GPIO(21);  OUT_GPIO(21);
   INP_GPIO(22);  OUT_GPIO(22);
   INP_GPIO(23);
   INP_GPIO(24);
   INP_GPIO(25);
   //
   // enable pull-up on GPIO 23,24,25
   // set pull to 2 (code for pull high)
   //
   GPIO_PULL = 2;
   short_wait();
   //
   // setting bits 23, 24 & 25 below means that the GPIO_PULL is applied to
   // GPIO 23, 24, & 25
   //
   GPIO_PULLCLK0 = 0x03800000;
   short_wait();
   GPIO_PULL     = 0;
   GPIO_PULLCLK0 = 0;
#endif

#ifdef FEATURE_IO_PIFACE                                    // Use PiFace Digital board
   static const BTNDEF stDefaultButtons[NUM_FP_KEYS] = 
   {
      { BTN_1,       BTN_KEY1,   BUTTON_DEBOUNCE_200    },  // Discrete key :
      { BTN_2,       BTN_KEY2,   BUTTON_DEBOUNCE_200    },  // Discrete key :
      { BTN_3,       BTN_KEY3,   BUTTON_DEBOUNCE_200    },  // Discrete key :
      { BTN_4,       BTN_KEY4,   BUTTON_DEBOUNCE_200    },  // Discrete key :
      { BTN_5,       BTN_KEY5,   BUTTON_DEBOUNCE_200    },  // Discrete key :
      { BTN_6,       BTN_KEY6,   BUTTON_DEBOUNCE_200    },  // Discrete key :
      { BTN_7,       BTN_KEY7,   BUTTON_DEBOUNCE_200    },  // Discrete key :
      { BTN_8,       BTN_KEY8,   BUTTON_DEBOUNCE_200    }   // Discrete key :
   };

   //=============================================================================
   // Use PiFace Digital board
   //=============================================================================
   btn_SetupSwitches(stDefaultButtons);
   //
   if( (wiringPiSetup() == -1) )
   {
      LOG_Report(0, "BTN", "btn_SetupGpio(): ERROR: wiringPiSetup");
      LOG_printf("btn_SetupGpio(): wiringPiSetup failed!" CRLF);
   }
   else
   {
      //
      // Setup piFace 
      //
      piFaceSetup(PIFACE);
      LOG_printf("btn_SetupGpio(): wiringPiSetup OKee" CRLF);
   }
#endif

#ifdef FEATURE_IO_PIFACECAD                                 // Use PiFaceCad I/O + LCD board
   static const BTNDEF stDefaultButtons[NUM_FP_KEYS] = 
   {
      { BTN_1,       BTN_KEY1,   BUTTON_DEBOUNCE_200    },  // Discrete key : Not used yet
      { BTN_2,       BTN_KEY2,   BUTTON_DEBOUNCE_200    },  // Discrete key : Not used yet
      { BTN_3,       BTN_KEY3,   BUTTON_DEBOUNCE_200    },  // Discrete key : Not used yet
      { BTN_4,       BTN_KEY4,   BUTTON_DEBOUNCE_200    },  // Discrete key : Not used yet
      { BTN_5,       BTN_KEY5,   BUTTON_DEBOUNCE_3200   },  // Discrete key : Poweroff
      { BTN_6,       BTN_KEY6,   BUTTON_DEBOUNCE_1000   },  // Discrete key : Execute
      { BTN_7,       BTN_KEY7,   BUTTON_DEBOUNCE_200    },  // Discrete key : Next
      { BTN_8,       BTN_KEY8,   BUTTON_DEBOUNCE_200    }   // Discrete key : Previous
   };

   //=============================================================================
   // Use PiFaceCAD board (IO and LCD)
   //=============================================================================
   btn_SetupSwitches(stDefaultButtons);
   //
   if( (wiringPiSetup() == -1) )
   {
      LOG_Report(0, "BTN", "btn_SetupGpio(): ERROR: wiringPiSetup");
      LOG_printf("btn_SetupGpio(): wiringPiSetup failed!" CRLF);
   }
   //
   INP_GPIO(BTN_1);
   INP_GPIO(BTN_2);
   INP_GPIO(BTN_3);
   INP_GPIO(BTN_4);
   INP_GPIO(BTN_5);
   INP_GPIO(BTN_6);
   INP_GPIO(BTN_7);
   INP_GPIO(BTN_8);
   //
#endif

#ifdef FEATURE_IO_TFTBUTS                                   // Use TFT screen plus custom digital I/O
   static const BTNDEF stDefaultButtons[NUM_FP_KEYS] = 
   {
      { BTN_1,       BTN_KEY1,   BUTTON_DEBOUNCE_200    },  // Discrete key : short
      { BTN_1,       BTN_KEY3,   BUTTON_DEBOUNCE_2000   },  // Discrete key : short
      { BTN_2,       BTN_KEY2,   BUTTON_DEBOUNCE_200    },  // Discrete key : short
      { BTN_2,       BTN_KEY4,   BUTTON_DEBOUNCE_2000   },  // Discrete key : short
      { 0xff,        0xff,       0                      },  // No discrete keys
      { 0xff,        0xff,       0                      },  // No discrete keys
      { 0xff,        0xff,       0                      },  // No discrete keys
      { 0xff,        0xff,       0                      }   // No discrete keys
   };

   //=============================================================================
   // Used Patrn.nl IO board (wiringPi API)
   //=============================================================================
   PRINTF("btn_SetupGpio(): TFT-Buts Setup" CRLF);
   btn_SetupSwitches(stDefaultButtons);
   //
   if( (wiringPiSetup() == -1) )
   {
      LOG_Report(0, "BTN", "btn_SetupGpio(): ERROR: wiringPiSetup");
      LOG_printf("btn_SetupGpio(): wiringPiSetup failed!" CRLF);
   }
   //
   INP_GPIO(BTN_1);
   INP_GPIO(BTN_2);
   //
#endif

#ifdef FEATURE_IO_LCDADA                                    // Use Adafruit I/O + LCD board
   static const BTNDEF stDefaultButtons[NUM_FP_KEYS] = 
   {
      { BTN_1,       BTN_KEY1,   BUTTON_DEBOUNCE_200    },  // Discrete key : short keypress
      { BTN_2,       BTN_KEY2,   BUTTON_DEBOUNCE_200    },  // Discrete key : short keypress
      { BTN_3,       BTN_KEY3,   BUTTON_DEBOUNCE_200    },  // Discrete key : short keypress
      { BTN_4,       BTN_KEY4,   BUTTON_DEBOUNCE_200    },  // Discrete key : short keypress
      { BTN_1,       BTN_KEY5,   BUTTON_DEBOUNCE_2000   },  // Discrete key : long  keypress
      { BTN_2,       BTN_KEY6,   BUTTON_DEBOUNCE_2000   },  // Discrete key : long  keypress
      { BTN_3,       BTN_KEY7,   BUTTON_DEBOUNCE_2000   },  // Discrete key : long  keypress
      { BTN_4,       BTN_KEY8,   BUTTON_DEBOUNCE_2000   }   // Discrete key : long  keypress
   };

   //=============================================================================
   // Use Adafruit board (4x IO and 2x16 LCD)
   //=============================================================================
   btn_SetupSwitches(stDefaultButtons);
   //
   if( (wiringPiSetup() == -1) )
   {
      LOG_Report(0, "BTN", "btn_SetupGpio(): ERROR: wiringPiSetup");
      LOG_printf("btn_SetupGpio(): wiringPiSetup failed!" CRLF);
   }
   //
   INP_GPIO(BTN_1);
   INP_GPIO(BTN_2);
   INP_GPIO(BTN_3);
   INP_GPIO(BTN_4);
   //
#endif
   PRINTF("btn_SetupGpio(): Ready" CRLF);
}

/*
 * Function    : btn_SetupSwitches()
 * Description : Setup switch configuration
 *
 * Parameters  : Default switch settings
 * Returns     : 
 *
 */
static void btn_SetupSwitches(const BTNDEF *pstDef)
{
   int      iIdx;
   GLOBUT  *pstBut=pstMap->G_stButtons;
   
   for(iIdx=0; iIdx<NUM_FP_KEYS; iIdx++)
   {
      pstBut->ubId         = pstDef->ubId;
      pstBut->ubKey        = pstDef->ubKey;
      pstBut->ulDebounce   = pstDef->ulDebounce;
      pstBut->pFuncButton  = NULL;
      pstBut->iStatus      = CAM_STATUS_KEYRELEASE;
      pstBut->fChanged     = FALSE;
      pstBut->fRepeat      = FALSE;
      pstBut->ulImage      = BTN_DEFAULT_STATUS;
      pstBut->ulLastImage  = BTN_DEFAULT_STATUS;
      //
      PRINTF3("btn_SetupSwitches(): Switch[%d] Id=%d, debounce = 0x%x * 100mSecs" CRLF, iIdx, pstBut->ubId, (int)pstBut->ulDebounce);
      pstBut++;
      pstDef++;
   }
}

