/*  (c) Copyright:  2013..2017  Patrn.nl, Confidential Data
 *
 *  Workfile:           cmd_alarm.c
 *  Revision:          
 *  Modtime:           
 *
 *  Purpose:            This module handles the ALARM list
 *
 *
 *
 *  Compiler/Assembler: Raspbian Linux GNU gcc
 *  Ext Packages:
 * 
 *  Author:             Peter Hillen
 *  Date Created:       17 Sep 2013
 * 
 *  Revisions:
 *  Entry Points:       ALARM_AddAlarm()
 *                      ALARM_DeleteAlarm()
 *                      ALARM_FindDuplicate()
 *                      ALARM_HandleAlarms()
 *                      ALARM_ListAlarms()
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
**/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <signal.h>
#include <sys/types.h>

#include <common.h>
#include "config.h"
#include "globals.h"
#include "cam_main.h"
#include "cmd_mail.h"
#include "cmd_cad.h"
#include "gen_func.h"
//
#include "cmd_alarm.h"

//#define USE_PRINTF
#include <printf.h>

//
// Alarm command handlers
//
static bool    alarm_AlarmNone           (ALARM *);
static bool    alarm_AlarmSnapshot       (ALARM *);
static bool    alarm_AlarmRecord         (ALARM *);
static bool    alarm_AlarmStop           (ALARM *);
static bool    alarm_AlarmPowerOff       (ALARM *);
static bool    alarm_AlarmReboot         (ALARM *);
static bool    alarm_AlarmMotionDetect   (ALARM *);
static bool    alarm_AlarmSlimMail       (ALARM *);
static bool    alarm_AlarmSnapMail       (ALARM *);
static bool    alarm_AlarmWanCheck       (ALARM *);
static bool    alarm_AlarmMotionMail     (ALARM *);
static bool    alarm_AlarmMotionStart    (ALARM *);
static bool    alarm_AlarmMotionStop     (ALARM *);
//
static const ALUT stAlarmLut[] = 
{
   { "none",            alarm_AlarmNone         },
   { "poweroff",        alarm_AlarmPowerOff     },
   { "reboot",          alarm_AlarmReboot       },
   { "snapshot",        alarm_AlarmSnapshot     },
   { "timelapse",       alarm_AlarmNone         },
   { "record",          alarm_AlarmRecord       },
   { "stream",          alarm_AlarmNone         },
   { "stop",            alarm_AlarmStop         },
   { "motion",          alarm_AlarmMotionDetect },
   { "slim-mail",       alarm_AlarmSlimMail     },
   { "snap-mail",       alarm_AlarmSnapMail     },
   { "mdet-mail",       alarm_AlarmMotionMail   },
   { "mdet-start",      alarm_AlarmMotionStart  },
   { "mdet-stop",       alarm_AlarmMotionStop   },
   { "wan",             alarm_AlarmWanCheck     },
   { NULL,              NULL                    }
};
static int iMaxNumAlarmCommands = sizeof(stAlarmLut)/sizeof(ALUT);

//
// Local prototypes
//
static bool    alarm_BackgroundIsActive   (GLOCMD *);
static bool    alarm_CheckClock           (char *, char *, bool);
static bool    alarm_LookupCommand        (const ALUT *, ALARM *);

//
// Function:   ALARM_AddAlarm
// Purpose:    Find free alarm entry
//
// Parms:      
// Returns:    New alarm
//
ALARM *ALARM_AddAlarm()
{
   int      iIdx;
   ALARM   *pstAlarm=pstMap->G_stAlarms;
   
   for(iIdx=0; iIdx<MAX_NUM_ALARMS; iIdx++)
   {
      if( (pstAlarm->iFlags & ALARM_USED) != ALARM_USED) 
      {
         //
         // Free space found: return new alarm
         //
         LOG_Report(0, "ALM", "Add alarm: %s:Run(%s)", pstAlarm->pcDandT, pstAlarm->pcCommand);
         pstMap->G_iNumAlarms++;
         return(pstAlarm);
      }
      pstAlarm++;
   }
   return(NULL);
}

//
// Function:   ALARM_CheckAlarms
// Purpose:    Check all alarm entries
//
// Parms:      
// Returns:    Num alarms
//
int ALARM_CheckAlarms()
{
   int      iIdx, iNr=0;
   ALARM   *pstAlarm=pstMap->G_stAlarms;
   
   for(iIdx=0; iIdx<MAX_NUM_ALARMS; iIdx++)
   {
      if(pstAlarm->iFlags & ALARM_USED) 
      {
         if((GEN_STRLEN(pstAlarm->pcCommand) > 0) && (GEN_STRLEN(pstAlarm->pcDandT) > 0)) 
         {
            iNr++;
         }
         else
         {
            LOG_Report(0, "ALM", "Check alarm: Delete Bad[%d]", iIdx);
            GEN_MEMSET(pstAlarm, 0, sizeof(ALARM));
         }
      }
      pstAlarm++;
   }
   ALARM_ListAlarms(NULL);
   return(iNr);
}

//
// Function:   ALARM_DeleteAlarm
// Purpose:    Delete this entry
//
// Parms:      AlarmNew
// Returns:    
//
void ALARM_DeleteAlarm(ALARM *pstDel)
{
   if(pstDel)
   {
      LOG_Report(0, "ALM", "Delete alarm: %s:Run(%s)", pstDel->pcDandT, pstDel->pcCommand);
      GEN_MEMSET(pstDel, 0, sizeof(ALARM));
      pstMap->G_iNumAlarms--;
   }
}

//
//  Function:   ALARM_FindDuplicate
//  Purpose:    Find alarm entry duplicated
//
//  Parms:      DataTime, Command
//  Returns:    pstAlarm of duplicate or NULL
//
ALARM *ALARM_FindDuplicate(char *pcAlarmDandT, char *pcAlarmCommand)
{
   int      iIdx;
   ALARM   *pstAlarm=pstMap->G_stAlarms;
   
   for(iIdx=0; iIdx<MAX_NUM_ALARMS; iIdx++)
   {
      if(pstAlarm->iFlags & ALARM_USED) 
      {
         if( (GEN_STRCMP(pstAlarm->pcCommand, pcAlarmCommand) == 0)
               &&
             (alarm_CheckClock(pstAlarm->pcDandT, pcAlarmDandT, TRUE)) )
         {
            PRINTF3("cmd_AlarmFindDuplicate():Alarm[%d]duplicate (%s-%s)" CRLF, iIdx, pstAlarm->pcDandT, pstAlarm->pcCommand);
            return(pstAlarm);
         }
      }
      pstAlarm++;
   }
   PRINTF3("cmd_AlarmFindDuplicate():Alarm[%d]duplicate (%s-%s): No Duplicate found" CRLF, iIdx, pstAlarm->pcDandT, pstAlarm->pcCommand);
   return(NULL);
}

//
// Function:   ALARM_GetCommand
// Purpose:    Return an alarm command
//
// Parms:      Index 0...max
// Returns:    Command^
//
const char *ALARM_GetCommand(int iIdx)
{  
   const char *pcCmd=NULL;

   if(iIdx < iMaxNumAlarmCommands)
   {
      pcCmd = stAlarmLut[iIdx].pcCommand;
   }
   return(pcCmd);
}
//
// Function:   ALARM_HandleAlarms
// Purpose:    Alarm clock handler
//
// Parms:      Alarm function list
// Returns:    FALSE if one of the tasks run returns FALSE, else TRUE
//
bool ALARM_HandleAlarms(void)
{  
   bool        fCc=TRUE;
   int         iIdx;
   u_int32     ulSecs;
   char        cDateTime[DATE_TIME_SIZE+1];
   ALARM      *pstAlarm=pstMap->G_stAlarms;
   const ALUT *pstAlarmList=stAlarmLut;

   //
   if( (LCD_MODE(LCD_MODE_ASK) != LCD_MODE_FB) && (pstMap->G_iLcdBacklight > 0) )
   {
      //PRINTF1("ALARM_HandleAlarms():Backlight=%d" CRLF, pstMap->G_iLcdBacklight);
      //
      // after delay:
      //    o Turn LCD Backlight OFF 
      //    o Switch back to Text mode
      //
      if(--pstMap->G_iLcdBacklight == 0) 
      {
         LCD_MODE(LCD_MODE_TEXT);
         LCD_BACKLIGHT(0);
      }
   }
   //
   // Get Time and date, check if one of the alarms went off
   //
   ulSecs = RTC_GetDateTime(NULL);
   //
   // Show time on LCD
   //
   RTC_ConvertDateTime(TIME_FORMAT_HH_MM_SS, cDateTime, ulSecs);
   //
   // Duplicate t&d
   //
   LCD_TEXT(LCD_ROW_TIME, LCD_COL_TIME+LCD_ACTUAL_COLS, cDateTime);

#ifdef FEATURE_VIRTUAL_TOGGLE
   {
      VLCD    *pstLcd=&pstMap->G_stVirtLcd;
      //
      // Swap screens every 5 secs
      //
      if( (ulSecs%10) == 0)   LCD_DISPLAY(pstLcd->ubVirtRow, 0);
      if( (ulSecs%10) == 5)   LCD_DISPLAY(pstLcd->ubVirtRow, LCD_ACTUAL_COLS);
   }
#endif //FEATURE_VIRTUAL_TOGGLE
   //
   // Get time for Alarm list
   //
   RTC_ConvertDateTime(TIME_FORMAT_YYYY_MM_DD_HH_MM_SS, cDateTime, ulSecs);
   //PRINTF1("ALARM_HandleAlarms():Alarm check (time=%s)" CRLF, cDateTime);

   for(iIdx=0; iIdx<MAX_NUM_ALARMS; iIdx++)
   {
      if(pstAlarm->iFlags & ALARM_USED) 
      {
         if(pstAlarm->ulRepNext > 0)
         {
            //
            // Repetition mode: it prevails the hard timer
            //
            if(pstAlarm->ulRepNext <= ulSecs)
            {
               //
               // Repetition fired
               //
               PRINTF5("ALARM_HandleAlarms():Soft[%d]:Alarm check (time=%s + %d sec + %d x): Run %s" CRLF, pstAlarm->iRepCount, cDateTime, pstAlarm->iRepTime, pstAlarm->iRepNum, pstAlarm->pcCommand);
               //LOG_Report(0, "ALM", "%s <%s>", pstAlarm->pcDandT, pstAlarm->pcCommand);
               if(pstAlarm->iRepCount) 
               {
                  if(pstAlarm->iRepCount > 0) pstAlarm->iRepCount--;
                  if( alarm_LookupCommand(pstAlarmList, pstAlarm) == FALSE) fCc = FALSE;
                  pstAlarm->ulRepNext = ulSecs + pstAlarm->iRepTime;
               }
               else
               {
                  //
                  // repetition over: resume hard clock mode
                  //
                  pstAlarm->ulRepNext = 0;
                  pstAlarm->iFlags &= ~ALARM_ARMED;
               }
            }
         }
         else
         {
            //
            // Hard clock mode
            //
            if( alarm_CheckClock(cDateTime, pstAlarm->pcDandT, TRUE) )
            {
               if(pstAlarm->iFlags & ALARM_ARMED)
               {
                  PRINTF5("ALARM_HandleAlarms():Hard[%d]:Alarm check (time=%s + %d sec + %d x): Run %s" CRLF, pstAlarm->iRepCount, cDateTime, pstAlarm->iRepTime, pstAlarm->iRepNum, pstAlarm->pcCommand);
                  //LOG_Report(0, "ALM", "%s <%s>", pstAlarm->pcDandT, pstAlarm->pcCommand);
                  if( alarm_LookupCommand(pstAlarmList, pstAlarm) == FALSE) fCc = FALSE;
                  //
                  // Trigger the repetition scheme on hard alarms
                  //    iRepTime  = repetition time (every xx secs), 0 = no repetition
                  //    ulRepNext = next seconds tickcount
                  //    iRepNum   = total number of repetions, 0 is indefinitely
                  //    iRepCount = remaining repetion count, -1 is indefinitely
                  //
                  if(pstAlarm->iRepTime)  
                  {
                     pstAlarm->ulRepNext = ulSecs + pstAlarm->iRepTime;
                     if(pstAlarm->iRepNum)   pstAlarm->iRepCount = pstAlarm->iRepNum - 1;
                     else                    pstAlarm->iRepCount = -1;
                     PRINTF2("ALARM_HandleAlarms(): %s - Enable triggers: RepCount=%d" CRLF, pstAlarm->pcCommand, pstAlarm->iRepCount);
                  }
                  else 
                  {
                     pstAlarm->ulRepNext = 0;
                     pstAlarm->iRepCount = 0;
                     PRINTF1("ALARM_HandleAlarms(): %s - Disable triggers" CRLF, pstAlarm->pcCommand);
                     pstAlarm->iFlags &= ~ALARM_ARMED;
                  }
               }
            }
            else
            {
               pstAlarm->iFlags |= ALARM_ARMED;
            }
         }
      }
      pstAlarm++;
   }
   return(fCc);
}

//
// Function:   ALARM_ListAlarms
// Purpose:    List all Alarms
//
// Parms:      Result ptr
// Returns:    Result size
// Note:       Caller MUST free the formatted Alarm list if supplied.
//
int ALARM_ListAlarms(char **ppcAlarms)
{  
   static const char pcAlarmSet[]   = "Alarms[%2d]:Count=%2d Time=%4d Nr=%2d D&T=%s Exec=%s" CRLF;
   static const char pcAlarmFree[]  = "Alarms[%2d]:** Free **" CRLF;
   //
   int      iIdx, iSize=0;
   char    *pcBuffer=NULL;
   char    *pcTemp;
   ALARM   *pstAlarm=pstMap->G_stAlarms;
   
   if(ppcAlarms) pcTemp = safemalloc(256);
   //
   for(iIdx=0; iIdx<MAX_NUM_ALARMS; iIdx++)
   {
      if(pstAlarm->iFlags & ALARM_USED) 
      {
         if(ppcAlarms)
         {
            GEN_SNPRINTF(pcTemp, 255, pcAlarmSet, iIdx, pstAlarm->iRepCount, pstAlarm->iRepTime, 
                           pstAlarm->iRepNum, pstAlarm->pcDandT, pstAlarm->pcCommand);
            //
            iSize   += GEN_STRLEN(pcTemp);
            pcBuffer = GEN_Remalloc(pcBuffer, iSize);
            GEN_STRCAT(pcBuffer, pcTemp);
         }
         #ifdef FEATURE_LIST_ALARMS
         LOG_printf(pcAlarmSet, iIdx, pstAlarm->iRepCount, pstAlarm->iRepTime, 
                     pstAlarm->iRepNum, pstAlarm->pcDandT, pstAlarm->pcCommand);
         #endif   //FEATURE_LIST_ALARMS
      }
      else
      {
         if(ppcAlarms)
         {
            GEN_SNPRINTF(pcTemp, 255, pcAlarmFree, iIdx);
            //
            iSize   += GEN_STRLEN(pcTemp);
            pcBuffer = GEN_Remalloc(pcBuffer, iSize);
            GEN_STRCAT(pcBuffer, pcTemp);
         }
         #ifdef FEATURE_LIST_ALARMS
         LOG_printf(pcAlarmFree, iIdx);
         #endif   //FEATURE_LIST_ALARMS
      }
      pstAlarm++;
   }
   //
   // Pass formatted result to caller, if requested
   //
   if(ppcAlarms) 
   {
      *ppcAlarms = pcBuffer;
       safefree(pcTemp);
   }
   return(iSize);
}

/*------  Local functions separator ------------------------------------
__LOCAL_FUNCTIONS(){};
----------------------------------------------------------------------------*/

//
// Function:   alarm_BackgroundIsActive
// Purpose:    Check if any background activity is ongoing
//
// Parms:      Command struct
// Returns:    TRUE if background activity
//
static bool alarm_BackgroundIsActive(GLOCMD *pstCmd)
{
   if( GEN_IsBusy(pstCmd) )                                         return(TRUE);
   if(!GEN_CheckBgndTerminated("Help", GLOBAL_PidGet(PID_HELP)) )   return(TRUE);
   if(!GEN_CheckBgndTerminated("Util", GLOBAL_PidGet(PID_UTIL)) )   return(TRUE);
   //
   return(FALSE);
}

//
// Function:   alarm_CheckClock
// Purpose:    Alarm Clock: Check if we hit an alarm timestamp
//
// Parms:      Current date/time, alarm date/time, exact-flag
// Returns:    TRUE if alarm goes off
// Note:       pcCurTime   = 20170527_112555
//             pcAlarmTime = 20170527_11....
//             The exact flag allows a hit if the alarmtime is in the past
//
static bool alarm_CheckClock(char *pcCurTime, char *pcAlarmTime, bool fExact)
{
   bool  fAlarmActive=TRUE;
   bool  fChecking=TRUE;

   //PRINTF1("alarm-CheckClock():Alarm check (Time=%s"CRLF, pcCurTime);
   //
   while(fChecking)
   {
      if(*pcAlarmTime)
      {
         if(*pcAlarmTime == '*')
         {
            // end of compare: exit with result
            fChecking = FALSE;
         }
         else if(*pcAlarmTime == '.')
         {
            // this match is always OKee  
         }
         else if(*pcAlarmTime == *pcCurTime)
         {
            // this match is OKee  
         }
         else if(!fExact)
         {
            // Trigger also if the alarm time is in the past 
            if(*pcAlarmTime < *pcCurTime) fChecking = FALSE;
            else                          fExact    = TRUE;
         }
         else
         {
            // no good
            fAlarmActive = FALSE;
            fChecking    = FALSE;
         }
         pcCurTime++;
         pcAlarmTime++;
      }
      else
      {
         // End of compare: exit with result
         fChecking = FALSE;
      }
   }
   return(fAlarmActive);
}

//
// Function:   alarm_LookupCommand
// Purpose:    Lookup an alarm in the list
//
// Parms:      Alarm lookup, alarm time 
// Returns:    TRUE if OKee
//
static bool alarm_LookupCommand(const ALUT *pstLut, ALARM *pstAlarm)
{
   bool     fCc=FALSE;

   while(pstLut->pcCommand)
   {
      if(GEN_STRCMP(pstLut->pcCommand, pstAlarm->pcCommand) == 0)
      {
         PRINTF2("alarm-LookupCommand():Run at %s task=%s" CRLF, pstAlarm->pcDandT, pstAlarm->pcCommand);
         LOG_Report(0, "ALM", "%s Run(%s)", pstAlarm->pcDandT, pstAlarm->pcCommand);
         fCc = pstLut->pfFunc(pstAlarm);
      }
      pstLut++;
   }
   return(fCc);
}


/*------  Local functions separator ------------------------------------
__ALARM_FUNCTIONS(){};
----------------------------------------------------------------------------*/


//
// Function:   alarm_AlarmNone
// Purpose:    
//
// Parms:      ALARM *
// Returns:    TRUE if command handled
//
static bool alarm_AlarmNone(ALARM *pstAlarm)
{
   PRINTF("alarm-AlarmNone" CRLF);
   GLOBAL_Notify(PID_CAMR, GLOBAL_ALM_NFY, SIGUSR1);
   return(TRUE);
}

//
// Function:   alarm_AlarmPowerOff
// Purpose:    Poweroff the RPI
//
// Parms:      ALARM *
// Returns:    TRUE if command handled
//
static bool alarm_AlarmPowerOff(ALARM *pstAlarm)
{
   const char *pcShell="shutdown -h -t 10 now \"Power OFF in 10 Secs\" ";
   
   //
   // One shot/multiple shots
   //
   LOG_Report(0, "ALM", "Power Off [%s]", (char *)pcShell);
   system(pcShell);
   GLOBAL_Notify(PID_CAMR, GLOBAL_ALM_NFY, SIGUSR1);
   return(TRUE);
}

//
// Function:   alarm_AlarmReboot
// Purpose:    Reboot the RPI
//
// Parms:      ALARM *
// Returns:    TRUE if command handled
//
static bool alarm_AlarmReboot(ALARM *pstAlarm)
{
   const char *pcShell="shutdown -F -r -t 10 now \"Rebooting in 10 Secs\" ";

   //
   // One shot/multiple shots
   //
   LOG_Report(0, "ALM", "Reboot with fsck [%s]", (char *)pcShell);
   system(pcShell);
   GLOBAL_Notify(PID_CAMR, GLOBAL_ALM_NFY, SIGUSR1);
   return(TRUE);
}

//
// Function:   alarm_AlarmRecord
// Purpose:    Alarm command recording
//
// Parms:      ALARM *
// Returns:    TRUE if command handled
//
static bool alarm_AlarmRecord(ALARM *pstAlarm)
{
   bool     fCc=FALSE;
   u_int32  ulSecs;
   char    *pcFile;
   GLOCMD  *pstCmd;

   pstCmd = GLOBAL_CommandCopy();
   if( alarm_BackgroundIsActive(pstCmd) )
   {
      PRINTF("alarm-AlarmRecord():Command is in progress: ignore" CRLF);
      LOG_Report(0, "ALM", "IGNORE %s Run(%s)", pstAlarm->pcDandT, pstAlarm->pcCommand);
   }
   else
   {
      //
      // No command in progress:
      // Enable the camera parameters we want to pass to the utility
      //
      GLOBAL_SetParameterChanged(CAM_WIDTH);
      GLOBAL_SetParameterChanged(CAM_HEIGHT);
      GLOBAL_SetParameterChanged(CAM_ROTATION);
      //
      // Collect command and parameters
      //    Build recording output filename "YYYYMMDD_HHMMSS"
      //    Spawn the command and args to the CMND
      //
      pcFile = (char *) safemalloc(MAX_PATH_LEN);
      ulSecs = RTC_GetSystemSecs();
      RTC_ConvertDateTime(TIME_FORMAT_YYYY_MM_DD_HH_MM_SS, pcFile, ulSecs);
      //
      GEN_MEMSET(pstCmd, 0, sizeof(GLOCMD));
      pstCmd->iCommand = CAM_CMD_ALM_RECORD;
      pstCmd->iArgs    = CAM_REC;
      GEN_SPRINTF(pstCmd->pcExec, RPI_VID_EXEC);
      GLOBAL_InsertOptions(pstCmd->pcArgs, MAX_ARG_LEN, CAM_REC);
      //
      LOG_Report(0, "ALM", "alarm-AlarmRecord [%s]:%s", pstMap->G_pcLastFile, pstCmd->pcArgs);
      PRINTF2("alarm-AlarmRecord():[%s]:%s" CRLF, pstMap->G_pcLastFile, pstCmd->pcArgs);
      //
      // Spawn command to ourselves
      //
      GLOBAL_CommandPutAll(pstCmd);
      GLOBAL_Notify(PID_CMND, GLOBAL_ALM_RUN, SIGUSR1);
      safefree(pcFile);
      fCc = TRUE;
   }
   GLOBAL_CommandDestroy(pstCmd);
   return(fCc);
}

//
// Function:   alarm_AlarmSnapshot
// Purpose:    Alarm command snapshot
//
// Parms:      ALARM *
// Returns:    TRUE if command handled
//
static bool alarm_AlarmSnapshot(ALARM *pstAlarm)
{
   bool     fCc=FALSE;
   u_int32  ulSecs;
   char    *pcFile;
   GLOCMD  *pstCmd;

   pstCmd = GLOBAL_CommandCopy();
   if( alarm_BackgroundIsActive(pstCmd) )
   {
      PRINTF("alarm-AlarmSnapshot():Command is in progress: ignore" CRLF);
      LOG_Report(0, "ALM", "IGNORE %s Run(%s)", pstAlarm->pcDandT, pstAlarm->pcCommand);
   }
   else
   {
      //
      // No command in progress:
      // Enable the camera parameters we want to pass to the utility
      //
      GLOBAL_SetParameterChanged(CAM_WIDTH);
      GLOBAL_SetParameterChanged(CAM_HEIGHT);
      GLOBAL_SetParameterChanged(CAM_ROTATION);
      GLOBAL_SetParameterChanged(CAM_FILEEXT);
      //
      // Collect command and parameters
      //    Build foto output filename "YYYYMMDD_HHMMSS"
      //    Spawn the command and args to the CMND
      //
      pcFile = (char *) safemalloc(MAX_PATH_LEN);
      ulSecs = RTC_GetSystemSecs();
      RTC_ConvertDateTime(TIME_FORMAT_YYYY_MM_DD_HH_MM_SS, pcFile, ulSecs);
      GEN_SPRINTF(pstMap->G_pcLastFile, "%s%s%s.%s", pstMap->G_pcWwwDir, pstMap->G_pcPixPath, pcFile, pstMap->G_pcPixFileExt);
      //
      GEN_MEMSET(pstCmd, 0, sizeof(GLOCMD));
      pstCmd->iCommand = CAM_CMD_ALM_SNAP;
      pstCmd->iArgs    = CAM_STI;
      pstCmd->iData1   = pstAlarm->iFlags;
      GEN_SPRINTF(pstCmd->pcExec, RPI_PIX_EXEC);
      GLOBAL_InsertOptions(pstCmd->pcArgs, MAX_ARG_LEN, CAM_STI);
      //
      LOG_Report(0, "ALM", "alarm-AlarmSnapshot [%s]:%s", pstMap->G_pcLastFile, pstCmd->pcArgs);
      PRINTF2("alarm-AlarmSnapshot():[%s]:%s" CRLF, pstMap->G_pcLastFile, pstCmd->pcArgs);
      //
      // Spawn command to ourselves
      //
      GLOBAL_CommandPutAll(pstCmd);
      GLOBAL_DUMPCOMMAND("alarm-AlarmSnapshot():PID_CMND(PutAll):Alarm Run", pstCmd);
      //
      GLOBAL_Notify(PID_CMND, GLOBAL_ALM_RUN, SIGUSR1);
      safefree(pcFile);
      fCc = TRUE;
   }
   GLOBAL_CommandDestroy(pstCmd);
   return(fCc);
}

//
// Function:   alarm_AlarmStop
// Purpose:    Alarm command stop
//
// Parms:      ALARM *
// Returns:    TRUE if command handled
//
static bool alarm_AlarmStop(ALARM *pstAlarm)
{
   GLOCMD  *pstCmd;

   pstCmd = GLOBAL_CommandCopy();
   GEN_MEMSET(pstCmd, 0, sizeof(GLOCMD));
   pstCmd->iCommand = CAM_CMD_ALM_STOP;
   pstCmd->iArgs    = CAM_STP;
   //
   LOG_Report(0, "ALM", "alarm-AlarmStop");
   PRINTF("alarm-AlarmStop()" CRLF);
   //
   // Spawn command to ourselves
   //
   GLOBAL_CommandPutAll(pstCmd);
   GLOBAL_DUMPCOMMAND("alarm-AlarmStop():PID_CMND(PutAll):Alarm Run", pstCmd);
   GLOBAL_Notify(PID_CMND, GLOBAL_ALM_RUN, SIGUSR1);
   GLOBAL_CommandDestroy(pstCmd);
   return(TRUE);
}

//
// Function:   alarm_AlarmMotionDetect
// Purpose:    Alarm command motion detect
//
// Parms:      ALARM *
// Returns:    TRUE if command handled
//
static bool alarm_AlarmMotionDetect(ALARM *pstAlarm)
{
   GLOCMD  *pstCmd;

   pstCmd = GLOBAL_CommandCopy();
   GEN_MEMSET(pstCmd, 0, sizeof(GLOCMD));
   pstCmd->iCommand = CAM_CMD_ALM_MOTION;
   pstCmd->iArgs    = CAM_VEC;
   //
   LOG_Report(0, "ALM", "alarm-AlarmMotionDetect");
   PRINTF("alarm-AlarmMotionDetect()" CRLF);
   //
   // Spawn command to ourselves
   //
   GLOBAL_CommandPutAll(pstCmd);
   GLOBAL_DUMPCOMMAND("alarm-AlarmMotionDetect():PID_CMND(PutAll):Alarm Run", pstCmd);
   //
   GLOBAL_Notify(PID_CMND, GLOBAL_ALM_RUN, SIGUSR1);
   GLOBAL_CommandDestroy(pstCmd);
   return(TRUE);
}

//
// Function:   alarm_AlarmSlimMail
// Purpose:    Alarm command forward slim data to email
//
// Parms:      ALARM *
// Returns:    TRUE if command handled
//
static bool alarm_AlarmSlimMail(ALARM *pstAlarm)
{
   GLOBAL_Notify(PID_PROX, GLOBAL_EML_RUN, SIGUSR1);
   //
   return(TRUE);
}

//
// Function:   alarm_AlarmSnapMail
// Purpose:    Alarm command forward snapshot to email
//
// Parms:      ALARM *
// Returns:    TRUE if command handled
//
static bool alarm_AlarmSnapMail(ALARM *pstAlarm)
{
   //
   // Set email trigger and handle as snapshot
   //
   GEN_STRCPY(pstMap->G_pcWidth, "400");
   GEN_STRCPY(pstMap->G_pcHeight, "225");
   pstAlarm->iFlags |= ALARM_EMAIL;
   return(alarm_AlarmSnapshot(pstAlarm));
}

//
// Function:   alarm_AlarmWanCheck
// Purpose:    Alarm command WAN IP address check
//
// Parms:      ALARM *
// Returns:    TRUE if command handled
//
static bool alarm_AlarmWanCheck(ALARM *pstAlarm)
{
   bool     fCc=FALSE;
   GLOCMD  *pstCmd;

   pstCmd = GLOBAL_CommandCopy();
   if( alarm_BackgroundIsActive(pstCmd) )
   {
      PRINTF("alarm-AlarmWanCheck():Command is in progress: ignore" CRLF);
      LOG_Report(0, "ALM", "IGNORE %s Run(%s)", pstAlarm->pcDandT, pstAlarm->pcCommand);
   }
   else
   {
      GEN_MEMSET(pstCmd, 0, sizeof(GLOCMD));
      pstCmd->iCommand = CAM_CMD_ALM_WAN;
      //
      // Copy the current WAN addr into the ARGS buffer for later reference
      //
      GEN_STRNCPY(pstCmd->pcArgs, pstMap->G_pcWanAddr, MAX_ADDR_LEN);
      //
      LOG_Report(0, "ALM", "alarm-AlarmWanCheck, Current=%s", pstCmd->pcArgs);
      PRINTF1("alarm-AlarmWanCheck(): Current=%s" CRLF, pstCmd->pcArgs);
      //
      // Spawn command to ourselves
      //
      GLOBAL_CommandPutAll(pstCmd);
      GLOBAL_DUMPCOMMAND("alarm-AlarmWanCheck():PID_CMND(PutAll):Alarm Run", pstCmd);
      //
      GLOBAL_Notify(PID_CMND, GLOBAL_ALM_RUN, SIGUSR1);
      fCc = TRUE;
   }
   GLOBAL_CommandDestroy(pstCmd);
   return(fCc);
}

//
// Function:   alarm_AlarmMotionMail
// Purpose:    Alarm command Motion mail
//
// Parms:      ALARM *
// Returns:    TRUE if command handled
//
static bool alarm_AlarmMotionMail(ALARM *pstAlarm)
{
   bool     fCc=FALSE;
   GLOCMD  *pstCmd;

   pstCmd = GLOBAL_CommandCopy();
   if( alarm_BackgroundIsActive(pstCmd) )
   {
      PRINTF("alarm-AlarmMotionMail():Command is in progress: ignore" CRLF);
      LOG_Report(0, "ALM", "IGNORE %s Run(%s)", pstAlarm->pcDandT, pstAlarm->pcCommand);
   }
   else
   {
      GEN_MEMSET(pstCmd, 0, sizeof(GLOCMD));
      pstCmd->iCommand = CAM_CMD_ALM_MDET_MAIL;
      //
      // Spawn command to ourselves
      //
      GLOBAL_CommandPutAll(pstCmd);
      GLOBAL_DUMPCOMMAND("alarm-AlarmMotionMail():PID_CMND(PutAll):Alarm Run", pstCmd);
      //
      GLOBAL_Notify(PID_CMND, GLOBAL_ALM_RUN, SIGUSR1);
      fCc = TRUE;
   }
   GLOBAL_CommandDestroy(pstCmd);
   return(fCc);
}

//
// Function:   alarm_AlarmMotionStart
// Purpose:    Alarm command Motion start
//
// Parms:      ALARM *
// Returns:    TRUE if command handled
//
static bool alarm_AlarmMotionStart(ALARM *pstAlarm)
{
   bool     fCc=FALSE;
   GLOCMD  *pstCmd;

   pstCmd = GLOBAL_CommandCopy();
   if( alarm_BackgroundIsActive(pstCmd) )
   {
      PRINTF("alarm-AlarmMotionStart():Command is in progress: ignore" CRLF);
      LOG_Report(0, "ALM", "IGNORE %s Run(%s)", pstAlarm->pcDandT, pstAlarm->pcCommand);
   }
   else
   {
      pstMap->G_stMotionDetect.iMotMode = MOTION_MODE_VIDEO;
      GEN_MEMSET(pstCmd, 0, sizeof(GLOCMD));
      pstCmd->iCommand = CAM_CMD_A_VECTOR;
      //
      // Spawn command to ourselves
      //
      GLOBAL_CommandPutAll(pstCmd);
      GLOBAL_DUMPCOMMAND("alarm-AlarmMotionStart():PID_CMND(PutAll):Alarm Run", pstCmd);
      //
      GLOBAL_Notify(PID_CMND, GLOBAL_ALM_RUN, SIGUSR1);
      fCc = TRUE;
   }
   GLOBAL_CommandDestroy(pstCmd);
   return(fCc);
}

//
// Function:   alarm_AlarmMotionStop
// Purpose:    Alarm command Motion stop
//
// Parms:      ALARM *
// Returns:    TRUE if command handled
//
static bool alarm_AlarmMotionStop(ALARM *pstAlarm)
{
   bool     fCc=FALSE;
   GLOCMD  *pstCmd;

   pstCmd = GLOBAL_CommandCopy();
   if( alarm_BackgroundIsActive(pstCmd) )
   {
      PRINTF("alarm-AlarmMotionStart():Command is in progress: ignore" CRLF);
      LOG_Report(0, "ALM", "IGNORE %s Run(%s)", pstAlarm->pcDandT, pstAlarm->pcCommand);
   }
   else
   {
      pstMap->G_stMotionDetect.iMotMode = MOTION_MODE_OFF;
      GEN_MEMSET(pstCmd, 0, sizeof(GLOCMD));
      pstCmd->iCommand = CAM_CMD_A_VECTOR;
      //
      // Spawn command to ourselves
      //
      GLOBAL_CommandPutAll(pstCmd);
      GLOBAL_DUMPCOMMAND("alarm-AlarmMotionStart():PID_CMND(PutAll):Alarm Run", pstCmd);
      //
      GLOBAL_Notify(PID_CMND, GLOBAL_ALM_RUN, SIGUSR1);
      fCc = TRUE;
   }
   GLOBAL_CommandDestroy(pstCmd);
   return(fCc);
}
