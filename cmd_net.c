/*  (c) Copyright:  2013..2017  Patrn.nl, Confidential Data
 *
 *  $Workfile:          cmd_net.c
 *  $Revision:   
 *  $Modtime:    
 *
 *  Purpose:            Interface local host network functions
 *
 * 
 * 
 *  Compiler/Assembler: Raspbian Linux GNU gcc
 *  Ext Packages:
 * 
 *  Author:             Peter Hillen
 *  Date Created:       25 Dec 2013
 *
 *  Revisions:
 *  Entry Points:       
 * 
 * 
 * 
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
**/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <time.h>
#include <errno.h>
#include <sys/types.h>

#include "typedefs.h"
#include "config.h"
#include "globals.h"
#include "gen_log.h"
#include "rpi_net.h"
#include "cmd_net.h"

//#define USE_PRINTF
#include "printf.h"

//
// Local prototypes
//
static int ip_SendRequest           (char *);

//
//  Function:   IP_SendToServer
//  Purpose:    Send HTTP request string via IP to local server
//
//  Parms:      Data
//  Returns:    Pid if OKee send, else -1 on error
//
pid_t IP_SendToServer(char *pcData)
{
   int   iCc=0;
   pid_t tPid;

   //==========================================================================
   // Split process : 
   //       HELPER:    returns with tPid of the IPCOM
   //       IPCOM:     returns with tPid=0
   //
   tPid = fork();
   //
   //==========================================================================
   switch(tPid)
   {
      case 0:
         // IPCOM emulate thread: send out IP data to local host
         PRINTF1("IP_SendToServer(): Emul=%d" CRLF, tPid);
         sleep(3); //pwjh
         iCc = ip_SendRequest(pcData);
         if(iCc) LOG_Report(errno, "IPC", "IP_SendToServer(): Helper ERROR");
         _exit(iCc);
   
      case -1:
         // Error
         PRINTF("IP_SendToServer(): IP_SendToServer(): Helper ERROR" CRLF);
         LOG_Report(errno, "IPC", "IP_SendToServer(): Helper ERROR");
         break;

      default:
         // EXEC returns here: 
         PRINTF1("IP_SendToServer(): Caller returns, Emul=%d" CRLF, tPid);
         break;
   }
   return(tPid);
}


/*------  Local functions separator ------------------------------------
__Local_functions_(){};
----------------------------------------------------------------------------*/

/*
 *  Function:     ip_SendRequest
 *  Description:  Send data to local IP stack
 *
 *  Arguments:    Data to send, length
 *  Returns:      CC (0=OKee)
 *
 */
static int ip_SendRequest(char *pcData)
{
   int      iCc=0;
   int      iSend;
   int      iRead, iTotal=0;
   int      iFd;
   char    *pcIp="127.0.0.1";
   char     pcBuffer[84];
   RPIMAP  *pstMap=GLOBAL_GetMapping();

   iFd = NET_ClientConnect(pcIp, pstMap->G_pcServerPort, "tcp");
   //
   if(iFd > 0)
   {
      PRINTF("ip_SendRequest(): Connect to server OK !" CRLF);
      iSend = NET_WriteString(iFd, (char *) pcData);
      if(iSend > 0)
      {
         PRINTF1("ip_SendRequest():%d bytes send" CRLF, iSend);
         //pwjh sleep(3);
         do
         {
            //iRead = NET_ReadString(iFd, pcBuffer, 80);
            iRead = read(iFd, pcBuffer, 80);
            if(iRead < 0)
            {
               PRINTF1("ip_SendRequest():ERROR %d reading socket" CRLF, iRead);
            }
            else
            {
               //PRINTF1("ip_SendRequest():%s" CRLF, pcBuffer);
               iTotal += iRead;
            }
         }
         while(iRead > 0);
         //
         PRINTF1("ip_SendRequest():%d bytes read" CRLF, iTotal);
      }
      else
      {
         iCc=1;
         PRINTF1("ip_SendRequest():ERROR: %d bytes send" CRLF, iSend);
      }
      NET_ClientDisconnect(iFd);
   }
   else
   {
      iCc=1;
      PRINTF1("ip_SendRequest(): Not able to connect to server %s !" CRLF, pcIp);
   }
   return(iCc);
}   

