/*  (c) Copyright:  2017  Patrn, Confidential Data
 *
 *  Workfile:           cmd_main.h
 *  Revision:          
 *  Modtime:           
 *
 *  Purpose:            Headerfile for command thread
 *
 *
 *
 *  Compiler/Assembler: Raspbian Linux GNU gcc
 *  Ext Packages:
 *
 *  Author:             Peter Hillen
 *  Date Created:       09 Mar 2017
 *
 *  Revisions:
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
**/

#ifndef _CMD_MAIN_H_
#define _CMD_MAIN_H_

int   CMD_Init           (void);


#endif /* _CMD_MAIN_H_ */
