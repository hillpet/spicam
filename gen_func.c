/*  (c) Copyright:  2017  Patrn, Confidential Data
 *
 *  Workfile:           gen_func.c
 *  Revision:          
 *  Modtime:           
 *
 *  Purpose:            Generic functions
 *
 *
 *
 *  Compiler/Assembler: Raspbian Linux GNU gcc
 *  Ext Packages:
 *
 *  Author:             Peter Hillen
 *  Date Created:       09 Mar 2017
 *
 *  Revisions:
 *  Entry Points:       
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
**/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdarg.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>
#include <sched.h>
#include <time.h>
//
#include <common.h>
#include "config.h"
#include "globals.h"
#include "rpi_page.h"
#include "graphics.h"
#include "gen_func.h"

//#define USE_PRINTF
#include <printf.h>


/*------  Local functions separator ------------------------------------
__GLOBAL_FUNCTIONS_________(){};
----------------------------------------------------------------------------*/

// 
// Function:   GEN_SegmentationFault
// Purpose:    Store segv cause
// 
// Parameters: 
// Returns:    
// 
void GEN_SegmentationFault(const char *pcFile, int iLineNr)
{
   GEN_STRNCPY(pstMap->G_pcSegFaultFile, pcFile, MAX_URL_LENZ);
   pstMap->G_iSegFaultLine = iLineNr;
   pstMap->G_iSegFaultPid  = getpid();
}

// 
// Function:   GEN_CheckDelete
// Purpose:    Check the status of the delete parameter
// 
// Parameters: 
// Returns:    TRUE if delete parameter found
// 
bool GEN_CheckDelete()
{
   bool     fDelete=FALSE;

   if(GEN_STRNCMPI(pstMap->G_pcDelete, "yes", 3) == 0)
   {
      fDelete = TRUE;
      GEN_STRCPY(pstMap->G_pcDelete, "no");
   }
   return(fDelete);
}

//
// Function:   GEN_Free
// Purpose:    Free this command
//
// Parms:      Command struct ^
// Returns:    
// Note:       
//             
void GEN_Free(GLOCMD *pstCmd)
{
   int   iStatus=pstCmd->iStatus;
   int   iError=pstCmd->iError;

   PRINTF("GEN_Free()" CRLF);
   //
   // pstCmd->iCommand   = CAM_CMD_IDLE;
   // pstCmd->iArgs      = CAM____;
   // pstCmd->pfCallback = NULL;
   //
   // Preserve status !
   //
   GEN_MEMSET(pstCmd, 0, sizeof(GLOCMD));
   //
   pstCmd->iStatus = iStatus;
   pstCmd->iError  = iError;
   GLOBAL_CommandPutAll(pstCmd);
   GLOBAL_DUMPCOMMAND("GEN_Free(global)", NULL);
}

//
// Function:   GEN_IsBusy
// Purpose:    Check if a command is still in progress
//
// Parms:      Command struct ^
// Returns:    TRUE if so
// Note:       
//             
bool GEN_IsBusy(GLOCMD *pstCmd)
{
   return(pstCmd->iCommand != CAM_CMD_IDLE);
}

// 
// Function:   GEN_Signal
// Purpose:    Signal a PID_xx
// 
// Parameters: PIDT PID_xxx, signal
// Returns:    
// Note:       
// 
void GEN_Signal(PIDT ePid, int iSignal)
{
   pid_t tPid;

   tPid = GLOBAL_PidGet(ePid);
   if(tPid > 0) kill(tPid, iSignal);
}

// 
// Function:   GEN_SignalWait
// Purpose:    Signal a PID_xx and wait for completion
// 
// Parameters: PIDT PID_xxx, signal
// Returns:    
// Note:       
// 
void GEN_SignalWait(PIDT ePid, int iSignal)
{
   pid_t tPid;

   tPid = GLOBAL_PidGet(ePid);
   if(tPid > 0) 
   {
      kill(tPid, iSignal);
      GEN_WaitProcess(tPid);
      GLOBAL_PidPut(ePid, 0);
   }
}


/*------  Local functions separator ------------------------------------
__LOCAL_FUNCTIONS_________(){};
----------------------------------------------------------------------------*/

//  
// Function:   
// Purpose:    
//  
// Parameters: 
// Returns:    
// Note:       
//             
