/*  (c) Copyright:  2017  Patrn, Confidential Data
 *
 *  Workfile:           dyn_rcu.h
 *  Revision:          
 *  Modtime:           
 *
 *  Purpose:            Extract headerfile for RPI dynamic http pages
 *
 *
 *
 *  Compiler/Assembler: Raspbian Linux GNU gcc
 *  Ext Packages:
 *
 *  Author:             Peter Hillen
 *  Date Created:       09 Mar 2017
 *
 *  Revisions:
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
**/

EXTRACT_DYN(RCU_DYN_JSON_RPC,      HTTP_JSON,  0, DYN_FLAG_PORT, "jsonrpc",           kodi_DynPageKodiRpc        )
EXTRACT_DYN(RCU_DYN_HTML_XBMC,     HTTP_HTML,  0, DYN_FLAG_PORT, "xbmcHttp",          kodi_DynPageXbmc           )
