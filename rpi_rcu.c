/*  (c) Copyright:  2014..2018  Patrn.nl, Confidential Data 
**
**  $Workfile:          rpi_rcu.c
**  $Revision:   
**  $Modtime:    
**
**  Purpose:            Interface PifaceCad IR RCU functions
**
*******************************************************************************    
**
**   struct lirc_list 
**    {
**            char*                   string;
**            struct lirc_list*       next;
**    };
**    
**    struct lirc_code
**    {
**            char*                   remote;
**            char*                   button;
**            struct lirc_code*       next;
**    };
**    
**    struct lirc_config
**    {
**            char*                           lircrc_class; 
**            char*                           current_mode;
**            struct lirc_config_entry*       next;
**            struct lirc_config_entry*       first;
**            int                             sockfd;
**    };
**    
**    struct lirc_config_entry
**    {
**            char*                           prog;
**            struct lirc_code*               code;
**            unsigned int                    rep_delay;
**            unsigned int                    ign_first_events;
**            unsigned int                    rep;
**            struct lirc_list*               config;
**            char*                           change_mode;
**            unsigned int                    flags;
**            char*                           mode;
**            struct lirc_list*               next_config;
**            struct lirc_code*               next_code;
**            struct lirc_config_entry*       next;
**    };
**    
**    typedef struct
**    {
**            char    packet[PACKET_SIZE + 1];        
**            char    buffer[PACKET_SIZE + 1];        
**            char    reply[PACKET_SIZE + 1];         
**            int     head;                           
**            int     reply_to_stdout;                
**            char*   next;                           
**    } lirc_cmd_ctx;
**    
*******************************************************************************    
**  Entry Points:       
*******************************************************************************    
**    
**    int         lirc_init                     (const char* prog, int verbose);
**    int         lirc_deinit                   (void);
**    int         lirc_readconfig               (const char* path, struct lirc_config** config, int (check) (char* s));
**    void        lirc_freeconfig               (struct lirc_config* config);
**    char*       lirc_nextir                   (void);
**    char*       lirc_ir2char                  (struct lirc_config* config, char* code);
**    int         lirc_nextcode                 (char** code);
**    int         lirc_code2char                (struct lirc_config* config, char* code, char** string);
**    //
**    // new interface for client daemon
**    //
**    int         lirc_readconfig_only          (const char* file, struct lirc_config** config, int (check) (char* s));
**    int         lirc_code2charprog            (struct lirc_config* config, char* code, char** string, char** prog);
**    size_t      lirc_getsocketname            (const char* id, char* buf, size_t size);
**    const char* lirc_getmode                  (struct lirc_config* config);
**    const char* lirc_setmode                  (struct lirc_config* config, const char* mode);
**    //
**    // 0.9.2: New interface for sending data
**    //
**    int         lirc_command_init             (lirc_cmd_ctx* ctx, const char* fmt, ...);
**    int         lirc_command_run              (lirc_cmd_ctx* ctx, int fd);
**    void        lirc_command_reply_to_stdout  (lirc_cmd_ctx* ctx);
**    int         lirc_send_one                 (int fd, const char* remote, const char* keysym);
**    int         lirc_simulate                 (int fd, const char* remote, const char* keysym, int scancode, int repeat);
**    int         lirc_get_remote_socket        (const char* address, int port, int quiet);
**    int         lirc_get_local_socket         (const char* path, int quiet);**
**
**
**
 *  Compiler/Assembler: Raspbian Linux
 *  Ext Packages:
**
 *  Author:             Peter Hillen
 *  Date Created:       20 Feb 2014
**
 *  Revisions:
 *    $Log:   $
 *
 *
 **/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <time.h>
#include <sys/types.h>
#include <signal.h>
#include <lirc/lirc_client.h>

#include <common.h>
#include "config.h"
#include "globals.h"
#include "gen_func.h"
#include "rpi_rcu.h"

//#define USE_PRINTF
#include <printf.h>

static struct lirc_config *pstRcuConfig=NULL;
static bool                fLircRunning=TRUE;
//
// Local prototypes
//
static bool rcu_Daemon              ();
static void rcu_Exit                ();
static int  rcu_LircCheck           (char *);
static int  rcu_LookupRcuKey        (char *);
static void rcu_ReceiveIrKeys       ();
static void rcu_ReceiveSignalTerm   (int);
static void rcu_ReceiveSignalInt    (int);
static void rcu_SendSignalToHost    (int);
static bool rcu_SignalRegister      (sigset_t *);

//
//  Function:   RCU_Init
//  Purpose:    Init PifaceCad RCU interface
//
//  Parms:      
//  Returns:    Startup code
//
int RCU_Init(void)
{
   int   iCc=0;

   if(rcu_Daemon()) iCc = GLOBAL_RCU_INI;
   return(iCc);
}

//
//  Function:   RCU_Exit
//  Purpose:    Close PifaceCad RCU interface
//
//  Parms:      
//  Returns:    
//
void RCU_Exit(void)
{
}

/*------  Local functions separator ------------------------------------
__Local_functions_(){};
----------------------------------------------------------------------------*/

// 
//  Function    : rcu_Exit
//  Description : Exit this thread
// 
//  Parameters  : 
//  Returns     : 
// 
// 
static void rcu_Exit()
{
   //
   // Termination signal has been receiver: clean up and exit
   //
   if(pstRcuConfig)
   {
      lirc_freeconfig(pstRcuConfig);
      lirc_deinit();
      pstRcuConfig = NULL;
      PRINTF("rcu_Exit(): Term signal received: exit." CRLF);
   }
   fLircRunning = FALSE;
   PRINTF("rcu_Exit(): LIRC thread now exiting" CRLF);
}

// 
//  Function    : rcu_LircCheck
//  Description : Callback from lirc config
// 
//  Parameters  : config string
//  Returns     : 0 if string seems OKee, else -1
// 
//  Note        : The RCU IR Keynames are defined in the lirc config file: /etc/lirc/lircrc
//                The actual remote is defined in the RCU config file:     /etc/lirc/lircd.conf
// 
static int rcu_LircCheck(char *pcCfg)
{
   if(pcCfg)
   {
      PRINTF1("rcu_LircCheck(): LIRC config %s" CRLF, pcCfg);
   }
   else
   {
      PRINTF("rcu_LircCheck(): LIRC NO config" CRLF);
   }
   return(0);
}

// 
//  Function    : rcu_LookupRcuKey
//  Description : Lookup the RCU key for signalling to the main thread
// 
//  Parameters  : Ascii RCU key
//  Returns     : Key enum or RCU_NOKEY
// 
//  Note        : The RCU IR Keynames are defined in the lirc config file: /etc/lirc/lircrc
//                The actual remote is defined in the RCU config file:     /etc/lirc/lircd.conf
// 
static int rcu_LookupRcuKey(char *pcKey)
{
   const RCULUT stRcuLookup[] =
   {
      { "Power",        RCU_POWER         },
      { "Select",       RCU_OK            },
      { "Enter",        RCU_OK            },
      { "Up",           RCU_UP            },
      { "Down",         RCU_DOWN          },
      { "Left",         RCU_LEFT          },
      { "Right",        RCU_RIGHT         },
      { "Play",         RCU_PLAY          },
      { "Stop",         RCU_STOP          },
      { "Exit",         RCU_EXIT,         },
      { "Pause",        RCU_PAUSE         },
      { "Skipplus",     RCU_NEXT          },
      { "Skipminus",    RCU_PREVIOUS      },
      { "Forward",      RCU_FORWARD       },
      { "Reverse",      RCU_REWIND        },
      { "Channelplus",  RCU_CHANNELUP     },
      { "Channelminus", RCU_CHANNELDOWN   },
      { "Title",        RCU_EPG           },
      { "Info",         RCU_INFO          },
      { "Menu",         RCU_MENU          },
      { "Back",         RCU_ESC           },
      { "Zero",         RCU_0             },
      { "One",          RCU_1             },
      { "Two",          RCU_2             },
      { "Three",        RCU_3             },
      { "Four",         RCU_4             },
      { "Five",         RCU_5             },
      { "Six",          RCU_6             },
      { "Seven",        RCU_7             },
      { "Eight",        RCU_8             },
      { "Nine",         RCU_9             },
      { "Red",          RCU_RED           },
      { "Blue",         RCU_BLUE          },
      { "Green",        RCU_GREEN         },
      { "Yellow",       RCU_YELLOW        }
   };
   const int iNumRcuLookup = sizeof(stRcuLookup)/sizeof(RCULUT);

   int   x, iCc=RCU_NOKEY;

   for(x=0; x<iNumRcuLookup;x++)
   {
      if(strcasecmp((char *)stRcuLookup[x].pcIrKey, pcKey) == 0)
      {
         iCc = stRcuLookup[x].iRcuKey;
         break;
      }
   }
   return(iCc);
}

// 
//  Function    : rcu_Daemon
//  Description : Split this thread and run the lirc function client in a 
//                separate thread
// 
//  Parameters  : 
//  Returns     : TRUE if OKee
// 
// 
static bool rcu_Daemon()
{
   bool     fCc=TRUE;
   pid_t    tPid;

   //==========================================================================
   // Split process : 
   //       Parent thread returns with tPid of the EXEC thread
   //       LIRC   thread returns with tPid=0
   //
   tPid = fork();
   //==========================================================================
   switch(tPid)
   {
      case 0:
         //
         // LIRC child
         //
         rcu_ReceiveIrKeys();
         _exit(0);
         break;

      case -1:
         // Error
         fCc = FALSE;
         break;
            
      default:
         //
         // This is the parent thread which started the LIRC process
         //
         PRINTF1("rcu_Daemon(): LIRC thread now running: pid=%d" CRLF, tPid);
         GLOBAL_PidPut(PID_LIRC, tPid);
         break;
   }
   return(fCc);
}

// 
//  Function    : rcu_ReceiveIrKeys
//  Description : Start receiving the RCU IR keys using the LIRC socket (localhost:8765)
// 
//  Parameters  : 
//  Returns     : Not at all, killed by the main thread
// 
// 
static void rcu_ReceiveIrKeys()
{
   sigset_t tBlockset;
   int      iFd, iCc, iFpKey;
   char    *pcCode=NULL;
   char    *pcConfig=NULL;

   LOG_Report(0, "RCU", "rcu_ReceiveIrKeys():Init Start....");
   if( rcu_SignalRegister(&tBlockset) )
   {
      PRINTF("rcu_ReceiveIrKeys(): This is the LIRC thread running....." CRLF);
      //
      // Setup RCU IR
      //
      if( (iFd = lirc_init("spicam", 1)) == -1)
      {
         //
         // Problems setting up LIRC
         //
         LOG_Report(errno, "RCU", "rcu_ReceiveIrKeys(): Problems opening LIRC");
         PRINTF1("rcu_ReceiveIrKeys(): Problems opening LIRC (%d)" CRLF, errno);
      }
      else
      {
         if(lirc_readconfig(NULL, &pstRcuConfig, rcu_LircCheck) != 0)
         {
            LOG_Report(errno, "RCU", "rcu_ReceiveIrKeys(): Problems opening RCU Config");
            PRINTF1("rcu_ReceiveIrKeys(): Problems opening RCU Config (%d)" CRLF, errno);
         }
         else
         {
            PRINTF1("rcu_ReceiveIrKeys(): Lirc cfg: mode=%s" CRLF, pstRcuConfig->current_mode);
            //
            // Signal strtup completion to host
            //
            GLOBAL_PidSaveGuard(PID_RCUD, GLOBAL_RCU_INI);
            GLOBAL_HostNotification(GLOBAL_RCU_INI);
            LOG_Report(0, "RCU", "rcu_ReceiveIrKeys():Init Ready");
            //
            while(fLircRunning)
            {
               //
               // This is the main RCU IR loop, reading IR keys from the LIRC socket
               //
               iCc = lirc_nextcode(&pcCode);
               if(iCc == 0)
               {
                  if(pcCode)
                  {
                     PRINTF1("rcu_ReceiveIrKeys(): LIRC=%s", pcCode);
                     do
                     {
                        if( ((iCc = lirc_code2char(pstRcuConfig, pcCode, &pcConfig)) == 0) && pcConfig != NULL)
                        {
                           //
                           // The RCU Lirc key has been defined in the /etc/lirc/lircrc configfile
                           //
                           iFpKey = rcu_LookupRcuKey(pcConfig);
                           //
                           rcu_SendSignalToHost(iFpKey);
                           PRINTF2("rcu_ReceiveIrKeys(): CONF=[%s]=%d" CRLF, pcConfig, iFpKey);
                        }
                     }
                     while(pcConfig);
                     free(pcCode);
                  }
                  else
                  {
                     LOG_Report(errno, "RCU", "rcu_ReceiveIrKeys(): Problems reading LIRC");
                     PRINTF1("rcu_ReceiveIrKeys(): Problems reading LIRC (%d)" CRLF, errno);
                  }
               }
               else
               {
                  //LOG_Report(errno, "RCU", "rcu_ReceiveIrKeys(): Problems reading LIRC nextcode");
                  PRINTF1("rcu_ReceiveIrKeys(): Problems reading LIRC nextcode (%d)" CRLF, errno);
               }
            }
         }
      }
   }
}

//
//  Function:   rcu_ReceiveSignalTerm
//  Purpose:    Add the SIGTERM command in the buffer (Ctl-X)
//
//  Parms:
//  Returns:    TRUE if delivered
//
static void rcu_ReceiveSignalTerm(int iSignal)
{
   LOG_Report(0, "RCU", "rcu_ReceiveSignalTerm()");
   PRINTF1("rcu_ReceiveSignalTerm(): signal=%d" CRLF, iSignal);
   rcu_Exit();
}

//
//  Function:   rcu_ReceiveSignalInt
//  Purpose:    Add the SIGINT command in the buffer (Ctl-C)
//
//  Parms:
//  Returns:    TRUE if delivered
//
static void rcu_ReceiveSignalInt(int iSignal)
{
   LOG_Report(0, "RCU", "rcu_ReceiveSignalInt()");
   PRINTF1("rcu_ReceiveSignalInt(): signal=%d" CRLF, iSignal);
   rcu_Exit();
}

// 
//  Function    : rcu_SendSignalToHost
//  Description : Signal the Host
// 
//  Parameters  : Data
//  Returns     : 
// 
//  Note        : Uses MMAP shared memory
// 
static void rcu_SendSignalToHost(int iData)
{
   if(GLOBAL_KeyPut(iData))
   {
      LOG_Report(0, "BTN", "rcu_SendSignalToHost():Key=%d", iData);
      //
      // Notify the host we have a RCU IR key
      //
      PRINTF1("rcu_SendSignalToHost(): Data=%d" CRLF, iData);
      GLOBAL_Notify(PID_HOST, GLOBAL_BTN_NFY, SIGUSR1);
   }
   else
   {
      LOG_Report(0, "BTN", "rcu_SendSignalToHost():Key=%d:BUFFER IS FULL!!", iData);
   }
}

//
//  Function:   rcu_SignalRegister
//  Purpose:    Register SIGxxx
//
//  Parms:      Blockset
//  Returns:    TRUE if delivered
//
static bool rcu_SignalRegister(sigset_t *ptBlockset)
{
   bool fCC = TRUE;

   // CTL-X handler
   if( signal(SIGTERM, &rcu_ReceiveSignalTerm) == SIG_ERR)
   {
      LOG_Report(errno, "RCU", "rcu_SignalRegister(): SIGTERM Error");
      fCC = FALSE;
   }
   // Ctl-C handler
   if( signal(SIGINT, &rcu_ReceiveSignalInt) == SIG_ERR)
   {
      LOG_Report(errno, "RCU", "rcu_SignalRegister(): SIGINT Error");
      fCC = FALSE;
   }
   if(fCC)
   {
      //
      // Setup the SIGxxx events
      //
      sigemptyset(ptBlockset);
      sigaddset(ptBlockset, SIGTERM);
      sigaddset(ptBlockset, SIGINT);
   }
   return(fCC);
}



