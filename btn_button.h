/*  (c) Copyright:  2013..2018  Patrn.nl, Confidential Data
 *
 *  Workfile:           btn_button.h
 *  Revision:   
 *  Modtime:    
 *
 *  Purpose:            btn_button header file
 *
 *
 *
 *  Compiler/Assembler: Raspbian Linux GNU gcc
 *  Ext Packages:
 *
 *  Author:             Peter Hillen
 *  Date Created:       28 Dec 2013
 * 
 *  Revisions:
 *
 *
 * 
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
**/

#ifndef _BTN_BUTTON_H_
#define _BTN_BUTTON_H_

#define MAX_BUTTON_MODES               8
#define BUTTON_MODE_MASK               0x0007
//
typedef void (*PFINT)(int);
// 
typedef struct BUTMODE
{
   u_int32     ulPattern;              // Mode LED blink pattern (No LCD Screen)
   const char *pcCommand;              // Mode text (LCD Screen)
   PFINT       pfMode;                 // Command execution
}  BUTMODE;
//
typedef struct BTNDEF
{
   u_int8      ubId;                   // Actual button  BTN_x
   u_int8      ubKey;                  // Button Key     BTN_KEYx
   u_int32     ulDebounce;             // 
}  BTNDEF;

#define  BUTTON_DEBOUNCE_100  0x00000001
#define  BUTTON_DEBOUNCE_200  0x00000003
#define  BUTTON_DEBOUNCE_300  0x00000007
#define  BUTTON_DEBOUNCE_400  0x0000000F
#define  BUTTON_DEBOUNCE_500  0x0000001F
#define  BUTTON_DEBOUNCE_1000 0x000003FF
#define  BUTTON_DEBOUNCE_1500 0x00007FFF
#define  BUTTON_DEBOUNCE_2000 0x000FFFFF
#define  BUTTON_DEBOUNCE_3200 0xFFFFFFFF
//
// Global prototypes
//
int      BTN_Init                (void);
bool     BTN_InitButtons         (void);
//
u_int8   BTN_LookupKeyAction     (u_int8);
u_int8   BTN_GetButtonKey        (int);
int      BTN_GetButtonStatus     (int);
int      BTN_GetButtonIndex      (u_int8);
bool     BTN_RegisterButton      (u_int8, PFKEY);
u_int8   BTN_ScanButtons         (void);
void     BTN_ShowButtonSetup     (const char *);

#endif  /*_BTN_BUTTON_H_ */

