/*  (c) Copyright:  2017  Patrn ESS, Confidential Data 
 *
 *  Workfile:           cam_json.c
 *  Revision:          
 *  Modtime:           
 *
 *  Purpose:            Raspberry pi dynamic JSON page command handlers
 *
 *
 *
 *  Compiler/Assembler: Raspbian Linux GNU gcc
 *  Ext Packages:
 *
 *  Author:             Peter Hillen
 *  Date Created:       17 Mar 2017
 *
 *  Revisions:
 *  Entry Points:       
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
**/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdarg.h>
#include <stddef.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>
#include <sched.h>
#include <time.h>
#include <errno.h>

#include <common.h>
#include "config.h"
#include "globals.h"
#include "rpi_func.h"
#include "cmd_main.h"
#include "cam_main.h"
#include "cam_page.h"
#include "gen_http.h"
#include "gen_func.h"
#include "graphics.h"
//
#include "cam_json.h"

//#define USE_PRINTF
#include <printf.h>

//
// Camera parameter list (as defined in cam.defs). Extract what you need here:
//
// iFunction:        Define if a parameter affects certain camera function
//                      JSN_TXT : For JSON API: parameter is formatted as JSON text    ("value" = "xxxxx")
//                      JSN_INT :               parameter is formatted as JSON number  ("value" = xxxxx)
//
//                      CAM_STI : Paramater concerns Snapshot fotos
//                      CAM_TIM :                    Timelapse fotos
//                      CAM_REC :                    Recordings
//                      CAM_STR :                    Streaming video
//                      CAM_PIX :                    Picture file retrieval
//                      CAM_VID :                    Video file retrieval
//                      CAM_PRM :                    Parameter definition
//                      CAM_STP :                    Stopping command-in-progress
//                      CAM_ALL :                    All HTTP commands
//                      CAM____ :                    None of the commands
//
// pcJson:           JSON API parameter name
// pcParm:           HTML API parameter name
// pcOption:         Raspberry Pi command option
// iValueOffset:     Global mmap structure offset of the parameter
// iValueSize:       Global mmap parameter array size
// iChangedOffset:   Global mmap structure offset of the parameter has changed-marker
//
// Local prototypes
//
#ifdef FEATURE_SHOW_MOTION
static void    cam_ShowMotionMatrix          (MDET *);
 #define  SHOW_MOTION(x)                     cam_ShowMotionMatrix(x)
#else
 #define  SHOW_MOTION(x)
#endif   //FEATURE_LIST_MOTION
//
static bool    cam_MotionNormalize           (void);
static bool    cam_MotionDelta               (void);
static bool    cam_MotionZoom                (void);
static void    cam_MotionDetectDefaults      (VARRESET);
//
static bool    cam_CallbackJsonAlarm         (GLOCMD *);
static bool    cam_CallbackJsonGeneric       (GLOCMD *);
static bool    cam_CallbackJsonMotionDetect  (GLOCMD *);
static bool    cam_CallbackJsonSnapshot      (GLOCMD *);
//
// URL command handler functions
//
static bool    cam_JsonAlarm                 (GLOCMD *);
static bool    cam_JsonDefaults              (GLOCMD *);
static bool    cam_JsonDeleteFotos           (GLOCMD *);
static bool    cam_JsonDeleteVideos          (GLOCMD *);
static bool    cam_JsonFotos                 (GLOCMD *);
static bool    cam_JsonMotionDetect          (GLOCMD *);
static bool    cam_JsonMotionParms           (GLOCMD *);
static bool    cam_JsonMotionSnap            (GLOCMD *);
static bool    cam_JsonMotionNormalize       (GLOCMD *);
static bool    cam_JsonMotionDelta           (GLOCMD *);
static bool    cam_JsonMotionZoom            (GLOCMD *);
static bool    cam_JsonMotionVector          (GLOCMD *);
static bool    cam_JsonParms                 (GLOCMD *);
static bool    cam_JsonRecord                (GLOCMD *);
static bool    cam_JsonSnapshot              (GLOCMD *);
static bool    cam_JsonStop                  (GLOCMD *);
static bool    cam_JsonStream                (GLOCMD *);
static bool    cam_JsonTimelapse             (GLOCMD *);
static bool    cam_JsonVideos                (GLOCMD *);

//
// Supported dynamic JSON functions
//
static const CAMPAGE stJsonCommands[] =
{  
//    iCommand             iFunction   pcCommand         pfCmdCb
   {  CAM_CMD_PARMS,       CAM_PRM,    "parms",          cam_JsonParms          },     // On top to show correct parms object
   //
   {  CAM_CMD_ALM,         CAM_ALM,    "alarm",          cam_JsonAlarm          },
   {  CAM_CMD_DEFAULTS,    CAM_DEF,    "defaults",       cam_JsonDefaults       },
   {  CAM_CMD_DEL_FOTOS,   CAM_RMP,    "rm-fotos",       cam_JsonDeleteFotos    },
   {  CAM_CMD_DEL_VIDEOS,  CAM_RMV,    "rm-videos",      cam_JsonDeleteVideos   },
   {  CAM_CMD_FOTOS,       CAM_PIX,    "fotos",          cam_JsonFotos          },     //Depricated: bypass through fotos.json
   {  CAM_CMD_MAN_RECORD,  CAM_REC,    "record",         cam_JsonRecord         },
   {  CAM_CMD_MAN_SNAP,    CAM_STI,    "snapshot",       cam_JsonSnapshot       },
   {  CAM_CMD_MAN_STOP,    CAM_STP,    "stop",           cam_JsonStop           },
   {  CAM_CMD_M_DELTA,     CAM_VEC,    "mdelta",         cam_JsonMotionDelta    },
   {  CAM_CMD_M_DETECT,    CAM_VEC,    "mdetect",        cam_JsonMotionDetect   },
   {  CAM_CMD_M_NORM,      CAM_VEC,    "mnorm",          cam_JsonMotionNormalize},
   {  CAM_CMD_M_PARMS,     CAM_VEC,    "mparms",         cam_JsonMotionParms    },
   {  CAM_CMD_M_SNAP,      CAM_VEC,    "msnap",          cam_JsonMotionSnap     },
   {  CAM_CMD_M_VECTOR,    CAM_VEC,    "mvector",        cam_JsonMotionVector   },
   {  CAM_CMD_M_ZOOM,      CAM_VEC,    "mzoom",          cam_JsonMotionZoom     },
   {  CAM_CMD_STREAM,      CAM_STR,    "stream",         cam_JsonStream         },
   {  CAM_CMD_TIMELAPSE,   CAM_TIM,    "timelapse",      cam_JsonTimelapse      },
   {  CAM_CMD_VIDEOS,      CAM_VID,    "videos",         cam_JsonVideos         },     //Depricated: bypass through videos.json
   {  0,                   0,          0,                0                      }
};
static const char *pcRaspiSeq       =  "_%04d";
//
// Depricated URL calls
//
static const char *pcDepricatedAlarm   = "alarm.json";
static const char *pcDepricatedDefault = "info.json";
static const char *pcDepricatedFotos   = "fotos.json";
static const char *pcDepricatedVideos  = "videos.json";
//
// Motion detect definitions
//
static const u_int16 usMotionDetectDefaults[MOTION_GRID_COUNT][MOTION_GRID_COUNT] =
                     {  //  1    2    3    4    5    6    7    8    9   10
                        {  100, 100, 100, 100, 100, 100, 100, 100, 100, 100 },    // 1
                        {  100, 100, 100, 100, 100, 100, 100, 100, 100, 100 },    // 2
                        {  100, 100, 100, 100, 100, 100, 100, 100, 100, 100 },    // 3
                        {  100, 100, 100, 100, 100, 100, 100, 100, 100, 100 },    // 4
                        {  100, 100, 100, 100, 100, 100, 100, 100, 100, 100 },    // 5
                        {  100, 100, 100, 100, 100, 100, 100, 100, 100, 100 },    // 6
                        {  100, 100, 100, 100, 100, 100, 100, 100, 100, 100 },    // 7
                        {  100, 100, 100, 100, 100, 100, 100, 100, 100, 100 },    // 8
                        {  100, 100, 100, 100, 100, 100, 100, 100, 100, 100 },    // 9
                        {  100, 100, 100, 100, 100, 100, 100, 100, 100, 100 }     // 10
                     };

/*------  Local functions separator ------------------------------------
__GLOBAL_FUNCTIONS(){};
----------------------------------------------------------------------------*/

//
// Function:   CAM_JsonGetCommands
// Purpose:    Return the list of supported dynamic HTTP JSON commands
//
// Parms:      
// Returns:    The list
//
const CAMPAGE *CAM_JsonGetCommands()
{
   return(stJsonCommands);
}

//
// Function:   CAM_JsonInit
// Purpose:    Init dynamic HTTP JSON commands
//
// Parms:      
// Returns:    
//
void CAM_JsonInit()
{
   cam_MotionDetectDefaults(VAR_COLD);
}


/*------  Local functions separator ------------------------------------
__JSON_PAGES(){};
----------------------------------------------------------------------------*/

//
// Function:   cam_JsonAlarm
// Purpose:    
//
// Parms:      Global Command struct^
// Returns:    CC
// Note:       if CC==TRUE Proceed to signal PID_CMND
//             else notify the HTTP server
//
static bool cam_JsonAlarm(GLOCMD *pstCmd)
{
   bool     fCc=FALSE;
   int      iIdx;

   switch(pstCmd->tUrl)
   {
      case CAM_DYN_JSON_CAM:
         PRINTF("cam-JsonAlarm()" CRLF);
         if(GEN_STRLEN(pstMap->G_pcAlarmCommand))
         {
            //
            // alarm set/delete: let CMD thread handle it
            //
            pstCmd->pfCallback = cam_CallbackJsonAlarm;
            fCc = TRUE;
         }
         else
         {
            //
            // Alarm parameters:
            // Deprecated  cam.json?command=alarm
            // trigger to: alarm.json
            //
            iIdx = CAM_FindDynamicPage(pcDepricatedAlarm);
            if(iIdx != -1)
            {
               PRINTF1("cam-JsonAlarm():Redirect to i=%d" CRLF, iIdx);
               pstMap->G_iCurDynPage = iIdx;
               pstCmd->iError        = CAM_STATUS_REDIRECT;
            }
            else
            {
               pstCmd->iError = CAM_STATUS_ERROR;
               pstCmd->iArgs |= CAM_ERR;
            }
         }
         break;

      default:
         pstCmd->iError = CAM_STATUS_ERROR;
         pstCmd->iArgs |= CAM_ERR;
         break;
   }
   return(fCc);
}

//
// Function:   cam_JsonDefaults
// Purpose:    
//
// Parms:      Global Command struct^
// Returns:    CC
// Note:       if CC==TRUE Proceed to signal PID_CMND
//             else notify the HTTP server
//
static bool cam_JsonDefaults(GLOCMD *pstCmd)
{
   int      iIdx;

   switch(pstCmd->tUrl)
   {
      case CAM_DYN_JSON_CAM:
         PRINTF("cam-JsonDefaults()" CRLF);
         //
         // Deprecated  cam.json?command=info
         // trigger to: info.json
         //
         iIdx = CAM_FindDynamicPage(pcDepricatedDefault);
         if(iIdx != -1)
         {
            PRINTF1("cam-JsonDefaults():Redirect to i=%d" CRLF, iIdx);
            pstMap->G_iCurDynPage = iIdx;
            pstCmd->iError        = CAM_STATUS_REDIRECT;
         }
         break;

      default:
         pstCmd->iError = CAM_STATUS_ERROR;
         pstCmd->iArgs |= CAM_ERR;
         break;
   }
   return(FALSE);
}

//
// Function:   cam_JsonDeleteFotos
// Purpose:    
//
// Parms:      Global Command struct^
// Returns:    CC
// Note:       if CC==TRUE Proceed to signal PID_CMND
//             else notify the HTTP server
//
static bool cam_JsonDeleteFotos(GLOCMD *pstCmd)
{
   bool        fCc=FALSE;
   char       *pcShell;
   const char *pcDeleteExec = "rm";

   switch(pstCmd->tUrl)
   {
      case CAM_DYN_JSON_CAM:
         if(GEN_CheckDelete())
         {
            pcShell = (char *) safemalloc(MAX_CMD_LEN);
            GEN_SPRINTF(pcShell, "%s %s%s%s*", pcDeleteExec, pstMap->G_pcWwwDir, pstMap->G_pcPixPath, pstMap->G_pcLastFile);
            //
            // Run execution
            //
            LOG_Report(0, "CAM", "cam-JsonDeleteFotos(): system(%s)", pcShell);
            PRINTF1("cam-JsonDeleteFotos():System(%s)" CRLF, pcShell);
            system(pcShell);
            safefree(pcShell);
            fCc = cam_JsonFotos(pstCmd);
         }
         break;

      default:
         pstCmd->iError = CAM_STATUS_ERROR;
         pstCmd->iArgs |= CAM_ERR;
         break;
   }
   return(fCc);
}

//
// Function:   cam_JsonDeleteVideos
// Purpose:    
//
// Parms:      Global Command struct^
// Returns:    CC
// Note:       if CC==TRUE Proceed to signal PID_CMND
//             else notify the HTTP server
//
static bool cam_JsonDeleteVideos(GLOCMD *pstCmd)
{
   bool        fCc=FALSE;
   char       *pcShell;
   const char *pcDeleteExec = "rm";

   switch(pstCmd->tUrl)
   {
      case CAM_DYN_JSON_CAM:
         if(GEN_CheckDelete())
         {
            pcShell = (char *) safemalloc(MAX_CMD_LEN);
            GEN_SPRINTF(pcShell, "%s %s%s/%s*", pcDeleteExec, pstMap->G_pcWwwDir, pstMap->G_pcVideoPath, pstMap->G_pcLastFile);
            //
            // Run execution
            //
            LOG_Report(0, "CAM", "cam-JsonDeleteVideos(): system(%s)", pcShell);
            PRINTF1("cam-JsonDeleteVideos(): system(%s)" CRLF, pcShell);
            system(pcShell);
            safefree(pcShell);
            fCc = cam_JsonVideos(pstCmd);
         }
         break;

      default:
         pstCmd->iError = CAM_STATUS_ERROR;
         pstCmd->iArgs |= CAM_ERR;
         break;
   }
   return(fCc);
}

//
// Function:   cam_JsonFotos
// Purpose:    
//
// Parms:      Global Command struct^
// Returns:    CC
// Note:       if CC==TRUE Proceed to signal PID_CMND
//             else notify the HTTP server
//
static bool cam_JsonFotos(GLOCMD *pstCmd)
{
   int      iIdx;

   switch(pstCmd->tUrl)
   {
      case CAM_DYN_JSON_CAM:
         PRINTF("cam-JsonFotos()" CRLF);
         //
         // Deprecated  cam.json?command=fotos
         // trigger to: fotos.json
         //
         iIdx = CAM_FindDynamicPage(pcDepricatedFotos);
         if(iIdx != -1)
         {
            PRINTF1("cam-JsonFotos():Redirect to i=%d" CRLF, iIdx);
            pstMap->G_iCurDynPage = iIdx;
            pstCmd->iError        = CAM_STATUS_REDIRECT;
         }
         else
         {
            pstCmd->iError = CAM_STATUS_ERROR;
            pstCmd->iArgs |= CAM_ERR;
         }
         break;

      default:
         pstCmd->iError = CAM_STATUS_ERROR;
         pstCmd->iArgs |= CAM_ERR;
         break;
   }
   return(FALSE);
}

//
// Function:   cam_JsonMotionDetect
// Purpose:    
//
// Parms:      Global Command struct^
// Returns:    CC
// Note:       if CC==TRUE Proceed to signal PID_CMND
//             else notify the HTTP server
//
static bool cam_JsonMotionDetect(GLOCMD *pstCmd)
{
   bool     fCc=FALSE;

   switch(pstCmd->tUrl)
   {
      case CAM_DYN_JSON_CAM:
         cam_MotionDetectDefaults(VAR_WARM);
         //
         // Toggle motion picture sequence number to make a new picture
         //
         pstMap->G_stMotionDetect.iSeq ^= 1;
         //
         // Build default output filename
         //
         GEN_SPRINTF(pstMap->G_pcLastFile, "%smotion_%d.%s", pstMap->G_pcRamDir, pstMap->G_stMotionDetect.iSeq, pstMap->G_pcPixFileExt);
         //
         // Collect command and parameters
         //
         if(pstMap->G_fFakeExec) GEN_SPRINTF(pstCmd->pcExec, RPI_PIX_EXEC_DUMMY);
         else                    GEN_SPRINTF(pstCmd->pcExec, RPI_PIX_EXEC);
         //
         GLOBAL_InsertOptions(pstCmd->pcArgs, MAX_ARG_LEN, CAM_STI);
         LOG_Report(0, "CAM", "JSON motion detect [%s]:%s", pstMap->G_pcLastFile, pstCmd->pcArgs);
         PRINTF2("cam-JsonMotionDetect():[%s]:%s" CRLF, pstMap->G_pcLastFile, pstCmd->pcArgs);
         //
         // Spawn execution to CMND thread:
         //
         pstCmd->pfCallback = cam_CallbackJsonMotionDetect;
         PRINTF("cam-JsonMotionDetect():Done" CRLF);
         fCc = TRUE;
         break;

      default:
         pstCmd->iError = CAM_STATUS_ERROR;
         pstCmd->iArgs |= CAM_ERR;
         break;
   }
   return(fCc);
}

//
// Function:   cam_JsonMotionParms
// Purpose:    Execute HTTP request: 
//             CAM_CMD_M_PARMS, CAM_VEC, "mparms"
// Parms:      Global Command struct^
// Returns:    CC
// Note:       if CC==TRUE Proceed to signal PID_CMND
//             else notify the HTTP server
//
static bool cam_JsonMotionParms(GLOCMD *pstCmd)
{

   PRINTF2("cam-JsonMotionParms():MM=%s, Count=%s (%d%%)" CRLF, pstMap->G_pcDetectMode, pstMap->G_pcDetectCount); 
   //
   switch(pstCmd->tUrl)
   {
      case CAM_DYN_JSON_CAM:
         //
         // Select correct mode (Auto, Manual)
         //
         cam_MotionDetectDefaults(VAR_WARM);
         break;

      default:
         pstCmd->iError = CAM_STATUS_ERROR;
         pstCmd->iArgs |= CAM_ERR;
         break;
   }
   return(FALSE);
}

//
// Function:   cam_JsonMotionSnap
// Purpose:    Handle the JSON Command=msnap
//
// Parms:      Global Command struct^
// Returns:    CC
// Note:       if CC==TRUE Proceed to signal PID_CMND
//             else notify the HTTP server
//
static bool cam_JsonMotionSnap(GLOCMD *pstCmd)
{
   bool     fCc=FALSE;
   MDET    *pstMdet=&(pstMap->G_stMotionDetect);

   switch(pstCmd->tUrl)
   {
      case CAM_DYN_JSON_CAM:
         cam_MotionDetectDefaults(VAR_WARM);
         //
         // Parse the header for parameters: store them in the G_ parameter array.
         // Take a snapshot now.
         //
         switch(pstMdet->iMotMode)
         {
            case MOTION_MODE_CYCLE:
            case MOTION_MODE_AUGMENT:
            case MOTION_MODE_PICTURE:
            case MOTION_MODE_MANUAL:
            case MOTION_MODE_VIDEO:
               //
               // We are in a motion detect mode:
               //    motion_x.* (0/1) has the latest captured motion. Outline (Augment) the motion to
               //    motion_A.*
               // Build foto output filename RPI_RAMFS_DIR/motion_?.ext
               //
               GEN_SPRINTF(pstMap->G_pcLastFile, "%smotion_%d.%s", pstMap->G_pcRamDir, pstMap->G_stMotionDetect.iSeq, pstMap->G_pcPixFileExt);
               //
               // Collect command and parameters
               //
               if(pstMap->G_fFakeExec) GEN_SPRINTF(pstCmd->pcExec, RPI_PIX_EXEC_DUMMY);
               else                    GEN_SPRINTF(pstCmd->pcExec, RPI_PIX_EXEC);
               //
               GLOBAL_InsertOptions(pstCmd->pcArgs, MAX_ARG_LEN, CAM_STI);
               LOG_Report(0, "CAM", "JSON motion snapshot [%s]:%s", pstMap->G_pcLastFile, pstCmd->pcArgs);
               PRINTF2("cam-JsonMotionSnap():[%s]:%s" CRLF, pstMap->G_pcLastFile, pstCmd->pcArgs);
               //
               // Spawn execution to CMND thread:
               //
               pstCmd->pfCallback = cam_CallbackJsonSnapshot;
               fCc = TRUE;
               break;

            default:
            case MOTION_MODE_OFF:
            case MOTION_MODE_STREAM:
               break;
         }
         break;

      default:
         pstCmd->iError = CAM_STATUS_ERROR;
         pstCmd->iArgs |= CAM_ERR;
         break;
   }
   return(fCc);
}

//
// Function:   cam_JsonMotionNormalize
// Purpose:    
//
// Parms:      Global Command struct^
// Returns:    CC
// Note:       if CC==TRUE Proceed to signal PID_CMND
//             else notify the HTTP server
//
static bool cam_JsonMotionNormalize(GLOCMD *pstCmd)
{
   bool     fCc=FALSE;

   switch(pstCmd->tUrl)
   {
      case CAM_DYN_JSON_CAM:
         cam_MotionDetectDefaults(VAR_WARM);
         //
         if( cam_MotionNormalize() )
         {
            //
            // Build foto output filename RPI_RAMFS_DIR/motion_?_norm.ext
            //
            GEN_SPRINTF(pstMap->G_pcLastFile, "%smotion_%d_norm.%s", pstMap->G_pcRamDir, pstMap->G_stMotionDetect.iSeq, pstMap->G_pcPixFileExt);
            FINFO_GetFileInfo(pstMap->G_pcLastFile, FI_SIZE, pstMap->G_pcLastFileSize, MAX_PATH_LEN);
            pstCmd->pfCallback = cam_CallbackJsonGeneric;
            fCc = TRUE;
         }
         else
         {
            PRINTF("cam-JsonMotionNormalize(): Error" CRLF);
            LOG_Report(0, "CAM", "Motion Normalize error");
            pstCmd->iError = CAM_STATUS_ERROR;
            pstCmd->iArgs |= CAM_ERR;
         }
         break;

      default:
         pstCmd->iError = CAM_STATUS_ERROR;
         pstCmd->iArgs |= CAM_ERR;
         break;
   }
   return(fCc);
}

//
// Function:   cam_JsonMotionDelta
// Purpose:    
//
// Parms:      Global Command struct^
// Returns:    CC
// Note:       if CC==TRUE Proceed to signal PID_CMND
//             else notify the HTTP server
//
static bool cam_JsonMotionDelta(GLOCMD *pstCmd)
{
   bool     fCc=FALSE;
   
   switch(pstCmd->tUrl)
   {
      case CAM_DYN_JSON_CAM:
         cam_MotionDetectDefaults(VAR_WARM);
         //
         if( cam_MotionDelta() )
         {
            //
            // Build foto output filename RPI_RAMFS_DIR/motion_delta.ext
            //
            pstCmd->pfCallback = cam_CallbackJsonGeneric;
            fCc = TRUE;
         }
         else
         {
            PRINTF("cam-JsonMotionDelta(): Error" CRLF);
            LOG_Report(0, "CAM", "Motion Delta error");
            pstCmd->iError = CAM_STATUS_ERROR;
            pstCmd->iArgs |= CAM_ERR;
         }
         break;

      default:
         pstCmd->iError = CAM_STATUS_ERROR;
         pstCmd->iArgs |= CAM_ERR;
         break;
   }
   return(fCc);
}

//
// Function:   cam_JsonMotionZoom
// Purpose:    
//
// Parms:      Global Command struct^
// Returns:    CC
// Note:       if CC==TRUE Proceed to signal PID_CMND
//             else notify the HTTP server
//
static bool cam_JsonMotionZoom(GLOCMD *pstCmd)
{
   bool     fCc=FALSE;
   
   switch(pstCmd->tUrl)
   {
      case CAM_DYN_JSON_CAM:
      cam_MotionDetectDefaults(VAR_WARM);
      //
      if( cam_MotionZoom() )
      {
         //
         // Build foto output filename RPI_RAMFS_DIR/motion_zoom.ext
         //
         pstCmd->pfCallback = cam_CallbackJsonGeneric;
         fCc = TRUE;
      }
      else
      {
         PRINTF("cam-JsonMotionZoom(): Error" CRLF);
         LOG_Report(0, "CAM", "Motion Zoom error");
         pstCmd->iError = CAM_STATUS_ERROR;
         pstCmd->iArgs |= CAM_ERR;
      }
      break;

      default:
         pstCmd->iError = CAM_STATUS_ERROR;
         pstCmd->iArgs |= CAM_ERR;
         break;
   }
   return(fCc);
}

//
// Function:   cam_JsonMotionVector
// Purpose:    
//
// Parms:      Global Command struct^
// Returns:    CC
// Note:       if CC==TRUE Proceed to signal PID_CMND
//             else notify the HTTP server
//
static bool cam_JsonMotionVector(GLOCMD *pstCmd)
{
   bool     fCc=FALSE;

   switch(pstCmd->tUrl)
   {
      case CAM_DYN_JSON_CAM:
         //
         // Select correct mode (Auto, Manual)
         //
         cam_MotionDetectDefaults(VAR_WARM);
         //
         // For motion vector parsing, we do not need Video output !!
         //
         pstMap->G_pcLastFile[0]       = '\0';
         pstMap->G_stMotionDetect.iSeq = 0;
         //
         // Collect command and parameters
         //
         GEN_SPRINTF(pstCmd->pcExec, RPI_VID_EXEC);
         GLOBAL_InsertOptions(pstCmd->pcArgs, MAX_ARG_LEN, pstCmd->iArgs);
         LOG_Report(0, "CAM", "Cmd Request Motion Vector parsing (%s %s)", pstCmd->pcExec, pstCmd->pcArgs);
         PRINTF2("cam-JsonMotionVector():[%s]:%s" CRLF, pstMap->G_pcLastFile, pstCmd->pcArgs);
         //
         // Spawn execution to CMND thread:
         //
         pstMap->G_ulRecordingStart = GLOBAL_ReadSecs();
         pstCmd->pfCallback = cam_CallbackJsonGeneric;
         fCc = TRUE;
         break;

      default:
         pstCmd->iError = CAM_STATUS_ERROR;
         pstCmd->iArgs |= CAM_ERR;
         break;
   }
   return(fCc);
}

//
// Function:   cam_JsonParms
// Purpose:    
//
// Parms:      Global Command struct^
// Returns:    CC
// Note:       if CC==TRUE Proceed to signal PID_CMND
//             else notify the HTTP server
//
static bool cam_JsonParms(GLOCMD *pstCmd)
{
   switch(pstCmd->tUrl)
   {
      case CAM_DYN_JSON_TIMELAPSE:
      case CAM_DYN_JSON_SNAPSHOT:
      case CAM_DYN_JSON_STREAM:
      case CAM_DYN_JSON_RECORD:
      case CAM_DYN_JSON_CAM:
         PRINTF("cam-JsonParms():Skip CMD Nfy!" CRLF);
         break;

      default:
         pstCmd->iError = CAM_STATUS_ERROR;
         pstCmd->iArgs |= CAM_ERR;
         break;
   }
   return(FALSE);
}

//
// Function:   cam_JsonRecord
// Purpose:    
//
// Parms:      Global Command struct^
// Returns:    CC
// Note:       if CC==TRUE Proceed to signal PID_CMND
//             else notify the HTTP server
//
static bool cam_JsonRecord(GLOCMD *pstCmd)
{
   bool     fCc=FALSE;
   u_int32  ulSecs;
   char    *pcFile;

   switch(pstCmd->tUrl)
   {
      case CAM_DYN_JSON_CAM:
         pcFile = (char *) safemalloc(MAX_PATH_LEN);
         //
         // Build video output filename "YYYYMMDD_HHMMSS"
         //
         ulSecs = RTC_GetSystemSecs();
         RTC_ConvertDateTime(TIME_FORMAT_YYYY_MM_DD_HH_MM_SS, pcFile, ulSecs);

         #ifdef FEATURE_SEGMENTED_RECORDING
         //
         // Add segmented filenaming -%04d
         //
         GEN_SPRINTF(pstMap->G_pcLastFile, "%s%s%s-%%04d.h264", pstMap->G_pcWwwDir, pstMap->G_pcVideoPath, pcFile);
         //
         // Collect command and parameters
         // Spawn the command and args to the CMND
         //
         GEN_SPRINTF(pstCmd->pcExec, RPI_VID_EXEC);
         //
         GLOBAL_SetParameterChanged(CAM_WRAP);
         GLOBAL_SetParameterChanged(CAM_SEGMENT);
         //
         GLOBAL_InsertOptions(pstCmd->pcArgs, MAX_ARG_LEN, CAM_REC);
         LOG_Report(0, "CAM", "Recording %s.h264 (%s %s)", pcFile, pstCmd->pcExec, pstCmd->pcArgs);
         PRINTF2("cam-JsonRecord():[%s]:%s" CRLF, pstMap->G_pcLastFile, pstCmd->pcArgs);
         //
         // Remove -%04d from reported filename
         //
         GEN_SPRINTF(pstMap->G_pcLastFile, "%s%s%s.h264", pstMap->G_pcWwwDir, pstMap->G_pcVideoPath, pcFile);
         
         #else    //FEATURE_SEGMENTED_RECORDING
         GEN_SPRINTF(pstMap->G_pcLastFile, "%s%s%s.h264", pstMap->G_pcWwwDir, pstMap->G_pcVideoPath, pcFile);
         GEN_SPRINTF(pstCmd->pcExec, RPI_VID_EXEC);
         GLOBAL_InsertOptions(pstCmd->pcArgs, MAX_ARG_LEN, CAM_REC);
         LOG_Report(0, "CAM", "Recording %s.h264 (%s %s)", pcFile, pstCmd->pcExec, pstCmd->pcArgs);
         PRINTF2("cam-JsonRecord():[%s]:%s" CRLF, pstMap->G_pcLastFile, pstCmd->pcArgs);
         
         #endif   //FEATURE_SEGMENTED_RECORDING
         
         pstMap->G_ulRecordingStart = GLOBAL_ReadSecs();
         pstCmd->pfCallback = cam_CallbackJsonGeneric;
         //
         safefree(pcFile);
         fCc = TRUE;
         break;

      default:
         pstCmd->iError = CAM_STATUS_ERROR;
         pstCmd->iArgs |= CAM_ERR;
         break;
   }
   return(fCc);
}

//
// Function:   cam_JsonSnapshot
// Purpose:    Take a snapshot using PID_CMND
//
// Parms:      Global Command struct^
// Returns:    CC
// Note:       if CC==TRUE Proceed to signal PID_CMND
//             else notify the HTTP server
//
static bool cam_JsonSnapshot(GLOCMD *pstCmd)
{
   bool     fCc=FALSE;
   u_int32  ulSecs;
   char    *pcFile;

   switch(pstCmd->tUrl)
   {
      case CAM_DYN_JSON_CAM:
         //
         // Build foto output filename "YYYYMMDD_HHMMSS"
         //
         pcFile = (char *) safemalloc(MAX_PATH_LEN);
         ulSecs = RTC_GetSystemSecs();
         RTC_ConvertDateTime(TIME_FORMAT_YYYY_MM_DD_HH_MM_SS, pcFile, ulSecs);
         GEN_SPRINTF(pstMap->G_pcLastFile, "%s%s%s.%s", pstMap->G_pcWwwDir, pstMap->G_pcPixPath, pcFile, pstMap->G_pcPixFileExt);
         //
         // Collect command and parameters
         //    Use the semaphore to synchronize CMND completion
         //    Spawn the command and args to the CMND
         //
         GEN_SPRINTF(pstCmd->pcExec, RPI_PIX_EXEC);
         GLOBAL_InsertOptions(pstCmd->pcArgs, MAX_ARG_LEN, CAM_STI);
         LOG_Report(0, "CAM", "JSON Snapshot [%s]:%s", pstMap->G_pcLastFile, pstCmd->pcArgs);
         PRINTF2("cam-JsonSnapshot():JSON snapshot [%s]:%s" CRLF, pstMap->G_pcLastFile, pstCmd->pcArgs);
         //
         // Spawn execution to CMND thread:
         //
         pstCmd->pfCallback = cam_CallbackJsonSnapshot;
         safefree(pcFile);
         PRINTF("cam-JsonSnapshot():Done" CRLF);
         fCc = TRUE;
         break;

      default:
         pstCmd->iError = CAM_STATUS_ERROR;
         pstCmd->iArgs |= CAM_ERR;
         break;
   }
   return(fCc);
}

//
// Function:   cam_JsonStop
// Purpose:    
//
// Parms:      Global Command struct^
// Returns:    CC
// Note:       if CC==TRUE Proceed to signal PID_CMND
//             else notify the HTTP server
//
static bool cam_JsonStop(GLOCMD *pstCmd)
{
   bool     fCc=FALSE;

   switch(pstCmd->tUrl)
   {
      case CAM_DYN_JSON_CAM:
      case CAM_DYN_JSON_RECORD:
      case CAM_DYN_JSON_STREAM:
         pstMap->G_ulRecordingStart = -1;
         pstCmd->iStatus    = CAM_STATUS_STOPPED;
         pstCmd->pfCallback = cam_CallbackJsonGeneric;
         fCc = TRUE;
         break;

      default:
         pstCmd->iError = CAM_STATUS_ERROR;
         pstCmd->iArgs |= CAM_ERR;
         break;
   }
   return(fCc);
}

//
// Function:   cam_JsonStream
// Purpose:    
//
// Parms:      Global Command struct^
// Returns:    CC
// Note:       if CC==TRUE Proceed to signal PID_CMND
//             else notify the HTTP server
//
static bool cam_JsonStream(GLOCMD *pstCmd)
{
   bool     fCc=FALSE;
   
   switch(pstCmd->tUrl)
   {
      case CAM_DYN_JSON_CAM:
         //
         // Set outputfile to life ("-o -")
         // Copy client IP address
         // Enable the demux option ":demux=h264"
         //
         strcpy(pstMap->G_pcLastFile, "-");
         strcpy(pstMap->G_pcStreamAddr, pstMap->G_pcIpAddr);
         //
         // Collect command and parameters
         //
         LOG_Report(0, "CAM", "Stream [ip=%s, port=%s]", pstMap->G_pcStreamAddr, pstMap->G_pcStreamPort);
         GEN_SPRINTF(pstCmd->pcExec, RPI_VID_EXEC);
         GLOBAL_InsertOptions(pstCmd->pcArgs, MAX_ARG_LEN, CAM_STR);
         PRINTF2("cam-JsonStream():[%s]:%s" CRLF, pstMap->G_pcLastFile, pstCmd->pcArgs);
         //
         pstMap->G_ulRecordingStart = GLOBAL_ReadSecs();
         pstCmd->pfCallback = cam_CallbackJsonGeneric;
         fCc = TRUE;
         break;

      default:
         pstCmd->iError = CAM_STATUS_ERROR;
         pstCmd->iArgs |= CAM_ERR;
         break;
   }
   return(fCc);
}

//
// Function:   cam_JsonTimelapse
// Purpose:    
//
// Parms:      Global Command struct^
// Returns:    CC
// Note:       if CC==TRUE Proceed to signal PID_CMND
//             else notify the HTTP server
//
static bool cam_JsonTimelapse(GLOCMD *pstCmd)
{
   bool     fCc=FALSE;
   u_int32  ulSecs;
   int      iNumPix, iDelayPix;
   char    *pcFile;

   switch(pstCmd->tUrl)
   {
      case CAM_DYN_JSON_CAM:
         pcFile = (char *) safemalloc(MAX_PATH_LEN);
         //
         // Make sure to enable the timelapse option
         //    G_pcTimNumber supplies the number of timelapse pix
         //     or
         //    G_TlTimeout supplies the amount of time
         //    
         if(pstMap->G_ubTimNumberChanged)
         {
            iNumPix    = atoi(pstMap->G_pcTimNumber);
            iDelayPix  = atoi(pstMap->G_pcTimelapse);
            PRINTF2("cam-JsonTimelapse(): Timelapse (%d pix each %d msecs)" CRLF, iNumPix, iDelayPix);
            iDelayPix *= iNumPix;
            if(iDelayPix)
            {
                //Overrule timelapse timeout
               GEN_SPRINTF(pstMap->G_pcTimDuration, "%d", iDelayPix);
            }
         }
         //
         // Build foto output filename "YYYYMMDD_HHMMSS"
         //
         ulSecs = RTC_GetSystemSecs();
         RTC_ConvertDateTime(TIME_FORMAT_YYYY_MM_DD_HH_MM_SS, pcFile, ulSecs);
         GEN_SPRINTF(pstMap->G_pcLastFile, "%s%s%s%s.%s", pstMap->G_pcWwwDir, pstMap->G_pcPixPath, pcFile, pcRaspiSeq, pstMap->G_pcPixFileExt);
         //
         // Collect command and parameters
         //
         GEN_SPRINTF(pstCmd->pcExec, RPI_PIX_EXEC);
         GLOBAL_InsertOptions(pstCmd->pcArgs, MAX_ARG_LEN, CAM_TIM);
         PRINTF2("cam-JsonTimelapse():[%s]:%s" CRLF, pstMap->G_pcLastFile, pstCmd->pcArgs);
         //
         pstCmd->pfCallback = cam_CallbackJsonSnapshot;
         GEN_SPRINTF(pstMap->G_pcLastFile, "%s%s%s%s.%s", pstMap->G_pcWwwDir, pstMap->G_pcPixPath, pcFile, pcRaspiSeq, pstMap->G_pcPixFileExt);
         safefree(pcFile);
         fCc = TRUE;
         break;

      default:
         pstCmd->iError = CAM_STATUS_ERROR;
         pstCmd->iArgs |= CAM_ERR;
         break;
   }
   return(fCc);
}

//
// Function:   cam_JsonVideos
// Purpose:    
//
// Parms:      Global Command struct^
// Returns:    CC
// Note:       if CC==TRUE Proceed to signal PID_CMND
//             else notify the HTTP server
//
static bool cam_JsonVideos(GLOCMD *pstCmd)
{
   int      iIdx;

   switch(pstCmd->tUrl)
   {
      case CAM_DYN_JSON_CAM:
         PRINTF("cam-JsonVideos()" CRLF);
         //
         // Deprecated  cam.json?command=videos
         // trigger to: videoss.json
         //
         iIdx = CAM_FindDynamicPage(pcDepricatedVideos);
         if(iIdx != -1)
         {
            pstMap->G_iCurDynPage = iIdx;
            pstCmd->iError        = CAM_STATUS_REDIRECT;
         }
         else
         {
            pstCmd->iError = CAM_STATUS_ERROR;
            pstCmd->iArgs |= CAM_ERR;
         }
         break;

      default:
         pstCmd->iError = CAM_STATUS_ERROR;
         pstCmd->iArgs |= CAM_ERR;
         break;
   }
   return(FALSE);
}

/*------  Local functions separator ------------------------------------
__MISC_FUNCTIONS(){};
----------------------------------------------------------------------------*/

//
// Function:   cam_MotionNormalize
// Purpose:    Normalize and do magic(k) on previous motion snapshots
//
// Parms:      
// Returns:    TRUE if OKee
// Note:       
//             
static bool cam_MotionNormalize()
{
   bool     fCc=FALSE;

   //
   // Motion detect:
   //    Take 2 snapshots
   //    Convert to grayscale
   //    Calculate delta (threshold)
   //    perform action on changes (threshold)
   //

   if( GRF_MotionNormalize() )
   {
      //
      // Normalise OK: pass average values
      //
      GEN_SPRINTF(pstMap->G_pcDetectAvg1, "%d", pstMap->G_stMotionDetect.iAverage1);
      GEN_SPRINTF(pstMap->G_pcDetectAvg2, "%d", pstMap->G_stMotionDetect.iAverage2);
      fCc = TRUE;
   }
   return(fCc);
}

//
// Function:   cam_MotionDelta
// Purpose:    Calculate and build delta of previous motion snapshots
//
// Parms:      
// Returns:    TRUE if OKee
// Note:       
//             
static bool cam_MotionDelta()
{
   bool     fCc=FALSE;
   
   //
   // Motion detect:
   //    Take 2 snapshots
   //    Convert to grayscale
   //    Calculate delta (threshold)
   //    perform action on changes (threshold)
   //
   if( GRF_MotionDelta() >= 0)
   {
      //
      // Delta OK: pass average values
      //
      GEN_SPRINTF(pstMap->G_pcDetectAvg1, "%d", pstMap->G_stMotionDetect.iAverage1);
      GEN_SPRINTF(pstMap->G_pcDetectAvg2, "%d", pstMap->G_stMotionDetect.iAverage2);
      fCc = TRUE;
   }
   return(fCc);
}

//
// Function:   cam_MotionZoom
// Purpose:    Zoom in/out
//
// Parms:      
// Returns:    TRUE if OKee
// Note:       
//             
static bool cam_MotionZoom()
{
   bool     fCc=FALSE;
   
   if( GRF_MotionZoom() >= 0)
   {
      //
      // Delta OK: pass average values
      //
      GEN_SPRINTF(pstMap->G_pcDetectAvg1, "%d", pstMap->G_stMotionDetect.iAverage1);
      GEN_SPRINTF(pstMap->G_pcDetectAvg2, "%d", pstMap->G_stMotionDetect.iAverage2);
      fCc = TRUE;
   }
   return(fCc);
}

//
// Function:   cam_MotionDetectDefaults
// Purpose:    Setup defaults for motion detect
//
// Parms:      Mode (WARM, COLD)
// Returns:    
// Note:       
//             
static void cam_MotionDetectDefaults(VARRESET tMode)
{
   int      iX, iY;
   MDET    *pstMdet=&pstMap->G_stMotionDetect;

   //
   // Always reset data
   //
   pstMdet->iAverage1 = 0;
   pstMdet->iAverage2 = 0;
   //
   switch(tMode)
   {
      default:
         RPI_SetMotionMode();
         break;

      case VAR_COLD:
         //
         // Cold reset
         //
         pstMdet->iCounter = 0;
         pstMdet->iDebug   = atoi(pstMap->G_pcDetectDebug);
         pstMdet->iSeq     = atoi(pstMap->G_pcDetectSeq);
         //
         for(iY=0; iY<MOTION_GRID_COUNT; iY++)
         {
            for(iX=0; iX<MOTION_GRID_COUNT; iX++)
            {
               pstMdet->usGridScales[iY][iX] = usMotionDetectDefaults[iY][iX];
            }
         }
         RPI_SetMotionMode();
         break;

      case VAR_UPDATE:
      case VAR_WARM:
         //
         // Warm reset
         //
         pstMdet->iDebug      = atoi(pstMap->G_pcDetectDebug);
         pstMdet->iTrigger1   = atoi(pstMap->G_pcDetectTrigger1);
         pstMdet->iTrigger2   = atoi(pstMap->G_pcDetectTrigger2);
         pstMdet->iSeq        = atoi(pstMap->G_pcDetectSeq);
         pstMdet->iZoomX      = atoi(pstMap->G_pcDetectZoomX);
         pstMdet->iZoomY      = atoi(pstMap->G_pcDetectZoomY);
         pstMdet->iZoomW      = atoi(pstMap->G_pcDetectZoomW);
         pstMdet->iZoomH      = atoi(pstMap->G_pcDetectZoomH);
         pstMdet->iThldPixel  = atoi(pstMap->G_pcDetectPixel);
         pstMdet->iThldLevel  = atoi(pstMap->G_pcDetectLevel);
         pstMdet->iThldRed    = atoi(pstMap->G_pcDetectRed);
         pstMdet->iThldGreen  = atoi(pstMap->G_pcDetectGreen);
         pstMdet->iThldBlue   = atoi(pstMap->G_pcDetectBlue);
         //
         pstMdet->iMvTiny     = atoi(pstMap->G_pcMvTiny);
         pstMdet->iMvSmall    = atoi(pstMap->G_pcMvSmall);
         pstMdet->iMvMedium   = atoi(pstMap->G_pcMvMedium);
         pstMdet->iMvLarge    = atoi(pstMap->G_pcMvLarge);
         pstMdet->iMvHuge     = atoi(pstMap->G_pcMvHuge);
         //
         GEN_STRNCPY(pstMdet->pcFormat, pstMap->G_pcDetectColor, MOTION_COLORS);
         pstMdet->pcFormat[MOTION_COLORS-1] = 0;
         //
         RPI_SetMotionMode();
         break;
   }
}

#ifdef FEATURE_SHOW_MOTION
//
// Function:   cam_ShowMotionMatrix
// Purpose:    Show the motion detect matrix
//
// Parms:      MDET matrix
// Returns:    
// Note:       
//             
static void cam_ShowMotionMatrix(MDET *pstMdet)
{
   int iX, iY;
   
   for(iY=0; iY<MOTION_GRID_COUNT; iY++)
   {
      for(iX=0; iX<MOTION_GRID_COUNT; iX++)
      {
         LOG_printf("%3ld ", pstMdet->ulGridMotion[iY][iX]);
      }
      LOG_printf(CRLF);
   }
}
#endif   //FEATURE_SHOW_MOTION

/*------  Local functions separator ------------------------------------
__CALLBACK_FUNCTIONS(){};
----------------------------------------------------------------------------*/

//
// Function:   cam_CallbackJsonAlarm
// Purpose:    Callback JSON: alarm callback: Command-thread has completed
//
// Parms:      Global command area
// Returns:    TRUE if OKee
//
static bool cam_CallbackJsonAlarm(GLOCMD *pstCmd)
{
   GLOBAL_CommandGet(pstCmd);
   GLOBAL_DUMPCOMMAND("cam-CallbackJsonAlarm():PID_CAMR(Get):Reply from PID_CMND", pstCmd);
   //
   // Erase alarm command
   //
   GEN_STRCPY(pstMap->G_pcAlarmCommand, "");
   pstCmd->pfCallback = NULL;
   //
   // Signal command completion to the initiator
   //
   return(TRUE);
}

//
// Function:   cam_CallbackJsonGeneric
// Purpose:    Callback JSON: generic callback: Command-thread has completed
//
// Parms:      Global command area
// Returns:    TRUE if OKee
//
static bool cam_CallbackJsonGeneric(GLOCMD *pstCmd)
{
   GLOBAL_CommandGet(pstCmd);
   GLOBAL_DUMPCOMMAND("cam-CallbackJsonGeneric():PID_CAMR(Get):Reply from PID_CMND", pstCmd);
   pstCmd->pfCallback = NULL;
   //
   // Signal command completion to the initiator
   //
   return(TRUE);
}

//
// Function:   cam_CallbackJsonMotionDetect
// Purpose:    Callback JSON: Motion detect callback: PID_CMND has completed
//
// Parms:      Global command area
// Returns:    TRUE if OKee
//
static bool cam_CallbackJsonMotionDetect(GLOCMD *pstCmd)
{
   int      iDelta;

   GLOBAL_CommandGet(pstCmd);
   GLOBAL_DUMPCOMMAND("cam-CallbackJsonMotionDetect():PID_CAMR(Get):Reply from PID_CMND", pstCmd);
   pstCmd->pfCallback = NULL;
   //
   FINFO_GetFileInfo(pstMap->G_pcLastFile, FI_SIZE, pstMap->G_pcLastFileSize, MAX_PATH_LEN);
   //
   // Motion detect:
   //    Take 2 snapshots
   //    Convert to grayscale
   //    Calculate delta (threshold)
   //    perform action on changes (threshold)
   //
   iDelta = GRF_MotionDetect();
   if(iDelta < 0)
   {
      //
      // Maybe we have insufficient files for the motion detect: take additional pictures
      //
      PRINTF1("cam-CallbackJsonMotionDetect():Error motion detect (Index=%d)" CRLF, pstMap->G_stMotionDetect.iSeq);
   }
   //else if( (iDelta > 0) && (iDelta < 20) )
   else if(iDelta)
   {
      // Check Grid results
      LOG_Report(0, "CAM", "Gridcells exceeding threshold[%d]=%d", pstMap->G_stMotionDetect.iThldLevel, iDelta);
      PRINTF2("cam-CallbackJsonMotionDetect():Gridcells exceeding threshold[%d]=%d" CRLF, pstMap->G_stMotionDetect.iThldLevel, iDelta);
      //
   }
   SHOW_MOTION(&(pstMap->G_stMotionDetect));
   //
   // Send back updated sequence number
   // Show pix on TFT screen (if any) 
   //
   GEN_SPRINTF(pstMap->G_pcDetectSeq, "%d", pstMap->G_stMotionDetect.iSeq);
   GRF_MotionDisplay(pstMap->G_pcLastFile);
   return(TRUE);
}

//
// Function:   cam_CallbackJsonSnapshot
// Purpose:    Callback JSON: Show picture, PID_CMND has completed
//
// Parms:      Global command area
// Returns:    TRUE if OKee
//
static bool cam_CallbackJsonSnapshot(GLOCMD *pstCmd)
{
   GLOBAL_CommandGet(pstCmd);
   GLOBAL_DUMPCOMMAND("cam-CallbackJsonSnapshot():PID_CAMR(Get):Reply from PID_CMND", pstCmd);
   pstCmd->pfCallback = NULL;
   FINFO_GetFileInfo(pstMap->G_pcLastFile, FI_SIZE, pstMap->G_pcLastFileSize, MAX_PATH_LEN);
   //
   // Signal command completion to the initiator
   //
   GRF_MotionDisplay(pstMap->G_pcLastFile);
   PRINTF("cam-CallbackJsonSnapshot():Done" CRLF);
   return(TRUE);
}

