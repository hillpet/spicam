/*  (c) Copyright:  2017  Patrn, Confidential Data
 *
 *  Workfile:           par_pids.h
 *  Revision:
 *  Modtime:
 *
 *  Purpose:            Extract headerfile for the RPI thread PIDs
 *
 *
 *
 *  Compiler/Assembler: Raspbian Linux GNU gcc
 *  Ext Packages:
 *
 *  Author:             Peter Hillen
 *  Date Created:       28 Dec 2017
 *
 *  Revisions:
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 **/

//          ePid           pcPid          pcHelp
EXTRACT_PID(PID_HOST,      "Host",        "Host")
EXTRACT_PID(PID_SRVR,      "Srvr",        "HTTP server")
EXTRACT_PID(PID_CAMR,      "Camr",        "Camera")
EXTRACT_PID(PID_CMND,      "Cmnd",        "Command")
EXTRACT_PID(PID_BUTN,      "Butn",        "Buttons")
EXTRACT_PID(PID_HELP,      "Help",        "Help")
EXTRACT_PID(PID_EMUL,      "Emul",        "IP Emulation")
EXTRACT_PID(PID_LIRC,      "Lirc",        "IR Lirc")
EXTRACT_PID(PID_UTIL,      "Util",        "Utilities")
EXTRACT_PID(PID_FBUF,      "Fbuf",        "Frame Buffer")
EXTRACT_PID(PID_KODI,      "Kodi",        "Kodi")
EXTRACT_PID(PID_PROX,      "Prox",        "Proxy")
EXTRACT_PID(PID_SUPP,      "Supp",        "Support")
EXTRACT_PID(PID_USBC,      "Usbc",        "USB Command")
EXTRACT_PID(PID_USBD,      "Usbd",        "USB Debug")
EXTRACT_PID(PID_RCUD,      "Rcud",        "RCU")
EXTRACT_PID(PID_SPARE1,    "Spr1",        "Spare 1")
EXTRACT_PID(PID_SPARE2,    "Spr2",        "Spare 2")
EXTRACT_PID(PID_SPARE3,    "Spr3",        "Spare 3")

