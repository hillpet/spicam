/*  (c) Copyright:  2017  Patrn, Confidential Data
 *
 *  Workfile:           dyn_proxy.h
 *  Revision:
 *  Modtime:
 *
 *  Purpose:            Extract headerfile for the proxy Smart-E dynamic http pages
 *
 *
 *
 *  Compiler/Assembler: Raspbian Linux GNU gcc
 *  Ext Packages:
 *
 *  Author:             Peter Hillen
 *  Date Created:       31 Oct 2017
 *
 *  Revisions:
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 **/

//           tUrl                   tType,      iTimeout,   iFlags,        pcUrl,                  pfDynCb;
EXTRACT_DYN(DYN_SME_HTML_DATA,      HTTP_HTML,  PRX_TO,     DYN_FLAG_PORT, "smartdata.html",       proxy_DynPageSmartMeter )
EXTRACT_DYN(DYN_SME_JSON_DATA,      HTTP_JSON,  PRX_TO,     DYN_FLAG_PORT, "smartdata.json",       proxy_DynPageSmartMeter )
EXTRACT_DYN(DYN_SME_HTML_FILES,     HTTP_HTML,  PRX_TO,     DYN_FLAG_PORT, "smartfiles.html",      proxy_DynPageSmartMeter )
EXTRACT_DYN(DYN_SME_JSON_FILES,     HTTP_JSON,  PRX_TO,     DYN_FLAG_PORT, "smartfiles.json",      proxy_DynPageSmartMeter )
EXTRACT_DYN(DYN_SME_HTML_DAY,       HTTP_HTML,  PRX_TO,     DYN_FLAG_PORT, "smartday.html",        proxy_DynPageSmartMeter )
EXTRACT_DYN(DYN_SME_HTML_DAYPRV,    HTTP_HTML,  PRX_TO,     DYN_FLAG_PORT, "smartprevday.html",    proxy_DynPageSmartMeter )
EXTRACT_DYN(DYN_SME_HTML_DAYNXT,    HTTP_HTML,  PRX_TO,     DYN_FLAG_PORT, "smartnextday.html",    proxy_DynPageSmartMeter )
EXTRACT_DYN(DYN_SME_JSON_DAY,       HTTP_JSON,  PRX_TO,     DYN_FLAG_PORT, "smartday.json",        proxy_DynPageSmartMeter )
EXTRACT_DYN(DYN_SME_HTML_DAYPLS,    HTTP_HTML,  PRX_TO,     DYN_FLAG_PORT, "smartdayplus.html",    proxy_DynPageSmartMeter )
EXTRACT_DYN(DYN_SME_HTML_DAYMIN,    HTTP_HTML,  PRX_TO,     DYN_FLAG_PORT, "smartdaymin.html",     proxy_DynPageSmartMeter )
EXTRACT_DYN(DYN_SME_HTML_DAYSLM,    HTTP_HTML,  PRX_TO,     DYN_FLAG_PORT, "slim",                 proxy_DynPageSmartMeter )
EXTRACT_DYN(DYN_SME_HTML_NXTSLM,    HTTP_HTML,  PRX_TO,     DYN_FLAG_PORT, "slimnext.html",        proxy_DynPageSmartMeter )
EXTRACT_DYN(DYN_SME_HTML_PRVSLM,    HTTP_HTML,  PRX_TO,     DYN_FLAG_PORT, "slimprev.html",        proxy_DynPageSmartMeter )
EXTRACT_DYN(DYN_SME_HTML_DAYGAS,    HTTP_HTML,  PRX_TO,     DYN_FLAG_PORT, "gas",                  proxy_DynPageSmartMeter )
EXTRACT_DYN(DYN_SME_HTML_DAYPWR,    HTTP_HTML,  PRX_TO,     DYN_FLAG_PORT, "stroom",               proxy_DynPageSmartMeter )
EXTRACT_DYN(DYN_SME_HTML_DAYSOL,    HTTP_HTML,  PRX_TO,     DYN_FLAG_PORT, "zon",                  proxy_DynPageSmartMeter )
EXTRACT_DYN(DYN_SME_HTML_MON,       HTTP_HTML,  PRX_TO,     DYN_FLAG_PORT, "smartmonth.html",      proxy_DynPageSmartMeter )
EXTRACT_DYN(DYN_SME_HTML_PRVMON,    HTTP_HTML,  PRX_TO,     DYN_FLAG_PORT, "smartprevmonth.html",  proxy_DynPageSmartMeter )
EXTRACT_DYN(DYN_SME_HTML_NXTMON,    HTTP_HTML,  PRX_TO,     DYN_FLAG_PORT, "smartnextmonth.html",  proxy_DynPageSmartMeter )
EXTRACT_DYN(DYN_SME_JSON_MON,       HTTP_JSON,  PRX_TO,     DYN_FLAG_PORT, "smartmonth.json",      proxy_DynPageSmartMeter )
EXTRACT_DYN(DYN_SME_HTML_MONPLS,    HTTP_HTML,  PRX_TO,     DYN_FLAG_PORT, "smartmonthplus.html",  proxy_DynPageSmartMeter )
EXTRACT_DYN(DYN_SME_HTML_MONMIN,    HTTP_HTML,  PRX_TO,     DYN_FLAG_PORT, "smartmonthmin.html",   proxy_DynPageSmartMeter )
