/*  (c) Copyright:  2017  Patrn, Confidential Data 
 *
 *  Workfile:           rpi_func.c
 *  Revision:          
 *  Modtime:           
 *
 *  Purpose:            
 *
 *
 *
 *  Compiler/Assembler: Raspbian Linux GNU gcc
 *  Ext Packages:
 *
 *  Author:             Peter Hillen
 *  Date Created:       09 Mar 2017
 *
 *  Revisions:
 *  Entry Points:       
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
**/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdarg.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>
#include <sched.h>
#include <time.h>

#include <common.h>
#include "config.h"
#include "globals.h"
#include "rpi_page.h"
#include "rpi_json.h"
#include "cmd_alarm.h"
#include "cmd_mail.h"
#include "gen_func.h"
#include "gen_http.h"
//
#include "rpi_func.h"

//#define USE_PRINTF
#include <printf.h>

typedef bool (*HTTPFUN)(int);
//
typedef struct httpargs
{
   int            iId;
   int            iFunction;
   const char    *pcJson;
   const char    *pcHtml;
   int            iValueOffset;
   int            iValueSize;
   int            iChangedOffset;
   HTTPFUN        pfHttpFun;
}  HTTPARGS;
//
// Prototypes Collect Callback
//
static bool  http_CollectRcuKey              (int);
static bool  http_CollectDebug               (int); 
static bool  http_MotionChanged              (int);
static bool  http_MailChanged                (int);
//
// Index list to global Arguments G_xxxx
//
static const HTTPARGS stHttpArguments[] =
{
//       a                 b                                c           d        f                             g              h                                      k
//       Enum,             iFunction                        pcJson,     pcHtml   iValueOffset                  iValueSize     iChangedOffset,                        pfHttpFun
//       xxx,              JSN_TXT|CAM_STI|...|CAM____,     "Width",    "w=",    offsetof(RPIMAP, G_pcWidth),  MAX_PARM_LEN,  offsetof(RPIMAP, G_ubWidthChanged),    NULL
//   
#define  EXTRACT_PAR(a,b,c,d,e,f,g,h,i,j,k)    {a,b,c,d,f,g,h,k},
#include "par_defs.h"
#undef   EXTRACT_PAR
   {     NUM_GLOBAL_DEFS,  0,                               NULL,       NULL,    0,                            0,             0,                                     NULL}
};


//
// Index list to global Rcu/Fp keys
//
static const char *pcRcuKeyLut[] =
{
#define  EXTRACT_KEY(a,b)    b,
#include "key_defs.h"
#include "btn_defs.h"
#undef   EXTRACT_KEY
         NULL
};

//
// Local prototypes
//
static bool       rpi_HandleParm          (char *, FTYPE, int);
static RPIDATA   *rpi_JsonShowFiles       (RPIDATA *, char *, char *, bool);
static RPIDATA   *rpi_JsonShowEntries     (RPIDATA *, char *, char *, int, int *);
static bool       rpi_ParmSetChanged      (const HTTPARGS *, bool);
static void       rpi_UpdateRecordingTime (void);
//
#ifdef  FEATURE_JSON_SHOW_REPLY
static void rpi_ShowNetReply              (const char *, int, char *);
#define SHOW_NET_REPLY(a,b,c)             rpi_ShowNetReply(a,b,c)
#else
#define SHOW_NET_REPLY(a,b,c)
#endif  //FEATURE_JSON_SHOW_REPLY
//
static const char pcWebPageCommandBusy[]  = "<p>Raspicam Error: Ongoing cam action!</p>" NEWLINE;
static const char pcWebPageCommandBad[]   = "<p>Raspicam Error: Unknown command!</p>" NEWLINE;
//
static const char pcAmpAnd[]              = "&";
static const char pcParmDelims[]          = "=:";
//
// Motion mode (see globas.h)
//
static const char *pcMotionModes[NUM_MOTION_MODES] =
{
   "MM_OFF",
   "MM_MAN",
   "MM_AUG",
   "MM_CYC",
   "MM_PIC",
   "MM_VID",
   "MM_STR"
};
//
static const char *pcMotionModeError = "MOTION_MODE_ERROR";


/*------  Local functions separator ------------------------------------
__GLOBAL_FUNCTIONS(){};
----------------------------------------------------------------------------*/

//
// Function:   RPI_BuildJsonMessageBody
// Purpose:    Build the JSON response object to be send back through the socket
//
// Parms:      Socket descriptor, Command 
// Returns:    TRUE if OKee
//
bool RPI_BuildJsonMessageBody(NETCL *pstCl, GLOCMD *pstCmd)
{
   bool     fCc;

   PRINTF("RPI_BuildJsonMessageBody()" CRLF);
   fCc = RPI_BuildJsonMessageArgs(pstCl, pstCmd->iArgs);
   return(fCc);
}

//
// Function:   RPI_BuildJsonMessageArgs
// Purpose:    Build the JSON response object to be send back through the socket
//
// Parms:      Socket descriptor, Args 
// Returns:    TRUE if OKee
//
bool RPI_BuildJsonMessageArgs(NETCL *pstCl, int iArgs)
{
   bool            fCc;
   char           *pcValue;
   RPIDATA        *pstCam=NULL;
   const HTTPARGS *pstArgs=stHttpArguments;

   PRINTF2("RPI_BuildJsonMessageBody():Function %s, Args=0x%X" CRLF, pstMap->G_pcCommand, iArgs);
   if(iArgs == 0)
   {
      LOG_Report(0, "RPI", "RPI_BuildJsonMessageArgs():%s has No JSON data to reply!", pstMap->G_pcCommand);
      PRINTF("RPI_BuildJsonMessageArgs():No JSON data to reply!" CRLF);
      iArgs = CAM_ALL;
   }
   //
   rpi_UpdateRecordingTime();
   //
   // Build JSON object
   //
   pstCam = JSON_InsertParameter(pstCam, NULL, NULL, JE_CURLB|JE_CRLF);
   while(pstArgs->iFunction)
   {
      if(pstArgs->iFunction & iArgs)
      {
         pcValue = (char *)pstMap + pstArgs->iValueOffset;
         //
         // Add all parameters to JSON object
         //
         if(pstArgs->iFunction & JSN_TXT) pstCam = JSON_InsertParameter(pstCam, pstArgs->pcJson, pcValue, JE_TEXT|JE_COMMA|JE_CRLF);
         else                             pstCam = JSON_InsertParameter(pstCam, pstArgs->pcJson, pcValue,         JE_COMMA|JE_CRLF);
      }
      pstArgs++;
   }
   pstCam = JSON_TerminateObject(pstCam);
   fCc    = JSON_RespondObject(pstCl, pstCam);
   //
   // Done with this function: exit
   //
   JSON_ReleaseObject(pstCam);
   return(fCc);
}

//
// Function:   RPI_BuildJsonMessageBodyAlarm
// Purpose:    Build applicable JSON array objects
//
// Parms:      Client, command args
// Returns:    TRUE if OKee
//             JSON object returned:
//                {
//                   "Command": "alarm",
//                   "...": "....",
//                   "Alarms":
//                   [
//                      { 
//                         "Command", "checkhdd", 
//                         "Timestamp", "........_....00", 
//                         "Armed", 1, 
//                         "Used", 1, 
//                            ....
//                      },
//                      {
//                         ....
//                      }
//                   ],
//                   "Number": "XXX"
//                }
// 
// 
bool RPI_BuildJsonMessageBodyAlarm(NETCL *pstCl, int iArgs)
{
   bool            fCc;
   int             iIdx;
   u_int32         ulSecs;
   RPIDATA        *pstCam=NULL;
   const HTTPARGS *pstArgs=stHttpArguments;
   char           *pcValue;
   char           *pcTemp;

   PRINTF("RPI_BuildJsonMessageBodyAlarm()" CRLF);
   //
   rpi_UpdateRecordingTime();
   //
   pcTemp = (char *) safemalloc(MAX_PATH_LEN);
   //
   // Build JSON object:
   // {
   //
   pstCam = JSON_InsertParameter(pstCam, NULL, NULL, JE_CURLB|JE_CRLF);
   //
   // Insert all relevant json items
   //
   while(pstArgs->iFunction)
   {
      pcValue = (char *)pstMap + pstArgs->iValueOffset;
      if(pstArgs->iFunction & iArgs)
      {
         if(pstArgs->iFunction & JSN_TXT) pstCam = JSON_InsertParameter(pstCam, pstArgs->pcJson, pcValue, JE_TEXT|JE_COMMA|JE_CRLF);
         else                             pstCam = JSON_InsertParameter(pstCam, pstArgs->pcJson, pcValue,         JE_COMMA|JE_CRLF);
      }
      pstArgs++;
   }
   //
   // Add all supported functions
   //    "Functions":
   //    "["
   //    {"Index":0, "Command":"none"},
   //    {"Index":1, "Command":"snapshot"},
   //    {"Index":2, "Command":"....."},
   //    "]"
   //
   iIdx   = 0;
   pstCam = JSON_InsertParameter(pstCam, "Functions", "", JE_SQRB|JE_CRLF);
   while( (pcValue = (char *)ALARM_GetCommand(iIdx)) )
   {
      GEN_SPRINTF(pcTemp, "%d", iIdx);
      pstCam = JSON_InsertParameter(pstCam, "Index",   pcTemp,  JE_CURLB|JE_COMMA);
      pstCam = JSON_InsertParameter(pstCam, "Command", pcValue, JE_TEXT|JE_CURLE|JE_CRLF);
      iIdx++;
   }
   pstCam = JSON_TerminateArray(pstCam);
   //
   // Add all array elements to JSON object
   //
   pstCam = JSON_InsertParameter(pstCam, "Alarms", "", JE_SQRB|JE_CRLF);
   for(iIdx=0; iIdx<MAX_NUM_ALARMS;iIdx++)
   {
      //
      // Used        : Alarm is in use
      // Armed       : Alarm is armed to run
      //
      GEN_SPRINTF(pcTemp, "%d", (pstMap->G_stAlarms[iIdx].iFlags & ALARM_USED?1:0) );
      pstCam = JSON_InsertParameter(pstCam, "Used", pcTemp, JE_CURLB|JE_COMMA);
      //
      // Add alarm flags : ALARM_ARMED, ..
      //
      GEN_SPRINTF(pcTemp, "%d", pstMap->G_stAlarms[iIdx].iFlags);
      pstCam = JSON_InsertParameter(pstCam, "Armed", pcTemp, JE_COMMA);
      //
      // Command     : Function to be called next run
      // Timestamp   : 20131130_223345 format time stamp
      //
      pstCam = JSON_InsertParameter(pstCam, "Command", pstMap->G_stAlarms[iIdx].pcCommand, JE_COMMA|JE_TEXT);
      pstCam = JSON_InsertParameter(pstCam, "Timestamp", pstMap->G_stAlarms[iIdx].pcDandT, JE_COMMA|JE_TEXT);
      //
      // RepeatNumber: Number of times to repeat the function
      // RepeatTime  : Seconds between repeats
      // RepeatCount : Actual number of times the function has been repeated
      // RepeatNext  : Timestamp of next repeat run
      //
      GEN_SPRINTF(pcTemp, "%d", pstMap->G_stAlarms[iIdx].iRepTime);
      pstCam = JSON_InsertParameter(pstCam, "RepeatTime", pcTemp, JE_COMMA);
      GEN_SPRINTF(pcTemp, "%d", pstMap->G_stAlarms[iIdx].iRepNum);
      pstCam = JSON_InsertParameter(pstCam, "RepeatNumber", pcTemp, JE_COMMA);
      GEN_SPRINTF(pcTemp, "%d", pstMap->G_stAlarms[iIdx].iRepCount);
      pstCam = JSON_InsertParameter(pstCam, "RepeatCount",  pcTemp, JE_COMMA);
      //
      // Format next time
      //
      ulSecs = pstMap->G_stAlarms[iIdx].ulRepNext;
      if(ulSecs > 0)
      {
         RTC_ConvertDateTime(TIME_FORMAT_YYYY_MM_DD_HH_MM_SS, pcTemp, ulSecs);
      }
      else
      {
         GEN_SPRINTF(pcTemp, "0");
      }
      pstCam = JSON_InsertParameter(pstCam, "RepeatNext", pcTemp, JE_TEXT|JE_CURLE|JE_CRLF);
   }
   pstCam = JSON_TerminateArray(pstCam);
   //
   // Insert global JSON info:
   //    "Command":"xxxxxxxx",
   //    "Version":"xxxxxxxx",
   //    "xxxx":"xxxxxxxx",
   //
   GEN_SPRINTF(pcTemp, "%d", pstMap->G_iNumAlarms);
   pstCam = JSON_InsertParameter(pstCam, "Number", pcTemp, JE_NONE);
   pstCam = JSON_TerminateObject(pstCam);
   fCc    = JSON_RespondObject(pstCl, pstCam);
   //
   // Done with this function: exit
   //
   JSON_ReleaseObject(pstCam);
   //
   safefree(pcTemp);
   //
   return(fCc);
}

//
// Function:   RPI_BuildJsonMessageBodyArray
// Purpose:    Build applicable JSON array objects
//
// Parms:      Client, command, fullpath-YesNo
// Returns:    TRUE if OKee
// Note:       G_Delete marks the deletion:
//                fotos.json?delete=yes
//
//             JSON object returned:
//             {
//                "Command": "fotos",     // or "videos"
//                "fotos":
//                [
//                   { "filename/pathname", filesize },
//                   { "filename/pathname", filesize },
//                   { ....                          },
//                   { "filename/pathname", filesize }
//                ],
//                "NumFiles": "XXX", 
//                "Filter", "201306"
//             }
//
bool RPI_BuildJsonMessageBodyArray(NETCL *pstCl, int tCmd, bool fFullpath)
{
   bool            fCc;
   int            iArgs=0;
   char           *pcValue;
   char           *pcFile, *pcFullPath;
   RPIDATA        *pstCam=NULL;
   const HTTPARGS *pstArgs=stHttpArguments;

   PRINTF("RPI_BuildJsonMessageBodyArray()" CRLF);
   //
   rpi_UpdateRecordingTime();
   //
   switch(tCmd)
   {
      default:
         pcFile = "";
         break;

      case RPI_DYN_JSON_FOTOS:
         if(GEN_CheckDelete())
         {
            iArgs = CAM_RMP;
         }
         else
         {
            iArgs = CAM_PIX;
         }
         pcFile = pstMap->G_pcPixPath;
         break;

      case RPI_DYN_JSON_VIDEOS:
         if(GEN_CheckDelete())
         {
            iArgs = CAM_RMV;
         }
         else
         {
            iArgs = CAM_VID;
         }
         pcFile = pstMap->G_pcVideoPath;
         break;
   }
   pcFullPath = (char *) safemalloc(MAX_PATH_LEN);
   GEN_SPRINTF(pcFullPath, "%s%s", pstMap->G_pcWwwDir, pcFile);
   //
   // Build JSON object (insert global JSON info):
   // {
   //    "Command":"fotos",
   //    "Version":"xxxxxxxx",
   //    "Pathname":"xxxxxxxx",
   //    "fotos["
   //       ....
   //    "]"
   //
   pstCam = JSON_InsertParameter(pstCam, NULL, NULL, JE_CURLB|JE_CRLF);
   pstCam = JSON_InsertParameter(pstCam, (const char *)pstMap->G_pcCommand, "", JE_SQRB|JE_CRLF);
   //
   // Add all specific array elements to JSON object
   //
   pstCam = rpi_JsonShowFiles(pstCam, pcFile, pstMap->G_pcFilter, fFullpath);
   pstCam = JSON_TerminateArray(pstCam);
   //
   while(pstArgs->iFunction)
   {
      pcValue = (char *)pstMap + pstArgs->iValueOffset;
      if(pstArgs->iFunction & iArgs)
      {
         if(pstArgs->iFunction & JSN_TXT) pstCam = JSON_InsertParameter(pstCam, pstArgs->pcJson, pcValue, JE_TEXT|JE_COMMA|JE_CRLF);
         else                             pstCam = JSON_InsertParameter(pstCam, pstArgs->pcJson, pcValue,         JE_COMMA|JE_CRLF);
      }
      pstArgs++;
   }
   pstCam = JSON_InsertParameter(pstCam, "Pathname", pcFullPath, JE_TEXT|JE_COMMA|JE_CRLF);
   pstCam = JSON_TerminateObject(pstCam);
   fCc    = JSON_RespondObject(pstCl, pstCam);
   //
   // Done with this function: exit
   //
   JSON_ReleaseObject(pstCam);
   safefree(pcFullPath);
   return(fCc);
}

//
// Function:   RPI_BuildJsonMessageKodiRpcGetInput
// Purpose:    Build the JSON response object to be send back through the socket
//
// Parms:      Socket descriptor, JSONRPC Id
// Returns:    TRUE if OKee
// Note:       Kodi reply to
//             {"method":"Input.Up","id":1770075167,"jsonrpc":"2.0"}
//                is 
//             {"id":1770075167,"jsonrpc":"2.0","result":"OK"}
//
bool RPI_BuildJsonMessageKodiRpcGetInput(NETCL *pstCl, int iId)
{
   bool     fCc;
   RPIDATA *pstCam=NULL;

   //
   // Build JSON object
   //
   pstCam = JSON_InsertParameter(pstCam, NULL,        NULL,                                 JE_CURLB);
   pstCam = JSON_InsertInteger(pstCam,   "id",        iId,                                  JE_COMMA);
   pstCam = JSON_InsertParameter(pstCam, "jsonrpc",   "2.0",                        JE_TEXT|JE_COMMA);
   pstCam = JSON_InsertParameter(pstCam, "result",    "OK",                         JE_TEXT|JE_COMMA);
   //
   pstCam = JSON_TerminateObject(pstCam);
   fCc    = JSON_RespondObject(pstCl, pstCam);
   //
   SHOW_NET_REPLY("RPI_BuildJsonMessageKodiRpcGetInput()", pstCl->iPort, pstCam->pcObject);
   //
   // Done with this function: exit
   //
   JSON_ReleaseObject(pstCam);
   return(fCc);
}

//
// Function:   RPI_BuildJsonMessageKodiRpcGetItems
// Purpose:    Build the JSON response object to be send back through the socket
//
// Parms:      Socket descriptor, JSONRPC Id
// Returns:    TRUE if OKee
// Note:       Kodi reply to
//             {"method":"Playlist.GetItems","id":804728480,"jsonrpc":"2.0",
//                "params":
//                {  "properties":
//                   [
//                      "thumbnail","duration","artist","album","runtime","showtitle", "season",
//                      "episode","artistid","albumid","genre","tvshowid","file","title"
//                   ],
//                   "playlistid":0
//                }
//             }
//             is 
//             {"id":804728480,"jsonrpc":"2.0","result":{"limits":{"end":0,"start":0,"total":0}}}
//
bool RPI_BuildJsonMessageKodiRpcGetItems(NETCL *pstCl, int iId)
{
   bool     fCc;
   RPIDATA *pstCam=NULL;

   //
   // Build JSON object
   //
   pstCam = JSON_InsertParameter(pstCam, NULL,        NULL,                                 JE_CURLB);
   pstCam = JSON_InsertInteger(pstCam,   "id",        iId,                                  JE_COMMA);
   pstCam = JSON_InsertParameter(pstCam, "jsonrpc",   "2.0",                        JE_TEXT|JE_COMMA);
   //
   pstCam = JSON_InsertParameter(pstCam, "result",    NULL,                                        0);
   pstCam = JSON_InsertParameter(pstCam, "limits",    NULL,                                 JE_CURLB);
   pstCam = JSON_InsertParameter(pstCam, "end",       "0",                         JE_CURLB|JE_COMMA);
   pstCam = JSON_InsertParameter(pstCam, "start",     "0",                                  JE_COMMA);
   pstCam = JSON_InsertParameter(pstCam, "total",     "0",                                  JE_CURLE);
   //
   pstCam = JSON_TerminateObject(pstCam);
   fCc    = JSON_RespondObject(pstCl, pstCam);
   //
   SHOW_NET_REPLY("RPI_BuildJsonMessageKodiRpcGetItems()", pstCl->iPort, pstCam->pcObject);
   //
   // Done with this function: exit
   //
   JSON_ReleaseObject(pstCam);
   return(fCc);
}

//
// Function:   RPI_BuildJsonMessageKodiRpcGetPing
// Purpose:    Build the JSON response object to be send back through the socket
//
// Parms:      Socket descriptor, JSONRPC Id
// Returns:    TRUE if OKee
// Note:       Kodi reply to
//             {"method":"JSONRPC.Ping","id":1770075167,"jsonrpc":"2.0"}
//                is 
//             {"id":1770075167,"jsonrpc":"2.0","result":"pong"}
//
bool RPI_BuildJsonMessageKodiRpcGetPing(NETCL *pstCl, int iId)
{
   bool     fCc;
   RPIDATA *pstCam=NULL;

   //
   // Build JSON object
   //
   pstCam = JSON_InsertParameter(pstCam, NULL,        NULL,                                 JE_CURLB);
   pstCam = JSON_InsertInteger(pstCam,   "id",        iId,                                  JE_COMMA);
   pstCam = JSON_InsertParameter(pstCam, "jsonrpc",   "2.0",                        JE_TEXT|JE_COMMA);
   pstCam = JSON_InsertParameter(pstCam, "result",    "pong",                       JE_TEXT|JE_COMMA);
   //
   pstCam = JSON_TerminateObject(pstCam);
   fCc    = JSON_RespondObject(pstCl, pstCam);
   //
   SHOW_NET_REPLY("RPI_BuildJsonMessageKodiRpcGetPing()", pstCl->iPort, pstCam->pcObject);
   //
   // Done with this function: exit
   //
   JSON_ReleaseObject(pstCam);
   return(fCc);
}

//
// Function:   RPI_BuildJsonMessageKodiRpcGetProps
// Purpose:    Build the JSON response object to be send back through the socket
//
// Parms:      Socket descriptor, JSONRPC Id
// Returns:    TRUE if OKee
// Note:       Kodi reply to
//             {"method":"Application.GetProperties","id":1770075167,"jsonrpc":"2.0","params":{"properties":["version","volume","name"]}}
//                is 
//             {"id":1770075167,"jsonrpc":"2.0","result":{"name":"Kodi","version":{"major":17,"minor":3,"revision":"20170525-1789806-dirty","tag":"stable"},"volume":96}}
//
bool RPI_BuildJsonMessageKodiRpcGetProps(NETCL *pstCl, int iId)
{
   bool     fCc;
   RPIDATA *pstCam=NULL;

   //
   // Build JSON object
   //
   pstCam = JSON_InsertParameter(pstCam, NULL,        NULL,                                 JE_CURLB);
   pstCam = JSON_InsertInteger(pstCam,   "id",        iId,                                  JE_COMMA);
   pstCam = JSON_InsertParameter(pstCam, "jsonrpc",   "2.0",                        JE_TEXT|JE_COMMA);
   pstCam = JSON_InsertParameter(pstCam, "result",    NULL,                                        0);
   pstCam = JSON_InsertParameter(pstCam, "name",      "Popkodi",           JE_CURLB|JE_TEXT|JE_COMMA);
   pstCam = JSON_InsertParameter(pstCam, "version",   NULL,                                        0);
   pstCam = JSON_InsertParameter(pstCam, "major",     "17",                        JE_CURLB|JE_COMMA);
   pstCam = JSON_InsertParameter(pstCam, "minor",     "3",                                  JE_COMMA);
   pstCam = JSON_InsertParameter(pstCam, "revision",  "20170525-1789806-dirty",     JE_TEXT|JE_COMMA);
   pstCam = JSON_InsertParameter(pstCam, "tag",       "stable",                     JE_TEXT|JE_CURLE);
   pstCam = JSON_InsertParameter(pstCam, "volume",    "86",                                 JE_CURLE);
   //
   pstCam = JSON_TerminateObject(pstCam);
   fCc    = JSON_RespondObject(pstCl, pstCam);
   //
   SHOW_NET_REPLY("RPI_BuildJsonMessageKodiRpcGetProps()", pstCl->iPort, pstCam->pcObject);
   //
   // Done with this function: exit
   //
   JSON_ReleaseObject(pstCam);
   return(fCc);
}

//
// Function:   RPI_BuildJsonMessageKodiRpcGetPlayers
// Purpose:    Build the JSON response object to be send back through the socket
//
// Parms:      Socket descriptor, JSONRPC Id
// Returns:    TRUE if OKee
// Note:       Kodi reply to
//             {"method":"Player.GetActivePlayers","id":1770075167,"jsonrpc":"2.0","params":{}}
//                is 
//             {"id":1770075167,"jsonrpc":"2.0","result":[]}
//
bool RPI_BuildJsonMessageKodiRpcGetPlayers(NETCL *pstCl, int iId)
{
   bool     fCc;
   RPIDATA *pstCam=NULL;

   //
   // Build JSON object
   //
   pstCam = JSON_InsertParameter(pstCam, NULL,        NULL,                                 JE_CURLB);
   pstCam = JSON_InsertInteger(pstCam,   "id",        iId,                                  JE_COMMA);
   pstCam = JSON_InsertParameter(pstCam, "jsonrpc",   "2.0",                        JE_TEXT|JE_COMMA);
   pstCam = JSON_InsertFile(pstCam,      "result",    pstMap->G_pcLastFile,                 JE_COMMA);
   //
   pstCam = JSON_TerminateObject(pstCam);
   fCc    = JSON_RespondObject(pstCl, pstCam);
   //
   SHOW_NET_REPLY("RPI_BuildJsonMessageKodiRpcGetPlayers()", pstCl->iPort, pstCam->pcObject);
   //
   // Done with this function: exit
   //
   JSON_ReleaseObject(pstCam);
   return(fCc);
}

//
// Function:   RPI_CollectParms
// Purpose:    Collect PiCam parameter passing from HTTP-data into the MMAP global area
//
// Parms:      Client area, HTTP-type
// Returns:    Nr of parms found
// Note:       
//             pstCl->pcUrl-> "/all/public/www/cam.json?Command=snapshot&Height=576&Ext=bmp&Preview=860,0,860,540&Rotation=180&Width=1024"
//                             |               |   |    |
//             iPathIdx = -----+               |   |    |
//             iFileIdx = ---------------------+   |    |
//             iExtnIdx = -------------------------+    |
//             iPaylIdx = ------------------------------+
//  
int RPI_CollectParms(NETCL *pstCl, FTYPE tType)
{
   int             iNr=0;
   char           *pcParm;
   const HTTPARGS *pstArgs=stHttpArguments;

   //
   // Set first parameter
   //
   pstCl->iNextIdx = pstCl->iPaylIdx;
   //
   // Before collecting new parms, reset all changed markers
   //
   while(pstArgs->iFunction)
   {
      rpi_ParmSetChanged(pstArgs, 0);
      pstArgs++;
   }

#ifdef FEATURE_HTML_DEFAULT_COMMAND
   //
   // HTML syntax has the command first:
   // so:   cam.html?snapshot&w=400
   // else: cam.html?cmd=snapshot&w=400
   //
   if(tType == HTTP_HTML)
   {
      if( (pcParm = HTTP_CollectGetParameter(pstCl)) )
      {
         iNr++;
         GEN_STRNCPY(pstMap->G_pcCommand, pcParm, MAX_PARM_LEN);
      }
   }
#endif   //FEATURE_HTML_DEFAULT_COMMAND
   //
   // Retrieve and store all arguments
   //
   do
   {
      pcParm = HTTP_CollectGetParameter(pstCl);
      if(pcParm)
      {
         iNr++;
         rpi_HandleParm(pcParm, tType, CAM_ALL);
      }
   }
   while(pcParm);
   //
   PRINTF1("RPI_CollectParms(): %d arguments found" CRLF, iNr);
   return(iNr);
}

//
// Function:   RPI_ReportBusy
// Purpose:    Report that the command thread is busy
//
// Parms:      Socket descriptor, command struct
// Returns:    
// Note:       
//             
void RPI_ReportBusy(NETCL *pstCl, GLOCMD *pstCmd)
{
   int      iIdx, tType;

   LOG_Report(0, "RPI", "RPI-ReportBusy():Command rejected[%s], Helper=%d", GLOBAL_PidGet(PID_HELP), pstCl->pcUrl);
   PRINTF2("RPI-ReportBusy():Command rejected[%s], Helper=%d" CRLF, GLOBAL_PidGet(PID_HELP), pstCl->pcUrl);
   //
   iIdx  = pstMap->G_iCurDynPage;
   tType = GEN_GetDynamicPageType(iIdx);        // HTTP_HTML, HTTP_JSON
   //
   switch(tType)
   {
      default:
      case HTTP_HTML:
         // Use HTML/JS API
         HTTP_BuildRespondHeader(pstCl);        //"HTTP/1.0 200 OK" NEWLINE "Content-Type=text/html" NEWLINE NEWLINE;
         HTTP_BuildStart(pstCl);                // Title - eyecandy
         HTTP_BuildGeneric(pstCl, pcWebPageCommandBusy);
         HTTP_BuildLineBreaks(pstCl, 1);
         HTTP_BuildEnd(pstCl);
         break;
   
      case HTTP_JSON:
         // Use JSON API
         RPI_BuildJsonMessageBody(pstCl, pstCmd);
         break;
   }
}

//
// Function:   RPI_ReportError
// Purpose:    Report error
//
// Parms:      Socket descriptor, command struct
// Returns:    
// Note:       
//             
void RPI_ReportError(NETCL *pstCl, GLOCMD *pstCmd)
{
   int      iIdx, tType;

   iIdx  = pstMap->G_iCurDynPage;
   tType = GEN_GetDynamicPageType(iIdx);        // HTTP_HTML, HTTP_JSON
   //
   switch(tType)
   {
      default:
      case HTTP_HTML:
         // Use HTML/JS API
         LOG_Report(0, "RPI", "RPI_ReportError():HTML[%s]", pstCl->pcUrl);
         PRINTF1("RPI_ReportError():HTML[%s]" CRLF, pstCl->pcUrl);
         HTTP_BuildRespondHeader(pstCl);        //"HTTP/1.0 200 OK" NEWLINE "Content-Type=text/html" NEWLINE NEWLINE;
         HTTP_BuildStart(pstCl);                // Title - eyecandy
         HTTP_BuildGeneric(pstCl, pcWebPageCommandBad);
         HTTP_BuildLineBreaks(pstCl, 1);
         HTTP_BuildEnd(pstCl);
         break;
   
      case HTTP_JSON:
         // Use JSON API
         LOG_Report(0, "RPI", "RPI_ReportError():JSON[%s]", pstCl->pcUrl);
         PRINTF1("RPI_ReportError():JSON[%s]" CRLF, pstCl->pcUrl);
         RPI_BuildJsonMessageBody(pstCl, pstCmd);
         break;
   }
}

//
// Function:   RPI_GetMotionMode
// Purpose:    Get the motion mode text
//
// Parms:      
// Returns:    Mode
// Note:       
//             "Off"      : MOTION_MODE_OFF
//             "Manual"   : MOTION_MODE_MANUAL
//             "Augment"  : MOTION_MODE_AUGMENT
//             "Cycle"    : MOTION_MODE_CYCLE
//             "Picture " : MOTION_MODE_PIXTURE
//             "Video"    : MOTION_MODE_VIDEO
//             "Stream"   : MOTION_MODE_STREAM
//
const char *RPI_GetMotionMode(int iMode)
{
   if(iMode < NUM_MOTION_MODES) return(pcMotionModes[iMode]);
   else                         return(pcMotionModeError);
}

//
// Function:   RPI_SetMotionMode
// Purpose:    Set the motion mode
//
// Parms:      
// Returns:    Mode
// Note:       G_pcDetectMode = 
//             "Off"      : MOTION_MODE_OFF
//             "Manual"   : MOTION_MODE_MANUAL
//             "Augment"  : MOTION_MODE_AUGMENT
//             "Cycle"    : MOTION_MODE_CYCLE
//             "Picture " : MOTION_MODE_PIXTURE
//             "Video"    : MOTION_MODE_VIDEO
//             "Stream"   : MOTION_MODE_STREAM
//
int RPI_SetMotionMode()
{
   int      iMode=MOTION_MODE_MANUAL;

   //
   // Select correct mode
   //
   switch(*pstMap->G_pcDetectMode)
   {
      default:
      case 'o':
      case 'O':
         iMode = MOTION_MODE_OFF;
         break;

      case 'm':
      case 'M':
         iMode = MOTION_MODE_MANUAL;
         break;

      case 'a':
      case 'A':
         iMode = MOTION_MODE_AUGMENT;
         break;

      case 'c':
      case 'C':
         iMode = MOTION_MODE_CYCLE;
         break;

      case 'p':
      case 'P':
         iMode = MOTION_MODE_PICTURE;
         break;

      case 'v':
      case 'V':
         iMode = MOTION_MODE_VIDEO;
         break;

      case 's':
      case 'S':
         iMode = MOTION_MODE_STREAM;
         break;
   }
   pstMap->G_stMotionDetect.iMotMode = iMode;
   return(iMode);
}


/*------  Local functions separator ------------------------------------
__LOCAL_FUNCTIONS(){};
----------------------------------------------------------------------------*/

//
// Function:   rpi_HandleParm
// Purpose:    Lookup an argement in the main Args list and copy its value (if any) into the Global mmap area
//
// Parms:      Ptr tp parm "XXX=500", HTTP_HTML|HTTP_JSON|..., CAM_xxx
// Returns:    TRUE if parm OKee
// Note:       
//             
static bool rpi_HandleParm(char *pcParm, FTYPE tType, int iFunction)
{
   bool            fCc=FALSE;
   int             x;
   const char     *pcArg;
   const HTTPARGS *pstArgs=stHttpArguments;
   char           *pcValue;
   HTTPFUN         pfCallback;

   //PRINTF1("rpi-HandleParm(): Parm=<%s>" CRLF, pcParm);
   //
   while(pstArgs->iFunction)
   {
      //
      // iFunction is the bitwise function-filter (CAM_xxx)
      //
      if(pstArgs->iFunction & iFunction)
      {
         switch(tType)
         {
            case HTTP_JSON:   
               pcArg = pstArgs->pcJson; 
               break;
            
            case HTTP_HTML:
            default:          
               pcArg = pstArgs->pcHtml; 
               break;
         }
         //
         // Check parameter_to_variable delimiter "=" or ":"
         //
         x = GEN_SizeToDelimiters(pcParm, (char *)pcParmDelims);
         if(x == 0)
         {
            //
            // No delimiter: check variable without value (flag, ...)
            //
            x = GEN_SizeToDelimiters(pcParm, (char *)pcAmpAnd);
         }
         if( (x > 0) && (GEN_STRNCMPI(pcParm, pcArg, x) == 0) )
         {
            //PRINTF3("rpi-HandleParm(): cmp(%s,%s,%d) match" CRLF, pcArg, pcParm, x);
            //
            // Option found: copy value
            //
            pcArg = GEN_FindDelimiters(pcParm, (char *)pcParmDelims);
            if(pcArg)
            {
               pcValue = (char *)pstMap + pstArgs->iValueOffset;
               //
               // Copy parameter value from HTTP buffer to Global mmap
               //  "Command=Blablablabla&Parm1=Blah&Parm2=Bloh"
               //           ^----------^
               //             copy
               //
               GEN_CopyToDelimiter((char)*pcAmpAnd, pcValue, (char *)pcArg, pstArgs->iValueSize);
               rpi_ParmSetChanged(pstArgs, 1);
               PRINTF2("rpi-HandleParm(): Opt=<%s>:Value=<%s>" CRLF, pcParm, pcValue);
               fCc = TRUE;
            }
            else
            {
               //
               // ParameterX without value:
               //    "ParameterX&ParameterY=12033"
               //    "ParameterX"
               //
               pcValue = (char *)pstMap + pstArgs->iValueOffset;
               GEN_STRCPY(pcValue, "1");
               rpi_ParmSetChanged(pstArgs, 1);
               PRINTF1("rpi-HandleParm(): Opt:<%s>:No Value:force <1> or TRUE" CRLF, pcParm);
               fCc = TRUE;
            }
            //
            // Call the parameter callback function, if any
            //
            pfCallback = pstArgs->pfHttpFun;
            if(pfCallback) 
            {
               PRINTF1("rpi-HandleParm(): Id=%d" CRLF, pstArgs->iId);
               fCc = pfCallback(pstArgs->iId);
            }
            //
            // Arg found: no need to search for more !
            //
            break;
         }
         else
         {
            //PRINTF3("rpi-HandleParm(): cmp(%s,%s,%d) NO match" CRLF, pcArg, pcParm, x);
         }
      }
      pstArgs++;
   }
   return(fCc);
}

//
// Function:   rpi_JsonShowFiles
// Purpose:    Retrieve all files in directory from disk
//
// Parms:      Json object, Subdir, Filter, YesNo:fullpath
// Returns:    Json object
// Note:       Subdir = "video/"
//             
static RPIDATA *rpi_JsonShowFiles(RPIDATA *pstCam, char *pcSubdir, char *pcFilter, bool fFullPath)
{
   int      iBaseIdx=0, iNumFiles=0;
   char    *pcPath;

   pcPath = (char *) safemalloc(MAX_PATH_LEN);
   //
   GEN_SPRINTF(pcPath, "%s%s", pstMap->G_pcWwwDir, pcSubdir);
   if(!fFullPath) iBaseIdx = GEN_STRLEN(pcPath);
   PRINTF1("rpi-JsonShowFiles():Path=%s" CRLF, pcPath);
   //
   pstCam = rpi_JsonShowEntries(pstCam, pcPath, pcFilter, iBaseIdx, &iNumFiles);
   //
   GEN_SPRINTF(pstMap->G_pcNumFiles, "%d", iNumFiles);
   safefree(pcPath);
   return(pstCam);
}

//
// Function:   rpi_JsonShowEntries
// Purpose:    Retrieve all files in directory from disk
//
// Parms:      Json object, Path, Filter, Base Pathname index, Ptr to Nr of Files
// Returns:    Json oject
// Note:       Called recursively by rpi_JsonShowFiles()
//             Base Index=0: Full path in JSON obj, else only the subdir path
//             
static RPIDATA *rpi_JsonShowEntries(RPIDATA *pstCam, char *pcPath, char *pcFilter, int iBaseIdx, int *piNumFiles)
{
   int      x, iNum=0, iNumFiles;
   char     cForD;
   char    *pcFileName;
   char    *pcFilePath;
   char    *pcTemp;
   void    *pvData;

   pcFileName = (char *) safemalloc(MAX_PATH_LENZ);
   pcFilePath = (char *) safemalloc(MAX_PATH_LENZ);
   pcTemp     = (char *) safemalloc(MAX_PATH_LENZ);
   //
   iNumFiles  = *piNumFiles;
   pvData     = FINFO_GetDirEntries(pcPath, pcFilter, &iNum);
   PRINTF2("rpi-JsonShowEntries():Path=%s:%d entries" CRLF, pcPath, iNum);
   //
   if(iNum > 0)
   {
      for (x=0; x<iNum; x++)
      {
         FINFO_GetFilename(pvData, x, pcFileName, MAX_PATH_LEN);
         GEN_SPRINTF(pcFilePath, "%s%s", pcPath, pcFileName);
         FINFO_GetFileInfo(pcFilePath, FI_FILEDIR, &cForD, 1);
         if(cForD == 'F')
         {
            PRINTF3("rpi-JsonShowEntries(): Filter(%s)-File(%c):[%s]" CRLF, pcFilter, cForD, pcFilePath);
            //
            pstCam = JSON_InsertParameter(pstCam, "Name", &pcFilePath[iBaseIdx], JE_CURLB|JE_TEXT|JE_COMMA);
            //
            FINFO_GetFileInfo(pcFilePath, FI_SIZE, pcTemp, MAX_PATH_LEN);
            pstCam = JSON_InsertParameter(pstCam, "Size", pcTemp, JE_COMMA);
            FINFO_GetFileInfo(pcFilePath, FI_MDATE, pcTemp, MAX_PATH_LEN);
            pstCam = JSON_InsertParameter(pstCam, "Date", pcTemp, JE_TEXT|JE_CURLE|JE_CRLF);
            //
            iNumFiles++;
         }
         else if(cForD == 'D')
         {
            if(pcFileName[0] != '.')
            {
               GEN_SPRINTF(pcFilePath, "%s%s/", pcPath, pcFileName);
               pstCam = rpi_JsonShowEntries(pstCam, pcFilePath, pcFilter, iBaseIdx, &iNumFiles);
            }
         }
         else
         {
            PRINTF3("rpi-JsonShowEntries(): Filter(%s)-(%c):[%s]" CRLF, pcFilter, cForD, pcFilePath);
         }
      }
      FINFO_ReleaseDirEntries(pvData, iNum);
   }
   *piNumFiles  = iNumFiles;
   //
   safefree(pcTemp);
   safefree(pcFileName);
   safefree(pcFilePath);
   return(pstCam);
}

//
// Function:   rpi_ParmSetChanged
// Purpose:    Set the parm changed marker
//
// Parms:      Parm ^, set/unset
// Returns:    TRUE if parm has changed successfully
// Note:       Some parms cannot be changed (NEVER_CHANGED)
//             Some are always volatile (ALWAYS_CHANGED)
//
static bool rpi_ParmSetChanged(const HTTPARGS *pstArgs, bool fChanged)
{
   u_int8     *pubChanged;
   int         iChangedOffset=pstArgs->iChangedOffset;

   switch(iChangedOffset)
   {
      case ALWAYS_CHANGED:
         // Do not alter the change marker
         break;

      case NEVER_CHANGED:
         // Cannot alter the change marker
         if(fChanged)  fChanged = FALSE;
         break;

      default:
         pubChanged = (u_int8 *)pstMap + iChangedOffset;
        *pubChanged = (u_int8)fChanged;
         fChanged   = TRUE;
         break;
   }
   return(fChanged);
}

// 
// Function:   rpi_UpdateRecordingTime
// Purpose:    Update the recording time
// 
// Parameters: 
// Returns:  
// Note:       Updates G_pcRecording
//  
static void rpi_UpdateRecordingTime(void)
{
   u_int32  ulSecs;

   if(pstMap->G_ulRecordingStart != -1)
   {
      ulSecs = GLOBAL_ReadSecs() - pstMap->G_ulRecordingStart;
      RTC_ConvertDateTime(TIME_FORMAT_HH_MM_SS, pstMap->G_pcRecording, ulSecs);
   }
   else
   {
      GEN_SPRINTF(pstMap->G_pcRecording, "--:--:--");
   }
}

#ifdef  FEATURE_JSON_SHOW_REPLY
/*
 * Function    : rpi_ShowNetReply
 * Description : Print out the network json payload reply
 *
 * Parameters  : Title, port, JSON Object
 * Returns     : 
 *
 */
static void rpi_ShowNetReply(const char *pcTitle, int iPort, char *pcData)
{
   LOG_printf("%s(p=%d):Data=%s" CRLF, pcTitle, iPort, pcData);
}
#endif   //FEATURE_JSON_SHOW_REPLY


/*------  Local functions separator ------------------------------------
__CALLBACK_FUNCTIONS(){};
----------------------------------------------------------------------------*/

//
// Function:   http_CollectDebug
// Purpose:    Callback function G_pcDebugMask
//
// Parms:      Parameter Enum
// Returns:    TRUE if OKee
// Note:       
//             
static bool http_CollectDebug(int iId)
{
   //pwjh pstMap->G_stMotionDetect.iDebug = strtoul(pstMap->G_pcDetectDebug, NULL, 16);
   pstMap->G_stMotionDetect.iDebug = atoi(pstMap->G_pcDetectDebug);

   GLOBAL_ConvertDebugMask(TRUE);
   //
   PRINTF1("http_CollectDebug(): ID=%ld" CRLF, iId);
   return(TRUE);
}

//
// Function:   http_MotionChanged
// Purpose:    Callback function G_pcMotion parameters
//
// Parms:      Parameter Enum
// Returns:    TRUE if OKee
// Note:       
//             
static bool http_MotionChanged(int iId)
{
   switch(iId)
   {
      default:
      // case CAM_MOT_PIXELS:
      // case CAM_MOT_LEVEL:
      // case CAM_MOT_SEQ:
      // case CAM_MOT_COLOR:
      // case CAM_MOT_RED:
      // case CAM_MOT_GREEN:
      // case CAM_MOT_BLUE:
      // case CAM_MOT_X:
      // case CAM_MOT_Y:
      // case CAM_MOT_W:
      // case CAM_MOT_H:
         break;

      case CAM_MOT_TRIGGER1:
      case CAM_MOT_TRIGGER2:
         pstMap->G_stMotionDetect.iCounter  = 0;
         pstMap->G_stMotionDetect.iTrigger1 = atoi(pstMap->G_pcDetectTrigger1);
         pstMap->G_stMotionDetect.iTrigger2 = atoi(pstMap->G_pcDetectTrigger2);
         break;

      case CAM_MOT_MODE:
         pstMap->G_stMotionDetect.iCounter = 0;
         pstMap->G_stMotionDetect.iMotMode = RPI_SetMotionMode();

         #ifdef FEATURE_PRINT_MOTION
         printf("http_MotionChanged():CB:%s" CRLF, RPI_GetMotionMode(pstMap->G_stMotionDetect.iMotMode));
         #endif   //FEATURE_PRINT_MOTION
         break;
   }
   PRINTF1("http_MotionChanged(): ID=%ld" CRLF, iId);
   return(TRUE);
}

//
// Function:   http_CollectRcuKey
// Purpose:    Callback function G_pcRemoteKey
//
// Parms:      Parameter Enum
// Returns:    TRUE if found
// Note:       
//             
static bool http_CollectRcuKey(int iId)
{
   bool     fCc=FALSE;
   int      iIdx, iLen;

   for(iIdx=0; iIdx<NUM_RCU_KEYS; iIdx++)
   {
      iLen = GEN_STRLEN(pcRcuKeyLut[iIdx]);
      if(GEN_STRNCMPI(pcRcuKeyLut[iIdx], pstMap->G_pcRemoteKey, iLen) == 0)
      {
         if(GLOBAL_KeyPut(iIdx))
         {
            LOG_Report(0, "FPK", "http_CollectRcuKey(): ID=%ld, Key=%d (%s)", iId, iIdx, pcRcuKeyLut[iIdx]);
            //
            // Key has been put into keybuffer:
            // Notify the host we have a button key
            //
            GLOBAL_Notify(PID_HOST, GLOBAL_BTN_NFY, SIGUSR1);
            fCc = TRUE;
         }
         else
         {
            LOG_Report(0, "FPK", "http_CollectRcuKey(): ID=%ld, Key=%d (%s):BUFFER IS FULL !!", iId, iIdx, pcRcuKeyLut[iIdx]);
         }
         break;
      }
   }
   return(fCc);
}

//
// Function:   http_MailChanged
// Purpose:    Callback function Mail settings
//
// Parms:      Parameter Enum
// Returns:    TRUE if found
// Note:       
//             
static bool http_MailChanged(int iId)
{
   //
   // A mail parameter has changed: notify the owner (CMD thread)
   //
   pstMap->G_iChangedParm = iId;
   GLOBAL_Notify(PID_CMND, GLOBAL_SVR_RUN, SIGUSR1);
   //
   // SIGNAL changed parameter
   //
   return(TRUE);
}

/*------  Local functions separator ------------------------------------
__DELETED_FUNCTIONS(){};
----------------------------------------------------------------------------*/

#ifdef COMMENT
//
// Function:   rpi_JsonShowFiles
// Purpose:    Retrieve all files in directory from disk
//
// Parms:      Json object, www dir name, filter, YesNo:fullpath
// Returns:    Json object
// Note:       
//             
static RPIDATA *rpi_JsonShowFiles(RPIDATA *pstCam, char *pcWww, char *pcFilter, bool fFullpath)
{
   int      x, iNum=0, iNumFiles=0;
   char     cForD;
   char    *pcFileName;
   char    *pcFullPath;
   char    *pcFilePath;
   char    *pcTemp;
   void    *pvData;

   pcFileName = (char *) safemalloc(MAX_PATH_LEN);
   pcFilePath = (char *) safemalloc(MAX_PATH_LEN);
   pcFullPath = (char *) safemalloc(MAX_PATH_LEN);
   pcTemp     = (char *) safemalloc(MAX_PATH_LEN);
   //
   GEN_SPRINTF(pcFullPath, "%s%s", pstMap->G_pcWwwDir, pcWww);
   PRINTF1("rpi-JsonShowFiles(): Path = %s" CRLF, pcFullPath);

   pvData = FINFO_GetDirEntries(pcFullPath, pcFilter, &iNum);

   if(iNum > 0)
   {
      for (x=0; x<iNum; x++)
      {
         FINFO_GetFilename(pvData, x, pcFileName, MAX_PATH_LEN);
         GEN_SPRINTF(pcFilePath, "%s%s", pcFullPath, pcFileName);
         FINFO_GetFileInfo(pcFilePath, FI_FILEDIR, &cForD, 1);
         if(cForD == 'F')
         {
            PRINTF3("rpi-JsonShowFiles(): Filter(%s)-File(%c):[%s]" CRLF, pcFilter, cForD, pcFilePath);
            // { "name":"20130712xxx",
            if(fFullpath)  pstCam = JSON_InsertParameter(pstCam, "Name", pcFilePath, JE_CURLB|JE_TEXT|JE_COMMA);
            else           pstCam = JSON_InsertParameter(pstCam, "Name", pcFileName, JE_CURLB|JE_TEXT|JE_COMMA);
            // "Size":12345,
            FINFO_GetFileInfo(pcFilePath, FI_SIZE, pcTemp, MAX_PATH_LEN);
            //PRINTF1("rpi-JsonShowFiles(): Size=[%s]" CRLF, pcTemp);
            pstCam = JSON_InsertParameter(pstCam, "Size", pcTemp, JE_COMMA);
            // "Date":"xxxxx xx"},
            FINFO_GetFileInfo(pcFilePath, FI_MDATE, pcTemp, MAX_PATH_LEN);
            //PRINTF1("rpi-JsonShowFiles(): Date=[%s]" CRLF, pcTemp);
            pstCam = JSON_InsertParameter(pstCam, "Date", pcTemp, JE_TEXT|JE_CURLE|JE_CRLF);
            iNumFiles++;
         }
         else if(cForD == 'D')
         {

         PRINTF2("rpi-JsonShowFiles(): Filter(%s)-Dir=[%s]" CRLF, pcFilter, pcFileName);
         }
      }
      FINFO_ReleaseDirEntries(pvData, iNum);
   }
   else
   {
      PRINTF("rpi-JsonShowFiles(): No file/dir entries" CRLF);
   }
   GEN_SPRINTF(pstMap->G_pcNumFiles, "%d", iNumFiles);
   safefree(pcFileName);
   safefree(pcFilePath);
   safefree(pcFullPath);
   safefree(pcTemp);
   return(pstCam);
}

#endif
