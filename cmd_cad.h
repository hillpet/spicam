/*  (c) Copyright:  2013..2017  Patrn.nl, Confidential Data
 *
 *  Workfile:           cmd_cad.h
 *  Revision:  
 *  Modtime:   
 *
 *  Purpose:            cmdcad.c header file
 *
 *  Compiler/Assembler: Raspbian Linux GNU gcc
 *  Ext Packages:
 * 
 *  Author:             Peter Hillen
 *  Date Created:       25 Dec 2013
 * 
 *  Revisions:
 * 
 * 
 *
 * 
 * 
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
**/

#ifndef _CMD_CAD_H_
#define _CMD_CAD_H_

//
// Global prototypes
//
void     CAD_Backlight        (int);
void     CAD_Exit             (void);
void     CAD_Cursor           (u_int8, u_int8, bool);
void     CAD_SetDisplay       (u_int8, u_int8);
int      CAD_DisplayRows      (void);
int      CAD_DisplayCols      (void);
int      CAD_DisplayMode      (int);
bool     CAD_Init             (void);
void     CAD_Text             (u_int8, u_int8, char *);
void     CAD_WriteLine        (u_int8, u_int8, char *);

#endif  /*_CMD_CAD_H_ */

