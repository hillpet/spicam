/*  (c) Copyright:  2017  Patrn, Confidential Data
 *
 *  Workfile:           cam_page.h
 *  Revision:          
 *  Modtime:           
 *
 *  Purpose:            Headerfile for cam_page.c
 *
 *
 *
 *  Compiler/Assembler: Raspbian Linux GNU gcc
 *  Ext Packages:
 *
 *  Author:             Peter Hillen
 *  Date Created:       16 Mar 2017
 *
 *  Revisions:
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
**/

#ifndef _CAM_PAGE_H_
#define _CAM_PAGE_H_

typedef struct _campage_
{
   int            iCommand;
   int            iFunction;
   char          *pcCommand;
   PFCMD          pfCmdCb;
}  CAMPAGE;
//
int   CAM_FindDynamicPage           (const char *);
int   CAM_InitDynamicPages          (void);
bool  CAM_DynamicPageHandler        (GLOCMD *, int);


#endif /* _CAM_PAGE_H_ */
