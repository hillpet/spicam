/*  (c) Copyright:  2013..2021  Patrn, Confidential Data
 *
 *  Workfile:           globals.h
 *  Revision:          
 *  Modtime:           
 *
 *  Purpose:            Define the mmap variables, common over all threads. If
 *                      the structure needs to change, roll the revision number to make sure a new mmap file is created !
 *                     
 * 
 *  Compiler/Assembler: Raspbian Linux GNU gcc
 *  Ext Packages:
 * 
 *  Author:             Peter Hillen
 *
 *  Revisions:
 *    27 May 2013       Created
 *    05 Jul 2018       Add Global RCU/Button key stack 
 *    14 May 2021       Add G_stLog; Make pstMap global
 *
 *
 * 
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
**/

#ifndef _GLOBALS_H_
#define _GLOBALS_H_

#include <semaphore.h>
#include <pthread.h>
#include <sys/types.h>
#include <sys/mman.h>
#include <netinet/in.h>
//
//
// Enums and dynamic URL's
//
enum
{
   #define  EXTRACT_DYN(a,b,c,d,e,f)   a,
   #include "dyn_host.h"
   #include "dyn_cam.h"
   #include "dyn_rcu.h"
   #include "dyn_proxy.h"
   #include "dyn_io.h"
   #undef EXTRACT_DYN
   //
   NUM_DYN_PAGES
};
//
// Enums PIDs
//
typedef enum _proc_pids_
{
   #define  EXTRACT_PID(a,b,c)   a,
   #include "par_pids.h"
   #undef EXTRACT_PID
   //
   NUM_PIDT
}  PIDT;
//
typedef struct _proc_pidlist_
{
   pid_t       tPid;
   int         iFlag;
   int         iNotify;
   const char *pcName;
   const char *pcHelp;
}  PIDL;
//
//
// Apps functions/commands
//
//
typedef enum _global_cmd_
{
   #define  EXTRACT_CMD(a,b,c)   a,
   #include "cmd_defs.h"
   #undef EXTRACT_CMD
   //
   NUM_CAM_CMDS                        //
}  ENCMD;
//
#define  ONEMILLION                       1000000L
#define  ONEBILLION                       1000000000L
//
#define  MOTION_VIDEO_CAPTURE_SECS        2              // extra recording time after MD in secs
#define  CLUSTER_MIN_SIZE                 5              // MD clusters must be of certain size
#define  CLUSTER_STRENGTH_COUNT           5              // MD cluster strength nr of MVs
#define  CLUSTER_REMAINING_COUNT          5              // MD remaining clusters
#define  CLUSTER_STRENGTH                 75             // % MD cluster strength
//
#define  MOTION_WAIT_TIMEOUT              5000           // max 5 secs timeout
#define  MOTION_WAIT_MSECS                100            // poll delay in msecs
//
#define  GUARD_COUNT                      3
#define  GUARD_SECS                       (30*60)
#define  GUARD_SECS_RETRY                 ( 2*60)
#define  GUARD_SECS_DEBUG                 ( 1*60)
//
#define  MAX_DYN_PAGES                    64
//
#define  MAX_CMD_LEN                      200
#define  MAX_ARG_LEN                      1000
#define  MAX_CMDLINE_LEN                  (MAX_CMD_LEN + MAX_ARG_LEN)
//
#define  MAX_TEMP_LEN                     80
#define  MAX_NUM_ALARMS                   16
//
// Global area array sizes
//
#define  MAX_PATH_LEN                     499
#define  MAX_PATH_LENZ                    MAX_PATH_LEN+1
#define  MAX_URL_LEN                      31
#define  MAX_URL_LENZ                     MAX_URL_LEN+1
#define  MAX_ADDR_LEN                     INET_ADDRSTRLEN
#define  MAX_ADDR_LENZ                    INET_ADDRSTRLEN+1
#define  MAX_PARM_LEN                     15
#define  MAX_PARM_LENZ                    MAX_PARM_LEN+1
#define  MAX_MAIL_LEN                     31
#define  MAX_MAIL_LENZ                    MAX_MAIL_LEN+1
#define  MAX_PASS_LEN                     127
#define  MAX_PASS_LENZ                    MAX_PASS_LEN+1
//
// Virtual LCD:
//
typedef enum _lcdcmd_
{
   LCD_CMD_NONE   = 0,
   LCD_CMD_HOME,
   LCD_CMD_CLEAR,
   LCD_CMD_END,
   //
   NUM_LCD_CMDS
}  LCD_CMD;

#define  LCD_VIRTUAL_ROWS     80          // 25
#define  LCD_VIRTUAL_COLS     120         // 80
#define  LCD_VIRTUAL_LOG      14
#define  LCD_VIRTUAL_LOG_LINES 5
//
#define  LCD_BACKLIGHT_ON     10
#define  LCD_SCROLL_COL       16
#define  LCD_MODE_STACK_SIZE  10
//
// Buttons and RCU Key defines
//
#define  KEY_BUFFER_SIZE      8
#define  MAX_NUM_BUTTONS      8
//
// GLOBAL Notification:
// Init notifications  0x0000.0FFF
//
#define  GLOBAL_SVR_INI 0x00000001        // Ini: HTTP Server
#define  GLOBAL_CAM_INI 0x00000002        // Ini: Camera
#define  GLOBAL_CMD_INI 0x00000004        // Ini: Cmd handler
#define  GLOBAL_KOD_INI 0x00000008        // Ini: KODI RCU
#define  GLOBAL_PRX_INI 0x00000010        // Ini: Proxy server
#define  GLOBAL_SUP_INI 0x00000020        // Ini: Support
#define  GLOBAL_UDM_INI 0x00000040        // Ini: USB Daemon
#define  GLOBAL_UDB_INI 0x00000080        // Ini: USB Debug
#define  GLOBAL_RCU_INI 0x00000100        // Ini: RCU
#define  GLOBAL_LCD_INI 0x00000200        // Ini: LCD
#define  GLOBAL_BTN_INI 0x00000400        // Ini: Button handler
#define  GLOBAL_XXX_INI 0x000007FF        // Init Mask
//
// GLOBAL Notification:
// Activations:
//
#define  GLOBAL_SVR_RUN 0x00001000        // Server  Run    
#define  GLOBAL_RPI_END 0x00002000        // RPI     Notify Exit
#define  GLOBAL_RPI_RST 0x00004000        // RPI     Restart
#define  GLOBAL_RPI_RBT 0x00008000        // RPI     Reboot
#define  GLOBAL_CAM_RUN 0x00010000        // CAM     Run   
#define  GLOBAL_HLP_RUN 0x00020000        // Helper  Run
#define  GLOBAL_ALM_RUN 0x00040000        // Alarm   Run
#define  GLOBAL_EML_RUN 0x00080000        // Email   Run
#define  GLOBAL_GRD_RUN 0x00100000        // Guard   Run
#define  GLOBAL_CMD_RUN 0x00200000        // CMD     Run   
#define  GLOBAL_XXX_RUN 0x00FFF000        // Run Mask
//
// GLOBAL Notification:
// Completion notifications:
//
#define  GLOBAL_CAM_NFY 0x01000000        // CAM     Notify
#define  GLOBAL_CMD_NFY 0x02000000        // CMD     Notify
#define  GLOBAL_RCU_NFY 0x04000000        // RCU key Notify
#define  GLOBAL_PRX_NFY 0x08000000        // Proxy   Notify
#define  GLOBAL_ALM_NFY 0x10000000        // Alarm   Notify
#define  GLOBAL_FLT_NFY 0x20000000        // Seg Flt Notify
#define  GLOBAL_BTN_NFY 0x40000000        // Button  Notify
#define  GLOBAL_GRD_NFY 0x80000000        // Guard   Notify
#define  GLOBAL_XXX_NFY 0xFF000000        // Notify Mask
//
// Define the debug mask bits
//
#define  DEBUG_FILE           0x80000000L
//
// Global debug filter bits
//
#define  DEBUG_NET            0x00000001L
#define  DEBUG_CAM            0x00000010L
#define  DEBUG_CMD            0x00000100L
#define  DEBUG_RCU            0x00001000L
#define  DEBUG_USB            0x00010000L
#define  DEBUG_USD            0x00100000L
//
// Remote IO bits
//
#define  RIO_ERR              0x00008000L
#define  RIO_RE1              0x00000100L
#define  RIO_RE2              0x00000200L
#define  RIO_SG1              0x00000001L
#define  RIO_SG2              0x00000002L
#define  RIO_SG3              0x00000004L
#define  RIO_SG4              0x00000008L
#define  RIO_SG5              0x00000010L
#define  RIO_OFF              0x00000000L
//
#define  CAM____        0x00000000     // No command
//
// HTTP Functions (callback through HTML or JSON)
//
#define  CAM_STI        0x00000001     // Snapshot foto
#define  CAM_TIM        0x00000002     // Timelapse foto's
#define  CAM_REC        0x00000004     // Video recording
#define  CAM_STR        0x00000008     // Video streaming
#define  CAM_PIX        0x00000010     // List of pictures
#define  CAM_VID        0x00000020     // List of Videos
#define  CAM_PRM        0x00000040     // List of Parameters
#define  CAM_RMP        0x00000080     // Remove fotos
#define  CAM_RMV        0x00000100     // Remove videos
#define  CAM_DEF        0x00000200     // Defaults
#define  CAM_STP        0x00000400     // Stop
#define  CAM_ALM        0x00000800     // Alarm timer
#define  CAM_VEC        0x00001000     // Video motion vectors
#define  CAM_ERR        0x00008000     // Error replies
#define  CAM_ALL        0x0000FFFF     // All HTTP functions
//
// Internal Functions (callback through SIGUSR1=10/SIGUSR2=12)
//
#define  CAM_UNM        0x01000000     // Unmount
#define  CAM_PAT        0x02000000     // Mode pattern changed
#define  CAM_KEY        0x04000000     // RCU IR keypress
#define  CAM_BUT        0x08000000     // Button changed
//
// JSON markers for text-style-vars ("value" = "123456")
//             or number-style-vars ("value" =  123456 )
//
#define  JSN_TXT        0x10000000     // JSON Variable is text
#define  JSN_INT        0x20000000     // JSON Variable is integer
#define  WB             0x80000000     // Warm boot var
//
#define  ALWAYS_CHANGED -1
#define  NEVER_CHANGED  -2
//
//=============================================================================
// GLOBAL MMAP structure pointer
//=============================================================================
#ifdef DEFINE_GLOBALS
   #define EXTERN
#else
   #define EXTERN extern
#endif

typedef u_int8 (*PFKEY)(u_int8);
//
typedef struct glocmd
{
   int      iStatus;                      // CAM_STATUS_...          CMD Camera status (RECORDING, STREAMING, ..)
   int      iError;                       // CAM_STATUS_...          Generic error conditions
   int      tUrl;                         // CAM_DYN_HTML_INFO, ...  Dynamic Page URL
   int      iCommand;                     // CAM_CMD_xxx, ...        Page command
   int      iArgs;                        // CAM_xxx                 Page argument type
   int      iData1;                       //   ..misc
   int      iData2;                       //   ..misc
   int      iData3;                       //   ..misc
   //                                     //
   bool   (*pfCallback)(struct glocmd *); // Completion callback function 
   //                                     //
   char     pcExec[MAX_CMD_LEN];          // tools pathnames
   char     pcArgs[MAX_ARG_LEN];          // tools arguments
}  GLOCMD;
//
typedef bool (*PFCMD) (GLOCMD *);
typedef bool (*PFCMDI)(GLOCMD *, int);
//
typedef struct _cmd_dynpage_
{
   int         tUrl;
   FTYPE       tType;
   int         iTimeout;
   int         iFlags;
   const char *pcUrl;
   PFCMD       pfDynCb;
}  CMDDYN;
//
typedef struct _cmdi_dynpage_
{
   int         tUrl;
   FTYPE       tType;
   int         iTimeout;
   int         iFlags;
   const char *pcUrl;
   PFCMDI      pfDynCb;
}  CMDIDYN;
//
#define  CAM_DYN_REJECTED  -2          // CAM command rejected
#define  CAM_DYN_ERROR     -3          // CAM command error
//
//
#ifdef   FEATURE_SHOW_VARS
void     GLOBAL_DumpVars(const char *);
#define  GLOBAL_DUMPVARS(x)            GLOBAL_DumpVars(x)
#else    //FEATURE_SHOW_VARS
#define  GLOBAL_DUMPVARS(x)
#endif   //FEATURE_SHOW_VARS
//
#ifdef   FEATURE_SHOW_COMMANDS
void     GLOBAL_DumpCommand(const char *, GLOCMD *);
#define  GLOBAL_DUMPCOMMAND(x,y)       GLOBAL_DumpCommand(x,y)
#else    //FEATURE_SHOW_COMMANDS
#define  GLOBAL_DUMPCOMMAND(x,y)
#endif   //FEATURE_SHOW_COMMANDS
//
// Global parms
//
typedef enum _global_opt_
{
   GLOOPT_NONE = 0,                 // 
   GLOOPT_SWITCH_ON,                // Option is flag and  ON
   GLOOPT_SWITCH_OFF,               //                     OFF
   GLOOPT_VALUE_ON,                 //           value and present
   GLOOPT_VALUE_OFF,                //                     not present
}  GLOOPT;

typedef enum _global_parameters_
{
#define  EXTRACT_PAR(a,b,c,d,e,f,g,h,i,j,k)    a,
#include "par_defs.h"
#undef EXTRACT_PAR
   NUM_GLOBAL_DEFS
}  GLOPAR;
//
typedef struct _par_args_
{
   int            iFunction;
   const char    *pcJson;
   const char    *pcHtml;
   const char    *pcOption;
   int            iValueOffset;
   int            iValueSize;
   int            iChangedOffset;
}  PARARGS;
//
typedef struct _globals_defaults_
{
   int         iFunction;
   const char *pcDefault;
   int         iChanged;
   int         iChangedOffset;
   int         iValueOffset;
   int         iGlobalSize;
}  GLODEFS;
//
// Alarm flags
//
#define ALARM_ARMED        0x00000001
#define ALARM_USED         0x00000002
#define ALARM_EMAIL        0x00000004
//
typedef struct _alarm_
{
   int      iFlags;
   int      iSpare;
   char     pcCommand[MAX_PARM_LEN];
   char     pcDandT[MAX_PARM_LEN];
   int      iRepTime;
   int      iRepNum;
   u_int32  ulRepNext;
   int      iRepCount;
}  ALARM;
//
// Generic status strings/enums
//
typedef enum _camstat_
{
#define  EXTRACT_ST(a,b)   a,
#include "gen_stats.h"
#undef   EXTRACT_ST
   //
   NUM_CAM_STATUS,
   CAM_STATUS_ASK                   // Return current status
}  CAMSTAT;
//
typedef struct remio
{
   int         iIdx;                // IO Index 0..?
   GLOPAR      eParm;               // Global parameter enum
   int         iMask;               // Mask
   int         iBitNr;              // Parameter
   int         (*pfRemIo)(const struct remio *, int);
}  REMIO;
//
//
typedef struct _keylut_
{
   u_int8   ubKey;
   PFKEY    pfCallback;
}  KEYLUT;
//
typedef struct _globut_
{
   u_int8      ubId;                // Physical Key ID: BTN_1...BTN_8 or 0
   u_int8      ubKey;               // Logical  Key ID: BTN_KEY1...
   bool        fChanged;            // Key changed yesno
   int         iStatus;             // Key status CAM_STATUS_KEYPRESS, ...
   PFKEY       pFuncButton;         // Key handler
   bool        fRepeat;             // Key pressed repeated
   u_int32     ulImage;             // Key pressed/depressed shift image
   u_int32     ulLastImage;         // Key last pressed/depressed shift image
   u_int32     ulDebounce;          // Key debounce mask
}  GLOBUT;
//
typedef struct _vlcd_
{
   bool     fLcdCursor;
   int      iLcdCmd;
   int      iLcdMode;
   int      tLcdHandle;
   u_int8   ubVirtRows, ubVirtCols;
   u_int8   ubVirtRow,  ubVirtCol;
   u_int8   ubCursRow,  ubCursCol;
   //
   char     pcVirtLcd[LCD_VIRTUAL_ROWS*LCD_VIRTUAL_COLS];
}  VLCD;
//
// Define argument structure to streamer apps
//
typedef struct sarg
{
   int         iLenArgs;
   const char *pcArg;
   char       *(*pfGetArg)(const struct sarg *);
}  SARG;
//
typedef enum _varreset_
{
   VAR_COLD    = 0,                 // Cold reset
   VAR_WARM,                        // Warm reset
   VAR_UPDATE,                      // Update

   NUM_VARRESETS
}  VARRESET;
//
typedef enum _mapstate_
{
  MAP_CLEAR = 0,                    // Clear all the mapped memory
  MAP_SYNC,                         // Synchronize the map
  MAP_RESTART,                      // Restart the counters using the last stored data

  NUM_MAPSTATES
} MAPSTATE;
//
#define MOTION_GRID_COUNT     10
#define MOTION_COLORS         5
//
// Motion detection modes
//
enum
{
   MOTION_MODE_OFF = 0,                   // Off
   MOTION_MODE_MANUAL,                    // Manual detection
   MOTION_MODE_AUGMENT,                   // Augment MVs onto picture
   MOTION_MODE_CYCLE,                     // Cycle contineously
   MOTION_MODE_PICTURE,                   // Cycle + picture snapshot
   MOTION_MODE_VIDEO,                     // Cycle + video   snapshot
   MOTION_MODE_STREAM,                    // Cycle + stream  snapshot
   //
   NUM_MOTION_MODES
};
//
#define  MM_DEBUG_FILE_CSV_SRT   0x0001   //    1 Sorted MV Frames - count, y, x, sad, VectY, VectX, Dir
#define  MM_DEBUG_FILE_CSV_ALL   0x0002   //    2 Put out MV data
#define  MM_DEBUG_FILE_CSV_UNS   0x0004   //    4 Unsorted ALL Frames - count, y, x, sad, VectY, VectX, Dir
#define  MM_DEBUG_FILE           0x0007   //      All Timestamped file out
#define  MM_DEBUG_FILE_READ_MV   0x0008   //    8 Read MV frames from binary file
#define  MM_DEBUG_OUT_LCD        0x0010   //   16 VirtLCD with MV data
#define  MM_DEBUG_FILE_MVS       0x0020   //   32 Write all MV structs binary
#define  MM_DEBUG_ONE_SHOT       0x0040   //   64 Exit always after 1 frame
#define  MM_DEBUG_FILE_RGB       0x0080   //  128 Write Graphics RGB levels
#define  MM_DEBUG_MASK           0x0100   //  256 Mask motion vector frame
//
typedef struct _mdet_
{
   int         iMotMode;                  // Detect motion mode
   int         iTrigger1;                 // Detect trigger 1 (default #MVs)
   int         iTrigger2;                 // Detect trigger 2 (default Vector size)
   int         iCounter;                  // Detect counter
   int         iDebug;                    // Detect debug
   int         iCluster;                  // Detect motion LARGE cluster
   int         iSeq;                      // Detect sequence 0..1
   int         iZoomX;                    // Zoom Column
   int         iZoomY;                    // Zoom Line
   int         iZoomW;                    // Zoom Width
   int         iZoomH;                    // Zoom Height
   int         iAverage1;                 // Graylevel average picture 1
   int         iAverage2;                 // Graylevel average picture 2
   int         iThldRed;                  // Pixel threshold RED
   int         iThldGreen;                // Pixel threshold GREEN
   int         iThldBlue;                 // Pixel threshold BLUE
   int         iThldPixel;                // Pixel motion threshold
   int         iThldLevel;                // Motion average percentage
   //
   int         iMvTiny;                   // Mv motion Tiny
   int         iMvSmall;                  // Mv motion Small
   int         iMvMedium;                 // Mv motion Medium
   int         iMvLarge;                  // Mv motion Large
   int         iMvHuge;                   // Mv motion Huge
   //
   char        pcFormat[MOTION_COLORS];   // ColorFormat ("RGB", "I", ...)
   //
   u_int32     ulGridMotion[MOTION_GRID_COUNT][MOTION_GRID_COUNT];
   u_int16     usGridScales[MOTION_GRID_COUNT][MOTION_GRID_COUNT];
   u_int16     usGridCounts[MOTION_GRID_COUNT][MOTION_GRID_COUNT];
}  MDET;
//
typedef struct _rpidata_
{
   char    *pcObject;
   FTYPE    tType;
   int      iIdx;
   int      iSize;
   int      iFree;
   int      iLastComma;
}  RPIDATA;

//
// Define flags for dynamic URLs
//
#define  DYN_FLAG_NONE                 0
#define  DYN_FLAG_PORT                 0x0001
//
// Dynamic page registered URLs
//
typedef bool(*PFVOIDPI)(void *, int);
typedef struct _dynr_
{
   int         tUrl;                   // RPI_DYN_HTML_EMPTY, ...
   FTYPE       tType;                  // HTTP_JSON, HTTP_...
   int         ePid;                   // Owner pid enum PID_xxxx
   int         iTimeout;               // Estimated call duration 
   int         iPort;                  // Port
   char        pcUrl[MAX_URL_LENZ];    // URL
   PFVOIDPI    pfCallback;             // Callback function
}  DYNR;

//=============================================================================
// Global parameters mapped to a shared MMAP file 
//=============================================================================
typedef struct _rpimap_
{
   int               G_iSignature1;
   char              G_iVersionMajor;
   char              G_iVersionMinor;
   char              G_pcHostname         [MAX_URL_LENZ];
   u_int32           G_ulStartTimestamp;
   u_int32           G_ulRecordingStart;
   //
   // Misc semaphores
   //
   sem_t             G_tSemRpi;
   sem_t             G_tSemSvr;
   sem_t             G_tSemCam;
   sem_t             G_tSemCmd;
   sem_t             G_tSemBtn;
   sem_t             G_tSemLcd;
   sem_t             G_tSemFrb;
   sem_t             G_tSemSup;
   sem_t             G_tSemPrx;
   //
   pthread_mutex_t   G_tMutex;
   //
   int               G_iNumAlarms;
   ALARM             G_stAlarms           [MAX_NUM_ALARMS];
   MDET              G_stMotionDetect;
   VLCD              G_stVirtLcd;
   //
   //=============== Start WB reset area ======================================
   int               G_iResetStart;
   //
   bool              G_fFakeExec;
   u_int32           G_ulDebugMask;
   u_int32           G_ulSecondsCounter;
   int               G_iNumDynPages;
   int               G_iCurDynPage;
   int               G_iMallocs;
   int               G_iSegFaultLine;
   int               G_iSegFaultPid;
   //
   int               G_iHttpStatus;
   int               G_iTraceCode;
   int               G_iNotify;
   int               G_iGuards;   
   int               G_iRemoteKeys[KEY_BUFFER_SIZE];   
   int               G_iRemoteKeyIdx;
   int               G_iLcdBacklight;
   int               G_iChangedParm;
   //
   DYNR              G_stDynPages         [MAX_DYN_PAGES];
   GLOCMD            G_stCmd;
   GLOBUT            G_stButtons          [NUM_RCU_KEYS];
   PIDL              G_stPidList          [NUM_PIDT];
   GLOG              G_stLog;
   //
   //--------------------------------------------------------------------------
   // Take new power-on reset parms from this pool:
   //--------------------------------------------------------------------------
   #define SPARE_POOL_ZERO 100
   #define EXTRA_POOL_ZERO 0
   //
   char              G_pcSparePoolZero    [SPARE_POOL_ZERO-EXTRA_POOL_ZERO];
   //
   // Extra from zero-init pool
   //

   //--------------------------------------------------------------------------
   int               G_iResetEnd;
   //=============== End WB reset area ========================================
   //
   char              G_pcDebugMask        [MAX_PARM_LENZ];
   char              G_pcCommand          [MAX_PARM_LENZ];
   char              G_pcMyIpIfce         [MAX_PARM_LENZ];
   char              G_pcMyIpAddr         [MAX_ADDR_LENZ];
   char              G_pcIpAddr           [MAX_ADDR_LENZ];
   char              G_pcWanAddr          [MAX_ADDR_LENZ];
   char              G_pcSrvrPort         [MAX_PARM_LENZ];
   char              G_pcDefault          [MAX_PATH_LENZ];
   char              G_pcPixPath          [MAX_PATH_LENZ];
   char              G_pcVideoPath        [MAX_PATH_LENZ];
   char              G_pcFilter           [MAX_PATH_LENZ];
   char              G_pcNumFiles         [MAX_PARM_LENZ];
   char              G_pcStatus           [MAX_PARM_LENZ];
   char              G_pcDelete           [MAX_PARM_LENZ];
   char              G_pcRecording        [MAX_PARM_LENZ];
   char              G_pcRemoteKey        [MAX_PATH_LENZ];
   char              G_pcSegFaultFile     [MAX_URL_LENZ];
   //
   // Raspi streaming args
   //
   char              G_pcStreamFps        [MAX_PARM_LENZ];
   char              G_pcStreamAddr       [MAX_ADDR_LENZ];
   char              G_pcStreamPort       [MAX_PARM_LENZ];
   char              G_pcKodiPort         [MAX_PARM_LENZ];
   //
   // Raspi device options:
   //
   char              G_pcWidth            [MAX_PARM_LENZ];
   char              G_pcHeight           [MAX_PARM_LENZ];
   char              G_pcFlipH            [MAX_PARM_LENZ];
   char              G_pcFlipV            [MAX_PARM_LENZ];
   char              G_pcPixFileExt       [MAX_PARM_LENZ];
   char              G_pcPixDelay         [MAX_PARM_LENZ];
   char              G_pcTimNumber        [MAX_PARM_LENZ];
   char              G_pcTimDuration      [MAX_PARM_LENZ];
   char              G_pcTimelapse        [MAX_PARM_LENZ];
   char              G_pcNoPreview        [MAX_PARM_LENZ];
   char              G_pcPixQuality       [MAX_PARM_LENZ];
   char              G_pcSharpness        [MAX_PARM_LENZ];
   char              G_pcContrast         [MAX_PARM_LENZ];
   char              G_pcBrightness       [MAX_PARM_LENZ];
   char              G_pcSaturation       [MAX_PARM_LENZ];
   char              G_pcExposure         [MAX_PARM_LENZ];
   char              G_pcAutoWB           [MAX_PARM_LENZ];
   char              G_pcImgEffects       [MAX_PARM_LENZ];
   char              G_pcColEffects       [MAX_PARM_LENZ];
   char              G_pcMeterMode        [MAX_PARM_LENZ];
   char              G_pcRotation         [MAX_PARM_LENZ];
   char              G_pcVideoStab        [MAX_PARM_LENZ];
   char              G_pcRawBanner        [MAX_PARM_LENZ];
   char              G_pcEvCompensation   [MAX_PARM_LENZ];
   char              G_pcShutterspeed     [MAX_PARM_LENZ];
   char              G_pcProfile          [MAX_PARM_LENZ];
   //
   char              G_pcBitrate          [MAX_PARM_LENZ];
   char              G_pcFullscreen       [MAX_PARM_LENZ];
   char              G_pcGop              [MAX_PARM_LENZ];
   char              G_pcIso              [MAX_PARM_LENZ];
   char              G_pcOpacity          [MAX_PARM_LENZ];
   char              G_pcPreview          [MAX_PARM_LENZ];
   char              G_pcRoi              [MAX_PARM_LENZ];
   char              G_pcThumbnail        [MAX_PARM_LENZ];
   char              G_pcVerbose          [MAX_PARM_LENZ];
   char              G_pcDemo             [MAX_PARM_LENZ];
   char              G_pcWrap             [MAX_PARM_LENZ];
   char              G_pcSegment          [MAX_PARM_LENZ];
   char              G_pcPenc             [MAX_PARM_LENZ];
   char              G_pcAnnotate         [MAX_PARM_LENZ];
   char              G_pcAnnotateEx       [MAX_PARM_LENZ];
   //
   char              G_pcWwwDir           [MAX_PATH_LENZ];
   char              G_pcRamDir           [MAX_PATH_LENZ];
   char              G_pcLastFile         [MAX_PATH_LENZ];
   char              G_pcLastFileSize     [MAX_PARM_LENZ];
   //
   char              G_pcAlarmCommand     [MAX_PARM_LENZ];
   char              G_pcAlarmDandT       [MAX_PARM_LENZ];
   char              G_pcAlarmRep         [MAX_PARM_LENZ];
   char              G_pcAlarmNum         [MAX_PARM_LENZ];
   char              G_pcVersionSw        [MAX_PARM_LENZ];
   char              G_pcRecDuration      [MAX_PARM_LENZ];
   char              G_pcDetectMode       [MAX_PARM_LENZ];
   char              G_pcDetectCount      [MAX_PARM_LENZ];
   char              G_pcDetectTrigger1   [MAX_PARM_LENZ];
   char              G_pcDetectTrigger2   [MAX_PARM_LENZ];
   char              G_pcDetectDebug      [MAX_PARM_LENZ];
   char              G_pcDetectCluster    [MAX_PARM_LENZ];
   char              G_pcDetectPixel      [MAX_PARM_LENZ];
   char              G_pcDetectLevel      [MAX_PARM_LENZ];
   char              G_pcDetectColor      [MAX_PARM_LENZ];
   char              G_pcDetectSeq        [MAX_PARM_LENZ];
   char              G_pcDetectRed        [MAX_PARM_LENZ];
   char              G_pcDetectGreen      [MAX_PARM_LENZ];
   char              G_pcDetectBlue       [MAX_PARM_LENZ];
   char              G_pcDetectZoomX      [MAX_PARM_LENZ];
   char              G_pcDetectZoomY      [MAX_PARM_LENZ];
   char              G_pcDetectZoomW      [MAX_PARM_LENZ];
   char              G_pcDetectZoomH      [MAX_PARM_LENZ];
   char              G_pcDetectAvg1       [MAX_PARM_LENZ];
   char              G_pcDetectAvg2       [MAX_PARM_LENZ];
   char              G_pcMvTiny           [MAX_PARM_LENZ];
   char              G_pcMvSmall          [MAX_PARM_LENZ];
   char              G_pcMvMedium         [MAX_PARM_LENZ];
   char              G_pcMvLarge          [MAX_PARM_LENZ];
   char              G_pcMvHuge           [MAX_PARM_LENZ];
   //
   char              G_pcRemoteIo         [MAX_PARM_LENZ];
   char              G_pcRemoteIp         [MAX_ADDR_LENZ];
   char              G_pcRemoteOn1        [MAX_PARM_LENZ];
   char              G_pcRemoteOn2        [MAX_PARM_LENZ];
   char              G_pcRemoteOn3        [MAX_PARM_LENZ];
   //
   // Email user/passw/server data
   //
   char              G_pcPop3User         [MAX_MAIL_LENZ];
   char              G_pcPop3Pass         [MAX_PASS_LENZ];
   char              G_pcPop3Server       [MAX_MAIL_LENZ];
   char              G_pcPop3Port         [MAX_PARM_LENZ];
   //
   char              G_pcSmtpUser         [MAX_MAIL_LENZ];
   char              G_pcSmtpPass         [MAX_PASS_LENZ];
   char              G_pcSmtpServer       [MAX_MAIL_LENZ];
   char              G_pcSmtpPort         [MAX_PARM_LENZ];
   //
   // Changed markers
   //
   u_int8            G_ubCommandChanged;
   u_int8            G_ubIpAddrChanged;
   u_int8            G_ubPixPathChanged;
   u_int8            G_ubVideoPathChanged;
   u_int8            G_ubStreamFpsChanged;
   u_int8            G_ubWidthChanged;
   u_int8            G_ubHeightChanged;
   u_int8            G_ubFlipHChanged;
   u_int8            G_ubFlipVChanged;
   u_int8            G_ubPixFileExtChanged;
   u_int8            G_ubPixDelayChanged;
   u_int8            G_ubTimNumberChanged;
   u_int8            G_ubTimelapseChanged;
   u_int8            G_ubNoPreviewChanged;
   u_int8            G_ubPixQualityChanged;
   u_int8            G_ubSharpnessChanged;
   u_int8            G_ubContrastChanged;
   u_int8            G_ubBrightnessChanged;
   u_int8            G_ubSaturationChanged;
   u_int8            G_ubExposureChanged;
   u_int8            G_ubAutoWBChanged;
   u_int8            G_ubImgEffectsChanged;
   u_int8            G_ubColEffectsChanged;
   u_int8            G_ubMeterModeChanged;
   u_int8            G_ubRotationChanged;
   u_int8            G_ubAnnotateChanged;
   u_int8            G_ubAnnotateExChanged;
   u_int8            G_ubVideoStabChanged;
   u_int8            G_ubRawBannerChanged;
   u_int8            G_ubEvCompensationChanged;
   u_int8            G_ubShutterspeedChanged;
   u_int8            G_ubProfileChanged;
   u_int8            G_ubBitrateChanged;
   u_int8            G_ubFullscreenChanged;
   u_int8            G_ubGopChanged;
   u_int8            G_ubIsoChanged;
   u_int8            G_ubOpacityChanged;
   u_int8            G_ubPreviewChanged;
   u_int8            G_ubRoiChanged;
   u_int8            G_ubThumbnailChanged;
   u_int8            G_ubVerboseChanged;
   u_int8            G_ubDemoChanged;
   u_int8            G_ubWrapChanged;
   u_int8            G_ubSegmentChanged;
   u_int8            G_ubPencChanged;
   u_int8            G_ubOutputFileChanged;
   //
   //--------------------------------------------------------------------------
   // Take new persistent parms from this pool:
   //--------------------------------------------------------------------------
   #define SPARE_POOL_PERS 1000
   #define EXTRA_POOL_PERS 0
   //
   // Extra from non-volatile pool
   //
   char              G_pcSparePool2          [SPARE_POOL_PERS-EXTRA_POOL_PERS];
   
   //--------------------------------------------------------------------------
   int               G_iSignature2;          // Signature bottom
}  RPIMAP;

EXTERN RPIMAP *pstMap;
//
// Global functions
//
bool           GLOBAL_Init                   (void);
//
void           GLOBAL_ExpandMap              (int, int);
bool           GLOBAL_Close                  (void);
int            GLOBAL_Lock                   (void);
int            GLOBAL_Unlock                 (void);
char          *GLOBAL_GetHostName            (void);
int            GLOBAL_GetTraceCode           (void);
u_int32        GLOBAL_ConvertDebugMask       (bool);
u_int32        GLOBAL_GetDebugMask           (void);
void           GLOBAL_SetDebugMask           (u_int32);
u_int32        GLOBAL_ReadSecs               (void);
void           GLOBAL_RestoreDefaults        (void);
bool           GLOBAL_Share                  (void);
int            GLOBAL_Status                 (int);
bool           GLOBAL_Sync                   (void);
const PARARGS *GLOBAL_GetParameters          (void);
char          *GLOBAL_GetParameter           (GLOPAR);
bool           GLOBAL_SetParameterChanged    (GLOPAR);
int            GLOBAL_InsertOptions          (char *, int, int);
char          *GLOBAL_GetOption              (const char *);
GLOOPT         GLOBAL_OptionIsActive         (const PARARGS *);
bool           GLOBAL_ParmHasChanged         (const PARARGS *);
bool           GLOBAL_ParmSetChanged         (const PARARGS *, bool);
GLOCMD        *GLOBAL_CommandCopy            (void);
void           GLOBAL_CommandDestroy         (GLOCMD *);
void           GLOBAL_CommandGet             (GLOCMD *);
void           GLOBAL_CommandGetAll          (GLOCMD *);
const char    *GLOBAL_CommandGetString       (int);
void           GLOBAL_CommandPut             (GLOCMD *);
void           GLOBAL_CommandPutAll          (GLOCMD *);
bool           GLOBAL_PidCheckGuards         (void);
bool           GLOBAL_PidGetGuard            (PIDT);
bool           GLOBAL_PidSetGuard            (PIDT);
void           GLOBAL_PidClearGuards         (void);
void           GLOBAL_PidSaveGuard           (PIDT, int);
pid_t          GLOBAL_PidGet                 (PIDT);
const char    *GLOBAL_PidGetName             (PIDT);
const char    *GLOBAL_PidGetHelp             (PIDT);
bool           GLOBAL_PidPut                 (PIDT, pid_t);
int            GLOBAL_PidsLog                (void);
int            GLOBAL_PidsTerminate          (int);
int            GLOBAL_Notify                 (int, int, int);
int            GLOBAL_HostNotification       (int);
bool           GLOBAL_GetSignalNotification  (int, int);
int            GLOBAL_SetSignalNotification  (int, int);
void           GLOBAL_UpdateSecs             (void);
//
int            GLOBAL_GetMallocs             (void);
void           GLOBAL_PutMallocs             (int);
//
int            GLOBAL_KeyGet                 (void);
bool           GLOBAL_KeyPut                 (int);
//
GLOG          *GLOBAL_GetLog                 (void);

#endif /* _GLOBALS_H_ */
