/*  (c) Copyright:  2018  Patrn ESS, Confidential Data
 *
 *  Workfile:           btn_main.c
 *  Revision:          
 *  Modtime:           
 *
 *  Purpose:            Main keyboard/button module
 *
 *
 *
 *  Compiler/Assembler: Raspbian Linux GNU gcc
 *  Ext Packages:
 *
 *  Author:             Peter Hillen
 *
 *
 *  Revisions:
 *    18 Feb 2018       Created
 *    06 Jul 2018       Improve remote IO defs
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
**/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <fcntl.h>
#include <unistd.h>
#include <sched.h>
#include <time.h>
#include <errno.h>

#include <common.h>
#include "config.h"
#include "globals.h"
#include "gen_func.h"
#include "gen_http.h"
//
#include "btn_button.h"

//#define USE_PRINTF
#include <printf.h>

//
// Variables
//
static volatile bool fBtnRunning       = TRUE;
static GLOCMD       *pstCmd            = NULL;
//
// Remote IO storage
//
static bool          fIoIsOn           = FALSE;
static u_int32      *pulIoOnSecs       = NULL;
static int           iRemIo            = 0;
//
//    
// Local prototypes
//
static pid_t   btn_Deamon                 (void);
static void    btn_Execute                (void);
//
static void    btn_InitDynamicPages       (int);
static bool    btn_HandleServerRequest    (void);
static bool    btn_DynPageRemoteIo        (GLOCMD *, int);
static void    btn_HandleGuard            (void);
static void    btn_ScanButtons            (void);
//
static bool    btn_CheckRemoteIo          (void);
static void    btn_HandleRemoteIo         (int);
static int     btn_HandleRemoteIoStatus   (const REMIO *, int);
static int     btn_HandleRemoteIoRelay    (const REMIO *, int);
static int     btn_HandleRemoteIoError    (const REMIO *, int);
static void    btn_NotifyServer           (int, int);
//
static bool    btn_SignalRegister         (sigset_t *);
static void    btn_ReceiveSignalSegmnt    (int);
static void    btn_ReceiveSignalInt       (int);
static void    btn_ReceiveSignalTerm      (int);
static void    btn_ReceiveSignalUser1     (int);
static void    btn_ReceiveSignalUser2     (int);
//
static void    btn_InitSemaphore          (void);
static void    btn_ExitSemaphore          (void);
static void    btn_PostSemaphore          (void);
static int     btn_WaitSemaphore          (int);


#define  EXTRACT_DYN(a,b,c,d,e,f)   {a,b,c,d,e,f},
static const CMDIDYN stDynamicIoPages[] =
{
//    tUrl,    tType,   iTimeout,   iFlags,  pcUrl,   pfDynCb;
   #include "dyn_io.h"
   {  -1,      0,       0,          0,       NULL,    NULL  }
};
#undef EXTRACT_DYN

//
// Remote IO lookup table:
//    int         iIdx;                // IO Index 0..?
//    GLOPAR      eParm;               // Global parameter enum
//    int         iMask;               // Mask
//    int         iBitNr;              // Actual output bit
//    PFRIO       pfRemIo;             // Handler Function
//
static const REMIO stRemoteIoLookup[] =
{
   {  0, CAM_REMOTE_ON_3,  0x0001,  1,    btn_HandleRemoteIoStatus      },
   {  1, CAM_REMOTE_ON_3,  0x0002,  2,    btn_HandleRemoteIoStatus      },
   {  2, CAM_REMOTE_ON_3,  0x0004,  3,    btn_HandleRemoteIoStatus      },
   {  3, CAM_REMOTE_ON_3,  0x0008,  4,    btn_HandleRemoteIoStatus      },
   {  4, CAM_REMOTE_ON_3,  0x0010,  5,    btn_HandleRemoteIoStatus      },
   {  5, CAM_REMOTE_ON_1,  0x0100,  1,    btn_HandleRemoteIoRelay       },
   {  6, CAM_REMOTE_ON_2,  0x0200,  2,    btn_HandleRemoteIoRelay       },
   {  7, CAM_REMOTE_ON_3,  0x8000,  0,    btn_HandleRemoteIoError       }
};
static const int iNumRemoteIo = sizeof(stRemoteIoLookup)/sizeof(REMIO);


/*------  Local functions separator ------------------------------------
__GLOBAL_FUNCTIONS_________(){};
----------------------------------------------------------------------------*/

// 
// Function:   BTN_Init
// Purpose:    Init the button thread
// 
// Parameters: 
// Returns:    Completion
// Note:       Some IO drivers have variables initialized, and therefor all
//             access to the IO MUST take place through this thread !
// 
int BTN_Init()
{
   int iCc=0;

   if( btn_Deamon() > 0) iCc = GLOBAL_BTN_INI;
   //
   // PID_HOST will return here. 
   // The btn_main deamon PID_BUTN will exit upon completion.
   //
   return(iCc);
}

/*------  Local functions separator ------------------------------------
__LOCAL_FUNCTIONS_________(){};
----------------------------------------------------------------------------*/

//
// Function:   btn_Deamon
// Purpose:    Instantiate the command thread 
//
// Parms:      
// Returns:    The pid of the new command thread
//
static pid_t btn_Deamon(void)
{
   pid_t    tPid;

   tPid = fork();
   //
   switch(tPid)
   {
      default:
         // parent 
         GLOBAL_PidPut(PID_BUTN, tPid);
         break;

      case 0:
         // child
         PRINTF("btn-Deamon()" CRLF);
         btn_Execute();
         _exit(0);
         break;

      case -1:
         // Error
         LOG_printf("btn-Deamon(): Error!"CRLF);
         LOG_Report(errno, "BTN", "btn-Deamon(): Error!");
         break;
   }
   return(tPid);
}

// 
// Function:   btn_Execute 
// Purpose:    Run the command thread
// 
// Parameters:  
// Returns:  
// 
static void btn_Execute(void)
{
   sigset_t tBlockset;

   if(btn_SignalRegister(&tBlockset) == FALSE)  _exit(1);
   //
   btn_InitSemaphore();
   //
   btn_InitDynamicPages(80);
   //
   pulIoOnSecs = safemalloc(iNumRemoteIo * sizeof(u_int32 *));
   //
   BTN_InitButtons();
   pstCmd = GLOBAL_CommandCopy();
   GLOBAL_DUMPCOMMAND("btn-Execute():PID_BUTN(Copy)", pstCmd);
   //
   GLOBAL_PidSaveGuard(PID_BUTN, GLOBAL_BTN_INI);
   GLOBAL_HostNotification(GLOBAL_BTN_INI);
   LOG_Report(0, "BTN", "btn-Execute():Init Ready");
   //
   while(fBtnRunning)
   {
      //
      // The button mainloop
      // While waiting for a command, a 100 msecs timeout will be used to handle:
      //    o Button scan
      //
      switch( btn_WaitSemaphore(100) )
      {
         case 0:
            if(GLOBAL_GetSignalNotification(PID_BUTN, GLOBAL_SVR_RUN)) btn_HandleServerRequest();
            if(GLOBAL_GetSignalNotification(PID_BUTN, GLOBAL_GRD_RUN)) btn_HandleGuard();
            break;

         default:
            break;

         case -1:
            // Error
            PRINTF("btn-Execute(): Error btn_WaitSemaphore"CRLF);
            LOG_Report(errno, "BTN", "btn-Execute(): Error btn_WaitSemaphore");
            fBtnRunning = FALSE;
            break;

         case 1:
            GLOBAL_PidSetGuard(PID_BUTN);
            //
            // 100 mSec timeout: handle Buttons and IO
            //
            if(fIoIsOn) fIoIsOn = btn_CheckRemoteIo();
            btn_ScanButtons();
            break;
      }
   }
   safefree(pulIoOnSecs);
   GLOBAL_CommandDestroy(pstCmd);
   btn_ExitSemaphore();
}

/*------  Local functions separator -----------------------------------------
__MISC_FUNCTIONS_________(){};
----------------------------------------------------------------------------*/

//
// Function:   btn_InitDynamicPages
// Purpose:    Register dynamic webpages
//
// Parameters: Port
// Returns:
// Note:
//
static void btn_InitDynamicPages(int iPort)
{
   const CMDIDYN *pstDyn=stDynamicIoPages;
   
   PRINTF1("btn-InitDynamicPages(): port=%d" CRLF, iPort);
   //
   while(pstDyn->pcUrl)
   {
      PRINTF1("btn-InitDynamicPages():Url=%s" CRLF, pstDyn->pcUrl);
      if(pstDyn->iFlags & DYN_FLAG_PORT)
      {
         //
         // Register for this specific port
         //
         GEN_RegisterDynamicPage(pstDyn->tUrl, PID_BUTN, pstDyn->tType, pstDyn->iTimeout, 80, pstDyn->pcUrl, (PFVOIDPI)pstDyn->pfDynCb);
      }
      else
      {
         //
         // Register for all ports
         //
         GEN_RegisterDynamicPage(pstDyn->tUrl, PID_BUTN, pstDyn->tType, pstDyn->iTimeout,     0, pstDyn->pcUrl, (PFVOIDPI)pstDyn->pfDynCb);
      }
      pstDyn++;
   }
}

//
// Function:   btn_HandleServerRequest
// Purpose:    Handle the request
//
// Parms:      
// Returns:    
// Note:
//
static bool btn_HandleServerRequest(void)
{
   bool     fCc=FALSE;
   FTYPE    tType;
   int      iIdx;
   PFVOIDPI pfDynCb;

   PRINTF("btn-HandleServerRequest():" CRLF);
   GLOBAL_CommandGet(pstCmd);
   GLOBAL_DUMPCOMMAND("btn-HandleServerRequest():PID_BUTN(Get)", pstCmd);
   //
   //
   // We are signalled to do something:
   //
   iIdx  = pstMap->G_iCurDynPage;
   tType = GEN_GetDynamicPageType(iIdx);        // HTTP_HTML, HTTP_JSON
   //
   // Lookup command
   //
   switch(tType)
   {
      case HTTP_HTML:
         break;
      
      case HTTP_JSON:
         //
         // Use JSON API
         //
         pfDynCb = GEN_GetDynamicPageCallback(iIdx);
         if(pfDynCb) fCc = pfDynCb(pstCmd, iIdx);
         break;
      
      default:
         break;
   }
   pstCmd->iCommand = CAM_CMD_REMOTEIO;
   pstCmd->iArgs    = CAM_PRM;
   btn_NotifyServer(CAM_STATUS_IDLE, GLOBAL_BTN_NFY);
   PRINTF("btn-HandleServerRequest():Done" CRLF);
   return(fCc);
}

// 
// Function:   btn_DynPageRemoteIo
// Purpose:    Dynamically created remoteIo page
// 
// Parameters: Client, DynPage index
// Returns:    1 if OKee
// Note:       
//
static bool btn_DynPageRemoteIo(GLOCMD *pstCmd, int iIdx)
{
   bool     fCc=FALSE;

   switch(GEN_GetDynamicPageType(iIdx))
   {
      default:
         break;

      case HTTP_HTML:
         break;

      case HTTP_JSON:
         GEN_STRCPY(pstMap->G_pcCommand, "RemIo");
         btn_HandleRemoteIo(atoi(pstMap->G_pcRemoteIo));
         fCc = TRUE;
         break;
   }
   return(fCc);
}

// 
// Function:   btn_HandleGuard
// Purpose:    Respond to the guard query
// 
// Parameters: 
// Returns:    
// Note:       
//
static void btn_HandleGuard(void)
{
   LOG_Report(0, "BUT", "btn-HandleGuard():Host-notification send");
   GLOBAL_HostNotification(GLOBAL_GRD_NFY);
}

// 
// Function:   btn_ScanButtons
// Purpose:    Scan the button status
// 
// Parameters: 
// Returns:    Signal SIGUSR1 (10) to the HTTP-Server we have a change
// Note:       
//             ===========================================================
//             Timer event: Use 100 msecs tick to check buttons and stuff 
//             ===========================================================
//
static void btn_ScanButtons(void)
{
   u_int8   ubButKey;

   if( (ubButKey = BTN_ScanButtons()) != RCU_NOKEY)
   {
      if(GLOBAL_KeyPut((int)ubButKey))
      {
         LOG_Report(0, "BTN", "btn-ScanButtons():Key=%d", (int)ubButKey);
         //
         // Key has been put into keybuffer:
         // Notify the host we have a button key
         //
         PRINTF1("btn-ScanButtons():Key=%d" CRLF, (int)ubButKey);
         GLOBAL_Notify(PID_HOST, GLOBAL_BTN_NFY, SIGUSR1);
      }
      else
      {
         LOG_Report(0, "BTN", "btn-ScanButtons():Key=%d:BUFFER IS FULL!!", (int)ubButKey);
      }
   }
}

// 
// Function:   btn_CheckRemoteIo
// Purpose:    Turn remote IO off if timeout
// 
// Parameters: 
// Returns:    TRUE if IO still ON
// Note:       
//
static bool btn_CheckRemoteIo(void)
{
   bool     fCc=FALSE;
   int      iIdx;
   u_int32  ulIoOnSecs;
   u_int32  ulSecs=RTC_GetSystemSecs();
   
   if(pulIoOnSecs == NULL) return(FALSE);

   for(iIdx=0; iIdx<iNumRemoteIo; iIdx++)
   {
      ulIoOnSecs = pulIoOnSecs[iIdx];
      if(ulIoOnSecs)
      {
         if(ulSecs > ulIoOnSecs)
         {
            // 
            // Time is up: turn off
            //
            stRemoteIoLookup[iIdx].pfRemIo(&stRemoteIoLookup[iIdx], 0);
            pulIoOnSecs[iIdx] = 0;
            iRemIo &= ~stRemoteIoLookup[iIdx].iMask;
            GEN_SNPRINTF(pstMap->G_pcRemoteIo, MAX_PARM_LEN, "%d", iRemIo);
         }
         else
         {
            fCc = TRUE;
         }
      }
   }
   return(fCc);
}

// 
// Function:   btn_HandleRemoteIo
// Purpose:    Turn remote IO on/of
// 
// Parameters: Buffer, length
// Returns:    
// Note:       
//
static void btn_HandleRemoteIo(int iBits)
{
   int      i;

   PRINTF1("btn-HandleRemoteIo(): bits 0x%x" CRLF, iBits);
   //
   if(iBits != iRemIo)
   {
      //
      // IO has changed: Update
      //
      for(i=0; i<iNumRemoteIo; i++)
      {
         if(stRemoteIoLookup[i].iMask & iBits)
         {
            PRINTF2("btn-HandleRemoteIo(): %d: bit 0x%x=1" CRLF, i+1, stRemoteIoLookup[i].iMask);
            if(GEN_CheckDelete())   
            {
               stRemoteIoLookup[i].pfRemIo(&stRemoteIoLookup[i], 0);
               iRemIo &= ~stRemoteIoLookup[i].iMask;
            }
            else                    
            {
               stRemoteIoLookup[i].pfRemIo(&stRemoteIoLookup[i], 1);
               iRemIo |= stRemoteIoLookup[i].iMask;
            }
            GEN_SNPRINTF(pstMap->G_pcRemoteIo, MAX_PARM_LEN, "%d", iRemIo);
         }
      }
   }
}

// 
// Function:   btn_HandleRemoteIoStatus
// Purpose:    Handle remote IO status
// 
// Parameters: REMIO ptr, value
// Returns:    0
// Note:       
//
static int btn_HandleRemoteIoStatus(const REMIO *pstRemIo, int iVal)
{
   u_int32  ulSecs=RTC_GetSystemSecs();
   char    *pcParm;

   if(iVal) fIoIsOn = TRUE;
   pcParm = GLOBAL_GetParameter(pstRemIo->eParm);

   if(pulIoOnSecs) pulIoOnSecs[pstRemIo->iIdx] = ulSecs + atoi(pcParm);
   else            PRINTF("btn-HandleRemoteIoStatus(): ERROR: No sec buffer" CRLF);
   //
   PRINTF2("btn-HandleRemoteIoStatus(): bit-%d=%d" CRLF, pstRemIo->iBitNr, iVal);
   switch(pstRemIo->iBitNr)
   {
      case 3:
      case 4:
      case 5:
         OUT(LED_W, iVal);
         break;

      case 2:
         OUT(LED_Y, iVal);
         break;

      case 1:
         OUT(LED_G, iVal);
         break;

      default:
         break;
   }
   LOG_Report(0, "BTN", "btn-HandleRemoteIoStatus():Led%d=%d", pstRemIo->iBitNr, iVal);
   return(0);
}

// 
// Function:   btn_HandleRemoteIoRelay
// Purpose:    Turn remote IO on/of
// 
// Parameters: REMIO ptr, value
// Returns:    
// Note:       
//
static int btn_HandleRemoteIoRelay(const REMIO *pstRemIo, int iVal)
{
   u_int32  ulSecs=RTC_GetSystemSecs();
   char    *pcParm;

   if(iVal) fIoIsOn = TRUE;
   pcParm = GLOBAL_GetParameter(pstRemIo->eParm);
   if(pulIoOnSecs) pulIoOnSecs[pstRemIo->iIdx] = ulSecs + atoi(pcParm);
   else            PRINTF("btn-HandleRemoteIoRelay(): ERROR: No sec buffer" CRLF);
   //
   switch(pstRemIo->iBitNr)
   {
      case 1:
         PRINTF1("btn-HandleRemoteIoRelay(): Relay-1=%d" CRLF, iVal);
         OUT(REL_1, iVal);
         LOG_Report(0, "BTN", "btn-HandleRemoteIoRelay():Relais-1=%d", iVal);
         break;

      case 2:
         PRINTF1("btn-HandleRemoteIoRelay(): Relay-2=%d" CRLF, iVal);
         OUT(REL_2, iVal);
         LOG_Report(0, "BTN", "btn-HandleRemoteIoRelay():Relais-2=%d", iVal);
         break;

      default:
         break;
   }
   return(0);
}

// 
// Function:   btn_HandleRemoteIoError
// Purpose:    Turn remote IO on/of
// 
// Parameters: REMIO ptr, value
// Returns:    
// Note:       
//
static int btn_HandleRemoteIoError(const REMIO *pstRemIo, int iVal)
{
   u_int32  ulSecs=RTC_GetSystemSecs();
   char    *pcParm;

   if(iVal) fIoIsOn = TRUE;
   pcParm = GLOBAL_GetParameter(pstRemIo->eParm);
   if(pulIoOnSecs) pulIoOnSecs[pstRemIo->iIdx] = ulSecs + atoi(pcParm);
   else            PRINTF("btn-HandleRemoteIoError(): ERROR: No sec buffer" CRLF);
   //
   PRINTF1("btn-HandleRemoteIoError(): LED-R=%d" CRLF, iVal);
   OUT(LED_R, iVal);
   LOG_Report(0, "BTN", "btn-HandleRemoteIoError():LED-R=%d", iVal);
   return(0);
}

// 
// Function:   btn_NotifyServer
// Purpose:    Signal PID_SRVR using SIGUSR1
// 
// Parameters: Command completion status, notification 
// Returns:    
// Note:       Uses MMAP shared memory
//
static void btn_NotifyServer(int iStatus, int iNotification)
{
   pstCmd->iStatus = iStatus;
   //
   PRINTF5("btn-NotifyServer():Notify PID_SRVR:Url=%d, Cmd=%d, Status=%d, Args=0x%X, Data=%d" CRLF, 
               pstCmd->tUrl, pstCmd->iCommand, pstCmd->iStatus, pstCmd->iArgs, pstCmd->iData1);
   //
   GLOBAL_CommandPut(pstCmd);
   GLOBAL_DUMPCOMMAND("btn-NotifyServer():PID_BUTN(Put)", pstCmd);
   //
   GLOBAL_SetSignalNotification(PID_SRVR, iNotification);
   GEN_Signal(PID_SRVR, SIGUSR1);
}


/*------  Local functions separator ------------------------------------
__SIGNAL_FUNCTIONS(){};
----------------------------------------------------------------------------*/

// 
// Function:   btn_SignalRegister
// Purpose:    Register all SIGxxx
// 
// Parameters: Blockset
// Returns:    TRUE if delivered
// 
static bool btn_SignalRegister(sigset_t *ptBlockset)
{
  bool fCC = TRUE;

  // SIGSEGV:
  if( signal(SIGSEGV, &btn_ReceiveSignalSegmnt) == SIG_ERR)
  {
     LOG_Report(errno, "BTN", "btn-SignalRegister(): SIGSEGV ERROR");
     fCC = FALSE;
  }
  // SIGUSR1:
  if( signal(SIGUSR1, &btn_ReceiveSignalUser1) == SIG_ERR)
  {
     LOG_Report(errno, "BTN", "btn-SignalRegister(): SIGUSR1 ERROR");
     fCC = FALSE;
  }
  // SIGUSR2:
  if( signal(SIGUSR2, &btn_ReceiveSignalUser2) == SIG_ERR)
  {
     LOG_Report(errno, "BTN", "btn-SignalRegister(): SIGUSR2 ERROR");
     fCC = FALSE;
  }
  // CTL-X handler
  if( signal(SIGTERM, &btn_ReceiveSignalTerm) == SIG_ERR)
  {
     LOG_Report(errno, "BTN", "btn-SignalRegister(): SIGTERM ERROR");
     fCC = FALSE;
  }
  // Ctl-C handler
  if( signal(SIGINT, &btn_ReceiveSignalInt) == SIG_ERR)
  {
     LOG_Report(errno, "BTN", "btn-SignalRegister(): SIGINT ERROR");
     fCC = FALSE;
  }

  if(fCC)
  {
     //
     // Setup the SIGxxx events
     //
     sigemptyset(ptBlockset);
     sigaddset(ptBlockset, SIGSEGV);
     sigaddset(ptBlockset, SIGTERM);
     sigaddset(ptBlockset, SIGINT);
     sigaddset(ptBlockset, SIGUSR1);
     sigaddset(ptBlockset, SIGUSR2);
  }
  return(fCC);
}

//
// Function:   btn_ReceiveSignalSegmnt
// Purpose:    Handle the SIGSEGV
//
// Parms:
// Returns:    TRUE if delivered
// Note:       Do NOT use LOG_xxx() here
//
static void btn_ReceiveSignalSegmnt(int iSignal)
{
   if(fBtnRunning)
   {
      fBtnRunning = FALSE;
      //
      GEN_SegmentationFault(__FILE__, __LINE__);
      LOG_SegmentationFault(__FILE__, __LINE__);
      //
      // (TRY TO) Cleanup background threads:
      //
      GLOBAL_Notify(PID_HOST, GLOBAL_FLT_NFY, SIGTERM);
   }
   btn_PostSemaphore();
}

// 
// Function:   btn_ReceiveSignalTerm
// Purpose:    Add the SIGTERM command in the buffer (Ctl-X)
// 
// Parameters: 
// Returns:    TRUE if delivered
// 
static void btn_ReceiveSignalTerm(int iSignal)
{
   LOG_Report(0, "BTN", "btn-ReceiveSignalTerm()");
   fBtnRunning = FALSE;
   btn_PostSemaphore();
}

//
// Function:   btn_ReceiveSignalInt
// Purpose:    System callback for SIGINT (Ctl-C)
//
// Parms:
// Returns:    TRUE if delivered
//
static void btn_ReceiveSignalInt(int iSignal)
{
   LOG_Report(0, "BTN", "btn-ReceiveSignalInt()");
   fBtnRunning = FALSE;
   btn_PostSemaphore();
}

//
// Function:   btn_ReceiveSignalUser1
// Purpose:    System callback for SIGUSR1 signal
//
// Parms:
// Returns:    
// Note:       The camera thread will send us a SIGUSR1 to
//             signal a new command
//
static void btn_ReceiveSignalUser1(int iSignal)
{
   btn_PostSemaphore();
}

//
// Function:   btn_ReceiveSignalUser2
// Purpose:    System callback for SIGUSR2 signal
//
// Parms:
// Returns:    
// Note:       Not used
//
static void btn_ReceiveSignalUser2(int iSignal)
{
}

/*------  Local functions separator ------------------------------------
__SEMAPHORE_FUNCTIONS_________(){};
----------------------------------------------------------------------------*/

//
// Function:   btn_InitSemaphore
// Purpose:    Init the Semaphore
//
// Parms:      
// Returns:    
// Note:       If pshared has the value 0, then the semaphore is shared between the
//             threads of a process, and should be located at some address that is
//             visible to all threads (e.g., a global variable, or a variable
//             allocated dynamically on the heap).
//             If pshared is nonzero, then the semaphore is shared between
//             processes, and should be located in a region of shared memory.
//
//             The sem value is always >= 0:
//                               0 means LOCKED
//                              >0 means UNLOCKED
//             Init(sem 1, VAL)  VAL=1 unlocked
//                               VAL=0 locked
//             Wait(sem)         LOCK:    if(VAL> 0) { VAL--; return;         } 
//                                        if(VAL==0) { Wait till > 0; return; }
//             Post(sem)         UNLOCK:  VAL++
//
static void btn_InitSemaphore(void)
{
   //
   // Init the semaphore as LOCKED
   //
   if(sem_init(&pstMap->G_tSemBtn, 1, 0) == -1)
   {
      LOG_Report(errno, "BTN", "btn-InitSemaphore(): sem_init Error");
   }
}

//
// Function:   btn_ExitSemaphore
// Purpose:    Exit the Semaphore
//
// Parms:      
// Returns:    
// Note:       
//
static void btn_ExitSemaphore(void)
{
   if( sem_destroy(&pstMap->G_tSemBtn) == -1) LOG_Report(errno, "BTN", "btn-ExitSemaphore():sem_destroy");
}

//
// Function:   cad_PostSemaphore
// Purpose:    Unlock the screen Semaphore
//
// Parms:      
// Returns:    
// Note:       sem_post() increments (unlocks) the semaphore pointed to by sem. If the semaphore's 
//             value consequently becomes greater than zero, then another process or thread blocked
//             in a sem_wait(3) call will be woken up and proceed to lock the semaphore. 
//
static void btn_PostSemaphore()
{
   int      iCc;

   //
   // sem++ (UNLOCK)   
   //
   iCc = sem_post(&pstMap->G_tSemBtn);
   if(iCc == -1) LOG_Report(errno, "BTN", "btn-PostSemaphore(): sem_post error");
}

//
// Function:   btn_WaitSemaphore
// Purpose:    Wait till Semaphore has been unlocked, then LOCK and proceed
//
// Parms:      TIMEOUT (or -1 if wait indefinitely)
// Returns:     0 if sem unlocked
//             +1 if sem timeout
//             -1 on error (errno)
// Note:       sem_wait() tries to decrement (lock) the semaphore. 
//             If the semaphore currently has the value zero, then the call blocks until 
//             either it becomes possible to perform the decrement (the semaphore value rises 
//             above zero) or a signal handler interrupts the call. 
//             If the semaphore's value is greater than zero, then the decrement proceeds, 
//             and the function returns immediately. 
//
//             sem_timedwait() is the same as sem_wait(), except that TIMEOUT specifies a 
//             limit on the amount of time that the call should block if the decrement cannot 
//             be immediately performed. The stTime points to a structure that
//             specifies an absolute timeout in seconds and nanoseconds since the Epoch, 
//             1970-01-01 00:00:00 +0000 (UTC). This structure is defined as follows:
//
//             struct timespec 
//             {
//                time_t tv_sec;      /* Seconds */
//                long   tv_nsec;     /* Nanoseconds [0 .. 999999999] */
//             };
//             If the timeout has already expired by the time of the call, and the semaphore 
//             could not be locked immediately, then sem_timedwait() fails with a timeout error 
//             (errno set to ETIMEDOUT). If the operation can be performed immediately, then 
//             sem_timedwait() never fails with a timeout error, regardless of the value of 
//             TIMEOUT.
//
static int btn_WaitSemaphore(int iMsecs)
{
   //
   // if    sem == 0 : wait
   // else  sem-- (LOCK)   
   //
   return(GEN_WaitSemaphore(&pstMap->G_tSemBtn, iMsecs));
}

