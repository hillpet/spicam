/*  (c) Copyright:  2017..2018  Patrn, Confidential Data
 *
 *  Workfile:           btn_defs.h
 *  Revision:          
 *  Modtime:           
 *
 *  Purpose:            Headerfile for FP button keys
 *
 *
 *
 *  Compiler/Assembler: Raspbian Linux GNU gcc
 *  Ext Packages:
 *
 *  Author:             Peter Hillen
 *  Date Created:       06 Jan 2018
 *
 *  Revisions:
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
**/

//
// FP Buttons
//
EXTRACT_KEY(BTN_KEY1,            "FP_KEY_1")             // --                Push button 1
EXTRACT_KEY(BTN_KEY2,            "FP_KEY_2")             // --                Push button 2
EXTRACT_KEY(BTN_KEY3,            "FP_KEY_3")             // --                Push button 3
EXTRACT_KEY(BTN_KEY4,            "FP_KEY_4")             // --                Push button 4
EXTRACT_KEY(BTN_KEY5,            "FP_KEY_5")             // --                Push button 5
EXTRACT_KEY(BTN_KEY6,            "FP_KEY_6")             // --                Push button 6
EXTRACT_KEY(BTN_KEY7,            "FP_KEY_7")             // --                Push button 7
EXTRACT_KEY(BTN_KEY8,            "FP_KEY_8")             // --                Push button 8
