/*  (c) Copyright:  2017  Patrn ESS, Confidential Data 
 *
 *  Workfile:           mail_ssl.h
 *  Revision:          
 *  Modtime:           
 *
 *  Purpose:            Raspberry pi SSL mail handler header file
 *
 *
 *
 *  Compiler/Assembler: Raspbian Linux GNU gcc
 *  Ext Packages:
 *
 *  Author:             Peter Hillen
 *  Date Created:       16 Nov 2017 ported from landog mail
 *
 *  Revisions:
 *  Entry Points:       
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
**/

#ifndef _MAIL_SSL_H_
#define _MAIL_SSL_H_

#include "mail_func.h"

bool SSL_RpiDigestMD5         (char *, int, char *, int);
//
bool SSL_RpiInit              (MAILO *);
bool SSL_RpiConnect           (MAILO *);
int  SSL_RpiRead              (MAILO *, char *, int);
int  SSL_RpiWrite             (MAILO *, char *, int);
bool SSL_RpiClose             (MAILO *);

#endif /* _MAIL_SSL_H_ */
