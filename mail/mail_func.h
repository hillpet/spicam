/*  (c) Copyright:  2017  Patrn ESS, Confidential Data 
 *
 *  Workfile:           cmd_mail.h
 *  Revision:          
 *  Modtime:           
 *
 *  Purpose:            Raspberry pi mail handler header file
 *
 *
 *
 *  Compiler/Assembler: Raspbian Linux GNU gcc
 *  Ext Packages:
 *
 *  Author:             Peter Hillen
 *  Date Created:       16 Nov 2017 ported from landog mail
 *
 *  Revisions:
 *  Entry Points:       
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
**/

#ifndef _MAIL_FUNC_H_
#define _MAIL_FUNC_H_

#include "encode.h"

#ifdef   FEATURE_USE_SSL
#include <openssl/ssl.h>
#include <openssl/err.h>
#endif   //FEATURE_USE_SSL

#define BAD_SOCKET              -1
#define SSL_SOCKET              -2
//
#define BUF_MISC                 256         // Misc buffers space
//
#define POP3_TIMEOUT             5           // 5 secs timeout
#define SMTP_TIMEOUT             5           // 5 secs timeout
#define MAIL_BUFLEN              2048        // Buffer for mail data
//
typedef enum __auth_type_
{
   MAIL_SETUP_NO_AUTH = 0,                   // Setup for no        authentication
   MAIL_SETUP_PLAIN_AUTH,                    // Setup for plaintext authentication
   MAIL_SETUP_SSL_AUTH,                      // Setup for SSL       authentication
   //
   NUM_MAIL_SETUP_AUTH
}  ATYPE;
//
// Default port designators, if not supplied through the Global MMAP
//
#define MAIL_POP3_PORT_PLAIN     110         // POP3 server port for plaintext authentication
#define MAIL_POP3_PORT_SSL       995         // POP3 server port for ssl       authentication
#define MAIL_IMAP_PORT_SSL       935         // IMAP server port for ssl       authentication
#define MAIL_SMTP_PORT_PLAIN     25          // SMTP server port for plaintext authentication
#define MAIL_SMTP_PORT_SSL       587         // SMTP server port for ssl       authentication
//
typedef enum _mail_type_
{
  MAIL_POP3 = 0,
  MAIL_IMAP,
  MAIL_SMTP,
  //
  NUM_MAIL_TYPES
} MAILT;
//
typedef enum _mail_cont_type_
{
  MAIL_CT_NONE = 0,
  MAIL_CT_TEXT,
  MAIL_CT_HTML,
  MAIL_CT_PICT,
  MAIL_CT_ATTM,
  MAIL_CT_FILE,
  //
  NUM_MAIL_CT
} MAILCT;
//
typedef enum _mail_data_type_
{
  MAIL_DT_NONE = 0,
  MAIL_DT_FROM,
  MAIL_DT_SUBJECT,
  MAIL_DT_DATE,
  MAIL_DT_ID,
  MAIL_DT_REPLY,
  MAIL_DT_TO,
  MAIL_DT_CC,
  MAIL_DT_BCC,
  MAIL_DT_BODY,
  MAIL_DT_ATTM,
  //
  NUM_MAIL_DT
} MAILDT;
//
typedef struct mail_list
{
   char             *pcData;
   int               iDataSize;
   MAILDT            tDataType;
   MAILCT            tContType;
   struct mail_list *pstNext;
}  MAILL;
//
typedef bool   (*MAILRCB)(int, char *, int, void *);
typedef MAILCT (*MAILSCB)(MAILDT, int, char **, int *);
//
//
//   MAILO contains all needed values to send the mail
//
typedef struct _mail_opt_
{
   MAILT    tType;                  // POP3/SMTP
   ATYPE    tAuth;                  // Authentication type
   int      iSocket;                // Socket server
   int      iPort;                  // Port
   bool     fSsl;                   // SSL
   u_int32  ulSecs;                 // Mail T&D stamp
   char    *pcHost;                 // Server
   char    *pcUser;                 // Username
   char    *pcPass;                 // Password
   //
   char    *pcMail;                 // Mail buffer
   int      iMailSize;              // Size
   //
   MAILL   *pstList;                // Mailing parameter list
   //
#ifdef   FEATURE_USE_SSL
   SSL_CTX *pstCtx;                 // SSL CTX struct ptr
   SSL     *pstSsl;                 // SSL     struct ptr
#endif   //FEATURE_USE_SSL
   //
}  MAILO;


MAILO  *MAIL_Init               (ATYPE);
bool    MAIL_SetMailServer      (MAILO *, MAILT, char *, int, char *, char *);
bool    MAIL_Login              (MAILO *);
void    MAIL_SendMail           (MAILO *);
void    MAIL_Logout             (MAILO *);
MAILO  *MAIL_Cleanup            (MAILO *);
bool    MAIL_DeleteMail         (MAILO *, int);
bool    MAIL_RetrieveHeader     (MAILO *, int, int, MAILRCB, void *);
bool    MAIL_ReceiveMail        (MAILO *, int, MAILRCB, void *);
int     MAIL_RetrieveAccount    (MAILO *);

#endif /* _MAIL_FUNC_H_ */
