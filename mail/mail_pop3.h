/*  (c) Copyright:  2017  Patrn ESS, Confidential Data 
 *
 *  Workfile:           mail_pop3.h
 *  Revision:          
 *  Modtime:           
 *
 *  Purpose:            Raspberry pi pop3 mail handler header file
 *
 *
 *
 *  Compiler/Assembler: Raspbian Linux GNU gcc
 *  Ext Packages:
 *
 *  Author:             Peter Hillen
 *  Date Created:       16 Nov 2017 ported from landog mail
 *
 *  Revisions:
 *  Entry Points:       
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
**/

#ifndef _MAIL_POP3_H_
#define _MAIL_POP3_H_

bool POP3_LogIn            (MAILO *);
bool POP3_DeleteMessage    (MAILO *, int);
bool POP3_RetrieveHeader   (MAILO *t, int, int, MAILRCB, void *);
int  POP3_RetrieveAccount  (MAILO *);
bool POP3_ReceiveMail      (MAILO *, int, MAILRCB, void *);
bool POP3_LogOff           (MAILO *);

#endif /* _MAIL_POP3_H_ */
