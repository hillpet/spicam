/*  (c) Copyright:  2017  Patrn ESS, Confidential Data 
 *
 *  Workfile:           crypto.c
 *  Revision:          
 *  Modtime:           
 *
 *  Purpose:            API for encryption functions
 *
 *
 *
 *  Compiler/Assembler: Raspbian Linux GNU gcc
 *  Ext Packages:
 *
 *  Author:             Peter Hillen
 *  Date Created:       16 Nov 2017 ported from landog mail
 *
 *  Entry Points:       Crypto_Control
 *                      Crypto_Open
 *                      Crypto_Close
 *                      Crypto_Encrypt
 *                      Crypto_Decrypt
 *                      Crypto_GetEncryptionSize
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
**/

#include <string.h>

#include <common.h>
#include "../config.h"
#include "../globals.h"
#include "blowfish.h"
#include "crypto.h"

//#define  USE_PRINTF
#include <printf.h>

static bool          fCryptValid  = FALSE;
static u_int8       *pubUserKey   = NULL;
static u_int32       ulUserLength = 0;

//
// P- and S-boxes for storage of expanded keys.
//
static CRYPTO        stCrypt[CRYPT_MAX_HANDLES];
static const u_int8  pubStaticKey[CRYPT_STATIC_KEY_LENGTH] = 
{ 
   0x19, 0x2F, 0xBB, 0x6B, 0x2A, 0x84, 0xD5, 0x14, 
   0x14, 0xD5, 0x84, 0x2A, 0x6B, 0xBB, 0x2F, 0x19
};

//
// Local prototypes
//
static bool    crypto_InitDiskLevel    (CRYPT_MODE);
static bool    crypto_InitFileLevel    (void *, CRYPT_MODE);
static u_int16 crypto_InitKeys         (u_int8 *, CRYPT_MODE);
static bool    crypto_InitStreamLevel  (void *, CRYPT_MODE);
static bool    crypto_Selftest         (void *, CRYPT_MODE);


/*
 **
 *  Name:        Crypto_Control
 *
 *  Description: Perform control functions
 *
 *  Arguments:   Cmd, data ptr, Cmd parameter
 *
 *  Returns:     TRUE if OKee
 *
 */
bool Crypto_Control(CRYPT_CMD tCmd, void *pData, u_int32 ulParm)
{
  bool  fCc = FALSE;

  switch(tCmd)
  {
     case CRYPT_CMD_SET_KEY:
        if( pData && (ulParm <= CRYPT_MAX_KEY_LENGTH) )
        {
           pubUserKey   = (u_int8 *)pData;
           ulUserLength = ulParm;
           fCc          = TRUE;
        }
        break;

    case CRYPT_CMD_CLEAR_KEY:
        pubUserKey   = NULL;
        ulUserLength = 0;
        fCc          = TRUE;
        break;

     default:
        break;
  }
  return(fCc);
}

/*
 **
 *  Name:        Crypto_Close
 *
 *  Description: Close the Encryption/Decryption module
 *
 *  Arguments:   Handle
 *
 *  Returns:     void
 *
 */
void Crypto_Close(CRYPTH tHandle)
{
   if(tHandle == CRYPT_ILLEGAL_HANDLE) return;
   //
   if(--tHandle < CRYPT_MAX_HANDLES)
   {
      switch(stCrypt[tHandle].tLevel)
      {
         case CRYPT_LEVEL_DISK:
            break;

         case CRYPT_LEVEL_FILE:
         case CRYPT_LEVEL_STREAM:
            safefree(stCrypt[tHandle].pBox);
            break;

         default:
            break;
      }
      stCrypt[tHandle].tLevel  = CRYPT_LEVEL_NONE;
      stCrypt[tHandle].tMode   = CRYPT_MODE_NONE;
      stCrypt[tHandle].ulDepth = 0;
      stCrypt[tHandle].pBox    = NULL;
   }
}

/*
 **
 *  Name:        Crypto_Open
 *
 *  Description: Openthe Encryption/Decryption module
 *
 *  Arguments:   Mode, Level, default crypto size
 *
 *  Returns:     Handle 1..?
 *
 */
CRYPTH Crypto_Open(CRYPT_MODE tMode, CRYPT_LEVEL tLevel, u_int32 ulDepth)
{
  CRYPTH   tHandle;
  u_int32  ulBoxSize;
  void    *pBox = NULL;

  if(fCryptValid == FALSE)
  {
     PRINTF("Crypto_Open:Init crypto module"CRLF);
     //
     // Init the whole module
     //
     GEN_MEMSET(stCrypt, 0, sizeof(CRYPTO) * CRYPT_MAX_HANDLES);
     fCryptValid = TRUE;
  }
  for(tHandle=0; tHandle<CRYPT_MAX_HANDLES; tHandle++)
  {
     if(stCrypt[tHandle].tLevel == CRYPT_LEVEL_NONE)
     {
        break;
     }
  }
  if(tHandle == CRYPT_MAX_HANDLES) return(CRYPT_ILLEGAL_HANDLE);
  //
  switch(tLevel)
  {
     case CRYPT_LEVEL_DISK:
        // Use Disk level scrambling
        PRINTF("Crypto_Open:Init disk level"CRLF);
        if(crypto_InitDiskLevel(tMode) == FALSE)
        {
           tHandle = CRYPT_ILLEGAL_HANDLE;
        }
        break;

     case CRYPT_LEVEL_STREAM:
        // Use Stream level encryption
        PRINTF("Crypto_Open:Init stream level"CRLF);
        ulBoxSize = CryptoModule_GetBoxSize();
        pBox      = safemalloc(ulBoxSize);
        if(crypto_InitStreamLevel(pBox, tMode) == FALSE)
        {
           tHandle = CRYPT_ILLEGAL_HANDLE;
           pBox    = safefree(pBox);
        }
        break;

     case CRYPT_LEVEL_FILE:
        // Use File level encryption
        PRINTF("Crypto_Open:Init file level"CRLF);
        ulBoxSize = CryptoModule_GetBoxSize();
        pBox      = safemalloc(ulBoxSize);
        if(crypto_InitFileLevel(pBox, tMode) == FALSE)
        {
           tHandle = CRYPT_ILLEGAL_HANDLE;
           pBox    = safefree(pBox);
        }
        break;

     default:
        PRINTF("Crypto_Open:NO level!"CRLF);
        tLevel = CRYPT_LEVEL_NONE;
        break;
  }

  if(tHandle < CRYPT_MAX_HANDLES)
  {
     stCrypt[tHandle].tLevel  = tLevel;
     stCrypt[tHandle].tMode   = tMode;
     stCrypt[tHandle].ulDepth = ulDepth;
     stCrypt[tHandle].pBox    = pBox;
     return(tHandle+1);
  }
  else return(tHandle);
}

/*
 **
 *  Name:        Crypto_GetEncryptSize
 *  Description: Calculate the necessary size to encrypt a plaintext completely.
 *               Due to the blocklength this is might be bigger than the actual plaintext. 
 *  Arguments:   Plain size
 *
 *  Returns:     Actual required encryption size of the cryptdata
 *
 */
u_int32 Crypto_GetEncryptSize(u_int32 ulSize)
{
  return( CryptoModule_GetBlockSize(ulSize) );
}

/*
 **
 *  Name:        Crypto_Encrypt
 *  Description: Encrypt a block of data
 *  Arguments:   Handle, data ptr, size
 *
 *  Returns:     Actual encryption depth
 *
 */
u_int32 Crypto_Encrypt(CRYPTH tHandle, u_int8 *pubData, u_int32 ulSize)
{
   u_int32  ulActSize=0;

   if(tHandle == CRYPT_ILLEGAL_HANDLE) return(0);
   //
   if(--tHandle < CRYPT_MAX_HANDLES)
   {
      if(ulSize == CRYPT_DEFAULT_DEPTH)
      {
         //
         // Use default encryption depth
         //
         ulSize = stCrypt[tHandle].ulDepth;
      }

      switch(stCrypt[tHandle].tLevel)
      {
         case CRYPT_LEVEL_STREAM:
         case CRYPT_LEVEL_FILE:
            ulActSize = CryptoModule_Encrypt(stCrypt[tHandle].pBox, pubData, ulSize);
            break;

         case CRYPT_LEVEL_DISK:
         default:
            break;
      }
   }
   return(ulActSize);
}

/*
 **
 *  Name:        Crypto_Decrypt
 *
 *  Description: Decrypt a block of data
 *
 *  Arguments:   Handle, data ptr, size
 *
 *  Returns:     Actual decryption depth
 *
 */
u_int32 Crypto_Decrypt(CRYPTH tHandle, u_int8 *pubData, u_int32 ulSize)
{
   u_int32  ulActSize = 0;

   if(tHandle == CRYPT_ILLEGAL_HANDLE) return(0);
   //
   if(--tHandle < CRYPT_MAX_HANDLES)
   {
      if(ulSize == CRYPT_DEFAULT_DEPTH)
      {
         //
         // Use default encryption depth
         //
         ulSize = stCrypt[tHandle].ulDepth;
      }

      switch(stCrypt[tHandle].tLevel)
      {
         case CRYPT_LEVEL_STREAM:
         case CRYPT_LEVEL_FILE:
            ulActSize = CryptoModule_Decrypt(stCrypt[tHandle].pBox, pubData, ulSize);
            break;

         default:
            break;
      }
   }
   return(ulActSize);
}

/*------  Local functions separator ------------------------------------
__LOCAL_FUNCTIONS_________()
{};
----------------------------------------------------------------------------*/

/*
 **
 *  Name:        crypto_InitDiskLevel
 *
 *  Description: Init the Disk level encryption module
 *
 *  Arguments:   Mode
 *
 *  Returns:     TRUE if OKee
 *
 */
static bool crypto_InitDiskLevel(CRYPT_MODE tMode)
{
   return(TRUE);
}

/*
 **
 *  Name:        crypto_InitFileLevel
 *
 *  Description: Init the File level encryption module
 *
 *  Arguments:   Box, Mode
 *
 *  Returns:     TRUE if OKee
 *
 */
static bool crypto_InitFileLevel(void *pBox, CRYPT_MODE tMode)
{
  bool        fCc = FALSE;
  u_int8      pubKey[CRYPT_MAX_KEY_LENGTH];
  u_int16     usLength = 0;

  if(crypto_Selftest(pBox, tMode) == FALSE) return(FALSE);

  usLength = crypto_InitKeys(pubKey, tMode);

  if(usLength)
  {
     fCc = CryptoModule_FileSetup(pBox, pubKey, usLength);
  }
  return(fCc);
}

/*
 **
 *  Name:        crypto_InitKeys
 *
 *  Description: Init the encryption keys
 *
 *  Arguments:   Mode
 *
 *  Returns:     Actual key size
 *
 */
static u_int16 crypto_InitKeys(u_int8 *pubKey, CRYPT_MODE tMode)
{
   u_int16     usLength = 0;

   switch(tMode)
   {
      default:
      case CRYPT_MODE_NONE:
         break;

      case CRYPT_MODE_INTERNAL_KEY:
         usLength = CRYPT_INTERNAL_KEY_LENGTH;
         GEN_GetPublicKey(pubKey, CRYPT_STATIC_KEY_LENGTH);
         break;

      case CRYPT_MODE_EXTERNAL_KEY:
         // Use static key for now !!!
      case CRYPT_MODE_STATIC_KEY:
         usLength = CRYPT_STATIC_KEY_LENGTH;
         GEN_MEMCPY(pubKey, (u_int8 *)pubStaticKey, CRYPT_STATIC_KEY_LENGTH );
         break;

      case CRYPT_MODE_USER_KEY:
         if( (pubUserKey) && (ulUserLength <= CRYPT_MAX_KEY_LENGTH) )
         {
            usLength = (u_int16) ulUserLength;
            GEN_MEMCPY(pubKey, pubUserKey, usLength );
         }
         break;

      case CRYPT_MODE_STB_KEY:
         usLength = CRYPT_STB_KEY_LENGTH;
         GEN_MEMCPY(pubKey, (u_int8 *)pubStaticKey, CRYPT_STATIC_KEY_LENGTH );
         break;
   }
   return(usLength);
}

/*
 **
 *  Name:        crypto_InitStreamLevel
 *
 *  Description: Init the Stream level encryption module
 *
 *  Arguments:   Box, Mode
 *
 *  Returns:     TRUE if OKee
 *
 */
static bool crypto_InitStreamLevel(void *pBox, CRYPT_MODE tMode)
{
  bool        fCc = FALSE;
  u_int8      pubKey[CRYPT_MAX_KEY_LENGTH];
  u_int16     usLength = 0;

  if(crypto_Selftest(pBox, tMode) == FALSE) return(FALSE);

  usLength = crypto_InitKeys(pubKey, tMode);

  if(usLength)
  {
     fCc = CryptoModule_StreamSetup(pBox, pubKey, usLength);
  }
  return(fCc);
}

/*
 **
 *  Name:        crypto_Selftest
 *
 *  Description: Selftest the crypto system
 *
 *  Arguments:   Box, Mode
 *
 *  Returns:     TRUE if OKee
 *
 */
static bool crypto_Selftest(void *pBox, CRYPT_MODE tMode)
{
  bool fCc = TRUE;

#if defined(FEATURE_ENABLE_CRYPTO_TEST)
  //
  // Perform a Crypto_selftest
  //
  if( CryptoModule_Validate(pBox) )
  {
     LOG_printf("crypto_Selftest(): OKee"CRLF);
  }
  else              
  {
     LOG_printf("crypto_Selftest(): ERROR !"CRLF);
     fCc = FALSE;
  }
#endif // FEATURE_ENABLE_CRYPTO_TEST

   return(fCc);
}
