/*  (c) Copyright:  2017  Patrn ESS, Confidential Data 
 *
 *  Workfile:           mail_client.h
 *  Revision:          
 *  Modtime:           
 *
 *  Purpose:            Raspberry pi mail handler mail_client.c header file
 *
 *
 *
 *  Compiler/Assembler: Raspbian Linux GNU gcc
 *  Ext Packages:
 *
 *  Author:             Peter Hillen
 *  Date Created:       16 Nov 2017 ported from landog mail
 *
 *  Revisions:
 *  Entry Points:       
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
**/

#ifndef _MAIL_CLIENT_H_
#define _MAIL_CLIENT_H_


#define CLIENT_MAX_RECORD       255
#define CLIENT_BUFFER_SIZE      10000
#define MAIL_LIST_SIZE          20

typedef enum _client_communications_
{
  CL_COMM_MAIL_IN = 0,
  CL_COMM_MAIL_OUT,
  CL_COMM_ROUTER,

  NUM_CLIENT_COMMS
} CTYPE;

typedef struct _mailtags_
{
  int   iMaxMailNr;
  int   iActMailNr;
  int  *piMailNr;
} MTAGS;
   
typedef struct cinfo
{
   CTYPE          tType;               // CL_COMM_xxx
   ATYPE          tAuth;               // MAIL_SETUP_xxx Authentication 
   int            iPort;               // Port
   char          *pcServer;            // Name of the server
   char          *pcUser;              //    -Username
   char          *pcPass;              //    -Password
}  CINFO;

bool  CLIENT_Init                      (void);
char *CLIENT_GetServer                 (CTYPE);
int   CLIENT_GetPort                   (CTYPE);
char *CLIENT_GetUser                   (CTYPE);
bool  CLIENT_GetPassword               (CTYPE, char *, int);
bool  CLIENT_EncryptPassword           (CTYPE);
bool  CLIENT_ParameterChanged          (int);
void  CLIENT_Exit                      (void);
ATYPE CLIENT_Setup                     (CTYPE);

#endif  /*_MAIL_CLIENT_H_ */

