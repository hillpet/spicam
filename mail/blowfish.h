/*  (c) Copyright:  2017  Patrn ESS, Confidential Data 
 *
 *  Workfile:           blowfish.c
 *  Revision:          
 *  Modtime:           
 *
 *  Purpose:            Raspberry pi mail handler
 *
 *
 *
 *  Compiler/Assembler: Raspbian Linux GNU gcc
 *  Ext Packages:
 *
 *  Author:             Peter Hillen
 *  Date Created:       16 Nov 2017 ported from landog mail
 *
 *  Revisions:
 *  Entry Points:       
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
**/
#ifndef _BLOWFISH_H_
#define _BLOWFISH_H_

#define BF_BLOCK_SIZE               8
#define BF_BLOCK_MASK               0xFFFFFFF8
#define NUMBER_OF_ROUNDS            16
#define MAXKEYBYTES                 56     /* 448 bits */
#define P_BOXES                     18
#define S_BOXES                     4
#define S_ENTRIES                   256
//
typedef struct 
{
   u_int32    ulPbox   [P_BOXES];
   u_int32    ulSbox   [S_BOXES][S_ENTRIES];
}  BLOW;


extern const u_int32 ulDefaultPbox[P_BOXES];
extern const u_int32 ulDefaultSbox[S_BOXES][S_ENTRIES];


#endif   //_BLOWFISH_H_




