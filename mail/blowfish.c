/*  (c) Copyright:  2017  Patrn ESS, Confidential Data 
 *
 *  Workfile:           blowfish.c
 *  Revision:          
 *  Modtime:           
 *
 *  Purpose:            Raspberry pi blowfish symmetrical encryption
 *
 *
 *
 *  Compiler/Assembler: Raspbian Linux GNU gcc
 *  Ext Packages:
 *
 *  Author:             Peter Hillen
 *  Date Created:       16 Nov 2017 ported from landog mail
 *
 *  Entry Points:       CryptoModule_FileSetup()
 *                      CryptoModule_StreamSetup()
 *                      CryptoModule_Encrypt()
 *                      CryptoModule_Decrypt()
 *                      CryptoModule_GetBlockSize()
 *                      CryptoModule_GetBoxSize()
 *                      CryptoModule_Validate()
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
**/
#include <string.h>

#include <common.h>
#include "../config.h"
#include "../globals.h"
#include "blowfish.h"

//#define  USE_PRINTF
#include <printf.h>


static u_int32 FunBlow              (BLOW *, u_int32);

#if defined(FEATURE_ENABLE_CRYPTO_TEST)
static const u_int8  ubTestIn[8]  = { 0x00, 0x00, 0x00, 0x01, 0x00, 0x00, 0x00, 0x02 };
static const u_int8  ubTestOut[8] = { 0xDF, 0x33, 0x3F, 0xD2, 0x30, 0xA7, 0x1B, 0xB4 };
#endif // FEATURE_ENABLE_CRYPTO_TEST

/**
 **
 **  Name:        CryptoModule_GetBlockSize
 **  Description: Calculate the actual blocksize needed for the whole plaintext
 **  Arguments:   True plainsize (if asciiz buffer)
 **
 **  Returns:     Actuale required blocksize
 **/

u_int32 CryptoModule_GetBlockSize(u_int32 ulSize) 
{
  u_int32 ulActSize;

  //
  // The algorithm is a 64 bits block-cipher, so the input stream needs to be MOD(8). 
  // To encrypt the whole block, a larger buffer is needed.
  //
  ulActSize  = ulSize+BF_BLOCK_SIZE-1;   // Plus blocksize
  ulActSize &= BF_BLOCK_MASK;            // Mod(8)
  return(ulActSize);
}

/**
 **
 **  Name:        CryptoModule_Encrypt
 **
 **  Description: Encryption algorithm 
 **
 **  Arguments:   Box ptr, buffer ptr, size
 **
 **  Returns:     Actuale encrypted size
 **/

u_int32 CryptoModule_Encrypt(BLOW *pstCtx, u_int8 *pubBuffer, u_int32 ulSize) 
{
   u_int32  ulXl;
   u_int32  ulXr;
   u_int32  ulTemp, ulActSize = ulSize;
   int16    i;

   if(!pstCtx) return(0);
   //
   // The algorithm is a 64 bits block-cipher, so the input stream needs to be MOD(8)
   //
   ulActSize &= BF_BLOCK_MASK;         // Mod(8) Mask 0xFFFFFFF8
   ulSize    /= BF_BLOCK_SIZE;         // Block size (probably 8)

   while(ulSize--)
   {
      ulXl = GEN_ulong_get(pubBuffer+0, 0xffffffffL, 0);
      ulXr = GEN_ulong_get(pubBuffer+4, 0xffffffffL, 0);
      //
      for (i = 0; i < NUMBER_OF_ROUNDS; ++i) 
      {
         ulXl   = ulXl ^ pstCtx->ulPbox[i];
         ulXr   = FunBlow(pstCtx, ulXl) ^ ulXr;
         ulTemp = ulXl;
         ulXl   = ulXr;
         ulXr   = ulTemp;
      }
      ulTemp = ulXl;
      ulXl   = ulXr;
      ulXr   = ulTemp;
      ulXr   = ulXr ^ pstCtx->ulPbox[NUMBER_OF_ROUNDS];
      ulXl   = ulXl ^ pstCtx->ulPbox[NUMBER_OF_ROUNDS + 1];
      //
      pubBuffer  = GEN_ulong_put(pubBuffer, ulXl);
      pubBuffer  = GEN_ulong_put(pubBuffer, ulXr);
   }
   return(ulActSize);
}

/**
 **
 **  Name:        CryptoModule_Decrypt
 **
 **  Description: Decryption algorithm 
 **
 **  Arguments:   Box ptr, buffer ptr, size
 **
 **  Returns:     Actuale decrypted size
 **/
u_int32 CryptoModule_Decrypt(BLOW *pstCtx, u_int8 *pubBuffer, u_int32 ulSize) 
{
   u_int32  ulXl;
   u_int32  ulXr;
   u_int32  ulTemp, ulActSize = ulSize;
   int16    i;

   if(!pstCtx) return(FALSE);

   //
   // The algorithm is a 64 bits block-cipher, so the input stream needs to be MOD(8)
   //
   ulActSize &= BF_BLOCK_MASK;         // Mod(8) Mask 0xFFFFFFF8
   ulSize    /= BF_BLOCK_SIZE;         // Block size (probably 8)

   while(ulSize--)
   {
      ulXl = GEN_ulong_get(pubBuffer+0, 0xffffffffL, 0);
      ulXr = GEN_ulong_get(pubBuffer+4, 0xffffffffL, 0);

      for (i = NUMBER_OF_ROUNDS + 1; i > 1; --i) 
      {
         ulXl = ulXl ^ pstCtx->ulPbox[i];
         ulXr = FunBlow(pstCtx, ulXl) ^ ulXr;

         /* Exchange Xl and Xr */
         ulTemp = ulXl;
         ulXl   = ulXr;
         ulXr   = ulTemp;
      }

      /* Exchange Xl and Xr */
      ulTemp = ulXl;
      ulXl   = ulXr;
      ulXr   = ulTemp;

      ulXr   = ulXr ^ pstCtx->ulPbox[1];
      ulXl   = ulXl ^ pstCtx->ulPbox[0];

      pubBuffer = GEN_ulong_put(pubBuffer, ulXl);
      pubBuffer = GEN_ulong_put(pubBuffer, ulXr);
   }
   return(ulActSize);
}

/**
 **
 **  Name:        CryptoModule_StreamSetup
 **
 **  Description: Setup the algorithm for streaming encryption
 **
 **  Arguments:   Box structure ptr, Key ptr, Key size
 **
 **  Returns:     TRUE if OKee
 **/
bool CryptoModule_StreamSetup(BLOW *pstCtx, u_int8 *pubKey, u_int16 usKeyLen) 
{
   u_int16  i, j, k;
   u_int32  ulData;
   u_int8   ubData[8];

   if(!pstCtx) return(FALSE);

   for (i = 0; i < S_BOXES; i++) 
   {
      for (j = 0; j < S_ENTRIES; j++)
      {
         pstCtx->ulSbox[i][j] = ulDefaultSbox[i][j];
      }
   }

   j = 0;

   for (i = 0; i < NUMBER_OF_ROUNDS + 2; ++i) 
   {
      ulData = 0x00000000;

      for (k = 0; k < 4; ++k) 
      {
         ulData = (ulData << 8) | pubKey[j];
         j++;
         if (j >= usKeyLen)
         {  
            j = 0;
         }
      }
      pstCtx->ulPbox[i] = ulDefaultPbox[i] ^ ulData;
   }

   GEN_MEMSET(ubData, 0, BF_BLOCK_SIZE);

   for (i = 0; i < NUMBER_OF_ROUNDS + 2; i += 2) 
   {
      CryptoModule_Encrypt(pstCtx, ubData, 8);
                                      
      pstCtx->ulPbox[i]     = GEN_ulong_get(ubData+0, 0xffffffffL, 0);
      pstCtx->ulPbox[i + 1] = GEN_ulong_get(ubData+4, 0xffffffffL, 0);
   }

   for (i = 0; i < S_BOXES; ++i) 
   {
      for (j = 0; j < S_ENTRIES; j += 2) 
      {
         CryptoModule_Encrypt(pstCtx, ubData, 8);

         pstCtx->ulSbox[i][j]     = GEN_ulong_get(ubData+0, 0xffffffffL, 0);
         pstCtx->ulSbox[i][j + 1] = GEN_ulong_get(ubData+4, 0xffffffffL, 0);
      }
   }
   return(TRUE);
}


/**
 **
 **  Name:        CryptoModule_FileSetup
 **
 **  Description: Setup the algorithm for File encryption
 **
 **  Arguments:   Box structure ptr, Key ptr, Key size
 **
 **  Returns:     TRUE if OKee
 **/
bool CryptoModule_FileSetup(BLOW *pstCtx, u_int8 *pubKey, u_int16 usKeyLen) 
{
   u_int16  i, j, k;
   u_int32  ulData;
   u_int8   ubData[8];

   if(!pstCtx) return(FALSE);

   for (i = 0; i < S_BOXES; i++) 
   {
      for (j = 0; j < S_ENTRIES; j++)
      {
         pstCtx->ulSbox[i][j] = ulDefaultSbox[i][j];
      }
   }

   j = 0;

   for (i = 0; i < NUMBER_OF_ROUNDS + 2; ++i) 
   {
      ulData = 0x00000000;

      for (k = 0; k < 4; ++k) 
      {
         ulData = (ulData << 8) | pubKey[j];
         j++;
         if (j >= usKeyLen)
         {  
            j = 0;
         }
      }
      pstCtx->ulPbox[i] = ulDefaultPbox[i] ^ ulData;
   }

   GEN_MEMSET(ubData, 0, BF_BLOCK_SIZE);

   for (i = 0; i < NUMBER_OF_ROUNDS + 2; i += 2) 
   {
      CryptoModule_Encrypt(pstCtx, ubData, 8);
                                      
      pstCtx->ulPbox[i]     = GEN_ulong_get(ubData+0, 0xffffffffL, 0);
      pstCtx->ulPbox[i + 1] = GEN_ulong_get(ubData+4, 0xffffffffL, 0);
   }

   for (i = 0; i < S_BOXES; ++i) 
   {
      for (j = 0; j < S_ENTRIES; j += 2) 
      {
         pstCtx->ulSbox[i][j]     = GEN_ulong_get(ubData+0, 0xffffffffL, 0);
         pstCtx->ulSbox[i][j + 1] = GEN_ulong_get(ubData+4, 0xffffffffL, 0);
      }
   }
   return(TRUE);
}

/**
 **
 **  Name:        CryptoModule_GetBoxSize
 **
 **  Description: Retrieve the generic expanded key size
 **
 **  Arguments:   void
 **
 **  Returns:     Total box size
 **/

u_int32 CryptoModule_GetBoxSize(void) 
{
   return( sizeof(BLOW) );
}

#if defined(FEATURE_ENABLE_CRYPTO_TEST)

/**
 **
 **  Name:        CryptoModule_Validate
 **
 **  Description: Test calling sequence of blowfish encryption/decryption
 **
 **  Arguments:   void
 **
 **  Returns:     TRUE if OKee
 **/

bool CryptoModule_Validate(BLOW *pstCtx) 
{
   u_int8 ubBuffer[8];

   CryptoModule_StreamSetup(pstCtx, (u_int8 *)"TESTKEY", 7);

   GEN_MEMCPY(ubBuffer, (u_int8 *)ubTestIn, 8);

   CryptoModule_Encrypt(pstCtx, ubBuffer, 8);
   if( GEN_MEMCMP(ubBuffer, (u_int8 *)ubTestOut, 8) != 0) return (FALSE);

   CryptoModule_Decrypt(pstCtx, ubBuffer, 8);
   if( GEN_MEMCMP(ubBuffer, (u_int8 *)ubTestIn,  8) != 0) return (FALSE);
   else                                                   return (TRUE);
}

#endif // FEATURE_ENABLE_CRYPTO_TEST


/*------ Brief Local functions separator ------------------------------------
___Static_Funtions_________(){};
----------------------------------------------------------------------------*/

/**
 **
 **  Name:        FunBlow
 **
 **  Description: General Blowfish algorithm crypt function
 **
 **  Arguments:   void
 **
 **  Returns:     TRUE if 
 **/

static u_int32 FunBlow(BLOW *pstCtx, u_int32 x) 
{
   u_int16  a, b, c, d;
   u_int32  y;


   d = x & 0x00FF;
   x >>= 8;
   c = x & 0x00FF;
   x >>= 8;
   b = x & 0x00FF;
   x >>= 8;
   a = x & 0x00FF;

   y = pstCtx->ulSbox[0][a] + pstCtx->ulSbox[1][b];

   y = y ^ pstCtx->ulSbox[2][c];
   y = y + pstCtx->ulSbox[3][d];

   return(y);
}


