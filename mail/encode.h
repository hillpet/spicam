/*  (c) Copyright:  2017  Patrn ESS, Confidential Data 
 *
 *  Workfile:           encode.h
 *  Revision:          
 *  Modtime:           
 *
 *  Purpose:            Raspberry pi mail handler mime encoder
 *
 *
 *
 *  Compiler/Assembler: Raspbian Linux GNU gcc
 *  Ext Packages:
 *
 *  Author:             Peter Hillen
 *  Date Created:       16 Nov 2017 ported from landog mail
 *
 *  Revisions:
 *  Entry Points:       
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
**/

#ifndef _ENCODE_H_
#define _ENCODE_H_

#define MIME_LENGTH              77          // Breakup MIME encoding 76 chars
//
char    *MIME_Encode64           (const u_int8 *, int, char *, int *);
u_int8  *MIME_Decode64           (const char *, int, u_int8 *, int *);
//
char    *MIME_Bin2Ascii          (char *, char *, int);
char    *MIME_Ascii2Bin          (char *, char *, int);
char    *MIME_Splitup            (char *, int);

#endif /* _ENCODE_H_ */
