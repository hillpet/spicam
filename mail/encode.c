/*  (c) Copyright:  2017  Patrn ESS, Confidential Data 
 *
 *  Workfile:           encode.c
 *  Revision:          
 *  Modtime:           
 *
 *  Purpose:            Raspberry pi mail handler
 *
 *
 *
 *  Compiler/Assembler: Raspbian Linux GNU gcc
 *  Ext Packages:
 *
 *  Author:             Peter Hillen
 *  Date Created:       16 Nov 2017 ported from landog mail
 *
 *  Revisions:
 *  Entry Points:       
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
**/

#include <stdio.h>
#include <stdarg.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>

#include <common.h>
#include "../config.h"
#include "../globals.h"
#include "encode.h"

//#define USE_PRINTF
#include <printf.h>

//
// Base64 decode LUT
//
static const u_int8 pubDecodeTable[256] =
{
//   0    1    2    3    4    5    6    7    8    9
//-------------------------------------------------------------
     0,   0,   0,   0,   0,   0,   0,   0,   0,   0,     //  00
     0,   0,   0,   0,   0,   0,   0,   0,   0,   0,     //  10
     0,   0,   0,   0,   0,   0,   0,   0,   0,   0,     //  20
     0,   0,   0,   0,   0,   0,   0,   0,   0,   0,     //  30
     0,   0,   0,  62,  63,   0,   0,   0,  52,  53,     //  40
    54,  55,  56,  57,  58,  59,  60,  61,   0,   0,     //  50
     0,   0,   0,   0,   0,   0,   1,   2,   3,   4,     //  60
     5,   6,   7,   8,   9,  10,  11,  12,  13,  14,     //  70
    15,  16,  17,  18,  19,  20,  21,  22,  23,  24,     //  80
    25,   0,   0,   0,   0,   0,   0,  26,  27,  28,     //  90
    29,  30,  31,  32,  33,  34,  35,  36,  37,  38,     // 100
    39,  40,  41,  42,  43,  44,  45,  46,  47,  48,     // 110
    49,  50,  51,   0,   0,   0,   0,   0,   0,   0,     // 120
     0,   0,   0,   0,   0,   0,   0,   0,   0,   0,     // 130
     0,   0,   0,   0,   0,   0,   0,   0,   0,   0,     // 140
     0,   0,   0,   0,   0,   0,   0,   0,   0,   0,     // 150
     0,   0,   0,   0,   0,   0,   0,   0,   0,   0,     // 160
     0,   0,   0,   0,   0,   0,   0,   0,   0,   0,     // 170
     0,   0,   0,   0,   0,   0,   0,   0,   0,   0,     // 180
     0,   0,   0,   0,   0,   0,   0,   0,   0,   0,     // 190
     0,   0,   0,   0,   0,   0,   0,   0,   0,   0,     // 200
     0,   0,   0,   0,   0,   0,   0,   0,   0,   0,     // 210
     0,   0,   0,   0,   0,   0,   0,   0,   0,   0,     // 220
     0,   0,   0,   0,   0,   0,   0,   0,   0,   0,     // 230
     0,   0,   0,   0,   0,   0,   0,   0,   0,   0,     // 240
     0,   0,   0,   0,   0,   0                          // 250
};

//
// Base64 encode LUT
//
static char pcEncodeTable[64] = 
{
      'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H',
      'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P',
      'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X',
      'Y', 'Z', 'a', 'b', 'c', 'd', 'e', 'f',
      'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n',
      'o', 'p', 'q', 'r', 's', 't', 'u', 'v',
      'w', 'x', 'y', 'z', '0', '1', '2', '3',
      '4', '5', '6', '7', '8', '9', '+', '/'
};
//
static const int iModTable[] = {0, 2, 1};

//
// Local prototypes
//
static char   *mime_ToAscii   (char *, u_int8 *);
static char   *mime_ToBin     (u_int8 *, char *);
 
//
// Function:   MIME_Encode64
// Purpose:    Base64 encode a buffer
// Parms:      Input, InputLength, Output, OutputLength-Ptr
//
// Returns:    Output buffer and Length
// Notes:      If the output buffer is NULL, a new Output buffer will be allocated
//             of the exact length of the base64 digest plus a terminating '\0' !
//
char *MIME_Encode64(const u_int8 *pubIn, int iLenIn, char *pcOut, int *piLenTot) 
{
   int   i, j;
   int   iLenOut, iLenTot;
   char *pcMime;

   if(piLenTot == NULL) return(NULL);
   //
   // Calculate MIME buffer size
   // Calculate MIME Splitup
   //
   iLenOut = ((iLenIn/3)+2)*4;
   iLenTot = iLenOut + ((iLenOut/MIME_LENGTH)+1)*2;
   //
   PRINTF3("MIME_Encode64():In=%d, Mime=%d, Split=%d" CRLF, iLenIn, iLenOut, iLenTot);
   //
   if(pcOut == NULL)
   {
      //
      // Output buffer not specified: allocate here and return to caller.
      // Caller needs to free the buffer !
      //
      pcOut = safemalloc(iLenTot + 1);
   }
   else
   {
      //
      // Wrong encode buffer length
      //
      if(*piLenTot < iLenTot+1) return(NULL);
   }
   //
   for (i=0, j=0; i<iLenIn; ) 
   {
      u_int32 ulOctetA = i < iLenIn ? (u_int32)pubIn[i++] : 0;
      u_int32 ulOctetB = i < iLenIn ? (u_int32)pubIn[i++] : 0;
      u_int32 ulOctetC = i < iLenIn ? (u_int32)pubIn[i++] : 0;
 
      u_int32 ulTriple = (ulOctetA << 0x10) + (ulOctetB << 0x08) + ulOctetC;
 
      pcOut[j++] = pcEncodeTable[(ulTriple >> 3 * 6) & 0x3F];
      pcOut[j++] = pcEncodeTable[(ulTriple >> 2 * 6) & 0x3F];
      pcOut[j++] = pcEncodeTable[(ulTriple >> 1 * 6) & 0x3F];
      pcOut[j++] = pcEncodeTable[(ulTriple >> 0 * 6) & 0x3F];
   }
   for(i=0; i<iModTable[iLenIn % 3]; i++) pcOut[iLenOut - 1 - i] = '=';
   //
   // Terminate output
   //
   pcOut[iLenOut+1] = '\0';
   //
   // Break up in MIME_LENGTH lines
   //
   pcMime    = MIME_Splitup(pcOut, iLenOut);
   *piLenTot = iLenTot;
   return(pcMime);
}
 
 
//
// Function:   MIME_Encode64
// Purpose:    Base64 decode a buffer
// Parms:      Input, InputLength, Output, OutputLength-Ptr
//
// Returns:    Output buffer and Length
// Notes:      If the output buffer is NULL, a new Output buffer will be allocated
//             of the exact length of the original data
//
u_int8 *MIME_Decode64(const char *pcIn, int iInLen, u_int8 *pubOut, int *piOutLen) 
{
   int   i, j;
   int   iOutLen=(iInLen/4)*3;
 
   if(piOutLen == NULL) return(NULL);
   if(iInLen % 4 != 0)  return NULL;
   //
   if(pubOut == NULL) pubOut = safemalloc(iOutLen);
   else
   {
      //
      // Wrong decode buffer length
      //
      if(*piOutLen < iOutLen) return(NULL);
   }
   //
   if (pcIn[iInLen - 1] == '=') iOutLen--;
   if (pcIn[iInLen - 2] == '=') iOutLen--;
 
 
   for (i=0, j=0; i<iInLen; ) 
   {
      u_int32 ulSextetA = pcIn[i] == '=' ? 0 & i++ : (u_int32)pubDecodeTable[(int)pcIn[i++]];
      u_int32 ulSextetB = pcIn[i] == '=' ? 0 & i++ : (u_int32)pubDecodeTable[(int)pcIn[i++]];
      u_int32 ulSextetC = pcIn[i] == '=' ? 0 & i++ : (u_int32)pubDecodeTable[(int)pcIn[i++]];
      u_int32 ulSextetD = pcIn[i] == '=' ? 0 & i++ : (u_int32)pubDecodeTable[(int)pcIn[i++]];
 
      u_int32 ulTriple = (ulSextetA << 3 * 6) +
                         (ulSextetB << 2 * 6) +
                         (ulSextetC << 1 * 6) +
                         (ulSextetD << 0 * 6);
 
      if (j < iOutLen) pubOut[j++] = (ulTriple >> 2 * 8) & 0xFF;
      if (j < iOutLen) pubOut[j++] = (ulTriple >> 1 * 8) & 0xFF;
      if (j < iOutLen) pubOut[j++] = (ulTriple >> 0 * 8) & 0xFF;
   }
   *piOutLen = iOutLen;
   return(pubOut);
}

//
//  Function:   MIME_Bin2Ascii
//  Purpose:    Compress a binary buffer to Ascii string
//
//  Parms:      Dest, Src, Src length
//               Bin  -> 4161700A00 ("Aap\n")
//               Ascii-> 0x34, 0x31, 0x36, 0x31, 0x37, 0x30, 0x30, 0x41, 0x30, 0x30
//               Size = 5
//  Returns:    New dest
//
char *MIME_Bin2Ascii(char *pcAscii, char *pcBin, int iSize)
{
   do
   {
      pcAscii = mime_ToAscii(pcAscii, (u_int8 *)pcBin);
      pcBin++;
      iSize--;
   }
   while(iSize);
   //
   return(pcAscii);
}

//
//  Function:  MIME_Ascii2Bin
//  Purpose:   Rework ASCII representation to original data
//                Ascii-> 0x34, 0x31, 0x36, 0x31, 0x37, 0x30, 0x30, 0x41, 0x30, 0x30 (Size=10 bytes)
//                Data -> 41 61 70 0A 00 ("Aap\n")
//
//  Parms:      Dest, Src, Src length
//  Returns:    New dest ptr
//
char *MIME_Ascii2Bin(char *pcBin, char *pcAscii, int iSize)
{
   do
   {
      pcBin = mime_ToBin((u_int8 *)pcBin, pcAscii);
      pcAscii += 2;
      iSize   -= 2;
   }
   while(iSize);
   //
   return(pcBin);
}

//
//  Function:   MIME_Splitup
//  Purpose:    Split up MIME buffer by adding CrLf's 
//
//  Parms:      Original allocated Buffer, size
//  Returns:    New allocated Buffer
//
char *MIME_Splitup(char *pcOrgBuf, int iOrgLen)
{
   char *pcNewBuf;
   int   i, j=0;
   int   iNewLen = ((iOrgLen/MIME_LENGTH)+1)*(MIME_LENGTH+3);

   PRINTF2("MIME_Splitup():Org=%d, New=%d" CRLF, iOrgLen, iNewLen);
   pcNewBuf = (char*) safemalloc(iNewLen);
   for(i=0; i<iOrgLen; i++)
   {
      pcNewBuf[i+j] = pcOrgBuf[i];
      if(i>0 && ((i+1)%(MIME_LENGTH-1)) == 0)
      {
         pcNewBuf[i+j+1] = '\r';
         pcNewBuf[i+j+2] = '\n';
         j+=2;
      }
   }
   j+=2;
   pcNewBuf[i+j+1] = '\r';
   pcNewBuf[i+j+2] = '\n';
   pcNewBuf[i+j+3] = '\0';
   //
   safefree(pcOrgBuf);
   PRINTF("MIME_Splitup():OKee" CRLF);
   return(pcNewBuf);
}


/* ======   Local Functions separator ===========================================
void ___local_functions(){}
==============================================================================*/

//
//  Function:   mime_ToAscii
//  Purpose:    Convert 8 bits byte to its ASCII components
//               pubChar -> 0x41
//               pcAscii -> "41" or 0x34, 0x31
//
//  Parms:      Dest, Src
//  Returns:    New dest
//
static char *mime_ToAscii(char *pcAscii, u_int8 *pubChar)
{
   char cChar;

   cChar = (*pubChar>>4) & 0x0F;
   if(cChar > 9) cChar += 55;   // A..F
   else          cChar += 48;   // 0..9
   *pcAscii++ = cChar;

   cChar = *pubChar & 0x0F;
   if(cChar > 9) cChar += 55;   // A..F
   else          cChar += 48;   // 0..9
   *pcAscii++ = cChar;
   //
   return(pcAscii);
}

//
//  Function:   mime_ToBin
//  Purpose:    Convert ASCII components to its 8 bits byte
//               pcChar -> "A9....." or 0x41, 0x39, .....
//               pubChar-> 0xA9
//  Parms:      Dest, Src
//  Returns:    New dest
//
static char *mime_ToBin(u_int8 *pubChar, char *pcChar)
{
   u_int8 ubChar=0;

   if     ( (*pcChar >= '0') && (*pcChar <= '9') ) ubChar = (*pcChar - 48);
   else if( (*pcChar >= 'A') && (*pcChar <= 'F') ) ubChar = (*pcChar - 55);

   pcChar++;
   ubChar <<= 4;

   if     ( (*pcChar >= '0') && (*pcChar <= '9') ) ubChar |= (*pcChar - 48);
   else if( (*pcChar >= 'A') && (*pcChar <= 'F') ) ubChar |= (*pcChar - 55);

   *pubChar++ = ubChar;
   //
   return((char *)pubChar);
}

 
