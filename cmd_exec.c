/*  (c) Copyright:  2017..2018  Patrn, Confidential Data 
 *
 *  Workfile:           cmd_exec.c
 *  Revision:          
 *  Modtime:           
 *
 *  Purpose:            Handle the execution of utilities and helper threads
 *                      Split off from cmd_main.c
 *
 *
 *  Compiler/Assembler: Raspbian Linux GNU gcc
 *  Ext Packages:
 *
 *  Author:             Peter Hillen
 *  Date Created:       16 Apr 2017
 *
 *  Revisions:
 *  Entry Points:       
 *  Notes:
 *
 *          G_pcDetectMode (Motion Detect) modes
 *          ==================================================================================================================
 *          'R':  "Restart"   Used by PiCam at the startup phase
 *          'O':  "Off"       MOTION_MODE_OFF
 *                            MD is OFF
 *          'M':  "Manual"    MOTION_MODE_MANUAL
 *                            Server will setup MD using PiCam parameters. Read the initial frames to determine
 *                            the background SAD and then NUM_FRAMES starting with a non-zero frame, which will be written to 
 *                            file.
 *                            non-zero Frame: pstMv->iCalMotionVectors > iTrigger1
 *          'C':  "Cycle"     MOTION_MODE_CYCLE
 *                            Same as Manual, but will now restart the sequence after completion.
 *          'P':  "Picture "  MOTION_MODE_PIXTURE
 *                            Server will setup MD using PiCam parameters. If MD triggered it will make 
 *                            a snapshot using current parameters and create a second normalized b&w picture to 
 *                            augment the MD vectors onto it. It will restart MD after both pictures have been stored.
 *          'A':  "Augment"   MOTION_MODE_AUGMENT
 *                            PiCam will poll Server every RPI_POLL_TIME secs and download plus display 
 *                            both last-picture plus augmented picture and the VirtS if MD was triggered, 
 *                            which can be detected by monitoring the MotionCounter changes.
 *          'V':  "Video"     MOTION_MODE_VIDEO
 *                            Server will setup MD using PiCam parameters. If MD triggered it will save the
 *                            last xx secords of video to file.
 *          'S':  "Stream"    MOTION_MODE_STREAM
 *                            TBD.
 *       
 *          VirtS (Virtual Screen): Server will transform the MV's into an ASCII representative which
 *          will be copied into a block of memory, the Virtual Screen. This screen will be 
 *          converted to a JSON object that can be read using the <motion.json> method.
 *       
 *          PID_HELP                                     PID_UTIL
 *          ========                                     ========
 *          hlp_RunHelper()                              Start raspivid -x
 *             VEC_InitMotion()                              Frames
 *             hlp_WaitForMotion()                              |
 *       +--->    VEC_ReadMotionFrame() <-----------------------+
 *       |        hlp_HandleTrigger()                         exit()
 *       +-----------(no motion)
 *                   
 *                   MOTION_MODE_OFF:
 *                      hlp_ReportMotion()
 *                   
 *                   MOTION_MODE_VIDEO:
 *                      hlp_HandleMotion()
 *                         hlp_DetermineMotion()
 *                         hlp_HandleOutput() --> Relays
 *                      hlp_ReportMotion()
 *                      Save recordings
 *                      hlp_HandleTriggerSnapshot()
 *                   
 *                   MOTION_MODE_CYCLE:
 *                      hlp_ReportMotion()
 *                   
 *                   MOTION_MODE_STREAM:
 *                   MOTION_MODE_MANUAL:
 *                      hlp_ReportMotion()
 *                   
 *                   MOTION_MODE_AUGMENT:
 *                      hlp_HandleMotion()
 *                         hlp_DetermineMotion()
 *                         hlp_HandleOutput() --> Relays
 *                      hlp_ReportMotion()
 *                   
 *                   MOTION_MODE_PICTURE:
 *                      hlp_HandleTriggerSnapshot()
 *                      hlp_HandleMotion()
 *                         hlp_DetermineMotion()
 *                         hlp_HandleOutput() --> Relays
 *                      hlp_ReportMotion()
 *                      GRF_AugmentMotionVectorFile()
 *
 *                VEC_ExitMotion()
 *          exit()
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
**/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>
#include <sched.h>
#include <time.h>
#include <errno.h>

#include <common.h>
#include "config.h"
#include "globals.h"
#include "rpi_func.h"
#include "cam_main.h"
#include "cmd_main.h"
#include "gen_func.h"
#include "graphics.h"
//

//#define USE_PRINTF
#include <printf.h>

//
// Show the cmnd exec argumants
//
#ifdef   FEATURE_SHOW_EXEC_PARMS
static void    cmd_ShowExecParms       (char *, char **);
#define  CMD_SHOWEXECPARMS(x,y)        cmd_ShowExecParms(x,y)
#else  //FEATURE_SHOW_EXEC_PARMS
#define  CMD_SHOWEXECPARMS(x,y)
#endif //FEATURE_SHOW_EXEC_PARMS
//
// Local prototypes
//
static bool    cmd_ExecProcess            (GLOCMD *, const SARG *, int);
static char  **cmd_AllocateGlobalArgList  (char *, char *);
static char  **cmd_AllocateStaticArgList  (const SARG *, int);
static int     cmd_CountOptions           (char *);
static int     cmd_CopyOption             (char *, char *);
static void    cmd_FreeArgList            (char **);
//
static char   *cmd_GetArgGlobal           (const SARG *);
static char   *cmd_GetArgPort             (const SARG *);
static char   *cmd_GetArgVideo            (const SARG *);
static char   *cmd_GetArgNull             (const SARG *);
//
static bool    utl_RunUtility             (GLOCMD *, int, int, const SARG *, int);
static bool    hlp_RunHelper              (GLOCMD *, int, int, const SARG *, int);
//
#ifdef   FEATURE_CAMERA
static char   *cmd_GetArgFile             (const SARG *);
//
static bool    hlp_RunUtility             (GLOCMD *, const SARG *, int);
static void    hlp_WaitForMotion          (MVIMG *, GLOCMD *, int);
static MOTYPE  hlp_DetermineMotion        (MVIMG *, const char *);
static void    hlp_HandleMotion           (MVIMG *);
static bool    hlp_HandleOutput           (MOTYPE);
static void    hlp_RemoteIo               (int);
static bool    hlp_HandleTrigger          (MVIMG *, GLOCMD *, int);
static void    hlp_HandleTriggerSnapshot  (MVIMG *, GLOCMD *);
static int     hlp_SaveRamfiles           (MVIMG *, const char *);
static void    hlp_ReportMotion           (MVIMG *, MOTYPE);
static bool    hlp_WaitRunningUtil        (const char *);
static void    hlp_WriteFrame             (MVIMG *);
static void    hlp_RecordingPause         (int, MVIMG *, int);
static u_int32 hlp_RecordingGetSize       (void);
static void    hlp_RecordingResume        (int);
static void    hlp_RecordingStop          (int);
#endif   //FEATURE_CAMERA
//
// Global motion data
//
const char *pcMotionInput          = "motion_0";
const char *pcMotionAugment        = "motion_A";
const char *pcMotionVideo          = "motion_video";

//
// VLC streaming parameters
// Note: use FEATURE_HEXDUMP_VLC to run hexdump as a tool instead of cvlc.
//
//#define FEATURE_HEXDUMP_VLC
#ifdef  FEATURE_HEXDUMP_VLC
static const SARG stMotionVlcArguments[] =
{
// iLenArgs pcArg                                     pfGetArg
   {  0,    "hexdump",                                cmd_GetArgNull          },          // hexdump
// {  0,    "-b",                                     cmd_GetArgNull          },          // addr + 16x OOO OOO ... OOO 
// {  0,    "-d",                                     cmd_GetArgNull          },          // addr + 16x DDDDD DDDDD ... DDDDD
// {  0,    "-x",                                     cmd_GetArgNull          },          // addr + 16x HHHH HHHH ... HHHH
   {  0,    "-v",                                     cmd_GetArgNull          },          // dump on a single line
   {  0,    "-e",                                     cmd_GetArgNull          },          // 
   {  0,    "\"%x\r\"",                               cmd_GetArgNull          },          //
// {  0,    ">/dev/null",                             cmd_GetArgNull          },          // Illegal redirect !!! Crashes hexdump
// === last one needs to be NULL =========================================================// Arg list terminator
   {  0,    NULL,                                     NULL                    }
};

#else  //FEATURE_HEXDUMP_VLC
static const SARG stMotionVlcArguments[] =
{
// iLenArgs pcArg                                     pfGetArg
   {  0,    "cvlc",                                   cmd_GetArgNull          },          // Console version of vlc
// {  0,    "-vvv",                                   cmd_GetArgNull          },          // Verbose level max ? i.o -v 1....?
   {  0,    "-q",                                     cmd_GetArgNull          },          // Quietly !
   {  0,    "stream:///dev/stdin",                    cmd_GetArgNull          },          // input piped from raspivid
   {  0,    "--sout",                                 cmd_GetArgNull          },          //
   { -1,    "#standard{access=http,mux=ts,dst=:%s}",  cmd_GetArgPort          },          // 
   {  0,    ":demux=h264",                            cmd_GetArgNull          },          //
// {  0,    "2>/dev/null",                            cmd_GetArgNull          },          // Redirect errors to a black hole
// === last one needs to be NULL =========================================================// Arg list terminator
   {  0,    NULL,                                     NULL                    }
};
#endif //FEATURE_HEXDUMP_VLC
static const int iNumMotionVlcArguments = (sizeof(stMotionVlcArguments) / sizeof(SARG)) - 1;
//
// Motion Detect settings (Using Motion vectors)
//
static const SARG stMotionVideoArguments[] =
{
// iLenArgs pcArg                                     pfGetArg
   {  0,    RPI_VID_EXEC,                             cmd_GetArgNull          },          // raspivid
   {  0,    "-w",                                     cmd_GetArgNull          },          // Width
   {  0,    "-w",                                     cmd_GetArgGlobal        },          //  [Global parms]
   {  0,    "-h",                                     cmd_GetArgNull          },          // Height
   {  0,    "-h",                                     cmd_GetArgGlobal        },          //  [Global parms]
   {  0,    "-t",                                     cmd_GetArgNull          },          // Duration
   {  0,    "0",                                      cmd_GetArgNull          },          //  indefinitely
   {  0,    "-sg",                                    cmd_GetArgNull          },          // Segmented files
// {  0,    "-sg",                                    cmd_GetArgGlobal        },          //  [Global parms] OR
   {  0,    "20000",                                  cmd_GetArgNull          },          //  -sg 20000
   {  0,    "-wr",                                    cmd_GetArgNull          },          // Segment wrap around after X files
// {  0,    "-wr",                                    cmd_GetArgGlobal        },          //  [Global parms] OR
   {  0,    "3",                                      cmd_GetArgNull          },          //  -wr 3
   {  0,    "-rot",                                   cmd_GetArgNull          },          // Rotation
   {  0,    "-rot",                                   cmd_GetArgGlobal        },          //  [Global parm]
#ifdef FEATURE_ANNOTATE_TANDD
   {  0,    "-a",                                     cmd_GetArgNull          },          // Annotate
   {  0,    "12",                                     cmd_GetArgNull          },          //  time & Date
#endif   //FEATURE_ANNOTATE_TANDD
   {  0,    "-s",                                     cmd_GetArgNull          },          // Signal SIGUSR1/SIGUSR2
// {  0,    "-ss",                                    cmd_GetArgNull          },          // Shutterspeed
// {  0,    "-ss",                                    cmd_GetArgGlobal        },          //  [Global parm]
// {  0,    "-p",                                     cmd_GetArgNull          },          // Preview
// {  0,    "-p",                                     cmd_GetArgGlobal        },          //  [Global parm]
// {  0,    "1280,0,640,360",                         cmd_GetArgNull          },          //  OR manual: upper right corner
   {  0,    "-x",                                     cmd_GetArgNull          },          // Output MVs
   {  0,    "-",                                      cmd_GetArgNull          },          //  to stdout
// {  0,    "-o",                                     cmd_GetArgNull          },          // Output h264
// {  0,    "/dev/null",                              cmd_GetArgNull          },          //  to null
   {  0,    "-o",                                     cmd_GetArgNull          },          // Output h264
   {  0,    "%s",                                     cmd_GetArgVideo         },          //  to RAMFS/Timestamp_%d.h264
// === last one needs to be NULL =========================================================// Arg list terminator
   {  0,    NULL,                                     NULL                    }
};
static const int iNumMotArguments = (sizeof(stMotionVideoArguments) / sizeof(SARG)) - 1;

#ifdef   FEATURE_CAMERA
static const SARG stMotionPixArguments[] =
{
// iLenArgs pcArg                                     pfGetArg
   {  0,    RPI_PIX_EXEC,                             cmd_GetArgNull          },          // raspistill
   {  0,    "-w",                                     cmd_GetArgNull          },          // Width
   {  0,    "-w",                                     cmd_GetArgGlobal        },          //  [Global parms]
   {  0,    "-h",                                     cmd_GetArgNull          },          // Height
   {  0,    "-h",                                     cmd_GetArgGlobal        },          //  [Global parms]
   {  0,    "-rot",                                   cmd_GetArgNull          },          // Rotation
   {  0,    "-rot",                                   cmd_GetArgGlobal        },          //  [Global parm]
   {  0,    "-t",                                     cmd_GetArgNull          },          // Duration
   {  0,    "-t",                                     cmd_GetArgGlobal        },          //  [Global parm]
// {  0,    "-ss",                                    cmd_GetArgNull          },          // Shutterspeed
// {  0,    "-ss",                                    cmd_GetArgGlobal        },          //  [Global parm]
#ifdef FEATURE_ANNOTATE_TANDD
   {  0,    "-a",                                     cmd_GetArgNull          },          // Annotate
   {  0,    "12",                                     cmd_GetArgNull          },          //  time & Date
#endif   //FEATURE_ANNOTATE_TANDD
// {  0,    "-p",                                     cmd_GetArgNull          },          // Preview
// {  0,    "-p",                                     cmd_GetArgGlobal        },          //  [Global parms]
// {  0,    "-p",                                     cmd_GetArgNull          },          // preview
// {  0,    "1280,0,640,360",                         cmd_GetArgNull          },          //  upper right corner
   {  0,    "-n",                                     cmd_GetArgNull          },          // No preview
// {  0,    "-tl",                                    cmd_GetArgNull          },          // Timelapse
// {  0,    "200",                                    cmd_GetArgNull          },          //  200 msec
// {  0,    "-t",                                     cmd_GetArgNull          },          // Delay
// {  0,    "2000",                                   cmd_GetArgNull          },          //  10 pix
   {  0,    "-ex",                                    cmd_GetArgNull          },          // Exposure mode
   {  0,    "auto",                                   cmd_GetArgNull          },          //  auto
   {  0,    "-o",                                     cmd_GetArgNull          },          // Output
   {  0,    "%s",                                     cmd_GetArgFile          },          //   filename
// === last one needs to be NULL =========================================================// Arg list terminator
   {  0,    NULL,                                     NULL                    }
};
static const int iNumMotionPixArguments = (sizeof(stMotionPixArguments) / sizeof(SARG)) - 1;
//
static const char *pcRaspiVid      = RPI_RASPIVID;
static const char *pcMotionMask    = RPI_PUBLIC_IMAGES "motion_mask";
static const char *pcMotionReport  = RPI_MREP_PATH;
static const char *pcDirVideo      = RPI_PUBLIC_VIDEO;
//
static const char *pcMotionType[NUM_MOTIONS]  =
{
   "NONE  ",
   "NOISE ",
   "TINY  ",
   "SMALL ",
   "MEDIUM",
   "LARGE ",
   "HUGE  ",
   "STOP  ",
   "ERROR "
};
#endif   //FEATURE_CAMERA


/*------  Local functions separator ------------------------------------
__GLOBAL_FUNCTIONS_________(){};
----------------------------------------------------------------------------*/

//
// Function:   CMD_BashProcess
// Purpose:    Split this thread and run the desired function through bash. Kill any ongoing
//             process first ! 
// Parms:      
// Returns:    Get Apps and Args from Global mmap
// Note:       TRUE if OKee
//
bool CMD_BashProcess(GLOCMD *pstCmd)
{
   bool     fCc=TRUE;
   int      iCc;
   pid_t    tPid;
   char    *pcShell;

   GLOBAL_DUMPCOMMAND("CMD-BashProcess():PID_CMND(dump)", pstCmd);
   //
   GEN_KillProcess(GLOBAL_PidGet(PID_HELP), NO_FRIENDLY, KILL_TIMEOUT_FORCED);
   GEN_WaitProcess(GLOBAL_PidGet(PID_HELP));
   GLOBAL_PidPut(PID_HELP, 0);
   //
   GEN_KillProcess(GLOBAL_PidGet(PID_UTIL), NO_FRIENDLY, KILL_TIMEOUT_FORCED);
   GEN_WaitProcess(GLOBAL_PidGet(PID_UTIL));
   GLOBAL_PidPut(PID_UTIL, 0);
   //==========================================================================
   // Split process : 
   //       CMND thread returns with tPid of the HELP thread
   //       HELP thread returns with tPid=0
   //
   tPid = fork();
   //
   //==========================================================================
   switch(tPid)
   {
      case 0:
         GLOBAL_DUMPCOMMAND("CMD-BashProcess():PID_HELP(inherit)", pstCmd);
         //
         pcShell = (char *) safemalloc(MAX_CMDLINE_LEN);
         GEN_SPRINTF(pcShell, "%s %s", pstCmd->pcExec, pstCmd->pcArgs);
         PRINTF1("CMD-BashProcess():Helper system(%s)" CRLF, pcShell);
         iCc = system(pcShell);
         PRINTF1("CMD-BashProcess():Helper completion: cc=%d" CRLF, iCc);
         GLOBAL_PidPut(PID_HELP, 0);
         safefree(pcShell);
         _exit(iCc);

      case -1:
         // Error
         fCc = FALSE;
         break;
            
      default:
         // CMND
         PRINTF1("CMD-BashProcess():CMND thread returning: HELPER pid=%d" CRLF, tPid);
         GLOBAL_PidPut(PID_HELP, tPid);
         break;
   }
   return(fCc);
}

//
// Function:   CMD_ExecProcess
// Purpose:    
//             
// Parms:      Command struct
// Returns:    TRUE if OKee
// Note:       
//
bool CMD_ExecProcess(GLOCMD *pstCmd)
{
   bool  fCc=FALSE;

   switch(pstCmd->iCommand)
   {
      default:
         break;

      case CAM_CMD_ALM_MOTION:
      case CAM_CMD_M_VECTOR:
      case CAM_CMD_A_VECTOR:
         fCc = cmd_ExecProcess(pstCmd, stMotionVideoArguments, iNumMotArguments);
         break;

      case CAM_CMD_STREAM:
         fCc = cmd_ExecProcess(pstCmd, stMotionVlcArguments, iNumMotionVlcArguments);
         break;

      case CAM_CMD_TIMELAPSE:
      case CAM_CMD_MAN_RECORD:
      case CAM_CMD_ALM_RECORD:
         fCc = cmd_ExecProcess(pstCmd, NULL, 0);
         break;
   }
   return(fCc);
}

/*------  Local functions separator ------------------------------------
__ARGS_FUNCTIONS_________(){};
----------------------------------------------------------------------------*/

#ifdef   FEATURE_CAMERA
//
// Function:   cmd_GetArgFile
// Purpose:    Get Argument: Output filename
//             
// Parms:      Argument entry
// Returns:    Composed pcArgument
// Note:       The result is allocated memory: Since the OS will free this memory,
//             we should NOT use safemalloc() here as to not confuse our mallc admin.
//
static char *cmd_GetArgFile(const SARG *pstArg)
{
   int         iLenArg;
   char       *pcArg=NULL;

   GEN_SNPRINTF(pstMap->G_pcLastFile, MAX_PATH_LEN, "%s%s.%s", pstMap->G_pcRamDir, pcMotionInput, pstMap->G_pcPixFileExt);
   iLenArg = GEN_STRLEN(pstMap->G_pcLastFile);
   if(iLenArg)
   {
      //
      // We need to add the output filename to this argument
      //
      pcArg = (char *) malloc(iLenArg+1);
      //
      // Copy args and vars to destination
      //
      GEN_STRCPY(pcArg, pstMap->G_pcLastFile);
      //PRINTF1("cmd-GetArgFile():Arg=%s" CRLF, pcArg);
   }
   return(pcArg);
}
#endif   //FEATURE_CAMERA

//
// Function:   cmd_GetArgGlobal
// Purpose:    Get Argument from the global list
//             
// Parms:      Argument entry
// Returns:    Composed pcArgument
// Note:       The result is allocated memory: Since the OS will free this memory,
//             we should NOT use safemalloc() here as to not confuse our mallc admin.
//
static char *cmd_GetArgGlobal(const SARG *pstArg)
{
   int         iLenArg, iLenVar;
   char       *pcOption;
   char       *pcArg=NULL;

   pcOption = GLOBAL_GetOption(pstArg->pcArg);
   if(pcOption)
   {
      iLenVar = GEN_STRLEN(pcOption);
      if(iLenVar)
      {
         //
         // We need to add the port number to this argument
         //       "Bla bla bla =%s bla bla"
         //
         iLenArg = iLenVar + 1;
         pcArg   = (char *) malloc(iLenArg);
         //
         // Copy args and vars to destination
         //
         GEN_SNPRINTF(pcArg, iLenArg, pcOption);
         //PRINTF1("cmd-GetArgGlobal():Arg=%s" CRLF, pcArg);
      }
   }
   return(pcArg);
}

//
// Function:   cmd_GetArgNull
// Purpose:    Get Argument: No variable settings
//             
// Parms:      Argument entry
// Returns:    Composed pcArgument
// Note:       The result is allocated memory: Since the OS will free this memory,
//             we should NOT use safemalloc() here as to not confuse our mallc admin.
//
static char *cmd_GetArgNull(const SARG *pstArg)
{
   int         iLenArg;
   const char *pcFormat;
   char       *pcArg=NULL;

   pcFormat = pstArg->pcArg;
   iLenArg  = GEN_STRLEN(pcFormat);
   if(iLenArg)
   {
      iLenArg += 1;
      pcArg    = (char *) malloc(iLenArg);
      //
      // Copy args and vars to destination
      //
      GEN_STRNCPY(pcArg, pcFormat, iLenArg);
      //PRINTF1("cmd-GetArgNull():Arg=%s" CRLF, pcArg);
   }
   return(pcArg);
}

//
// Function:   cmd_GetArgPort
// Purpose:    Get Argument: Streaming port nummer
//             
// Parms:      Argument entry
// Returns:    Composed pcArgument
// Note:       The result is allocated memory: Since the OS will free this memory,
//             we should NOT use safemalloc() here as to not confuse our mallc admin.
//
static char *cmd_GetArgPort(const SARG *pstArg)
{
   int         iLenArg, iLenVar;
   const char *pcFormat;
   char       *pcArg=NULL;

   pcFormat = pstArg->pcArg;
   iLenArg  = GEN_STRLEN(pcFormat);
   if(iLenArg)
   {
      iLenVar = GEN_STRLEN(pstMap->G_pcStreamPort);
      if(iLenVar)
      {
         //
         // We need to add the port number to this argument
         //       "Bla bla bla =%s bla bla"
         //
         iLenArg += iLenVar + 1;
         pcArg    = (char *) malloc(iLenArg);
         //
         // Copy args and vars to destination
         //
         GEN_SNPRINTF(pcArg, iLenArg, pcFormat, pstMap->G_pcStreamPort);
         //PRINTF1("cmd-GetArgPort(): Arg=%s" CRLF, pcArg);
      }
   }
   return(pcArg);
}

//
// Function:   cmd_GetArgVideo
// Purpose:    Get Argument: Video segmented filesname
//             
// Parms:      Argument entry
// Returns:    Composed pcArgument
// Note:       The result is allocated memory: Since the OS will free this memory,
//             we should NOT use safemalloc() here as to not confuse our mallc admin.
//             Video output filename:
//
//             -o /<G_pcRamDir>/motion_video_%1.h264
//
static char *cmd_GetArgVideo(const SARG *pstArg)
{
   int         iLenArg;
   char       *pcArg=NULL;
   char       *pcTemp=safemalloc(MAX_PATH_LEN);

   //
   // Build input image filename f.i. /mnt/rpipix/motion_video_%d.h264
   //
   GEN_SNPRINTF(pcTemp, MAX_PATH_LEN, "%s%s_%%d.h264", pstMap->G_pcRamDir, pcMotionVideo);
   //
   iLenArg  = GEN_STRLEN(pcTemp) + 1;
   pcArg    = (char *) malloc(iLenArg);
   //
   // Copy args and vars to destination
   //
   GEN_STRCPY(pcArg, pcTemp);
   PRINTF1("cmd-GetArgVideo(): Arg=%s" CRLF, pcTemp);
   safefree(pcTemp);
   return(pcArg);
}


/*------  Local functions separator ------------------------------------
__LOCAL_FUNCTIONS_________(){};
----------------------------------------------------------------------------*/

//
// Function:   cmd_ExecProcess
// Purpose:    Split this thread and execute the desired function. Kill any ongoing
//             process first ! 
// Parms:      Command, Pipiline args list, num args (zero = no pipe)
// Returns:    TRUE if OKee
// Note:       As long as we are running the PID_CMND thread we should get Apps and Args from the Command
//             struct. As soon as we have notified command execution down the ladder, helper threads are 
//             on their own and should get additional args from the global mmap struct !
//
static bool cmd_ExecProcess(GLOCMD *pstCmd, const SARG *pstArgs, int iNumArgs)
{
   bool     fCc=TRUE;
   int      iFds[2];
   pid_t    tPid;
   char   **ppcArgsList;
   
   //
   // Make sure CMD data is available to new thread !
   //
   GLOBAL_DUMPCOMMAND("cmd-ExecProcess():PID_CMND(dump)", pstCmd);
   //
   GEN_KillProcess(GLOBAL_PidGet(PID_UTIL), NO_FRIENDLY, KILL_TIMEOUT_FORCED);
   GEN_WaitProcess(GLOBAL_PidGet(PID_UTIL));
   GLOBAL_PidPut(PID_UTIL, 0);
   //
   GEN_KillProcess(GLOBAL_PidGet(PID_HELP), NO_FRIENDLY, KILL_TIMEOUT_FORCED);
   GEN_WaitProcess(GLOBAL_PidGet(PID_HELP));
   GLOBAL_PidPut(PID_HELP, 0);
   //
   tPid = fork();
   //
   //          |
   //    Command thread
   //          |              Help-Thread
   //          +-------------------+
   //          |                   |                  Util-Thread (local or extern)
   //          |                   +-----------------------+
   //          |                   |                       |
   //    PID_CMND             PID_HELP                 PID_UTIL
   //          |                   |                       |
   //          |                   |-<<<<-pipe--<<<<<<<<<--| : Motion Vectors
   //          |                   |->>>>-pipe-->>>>>>>>>--| : Streaming to socket
   //          |                   |                       |
   //      Notify CAM            exit                    exit
   //
   switch(tPid)
   {
      case 0:
         if(iNumArgs)
         {
            PRINTF1("cmd-ExecProcess():Fork:Helper:Fork Utility (%d args)" CRLF, iNumArgs);
            //====================================================================
            // Complex Helper thread: requires an additional tool to handle
            //
            // We have arguments for an utility thread to handle additional functionality.
            // To pass arguments to this thread, we need to create a pipeline to connect
            // the Helper thread to this utility.
            //====================================================================
            GLOBAL_DUMPCOMMAND("cmd-ExecProcess():PID_HELP(inherit):Spawn utility", pstCmd);
            //
            pipe(iFds);
            //
            tPid = fork();
            switch(tPid)
            {
               case 0:
                  //PRINTF("cmd-ExecProcess():Fork:Utility" CRLF);
                  //===========================================================
                  // Utility thread (either a local thread or external tool)
                  //===========================================================
                  utl_RunUtility(pstCmd, iFds[0], iFds[1], pstArgs, iNumArgs);
                  //
                  // We will most likely NOT return here, unless something went wrong
                  //
                  _exit(1);
         
               case -1:
                  //===========================================================
                  // Error
                  //===========================================================
                  _exit(1);

               default:
                  //PRINTF1("cmd-ExecProcess():Fork:Helper, Util has pid=%d" CRLF, tPid);
                  //===========================================================
                  // Helper thread: 
                  //===========================================================
                  GLOBAL_PidPut(PID_UTIL, tPid);
                  hlp_RunHelper(pstCmd, iFds[0], iFds[1], pstArgs, iNumArgs);
                  //
                  // In some cases, we will NOT return here, unless something went wrong
                  //
                  _exit(1);
            }
         }
         else
         {
            GLOBAL_DUMPCOMMAND("cmd-ExecProcess():PID_HELP(inherit):Stand alone", pstCmd);
            //PRINTF("cmd-ExecProcess():Fork:Helper:Alone" CRLF);
            //=================================================================
            // Simple Helper thread
            //=================================================================
            ppcArgsList = cmd_AllocateGlobalArgList(pstCmd->pcExec, pstCmd->pcArgs);
            CMD_SHOWEXECPARMS("cmd-ExecProcess(Helper)", ppcArgsList);
            //
            execvp(ppcArgsList[0], ppcArgsList);
            //
            // ================================================================
            // We should never return here !!
            // ================================================================
            //
            LOG_Report(errno, "CMD", "cmd-ExecProcess():%s thread error return:%s", ppcArgsList[0], strerror(errno));
            PRINTF2("cmd-ExecProcess():Helper:%s completion:%s-SHOULD NOT BE HERE!!!!" CRLF, ppcArgsList[0], strerror(errno));
            //
            cmd_FreeArgList(ppcArgsList);
            _exit(1);
         }
         break;
   
      case -1:
         //====================================================================
         // Error
         //====================================================================
         fCc = FALSE;
         break;

      default:
         //====================================================================
         // Command thread
         //====================================================================
         //    - save Helper thread pid
         //    - return completion
         //
         //PRINTF("cmd-ExecProcess():Fork:CMD" CRLF);
         GLOBAL_PidPut(PID_HELP, tPid);
         break;
   }
   return(fCc);
}

// 
// Function:   cmd_AllocateGlobalArgList
// Purpose:    Allocate applicable option list
// 
// Parameters: Executable name (or NULL if none)
// Returns:    Allocated arg array
// Note:       The result is allocated memory: Since the OS will free this memory,
//             we should NOT use safemalloc() here as to not confuse our mallc admin.
//
//             Args list ("-w 1024 -h 768 -hf -vf")
//
static char **cmd_AllocateGlobalArgList(char *pcExec, char *pcArgs)
{
   int      x=0, iNr=0, iLen, iIdx;
   char    *pcArg;
   char   **ppcArgList;

   if(pcArgs)
   {
      //
      // Count applicable options
      // Add command and NULL to the list
      //
      pcArgs = GEN_TrimLeft(pcArgs);
      iNr    = cmd_CountOptions(pcArgs);
   }
   PRINTF1("cmd-AllocateGlobalArgList():%d Args" CRLF, iNr);
   ppcArgList = (char **) malloc((iNr+2) * sizeof(ppcArgList));
   //
   if(pcExec)
   {
      pcExec = GEN_TrimLeft(pcExec);
      //
      // First entry in arglist is the name of the exec
      //
      iLen            = GEN_STRLEN(pcExec);
      pcArg           = (char *) malloc(iLen+1);
      ppcArgList[x++] = pcArg;
      GEN_STRCPY(pcArg, pcExec);
   }
   //
   // Extract the arglist (always terminate the array !)
   // Allocate space for each option (use the total args list length so we always have enough)
   //
   for(iIdx=0; iIdx<iNr; iIdx++)
   {
      pcArgs = GEN_TrimLeft(pcArgs);
      iLen   = GEN_STRLEN(pcArgs);
      if(iLen)
      {
         pcArg = (char *) malloc(iLen+1);
         iLen  = cmd_CopyOption(pcArg, pcArgs);
         ppcArgList[x++] = pcArg;
         pcArgs += iLen;
      }
   }
   ppcArgList[x] = NULL;
   //
   return(ppcArgList);
}

// 
// Function:   cmd_AllocateStaticArgList
// Purpose:    Allocate exec-arg list
// 
// Parameters: Initial NULL terminated Arg list, Max number of list entries
// Returns:    Allocated arg array
// Note:       The arg list may be extended at some later stage to add additional
//             args. Add one additional entry for the NULL terminator
//
//             The result is allocated memory: Since the OS will free this memory,
//             we should NOT use safemalloc() here as to not confuse our mallc admin.
//
static char **cmd_AllocateStaticArgList(const SARG *pstArg, int iNumArgs)
{
   int         x=0;
   char       *pcArg;
   char      **ppcArgList;

   PRINTF1("cmd-AllocateStaticArgList():%d Args" CRLF, iNumArgs);
   iNumArgs++;
   ppcArgList = (char **) malloc(iNumArgs * sizeof(ppcArgList));
   //
   while( (x < iNumArgs) && (pstArg->pcArg) )
   {
      pcArg = pstArg->pfGetArg(pstArg);
      if(pcArg)
      {
         //
         // We have an allocated argument to include in the list
         //
         ppcArgList[x++] = pcArg;
      }
      pstArg++;
   }
   //
   // Make sure to NULL terminate the list always
   //
   ppcArgList[x] = NULL;
   //
   return(ppcArgList);
}

// 
// Function:   cmd_CountOptions
// Purpose:    Count all applicable options
// 
// Parameters: Arg list ("-w 1024 -h 768 -hf -vf")  --> 6 args
// Returns:    Num options
// 
static int cmd_CountOptions(char *pcArgs)
{
   bool     fCounting=TRUE;
   int      iNr=0;

   if(pcArgs)
   {
      //
      // Get rid of leading/trailing spaces.
      // Count options
      //
      pcArgs = GEN_TrimAll(pcArgs);
      do
      {
         switch(*pcArgs)
         {
            default:
               break;

            case 0:
               iNr++;
               fCounting=FALSE;
               break;

            case ' ':
               iNr++;
               break;
         }
         pcArgs++;
      }
      while(fCounting);
   }
   return(iNr);
}

// 
// Function:   cmd_CopyOption
// Purpose:    Copy applicable option to the list
// 
// Parameters: Args list ("-w 1024 -h 768 -hf -vf")
// Returns:    Option length
// 
static int cmd_CopyOption(char *pcArgs, char *pcOption)
{
   int   iLen=0;

   while( (*pcOption != ' ') && (*pcOption != 0) )
   {
      *pcArgs++ = *pcOption++;
      iLen++;
   }
   *pcArgs = 0;
   return(iLen);
}

// 
// Function:   cmd_FreeArgList
// Purpose:    Free applicable option list
// 
// Parameters: Arg list
// Returns:    
// Note:       We will only call this function if the exec() system call fails.
//             In other cases, the OS will free all allocated memory for the ARG list !
//             Therefor, we should NOT use safefree() here as to not confuse our mallc admin.
//
static void cmd_FreeArgList(char **ppcArgList)
{
   int   x=0;

   if(ppcArgList)
   {
      while(ppcArgList[x])
      {
         free(ppcArgList[x]);
         x++;
      }
      free(ppcArgList);
   }
}

#ifdef   FEATURE_SHOW_EXEC_PARMS
// 
// Function:   cmd_ShowExecParms
// Purpose:    Show exec parms
// 
// Parameters: Caller, Arg list
// Returns:    
// Note:       
//
static void cmd_ShowExecParms(char *pcCaller, char **ppcArgList)
{
   int   x, iLen=0;
   char *pcTemp;

   if(ppcArgList)
   {
      //
      //
      // Deternine total size
      x = 0;
      while(ppcArgList[x])
      {
         iLen += GEN_STRLEN(ppcArgList[x++]);
         // add 1 space
         iLen++;
      }
      //
      // Assemble log string as well
      //
      iLen++;
      pcTemp = (char *)safemalloc(iLen);
      //
      x = 0;
      while(ppcArgList[x])
      {
         safestrcat(pcTemp, ppcArgList[x], iLen);
         safestrcat(pcTemp, " ", iLen);
#ifdef   FEATURE_SHOW_EXEC_PARMS_ALL
         LOG_printf("cmd_ShowExecParms(%s): %02d:[%s]" CRLF, pcCaller, x, ppcArgList[x]);
#endif   //FEATURE_SHOW_EXEC_PARMS_ALL
         x++;
      }
      LOG_printf("cmd_ShowExecParms(%s):%s" CRLF, pcCaller, pcTemp);
      safefree(pcTemp);
   }
}
#endif   //FEATURE_SHOW_EXEC_PARMS

/*------  Local functions separator ------------------------------------
__PID_UTIL_________(){};
----------------------------------------------------------------------------*/

//
// Function:   utl_RunUtility
// Purpose:    Run the required utility thread
//             
// Parms:      Command, Fd Read, Fd Write, Pipiline args list, num args
// Returns:    TRUE if OKee
// Note:       CMD info is inherited for reading from PID_CMND, 
//             PID_UTIL should get not additional args from the cmd struc but from the global mmap struct !
//
static bool utl_RunUtility(GLOCMD *pstCmd, int iFdRd, int iFdWr, const SARG *pstArgs, int iNumArgs)
{
   bool     fCc;
   char   **ppcArgsList;

   GLOBAL_DUMPCOMMAND("utl-RunUtility():PID_UTIL(inherit)", pstCmd);
   //
   switch(pstCmd->iCommand)
   {
      default:
         //
         // We seem to have get lost here
         //
         LOG_Report(0, "CMD", "utl-RunUtility():Cmd=%d:we got lost", pstCmd->iCommand);
         PRINTF1("utl-RunUtility():Cmd=%d:SHOULD NOT BE HERE!!!!" CRLF, pstCmd->iCommand);
         close(iFdWr);
         close(iFdRd);
         fCc = FALSE;
         break;

      case CAM_CMD_ALM_MOTION:
      case CAM_CMD_M_VECTOR:
      case CAM_CMD_A_VECTOR:
         //
         // Allocate the argument list for the Motion Vector generation utility. 
         // Reserve entries for the global args as well
         //
         ppcArgsList = cmd_AllocateStaticArgList(pstArgs, iNumArgs);
         PRINTF2("utl-RunUtility():CAM_CMD_x_VECTOR:Run [%s](no GLOBAL args, %d LOCAL args)" CRLF, ppcArgsList[0], iNumArgs);
         CMD_SHOWEXECPARMS("utl-RunUtility(CAM_CMD_x_VECTOR)", ppcArgsList);
         GEN_Sleep(2000);
         //
         // RUN: raspivid to write motion vector data
         //
         close(iFdRd);           // No need for a read pipe
         close(STDOUT_FILENO);   // Close system stdout
         dup(iFdWr);             // Redirect write pipe to Helper
         execvp(ppcArgsList[0], ppcArgsList);
         // ==========================================================
         // We should not return here !!
         // ==========================================================
         LOG_Report(errno, "CMD", "utl-RunUtility():%s thread error return:%s", ppcArgsList[0], strerror(errno));
         PRINTF2("utl-RunUtility():[%s] completion:%s-SHOULD NOT BE HERE!!!!" CRLF, ppcArgsList[0], strerror(errno));
         cmd_FreeArgList(ppcArgsList);
         close(iFdWr);
         fCc = FALSE;
         break;

      case CAM_CMD_STREAM:
         ppcArgsList = cmd_AllocateStaticArgList(pstArgs, iNumArgs);
         CMD_SHOWEXECPARMS("utl-RunUtility(CAM_CMD_STREAM)", ppcArgsList);
         //
         // RUN: an external tool like VLC to read h264 data and stream it
         // to a socket
         //
         close(iFdWr);           // No need for a write pipe back to Helper thread
         close(STDIN_FILENO);    // Close system stdin
         dup(iFdRd);             // Redirect input pipe to Helper
         //
         execvp(ppcArgsList[0], ppcArgsList);
         // ==========================================================
         // We should not return here !!
         // ==========================================================
         LOG_Report(errno, "CMD", "utl-RunUtility():[%s] thread error return:%s", ppcArgsList[0], strerror(errno));
         PRINTF2("utl-RunUtility():[%s] completion:%s-SHOULD NOT BE HERE!!!!" CRLF, ppcArgsList[0], strerror(errno));
         cmd_FreeArgList(ppcArgsList);
         close(iFdRd);
         fCc = FALSE;
         break;

      case CAM_CMD_MAN_SNAP:
         //
         // Allocate the argument list for the Snapshot utility. 
         //
         ppcArgsList = cmd_AllocateStaticArgList(pstArgs, iNumArgs);
         PRINTF2("utl-RunUtility():CAM_CMD_MAN_SNAP:Run [%s](no GLOBAL args, %d LOCAL args)" CRLF, ppcArgsList[0], iNumArgs);
         CMD_SHOWEXECPARMS("utl-RunUtility(CAM_STI)", ppcArgsList);
         //
         execvp(ppcArgsList[0], ppcArgsList);
         // ==========================================================
         // We should not return here !!
         // ==========================================================
         LOG_Report(errno, "CMD", "utl-RunUtility():[%s] thread error return:%s", ppcArgsList[0], strerror(errno));
         PRINTF2("utl-RunUtility():[%s] completion:%s-SHOULD NOT BE HERE!!!!" CRLF, ppcArgsList[0], strerror(errno));
         cmd_FreeArgList(ppcArgsList);
         fCc = FALSE;
         break;

   }
   return(fCc);
}


/*------  Local functions separator ------------------------------------
__PID_HELP_________(){};
----------------------------------------------------------------------------*/

//
// Function:   hlp_RunHelper
// Purpose:    Run the required helper thread
//             
// Parms:      Fd Read, Fd Write, Pipiline args list, num args
// Returns:    TRUE if OKee
// Note:       PID_HELP should get not additional args from the cmd struc but from the global mmap struct !
//
static bool hlp_RunHelper(GLOCMD *pstCmd, int iFdRd, int iFdWr, const SARG *pstArgs, int iNumArgs)
{
   bool     fCc=TRUE;
   char   **ppcArgsList;

   GLOBAL_DUMPCOMMAND("hlp-RunHelper():PID_HELP(inherit)", pstCmd);
   //
   switch(pstCmd->iCommand)
   {
      default:
         //
         // We seem to have get lost here
         //
         LOG_Report(0, "CMD", "hlp-RunHelper():Cmd=%x:we got lost", pstCmd->iCommand);
         PRINTF1("hlp-RunHelper():Cmd=%x:SHOULD NOT BE HERE!!!!" CRLF, pstCmd->iCommand);
         close(iFdWr);
         close(iFdRd);
         fCc = FALSE;
         break;

      case CAM_CMD_ALM_MOTION:
      case CAM_CMD_M_VECTOR:
      case CAM_CMD_A_VECTOR:
         #ifdef FEATURE_CAMERA
         {
            int      iW, iH;
            char    *pcMaskFile;
            MVIMG   *pstMv;

            // 
            // The helper thread processes incoming motion vector data, which will be
            // send to us by the raspivid utility
            //
            PRINTF("hlp-RunHelper(CAM_CMD_x_VECTOR):Init MVs..." CRLF);
            close(STDIN_FILENO);    // Close system stdin
            close(iFdWr);           // No need for a write pipe
            dup(iFdRd);             // Redirect input pipe to Util
            //
            // Wait till utility is running
            //
            if( hlp_WaitRunningUtil(pcRaspiVid) )
            {
               PRINTF("hlp-RunHelper(CAM_CMD_x_VECTOR):RaspiVid is running" CRLF);
               pstMv = VEC_InitMotion();
               iW = atoi(pstMap->G_pcWidth);
               iH = atoi(pstMap->G_pcHeight);
               //
               pcMaskFile = safemalloc(MAX_PATH_LENZ);
               GEN_SNPRINTF(pcMaskFile, MAX_PATH_LEN, "%s_%dx%d.%s", pcMotionMask, iW, iH, pstMap->G_pcPixFileExt);
               if( pstMap->G_stMotionDetect.iDebug & MM_DEBUG_MASK )
               {
                  GRF_MotionCreateMask(pstMv, (char *)pcMaskFile, iW, iH);
                  PRINTF1("hlp-RunHelper(CAM_CMD_x_VECTOR):Create mask for %s" CRLF, pcMaskFile);
               }
               else
               {
                  GEN_MEMSET(pstMv->pubMask, 0xFF, (pstMv->iNumMotionVectors * sizeof(u_int8)));
               }
               safefree(pcMaskFile);
               //
               hlp_WaitForMotion(pstMv, pstCmd, iFdRd);
               //
               // We have detected some motion or we have stopped: determine the need for action
               //
               hlp_HandleMotion(pstMv);
               VEC_ExitMotion(pstMv);
            }
            else
            {
               PRINTF("hlp-RunHelper(CAM_CMD_x_VECTOR):RaspiVid is NOT running" CRLF);
            }
            close(iFdRd);
         }
         #endif //FEATURE_CAMERA
         break;

      case CAM_CMD_STREAM:
         // 
         // The helper thread executes raspivid, which writes h264 data to stdout to an
         // external tool like VLC, which reads data from stdin and streams it to a socket
         // Build raspivid exec+args
         //
         ppcArgsList = cmd_AllocateGlobalArgList(pstCmd->pcExec, pstCmd->pcArgs);
         CMD_SHOWEXECPARMS("hlp-RunHelper(CAM_CMD_STREAM)", ppcArgsList);
         //
         //PRINTF2("hlp-RunHelper(CAM_CMD_STREAM):Run [%s]-[%s]" CRLF, ppcArgsList[0], pstCmd->pcArgs);
         //
         close(STDOUT_FILENO);   // Close system stdout
         close(iFdRd);           // No need for a read pipe
         dup(iFdWr);             // Redirect write pipe to utility thread
         //
         execvp(ppcArgsList[0], ppcArgsList);
         // ==========================================================
         // We should never return here !!
         // ==========================================================
         LOG_Report(errno, "CMD", "hlp-RunHelper():[%s] thread error return:%s", ppcArgsList[0], strerror(errno));
         PRINTF2("hlp-RunHelper():[%s] completion:%s-SHOULD NOT BE HERE!!!!" CRLF, ppcArgsList[0], strerror(errno));
         //
         cmd_FreeArgList(ppcArgsList);
         close(iFdWr);
         fCc = FALSE;
         break;
   }
   // ==========================================================
   // Return here will terminate this thread
   // ==========================================================
   return(fCc);
}

#ifdef FEATURE_CAMERA
// 
// Function:   hlp_WaitForMotion
// Purpose:    Get the motion status from the camera
// 
// Parameters: Cmd struct, Mv Data, Read file handle
// Returns:    
// Note:       Running the Help-Thread: 
//             Keep reading motion vectors coming from the Util-Thread until the motion exceeds a threshold. 
//             Returning from this function will terminate the UTIL thread (running raspivid exec).
//
static void hlp_WaitForMotion(MVIMG *pstMv, GLOCMD *pstCmd, int iFdRd)
{
   u_int32  ulMdStart, ulMdEnd;

   bool     fWaitForMotion=TRUE, fOverload=FALSE;
   int      iCalMotionVectors, iCurMode, iFd;
   MDET    *pstMdet=&pstMap->G_stMotionDetect;

   iCurMode = pstMdet->iMotMode;

   #ifdef FEATURE_PRINT_MOTION
   printf("hlp-WaitForMotion():%s" CRLF, RPI_GetMotionMode(iCurMode));
   #endif

   if(pstMdet->iDebug & MM_DEBUG_FILE_READ_MV)
   {
      if( (iFd = safeopen(RPI_MVEC_PATH, O_RDONLY)) > 0)
      {
         #ifdef FEATURE_PRINT_MOTION
         printf("hlp-WaitForMotion():Read from file:" RPI_MVEC_PATH CRLF);
         #endif   //FEATURE_PRINT_MOTION
         //
         // File read mode: read dummy frame(s) from file
         //
         close(iFdRd);
         iFdRd = iFd;
      }
      #ifdef FEATURE_PRINT_MOTION
      else
      {
         printf("hlp-WaitForMotion():ERROR: Read from file: %s" CRLF, strerror(errno));
      }
      #endif   //FEATURE_PRINT_MOTION
   }
   //
   VEC_SetMotionBackground(pstMv, iFdRd);
   //
   PRINTF1("hlp-WaitForMotion():MotionVectors Hor   =%d" CRLF, pstMv->iMotionVectorsHor);
   PRINTF1("hlp-WaitForMotion():MotionVectors Ver   =%d" CRLF, pstMv->iMotionVectorsVer);
   PRINTF1("hlp-WaitForMotion():Nr of Vector samples=%d" CRLF, pstMv->iNumMotionVectors);
   PRINTF1("hlp-WaitForMotion():Avg background Sad  =%d" CRLF, pstMv->usAvgBgndSad);

   #ifdef FEATURE_PRINT_MOTION
   printf("hlp-WaitForMotion():MotionVectors Hor   =%d" CRLF, pstMv->iMotionVectorsHor);
   printf("hlp-WaitForMotion():MotionVectors Ver   =%d" CRLF, pstMv->iMotionVectorsVer);
   printf("hlp-WaitForMotion():Nr of Vector samples=%d" CRLF, pstMv->iNumMotionVectors);
   printf("hlp-WaitForMotion():Avg background Sad  =%d" CRLF, pstMv->usAvgBgndSad);
   //
   printf("hlp-WaitForMotion():Debugmode           =0x%x" CRLF, pstMdet->iDebug);
   #endif   //FEATURE_PRINT_MOTION

   //======================================================================================
   // We have streaming Motion Vectors coming in from the Util thread: Read one whole
   // frame of vectors at a time, then parse them according the mode were are in.
   //======================================================================================
   
   ulMdStart       = RTC_GetSystemSecs();
   pstMv->iFrameNr = 0;

   do
   {
      // If file-mode: reset Fd
      if(pstMdet->iDebug & MM_DEBUG_FILE_READ_MV) safeseek(iFdRd, 0);
      //
      iCalMotionVectors = VEC_ReadMotionFrame(pstMv, iFdRd);
      //
      // Check if mode has changed. If so, abort here
      //
      if(iCurMode == pstMdet->iMotMode) 
      {
         //
         // iCalMotionVectors is the number of MVs in this frame which show motion
         //    0 <= iCalMotionVectors < iNumMotionVectors
         //
         // If the number of MVs exceeds 10% of the actual non-masked MVs the sensor might be
         // saturated: report and act accordingly
         //
         switch(iCalMotionVectors)
         {
            case -1:
               PRINTF("hlp-WaitForMotion():STOPPED" CRLF);
               fWaitForMotion = FALSE;
               break;

            case -2:
               PRINTF("hlp-WaitForMotion():ERROR: Exit Motion Detect" CRLF);
               fWaitForMotion = FALSE;
               break;

            case -3:
               PRINTF("hlp-WaitForMotion():Single action:" CRLF);
               fWaitForMotion = FALSE;
               break;

            case 0:
            default:
               //PRINTF("hlp-WaitForMotion():We have MVs:" CRLF);
               if(iCalMotionVectors > (pstMv->iActCount/10))
               {
                  if(fOverload == FALSE)
                  {
                     LOG_Report(0, "HLP", "hlp-WaitForMotion():Sensor overload (%d of %d)", iCalMotionVectors, pstMv->iActCount);
                     fOverload = TRUE;
                  }
               }
               else
               {
                  fOverload      = FALSE;
                  fWaitForMotion = hlp_HandleTrigger(pstMv, pstCmd, iFdRd);
               }
               break;
         }
      }
      else
      {
         //
         // Motion Mode changed to something: treat as MM-dependent trigger
         //
         #ifdef FEATURE_PRINT_MOTION
         printf("hlp-WaitForMotion():MODE Changed from %s to %s: exit!" CRLF, 
                  RPI_GetMotionMode(iCurMode), RPI_GetMotionMode(pstMdet->iMotMode));
         #endif   //FEATURE_PRINT_MOTION

         hlp_HandleTrigger(pstMv, pstCmd, iFdRd);
         fWaitForMotion = FALSE;
      }
   }
   while(fWaitForMotion);
   //
   //PRINTF1("hlp-WaitForMotion():Max background Sad=%d" CRLF, pstMv->usMaxBgndSad);
   //PRINTF1("hlp-WaitForMotion():Min foreground Sad=%d" CRLF, pstMv->usMinFgndSad);
   //PRINTF1("hlp-WaitForMotion():Avg foreground Sad=%d" CRLF, pstMv->usAvgFgndSad);
   //PRINTF1("hlp-WaitForMotion():Max foreground Sad=%d" CRLF, pstMv->usMaxFgndSad);
   //PRINTF2("hlp-WaitForMotion():#MVs=%d, MaxMV=%d" CRLF, pstMv->iActCount, pstMv->iMaxNormalVector);

   ulMdEnd = RTC_GetSystemSecs();
   #ifdef FEATURE_PRINT_MOTION
   printf("hlp-WaitForMotion():#MVs=%d, Secs=%d" CRLF, pstMv->iFrameNr, (int)(ulMdEnd-ulMdStart));
   #endif   //FEATURE_PRINT_MOTION
}

// 
// Function:   hlp_HandleTrigger
// Purpose:    Camera triggered motion, see if we have to do something. The MD video PID_UTIL is still running.
// 
// Parameters: Movement image struct, Cmd struct, Fd
// Context:    PID_HELP
// Returns:    FALSE if we are going to stop the motion detection process to take further action
// Note:       Various modes apply here:
//
//             Off         : Exit unconditionally.
//             Manual      : Exit if the motion vector exceeds the trigger threshold.
//             Cycle       : Just keep detecting motion (to handle the I/O).
//             Picture     : Kill the streaming Util-Thread (running Raspivid -x) and restart the Util-Thread running
//                           Raspistill to make a snapshot of the cause of motion, then proceed motion detect.
//             Augment     : Picture mode, but for a single trigger.
//             Recording   : Just keep recording.
//             Streaming   : Just keep streaming.
//
static bool hlp_HandleTrigger(MVIMG *pstMv, GLOCMD *pstCmd, int iFdRd)
{
   bool     fKeepRecording=TRUE;
   int      iNr, iFramesMotion=0;
   u_int16  usIdx;
   MOTYPE   tMotion;
   MDET    *pstMdet=&pstMap->G_stMotionDetect;

   switch(pstMdet->iMotMode)
   {
      default:
      case MOTION_MODE_OFF:
         PRINTF("hlp-HandleTrigger():MM_OFF" CRLF);
         hlp_RecordingStop(0);
         pstMv->ulSecsMotion = RTC_GetSystemSecs();
         tMotion = hlp_DetermineMotion(pstMv, "Off");
         hlp_HandleOutput(tMotion);
         hlp_ReportMotion(pstMv, MOTION_STOPPED);
         //
         fKeepRecording = FALSE;
         break;

      case MOTION_MODE_CYCLE:
         //PRINTF1("hlp-HandleTrigger():MM_CYCLE: %d MVs" CRLF, pstMv->iCalMotionVectors);
         //
         // Cycle mode,  sample frames
         //    o Store motion time/date
         //    o Sample NUM_FRAMES frames
         //    o Stop MD
         //    o Analyse all frames
         //
         if(( pstMv->iCalMotionVectors >= pstMdet->iTrigger1) && (pstMv->iMaxNormalVector >= pstMdet->iTrigger2) )
         {
            pstMv->ulSecsMotion = RTC_GetSystemSecs();
            pstMdet->iCounter++;
            //
            // Frame[0] has been acquired:
            // Read frames[1] ..  [NUM_FRAMES-1] into the cache
            // Stop recording
            //
            pstMv->iFrameIdx = 1;
            while(pstMv->iFrameIdx < NUM_FRAMES)
            {
               VEC_SkipMotionFrame(pstMv, iFdRd);
               VEC_SkipMotionFrame(pstMv, iFdRd);
               VEC_SkipMotionFrame(pstMv, iFdRd);
               VEC_SkipMotionFrame(pstMv, iFdRd);
               //
               iNr = VEC_ReadMotionFrame(pstMv, iFdRd);
               pstMv->iFrameIdx++;
            }
            //
            hlp_RecordingStop(0);
            //
            // Handle frame[0], which caused the trigger from cache
            //
            pstMv->iFrameIdx = 0;
            iNr = VEC_ReadMotionFrame(pstMv, 0);

            #ifdef FEATURE_PRINT_MOTION
            printf("MM_CYCLE(0): %d MVs" CRLF, pstMv->iCalMotionVectors);
            #endif

            tMotion = hlp_DetermineMotion(pstMv, "Cycle");
            usIdx   = VEC_GetClusterCenter(pstMv, pstMv->usMaxClusterSizeCid);
            hlp_ReportMotion(pstMv, tMotion);

            #ifdef FEATURE_PRINT_MOTION
            printf("MM_CYCLE(0): Motion           =%s"      CRLF, pcMotionType[tMotion]);
            printf("MM_CYCLE(0): Nr Of Clusters   =%d"      CRLF, pstMv->usNrOfClusters);
            printf("MM_CYCLE(0): Max Cluster Size =%d"      CRLF, pstMv->usMaxClusterSize);
            printf("MM_CYCLE(0): Cluster CoG      =(%d,%d)" CRLF, usIdx % pstMv->iMotionVectorsHor, usIdx / pstMv->iMotionVectorsHor);
            #endif   

            //
            // Handle the cached frames[1] ..  [NUM_FRAMES-1]
            //
            pstMv->iFrameIdx = 1;
            do
            {
               iNr = VEC_ReadMotionFrame(pstMv, 0);

               #ifdef FEATURE_PRINT_MOTION
               printf("MM_CYCLE(%d): %d MVs" CRLF, pstMv->iFrameIdx, pstMv->iCalMotionVectors);
               #endif

               if(iNr)
               {
                  usIdx   = VEC_GetClusterCenter(pstMv, pstMv->usMaxClusterSizeCid);
                  tMotion = hlp_DetermineMotion(pstMv, "Cycle");
                  hlp_ReportMotion(pstMv, tMotion);

                  #ifdef FEATURE_PRINT_MOTION
                  printf("MM_CYCLE(%d): Motion           =%s"      CRLF, pstMv->iFrameIdx, pcMotionType[tMotion]);
                  printf("MM_CYCLE(%d): Nr Of Clusters   =%d"      CRLF, pstMv->iFrameIdx, pstMv->usNrOfClusters);
                  printf("MM_CYCLE(%d): Max Cluster Size =%d"      CRLF, pstMv->iFrameIdx, pstMv->usMaxClusterSize);
                  printf("MM_CYCLE(%d): Cluster CoG      =(%d,%d)" CRLF, usIdx % pstMv->iMotionVectorsHor, usIdx / pstMv->iMotionVectorsHor);
                  #endif   

               }
               pstMv->iFrameIdx++;
            }
            while(pstMv->iFrameIdx < NUM_FRAMES);
            //
            // Save the files in the RamFs to persistent storage (Timestamped dir)
            //
            hlp_SaveRamfiles(pstMv, pcDirVideo);
            pstMv->iFrameIdx = 0;
            
            fKeepRecording = FALSE;
         }
         break;

      case MOTION_MODE_VIDEO:
         //PRINTF("hlp-HandleTrigger():MM_VIDEO" CRLF);
         //
         // Auto Video mode,  wait for the motion trigger
         //    o Store motion time/date
         //    o Keep recording for another few secs
         //    o Pause the Motion Detect cyclic recording
         //    o Check if we have real motion
         //    o YES:   Copy the videofiles from RAMFS to WWW
         //    o        Take snapshot
         //    o        Augment motion vectors onto the snapshot
         //    o        Exit motion vector detection
         //    o NO:    Resume the Motion Detect cyclic recording
         //
         if(( pstMv->iCalMotionVectors >= pstMdet->iTrigger1) && (pstMv->iMaxNormalVector >= pstMdet->iTrigger2) )
         {
            pstMv->ulSecsMotion = RTC_GetSystemSecs();
            pstMdet->iCounter++;
            //LOG_Report(0, "HLP", "hlp-HandleTrigger():MM_VIDEO:Count=%d, MaxNv=%d, MaxWv=%d", pstMdet->iCounter, pstMv->iMaxNormalVector, pstMv->iMaxWeightVector);
            //
            // Let video record for a while, then pause, check and copy recordings
            //
            hlp_RecordingPause(MOTION_VIDEO_CAPTURE_SECS, pstMv, iFdRd);
            tMotion = hlp_DetermineMotion(pstMv, "Video");
            hlp_HandleOutput(tMotion);
            //
            PRINTF4("hlp-HandleTrigger():MM_VIDEO-PAUSED:iCalMVs %d >= %d, iMaxNMV %d >= %d" CRLF, 
               pstMv->iCalMotionVectors, pstMdet->iTrigger1, pstMv->iMaxNormalVector, pstMdet->iTrigger2);
            //
            switch(tMotion)
            {
               default:
                  //
                  // Little motion: wait a while and resume recording
                  //
                  PRINTF("hlp-HandleTrigger():MM_VIDEO-RESUME" CRLF); 
                  hlp_RecordingResume(5000);
                  break;

               case MOTION_STOPPED:
                  PRINTF("hlp-HandleTrigger():MM_VIDEO-STOPPED" CRLF); 
                  hlp_RecordingStop(0);
                  fKeepRecording = FALSE;
                  break;

               case MOTION_LARGE:
               case MOTION_HUGE:
                  hlp_RecordingStop(0);
                  PRINTF("hlp-HandleTrigger():MM_VIDEO: take additional snapshot" CRLF);
                  pstCmd->iCommand = CAM_CMD_MAN_SNAP;
                  hlp_HandleTriggerSnapshot(pstMv, pstCmd);
                  fKeepRecording = FALSE;
                  break;
            }
            hlp_ReportMotion(pstMv, tMotion);
         }
         break;

      case MOTION_MODE_STREAM:
         //PRINTF("hlp-HandleTrigger():MM_STREAM" CRLF);
         //
         // Auto Streaming mode,  wait for the motion trigger
         //    o Store motion time/date
         //    o Keep streaming until stopped (manual or duration)
         //
         if(( pstMv->iCalMotionVectors >= pstMdet->iTrigger1) && (pstMv->iMaxNormalVector >= pstMdet->iTrigger2) )
         {
            pstMv->ulSecsMotion = RTC_GetSystemSecs();
            pstMdet->iCounter++;
            hlp_RecordingStop(0);
            tMotion = hlp_DetermineMotion(pstMv, "Stream");
            hlp_HandleOutput(tMotion);
            hlp_ReportMotion(pstMv, tMotion);
            //
            PRINTF("hlp-HandleTrigger():MM_STREAM" CRLF);
         }
         break;

      case MOTION_MODE_MANUAL:
         //PRINTF("hlp-HandleTrigger():MM_MANUAL" CRLF);
         //
         // Manual mode
         //    o Store motion time/date
         //    o Collect <iTrigger2> non-empty frames and save them to file
         //    o Stop the Motion Detect recording
         //    o Check if we have some motion: report motion data
         //
         if(pstMv->iCalMotionVectors > pstMdet->iTrigger1)
         {
            pstMv->ulSecsMotion = RTC_GetSystemSecs();
            pstMdet->iCounter++;
            //
            // Frame[0] has been acquired:
            // Read frames[1] ..  [NUM_FRAMES-1] into the cache
            // Stop recording
            //
            pstMv->iFrameIdx = 1;
            while(pstMv->iFrameIdx < NUM_FRAMES)
            {
               VEC_ReadMotionFrame(pstMv, iFdRd);
               pstMv->iFrameIdx++;
            }
            //
            hlp_RecordingStop(0);
            //
            // Handle frame[0], which caused the trigger from cache
            //
            pstMv->iFrameIdx = 0;
            iNr = VEC_ReadMotionFrame(pstMv, 0);

            #ifdef FEATURE_PRINT_MOTION
            printf("MM_MANUAL(0): %d MVs" CRLF, iNr);
            #endif

            tMotion = hlp_DetermineMotion(pstMv, "Manual");
            usIdx   = VEC_GetClusterCenter(pstMv, pstMv->usMaxClusterSizeCid);
            hlp_ReportMotion(pstMv, tMotion);

            #ifdef FEATURE_PRINT_MOTION
            printf("MM_MANUAL(0): Motion           =%s"      CRLF, pcMotionType[tMotion]);
            printf("MM_MANUAL(0): Nr Of Clusters   =%d"      CRLF, pstMv->usNrOfClusters);
            printf("MM_MANUAL(0): Max Cluster Size =%d"      CRLF, pstMv->usMaxClusterSize);
            printf("MM_MANUAL(0): Cluster CoG      =(%d,%d)" CRLF, usIdx % pstMv->iMotionVectorsHor, usIdx / pstMv->iMotionVectorsHor);
            #endif   

            //
            // Handle the cached frames[1] ..  [NUM_FRAMES-1]
            //
            pstMv->iFrameIdx = 1;
            do
            {
               iNr = VEC_ReadMotionFrame(pstMv, 0);

               #ifdef FEATURE_PRINT_MOTION
               printf("MM_MANUAL(%d): %d MVs" CRLF, pstMv->iFrameIdx, pstMv->iCalMotionVectors);
               #endif

               if(iNr)
               {
                  usIdx   = VEC_GetClusterCenter(pstMv, pstMv->usMaxClusterSizeCid);
                  tMotion = hlp_DetermineMotion(pstMv, "Cycle");
                  hlp_ReportMotion(pstMv, tMotion);

                  #ifdef FEATURE_PRINT_MOTION
                  printf("MM_MANUAL(%d): Motion           =%s"      CRLF, pstMv->iFrameIdx, pcMotionType[tMotion]);
                  printf("MM_MANUAL(%d): Nr Of Clusters   =%d"      CRLF, pstMv->iFrameIdx, pstMv->usNrOfClusters);
                  printf("MM_MANUAL(%d): Max Cluster Size =%d"      CRLF, pstMv->iFrameIdx, pstMv->usMaxClusterSize);
                  printf("MM_MANUAL(%d): Cluster CoG      =(%d,%d)" CRLF, usIdx % pstMv->iMotionVectorsHor, usIdx / pstMv->iMotionVectorsHor);
                  #endif   

               }
               pstMv->iFrameIdx++;
            }
            while(pstMv->iFrameIdx < NUM_FRAMES);
            //
            hlp_WriteFrame(pstMv);
            pstMv->iFrameIdx = 0;
            fKeepRecording   = FALSE;
         }
         break;

      case MOTION_MODE_AUGMENT:
         //
         // Augment picture mode, if motion beyond user threshold (trigger):
         //    o Store motion time/date
         //    o Exit motion vector detection
         //    o Augment motion vectors onto the last snapshot
         //
         if(( pstMv->iCalMotionVectors >= pstMdet->iTrigger1) && (pstMv->iMaxNormalVector >= pstMdet->iTrigger2) )
         {
            pstMv->ulSecsMotion = RTC_GetSystemSecs();
            pstMdet->iCounter++;
            hlp_RecordingStop(0);
            tMotion = hlp_DetermineMotion(pstMv, "Augment");
            hlp_HandleOutput(tMotion);
            hlp_ReportMotion(pstMv, tMotion);
            fKeepRecording = FALSE;
            //
            PRINTF("hlp-HandleTrigger():MM_AUGMENT" CRLF);
         }
         break;

      case MOTION_MODE_PICTURE:
         //
         // Snapshot picture mode, if motion beyond user threshold (trigger):
         //    o Store motion time/date
         //    o Stop the Motion Detect recording
         //    o Check if we have real motion
         //    o YES:   Take snapshot
         //    o        Augment motion vectors onto the snapshot
         //    o        Copy the files from RAMFS to WWW
         //    o Exit motion vector detection
         //
         if(( pstMv->iCalMotionVectors >= pstMdet->iTrigger1) && (pstMv->iMaxNormalVector >= pstMdet->iTrigger2) )
         {
            pstMv->ulSecsMotion = RTC_GetSystemSecs();
            pstMdet->iCounter++;
            //
            // Frame[0] has been acquired:
            // Read frames[1] ..  [NUM_FRAMES-1] into the cache
            // Stop recording
            //
            pstMv->iFrameIdx = 1;
            while(pstMv->iFrameIdx < NUM_FRAMES)
            {
               VEC_SkipMotionFrame(pstMv, iFdRd);
               VEC_SkipMotionFrame(pstMv, iFdRd);
               VEC_SkipMotionFrame(pstMv, iFdRd);
               VEC_SkipMotionFrame(pstMv, iFdRd);
               //
               VEC_ReadMotionFrame(pstMv, iFdRd);
               pstMv->iFrameIdx++;
            }
            //
            hlp_RecordingStop(0);
            //
            // Handle frame[0], which caused the trigger from cache
            //
            pstMv->iFrameIdx = 0;
            iNr = VEC_ReadMotionFrame(pstMv, 0);

            #ifdef FEATURE_PRINT_MOTION
            printf("MM_PICTURE(0): %d MVs" CRLF, iNr);
            #endif

            tMotion = hlp_DetermineMotion(pstMv, "Picture");
            usIdx   = VEC_GetClusterCenter(pstMv, pstMv->usMaxClusterSizeCid);
            hlp_ReportMotion(pstMv, tMotion);

            #ifdef FEATURE_PRINT_MOTION
            printf("MM_PICTURE(0): Motion           =%s"      CRLF, pcMotionType[tMotion]);
            printf("MM_PICTURE(0): Nr Of Clusters   =%d"      CRLF, pstMv->usNrOfClusters);
            printf("MM_PICTURE(0): Max Cluster Size =%d"      CRLF, pstMv->usMaxClusterSize);
            printf("MM_PICTURE(0): Cluster CoG      =(%d,%d)" CRLF, usIdx % pstMv->iMotionVectorsHor, usIdx / pstMv->iMotionVectorsHor);
            #endif   

            //
            // Handle the cached frames[1] ..  [NUM_FRAMES-1]
            //
            pstMv->iFrameIdx = 1;
            do
            {
               iNr = VEC_ReadMotionFrame(pstMv, 0);

               #ifdef FEATURE_PRINT_MOTION
               printf("MM_PICTURE(%d): %d MVs" CRLF, pstMv->iFrameIdx, pstMv->iCalMotionVectors);
               #endif

               if(iNr)
               {
                  //
                  // Consecutive frame has some MVs
                  //
                  tMotion = hlp_DetermineMotion(pstMv, "Picture");
                  usIdx   = VEC_GetClusterCenter(pstMv, pstMv->usMaxClusterSizeCid);
                  hlp_ReportMotion(pstMv, tMotion);
                  //
                  switch(tMotion)
                  {
                     default:
                        //
                        // Little motion: restart recording
                        //
                        PRINTF("hlp-HandleTrigger():MM_PICTURE:Too little motion" CRLF);
                        break;

                     case MOTION_LARGE:
                     case MOTION_HUGE:
                        iFramesMotion++;
                        break;
                  }

                  #ifdef FEATURE_PRINT_MOTION
                  printf("MM_PICTURE(%d): Motion           =%s"      CRLF, pstMv->iFrameIdx, pcMotionType[tMotion]);
                  printf("MM_PICTURE(%d): Nr Of Clusters   =%d"      CRLF, pstMv->iFrameIdx, pstMv->usNrOfClusters);
                  printf("MM_PICTURE(%d): Max Cluster Size =%d"      CRLF, pstMv->iFrameIdx, pstMv->usMaxClusterSize);
                  printf("MM_PICTURE(%d): Cluster CoG      =(%d,%d)" CRLF, usIdx % pstMv->iMotionVectorsHor, usIdx / pstMv->iMotionVectorsHor);
                  #endif   

               }
               else
               {

                  #ifdef FEATURE_PRINT_MOTION
                  printf("MM_PICTURE(%d):No MV" CRLF, pstMv->iFrameIdx);
                  #endif   

               }
               pstMv->iFrameIdx++;
            }
            while(pstMv->iFrameIdx < NUM_FRAMES);
            //
            // If more frames show similar motion, kick the scarecrow
            //
            if(iFramesMotion)
            {

               #ifdef FEATURE_PRINT_MOTION
               printf("MM_PICTURE():%d frames show motion" CRLF, iFramesMotion);
               #endif   

               hlp_HandleOutput(tMotion);
               LOG_Report(0, "HLP", "hlp-HandleTrigger():MM_PICTURE:Count=%d, MaxNv=%d, MaxWv=%d", pstMdet->iCounter, pstMv->iMaxNormalVector, pstMv->iMaxWeightVector);
               PRINTF1("hlp-HandleTrigger():MM_PICTURE:Take snapshot (Ctr=%d)" CRLF, pstMdet->iCounter);
               pstCmd->iCommand = CAM_CMD_MAN_SNAP;
               hlp_HandleTriggerSnapshot(pstMv, pstCmd);
               PRINTF("hlp-HandleTrigger():MM_PICTURE:Snapshot handled" CRLF);
               //
               // Save the files in the RamFs to persistent storage (Timestamped dir)
               //
               hlp_SaveRamfiles(pstMv, pcDirVideo);
            }
            pstMv->iFrameIdx = 0;
            
            fKeepRecording = FALSE;
         }
         break;
   }
   //
   GEN_SNPRINTF(pstMap->G_pcDetectCount,   MAX_PARM_LEN, "%d", pstMdet->iCounter);
   GEN_SNPRINTF(pstMap->G_pcDetectCluster, MAX_PARM_LEN, "%d", pstMdet->iCluster);
   //
   return(fKeepRecording);
}

// 
// Function:   hlp_HandleTriggerSnapshot
// Purpose:    Handle snapshot after trigger
// 
// Parameters: Movement image struct, Cmds
// Context:    PID_HELP
// Returns:    
// Note:       
//
static void hlp_HandleTriggerSnapshot(MVIMG *pstMv, GLOCMD *pstCmd)
{
   //
   // Picture mode, if motion beyond user threshold (trigger):
   //    o Terminate utility thread (RaspiVid "circular" recording)
   //    o Take snapshot
   //    o Augment motion vectors onto the snapshot
   //    o Exit motion vector detection
   //          MOTION_MODE_PICTURE: will resume motion detect
   //          MOTION_MODE_VIDEO:   will resume motion detect
   //          MOTION_MODE_AUGMENT: will stop   motion detect
   //
   PRINTF2("hlp-HandleTriggerSnapshot():#MotionVectors =%5d, MaxFgndSad     =%5d" CRLF, pstMv->iCalMotionVectors, (int)pstMv->usMaxFgndSad);
   PRINTF2("hlp-HandleTriggerSnapshot():MaxNormalVector=%5d, MaxWeightVector=%5d" CRLF, pstMv->iMaxNormalVector, pstMv->iMaxWeightVector);
   PRINTF2("hlp-HandleTriggerSnapshot():Trigger1       =%5d, Trigger2       =%5d" CRLF, pstMap->G_stMotionDetect.iTrigger1, pstMap->G_stMotionDetect.iTrigger2);
   //
   // The stMotionPixArguments determine the snapshot filename
   //
   PRINTF("hlp-HandleTriggerSnapshot():Make snapshot..." CRLF);
   hlp_RunUtility(pstCmd, stMotionPixArguments, iNumMotionPixArguments);
   GEN_WaitProcess(GLOBAL_PidGet(PID_UTIL));
   GLOBAL_PidPut(PID_UTIL, 0);
   PRINTF1("hlp-HandleTriggerSnapshot():Done (%s)" CRLF, pstMap->G_pcLastFile);
}

// 
// Function:   hlp_HandleMotion
// Purpose:    Handle the motion detection aftermath
//             We have detected motion, and the current mode has determined that we had to 
//             exit the motion detection process, so PID_UTIL has exited already.
// 
// Parameters: Mv Data
// Returns:    
// Note:       Context=PID_HELP: 
//             1) Turn on/off outputs depending motion
//             2) Sort the Motion Vectors to Hi->Lo vectors
//             3) Save the Motion Vectors to file (time-stamped)
//             4) Handle Motion depending the motion detect mode:
//
//
static void hlp_HandleMotion(MVIMG *pstMv)
{
   MOTYPE   tMotion;
   char    *pcTemp=safemalloc(MAX_PATH_LEN);
   MDET    *pstMdet=&pstMap->G_stMotionDetect;

   VEC_SortMotionVectors(pstMv, MVKEY_HL_NMV);
   //
   // Decide if this is a memorable event: 
   // If so:
   //    o copy the augmented file to the public area
   //    o write the MV data to the same area
   //
   switch(pstMdet->iMotMode)
   {
      case MOTION_MODE_AUGMENT:
         if(pstMdet->iDebug & MM_DEBUG_FILE_MVS) hlp_WriteFrame(pstMv);
         //
         // Handle the relay outputs through remote IO
         //
         tMotion = hlp_DetermineMotion(pstMv, "Augment");
         hlp_HandleOutput(tMotion);
         //
         // Create augmented file xxxx_A.* from all the MVs in this frame
         // One shot has been accomplished: No need to notify the PID_CMND to restart the process again.
         // Build input image filename f.i. /mnt/rpipix/motion_0.jpg
         //
         GEN_SNPRINTF(pcTemp, MAX_PATH_LEN, "%s%s.%s", pstMap->G_pcRamDir, (char *)pcMotionInput, pstMap->G_pcPixFileExt);
         GRF_AugmentMotionVectorFile(pstMv, pcTemp, pstMap->G_pcRamDir, (char *)pcMotionAugment, pstMap->G_pcPixFileExt);
         PRINTF1("hlp-HandleMotion():MM-AUGMENT:%s" CRLF, pstMap->G_pcLastFile);
         break;

      case MOTION_MODE_VIDEO:
      case MOTION_MODE_PICTURE:
         //
         // Handle the relay outputs through remote IO
         //
         tMotion = hlp_DetermineMotion(pstMv, "Video/Picture");
         hlp_HandleOutput(tMotion);
         //
         // Build input image filename f.i. /mnt/rpipix/motion_0.jpg
         // Create augmented file /mnt/rpipix/motion_A.jpg from all the MVs in this frame
         //
         GEN_SNPRINTF(pcTemp, MAX_PATH_LEN, "%s%s.%s", pstMap->G_pcRamDir, (char *)pcMotionInput, pstMap->G_pcPixFileExt);
         if(GRF_AugmentMotionVectorFile(pstMv, pcTemp, pstMap->G_pcRamDir, (char *)pcMotionAugment, pstMap->G_pcPixFileExt))
         {
            //
            // Save the augmented file in the permanent File storage (timestamped)
            //    f.i.:
            //    src = /mnt/rpipix/*
            //    dst = /all/public/www/video/
            //
            hlp_SaveRamfiles(pstMv, pcDirVideo);
         }
         //
         // Write clusters to file if enabled
         //
         if(pstMdet->iDebug & MM_DEBUG_FILE_MVS) hlp_WriteFrame(pstMv);
         else
         {
            //
            // We might need to notify the PID_CMND to restart the process again.
            //
            if( (pstMdet->iDebug & MM_DEBUG_FILE_READ_MV) == 0)
            {
               //
               // Live read mode: restart
               //
               GLOBAL_Notify(PID_CMND, GLOBAL_HLP_RUN, SIGUSR1);
               PRINTF("hlp-HandleMotion():MM-VID/PIX: Restart!" CRLF);
            }
            else pstMdet->iMotMode = MOTION_MODE_OFF;
         }
         break;

      case MOTION_MODE_CYCLE:
         //
         // Write clusters to file if enabled
         //
         if(pstMdet->iDebug & MM_DEBUG_FILE_MVS) hlp_WriteFrame(pstMv);
         else
         {
            if( (pstMdet->iDebug & MM_DEBUG_FILE_READ_MV) == 0)
            {
               PRINTF("hlp-HandleMotion():MM-CYCLE: Restart!" CRLF);
               GLOBAL_Notify(PID_CMND, GLOBAL_HLP_RUN, SIGUSR1);
            }
            else pstMdet->iMotMode = MOTION_MODE_OFF;
         }
         break;
   
      case MOTION_MODE_STREAM:
         //
         // Write clusters to file if enabled
         //
         if(pstMdet->iDebug & MM_DEBUG_FILE_MVS) hlp_WriteFrame(pstMv);
         else
         {
            //
            // We might need to notify the PID_CMND to restart the process again.
            //
            if( (pstMdet->iDebug & MM_DEBUG_FILE_READ_MV) == 0)
            {
               //
               // Live read mode: restart
               //
               GLOBAL_Notify(PID_CMND, GLOBAL_HLP_RUN, SIGUSR1);
               PRINTF("hlp-HandleMotion():MM-STREAM: Restart!" CRLF);
            }
            else pstMdet->iMotMode = MOTION_MODE_OFF;
         }
         break;
   
      case MOTION_MODE_MANUAL:
         //
         // Write clusters to file if enabled
         //
         if(pstMdet->iDebug & MM_DEBUG_FILE_MVS) hlp_WriteFrame(pstMv);
         //
         // No need to notify the PID_CMND to restart the process again.
         //
         GEN_SNPRINTF(pcTemp, MAX_PATH_LEN, "%s%s.%s", pstMap->G_pcRamDir, (char *)pcMotionInput, pstMap->G_pcPixFileExt);
         if(GRF_AugmentMotionVectorFile(pstMv, pcTemp, pstMap->G_pcRamDir, (char *)pcMotionAugment, pstMap->G_pcPixFileExt))
         {
            //
            // Save the recordings plus (augmented) pix files in the permanent File storage (timestamped)
            //    f.i.:
            //    src = /mnt/rpipix/*
            //    dst = /all/public/www/video/
            //
            hlp_SaveRamfiles(pstMv, pcDirVideo);
         }
         else
         {
            PRINTF1("hlp-HandleMotion():MM-MANUAL:ERROR Augment (input file=%s)" CRLF, pcTemp);
         }
         PRINTF("hlp-HandleMotion():MM-MANUAL:Terminate!" CRLF);
         break;
   
      default:
      case MOTION_MODE_OFF:
         //
         // Write clusters to file if enabled
         //
         if(pstMdet->iDebug & MM_DEBUG_FILE_MVS) hlp_WriteFrame(pstMv);
         //
         //
         // Handle the relay outputs through remote IO
         //
         hlp_HandleOutput(MOTION_STOPPED);
         PRINTF("hlp-HandleMotion():MM-OFF:Terminate!" CRLF);
         break;
   }
   safefree(pcTemp);
}

// 
// Function:   hlp_DetermineMotion
// Purpose:    Determine motion attributes
// 
// Parameters: Mv Data, Caption
// Returns:    Motion type
// Note:
//             MOTION_NONE
//             MOTION_TINY
//             MOTION_SMALL
//             MOTION_MEDIUM
//             MOTION_LARGE
//             MOTION_HUGE
//             MOTION_STOPPED
//             MOTION_ERROR
//
static MOTYPE hlp_DetermineMotion(MVIMG *pstMv, const char *pcCaption)
{
   MOTYPE   tMotion=MOTION_NONE;
   int      iStrength;
   u_int16  usTotalRemoved=0, usRemoved, usNr, usCid, usSize;
   MDET    *pstMdet=&pstMap->G_stMotionDetect;
   u_int16 *pusCids;

   VEC_AssignClusterIds(pstMv);
   VEC_SaveMotionVectors(pstMv, pcCaption);
   PRINTF1("hlp-DetermineMotion(A): usNrOfClusters     =%d" CRLF, pstMv->usNrOfClusters);
   PRINTF1("hlp-DetermineMotion(A): usMaxClusterSize   =%d" CRLF, pstMv->usMaxClusterSize);
   PRINTF1("hlp-DetermineMotion(A): usMaxClusterSizeCid=%d" CRLF, pstMv->usMaxClusterSizeCid);
   //
   if(pstMv->usNrOfClusters > pstMv->iCalMotionVectors)
   {
      LOG_Report(0, "HLP", "hlp-DetermineMotion(B):ERROR Too many clusters (%d), #MVs=%d", pstMv->usNrOfClusters, pstMv->iCalMotionVectors);
      tMotion = MOTION_ERROR;
   }
   else
   {
      pusCids = (u_int16 *)safemalloc(pstMv->iNumMotionVectors * sizeof(u_int16));
      //
      // Count sizes of each CID 1...? (ID=0 is not used)
      //
      for(usCid=1; usCid<=pstMv->usNrOfClusters; usCid++)
      {
         pusCids[usCid] = VEC_GetClusterSize(pstMv, usCid);
         PRINTF2("hlp-DetermineMotion(B): Cluster-%2d=%d MVs" CRLF, usCid, pusCids[usCid]);
      }
      //
      // Check all existing clusters
      //
      for(usCid=1; usCid<=pstMv->usNrOfClusters; usCid++)
      {
         if(pusCids[usCid] > 0)
         {
            if(pusCids[usCid] < CLUSTER_MIN_SIZE)
            {
               //
               // Discard many small clusters; If there are many small clusters, consider it noise
               //
               usRemoved       = VEC_RemoveCluster(pstMv, usCid);
               usTotalRemoved += usRemoved;

               #ifdef FEATURE_PRINT_MOTION
               printf("hlp-DetermineMotion(C): %d Removed (CId-%d: too small))!" CRLF, usRemoved, usCid);
               #endif   //FEATURE_PRINT_MOTION

            }
            else 
            {
               //
               // Determine the strength of this cluster. If less than CLUSTER_STRENGTH % of the vector <trigger2>
               // ignore this cluster (noise)
               //
               iStrength = VEC_GetClusterStrength(pstMv, usCid, pstMdet->iTrigger2, CLUSTER_STRENGTH_COUNT);
               if(iStrength < ((CLUSTER_STRENGTH * pstMdet->iTrigger2) / 100))
               {
                  //
                  // Ignore this cluster: probably wind noise
                  //
                  usRemoved       = VEC_RemoveCluster(pstMv, usCid);
                  usTotalRemoved += usRemoved;

                  #ifdef FEATURE_PRINT_MOTION
                  printf("hlp-DetermineMotion(C): %d Removed (CId-%d: Noise)!" CRLF, usRemoved, usCid);
                  #endif   //FEATURE_PRINT_MOTION
               }
            }
         }
      }

      #ifdef FEATURE_PRINT_MOTION
      printf("hlp-DetermineMotion(C): %d Removed !" CRLF, usTotalRemoved);
      #endif   //FEATURE_PRINT_MOTION

      //
      //
      if(usTotalRemoved > pstMv->iActCount/10 )
      {
         tMotion = MOTION_NOISE;
         PRINTF2("hlp-DetermineMotion(D): Noise (Nr=%d of %d)" CRLF, usTotalRemoved, pstMv->iActCount/10);
         //
         //VEC_AssignClusterIds(pstMv);
         //VEC_SortMotionVectors(pstMv, MVKEY_LH_CID);
         //pwjh VEC_SaveMotionVectors(pstMv, "Noise clusters");
         //pwjh VEC_CopyVirtualData(pstMv);
      }
      else
      {
         tMotion = MOTION_TINY;
         usSize  = CLUSTER_MIN_SIZE;
         //
         while( (pstMv->usNrOfClusters > CLUSTER_REMAINING_COUNT) && (usSize < pstMv->usMaxClusterSize) )
         {
            VEC_AssignClusterIds(pstMv);
            PRINTF1("hlp-DetermineMotion(D): usNrOfClusters      =%d" CRLF, pstMv->usNrOfClusters);
            PRINTF1("hlp-DetermineMotion(D): usMaxClusterSize    =%d" CRLF, pstMv->usMaxClusterSize);
            PRINTF1("hlp-DetermineMotion(D): usMaxClusterSizeCid =%d" CRLF, pstMv->usMaxClusterSizeCid);
            PRINTF1("hlp-DetermineMotion(D): Remove clusters size=%d" CRLF, usSize);
            //
            // All smaller clusters have been removed. Isolate the large ones until only 
            // <CLUSTER_REMAINING_COUNT> remain
            //
            for(usCid=1; usCid<=pstMv->usNrOfClusters; usCid++)
            {
               PRINTF2("hlp-DetermineMotion(D): Check cluster ID=%d with size=%d" CRLF, usCid, usSize);
               usNr = VEC_GetClusterSize(pstMv, usCid);
               if(usNr == usSize)
               {
                  usNr = VEC_RemoveCluster(pstMv, usCid);
                  PRINTF2("hlp-DetermineMotion(E): Cluster-%2d with %d MVs has been Removed !" CRLF, usCid, usNr);
               }
            }
            usSize++;
         }
         //
         // All clusters but at max <CLUSTER_REMAINING_COUNT> have been removed: Sort on cluster ID
         //
         VEC_AssignClusterIds(pstMv);
         VEC_SortMotionVectors(pstMv, MVKEY_LH_CID);
         PRINTF1("hlp-DetermineMotion(Z): usNrOfClusters     =%d" CRLF, pstMv->usNrOfClusters);
         PRINTF1("hlp-DetermineMotion(Z): usMaxClusterSize   =%d" CRLF, pstMv->usMaxClusterSize);
         PRINTF1("hlp-DetermineMotion(Z): usMaxClusterSizeCid=%d" CRLF, pstMv->usMaxClusterSizeCid);
         //
         //pwjh VEC_SaveMotionVectors(pstMv, "Biggest clusters");
         //pwjh VEC_CopyVirtualData(pstMv);
         //
         // Depending the different results, determine the motion type within the secluded area.
         //
         usSize = pstMv->usMaxClusterSize;
         //
         if     (usSize > pstMdet->iMvHuge)        tMotion = MOTION_HUGE;  
         else if(usSize > pstMdet->iMvLarge)       tMotion = MOTION_LARGE; 
         else if(usSize > pstMdet->iMvMedium)      tMotion = MOTION_MEDIUM;
         else if(usSize > pstMdet->iMvSmall)       tMotion = MOTION_SMALL; 
      }
      PRINTF1("hlp-DetermineMotion(Z): Motion=%s" CRLF, pcMotionType[tMotion]);
      safefree(pusCids);
   }
   return(tMotion);
}

// 
// Function:   hlp_HandleOutput
// Purpose:    Turn hardware On/Off depending the motion
// 
// Parameters: motion type
// Returns:    TRUE if relays active
// Note:       Running the Help-Thread
//
static bool hlp_HandleOutput(MOTYPE tMotion)
{
   bool     fCc=TRUE;

   switch(tMotion)
   {
      default:
         PRINTF1("hlp-HandleOutput():??????????" CRLF, tMotion);
         hlp_RemoteIo(RIO_ERR);
         break;

      case MOTION_STOPPED:
         //PRINTF("hlp-HandleOutput():STOPPED" CRLF);
         hlp_RemoteIo(RIO_OFF);
         fCc = FALSE;
         break;

      case MOTION_NONE:
         hlp_RemoteIo(RIO_OFF);
         fCc = FALSE;
         break;

      case MOTION_TINY:
         //PRINTF1("hlp-HandleOutput():TINY (%d)" CRLF, tMotion);
         hlp_RemoteIo(RIO_SG1);
         fCc = FALSE;
         break;

      case MOTION_SMALL:
         //PRINTF1("hlp-HandleOutput():SMALL (%d)" CRLF, tMotion);
         hlp_RemoteIo(RIO_SG1|RIO_SG2);
         fCc = FALSE;
         break;

      case MOTION_MEDIUM:
         //PRINTF1("hlp-HandleOutput():MEDIUM (%d)" CRLF, tMotion);
         hlp_RemoteIo(RIO_SG1|RIO_SG2|RIO_SG3);
         break;

      case MOTION_LARGE:
         //PRINTF1("hlp-HandleOutput():LARGE (%d)" CRLF, tMotion);
         hlp_RemoteIo(RIO_SG1|RIO_SG2|RIO_SG3|RIO_SG4|RIO_SG5|RIO_RE1);
         break;

      case MOTION_HUGE:
         //PRINTF1("hlp-HandleOutput():HUGE (%d)" CRLF, tMotion);
         hlp_RemoteIo(RIO_SG1|RIO_SG2|RIO_SG3|RIO_SG4|RIO_SG5|RIO_RE1|RIO_RE2);
         break;

      case MOTION_ERROR:
         PRINTF("hlp-HandleOutput():ERROR: Exit Motion Detect" CRLF);
         hlp_RemoteIo(RIO_ERR);
         fCc = FALSE;
         break;
   }
   return(fCc);
}

//
// Function:   hlp_RunUtility
// Purpose:    Split this thread and execute the desired external function. Kill any ongoing
//             process first ! No pipeline is created
// Parms:      Cmd struct, Pipiline args list, num args
// Returns:    TRUE if OKee
// Note:       Get Apps and Args from Global mmap
//
static bool hlp_RunUtility(GLOCMD *pstCmd, const SARG *pstArgs, int iNumArgs)
{
   bool     fCc=TRUE;
   pid_t    tPid;
   
   GLOBAL_DUMPCOMMAND("hlp-RunUtility():PID_HELP(Dump)", pstCmd);
   //
   GEN_KillProcess(GLOBAL_PidGet(PID_UTIL), DO_FRIENDLY, KILL_TIMEOUT_FRIENDLY);
   GEN_WaitProcess(GLOBAL_PidGet(PID_UTIL));
   GLOBAL_PidPut(PID_UTIL, 0);

   //
   tPid = fork();
   //
   //    This thread
   //         +
   //         |                  Util thread
   //         +-----------------------+
   //         |                       |
   //         |                    PID_UTIL
   //         |                       |
   //         |                       |
   //
   switch(tPid)
   {
      case 0:
         //====================================================================
         // Util thread: additional tool to handle commsnds
         //====================================================================
         //PRINTF("hlp-RunUtility():Util thread" CRLF);
         GLOBAL_DUMPCOMMAND("hlp-RunUtility():PID_UTIL(inherit)", pstCmd);
         utl_RunUtility(pstCmd, 0, 0, pstArgs, iNumArgs);
         //
         // We will most likely NOT return here, unless something went wrong
         //
         _exit(1);
   
      case -1:
         //====================================================================
         // Error
         //====================================================================
         fCc = FALSE;
         break;

      default:
         //====================================================================
         // Parent thread
         //====================================================================
         //    - save Util thread pid
         //    - return completion
         //
         //PRINTF1("hlp-RunUtility():Help thread: Util-pid=%d" CRLF, tPid);
         GLOBAL_PidPut(PID_UTIL, tPid);
         break;
   }
   return(fCc);
}

// 
// Function:   hlp_RecordingPause
// Purpose:    Pause an ongoing (cyclic) recording
// 
// Parameters: Delay(Secs) before pause, MV struct, Fd
// Returns:    
// Note:       Running the Help-Thread
//
static void hlp_RecordingPause(int iDelay, MVIMG *pstMv, int iFd)
{
   bool     fRunning=TRUE;
   u_int32  ulSecsNow, ulSecsWait;
   u_int32  ulSizeOld, ulSizeNew;

   ulSecsWait = pstMv->ulSecsMotion + iDelay;

   #ifdef FEATURE_PRINT_MOTION
   printf("hlp-RecordingPause():Keep recording......." CRLF);
   #endif

   //
   // We need to keep reading frames to ensure the actual video data is written
   // to the circular buffer as well !!
   // So: NOT GEN_Sleep(iDelay);
   //
   do
   {
      VEC_SkipMotionFrame(pstMv, iFd);
      ulSecsNow = RTC_GetSystemSecs();
   }
   while(ulSecsNow < ulSecsWait);
   //
   do
   {
      //
      // Give RaspiVid max 3 secs to pause
      //
      ulSecsWait = ulSecsNow + 3;
      //
      GEN_Signal(PID_UTIL, SIGUSR1);
      //
      // Wait till recording has paused
      //
      ulSizeNew = hlp_RecordingGetSize();
      //
      do
      {
         ulSecsNow = RTC_GetSystemSecs();
         ulSizeOld = ulSizeNew;
         //GEN_Sleep(100);
         ulSizeNew = hlp_RecordingGetSize();

         if(ulSizeOld != ulSizeNew)
         {
            //LOG_Report(0, "HLP", "hlp-RecordingPause():Old=%u, New=%u", ulSizeOld, ulSizeNew);

            #ifdef FEATURE_PRINT_MOTION
               printf("hlp-RecordingPause():Old=%d, New=%d" CRLF, (int)ulSizeOld, (int)ulSizeNew);
            #endif
         }
      }
      while( (ulSizeOld != ulSizeNew) && (ulSecsNow < ulSecsWait) );
      //
      // We HAVE to be stopped now, otherwise RaspiVid is still running: retry !
      //
      if(ulSizeOld == ulSizeNew) fRunning = FALSE;
      else
      {
         LOG_Report(0, "HLP", "hlp-RecordingPause():RaspiVid still running: Retry");
         GEN_Signal(PID_UTIL, SIGUSR1);
      }
   }
   while(fRunning);

   #ifdef FEATURE_PRINT_MOTION
   printf("hlp-RecordingPause(): Paused" CRLF);
   #endif
}

// 
// Function:   hlp_RecordingResume
// Purpose:    Resume an ongoing (cyclic) recording
// 
// Parameters: Delay(mSecs) before resume
// Returns:    
// Note:       Running the Help-Thread
//
static void hlp_RecordingResume(int iDelay)
{
   GEN_Sleep(iDelay);
   GEN_Signal(PID_UTIL, SIGUSR1);

   #ifdef FEATURE_PRINT_MOTION
   printf("hlp-RecordingResume()" CRLF);
   #endif
}

//
// Function:   hlp_RecordingGetSize
// Purpose:    Get the filesize of a segmented recording
//             
// Parms:      
// Returns:    Size of all recording files in bytes
// Note:       Recording is pseudo cyclic: X segments of Y MB are being 
//             recorded during MD. When the recording pauses, the size 
//             will remain constant. If not, the recirding is still ongoing.
//
static u_int32 hlp_RecordingGetSize(void)
{
   u_int32     ulSegment, ulSize=0;
   int         x, iSg, iWr;
   char       *pcTemp;
   char       *pcFile;

   iSg    = atoi(pstMap->G_pcSegment);
   iWr    = atoi(pstMap->G_pcWrap);
   //
   if(iSg && iWr)
   {
      pcTemp = safemalloc(16);
      pcFile = safemalloc(MAX_PATH_LEN);
      for(x=1; x<=iWr; x++)
      {
         //
         // Build input image filename f.i. /mnt/rpipix/motion_video_%d.h264
         //
         GEN_SNPRINTF(pcFile, MAX_PATH_LEN, "%s%s_%d.h264", pstMap->G_pcRamDir, pcMotionVideo, x);
         //
         FINFO_GetFileInfo(pcFile, FI_SIZE, pcTemp, 15);
         PRINTF2("hlp-RecordingGetSize():%s=%s" CRLF, pcFile, pcTemp);
         ulSegment = strtoul(pcTemp, NULL, 10);
         ulSize   += ulSegment;
      }
      safefree(pcFile);
      safefree(pcTemp);
   }
   return(ulSize);
}

// 
// Function:   hlp_RecordingStop
// Purpose:    Stop an ongoing (cyclic) recording
// 
// Parameters: Delay(mSecs) before stop
// Returns:    
// Note:       Running the Help-Thread
//
static void hlp_RecordingStop(int iDelay)
{
   GEN_Sleep(iDelay);
   //
   GEN_Signal(PID_UTIL, SIGUSR2);
   GEN_WaitProcess(GLOBAL_PidGet(PID_UTIL));
   GLOBAL_PidPut(PID_UTIL, 0);
   
   #ifdef FEATURE_PRINT_MOTION
   printf("hlp-RecordingStop()" CRLF);
   #endif
}

// 
// Function:   hlp_RemoteIo
// Purpose:    Turn Remote IO hardware On/Off depending the motion
// 
// Parameters: Remote IO bits 7:0
// Returns:    
// Note:       Running the Help-Thread
//
static void hlp_RemoteIo(int iIo)
{
   if(GEN_STRLEN(pstMap->G_pcRemoteIp))
   {
      GEN_SNPRINTF(pstMap->G_pcRemoteIo, MAX_PARM_LEN, "%d", iIo);
      PRINTF3("hlp-RemoteIo():Send 0x%04X(%s) to proxy(%s)" CRLF, iIo, pstMap->G_pcRemoteIo, pstMap->G_pcRemoteIp);

      #ifdef FEATURE_PRINT_MOTION
      printf("hlp-RemoteIo():Send 0x%04X(%s) to proxy(%s)" CRLF, iIo, pstMap->G_pcRemoteIo, pstMap->G_pcRemoteIp);
      #endif   //FEATURE_PRINT_MOTION

      GLOBAL_SetSignalNotification(PID_PROX, GLOBAL_CMD_RUN);
      GEN_Signal(PID_PROX, SIGUSR1);
   }
   #ifdef FEATURE_PRINT_MOTION
   else
   {
      printf("hlp-RemoteIo():NO proxy(%s)" CRLF, pstMap->G_pcRemoteIp);
   }
   #endif   //FEATURE_PRINT_MOTION

}

// 
// Function:   hlp_ReportMotion
// Purpose:    Write current MV motion data to file
// 
// Parameters: Mv Data, tMotion
// Returns:    
// Note:       
//
static void hlp_ReportMotion(MVIMG *pstMv, MOTYPE tMotion)
{
   FILE    *ptFile=NULL;
   int      iSize, iMv, iIdx;
   u_int16  usCount, usCid;
   char    *pcTimeStp;

   if( (ptFile = safefopen((char *)pcMotionReport, "a+")) )
   {
      
      iSize      = RTC_ConvertDateTimeSize(TIME_FORMAT_WW_DD_MM_YYYY_HH_MM_SS);
      pcTimeStp  = safemalloc(iSize+1);
      RTC_ConvertDateTime(TIME_FORMAT_WW_DD_MM_YYYY_HH_MM_SS, pcTimeStp, pstMv->ulSecsMotion);
      //
      GEN_FPRINTF(ptFile, "%s:%s:%s ", pcTimeStp, RPI_GetMotionMode(pstMap->G_stMotionDetect.iMotMode), pcMotionType[tMotion]);
      //
      if( (iIdx = pstMv->iFrameIdx) > 0 )
      {
         //
         // These are the followin frames
         //
         GEN_FPRINTF(ptFile, "#Cl=%5d (Frame %d)", pstMv->usNrOfClusters, iIdx);
      }
      else
      {
         //
         // This is the initial motion frame
         //
         GEN_FPRINTF(ptFile, "Cal=%5d (Frame %d)", pstMv->iCalMotionVectors, iIdx);
         GEN_FPRINTF(ptFile, "Max=%5d: ", pstMv->iMaxNormalVector);
         GEN_FPRINTF(ptFile, "#Cl=%5d ", pstMv->usNrOfClusters);
      }
      //
      // Show remaining clusters: coords and size
      // Count sizes of each CID 1...? (ID=0 is not used)
      //
      for(usCid=1; usCid<=pstMv->usNrOfClusters; usCid++)
      {
         usCount = VEC_GetClusterSize(pstMv, usCid);
         iMv     = VEC_GetClusterCenter(pstMv, usCid);
         GEN_FPRINTF(ptFile, "%5d(%3d,%2d)=%3d ", usCid, iMv%pstMv->iMotionVectorsHor, iMv/pstMv->iMotionVectorsHor, usCount);
      }
      GEN_FPRINTF(ptFile, CRLF);
      //
      safefree(pcTimeStp);
      safefclose(ptFile);
   }
   else
   {
      LOG_Report(errno, "HLP", "hlp-ReportMotion(): ERROR:%s", strerror(errno));
   }
}

// 
// Function:   hlp_SaveRamfiles
// Purpose:    Save all motion files from the RAM dir to persistent filesystem
// 
// Parameters: Movement image struct, Source dir
// Context:    PID_HELP
// Returns:    system call return code (-1 is error)
// Note:       Create timestamp dir to save video, pictute and data files
//
static int hlp_SaveRamfiles(MVIMG *pstMv, const char *pcSrcDir)
{
   int      iSize, iCc;
   char    *pcTst;
   char    *pcDst;
   char    *pcCmd;

   iSize = RTC_ConvertDateTimeSize(TIME_FORMAT_YYYY_MM_DD_HH_MM_SS);
   //
   pcTst = safemalloc(iSize+1);
   pcDst = safemalloc(MAX_PATH_LENZ);
   pcCmd = safemalloc(MAX_PATH_LENZ);
   //
   // Build input   image filename  f.i. /mnt/rpipix/*
   // Create output image directory f.i. /all/...../video/20180706_154525/
   //
   RTC_ConvertDateTime(TIME_FORMAT_YYYY_MM_DD_HH_MM_SS, pcTst, pstMv->ulSecsMotion);
   GEN_SNPRINTF(pcDst, MAX_PATH_LEN, "%s%s%s/", pstMap->G_pcWwwDir, pcSrcDir, pcTst);
   //
   iCc = mkdir(pcDst, 0755);
   if(iCc == 0)
   {
      GEN_SNPRINTF(pcCmd, MAX_PATH_LEN, "mv %s* %s", pstMap->G_pcRamDir, pcDst);
      PRINTF1("hlp-SaveRamfiles():Save motion files [%s]" CRLF, pcCmd);
      //
      iCc = system(pcCmd);
      if(iCc) LOG_Report(errno, "HLP", "hlp-SaveRamfiles():ERROR calling ", pcCmd);
   }
   else
   {
      LOG_Report(errno, "HLP", "hlp-SaveRamfiles():Cannot create directory %s", pcDst);
   }
   safefree(pcCmd);
   safefree(pcDst);
   safefree(pcTst);
   return(iCc);
}

//
// Function:   hlp_WaitRunningUtil
// Purpose:    Wait (max 5 secs) until the utility is running
//
// Parms:      Util name
// Returns:    TRUE if running OKee
// Note:       
//
static bool hlp_WaitRunningUtil(const char *pcName)
{
   bool  fCc=FALSE;
   int   iWait;
   pid_t tPid;
   
   iWait = MOTION_WAIT_TIMEOUT/MOTION_WAIT_MSECS;
   //
   while(iWait)
   {
      tPid = GEN_GetPidByName(pcName);
      if(tPid > 0)
      {
         iWait = 0;
         fCc   = TRUE;
      }
      else 
      {
         GEN_Sleep(MOTION_WAIT_MSECS);
         iWait--;
      }
   }
   return(fCc);
}

//
// Function:   hlp_WriteFrame
// Purpose:    Write all NUM_FRAMES MV frames to file
//
// Parms:      Frame, size
// Returns:    
// Note:       
//
static void hlp_WriteFrame(MVIMG *pstMv)
{
   int      iFd, iSize, iWr;
   u_int8  *pubFrame;

   pubFrame = (u_int8 *) pstMv->pstFrames;
   iSize    = pstMv->iNumMotionVectors * sizeof(MVEC) * NUM_FRAMES;
   //
   if( (iFd = safeopen2(RPI_MVEC_PATH, O_RDWR|O_CREAT|O_TRUNC, 0640)) > 0)
   {
      iWr = safewrite(iFd, (const char *)pubFrame, iSize);
      if(iWr != iSize)
      {
         LOG_Report(errno, "HLP", "hlp-WriteFrame():Write Error:%s", strerror(errno));
      }
      safeclose(iFd);
   }
   else
   {
      LOG_Report(errno, "HLP", "hlp-WriteFrame():Open Error:%s", strerror(errno));
   }
}

#endif   //FEATURE_CAMERA

/*------  Local functions separator ------------------------------------
__COMMENT_________(){};
----------------------------------------------------------------------------*/

#ifdef COMMENT
// 
// Function:   hlp_SaveAugmentedPictures
// Purpose:    Save snapshot plus augmented to persistent fs
// 
// Parameters: MV struct
// Returns:    
// Note:       Running the Help-Thread
//
static void hlp_SaveAugmentedPictures(MVIMG *pstMv)
{
   int      iSize;
   char    *pcTimeStp;
   char    *pcTemp=safemalloc(MAX_PATH_LEN);

   //
   // Create an identical version in the permanent File storage (timestamped)
   //    f.i.:
   //    src = /mnt/rpipix/motion_0.jpg
   //    dst = /all/public/www/pix/20180517_153455_A.jpg
   //
   iSize      = RTC_ConvertDateTimeSize(TIME_FORMAT_YYYY_MM_DD_HH_MM_SS);
   pcTimeStp  = safemalloc(iSize+1);
   RTC_ConvertDateTime(TIME_FORMAT_YYYY_MM_DD_HH_MM_SS, pcTimeStp, pstMv->ulSecsMotion);
   //
   // cp input
   //
   GEN_SNPRINTF(pcTemp, MAX_PATH_LEN, "cp %s%s.%s %s" RPI_PUBLIC_PIX "%s.%s", 
                        pstMap->G_pcRamDir, (char *)pcMotionInput, pstMap->G_pcPixFileExt, 
                        pstMap->G_pcWwwDir, pcTimeStp, pstMap->G_pcPixFileExt);
   system(pcTemp);
   //
   PRINTF1("hlp-SaveAugmentedPictures():cp [%s]" CRLF, pcTemp);
   //
   // cp augmented
   //
   GEN_SNPRINTF(pcTemp, MAX_PATH_LEN, "cp %s%s.%s %s" RPI_PUBLIC_PIX "%s_A.%s", 
                        pstMap->G_pcRamDir, (char *)pcMotionAugment, pstMap->G_pcPixFileExt, 
                        pstMap->G_pcWwwDir, pcTimeStp, pstMap->G_pcPixFileExt);
   system(pcTemp);
   PRINTF1("hlp-SaveAugmentedPictures():cp [%s]" CRLF, pcTemp);
   safefree(pcTimeStp);
   safefree(pcTemp);
}

// 
// Function:   hlp_HandleTrigger
// Purpose:    Camera triggered motion, see if we have to do something. The MD video PID_UTIL is still running.
// 
// Parameters: Movement image struct, Cmd struct, Fd
// Context:    PID_HELP
// Returns:    FALSE if we are going to stop the motion detection process to take further action
// Note:       Various modes apply here:
//
//             Off         : Exit unconditionally.
//             Manual      : Exit if the motion vector exceeds the trigger threshold.
//             Cycle       : Just keep detecting motion (to handle the I/O).
//             Picture     : Kill the streaming Util-Thread (running Raspivid -x) and restart the Util-Thread running
//                           Raspistill to make a snapshot of the cause of motion, then proceed motion detect.
//             Augment     : Picture mode, but for a single trigger.
//             Recording   : Just keep recording.
//             Streaming   : Just keep streaming.
//
static bool hlp_HandleTrigger(MVIMG *pstMv, GLOCMD *pstCmd, int iFdRd)
{
   bool     fKeepRecording=TRUE;
   int      iNr;
   u_int16  usIdx;
   MOTYPE   tMotion;
   MDET    *pstMdet=&pstMap->G_stMotionDetect;

   switch(pstMdet->iMotMode)
   {
      default:
      case MOTION_MODE_OFF:
         PRINTF("hlp-HandleTrigger():MM_OFF" CRLF);
         hlp_RecordingStop(0);
         pstMv->ulSecsMotion = RTC_GetSystemSecs();
         tMotion = hlp_DetermineMotion(pstMv, "Off");
         hlp_HandleOutput(tMotion);
         hlp_ReportMotion(pstMv, MOTION_STOPPED);
         //
         fKeepRecording = FALSE;
         break;

      case MOTION_MODE_CYCLE:
         //PRINTF1("hlp-HandleTrigger():MM_CYCLE: %d MVs" CRLF, pstMv->iCalMotionVectors);
         //
         // Cycle mode,  sample frames
         //    o Store motion time/date
         //    o Sample NUM_FRAMES frames
         //    o Stop MD
         //    o Analyse all frames
         //
         if(( pstMv->iCalMotionVectors >= pstMdet->iTrigger1) && (pstMv->iMaxNormalVector >= pstMdet->iTrigger2) )
         {
            pstMv->ulSecsMotion = RTC_GetSystemSecs();
            pstMdet->iCounter++;
            //
            // Frame[0] has been acquired:
            // Read frames[1] ..  [NUM_FRAMES-1] into the cache
            // Stop recording
            //
            pstMv->iFrameIdx = 1;
            while(pstMv->iFrameIdx < NUM_FRAMES)
            {
               VEC_SkipMotionFrame(pstMv, iFdRd);
               VEC_SkipMotionFrame(pstMv, iFdRd);
               VEC_SkipMotionFrame(pstMv, iFdRd);
               VEC_SkipMotionFrame(pstMv, iFdRd);
               //
               iNr = VEC_ReadMotionFrame(pstMv, iFdRd);
               pstMv->iFrameIdx++;
            }
            //
            hlp_RecordingStop(0);
            //
            // Handle frame[0], which caused the trigger from cache
            //
            pstMv->iFrameIdx = 0;
            iNr = VEC_ReadMotionFrame(pstMv, 0);

            #ifdef FEATURE_PRINT_MOTION
            printf("MM_CYCLE(0): %d MVs" CRLF, pstMv->iCalMotionVectors);
            #endif

            usIdx   = VEC_GetClusterCenter(pstMv, pstMv->usMaxClusterSizeCid);
            tMotion = hlp_DetermineMotion(pstMv, "Cycle");
            hlp_ReportMotion(pstMv, tMotion);

            #ifdef FEATURE_PRINT_MOTION
            printf("MM_CYCLE(0): Motion  =%s"               CRLF, pcMotionType[tMotion]);
            printf("MM_CYCLE(0): Nr Of Clusters   =%d"      CRLF, pstMv->usNrOfClusters);
            printf("MM_CYCLE(0): Max Cluster Size =%d"      CRLF, pstMv->usMaxClusterSize);
            printf("MM_CYCLE(0): Cluster CoG      =(%d,%d)" CRLF, usIdx % pstMv->iMotionVectorsHor, usIdx / pstMv->iMotionVectorsHor);
            #endif   

            //
            // Handle the cached frames[1] ..  [NUM_FRAMES-1]
            //
            pstMv->iFrameIdx = 1;
            do
            {
               iNr = VEC_ReadMotionFrame(pstMv, 0);

               #ifdef FEATURE_PRINT_MOTION
               printf("MM_CYCLE(%d): %d MVs" CRLF, pstMv->iFrameIdx, pstMv->iCalMotionVectors);
               #endif

               if(iNr)
               {
                  usIdx   = VEC_GetClusterCenter(pstMv, pstMv->usMaxClusterSizeCid);
                  tMotion = hlp_DetermineMotion(pstMv, "Cycle");
                  hlp_ReportMotion(pstMv, tMotion);

                  #ifdef FEATURE_PRINT_MOTION
                  printf("MM_CYCLE(%d): Motion           =%s"      CRLF, pstMv->iFrameIdx, pcMotionType[tMotion]);
                  printf("MM_CYCLE(%d): Nr Of Clusters   =%d"      CRLF, pstMv->iFrameIdx, pstMv->usNrOfClusters);
                  printf("MM_CYCLE(%d): Max Cluster Size =%d"      CRLF, pstMv->iFrameIdx, pstMv->usMaxClusterSize);
                  printf("MM_CYCLE(%d): Cluster CoG      =(%d,%d)" CRLF, usIdx % pstMv->iMotionVectorsHor, usIdx / pstMv->iMotionVectorsHor);
                  #endif   

               }
               pstMv->iFrameIdx++;
            }
            while(pstMv->iFrameIdx < NUM_FRAMES);
            //
            // Save the files in the RamFs to persistent storage (Timestamped dir)
            //
            hlp_SaveRamfiles(pstMv, pcDirVideo);
            pstMv->iFrameIdx = 0;
            
            fKeepRecording = FALSE;
         }
         break;

      case MOTION_MODE_VIDEO:
         //PRINTF("hlp-HandleTrigger():MM_VIDEO" CRLF);
         //
         // Auto Video mode,  wait for the motion trigger
         //    o Store motion time/date
         //    o Keep recording for another few secs
         //    o Pause the Motion Detect cyclic recording
         //    o Check if we have real motion
         //    o YES:   Copy the videofiles from RAMFS to WWW
         //    o        Take snapshot
         //    o        Augment motion vectors onto the snapshot
         //    o        Exit motion vector detection
         //    o NO:    Resume the Motion Detect cyclic recording
         //
         if(( pstMv->iCalMotionVectors >= pstMdet->iTrigger1) && (pstMv->iMaxNormalVector >= pstMdet->iTrigger2) )
         {
            pstMv->ulSecsMotion = RTC_GetSystemSecs();
            pstMdet->iCounter++;
            //LOG_Report(0, "HLP", "hlp-HandleTrigger():MM_VIDEO:Count=%d, MaxNv=%d, MaxWv=%d", pstMdet->iCounter, pstMv->iMaxNormalVector, pstMv->iMaxWeightVector);
            //
            // Let video record for a while, then pause, check and copy recordings
            //
            hlp_RecordingPause(MOTION_VIDEO_CAPTURE_SECS, pstMv, iFdRd);
            tMotion = hlp_DetermineMotion(pstMv, "Video");
            hlp_HandleOutput(tMotion);
            //
            PRINTF4("hlp-HandleTrigger():MM_VIDEO-PAUSED:iCalMVs %d >= %d, iMaxNMV %d >= %d" CRLF, 
               pstMv->iCalMotionVectors, pstMdet->iTrigger1, pstMv->iMaxNormalVector, pstMdet->iTrigger2);
            //
            switch(tMotion)
            {
               default:
                  //
                  // Little motion: wait a while and resume recording
                  //
                  PRINTF("hlp-HandleTrigger():MM_VIDEO-RESUME" CRLF); 
                  hlp_RecordingResume(5000);
                  break;

               case MOTION_STOPPED:
                  PRINTF("hlp-HandleTrigger():MM_VIDEO-STOPPED" CRLF); 
                  hlp_RecordingStop(0);
                  fKeepRecording = FALSE;
                  break;

               case MOTION_LARGE:
               case MOTION_HUGE:
                  hlp_RecordingStop(0);
                  PRINTF("hlp-HandleTrigger():MM_VIDEO: take additional snapshot" CRLF);
                  pstCmd->iCommand = CAM_CMD_MAN_SNAP;
                  hlp_HandleTriggerSnapshot(pstMv, pstCmd);
                  fKeepRecording = FALSE;
                  break;
            }
            hlp_ReportMotion(pstMv, tMotion);
         }
         break;

      case MOTION_MODE_STREAM:
         //PRINTF("hlp-HandleTrigger():MM_STREAM" CRLF);
         //
         // Auto Streaming mode,  wait for the motion trigger
         //    o Store motion time/date
         //    o Keep streaming until stopped (manual or duration)
         //
         if(( pstMv->iCalMotionVectors >= pstMdet->iTrigger1) && (pstMv->iMaxNormalVector >= pstMdet->iTrigger2) )
         {
            pstMv->ulSecsMotion = RTC_GetSystemSecs();
            pstMdet->iCounter++;
            hlp_RecordingStop(0);
            tMotion = hlp_DetermineMotion(pstMv, "Stream");
            hlp_HandleOutput(tMotion);
            hlp_ReportMotion(pstMv, tMotion);
            //
            PRINTF("hlp-HandleTrigger():MM_STREAM" CRLF);
         }
         break;

      case MOTION_MODE_MANUAL:
         //PRINTF("hlp-HandleTrigger():MM_MANUAL" CRLF);
         //
         // Manual mode
         //    o Store motion time/date
         //    o Collect <iTrigger2> non-empty frames and save them to file
         //    o Stop the Motion Detect recording
         //    o Check if we have some motion: report motion data
         //
         if(pstMv->iCalMotionVectors > pstMdet->iTrigger1)
         {
            pstMdet->iCounter++;
            //
            if(pstMdet->iCounter == 1)
            {
               //
               // Start sampling manual MV frames
               //
               pstMv->ulSecsMotion = RTC_GetSystemSecs();
            }
            if(pstMdet->iCounter < pstMdet->iTrigger2)
            {
               //
               // Quickly sample manual MV frames up to iTrigger2 frames.
               // The pipe-line is still being filled with new MVs so do NOT take too long.
               //
               VEC_SaveMotionVectors(pstMv, "Manual(Saved frame)");
            }                                          
            else
            {
               //
               // Done
               //
               hlp_RecordingStop(0);
               VEC_SaveMotionVectors(pstMv, "Manual(Final frame)");
               hlp_RecordingGetSize();
               //
               tMotion = hlp_DetermineMotion(pstMv, "Manual");
               hlp_HandleOutput(tMotion);
               hlp_ReportMotion(pstMv, tMotion);
               PRINTF1("hlp-HandleTrigger():MM_MANUAL:Motion=%s" CRLF, pcMotionType[tMotion]);
               //
               fKeepRecording = FALSE;
            }
         }
         break;

      case MOTION_MODE_AUGMENT:
         //
         // Augment picture mode, if motion beyond user threshold (trigger):
         //    o Store motion time/date
         //    o Exit motion vector detection
         //    o Augment motion vectors onto the last snapshot
         //
         if(( pstMv->iCalMotionVectors >= pstMdet->iTrigger1) && (pstMv->iMaxNormalVector >= pstMdet->iTrigger2) )
         {
            pstMv->ulSecsMotion = RTC_GetSystemSecs();
            pstMdet->iCounter++;
            hlp_RecordingStop(0);
            tMotion = hlp_DetermineMotion(pstMv, "Augment");
            hlp_HandleOutput(tMotion);
            hlp_ReportMotion(pstMv, tMotion);
            fKeepRecording = FALSE;
            //
            PRINTF("hlp-HandleTrigger():MM_AUGMENT" CRLF);
         }
         break;

      case MOTION_MODE_PICTURE:
         //
         // Snapshot picture mode, if motion beyond user threshold (trigger):
         //    o Store motion time/date
         //    o Stop the Motion Detect recording
         //    o Check if we have real motion
         //    o YES:   Take snapshot
         //    o        Augment motion vectors onto the snapshot
         //    o        Copy the files from RAMFS to WWW
         //    o Exit motion vector detection
         //
         if(( pstMv->iCalMotionVectors >= pstMdet->iTrigger1) && (pstMv->iMaxNormalVector >= pstMdet->iTrigger2) )
         {
            pstMv->ulSecsMotion = RTC_GetSystemSecs();
            pstMdet->iCounter++;
            hlp_RecordingStop(0);
            //
            tMotion = hlp_DetermineMotion(pstMv, "Picture");
            hlp_HandleOutput(tMotion);
            //
            switch(tMotion)
            {
               default:
                  //
                  // Little motion: restart recording
                  //
                  PRINTF("hlp-HandleTrigger():MM_PICTURE:Little motion" CRLF);
                  fKeepRecording = FALSE;
                  break;

               case MOTION_LARGE:
               case MOTION_HUGE:
                  LOG_Report(0, "HLP", "hlp-HandleTrigger():MM_PICTURE:Count=%d, MaxNv=%d, MaxWv=%d", pstMdet->iCounter, pstMv->iMaxNormalVector, pstMv->iMaxWeightVector);
                  PRINTF1("hlp-HandleTrigger():MM_PICTURE:Take snapshot (Ctr=%d)" CRLF, pstMdet->iCounter);
                  pstCmd->iCommand = CAM_CMD_MAN_SNAP;
                  hlp_HandleTriggerSnapshot(pstMv, pstCmd);
                  PRINTF("hlp-HandleTrigger():MM_PICTURE:Snapshot handled" CRLF);
                  fKeepRecording = FALSE;
                  break;
            }
            hlp_ReportMotion(pstMv, tMotion);
         }
         break;
   }
   //
   GEN_SNPRINTF(pstMap->G_pcDetectCount,   MAX_PARM_LEN, "%d", pstMdet->iCounter);
   GEN_SNPRINTF(pstMap->G_pcDetectCluster, MAX_PARM_LEN, "%d", pstMdet->iCluster);
   //
   return(fKeepRecording);
}

#endif   //COMMENT

