/*  (c) Copyright:  2017  Patrn, Confidential Data
 *
 *  Workfile:           dyn_cam.h
 *  Revision:          
 *  Modtime:           
 *
 *  Purpose:            Extract headerfile for CAM dynamic http pages
 *
 *
 *
 *  Compiler/Assembler: Raspbian Linux GNU gcc
 *  Ext Packages:
 *
 *  Author:             Peter Hillen
 *  Date Created:       09 Mar 2017
 *
 *  Revisions:
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
**/

EXTRACT_DYN(CAM_DYN_HTML_CAM,         HTTP_HTML,  10000,   DYN_FLAG_PORT, "cam.html",          cam_DynPageCameraHtml      )
EXTRACT_DYN(CAM_DYN_JSON_CAM,         HTTP_JSON,  10000,   DYN_FLAG_PORT, "cam.json",          cam_DynPageCameraJson      )
EXTRACT_DYN(CAM_DYN_HTML_SNAPSHOT,    HTTP_HTML,  10000,   DYN_FLAG_PORT, "snapshot.html",     cam_DynPageSnapshotHtml    )
EXTRACT_DYN(CAM_DYN_JSON_SNAPSHOT,    HTTP_JSON,  10000,   DYN_FLAG_PORT, "snapshot.json",     cam_DynPageSnapshotJson    )
EXTRACT_DYN(CAM_DYN_HTML_TIMELAPSE,   HTTP_HTML,  10000,   DYN_FLAG_PORT, "timelapse.html",    cam_DynPageTimelapseHtml   )
EXTRACT_DYN(CAM_DYN_JSON_TIMELAPSE,   HTTP_JSON,  10000,   DYN_FLAG_PORT, "timelapse.json",    cam_DynPageTimelapseJson   )
EXTRACT_DYN(CAM_DYN_HTML_RECORD,      HTTP_HTML,  10000,   DYN_FLAG_PORT, "record.html",       cam_DynPageRecordHtml      )
EXTRACT_DYN(CAM_DYN_JSON_RECORD,      HTTP_JSON,  10000,   DYN_FLAG_PORT, "record.json",       cam_DynPageRecordJson      )
EXTRACT_DYN(CAM_DYN_HTML_STREAM,      HTTP_HTML,  10000,   DYN_FLAG_PORT, "stream.html",       cam_DynPageStreamHtml      )
EXTRACT_DYN(CAM_DYN_JSON_STREAM,      HTTP_JSON,  10000,   DYN_FLAG_PORT, "stream.json",       cam_DynPageStreamJson      )
