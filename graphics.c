/*  (c) Copyright:  2014..2018 Patrn, Confidential Data
 *
 *  Workfile:           graphics.c
 *  Revision:          
 *  Modtime:           
 *
 *  Purpose:            Add graphics capabilities for still pix analysis
 *                      iThreshold comes from the PiCal app (MotionPix) and dictates the difference in pixel-values. The LSbit enabled 
 *                      the dump of the levels to stdout
 *
 *  Compiler/Assembler: Raspbian Linux GNU gcc
 *  Ext Packages:
 * 
 *  Author:             Peter Hillen
 *  Date Created:       29 Apr 2014
 * 
 *  Revisions:
 *  Entry Points:
 * 
 *
 *
 *
 *
 * 
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
**/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <sys/types.h>
#include <magick/api.h>
#include <wand/magick_wand.h>

#include <common.h>
#include "config.h"
#include "globals.h"
#include "cmd_cad.h"
#include "graphics.h"

//#define USE_PRINTF
#include <printf.h>

//
// Local prototypes
//
static int              grf_HighlightGrids         (MagickWand *, char *, char *, MDET *, int, int);
static void             grf_ImageDrawLine          (MagickWand *, const char *, int, int, int, int);
static int              grf_ImageGetDelta          (char *, char *, int, char *, int, int, int, int, int, int);
static int              grf_CreateDeltaImage       (char *, char *, int, char *, int, int, int, int, int, int);
static int              grf_ImageBufferGrayscale   (char *, MDET *, int, int, int, int, int, int);
static int              grf_ImageBufferNormalize   (char *, MDET *, int, int, int, int, int, int);
static char            *grf_MagickToImageBuffer    (MagickWand *, char *, int, int, int, int, int *);
static MagickPassFail   grf_ImageWriteMagickToFile (MagickWand *, char *, char *, char *, char *, bool);
static int              grf_ImageBufferToMagick    (MagickWand *, char *, char *, int, int, int, int);
static void             grf_ImageWriteTft          (char *, int, int, int);

#ifdef USE_DRAW_RECT
static void             grf_ImageDrawRect          (MagickWand *, const char *, int, int, int, int);
static void             grf_ImageDrawText          (MagickWand *, const char *, char *, int, int);
#endif //USE_DRAW_RECT

#ifdef FEATURE_WRITE_TFT
static void             grf_DrawPicture            (u_int16 *, u_int32);
#endif //FEATURE_WRITE_TFT

#ifdef FEATURE_DUMP_RGB_LEVELS
//
// Dump RGB levels
//
static void grf_DumpRgbLevels(MDET *, int, int, int);
//
#define GRF_DUMP_RGB_LEVELS(s,g,t,m)     grf_DumpRgbLevels(s,g,t,m)

#else
//
// No dump
//
#define GRF_DUMP_RGB_LEVELS(s,g,t,m)
//
#endif   //FEATURE_DUMP_RGB_LEVELS


static       char *pcNull         = "";
//
// files in RAM filesystem, f.i. /mnt/rpipix/
// timestamped files in WWW filesystem, f.i. /all/public/www/
//
static const char *pcMotionOrigin      = "motion_origin";
static const char *pcMotionHlight      = "motion_hlight";
static const char *pcMotionDeltas      = "motion_deltas";
static const char *pcMotionPubNew      = "motion_pubnew";
//
static const char *pcMotion0           = "motion_0";
static const char *pcMotionNorm0       = "motion_0_norm";
static const char *pcMotionZoom0       = "motion_0_zoom";
static const char *pcMotion1           = "motion_1";
static const char *pcMotionNorm1       = "motion_1_norm";
static const char *pcMotionZoom1       = "motion_1_zoom";
//
static const char *pcMotionError       = RPI_PUBLIC_ERROR;
static const char *pcRgb               = "RGB";
//
/* ======   Local Functions separator ===========================================
void ___global_functions(){} 
==============================================================================*/

// 
//  Function:   GRF_Init
//  Purpose:    Graphics init
//
//  Parms:      
//  Returns:    -1 if error
//
int GRF_Init()
{
   return(0);
}

//
// Function:   GRF_AugmentMotionVectorFile
// Purpose:    Create a file showing the motion vectors
//
// Parms:      MVIMG *, Src image filename, Dest dir, filename, ext
// Returns:    TRUE if OKee
// Note:       Input     = Input file
//             Augmented = <pcDir><pcName>.<pcExt>
//
bool GRF_AugmentMotionVectorFile(MVIMG *pstMv, char *pcInput, char *pcDir, char *pcName, char *pcExt)
{
   bool           fCc=FALSE;
   int            iHs, iWs;
   int            iHe, iWe;
   int            iHt, iWt;
   int            iKey, iIdx;
   char          *pcImage=NULL;
   MagickWand    *pstWand;
   MagickPassFail tStatus=MagickPass;
   MDET          *pstMdet=&pstMap->G_stMotionDetect;

   //PRINTF1("GRF-AugmentMotionVectorFile():Using format[%s]" CRLF, pstMdet->pcFormat);
   //
   //====================================================================================
   // Initialize GraphicsMagick API
   //====================================================================================
   InitializeMagick(NULL);
   //====================================================================================
   // Allocate Wand handles
   //====================================================================================
   pstWand   = NewMagickWand();
   //====================================================================================
   // pix --> Wand
   //====================================================================================
   tStatus = MagickReadImage(pstWand, pcInput);
   iHt     = MagickGetImageHeight(pstWand);
   iWt     = MagickGetImageWidth(pstWand);
   //
   PRINTF3("GRF-AugmentMotionVectorFile():OKEE : Input image(%s) w=%d, h=%d" CRLF, pcInput, iWt, iHt);
   //
   // Make sure we have a valid image
   //
   if((iHt == 0) || (iWt == 0)) 
   {
      PRINTF1("GRF-AugmentMotionVectorFile():BAD input image %s" CRLF, pcInput);
      tStatus = MagickFail;
   }
   else
   {
      //
      if (tStatus == MagickPass) tStatus = MagickNormalizeImage(pstWand);
      //====================================================================================
      // Wand --> Image
      //====================================================================================
      pcImage = grf_MagickToImageBuffer(pstWand, pstMdet->pcFormat, 0, 0, iWt, iHt, NULL);
      //====================================================================================
      // Grayscale(Image) --> Image
      //====================================================================================
      grf_ImageBufferGrayscale(pcImage, pstMdet, iWt, iHt, 0, 0, iWt, iHt);
      //====================================================================================
      // Normalize(Image) --> Image
      //====================================================================================
      grf_ImageBufferNormalize(pcImage, pstMdet, iWt, iHt, 0, 0, iWt, iHt);
      //====================================================================================
      // Image --> Wand
      //====================================================================================
      if( grf_ImageBufferToMagick(pstWand, pcImage, pstMdet->pcFormat, 0, 0, iWt, iHt) )
      {
         #ifdef TEST_COORD_CORNERS
         // 
         // grf_ImageDrawPoint(pstWand, "red",    0,     0);
         // grf_ImageDrawPoint(pstWand, "blue",   0,   iHt);
         // grf_ImageDrawPoint(pstWand, "orange", iWt,   0);
         // grf_ImageDrawPoint(pstWand, "white",  iWt, iHt);
         //
         grf_ImageDrawLine(pstWand, "red",        0,     0,     4,     0);
         grf_ImageDrawLine(pstWand, "red",        0,     0,     0,     4);
         grf_ImageDrawLine(pstWand, "blue",       0, iHt-5,     0, iHt-1);
         grf_ImageDrawLine(pstWand, "blue",       0, iHt-1,     4, iHt-1);
         grf_ImageDrawLine(pstWand, "orange", iWt-5,     0, iWt-1,     0);
         grf_ImageDrawLine(pstWand, "orange", iWt-1,     0, iWt-1,     4);
         grf_ImageDrawLine(pstWand, "white",  iWt-5, iHt-1, iWt-1, iHt-1);
         grf_ImageDrawLine(pstWand, "white",  iWt-1, iHt-5, iWt-1, iHt-1);
         #endif   //TEST_COORD_CORNERS

         //
         // Mark all masked MBs
         //
         for(iIdx=0; iIdx<pstMv->iNumMotionVectors; iIdx++)
         {
            if(pstMv->pubMask[iIdx] == 0)
            {
               iHs = (iIdx / pstMv->iMotionVectorsHor) * MACRO_BLOCK_SIZE;
               iHe = iHs + MACRO_BLOCK_SIZE;
               iWs = (iIdx % pstMv->iMotionVectorsHor) * MACRO_BLOCK_SIZE;
               iWe = iWs + MACRO_BLOCK_SIZE;
               //
               // Picture 1920x1080 --> 121x67 = 8107 MacroBlocks !
               //                       121x16 = 1936 pixels
               //                        67x16 = 1072 lines
               // Keep end in range of picture !
               //
               if(iHe > iHt) iHe = iHt;
               if(iWe > iWt) iWe = iWt;
               //
               grf_ImageDrawLine(pstWand, "red", iWs, iHs, iWe, iHs);
               grf_ImageDrawLine(pstWand, "red", iWe, iHs, iWe, iHe);
               grf_ImageDrawLine(pstWand, "red", iWe, iHe, iWs, iHe);
               grf_ImageDrawLine(pstWand, "red", iWs, iHe, iWs, iHs);
            }
         }
         //====================================================================================
         // Transfer motion vectors(Image) --> Image
         // Each vector comes from a 16x16 bits macroblock
         //====================================================================================
         for(iIdx=0; iIdx<pstMv->iCalMotionVectors; iIdx++)
         {
            iKey = pstMv->pusKeys[iIdx];
            //
            iHs = (iKey / pstMv->iMotionVectorsHor) * MACRO_BLOCK_SIZE;
            iHe = iHs + MACRO_BLOCK_SIZE;
            iWs = (iKey % pstMv->iMotionVectorsHor) * MACRO_BLOCK_SIZE;
            iWe = iWs + MACRO_BLOCK_SIZE;
            //PRINTF6("GRF-AugmentMotionVectorFile():Idx=%4d, Key=%4d, MB at (%d,%d)-(%d,%d)" CRLF, iIdx, iKey, iHs, iWs, iHe, iWe);
            //PRINTF4("GRF-AugmentMotionVectorFile():Msk=%4d, MvX=%4d, MvY=%4d, SAD=%d" CRLF, 
            //            pstMv->pubMask[iKey], pstMv->pstFrame[iKey].bVectorX, pstMv->pstFrame[iKey].bVectorY, pstMv->pstFrame[iKey].usSad);
            //
            // Picture 1920x1080 --> 121x67 = 8107 MacroBlocks !
            //                       121x16 = 1936 pixels
            //                        67x16 = 1072 lines
            // Keep end in range of picture !
            //
            if(iHe > iHt) iHe = iHt;
            if(iWe > iWt) iWe = iWt;
            //
            grf_ImageDrawLine(pstWand, "yellow", iWs, iHs, iWe, iHs);
            grf_ImageDrawLine(pstWand, "yellow", iWe, iHs, iWe, iHe);
            grf_ImageDrawLine(pstWand, "yellow", iWe, iHe, iWs, iHe);
            grf_ImageDrawLine(pstWand, "yellow", iWs, iHe, iWs, iHs);
         }
         //====================================================================================
         // Write image to output (no timestamp --> FALSE)
         // Output is the global lastfile location: G_pcLastFile
         //====================================================================================
         if (tStatus == MagickPass) 
         {
            tStatus = grf_ImageWriteMagickToFile(pstWand, pcNull, pcDir, pcName, pcExt, FALSE);
            PRINTF2("GRF-AugmentMotionVectorFile():OKee completion, Image=%s, Augment=%s" CRLF, pcInput, pstMap->G_pcLastFile);
         }
      }
      else
      {
         LOG_Report(0, "GRF", "GRF-AugmentMotionVectorFile():ERROR MagickSetImagePixels()");
         PRINTF("GRF-AugmentMotionVectorFile():ERROR MagickSetImagePixels()" CRLF);
         tStatus = MagickFail;
      }
   }
   //====================================================================================
   // Diagnose any error
   //====================================================================================
   if(tStatus != MagickPass)
   {
      GEN_SNPRINTF(pstMap->G_pcLastFile, MAX_PATH_LEN, "%s", pcMotionError);
      FINFO_GetFileInfo(pstMap->G_pcLastFile, FI_SIZE, pstMap->G_pcLastFileSize, MAX_PATH_LEN);
      LOG_Report(0, "GRF", "GRF-AugmentMotionVectorFile():MAGICK ERROR, Image=%s", pcInput);
      PRINTF1("GRF-AugmentMotionVectorFile():MAGICK ERROR, Image=%s" CRLF, pcInput);
   }
   else
   {
      fCc = TRUE;
   }
   DestroyMagickWand(pstWand);
   DestroyMagick();
   safefree(pcImage);
   //
   return(fCc);
}

//
//  Function:   GRF_MotionCreateMask
//  Purpose:    Create motion detect mask array from template picture
//
//  Parms:      MotionVector data, Pix path, Width, height
//  Returns:    Nr pixels Masked
//
int GRF_MotionCreateMask(MVIMG *pstMv, char *pcFile, int iW, int iH)
{
   int            iHi, iWi;
   int            i, iVal, iGray, iSum, iFormatLength, iMasked=0;
   int            iR, iG, iB, iIdx;
   u_int8        *pubMask;
   char          *pcImage=NULL;
   char          *pcFrmt;
   char          *pcSrc;
   MagickPassFail tStatus=MagickPass;
   MagickWand    *pstWand;
   MDET          *pstMdet=&pstMap->G_stMotionDetect;

   //
   // Initialize GraphicsMagick API
   //
   InitializeMagick(NULL);
   //
   // Allocate Wand handles
   //
   pstWand = NewMagickWand();
   //
   if((tStatus = MagickReadImage(pstWand, pcFile)) == MagickPass)
   {
      pcFrmt = MagickGetImageFormat(pstWand);
      if(pcFrmt == NULL)
      {
         PRINTF2("GRF-MotionCreateMask(): BAD Image Format, image=%s, status=%d" CRLF, pcFile, tStatus);
         DestroyMagickWand(pstWand);
         DestroyMagick();
         return(0);
      } 
      PRINTF1("GRF-MotionCreateMask(): Image Format=[%s]" CRLF, pcFrmt);
      free(pcFrmt);
      //====================================================================================
      // Check if the picture has the correct format
      //====================================================================================
      iHi = MagickGetImageHeight(pstWand);
      iWi = MagickGetImageWidth(pstWand);
      //
      if( (iHi == iH) && (iWi == iW) )
      {
         iFormatLength = GEN_STRLEN(pstMdet->pcFormat);
         pcImage       = grf_MagickToImageBuffer(pstWand, (char *)pcRgb, 0, 0, iWi, iHi, NULL);
         pubMask       = pstMv->pubMask;
         iR            = pstMdet->iThldRed;
         iG            = pstMdet->iThldGreen;
         iB            = pstMdet->iThldBlue;
         //
         for(iIdx=0; iIdx<pstMv->iNumMotionVectors; iIdx++)
         {
            iHi = (iIdx / pstMv->iMotionVectorsHor) * MACRO_BLOCK_SIZE;
            iWi = (iIdx % pstMv->iMotionVectorsHor) * MACRO_BLOCK_SIZE;
            //PRINTF3("GRF-MotionCreateMask():i=%4d: MB at (%d, %d)" CRLF, iIdx, iWi, iHi);
            //
            // Create mask from Image (Pixel value = black (0x00)
            // Picture 1920x1080 --> 121x67 = 8107 MacroBlocks !
            //                       121x16 = 1936 pixels hor
            //                        67x16 = 1072 pixels ver
            //
            pcSrc = &pcImage[iFormatLength*((iHi*iW)+iWi)];
            iGray = iSum = 0;
            //
            for(i=0; i<iFormatLength; i++)
            {
               iVal = (int) *pcSrc++;
               //
               switch(pstMdet->pcFormat[i])
               {
                  case 'R':
                     iGray += (iR * iVal);
                     iSum  +=  iR;
                     break;

                  case 'G':
                     iGray += (iG * iVal);
                     iSum  +=  iG;
                     break;

                  case 'B':
                     iGray += (iB * iVal);
                     iSum  +=  iB;
                     break;

                  default:
                  //case 'Y':
                  //case 'U':
                  //case 'V':
                  //case 'I':
                     iGray += (1000 * iVal);
                     iSum  += 1000;
                     break;
               }
            }
            iGray /= iSum;
            *pubMask++ = (u_int8) iGray;
            if(iGray == 0) iMasked++;
         }
         PRINTF4("GRF-MotionCreateMask():Image[%s] (%d,%d):Masked=%d MBs" CRLF, pcFile, iWi, iHi, iMasked);
         safefree(pcImage);
      }
      else
      {
         PRINTF5("GRF-MotionCreateMask():Image [%s] wrong size (%d,%d) i.o. (%d,%d)" CRLF, pcFile, iWi, iHi, iW, iH);
      }
   }
   else
   {
      PRINTF2("GRF-MotionCreateMask():Problems reading image [%s], status=%d" CRLF, pcFile, tStatus);
   }
   //
   // Clean up
   //
   DestroyMagickWand(pstWand);
   DestroyMagick();
   return(iMasked);
}

//
//  Function:   GRF_MotionDisplay
//  Purpose:    Graphics motion display on TFT
//
//  Parms:      Pix dir path
//  Returns:    
//
void GRF_MotionDisplay(char *pcFile)
{
   int            iSize, iHi, iWi;
   char          *pcImage=NULL;
   char          *pcFrmt;
   MagickPassFail tStatus=MagickPass;
   MagickWand    *pstWand;

   //
   // Initialize GraphicsMagick API
   //
   InitializeMagick(NULL);
   //
   // Allocate Wand handles
   //
   pstWand = NewMagickWand();

   if((tStatus = MagickReadImage(pstWand, pcFile)) == MagickPass)
   {
      pcFrmt = MagickGetImageFormat(pstWand);
      if(pcFrmt)
      {
         PRINTF1("GRF-MotionDisplay(): Image Format=[%s]" CRLF, pcFrmt);
         free(pcFrmt);
      }
      else
      {
         PRINTF2("GRF-MotionDisplay(): BAD Image Format, image=%s, status=%d" CRLF, pcFile, tStatus);
         DestroyMagickWand(pstWand);
         DestroyMagick();
         return;
      } 
      //====================================================================================
      // Check if we need to scale the picture
      //====================================================================================
      iHi     = MagickGetImageHeight(pstWand);
      iWi     = MagickGetImageWidth(pstWand);
      pcImage = grf_MagickToImageBuffer(pstWand, (char *)pcRgb, 0, 0, iWi, iHi, &iSize);
      //
      // Check if formats are OKee
      //
      PRINTF3("GRF-MotionDisplay(): Displaying w=%d h=%d[%s]..." CRLF, iWi, iHi, pcFile);
      LCD_MODE(LCD_MODE_BMP);
      iSize = GEN_STRLEN(pcRgb);
      grf_ImageWriteTft(pcImage, iWi, iHi, iSize);
      safefree(pcImage);
   }
   else
   {
      PRINTF2("GRF-MotionDisplay(): Problems reading image [%s], status=%d" CRLF, pcFile, tStatus);
   }
   //
   // Clean up
   //
   DestroyMagickWand(pstWand);
   DestroyMagick();
}

//
//  Function:   GRF_MotionDetect
//  Purpose:    Graphics motion detect 
//
//  Parms:      
//  Returns:    Number of cells exceeding the motion threshold or -1 if error
//
int GRF_MotionDetect()
{
   int            iDelta=0;
   int            iHtOld=0, iWtOld=0, iHtNew=0, iWtNew=0;
   char          *pcFileOld; 
   char          *pcFileNew; 
   char          *pcImageOld; 
   char          *pcImageNew; 
   MagickPassFail tStatus=MagickPass;
   MagickWand    *pstWandOld;
   MagickWand    *pstWandNew;
   MDET          *pstMdet=&pstMap->G_stMotionDetect;

   PRINTF1("GRF-MotionDetect(0): using format [%s]" CRLF, pstMdet->pcFormat);
   //
   // Initialize GraphicsMagick API
   //
   InitializeMagick(NULL);
   //
   // Allocate Wand handles
   //
   pstWandOld = NewMagickWand();
   pstWandNew = NewMagickWand();
   //
   pcFileOld = safemalloc(MAX_PATH_LEN);
   pcFileNew = safemalloc(MAX_PATH_LEN);
   //
   if(pstMdet->iSeq & 1)
   {
      //
      // Old image is motion_0.ext
      // New image is motion_1.ext
      //
      GEN_SPRINTF(pcFileOld, "%s%s.%s", pstMap->G_pcRamDir, pcMotion0, pstMap->G_pcPixFileExt);
      GEN_SPRINTF(pcFileNew, "%s%s.%s", pstMap->G_pcRamDir, pcMotion1, pstMap->G_pcPixFileExt);
   }
   else
   {
      //
      // Old image is motion_1.ext
      // New image is motion_0.ext
      //
      GEN_SPRINTF(pcFileOld, "%s%s.%s", pstMap->G_pcRamDir, pcMotion1, pstMap->G_pcPixFileExt);
      GEN_SPRINTF(pcFileNew, "%s%s.%s", pstMap->G_pcRamDir, pcMotion0, pstMap->G_pcPixFileExt);
   }
   //====================================================================================
   // Step-1
   // oldest pix --> WandOld
   // newest pix --> WandNew
   //====================================================================================
   if (tStatus == MagickPass) tStatus = MagickReadImage(pstWandOld, pcFileOld);
   if (tStatus == MagickPass) PRINTF1("GRF-MotionDetect(1):Image old(%s):Status=OK"    CRLF, pcFileOld);
   else                       PRINTF1("GRF-MotionDetect(1):Image old(%s):Status=ERROR" CRLF, pcFileOld);
   //
   if (tStatus == MagickPass) tStatus = MagickReadImage(pstWandNew, pcFileNew);
   if (tStatus == MagickPass) PRINTF1("GRF-MotionDetect(1):Image new(%s):Status=OK"    CRLF, pcFileNew);
   else                       PRINTF1("GRF-MotionDetect(1):Image new(%s):Status=ERROR" CRLF, pcFileNew);
   //====================================================================================
   // Step-2
   // Abort if both images have different sizes
   //====================================================================================
   if (tStatus == MagickPass)
   {
      PRINTF( "GRF-MotionDetect(2):MagickReadImage() OKee" CRLF);
      iHtOld = MagickGetImageHeight(pstWandOld);
      iWtOld = MagickGetImageWidth(pstWandOld);
      iHtNew = MagickGetImageHeight(pstWandNew);
      iWtNew = MagickGetImageWidth(pstWandNew);
      //
      // Check if formats are non-zero and identical
      //
      if(iWtOld && iWtNew && iHtOld && iHtNew)
      {
         if( (iWtOld != iWtNew) || (iHtOld != iHtNew) )
         {
            // Nope: abort
            PRINTF( "GRF-MotionDetect(2):ERROR! Bad Image(s)" CRLF);
            PRINTF3("GRF-MotionDetect(2): Image(%s):w=%d, h=%d" CRLF, pcFileOld, iWtOld, iHtOld);
            PRINTF3("GRF-MotionDetect(2): Image(%s):w=%d, h=%d" CRLF, pcFileNew, iWtNew, iHtNew);
            tStatus = MagickFail;
         }
      }
      else
      {
         tStatus = MagickFail;
      }
   }
   if (tStatus != MagickPass)
   {
      GEN_SPRINTF(pstMap->G_pcLastFile, "%s", pcMotionError);
      FINFO_GetFileInfo(pstMap->G_pcLastFile, FI_SIZE, pstMap->G_pcLastFileSize, MAX_PATH_LEN);
      DestroyMagickWand(pstWandOld);
      DestroyMagickWand(pstWandNew);
      DestroyMagick();
      safefree(pcFileOld);
      safefree(pcFileNew);
      PRINTF4("GRF-MotionDetect(2):ERROR! Images Old w=%d, h=%d, New w=%d, h=%d" CRLF, iWtOld, iHtOld, iWtNew, iHtNew);
      return(-1);
   }
   //====================================================================================
   // Two pictues to compage : Normalize colors first
   //====================================================================================
   MagickNormalizeImage(pstWandOld);
   MagickNormalizeImage(pstWandNew);
   //
   PRINTF3("GRF-MotionDetect(2):Image(%s) w=%d, h=%d" CRLF, pcFileOld, iWtOld, iHtOld);
   PRINTF3("GRF-MotionDetect(2):Image(%s) w=%d, h=%d" CRLF, pcFileNew, iWtNew, iHtNew);
   //====================================================================================
   // Step-3
   // WandOld --> ImageOld
   // WandNew --> ImageNew
   //====================================================================================
   pcImageOld = grf_MagickToImageBuffer(pstWandOld, pstMdet->pcFormat, 0, 0, iWtOld, iHtOld, NULL);
   pcImageNew = grf_MagickToImageBuffer(pstWandNew, pstMdet->pcFormat, 0, 0, iWtNew, iHtNew, NULL);
   PRINTF("GRF-MotionDetect(3):Wands to Images" CRLF);
   //====================================================================================
   // Grayscale(ImageOld) --> ImageOld
   // Grayscale(ImageNew) --> ImageNew
   //====================================================================================
   grf_ImageBufferGrayscale(pcImageOld, pstMdet, iWtOld, iHtOld, 0, 0, iWtOld, iHtOld);
   grf_ImageBufferGrayscale(pcImageNew, pstMdet, iWtNew, iHtNew, 0, 0, iWtNew, iHtNew);
   PRINTF("GRF-MotionDetect(3):Images grayscaled" CRLF);
   //====================================================================================
   // Normalize(ImageOld) --> ImageOld
   // Normalize(ImageNew) --> ImageNew
   //====================================================================================
   pstMdet->iAverage1 = grf_ImageBufferNormalize(pcImageOld, pstMdet, iWtOld, iHtOld, 0, 0, iWtOld, iHtOld);
   pstMdet->iAverage2 = grf_ImageBufferNormalize(pcImageNew, pstMdet, iWtNew, iHtNew, 0, 0, iWtNew, iHtNew);
   PRINTF2("GRF-MotionDetect(3):Images Normalized (Old=%d, New=%d)" CRLF, pstMdet->iAverage1, pstMdet->iAverage2);
   //====================================================================================
   // Step-4
   // ImageNew --> WandNew
   // Save the Grayscaled/Normalized NEW ImageBuffer to file
   //====================================================================================
   if( grf_ImageBufferToMagick(pstWandNew, pcImageNew, pstMdet->pcFormat, 0, 0, iWtNew, iHtNew) )
   {
      PRINTF("GRF-MotionDetect(4):New image to wand" CRLF);
      //====================================================================================
      // High-light cells exceeding the motion threshold between OLD and NEW images
      //====================================================================================
      iDelta = grf_HighlightGrids(pstWandNew, pcImageNew, pcImageOld, pstMdet, iWtOld, iHtOld);
      //====================================================================================
      // Step-5
      // Write hight lighted image to file
      //====================================================================================
      PRINTF3("GRF-MotionDetect(5):Save high-lighted image-file to %s%s%s" CRLF, pstMap->G_pcRamDir, pcNull, pcMotionHlight);
      tStatus = grf_ImageWriteMagickToFile(pstWandNew, pstMap->G_pcRamDir, pcNull, (char *)pcMotionHlight, pstMap->G_pcPixFileExt, FALSE);
      //====================================================================================
      // Step-6
      // ImageOld --> WandOld
      // Save the Grayscaled/Normalized OLD ImageBuffer to file
      //====================================================================================
      if( grf_ImageBufferToMagick(pstWandOld, pcImageOld, pstMdet->pcFormat, 0, 0, iWtOld, iHtOld) )
      {
         PRINTF3("GRF-MotionDetect(6):Save original normalized image-file to %s%s%s" CRLF, pstMap->G_pcRamDir, pcNull, pcMotionOrigin);
         tStatus = grf_ImageWriteMagickToFile(pstWandOld, pstMap->G_pcRamDir, pcNull, (char *)pcMotionOrigin, pstMap->G_pcPixFileExt, FALSE);
      }
      else
      {
         LOG_Report(0, "GRF", "GRF-MotionDetect(): Old-MagickSetImagePixels() error");
         PRINTF("GRF-MotionDetect(6):Old image to wand error" CRLF);
         tStatus = MagickFail;
      }
      //====================================================================================
      // Step-7
      // If we have grids that exceed the motion threshold:
      // Save permananetly: motion_YYYMMDD_HHMMSS.ext (including outlined cells) 
      //
      // Example:
      //       Public-WWW-dir    PixPath  file     Timestamp
      //       /all/public/www   pix      motion   0170205_105138
      //====================================================================================
      if(iDelta)
      {
         PRINTF("GRF-MotionDetect(7):PUBLISH high lighted file:" CRLF);
         tStatus = grf_ImageWriteMagickToFile(pstWandNew, pstMap->G_pcWwwDir, pstMap->G_pcPixPath, (char *)pcMotionHlight, pstMap->G_pcPixFileExt, TRUE);
         //====================================================================================
         // Convert the DELTA image from Grayscale to Black/White
         //====================================================================================
         grf_CreateDeltaImage(pcImageNew, pcImageOld, pstMdet->iThldPixel, pstMdet->pcFormat, iWtOld, iHtOld, 0, 0, iWtOld, iHtOld);
         //====================================================================================
         // Step-8
         // ImageNew --> WandNew --> File 
         // Save permananetly: deltas_YYYMMDD_HHMMSS.ext   
         //
         // Example:
         //       Public-WWW-dir    PixPath  file     Timestamp
         //       /all/public/www   pix      deltas   0170205_105138
         //====================================================================================
         if( grf_ImageBufferToMagick(pstWandNew, pcImageNew, pstMdet->pcFormat, 0, 0, iWtNew, iHtNew) )
         {
            PRINTF("GRF-MotionDetect(8):PUBLISH deltas-file" CRLF);
            tStatus = grf_ImageWriteMagickToFile(pstWandNew, pstMap->G_pcWwwDir, pstMap->G_pcPixPath, (char *)pcMotionDeltas, pstMap->G_pcPixFileExt, TRUE);
         }
         else
         {
            LOG_Report(0, "GRF", "GRF-MotionDetect():MagickSetImagePixels() error");
            PRINTF("GRF-MotionDetect(8):MagickSetImagePixels() error" CRLF);
            tStatus = MagickFail;
         }
         //====================================================================================
         // Step-9
         // Reload newest image and safe also permanently: newpix_YYYMMDD_HHMMSS.ext   
         //
         // Example:
         //       Public-WWW-dir    PixPath  file     Timestamp
         //       /all/public/www   pix      newpix   0170205_105138
         //====================================================================================
         if (tStatus == MagickPass) tStatus = MagickReadImage(pstWandNew, pcFileNew);
         if (tStatus == MagickPass)
         {
            PRINTF("GRF-MotionDetect(9):Save newpix-file" CRLF);
            tStatus = grf_ImageWriteMagickToFile(pstWandNew, pstMap->G_pcWwwDir, pstMap->G_pcPixPath, (char *)pcMotionPubNew, pstMap->G_pcPixFileExt, TRUE);
         }
      }
      else
      {
         PRINTF("GRF-MotionDetect(7):Images OLD and NEW are identical!" CRLF);
      }
   }
   else
   {
      PRINTF("GRF-MotionDetect(4):New image to wand error" CRLF);
      LOG_Report(0, "GRF", "GRF-MotionDetect(): New-MagickSetImagePixels() error");
      tStatus = MagickFail;
   }
   //====================================================================================
   // Diagnose any error
   //====================================================================================
   if(tStatus != MagickPass)
   {
      char          *pcDescription;
      ExceptionType  tSeverity;

      GEN_SPRINTF(pstMap->G_pcLastFile, "%s", pcMotionError);
      FINFO_GetFileInfo(pstMap->G_pcLastFile, FI_SIZE, pstMap->G_pcLastFileSize, MAX_PATH_LEN);
      //
      pcDescription = MagickGetException(pstWandOld, &tSeverity);
      LOG_Report(0, "GRF", "GRF-MotionDetect(X):Old Generic error:%s (Severity=%d)", pcDescription, tSeverity);
      PRINTF2("GRF-MotionDetect(X): Old:Generic error:%s (Severity=%d)" CRLF, pcDescription, tSeverity);
      //
      pcDescription = MagickGetException(pstWandNew, &tSeverity);
      LOG_Report(0, "GRF", "GRF-MotionDetect(X):New:Generic error:%s (Severity=%d)", pcDescription, tSeverity);
      PRINTF2("GRF-MotionDetect(X): New:Generic error:%s (Severity=%d)" CRLF, pcDescription, tSeverity);
   }
   else
   {
      //
      // This is the file we will return in the JSON object
      //
      GEN_SPRINTF(pstMap->G_pcLastFile, "%s%s.%s", pstMap->G_pcRamDir, pcMotionHlight, pstMap->G_pcPixFileExt);
      PRINTF("GRF-MotionDetect(X):OKee completion" CRLF);
   }
   DestroyMagickWand(pstWandOld);
   DestroyMagickWand(pstWandNew);
   DestroyMagick();
   safefree(pcFileOld);
   safefree(pcFileNew);
   safefree(pcImageOld);
   safefree(pcImageNew);
   return(iDelta);
}

//
//  Function:   GRF_MotionNormalize
//  Purpose:    Normalize snapshot
//
//  Parms:      
//  Returns:    TRUE if OKee
//
bool GRF_MotionNormalize()
{
   bool           fCc=FALSE;
   int            iHt, iWt;
   char          *pcFile; 
   char          *pcImage=NULL; 
   MagickWand    *pstWand;
   MagickPassFail tStatus=MagickPass;
   MDET          *pstMdet=&pstMap->G_stMotionDetect;

   PRINTF2("GRF-MotionNormalize():Using format[%s]:Seq=%d" CRLF, pstMdet->pcFormat, pstMdet->iSeq);
   //====================================================================================
   // Initialize GraphicsMagick API
   //====================================================================================
   InitializeMagick(NULL);
   //====================================================================================
   // Allocate Wand handles
   //====================================================================================
   pstWand   = NewMagickWand();
   pcFile    = safemalloc(MAX_PATH_LEN);
   //====================================================================================
   // image is motion_?.ext
   //====================================================================================
   if(pstMdet->iSeq)  GEN_SPRINTF(pcFile, "%s%s.%s", pstMap->G_pcRamDir, pcMotion1, pstMap->G_pcPixFileExt);
   else               GEN_SPRINTF(pcFile, "%s%s.%s", pstMap->G_pcRamDir, pcMotion0, pstMap->G_pcPixFileExt);
   //====================================================================================
   // pix --> Wand
   //====================================================================================
   tStatus = MagickReadImage(pstWand, pcFile);
   if (tStatus == MagickPass)
   {
      iHt = MagickGetImageHeight(pstWand);
      iWt = MagickGetImageWidth(pstWand);
      //
      PRINTF3("GRF-MotionNormalize():Image(%s) w=%d, h=%d" CRLF, pcFile, iWt, iHt);
      //
      tStatus = MagickNormalizeImage(pstWand);
      //====================================================================================
      // Wand --> Image
      //====================================================================================
      pcImage = grf_MagickToImageBuffer(pstWand, pstMdet->pcFormat, 0, 0, iWt, iHt, NULL);
      //====================================================================================
      // Grayscale(Image) --> Image
      //====================================================================================
      grf_ImageBufferGrayscale(pcImage, pstMdet, iWt, iHt, 0, 0, iWt, iHt);
      //====================================================================================
      // Normalize(Image) --> Image
      //====================================================================================
      grf_ImageBufferNormalize(pcImage, pstMdet, iWt, iHt, 0, 0, iWt, iHt);
      //====================================================================================
      // Image --> Wand
      //====================================================================================
      if( grf_ImageBufferToMagick(pstWand, pcImage, pstMdet->pcFormat, 0, 0, iWt, iHt) )
      {
         //====================================================================================
         // Write normalized image back:
         //       - to public www including timestamp
         //       - to original *_norm.*
         //       - overwrite original input
         //====================================================================================
         if(pstMdet->iSeq)  
         {
            if (tStatus == MagickPass) tStatus = grf_ImageWriteMagickToFile(pstWand, pstMap->G_pcWwwDir, pstMap->G_pcPixPath, (char *)pcMotionNorm1, pstMap->G_pcPixFileExt, TRUE);
            if (tStatus == MagickPass) tStatus = grf_ImageWriteMagickToFile(pstWand, pstMap->G_pcRamDir, pcNull, (char *)pcMotionNorm1, pstMap->G_pcPixFileExt, FALSE);
            if (tStatus == MagickPass) tStatus = grf_ImageWriteMagickToFile(pstWand, pstMap->G_pcRamDir, pcNull, (char *)pcMotion1, pstMap->G_pcPixFileExt, FALSE);
            //
            // This is the file we will return in the JSON object
            //
            GEN_SPRINTF(pstMap->G_pcLastFile, "%s%s.%s", pstMap->G_pcRamDir, pcMotionNorm1, pstMap->G_pcPixFileExt);
         }
         else
         {
            if (tStatus == MagickPass) tStatus = grf_ImageWriteMagickToFile(pstWand, pstMap->G_pcWwwDir, pstMap->G_pcPixPath, (char *)pcMotionNorm0, pstMap->G_pcPixFileExt, TRUE);
            if (tStatus == MagickPass) tStatus = grf_ImageWriteMagickToFile(pstWand, pstMap->G_pcRamDir, pcNull, (char *)pcMotionNorm0, pstMap->G_pcPixFileExt, FALSE);
            if (tStatus == MagickPass) tStatus = grf_ImageWriteMagickToFile(pstWand, pstMap->G_pcRamDir, pcNull, (char *)pcMotion0, pstMap->G_pcPixFileExt, FALSE);
            //
            // This is the file we will return in the JSON object
            //
            GEN_SPRINTF(pstMap->G_pcLastFile, "%s%s.%s", pstMap->G_pcRamDir, pcMotionNorm0, pstMap->G_pcPixFileExt);
         }
      }
      else
      {
         LOG_Report(0, "GRF", "GRF-MotionNormalize(): MagickSetImagePixels() error");
         PRINTF("GRF-MotionNormalize(): MagickSetImagePixels() error" CRLF);
         tStatus = MagickFail;
      }
   }
   //====================================================================================
   // Diagnose any error
   //====================================================================================
   if(tStatus != MagickPass)
   {
      char          *pcDescription;
      ExceptionType  tSeverity;

      GEN_SPRINTF(pstMap->G_pcLastFile, "%s", pcMotionError);
      FINFO_GetFileInfo(pstMap->G_pcLastFile, FI_SIZE, pstMap->G_pcLastFileSize, MAX_PATH_LEN);
      //
      pcDescription = MagickGetException(pstWand, &tSeverity);
      LOG_Report(0, "GRF", "GRF-MotionNormalize(): %s (Severity=%d)", pcDescription, tSeverity);
      PRINTF2("GRF-MotionNormalize(): %s (Severity=%d)" CRLF, pcDescription, tSeverity);
   }
   else
   {
      PRINTF("GRF-MotionNormalize():OKee completion" CRLF);
      fCc = TRUE;
   }
   DestroyMagickWand(pstWand);
   DestroyMagick();
   safefree(pcFile);
   safefree(pcImage);
   //
   return(fCc);
}

//
//  Function:   GRF_MotionDelta
//  Purpose:    Graphics motion calculate and build delta image
//
//  Parms:      
//  Returns:    Number of cells exceeding the motion threshold or -1 if error
//
int GRF_MotionDelta()
{
   int            iDelta=0;
   int            iHtOld=0, iWtOld=0, iHtNew=0, iWtNew=0;
   char          *pcFileOld; 
   char          *pcFileNew; 
   char          *pcImageOld; 
   char          *pcImageNew; 
   MagickPassFail tStatus=MagickPass;
   MagickWand    *pstWandOld;
   MagickWand    *pstWandNew;
   MDET          *pstMdet=&pstMap->G_stMotionDetect;

   PRINTF1("GRF-MotionDelta(): using format [%s]" CRLF, pstMdet->pcFormat);
   //
   // Initialize GraphicsMagick API
   //
   InitializeMagick(NULL);
   //
   // Allocate Wand handles
   //
   pstWandOld = NewMagickWand();
   pstWandNew = NewMagickWand();
   //
   pcFileOld = safemalloc(MAX_PATH_LEN);
   pcFileNew = safemalloc(MAX_PATH_LEN);
   //
   if(pstMdet->iSeq)
   {
      //====================================================================================
      // Old image is smotion_0_norm.ext
      // New image is smotion_1_norm.ext
      //====================================================================================
      GEN_SPRINTF(pcFileOld, "%s%s.%s", pstMap->G_pcRamDir, pcMotion0, pstMap->G_pcPixFileExt);
      GEN_SPRINTF(pcFileNew, "%s%s.%s", pstMap->G_pcRamDir, pcMotion1, pstMap->G_pcPixFileExt);
   }
   else
   {
      //====================================================================================
      // Old image is smotion_1_norm.ext
      // New image is smotion_0_norm.ext
      //====================================================================================
      GEN_SPRINTF(pcFileOld, "%s%s.%s", pstMap->G_pcRamDir, pcMotion1, pstMap->G_pcPixFileExt);
      GEN_SPRINTF(pcFileNew, "%s%s.%s", pstMap->G_pcRamDir, pcMotion0, pstMap->G_pcPixFileExt);
   }
   //
   if (tStatus == MagickPass) tStatus = MagickReadImage(pstWandOld, pcFileOld);
   if (tStatus == MagickPass) tStatus = MagickReadImage(pstWandNew, pcFileNew);
   //====================================================================================
   // Abort if both images have different sizes
   //====================================================================================
   if (tStatus == MagickPass)
   {
      iHtOld = MagickGetImageHeight(pstWandOld);
      iWtOld = MagickGetImageWidth(pstWandOld);
      iHtNew = MagickGetImageHeight(pstWandNew);
      iWtNew = MagickGetImageWidth(pstWandNew);
      //
      // Check if formats are non-zero and identical
      //
      if(iWtOld && iWtNew && iHtOld && iHtNew)
      {
         if( (iWtOld != iWtNew) || (iHtOld != iHtNew) )
         {
            // Nope: abort
            PRINTF( "GRF-MotionDelta():Bad Image(s)" CRLF);
            PRINTF3("GRF-MotionDelta():    Image(%s):w=%d, h=%d" CRLF, pcFileOld, iWtOld, iHtOld);
            PRINTF3("GRF-MotionDelta():    Image(%s):w=%d, h=%d" CRLF, pcFileNew, iWtNew, iHtNew);
            tStatus = MagickFail;
         }
      }
      else
      {
         tStatus = MagickFail;
      }
   }
   if (tStatus != MagickPass)
   {
      GEN_SPRINTF(pstMap->G_pcLastFile, "%s", pcMotionError);
      FINFO_GetFileInfo(pstMap->G_pcLastFile, FI_SIZE, pstMap->G_pcLastFileSize, MAX_PATH_LEN);
      LOG_Report(0, "GRF", "GRF-MotionDelta():BAD Image");
      //
      DestroyMagickWand(pstWandOld);
      DestroyMagickWand(pstWandNew);
      DestroyMagick();
      safefree(pcFileOld);
      safefree(pcFileNew);
      return(-1);
   }
   PRINTF3("GRF-MotionDelta():Old Image(%s) w=%d, h=%d" CRLF, pcFileOld, iWtOld, iHtOld);
   PRINTF3("GRF-MotionDelta():New Image(%s) w=%d, h=%d" CRLF, pcFileNew, iWtNew, iHtNew);
   //====================================================================================
   // FileOld --> WandOld --> ImageOld
   // FileNew --> WandNew --> ImageNew
   //====================================================================================
   pcImageOld = grf_MagickToImageBuffer(pstWandOld, pstMdet->pcFormat, 0, 0, iWtOld, iHtOld, NULL);
   pcImageNew = grf_MagickToImageBuffer(pstWandNew, pstMdet->pcFormat, 0, 0, iWtNew, iHtNew, NULL);
   //====================================================================================
   // Calculate number of cells exceeding the motion threshold between OLD and NEW images
   //====================================================================================
   iDelta = grf_HighlightGrids(pstWandNew, pcImageNew, pcImageOld, pstMdet, iWtOld, iHtOld);
   //====================================================================================
   // Create the DELTA image from Grayscale Converted to Black/White
   // ImageNew --> WandNew --> File 
   //
   // Write delta's image:
   //    - local RAM-fs b/w delta's
   //    - local RAM-fs highlighted delta's
   //    - public access www b/w delta's
   //    - public access www yellow marked delta's
   //
   //====================================================================================
   tStatus = grf_ImageWriteMagickToFile(pstWandNew, pstMap->G_pcWwwDir, pstMap->G_pcPixPath, (char *)pcMotionDeltas, pstMap->G_pcPixFileExt, TRUE);
   tStatus = grf_ImageWriteMagickToFile(pstWandNew, pstMap->G_pcRamDir, pcNull, (char *)pcMotionHlight, pstMap->G_pcPixFileExt, FALSE);
   //
   grf_CreateDeltaImage(pcImageNew, pcImageOld, pstMdet->iThldPixel, pstMdet->pcFormat, iWtOld, iHtOld, 0, 0, iWtOld, iHtOld);
   if( grf_ImageBufferToMagick(pstWandNew, pcImageNew, pstMdet->pcFormat, 0, 0, iWtNew, iHtNew) )
   {
      tStatus = grf_ImageWriteMagickToFile(pstWandNew, pstMap->G_pcWwwDir, pstMap->G_pcPixPath, (char *)pcMotionDeltas, pstMap->G_pcPixFileExt, TRUE);
      tStatus = grf_ImageWriteMagickToFile(pstWandNew, pstMap->G_pcRamDir, pcNull, (char *)pcMotionDeltas, pstMap->G_pcPixFileExt, FALSE);
   }
   else
   {
      LOG_Report(0, "GRF", "GRF-MotionDelta():MagickSetImagePixels() error");
      PRINTF("GRF-MotionDelta():MagickSetImagePixels() error" CRLF);
      tStatus = MagickFail;
   }
   //
   //====================================================================================
   // Diagnose any error
   //====================================================================================
   //
   if(tStatus != MagickPass)
   {
      char          *pcDescription;
      ExceptionType  tSeverity;

      GEN_SPRINTF(pstMap->G_pcLastFile, "%s", pcMotionError);
      FINFO_GetFileInfo(pstMap->G_pcLastFile, FI_SIZE, pstMap->G_pcLastFileSize, MAX_PATH_LEN);
      //
      pcDescription = MagickGetException(pstWandOld, &tSeverity);
      LOG_Report(0, "GRF", "GRF-MotionDelta():Old: %s (Severity=%d)", pcDescription, tSeverity);
      PRINTF2("GRF-MotionDelta(): Old: %s (Severity=%d)" CRLF, pcDescription, tSeverity);
      //
      pcDescription = MagickGetException(pstWandNew, &tSeverity);
      LOG_Report(0, "GRF", "GRF-MotionDelta():New : %s (Severity=%d)", pcDescription, tSeverity);
      PRINTF2("GRF-MotionDelta(): Old : %s (Severity=%d)" CRLF, pcDescription, tSeverity);
   }
   else
   {
      //
      // This is the file we will return in the JSON object
      //
      GEN_SPRINTF(pstMap->G_pcLastFile, "%s%s.%s", pstMap->G_pcRamDir, pcMotionHlight, pstMap->G_pcPixFileExt);
      PRINTF("GRF-MotionDelta():OKee completion" CRLF);
   }
   DestroyMagickWand(pstWandOld);
   DestroyMagickWand(pstWandNew);
   DestroyMagick();
   safefree(pcFileOld);
   safefree(pcFileNew);
   safefree(pcImageOld);
   safefree(pcImageNew);
   //
   return(iDelta);
}

//
//  Function:   GRF_MotionZoom
//  Purpose:    Zoom snapshot
//
//  Parms:      
//  Returns:    TRUE if OKee
//
bool GRF_MotionZoom()
{
   bool           fCc=FALSE;
   int            iWt, iHt;
   int            iXi, iYi, iHi, iWi;
   char          *pcFile; 
   char          *pcImage=NULL;
   MagickPassFail tStatus=MagickPass;
   MagickWand    *pstWand;
   MDET          *pstMdet=&pstMap->G_stMotionDetect;

   PRINTF2("GRF-MotionZoom():Using format[%s]:Seq=%d" CRLF, pstMdet->pcFormat, pstMdet->iSeq);
   //====================================================================================
   // Initialize GraphicsMagick API
   //====================================================================================
   InitializeMagick(NULL);
   //====================================================================================
   // Allocate Wand handles
   //====================================================================================
   pstWand   = NewMagickWand();
   pcFile    = safemalloc(MAX_PATH_LEN);
   //====================================================================================
   // image is motion_?.ext
   //====================================================================================
   if(pstMdet->iSeq) GEN_SPRINTF(pcFile, "%s%s.%s", pstMap->G_pcRamDir, pcMotion1, pstMap->G_pcPixFileExt);
   else              GEN_SPRINTF(pcFile, "%s%s.%s", pstMap->G_pcRamDir, pcMotion0, pstMap->G_pcPixFileExt);
   //====================================================================================
   // pix --> Wand
   //====================================================================================
   tStatus = MagickReadImage(pstWand, pcFile);
   //====================================================================================
   // Check if we need to scale the picture
   //====================================================================================
   if (tStatus == MagickPass)
   {
      iWt = MagickGetImageWidth(pstWand);
      iHt = MagickGetImageHeight(pstWand);
      //
      //   pstMdet->iZoomX = atoi(pstMap->G_pcDetectZoomX);
      //   pstMdet->iZoomY = atoi(pstMap->G_pcDetectZoomY);
      //   pstMdet->iZoomW = atoi(pstMap->G_pcDetectZoomW);
      //   pstMdet->iZoomH = atoi(pstMap->G_pcDetectZoomH);
      //
      iXi = pstMdet->iZoomX;
      iYi = pstMdet->iZoomY;
      iWi = pstMdet->iZoomW;
      iHi = pstMdet->iZoomH;
      //
      // if (tStatus == MagickPass) tStatus = MagickExtentImage(pstWand, iWt, iHt, 0, 0);
      //    not working ?  
      // if (tStatus == MagickPass) tStatus = MagickSetSize(pstWand, iWt, iHt);
      //    Lighter picture; zooming gives artifacts
      // if (tStatus == MagickPass) tStatus = MagickResizeImage(pstWand, (u_int32)iWt, (u_int32)iHt, LanczosFilter, 1.0);
      //    Darker picture; Zooming gives no artifacts
      //
      //                         unsigned int MagickResizeImage(MagickWand *wand, const unsigned long columns,
      //                                                        const unsigned long rows, const FilterTypes filter,
      //                                                        const double blur );
      //                         FilterTypes: LanczosFilter, ...
      //
      if (tStatus == MagickPass) tStatus = MagickCropImage(pstWand, (u_int32)iWi, (u_int32)iHi, (u_int32)iXi, (u_int32)iYi);
      if (tStatus == MagickPass) tStatus = MagickResizeImage(pstWand, (u_int32)iWt, (u_int32)iHt, LanczosFilter, 1.0);
      //====================================================================================
      // Write zoomed image back:
      //       - to public www including timestamp
      //       - to original *_zoom.*
      //       - overwrite original input
      //====================================================================================
      if(pstMdet->iSeq)  
      {
         if (tStatus == MagickPass) tStatus = grf_ImageWriteMagickToFile(pstWand, pstMap->G_pcWwwDir, pstMap->G_pcPixPath, (char *)pcMotionZoom1, pstMap->G_pcPixFileExt, TRUE);
         if (tStatus == MagickPass) tStatus = grf_ImageWriteMagickToFile(pstWand, pstMap->G_pcRamDir, pcNull, (char *)pcMotionZoom1, pstMap->G_pcPixFileExt, FALSE);
         if (tStatus == MagickPass) tStatus = grf_ImageWriteMagickToFile(pstWand, pstMap->G_pcRamDir, pcNull, (char *)pcMotion1, pstMap->G_pcPixFileExt, FALSE);
         //
         // This is the file we will return in the JSON object
         //
         GEN_SPRINTF(pstMap->G_pcLastFile, "%s%s.%s", pstMap->G_pcRamDir, pcMotionZoom1, pstMap->G_pcPixFileExt);
      }
      else
      {
         if (tStatus == MagickPass) tStatus = grf_ImageWriteMagickToFile(pstWand, pstMap->G_pcWwwDir, pstMap->G_pcPixPath, (char *)pcMotionZoom0, pstMap->G_pcPixFileExt, TRUE);
         if (tStatus == MagickPass) tStatus = grf_ImageWriteMagickToFile(pstWand, pstMap->G_pcRamDir, pcNull, (char *)pcMotionZoom0, pstMap->G_pcPixFileExt, FALSE);
         if (tStatus == MagickPass) tStatus = grf_ImageWriteMagickToFile(pstWand, pstMap->G_pcRamDir, pcNull, (char *)pcMotion0, pstMap->G_pcPixFileExt, FALSE);
         //
         // This is the file we will return in the JSON object
         //
         GEN_SPRINTF(pstMap->G_pcLastFile, "%s%s.%s", pstMap->G_pcRamDir, pcMotionZoom0, pstMap->G_pcPixFileExt);
      }
   }
   //====================================================================================
   // Diagnose any error
   //====================================================================================
   if(tStatus != MagickPass)
   {
      char          *pcDescription;
      ExceptionType  tSeverity;

      GEN_SPRINTF(pstMap->G_pcLastFile, "%s", pcMotionError);
      FINFO_GetFileInfo(pstMap->G_pcLastFile, FI_SIZE, pstMap->G_pcLastFileSize, MAX_PATH_LEN);
      //
      pcDescription = MagickGetException(pstWand, &tSeverity);
      LOG_Report(0, "GRF", "GRF-MotionZoom(): %s (Severity=%d)", pcDescription, tSeverity);
      PRINTF2("GRF-MotionZoom(): %s (Severity=%d)" CRLF, pcDescription, tSeverity);
   }
   else
   {
      PRINTF("GRF-MotionZoom():OKee completion" CRLF);
   }
   DestroyMagickWand(pstWand);
   DestroyMagick();
   safefree(pcFile);
   safefree(pcImage);
   return(fCc);
}


/* ======   Local Functions separator ===========================================
void ___local_functions(){} 
==============================================================================*/

//
//  Function:  grf_HighlightGrids
//  Purpose:   Calcutate the delta's for all grids and highlight the ones beyond the threshold.
//
//  Parms:     pstWand, pcImageNew, pcImageOld, pstMdet, iWi, iHi
//  Returns:   Number of grid-cells exceeding the threshold value   
//
static int grf_HighlightGrids(MagickWand *pstWand, char *pcImageNew, char *pcImageOld, MDET *pstMdet, int iWi, int iHi)
{
   int   iResult=0, iDelta, iX, iY;
   int   iXs, iXe, iYs, iYe;

   //
   // Setup grid-element coords: 
   //
   //       iXs         iXe
   //  iYs  +-----------+
   //       |           |
   //       |           |
   //       |           |
   //       |           |
   //  iYe  +-----------+
   //
   //

   //
   for(iY=0; iY<MOTION_GRID_COUNT; iY++)
   {
      iYs = ((iY+0)*iHi)/MOTION_GRID_COUNT;
      iYe = ((iY+1)*iHi)/MOTION_GRID_COUNT;
      //
      for(iX=0; iX<MOTION_GRID_COUNT; iX++)
      {
         iXs = ((iX+0)*iWi)/MOTION_GRID_COUNT;
         iXe = ((iX+1)*iWi)/MOTION_GRID_COUNT;
         //
         // o Divide whole picture into separate grids, default = 10*10 grids
         // o Calculate percentage of pixels that deviate above threshold (iThldPixel) within each grid
         // o Scale each value according to the grid-scales (GridScales 0..100%)
         // o If deviation exceeds level (iThldLevel): mark grid with yellow paint
         //
         iDelta  = grf_ImageGetDelta(pcImageNew, pcImageOld, pstMdet->iThldPixel, pstMdet->pcFormat, iWi, iHi, iXs, iYs, iXe, iYe);
         iDelta *= (int)pstMdet->usGridScales[iY][iX];
         iDelta  = (iDelta + 50)/100;
         //
         pstMdet->ulGridMotion[iY][iX] = (u_int32) abs(iDelta);
         if(iDelta > pstMdet->iThldLevel)
         {
            iResult++;
            grf_ImageDrawLine(pstWand, "yellow", iXs, iYs, iXe, iYs);
            grf_ImageDrawLine(pstWand, "yellow", iXe, iYs, iXe, iYe);
            grf_ImageDrawLine(pstWand, "yellow", iXe, iYe, iXs, iYe);
            grf_ImageDrawLine(pstWand, "yellow", iXs, iYe, iXs, iYs);
            //
            // Or block (fill color/opacity ?)
            // grf_ImageDrawRect(pstWand, "yellow", iXs, iYs, iXe, iYe);
            //
            PRINTF4("grf-HighlightGrids():Block[%d,%d]=%2d,%d%%" CRLF, iX, iY, iDelta/10, iDelta%10);
         }
      }
   }
   return(iResult);
}

//
//  Function:   grf_ImageDrawLine
//  Purpose:    Draw line onto imagewand
//
//  Parms:      Wand, linecolor, start w/h, end w/h
//  Returns:    
//
static void grf_ImageDrawLine(MagickWand *pstWand, const char *pcColor, int iXs, int iYs, int iXe, int iYe)
{
   DrawingWand   *pstDraw;
   PixelWand     *pstColor;
   
   pstDraw  = NewDrawingWand();
   pstColor = NewPixelWand();
   //
   PixelSetColor(pstColor, pcColor);
   DrawSetStrokeColor(pstDraw, pstColor);
   DrawSetStrokeOpacity(pstDraw, 1.0);
   DrawLine(pstDraw, (double)iXs, (double)iYs, (double)iXe, (double)iYe);
   MagickDrawImage(pstWand, pstDraw);
   //
   DestroyPixelWand(pstColor);
   DestroyDrawingWand(pstDraw);
}

#ifdef TEST_COORD_CORNERS
static void             grf_ImageDrawPoint         (MagickWand *, const char *, int, int);
//
//  Function:   grf_ImageDrawPoint
//  Purpose:    Draw pixel onto imagewand
//
//  Parms:      Wand, linecolor, X, Y
//  Returns:    
//
static void grf_ImageDrawPoint(MagickWand *pstWand, const char *pcColor, int iX, int iY)
{
   DrawingWand   *pstDraw;
   PixelWand     *pstColor;
   
   pstDraw  = NewDrawingWand();
   pstColor = NewPixelWand();
   //
   PixelSetColor(pstColor, pcColor);
   DrawSetStrokeColor(pstDraw, pstColor);
   DrawSetStrokeOpacity(pstDraw, 1.0);
   DrawPoint(pstDraw, (double)iX, (double)iY);
   MagickDrawImage(pstWand, pstDraw);
   //
   DestroyPixelWand(pstColor);
   DestroyDrawingWand(pstDraw);
}
#endif   //TEST_COORD_CORNERS

#ifdef USE_DRAW_RECT
//
//  Function:   grf_ImageDrawRect
//  Purpose:    Draw rectangle onto imagewand
//
//  Parms:      Wand, linecolor, start w/h, end w/h
//  Returns:    
//
static void grf_ImageDrawRect(MagickWand *pstWand, const char *pcColor, int iXs, int iYs, int iXe, int iYe)
{
   DrawingWand   *pstDraw;
   PixelWand     *pstColor;
   
   pstDraw  = NewDrawingWand();
   pstColor = NewPixelWand();
   //
   PixelSetColor(pstColor, pcColor);
   DrawSetStrokeColor(pstDraw, pstColor);
   DrawSetStrokeOpacity(pstDraw, 0.5);
   //
   PixelSetColor(pstColor, "white");
   DrawSetFillColor(pstDraw, pstColor);
   DrawSetFillOpacity(pstDraw, 0.5);
   //DrawLine(pstDraw, (double)iXs, (double)iYs, (double)iXe, (double)iYe);
   DrawRectangle(pstDraw, (double)iXs, (double)iYs, (double)iXe, (double)iYe);
   MagickDrawImage(pstWand, pstDraw);
   //
   DestroyPixelWand(pstColor);
   DestroyDrawingWand(pstDraw);
}

//
//  Function:   grf_ImageDrawText
//  Purpose:    Draw text onto imagewand
//
//  Parms:      Wand, linecolor, text, start w/h
//  Returns:    
//
static void grf_ImageDrawText(MagickWand *pstWand, const char *pcColor, char *pcText, int iXs, int iYs)
{
   DrawingWand   *pstDraw;
   PixelWand     *pstColor;
   
   pstDraw  = NewDrawingWand();
   pstColor = NewPixelWand();
   //
   PixelSetColor(pstColor, pcColor);
   DrawSetStrokeColor(pstDraw, pstColor);
   //
   MagickAnnotateImage(pstWand, pstDraw, (double)iXs, (double)iYs, 0.0, pcText);
   MagickDrawImage(pstWand, pstDraw);
   //
   DestroyPixelWand(pstColor);
   DestroyDrawingWand(pstDraw);
}
#endif   //USE_DRAW_RECT

//
//  Function:   grf_ImageGetDelta
//  Purpose:    Calculate the delta between two images
//
//  Parms:      Image Dest, Image Src, Threshold, color format("RGB"), Total w/h, start w/h, end w/h
//  Returns:    Delta 0-100.0%   
//
static int grf_ImageGetDelta(char *pcImgDest, char *pcImgSrc, int iThreshold, char *pcFormat, int iWt, int iHt, int iWs, int iHs, int iWe, int iHe)
{
   int            i, iTemp, iDelta=0;
   int            iPixSrc, iPixDest;
   int            iImageSize=0, iX=0, iY=0;
   int            iFormatLength;
   char          *pcSrc, *pcDest; 

   iFormatLength = GEN_STRLEN(pcFormat);
   //
   for(iY=iHs; iY<iHe; iY++)
   {
      pcSrc  = &pcImgSrc[iFormatLength*((iY*iWt)+iWs)];
      pcDest = &pcImgDest[iFormatLength*((iY*iWt)+iWs)];
      //
      for(iX=iWs; iX<iWe; iX++)
      {
         for(i=0; i<iFormatLength; i++)
         {
            iPixSrc  = (int) *pcSrc++;
            iPixDest = (int) *pcDest++;
            iTemp    = abs(iPixSrc - iPixDest);
            if(iTemp > iThreshold)
            {
               //
               // Significant change in grayscale pixel value : count it
               //
               iDelta++;
               //PRINTF3("grf-ImageGetDelta(): Delta=%5d (%3d=%3d)" CRLF, iDelta, iPixSrc, iPixDest);
            }
            else
            {
               //
               // Grayscale pixels are almost identical: clear them
               //
            }
            iImageSize++;
         }
      }
   }
   iTemp = (iDelta*1000)/iImageSize;
   //PRINTF6("grf-ImageGetDelta(): Image[%3d,%3d]-[%3d,%3d]:%3d,%d%%" CRLF, iWs, iHs, iWe, iHe, iTemp/10, iTemp%10);
   return(iTemp);
}

//
//  Function:  grf_CreateDeltaImage
//  Purpose:   Calculate the delta between two images and create a new b/w image containing the delta's
//
//  Parms:     Image Dest, Image Src, Threshold, Color format ("RGB"), Total w/h, start w/h, end w/h
//  Returns:   Delta 0-100.0% 
//  Note:      iThreshold comes from the PiCal app (MotionPix) and dictates the difference in pixel-values
//
static int grf_CreateDeltaImage(char *pcImgDest, char *pcImgSrc, int iThreshold, char *pcFormat, int iWt, int iHt, int iWs, int iHs, int iWe, int iHe)
{
   int            i, iTemp, iDelta=0;
   int            iPixSrc, iPixDest;
   int            iImageSize=0, iX=0, iY=0;
   int            iFormatLength;
   char          *pcSrc, *pcDest; 

   iFormatLength = GEN_STRLEN(pcFormat);
   //
   for(iY=iHs; iY<iHe; iY++)
   {
      pcSrc  = &pcImgSrc[iFormatLength*((iY*iWt)+iWs)];
      pcDest = &pcImgDest[iFormatLength*((iY*iWt)+iWs)];
      //
      for(iX=iWs; iX<iWe; iX++)
      {
         for(i=0; i<iFormatLength; i++)
         {
            iPixSrc  = (int) *pcSrc;
            iPixDest = (int) *pcDest;
            iTemp    = abs(iPixSrc - iPixDest);
            if(iTemp > iThreshold)
            {
               //
               // Significant change in grayscale pixel value : count it
               //
               iDelta++;
               *pcDest = 255;
               //PRINTF3("grf-CreateDeltaImage(): Delta=%5d (%3d=%3d)" CRLF, iDelta, iPixSrc, iPixDest);
            }
            else
            {
               //
               // Grayscale pixels are almost identical: clear them
               //
               *pcDest = 0;
            }
            pcSrc++;
            pcDest++;
            iImageSize++;
         }
      }
   }
   iTemp = (iDelta*1000)/iImageSize;
   //PRINTF6("grf-CreateDeltaImage(): Image[%3d,%3d]-[%3d,%3d]:%3d,%d%%" CRLF, iWs, iHs, iWe, iHe, iTemp/10, iTemp%10);
   return(iTemp);
}

//
//  Function:   grf_ImageBufferNormalize
//  Purpose:    Normalize an image: find MAX and recalculate all to MAX
//
//  Parms:      Image, MDET struct, image sizes
//  Returns:    New average
//
static int grf_ImageBufferNormalize(char *pcImage, MDET *pstMdet, int iWt, int iHt, int iWs, int iHs, int iWe, int iHe)
{
   int         i, iMin=255, iMax=0;
   int         iPix, iAvg=0, iImageSize=0;
   int         iX, iY;
   int         iFormatLength;
   long        lSum=0;
   char       *pcSrc;
   
   iFormatLength = GEN_STRLEN(pstMdet->pcFormat);
   //
   for(iY=iHs; iY<iHe; iY++)
   {
      pcSrc = &pcImage[iFormatLength*((iY*iWt)+iWs)];
      //
      for(iX=iWs; iX<iWe; iX++)
      {
         for(i=0; i<iFormatLength; i++)
         {
            iPix  = (int) *pcSrc++;
            // Keep track of min-max
            if(iPix > iMax) iMax = iPix;
            if(iPix < iMin) iMin = iPix;
         }
      }
   }
   //
   // Recalculate all value to normalize(max)
   //
   for(iY=iHs; iY<iHe; iY++)
   {
      pcSrc = &pcImage[iFormatLength*((iY*iWt)+iWs)];
      //
      for(iX=iWs; iX<iWe; iX++)
      {
         for(i=0; i<iFormatLength; i++)
         {
            iPix        = (int)*pcSrc;
            iPix       -= iMin;
            iPix        = (iPix * 255) / (iMax-iMin);
            lSum       += (long) iPix;
            *pcSrc++    = (char) iPix;
            iImageSize++;
         }
      }
   }
   if(iImageSize) iAvg = (int)(lSum/iImageSize);
   PRINTF4("grf-ImageBufferNormalize(): Min=%d, Max=%d, Avg=%d, Thld=%d" CRLF, iMin, iMax, iAvg, pstMdet->iThldPixel);
   return(iAvg);
}

//
//  Function:   grf_ImageBufferGrayscale
//  Purpose:    Transfer selected pixels into grayscale
//
//  Parms:      Pixelbuffer, MDET struct, total w/h, start w/h, end w/h
//  Returns:    1=OKee
//
static int grf_ImageBufferGrayscale(char *pcImage, MDET *pstMdet, int iWt, int iHt, int iWs, int iHs, int iWe, int iHe)
{
   int            i;
   int            iX, iY, iGray, iAverageSum;
   int            iRd, iGn, iBl;
   int            iFormatLength;
   char          *pcSrc, *pcDest; 

   iFormatLength = GEN_STRLEN(pstMdet->pcFormat);
   //
   // Keep end in range of picture !
   //
   if(iHe > iHt) iHe = iHt;
   if(iWe > iWt) iWe = iWt;
   //
   iRd = pstMdet->iThldRed;
   iGn = pstMdet->iThldGreen;
   iBl = pstMdet->iThldBlue;
   //
   for(iY=iHs; iY<iHe; iY++)
   {
      pcSrc  = &pcImage[iFormatLength*((iY*iWt)+iWs)];
      pcDest = &pcImage[iFormatLength*((iY*iWt)+iWs)];
      //
      for(iX=iWs; iX<iWe; iX++)
      {
         iGray       = 0;
         iAverageSum = 0;
         //
         for(i=0; i<iFormatLength; i++)
         {
            switch(pstMdet->pcFormat[i])
            {
               case 'R':
                  iGray       += (iRd * (*pcSrc++));
                  iAverageSum += iRd;
                  break;

               case 'G':
                  iGray       += (iGn * (*pcSrc++));
                  iAverageSum += iGn;
                  break;

               case 'B':
                  iGray       += (iBl * (*pcSrc++));
                  iAverageSum += iBl;
                  break;

               default:
               //case 'Y':
               //case 'U':
               //case 'V':
               //case 'I':
                  iGray       += (1000 * (*pcSrc++));
                  iAverageSum += 1000;
                  break;

            }
         }
         //
         // Scale back the average
         //
         iGray /= iAverageSum;
         //
         // Store the result
         //
         for(i=0; i<iFormatLength; i++)
         {
            *pcDest++ = (char) iGray;
         }
         //
         // Display result :
         //   pix, threshold, mode (0=CRLF, 1=value)
         //
         //GRF_DUMP_RGB_LEVELS(pstMdet, iGray, 100, 1);
         GRF_DUMP_RGB_LEVELS(pstMdet, iGray, 0, 1);
      }
      GRF_DUMP_RGB_LEVELS(pstMdet, 0,0,0);
   }
   return(1);
}

//
// Function:   grf_MagickToImageBuffer
// Purpose:    Transfer selected pixels from Magick cache into a new buffer
//
// Parms:      Wand-struct, color format ("RGB"), coordinates start/end, result buffer size
// Returns:    Buffer^ or NULL
// Note:       piSize holds the actual size of the allocated image buffer
//
static char *grf_MagickToImageBuffer(MagickWand *pstMagickWand, char *pcFormat, int iWs, int iHs, int iWe, int iHe, int *piSize)
{
   int   iTotalSize;
   int   iFormatLength;
   char *pcImage; 

   iFormatLength = GEN_STRLEN(pcFormat);
   //
   iTotalSize = (iWe-iWs) * (iHe-iHs) * iFormatLength * sizeof(char);
   pcImage    = safemalloc(iTotalSize);
   //
   if(piSize) *piSize = iTotalSize;
   //
   // Read image RGB pixels
   //
   if( MagickGetImagePixels(pstMagickWand, iWs, iHs, iWe, iHe, pcFormat, CharPixel, (void *)pcImage) == 0)
   {
      LOG_Report(0, "GRF", "grf-MagickToImageBuffer():MagickGetImagePixels() error");
      PRINTF("grf-MagickToImageBuffer():MagickGetImagePixels() error" CRLF);
   }
   return(pcImage);
}

//
//  Function:   grf_ImageBufferToMagick
//  Purpose:    Transfer selected pixels from pixel buffer to Magick cache
//
//  Parms:      Wand-struct, Pixel buffer, color format {"RGB"}, coords
//  Returns:    1=OKee
//
static int grf_ImageBufferToMagick(MagickWand *pstMagickWand, char *pcPixels, char *pcFormat, int iWs, int iHs, int iWe, int iHe)
{
   int   iCc;

   iCc = MagickSetImagePixels(pstMagickWand, iWs, iHs, iWe, iHe, pcFormat, CharPixel, (void *)pcPixels);
   return(iCc);
}

//
//  Function:   grf_ImageWriteMagickToFile
//  Purpose:    Write the Magick buffer to file
//
//  Parms:      Wand-struct, Main Dir, Sub Dir, Filename, timestamp yesno
//  Returns:    Pass/Fail
//
static MagickPassFail grf_ImageWriteMagickToFile(MagickWand *pstWand, char *pcDir, char *pcSubDir, char *pcName, char *pcExt, bool bTimestamp)
{
   MagickPassFail tStatus;
   char          *pcTimeStp;

   if(bTimestamp)
   {
      pcTimeStp = safemalloc(MAX_PATH_LEN);
      RTC_ConvertDateTime(TIME_FORMAT_YYYY_MM_DD_HH_MM_SS, pcTimeStp, RTC_GetSystemSecs());
      GEN_SNPRINTF(pstMap->G_pcLastFile, MAX_PATH_LEN, "%s%s%s_%s.%s", pcDir, pcSubDir, pcTimeStp, pcName, pcExt);
      safefree(pcTimeStp);
   }
   else
   {
      GEN_SNPRINTF(pstMap->G_pcLastFile, MAX_PATH_LEN, "%s%s%s.%s", pcDir, pcSubDir, pcName, pcExt);
   }
   PRINTF1("grf-ImageWriteMagickToFile():Save Image to [%s]" CRLF, pstMap->G_pcLastFile);
   tStatus = MagickWriteImage(pstWand, pstMap->G_pcLastFile);
   //
   // Pass correct filesize
   //
   if(tStatus == MagickPass) FINFO_GetFileInfo(pstMap->G_pcLastFile, FI_SIZE, pstMap->G_pcLastFileSize, MAX_PATH_LEN);
   else                      GEN_STRNCPY(pstMap->G_pcLastFileSize, "0", MAX_PATH_LEN);
   //
   return(tStatus);
}

//
//  Function:  grf_ImageWriteTft
//  Purpose:   Convert and write RGB pixels to TFT screen
//
//  Parms:     RGB Buffer, widt, heigth, number of subpixels (RGB, I)
//  Returns:    
//
static void grf_ImageWriteTft(char *pcImage, int iWi, int iHi, int iRgb)
{
#ifdef FEATURE_WRITE_TFT
   int      iW, iWiS, iH, iHiS;
   int      iR, iG, iB;
   int      iRow, iCol;
   int      iTftRow, iTftCol;
   u_int16  usImage;
   u_int8   ubR, ubG, ubB;
   char    *pcTemp;
   char    *pcPix;
   u_int16 *pusImage;
   u_int16 *pusTemp;
   u_int16 *pusImageEnd;

   PRINTF3("grf-ImageWriteTft(): Pix w=%d h=%d (%d bpp) " CRLF, iWi, iHi, iRgb);
   //
   // Allocate memory for TFT image
   //
   pusImage = (u_int16 *) safemalloc(TFT_ACTUAL_ROWS * TFT_ACTUAL_COLS * sizeof(u_int16));
   pusTemp  = pusImage;
   //
   pusImageEnd  = &pusImage[TFT_ACTUAL_ROWS * TFT_ACTUAL_COLS];
   PRINTF2("grf-ImageWriteTft(): Tft Start=%p End=%p" CRLF, pusTemp, pusImageEnd);
   //
   GEN_MEMSET(pusImage, 0, TFT_ACTUAL_ROWS * TFT_ACTUAL_COLS * sizeof(u_int16));
   //
   // Check if we need to scale the picture down or not
   //
   iWiS = iWi/TFT_ACTUAL_COLS;
   iHiS = iHi/TFT_ACTUAL_ROWS;
   //
   // If remainder: always round up !
   //
   if(iWi%TFT_ACTUAL_COLS) iWiS++;
   if(iHi%TFT_ACTUAL_ROWS) iHiS++;
   //
   // Scale down: we need to average the RGB values
   //
   PRINTF2("grf-ImageWriteTft(): TFT Scale WiS=%d HiS=%d" CRLF, iWiS, iHiS);
   //
   // Center scaled TFT screen
   //
   iTftRow = (TFT_ACTUAL_ROWS-(iHi/iHiS))/2;
   iTftCol = (TFT_ACTUAL_COLS-(iWi/iWiS))/2;
   PRINTF2("grf-ImageWriteTft(): TFT Center row=%d col=%d" CRLF, iTftRow, iTftCol);
   //
   for(iRow=0; iRow<iHi-iHiS; iRow+=iHiS)
   {
      pusTemp = &pusImage[(iTftRow*TFT_ACTUAL_COLS)+iTftCol];
      for(iCol=0; iCol<iWi-iWiS; iCol+=iWiS)
      {
         pcTemp = &pcImage[((iRow*iWi) + iCol) * iRgb];
         iR = iG = iB = 0;
         for(iH=0; iH<iHiS; iH++)
         {
            pcPix = &pcTemp[(iH*iWi)*iRgb];
            for(iW=0; iW<iWiS; iW++)
            {
               switch(iRgb)
               {
                  default:
                     // Unknown profile
                     break;

                  case 1:
                     // "I"
                     iR += *pcPix;
                     iG += *pcPix;
                     iB += *pcPix++;
                     break;

                  case 3:
                     // "RGB"
                     iR += *pcPix++;
                     iG += *pcPix++;
                     iB += *pcPix++;
                     break;
               }
            }
         }
         //
         // Convert each 24 bits RGB value into a 16 bits RGB value:
         //
         // +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
         // |B|B|B|B|B|B|B|B|G|G|G|G|G|G|G|G|R|R|R|R|R|R|R|R|
         // +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
         //
         // +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
         // |R|R|R|R|R|G|G|G|G|G|G|B|B|B|B|B|
         // +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
         //
         ubR = (u_int8)(iR/(iHiS * iWiS));
         ubG = (u_int8)(iG/(iHiS * iWiS));
         ubB = (u_int8)(iB/(iHiS * iWiS));
         //
         usImage = (ubR >> 3);
         usImage = usImage << 6;
         usImage = usImage | (ubG >> 2);
         usImage = usImage << 5;
         usImage = usImage | (ubB >> 3);     
         //
         *pusTemp++ = usImage;
         if(pusTemp > pusImageEnd)
         {
            PRINTF5("grf-ImageWriteTft(): pusImage overflow ! row=%d col=%d tft=%d: %p %p" CRLF, iRow, iCol, iTftRow, pusTemp, pusImageEnd);
            pusTemp--;
         }
      }
      if(++iTftRow>=TFT_ACTUAL_ROWS)
      {
         PRINTF3("grf-ImageWriteTft(): Tft row overflow! tft=%d: %p %p" CRLF, iTftRow, pusTemp, pusImageEnd);
         iTftRow--;
      }
   }
   PRINTF4("grf-ImageWriteTft(): Tft filled (%d rows): Sta=%p Cur=%p End=%p" CRLF, iTftRow+1, pusImage, pusTemp, pusImageEnd);
   grf_DrawPicture(pusImage, (u_int32)(TFT_ACTUAL_ROWS * TFT_ACTUAL_COLS));
   PRINTF("grf-ImageWriteTft(): Called TFT_DrawPicture()" CRLF);
   safefree(pusImage);
#endif   //FEATURE_WRITE_TFT
}

#ifdef FEATURE_WRITE_TFT
//
//  Function:   grf_DrawPicture
//  Purpose:    Write the 5-6-5-RGB image to the framebuffer
//
//  Parms:      
//  Returns:    
//
static void grf_DrawPicture(u_int16 *pusImage, u_int32 ulSize)
{
}
#endif   //FEATURE_WRITE_TFT

#ifdef FEATURE_DUMP_RGB_LEVELS
//
//  Function:  grf_DumpRgbLevels
//  Purpose:   Dump RGB levels to stdout
//
//  Parms:      
//  Returns:
//  Note:      Enable dump using pstMdet->iDebug
//
static void grf_DumpRgbLevels(MDET *pstMdet, int iGray, int iThres, int iMode)
{
   if(pstMdet->iDebug & MM_DEBUG_FILE_RGB)
   {
      switch(iMode)
      {
         case 0:
            //
            // One line done
            //
            GEN_Printf(CRLF);
            break;

         default:
         case 1:
            //
            // Busy assembling one line with either bcd values or ascii
            //
            if(iThres)
            {
               //
               // Dump as ascii
               //
               if(iGray > iThres) GEN_Printf("*");
               else               GEN_Printf(" ");
            }
            else
            {
               //
               // Dump raw level data
               //
               GEN_Printf("%03d ", iGray);
            }
            break;
      }
   }
}
#endif   //FEATURE_DUMP_RGB_LEVELS


/* ======   Local Functions separator ===========================================
void ___example_functions(){} 
==============================================================================*/

//
// Trials:
//

// int            iCc=0;
// double         dParm;
// Image         *pstImage1;
// Image         *pstImage2;
// ImageInfo     *pstImageInfo1;
// ImageInfo     *pstImageInfo2;
// ExceptionInfo  stException;
// 
// GEN_Printf("GRF-Init():Magick on (%s, %d)..." CRLF, pcFoto, iParm);
// dParm = (double)iParm;
// dParm /= 1000;
// //
// GetExceptionInfo(&stException);
// InitializeMagick(NULL);
// pstImageInfo1 = CloneImageInfo((ImageInfo *) NULL);
// GEN_STRCPY(pstImageInfo1->filename, pcFoto);
// pstImage1 = ReadImage(pstImageInfo1, &stException);
// //
// if (stException.severity != UndefinedException)
// {
//    GEN_Printf("GRF-Init():ReadImage(%s) ERROR %s:%s" CRLF, pstImageInfo1->filename, stException.reason, stException.description);
//    DestroyImageInfo(pstImageInfo1);
//    CatchException(&stException);
// }
// else
// {
//    GEN_Printf("GRF-Init():ReadImage(%s) OK: r=%d, c=%d (%s)" CRLF, pstImage1->filename, pstImage1->rows, pstImage1->columns, pstImage1->magick);
//    pstImageInfo2 = CloneImageInfo((ImageInfo *) NULL);
//    //pwjh pstImageInfo2 = CloneImageInfo(pstImageInfo1);
//    DestroyImageInfo(pstImageInfo1);
//    //
//    // Do something with the pix
//    //
//    pstImage2 = EmbossImage(pstImage1, (double)0, dParm, &stException );
//    //
//    GEN_SPRINTF(pstImageInfo2->filename, "%s_%s", pcFoto, "new.bmp");
//    SetImageInfo(pstImageInfo2, 0, &stException);
//    iCc = WriteImage(pstImageInfo2, pstImage2);
//    if(iCc)  GEN_Printf("GRF-Init():WriteImage(%s) OK" CRLF,    pstImageInfo2->filename);
//    else     GEN_Printf("GRF-Init():WriteImage(%s) ERROR" CRLF, pstImageInfo2->filename);
//    DestroyImageInfo(pstImageInfo2);
// }
// DestroyExceptionInfo(&stException);
// DestroyMagick();

   // Dump RGB levels of each pixel
   //
   // if(iParm < 175)
   // {
   //    // R
   //    for(iX=iParm; iX<iParm+25; iX++)
   //    {
   //       GEN_Printf("%03d ", pcPixels[(iY*200*3)+(iX+0)]);
   //    }
   //    GEN_Printf(CRLF);
   //    // G
   //    for(iX=iParm; iX<iParm+25; iX++)
   //    {
   //       GEN_Printf("%03d ", pcPixels[(iY*200*3)+(iX+1)]);
   //    }
   //    // B
   //    GEN_Printf(CRLF);
   //    for(iX=iParm; iX<iParm+25; iX++)
   //    {
   //       GEN_Printf("%03d ", pcPixels[(iY*200*3)+(iX+2)]);
   //    }
   //    GEN_Printf(CRLF);
   // }
   // else
   // {
   //    GEN_Printf("GRF-Filter():MagickGetImagePixels():Column too large" CRLF);
   // }

   //
   // Threshold image 
   //
   // if (tStatus == MagickPass)
   // {
   //    PixelWand *pstColor;
   //    pstColor=NewPixelWand();
   //    GEN_Printf("GRF-Filter():Filter image (%s)..." CRLF, pcFile);
   //    //PixelSetColor(pstColor, "#ffffff");
   //    PixelSetColorCount(pstColor, iParm);
   //    MagickBlackThresholdImage(pstMagickWand, pstColor);
   //    DestroyPixelWand(pstColor);
   // }

   //
   // Rotate image clockwise 30 degrees with black background
   // if (tStatus == MagickPass)
   // {
   //    PixelWand *pstBackground;
   //    GEN_Printf("GRF-Filter():Rotate image (%s)..." CRLF, pcFile);
   //    pstBackground=NewPixelWand();
   //    PixelSetColor(pstBackground, "#000000");
   //    tStatus = MagickRotateImage(pstMagickWand, pstBackground, iParm);
   //    DestroyPixelWand(pstBackground);
   // }

   // MagickPassFail tCc;
   // tCc = ImageToFile(pstImage, pcFile, &stException);
   // switch(tCc)
   // {
   //    case MagickPass:
   //       LOG_Report(0, "GRF", "Magick test edit OK : %s", pcFile);
   //       break;
   //
   //    case MagickFail:
   //       LOG_Report(0, "GRF", "Magick test edit BAD: %s", pcFile);
   //       break;
   //
   //    default:
   //       LOG_Report(0, "GRF", "Magick test edit UNKNOWN: %s", pcFile);
   //       break;
   // }


   // double         dAvg, dStDev;
   //
   // //====================================================================================
   // // WandOld : Calculate Avg, StDev
   // //====================================================================================
   // MagickGetImageChannelMean(pstWandOld, RedChannel, &dAvg, &dStDev);
   // PRINTF2("GRF-MotionDetect(): OLD:Original  RED: Avg=%5.2f, StDev=%5.2f" CRLF, dAvg, dStDev);
   // //
   // MagickGetImageChannelMean(pstWandOld, GreenChannel, &dAvg, &dStDev);
   // PRINTF2("GRF-MotionDetect(): OLD:Original  GRN: Avg=%5.2f, StDev=%5.2f" CRLF, dAvg, dStDev);
   // //
   // MagickGetImageChannelMean(pstWandOld, BlueChannel, &dAvg, &dStDev);
   // PRINTF2("GRF-MotionDetect(): OLD:Original  BLU: Avg=%5.2f, StDev=%5.2f" CRLF, dAvg, dStDev);
   // //====================================================================================
   // // WandNew : Calculate Avg, StDev
   // //====================================================================================
   // MagickGetImageChannelMean(pstWandNew, RedChannel, &dAvg, &dStDev);
   // PRINTF2("GRF-MotionDetect(): NEW:Original  RED: Avg=%5.2f, StDev=%5.2f" CRLF, dAvg, dStDev);
   // //
   // MagickGetImageChannelMean(pstWandNew, GreenChannel, &dAvg, &dStDev);
   // PRINTF2("GRF-MotionDetect(): NEW:Original  GRN: Avg=%5.2f, StDev=%5.2f" CRLF, dAvg, dStDev);
   // //
   // MagickGetImageChannelMean(pstWandNew, BlueChannel, &dAvg, &dStDev);
   // PRINTF2("GRF-MotionDetect(): NEW:Original  BLU: Avg=%5.2f, StDev=%5.2f" CRLF, dAvg, dStDev);

   // //====================================================================================
   // // Draw result into image
   // //
   // //====================================================================================
   // {
   //    DrawingWand *pstDraw;
   //    Image      *pstImage;
   // 
   //    pstImage = AllocateImage(NULL);
   //    pstDraw  = DrawAllocateWand(NULL, pstImage);
   // 
   //    DrawAnnotation(pstDraw, 0, 0, "Peter Hillen");
   // }

#ifdef COMMENT
//
//  Function:   grf_ImageBufferNormalize
//  Purpose:    Normalize an image: find MAX and recalculate all to MAX
//
//  Parms:      Image, Image size
//  Returns:    New average
//
static int grf_ImageBufferNormalize(char *pcImage, int iImageSize)
{
   int   i, iMin=255, iMax=0;
   int   iPix, iAvg;
   long  lSum=0;
   char *pcImage2 = pcImage;
   
   for(i=0; i<iImageSize; i++)
   {
      iPix  = (int) *pcImage2++;
      // Keep track of min-max
      if(iPix > iMax) iMax = iPix;
      if(iPix < iMin) iMin = iPix;
   }
   //
   // Recalculate all value to normalize(max)
   //
   for(i=0; i<iImageSize; i++)
   {
      //iPix        = ((int)*pcImage * 255)/ iMax;
      //*pcImage++  = (char) iPix;
      //
      iPix        = (int)*pcImage;
      iPix       -= iMin;
      iPix        = (iPix * 255) / (iMax-iMin);
      lSum       += (long) iPix;
      *pcImage++  = (char) iPix;
   }
   iAvg = (int)(lSum/iImageSize);
   PRINTF3("grf-ImageBufferNormalize(): Min=%d, Max=%d, Avg=%d" CRLF, iMin, iMax, iAvg);
   return(iAvg);
}

//
//  Function:   grf_ImageDelta
//  Purpose:    Calculate the delta between two images
//
//  Parms:      Image Dest, Image Src, Image size, threshold
//  Returns:    Delta 0-100.0%   
//
static int grf_ImageDelta(char *pcImgDest, char *pcImgSrc, int iImageSize, int iThreshold)
{
   int   i, iTemp, iDelta=0;
   int   iPixSrc, iPixDest;
   
   for(i=0; i<iImageSize; i++)
   {
      iPixSrc  = (int) *pcImgSrc;
      iPixDest = (int) *pcImgDest;
      iTemp    = abs(iPixSrc - iPixDest);
      if(iTemp > iThreshold)
      {
         //
         // Significant change in grayscale pixel value : count it
         //
         iDelta++;
         *pcImgDest = 255;
         //PRINTF3("grf-ImageDelta(): Delta=%5d (%3d=%3d)" CRLF, iDelta, iPixSrc, iPixDest);
      }
      else
      {
         //
         // Grayscale pixels are almost identical: clear them
         //
         *pcImgDest = 0;
      }
      pcImgSrc++;
      pcImgDest++;
   }
   iTemp = (iDelta*1000)/iImageSize;
   PRINTF4("grf-ImageDelta(): Image=%5d, Delta=%5d, Percentage=%3d,%d%%" CRLF, iImageSize, iDelta, iTemp/10, iTemp%10);
   return(iTemp);
}

//
//  Function:   grf_ImageBufferGrayscale
//  Purpose:    Transfer selected pixels into grayscale
//
//  Parms:      Wand-struct, color format ("RGB"), coordinates
//  Returns:    1=OKee
//
static int grf_ImageBufferGrayscale(char *pcImage, char *pcFormat, int iWs, int iHs, int iWe, int iHe)
{
   int            i;
   int            iX, iY, iGray, iAverageSum;
   int            iFormatLength;
   char          *pcSrc, *pcDest; 

   iFormatLength = GEN_STRLEN(pstMdet->pcFormat);
   //
   pcSrc  = pcImage;
   pcDest = pcImage;
   //
   for(iY=iHs; iY<iHe;iY++)
   {
      for(iX=iWs; iX<iWe; iX++)
      {
         iGray       = 0;
         iAverageSum = 0;
         //
         for(i=0; i<iFormatLength; i++)
         {
            switch(pcFormat[i])
            {
               case 'R':
                  iGray       += (213 * (*pcSrc++));
                  iAverageSum += 213;
                  break;

               case 'G':
                  iGray       += (715 * (*pcSrc++));
                  iAverageSum += 715;
                  break;

               case 'B':
                  iGray       += (72 * (*pcSrc++));
                  iAverageSum += 72;
                  break;

               default:
               case 'I':
                  iGray       += (1000 * (*pcSrc++));
                  iAverageSum += 1000;
                  break;

            }
         }
         //
         // Scale back the average
         //
         iGray /= iAverageSum;
         //
         // Store the result
         //
         for(i=0; i<iFormatLength; i++)
         {
            *pcDest++ = (char) iGray;
         }
         //
         // Display result :
         //   pix, threshold, mode (0=CRLF, 1=value)
         //
         //GRF_DUMP_RGB_LEVELS(iGray, 100, 1);
         GRF_DUMP_RGB_LEVELS(iGray, 0, 1);
      }
      GRF_DUMP_RGB_LEVELS(0,0,0);
   }
   return(1);
}
#endif //COMMENT
