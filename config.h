/*  (c) Copyright:  2017  Patrn, Confidential Data
 *
 *  Workfile:           config.h
 *  Revision:          
 *  Modtime:           
 *
 *  Purpose:            
 *
 *                     
 *
 *  Compiler/Assembler: Raspbian Linux GNU gcc
 *  Ext Packages:
 * 
 *  Author:             Peter Hillen
 *  Date Created:       11 Mar 2017 Ported from rpicam config.h
 * 
 *  Revisions:
 *
 *
 *
 *
 * 
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
**/

#ifndef _CONFIG_H_
#define _CONFIG_H_

//=============================================================================
// Feature switches
//
// makefile:
// If make debug:
//    -D DEBUG_ASSERT
//    -D FEATURE_USE_PRINTF (or overrule here)
//
//#define FEATURE_USE_PRINTF                 // Release mode: disable PRINTFs
//=============================================================================
  #define FEATURE_USE_SSL                    // Use SSL Openssl 
//#define FEATURE_TEST_IO                    // Test IO 
//#define FEATURE_NET_REPORT_PAGE            // Send back full HTML page on HTTP error
//#define FEATURE_MAIL_ALARM_LIST            // Mail us the Alarm list at power up
//#define FEATURE_LOG_MAIL_TO_FILE           // Log all mail trafic to file
//#define FEATURE_ENABLE_CRYPTO_TEST         // Do crypto selftest
//#define FEATURE_TEST_SEND_MAIL             // Test mail (send to stdout i.o. TCP/IP socket)
//#define FEATURE_DUMP_RGB_LEVELS            // Dump Motion detect RGB levels
//#define FEATURE_SHOW_MOTION                // Show motion matrix ulGridMotion[x][y]
  #define FEATURE_PRINT_MOTION               // Print motion debug
//#define FEATURE_PRINT_CYCLE                // Print motion cycle only debug
//#define FEATURE_PRINT_PROXY                // Print proxy debug
//#define FEATURE_SEGMENTED_RECORDING        // Segmented recording
  #define FEATURE_ANNOTATE_TANDD             // Annotate Tme and Date in pix and vids
//#define FEATURE_LOG_PRINTF_TO_FILE         // Send LOG_printf's to the Log file
//#define FEATURE_SHOW_PROCESS_STATUS        // Show PID status of process
//#define FEATURE_RESTORE_DEFAULTS_ON_REBOOT // Global restore of defaults on reboot
//#define FEATURE_AUTO_CHECK_HDD             // Setup Alarm-check HDD/USB 
//#define FEATURE_DUMMY_RPICAL               // Use to startup dymmy version
  #define FEATURE_ECHO_HTTP_IN               // Echo all HTTP incoming data
  #define FEATURE_ECHO_HTTP_OUT              // Echo all HTTP outgoing data
//#define FEATURE_ECHO_BIN_OUT               // Echo all bin outgoing data (if readable)
//#define FEATURE_SHOW_VARS                  // Show hot MAP vars
//#define FEATURE_SHOW_COMMANDS              // Show the CMND thread command buffer
//#define FEATURE_LIST_ALARMS                // List alarms
//#define FEATURE_NET_DUMP_RECORD            // Dump Network records
  #define FEATURE_JSON_SHOW_PAYLOAD          // Show Network JSON payload
  #define FEATURE_JSON_SHOW_REPLY            // Show Network JSON reply
//#define FEATURE_KODI_LOG_PAYLOAD           // Log Network Kodi records
//#define FEATURE_KODI_SHOW_PAYLOAD          // Show Network Kodi records
//#define FEATURE_VIRTUAL_TOGGLE             // Toggle virtual LCD columns every 5 secs
//#define FEATURE_VIRTUAL_LOG                // Show log entries on virtual LCD
//#define FEATURE_VIRTUAL_LOG_TS             // Show log entries on virtual LCD plus time stamps
//#define FEATURE_SHOW_BUTTON_IMAGES         // Show button images
//#define FEATURE_SHOW_BUTTON_SETUP          // Show button setup
//#define FEATURE_SHOW_EXEC_PARMS            // Show exec parameters command line
//#define FEATURE_SHOW_EXEC_PARMS_ALL        // Show exec parameters all args
//#define FEATURE_HTML_DEFAULT_COMMAND       // No need for the "cmd=" syntax on HTML url's
  #define FEATURE_LOG_RPI_INIT               // LOG the main() init traces
//
// CR500.003:  IO-Options are now defined in the makefile and the actual board itself:
//             Rpi-Proj root: file RPIBOARD --> decimal number 1...?
//
// IO:   select define $(INOUT):
//       FEATURE_IO_NONE                  // IO : No IO used !
//       FEATURE_IO_PATRN                 // IO : Use Patrn.nl board
//       FEATURE_IO_GERTBOARD             // IO : Use Gert Board
//       FEATURE_IO_PIFACE                // IO : Use PiFace board
//       FEATURE_IO_PIFACECAD             // IO : Use PiFace Control and Display board (IO and LCD)
//       FEATURE_IO_LCDADA                // IO : Use ADAFRUIT 4xbuttons and 2x16 LCD)
//       FEATURE_IO_TFT                   // IO : Use RAIO8870 320x240 TFT screen
//       FEATURE_IO_TFTBUTS               // IO : Use RAIO8870 320x240 TFT screen plus buttons

//=============================================================================
// Key definitions
//
//       +---+---+---+...+---+---+---+---+
//       | 31| 30| 29|...| 3 | 2 | 1 | 0 |
//       +---+---+---+...+---+---+---+---+
//        msb                         lsb  
//
//=============================================================================
#define  NUM_FP_KEYS          8
//
typedef enum RCUKEYS
{
#define  EXTRACT_KEY(a,b)     a,
#include "key_defs.h"
#include "btn_defs.h"
#undef EXTRACT_KEY
   //
   NUM_RCU_KEYS
}  RCUKEYS;
//
#define LCD_MODE_OFF          0
#define LCD_MODE_ASK          1
#define LCD_MODE_STBY         2
#define LCD_MODE_TEXT         3
#define LCD_MODE_BMP          4
#define LCD_MODE_FB           5
//
#define KEY_SELECT            0
#define KEY_NEXT              1
#define KEY_PREVIOUS          2
#define KEY_START_STOP        3
#define KEY_POWEROFF          4

//=============================================================================
#ifdef   FEATURE_CAMERA      // Camera
#define  CAM_INIT()           CAM_Init()
#else    //FEATURE_CAMERA
#define  CAM_INIT()           0
#endif   //FEATURE_CAMERA
//=============================================================================

//=============================================================================
#ifdef   FEATURE_IO_NONE      // IO : NO IO board used
//=============================================================================
#define  DIO_USE_NONE         //No IO
#define  LCD_USE_NONE         //No LCD screen
//
#define  RPI_IO_BOARD         "RpiCam uses NO IO !"
//
#define  LED_R
#define  LED_Y
#define  LED_G
#define  LED_W
//
#define  REL_1                0
#define  REL_2                1
//
#define  BTN_1                0
#define  BTN_2                1
//
#define  RCU_INIT()           0        
#define  RCU_EXIT()        
#define  BTN_DEFAULT_STATUS   0x00000000
//
#define  INP_GPIO(x)        
#define  OUT_GPIO(x)     
#define  BUTTON(x)            FALSE
#define  LED(x, y)            
#define  OUT(x, y)
//
// Actual LCD Size : 2 x 16 chars
//
#define  LCD_ACTUAL_ROWS      0
#define  LCD_ACTUAL_COLS      0
#define  LCD_FONT_SIZE        0
//
#define  LCD_INIT()           0
#define  LCD_SETUP()          -1
#define  LCD_BACKLIGHT(x)
#define  LCD_COLOR(f,b)
#define  LCD_CURSOR(r,c,x)
#define  LCD_DISPLAY(r,c)
#define  LCD_MENU(r,c,t)
#define  LCD_TEXT(r,c,t)
#define  LCD_MODE(m)          CAD_DisplayMode(m)
#define  LCD_EXIT()
//
#define  LCD_ROW_TIME         0
#define  LCD_COL_TIME         4
//
#define  LCD_ROW_VERSION      1
#define  LCD_COL_VERSION      0
//
#define  LCD_ROW_IP           2
#define  LCD_COL_IP           0
//
#endif

//=============================================================================
#ifdef   FEATURE_IO_GERTBOARD // IO : Use Gert board
//=============================================================================
#include <wiringPi.h>
//
#define  DIO_USE_WIRINGPI     //Digital IO uses wiringPi API library
#define  LCD_USE_NONE         //No LCD screen
//
#define  RPI_IO_BOARD         "RpiCam uses GertBoard"

//
#define  RCU_INIT()           0
#define  RCU_EXIT()        
#define  BTN_DEFAULT_STATUS   0x00000000
//
#define  INP_GPIO(x)        
#define  OUT_GPIO(x)     
#define  BUTTON(x)            
#define  LED(x, y)            
#define  OUT(x, y)
//
// Actual LCD Size : 2 x 16 chars
//
#define  LCD_ACTUAL_ROWS      0
#define  LCD_ACTUAL_COLS      0
#define  LCD_FONT_SIZE        0
//
#define  LCD_INIT()           0
#define  LCD_SETUP()          -1
#define  LCD_BACKLIGHT(x)
#define  LCD_COLOR(f,b)
#define  LCD_CURSOR(r,c,x)
#define  LCD_DISPLAY(r,c)
#define  LCD_MENU(r,c,t)
#define  LCD_TEXT(r,c,t)
#define  LCD_MODE(m)          CAD_DisplayMode(m)
#define  LCD_EXIT()
//
#define  LCD_ROW_TIME         0
#define  LCD_COL_TIME         4
//
#define  LCD_ROW_VERSION      1
#define  LCD_COL_VERSION      0
//
#define  LCD_ROW_IP           2
#define  LCD_COL_IP           0
//
#define  LED_R
#define  LED_Y
#define  LED_G
#define  LED_W
//
#define  REL_1                0
#define  REL_2                1
//
#define  BTN_1                23
#define  BTN_2                24
//
#endif

//=============================================================================
#ifdef   FEATURE_IO_PATRN     // IO : Use Patrn.nl board
//=============================================================================
#include <wiringPi.h>
//
#define  DIO_USE_WIRINGPI     //Digital IO uses wiringPi API library
#define  LCD_USE_NONE         //No LCD screen
//
#define  RPI_IO_BOARD         "RpiCam uses PatrnBoard"
//
#define  LED_R                0
#define  LED_Y                7
#define  LED_G                3
#define  LED_W                6
//
#define  REL_1                11
#define  REL_2                10
//
#define  BTN_1                1
#define  BTN_2                4
//
#define  RCU_INIT()           0
#define  RCU_EXIT()        
#define  BTN_DEFAULT_STATUS   0x00000000
//
#define  INP_GPIO(x)          pinMode(x, INPUT)
#define  OUT_GPIO(x)          pinMode(x, OUTPUT)
#define  BUTTON(x)            !digitalRead(x)
#define  LED(x, y)
#define  OUT(x, y)            digitalWrite(x, y)
//
// Actual LCD Size : 2 x 16 chars
//
#define  LCD_ACTUAL_ROWS      0
#define  LCD_ACTUAL_COLS      0
#define  LCD_FONT_SIZE        0
//
#define  LCD_INIT()           0
#define  LCD_SETUP()          -1
#define  LCD_BACKLIGHT(x)
#define  LCD_COLOR(f,b)
#define  LCD_CURSOR(r,c,x)
#define  LCD_DISPLAY(r,c)
#define  LCD_MENU(r,c,t)
#define  LCD_TEXT(r,c,t)
#define  LCD_MODE(m)          CAD_DisplayMode(m)
#define  LCD_EXIT()
//
#define  LCD_ROW_TIME         0
#define  LCD_COL_TIME         4
//
#define  LCD_ROW_VERSION      1
#define  LCD_COL_VERSION      0
//
#define  LCD_ROW_IP           2
#define  LCD_COL_IP           0
//
#endif

//=============================================================================
#ifdef   FEATURE_IO_PIFACE    // IO : Use PiFace I/O board
//=============================================================================
#include <wiringPi.h>
#include <piFace.h>
//
#define  DIO_USE_WIRINGPI     //Digital IO uses wiringPi API library
#define  LCD_USE_NONE         //No LCD screen
//
#define  RPI_IO_BOARD         "RpiCam uses PiFace Digital 1 Board"
//
#define  PIFACE               200
//
#define  LED_R                0
#define  LED_Y                1
#define  LED_G                2
#define  LED_W                3
//
#define  REL_1                0
#define  REL_2                1
//
#define  BTN_1                0
#define  BTN_2                1
#define  BTN_3                2
#define  BTN_4                3
#define  BTN_5                4
#define  BTN_6                5
#define  BTN_7                6
#define  BTN_8                7
//
#define  RCU_INIT()           0
#define  RCU_EXIT()        
#define  BTN_DEFAULT_STATUS   0x00000000
//
// I/O direction handled by piFaceSetup()
//
#define  INP_GPIO(x)
#define  OUT_GPIO(x)
#define  BUTTON(x)            digitalRead(PIFACE+x)
#define  LED(x, y)            digitalWrite(PIFACE+x,y)
#define  OUT(x, y)            digitalWrite(PIFACE+x,y)
//
// Actual LCD Size : 2 x 16 chars
//
#define  LCD_ACTUAL_ROWS      0
#define  LCD_ACTUAL_COLS      0
#define  LCD_FONT_SIZE        0
//
#define  LCD_INIT()           0
#define  LCD_SETUP()          -1
#define  LCD_BACKLIGHT(x)
#define  LCD_COLOR(f,b)
#define  LCD_CURSOR(r,c,x)
#define  LCD_DISPLAY(r,c)
#define  LCD_MENU(r,c,t)
#define  LCD_TEXT(r,c,t)
#define  LCD_MODE(m)          CAD_DisplayMode(m)
#define  LCD_EXIT()
//
#define  LCD_ROW_TIME         0
#define  LCD_COL_TIME         0
//
#define  LCD_ROW_VERSION      0
#define  LCD_COL_VERSION      0
//
#define  LCD_ROW_IP           0
#define  LCD_COL_IP           0
//
#endif

//=============================================================================
#ifdef   FEATURE_IO_PIFACECAD // IO : Use PiFaceCad I/O + LCD board
//=============================================================================
#include <wiringPi.h>
#include "rpi_rcu.h"
#include "cmd_cad.h"
//
#define  DIO_USE_WIRINGPI     //Digital IO uses wiringPi API library
#define  LCD_USE_WIRINGPI     //LCD screen used wiringPi API lbrary
//
#define  RPI_IO_BOARD         "RpiCam uses PiFace Control and Display Board"
//
#define  LED_R
#define  LED_Y
#define  LED_G
#define  LED_W
//
#define  REL_1                0
#define  REL_2                1
//
#define  BTN_1                0
#define  BTN_2                1
#define  BTN_3                2
#define  BTN_4                3
#define  BTN_5                4
#define  BTN_6                5
#define  BTN_7                6
#define  BTN_8                7
//
#define  RCU_INIT()           RCU_Init()
#define  RCU_EXIT()           RCU_Exit()
#define  BTN_DEFAULT_STATUS   0x00000000
//
#define  INP_GPIO(x)        
#define  OUT_GPIO(x)     
#define  BUTTON(x)            FALSE
#define  LED(x, y)            
#define  OUT(x, y)
//
// Actual LCD Size : 2 x 16 chars
//
#define  LCD_ACTUAL_ROWS      2
#define  LCD_ACTUAL_COLS      16
#define  LCD_FONT_SIZE        0
//
#define  LCD_INIT()           CAD_Init()
#define  LCD_SETUP()          lcdInit(LCD_ACTUAL_ROWS, LCD_ACTUAL_COLS, 4, 11, 10, 0, 1, 2, 3, 0, 0, 0, 0)
#define  LCD_BACKLIGHT(x)     CAD_Backlight(x)
#define  LCD_COLOR(f,b)
#define  LCD_CURSOR(r,c,x)    CAD_Cursor(r,c,x)
#define  LCD_DISPLAY(r,c)     CAD_SetDisplay(r,c)
#define  LCD_MENU(r,c,t)      CAD_WriteLine(r,c,t)
#define  LCD_TEXT(r,c,t)      CAD_Text(r,c,t)
#define  LCD_MODE(m)          CAD_DisplayMode(m)
#define  LCD_EXIT()           CAD_Exit()
//
#define  LCD_ROW_TIME         0
#define  LCD_COL_TIME         4
//
#define  LCD_ROW_VERSION      1
#define  LCD_COL_VERSION      0
//
#define  LCD_ROW_IP           2
#define  LCD_COL_IP           0
//
#endif

//=============================================================================
#ifdef   FEATURE_IO_TFT       // IO : Use RAIO8870 320x240 TFT
//=============================================================================
#define  DIO_USE_NONE         //No IO
#define  LCD_USE_FRAMEBUFFER  //TFT screen uses framebuffer deamon
//
#define  RPI_IO_BOARD         "RpiCam uses RAIO8870 board"
//
#define  LED_R
#define  LED_Y
#define  LED_G
#define  LED_W
//
#define  REL_1                0
#define  REL_2                1
//
#define  BTN_1                0
#define  BTN_2                1
#define  BTN_3                2
#define  BTN_4                3
#define  BTN_5                4
#define  BTN_6                5
#define  BTN_7                6
#define  BTN_8                7
//
#define  RCU_INIT()           0
#define  RCU_EXIT()        
#define  BTN_DEFAULT_STATUS   0x00000000
//
#define  INP_GPIO(x)        
#define  OUT_GPIO(x)     
#define  BUTTON(x)            FALSE
#define  LED(x, y)            
#define  OUT(x, y)
//
// Actual LCD Size : 2 x 16 chars
//
#define  LCD_ACTUAL_ROWS      7
#define  LCD_ACTUAL_COLS      20
#define  LCD_FONT_SIZE        5
//
#define  LCD_INIT()           CAD_Init()
#define  LCD_SETUP()          -1
#define  LCD_BACKLIGHT(x)     CAD_Backlight(x)
#define  LCD_COLOR(f,b)       CAD_Color(f,b)
#define  LCD_CURSOR(r,c,x)    CAD_Cursor(r,c,x)
#define  LCD_DISPLAY(r,c)     CAD_SetDisplay(r,c)
#define  LCD_MENU(r,c,t)      CAD_WriteLine(r,c,t)
#define  LCD_TEXT(r,c,t)      CAD_Text(r,c,t)
#define  LCD_MODE(m)          CAD_DisplayMode(m)
#define  LCD_EXIT()           CAD_Exit()
//
#define  LCD_ROW_TIME         0
#define  LCD_COL_TIME         32
//
#define  LCD_ROW_VERSION      0
#define  LCD_COL_VERSION      0
//
#define  LCD_ROW_IP           0
#define  LCD_COL_IP           15
//
#endif

//=============================================================================
#ifdef   FEATURE_IO_TFTBUTS   // IO : Use RAIO8870 320x240 TFT plus buttons
//=============================================================================
#include <wiringPi.h>
//
#define  DIO_USE_WIRINGPI     //Digital IO uses wiringPi API library
#define  LCD_USE_FRAMEBUFFER  //TFT screen uses framebuffer deamon
//
#define  RPI_IO_BOARD         "RpiCam uses RAIO8870 board API plus buttons"
//
#define  LED_R
#define  LED_Y
#define  LED_G
#define  LED_W
//
#define  REL_1                0
#define  REL_2                1
//
#define  BTN_1                25
#define  BTN_2                27
#define  BTN_3                2
#define  BTN_4                3
#define  BTN_5                4
#define  BTN_6                5
#define  BTN_7                6
#define  BTN_8                7
//
#define  RCU_INIT()           0
#define  RCU_EXIT()        
#define  BTN_DEFAULT_STATUS   0x00000000
//
#define  INP_GPIO(x)          pinMode(x, INPUT)
#define  OUT_GPIO(x)          pinMode(x, OUTPUT)
#define  BUTTON(x)            !digitalRead(x)
#define  LED(x, y)
#define  OUT(x, y)
//
// Actual LCD Size : 2 x 16 chars
//
#define  LCD_ACTUAL_ROWS      7
#define  LCD_ACTUAL_COLS      20
#define  LCD_FONT_SIZE        5
//
#define  LCD_INIT()           CAD_Init()
#define  LCD_SETUP()          -1
#define  LCD_BACKLIGHT(x)     CAD_Backlight(x)
#define  LCD_COLOR(f,b)       CAD_Color(f,b)
#define  LCD_CURSOR(r,c,x)    CAD_Cursor(r,c,x)
#define  LCD_DISPLAY(r,c)     CAD_SetDisplay(r,c)
#define  LCD_MENU(r,c,t)      CAD_WriteLine(r,c,t)
#define  LCD_TEXT(r,c,t)      CAD_Text(r,c,t)
#define  LCD_MODE(m)          CAD_DisplayMode(m)
#define  LCD_EXIT()           CAD_Exit()
//
#define  LCD_ROW_TIME         0
#define  LCD_COL_TIME         32
//
#define  LCD_ROW_VERSION      0
#define  LCD_COL_VERSION      0
//
#define  LCD_ROW_IP           0
#define  LCD_COL_IP           15
//
#endif

//=============================================================================
#ifdef   FEATURE_IO_LCDADA    // IO : Use Adafruit I/O + LCD board
//=============================================================================
#include <wiringPi.h>
//
#define  DIO_USE_WIRINGPI     //Digital IO uses wiringPi API library
#define  LCD_USE_WIRINGPI     //LCD screen used wiringPi API lbrary
//
#define  RPI_IO_BOARD         "RpiCam uses Adafruit LCD I/O board"
#define  LED_R
#define  LED_Y
#define  LED_G
#define  LED_W
//
#define  REL_1                0
#define  REL_2                1
//
#define  BTN_1                13
#define  BTN_2                7
#define  BTN_3                4
#define  BTN_4                12
//
#define  RCU_INIT()           0
#define  RCU_EXIT()        
#define  BTN_DEFAULT_STATUS   0x00000000
//
#define  INP_GPIO(x)          {pinMode(x, INPUT);pullUpDnControl(x,PUD_UP);}
#define  OUT_GPIO(x)          pinMode(x, OUTPUT)
#define  BUTTON(x)            !digitalRead(x)
#define  LED(x, y)            
#define  OUT(x, y)
//
// Actual LCD Size : 2 x 16 chars
//
#define  LCD_ACTUAL_ROWS      2
#define  LCD_ACTUAL_COLS      16
#define  LCD_FONT_SIZE        1
//
#define  LCD_INIT()           CAD_Init()
#define  LCD_SETUP()          lcdInit(LCD_ACTUAL_ROWS, LCD_ACTUAL_COLS, 4, 11, 10, 0, 1, 2, 3, 0, 0, 0, 0)
#define  LCD_BACKLIGHT(x)     CAD_Backlight(x)
#define  LCD_COLOR(f,b)       
#define  LCD_CURSOR(r,c,x)    CAD_Cursor(r,c,x)
#define  LCD_DISPLAY(r,c)     CAD_SetDisplay(r,c)
#define  LCD_MENU(r,c,t)      CAD_WriteLine(r,c,t)
#define  LCD_TEXT(r,c,t)      CAD_Text(r,c,t)
#define  LCD_MODE(m)          CAD_DisplayMode(m)
#define  LCD_EXIT()           CAD_Exit()
//
#define  LCD_ROW_TIME         0
#define  LCD_COL_TIME         4
//
#define  LCD_ROW_VERSION      1
#define  LCD_COL_VERSION      0
//
#define  LCD_ROW_IP           2
#define  LCD_COL_IP           0
//
//=============================================================================
#endif

//
// The build process determines RELEASE or DEBUG builds through the
// DEBUG_ASSERT compile-time switch.
//
#ifdef  DEBUG_ASSERT
#define VERSION                     "1.13-CR057"
#else   //DEBUG_ASSERT
#define VERSION                     "1.14-057"
#endif  //DEBUG_ASSERT
//
#define DATA_VALID_SIGNATURE        0xDEADBEEF           // Valid signature
#define DATA_VERSION_MAJOR          11
#define DATA_VERSION_MINOR          1
//
#define RPI_PUBLIC_WWW              "/all/public/www/"
#define RPI_PUBLIC_IMAGES           "/all/public/www/images/"
#define RPI_PUBLIC_ICONS            "/all/public/www/icons/"
#define RPI_PUBLIC_ERROR            "/all/public/www/icons/error.jpg"
#define RPI_EXTERNAL_STORAGE        "/all/public/www/hdd/video/"
#define RPI_PIX_EXEC_DUMMY          "/opt/vc/bin/raspistilldummy"
#define RPI_PIX_EXEC                "/opt/vc/bin/raspistill"
#define RPI_VID_EXEC                "/opt/vc/bin/raspivid"
#define RPI_VID_EXEC_DUMMY          "/opt/vc/bin/raspistilldummy"
#define RPI_PID_FILE                "/var/run/spicamd.pid"
#define RPI_PUBLIC_PIX              "pix/"
#define RPI_PUBLIC_VIDEO            "video/"
#define RPI_PUBLIC_PIX_EXT          "hdd/pix/"
#define RPI_PUBLIC_VIDEO_EXT        "hdd/video/"
#define RPI_PUBLIC_DEFAULT          "index.html"
//
#define RPI_BASE_NAME               "spicam"
#define RPI_ERR_FILE                RPI_BASE_NAME ".err"
#define RPI_LOG_FILE                RPI_BASE_NAME ".log"
#define RPI_MAP_FILE                RPI_BASE_NAME ".map"
#define RPI_SHELL_FILE              RPI_BASE_NAME ".txt"
#define RPI_ALL_FILES               RPI_BASE_NAME ".*"
//
// Target RAM disk for high load file access
//
#define RPI_WORK_DIR                "/mnt/rpicache/"
#define RPI_RAMFS_DIR               "/mnt/rpipix/"
#define RPI_BACKUP_DIR              "/usr/local/share/rpi/"
//
#define RPI_PUBLIC_LOG_PATH         RPI_WORK_DIR RPI_LOG_FILE
#define RPI_ERROR_PATH              RPI_WORK_DIR RPI_ERR_FILE
#define RPI_MAP_PATH                RPI_WORK_DIR RPI_MAP_FILE
#define RPI_MAP_NEWPATH             RPI_WORK_DIR RPI_BASE_NAME "new.map"
//
#define RPI_MREP_PATH               RPI_WORK_DIR      "spimrep.log"
#define RPI_PROX_PATH               RPI_WORK_DIR      "spiprox.tmp"
#define RPI_MAIL_PATH               RPI_WORK_DIR      "spimail.log"
#define RPI_MVEC_PATH               RPI_PUBLIC_IMAGES "spimvec.bin"
//
#define RPI_ONLINE_FILL             ' '
//
#define RPI_LOCALHOST               "www.patrn.nl"
#define RPI_WATCHDOG                "rpidog"
#define RPI_RASPISTILL              "raspistill"
#define RPI_RASPIVID                "raspivid"

#endif /* _CONFIG_H_ */
