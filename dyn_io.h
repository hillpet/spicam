/*  (c) Copyright:  2018  Patrn, Confidential Data
 *
 *  Workfile:           dyn_io.h
 *  Revision:          
 *  Modtime:           
 *
 *  Purpose:            Extract headerfile for Remote IO dynamic http pages
 *
 *
 *
 *  Compiler/Assembler: Raspbian Linux GNU gcc
 *  Ext Packages:
 *
 *  Author:             Peter Hillen
 *  Date Created:       21 May 2018
 *
 *  Revisions:
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
**/

EXTRACT_DYN(RIO_DYN_HTML_REMIO,    HTTP_HTML,  10000, DYN_FLAG_PORT, "io.html",           btn_DynPageRemoteIo       )
EXTRACT_DYN(RIO_DYN_JSON_REMIO,    HTTP_JSON,  10000, DYN_FLAG_PORT, "io.json",           btn_DynPageRemoteIo       )
