/*  (c) Copyright:  2016..2018  Patrn.nl, Confidential Data 
 *
 *  Workfile:           cmd_cad
 *  Revision:  
 *  Modtime:   
 *
 *  Purpose:            Interface Cad functions
 *
 * 
 *  Compiler/Assembler: Raspbian Linux GNU gcc
 * Ext Packages:
 * 
 * Author:             Peter Hillen
 * Date Created:       26 feb 2016
 *
 * Revisions:
 * Entry Points:       
 *
 * 
 * 
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
**/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <time.h>
#include <errno.h>
#include <sys/types.h>
#include <lcd.h>

#include <common.h>
#include "config.h"
#include "globals.h"
//
#include "cmd_cad.h"

//#define USE_PRINTF
#include <printf.h>

//
// Local prototypes
//
static void cad_InitSemaphore       (void);
static int  cad_WaitSemaphore       (int);
static void cad_PostSemaphore       (void);
static void cad_Write               (u_int8, u_int8, char *);
static int  min                     (int, int);
//
//
//  Function:   CAD_Backlight
//  Purpose:    Handle backlight
//
//  Parms:      Backlight secs (neg to keep bl ON
//  Returns:    
//
void CAD_Backlight(int iIntense)
{
   PRINTF("CAD_Backlight()" CRLF);
   cad_WaitSemaphore(NO_TIMEOUT);

#ifdef LCD_USE_WIRINGPI
   VLCD    *pstLcd=&pstMap->G_stVirtLcd;

   if(iIntense > 0)  
   {
      //
      // Turn LCD Backlight ON for xx secs
      //
      pstMap->G_iLcdBacklight = LCD_BACKLIGHT_ON; 
      if(pstLcd->tLcdHandle != -1)
      {
         lcdDisplay(pstLcd->tLcdHandle, 1);
      }
      PRINTF2("CAD_Backlight(Pos):Intense=%d, Timeout=%d" CRLF, iIntense, pstMap->G_iLcdBacklight);
   }
   else if(iIntense < 0)  
   {
      //
      // Turn LCD Backlight ON permanently
      //
      pstMap->G_iLcdBacklight = -LCD_BACKLIGHT_ON; 
      if(pstLcd->tLcdHandle != -1)
      {
         lcdDisplay(pstLcd->tLcdHandle, 1);
      }
      PRINTF2("CAD_Backlight(Neg):Intense=%d, Timeout=%d" CRLF, iIntense, pstMap->G_iLcdBacklight);
   }
   else 
   {
      if(pstLcd->tLcdHandle != -1)
      {
         lcdDisplay(pstLcd->tLcdHandle, 0);
      }
      PRINTF2("CAD_Backlight(Off):Intense=%d, Timeout=%d" CRLF, iIntense, pstMap->G_iLcdBacklight);
   }
#endif

#ifdef LCD_USE_FRAMEBUFFER
   pid_t    tPid;

   if( (tPid = TFT_CheckFramebuffer()) > 0)
   {
      if(iIntense > 0)  
      {
         PRINTF1("CAD_Backlight(TEMP):Pid=%d" CRLF, tPid);
         //
         // Turn LCD Backlight ON for xx secs
         // Send SIGUSR1 (10) : backlight ON
         //
         pstMap->G_iLcdBacklight = LCD_BACKLIGHT_ON; 
         GEN_SignalPid(tPid, SIGUSR1);
      }
      else if(iIntense < 0)  
      {
         PRINTF1("CAD_Backlight(ON):Pid=%d" CRLF, tPid);
         //
         // Turn LCD Backlight ON permanently
         // Send SIGUSR1 : backlight ON
         //
         pstMap->G_iLcdBacklight = -LCD_BACKLIGHT_ON;
         GEN_SignalPid(tPid, SIGUSR1);
      }
      else
      {
         PRINTF1("CAD_Backlight(OFF):Pid=%d" CRLF, tPid);
         //
         // Turn LCD Backlight OFF
         // Send SIGUSR2 (12) : backlight OFF
         //
         GEN_SignalPid(tPid, SIGUSR2);
      }
   }
#endif
   //
   cad_PostSemaphore();
}

//
//  Function:   CAD_Exit
//  Purpose:    Close interface
//
//  Parms:      
//  Returns:    
//
void CAD_Exit(void)
{
   cad_WaitSemaphore(NO_TIMEOUT);
   //

#ifdef LCD_USE_WIRINGPI
   VLCD       *pstLcd=&pstMap->G_stVirtLcd;

   if(pstLcd->tLcdHandle != -1)
   {
      lcdClear(pstLcd->tLcdHandle);
      lcdCursor(pstLcd->tLcdHandle, 0);
      lcdCursorBlink(pstLcd->tLcdHandle, 0);
   }
#endif

   cad_PostSemaphore();
   CAD_Backlight(0);
}

//
//  Function:   CAD_Cursor
//  Purpose:    Handle cursor
//
//  Parms:      
//  Returns:    
//
void CAD_Cursor(u_int8 ubRow, u_int8 ubCol, bool fOn)
{
   VLCD       *pstLcd=&pstMap->G_stVirtLcd;

   PRINTF("CAD_Cursor()" CRLF);
   cad_WaitSemaphore(NO_TIMEOUT);
   //
   if(fOn)  
   {
      pstLcd->fLcdCursor = TRUE;
      pstLcd->ubCursRow  = ubRow;
      pstLcd->ubCursCol  = ubCol;

#ifdef DIO_USE_WIRINGPI
      if(pstLcd->tLcdHandle != -1)
      {
         lcdPosition(pstLcd->tLcdHandle, ubRow, ubCol);
         lcdCursor(pstLcd->tLcdHandle, 1);
         lcdCursorBlink(pstLcd->tLcdHandle, 1);
      }
#endif

   }
   else 
   {
      pstLcd->fLcdCursor = FALSE;
      if(pstLcd->tLcdHandle != -1)
      {
         lcdCursor(pstLcd->tLcdHandle, 0);
      }
   }
   cad_PostSemaphore();
}

//
//  Function:   CAD_SetDisplay
//  Purpose:    Set display address for virtual screen and refresh
//
//  Parms:      row, col
//  Returns:    
//
void CAD_SetDisplay(u_int8 ubRow, u_int8 ubCol)
{
   VLCD       *pstLcd=&pstMap->G_stVirtLcd;

   cad_WaitSemaphore(NO_TIMEOUT);
   pstLcd->ubVirtRow  = min(ubRow, (LCD_VIRTUAL_ROWS - LCD_ACTUAL_ROWS));
   pstLcd->ubVirtCol  = min(ubCol, (LCD_VIRTUAL_COLS - LCD_ACTUAL_COLS));
   PRINTF2("CAD_SetDisplay():(%d, %d)" CRLF, pstLcd->ubVirtRow, pstLcd->ubVirtCol);
   cad_Write(pstLcd->ubVirtRow, pstLcd->ubVirtCol, NULL);
   cad_PostSemaphore();
}

//
//  Function:   CAD_DisplayCols
//  Purpose:    Get number of columns on the display
//
//  Parms:      Mode
//  Returns:    Lines
//
int CAD_DisplayCols(void)
{
   return(LCD_ACTUAL_COLS);
}

//
//  Function:   CAD_DisplayRows
//  Purpose:    Get number of lines on the display
//
//  Parms:      Mode
//  Returns:    Lines
//
int CAD_DisplayRows(void)
{
   return(LCD_ACTUAL_ROWS);
}

//
//  Function:   CAD_DisplayMode
//  Purpose:    Set/Get current LCD mode
//
//  Parms:      Mode
//  Returns:    Mode
//
int CAD_DisplayMode(int iMode)
{
   VLCD       *pstLcd=&pstMap->G_stVirtLcd;
   
   if(iMode == LCD_MODE_ASK)
   {
      iMode = pstLcd->iLcdMode;
   }
   else
   {
      pstLcd->iLcdMode = iMode;
   }
   return(iMode);
}

//
//  Function:   CAD_Init
//  Purpose:    Open display interface
//
//  Parms:      
//  Returns:    GLOBAL_LCD_INI flag
//
int CAD_Init()
{
   int         iCc=GLOBAL_LCD_INI;
   VLCD       *pstLcd=&pstMap->G_stVirtLcd;

   CAD_DisplayMode(LCD_MODE_TEXT);
   //
   cad_InitSemaphore();
   cad_WaitSemaphore(NO_TIMEOUT);
   //
   pstLcd->tLcdHandle = LCD_SETUP();
   cad_PostSemaphore();
   //
   CAD_Cursor(0,0, FALSE);
   return(iCc);
}

//
//  Function:   CAD_Text
//  Purpose:    Write text to (virtual) screen
//
//  Parms:      row, col, text
//  Returns:    
//
void CAD_Text(u_int8 ubRow, u_int8 ubCol, char *pcText)
{
   cad_WaitSemaphore(NO_TIMEOUT);
   //
   // Write text into virtual screen
   //
   PRINTF("CAD_Text()" CRLF);
   cad_Write(ubRow, ubCol, pcText);
   cad_PostSemaphore();
}

//
//  Function:   CAD_WriteLine
//  Purpose:    Write single line to the virtual screen
//
//  Parms:      row, col, text
//  Returns:    
//
void CAD_WriteLine(u_int8 ubRow, u_int8 ubCol, char *pcText)
{
   cad_WaitSemaphore(NO_TIMEOUT);
   PRINTF("CAD_Text()" CRLF);
   //
   // Write text into virtual screen
   //
   cad_Write(ubRow, ubCol, pcText);
   cad_PostSemaphore();
}

/*------  Local functions separator ------------------------------------
__Local_functions_(){};
----------------------------------------------------------------------------*/

//
// Function:   cad_InitSemaphore
// Purpose:    Init the screen Semaphore
//
// Parms:
// Returns:
// Note:       If pshared has the value 0, then the semaphore is shared between the
//             threads of a process, and should be located at some address that is
//             visible to all threads (e.g., a global variable, or a variable
//             allocated dynamically on the heap).
//             If pshared is nonzero, then the semaphore is shared between
//             processes, and should be located in a region of shared memory.
//
//             The sem value is always >= 0:
//                               0 means LOCKED
//                              >0 means UNLOCKED
//             Init(sem 1, VAL)  VAL=1 unlocked
//                               VAL=0 locked
//             Wait(sem)         LOCK:    if(VAL> 0) { VAL--; return;         } 
//                                        if(VAL==0) { Wait till > 0; return; }
//             Post(sem)         UNLOCK:  VAL++
//
static void cad_InitSemaphore(void)
{
   //
   // Init the semaphore as UNLOCKED
   //
   if(sem_init(&pstMap->G_tSemLcd, 1, 1) == -1)
   {
      LOG_Report(errno, "CAD", "cad_InitSemaphore(): sem_init Error");
   }
}

//
//  Function:  cad_WaitSemaphore
//  Purpose:   Wait till screen Semaphore has been unlocked, then LOCK and proceed
//
// Parms:      TIMEOUT (or -1 if wait indefinitely)
// Returns:     0 if sem unlocked
//             +1 if sem timeout
//             -1 on error (errno)
// Note:       sem_wait() tries to decrement (lock) the semaphore. 
//             If the semaphore currently has the value zero, then the call blocks until 
//             either it becomes possible to perform the decrement (the semaphore value rises 
//             above zero) or a signal handler interrupts the call. 
//             If the semaphore's value is greater than zero, then the decrement proceeds, 
//             and the function returns immediately. 
//
//             sem_timedwait() is the same as sem_wait(), except that TIMEOUT specifies a 
//             limit on the amount of time that the call should block if the decrement cannot 
//             be immediately performed. The stTime points to a structure that
//             specifies an absolute timeout in seconds and nanoseconds since the Epoch, 
//             1970-01-01 00:00:00 +0000 (UTC). This structure is defined as follows:
//
//             struct timespec 
//             {
//                time_t tv_sec;      /* Seconds */
//                long   tv_nsec;     /* Nanoseconds [0 .. 999,999,999] */
//             };
//             If the timeout has already expired by the time of the call, and the semaphore 
//             could not be locked immediately, then sem_timedwait() fails with a timeout error 
//             (errno set to ETIMEDOUT). If the operation can be performed immediately, then 
//             sem_timedwait() never fails with a timeout error, regardless of the value of 
//             TIMEOUT.
//
static int cad_WaitSemaphore(int iMsecs)
{
   //
   // if    sem == 0 : wait
   // else  sem-- (LOCK)   
   //
   return(GEN_WaitSemaphore(&pstMap->G_tSemLcd, iMsecs));
}

//
//  Function:   cad_PostSemaphore
//  Purpose:    Unlock the screen Semaphore
//
//  Parms:      
//  Returns:    
//
static void cad_PostSemaphore(void)
{
   int      iCc;

   PRINTF("cad_PostSemaphore():" CRLF);
   //
   // sem++ (UNLOCK)   
   //
   iCc = sem_post(&pstMap->G_tSemLcd);
   if(iCc == -1) LOG_Report(errno, "CAD", "cad_PostSemaphore(): sem_post error");
   PRINTF("cad_PostSemaphore():Done" CRLF);
}

//
//  Function:   cad_Write
//  Purpose:    Write to virtual screen; Update actual screen
//
//  Parms:      r, c, Text, size
//  Returns:    
//
//  Note:       A NULL pointer means the display window has shifted, so the whole
//              screens needs to be repainted
//
static void cad_Write(u_int8 ubRa, u_int8 ubCa, char *pcText)
{
   int         iLen=0;
   int         iMaxLen, iAddr=0;
#ifdef LCD_USE_WIRINGPI
   int         iCaL;
   u_int8      ubRt, ubCt;
   u_int8      ubRv, ubCv;
   u_int8      ubRe, ubCe;
   char       *pcLcd;
#endif
   VLCD       *pstLcd=&pstMap->G_stVirtLcd;

   if(pcText)
   {
      iAddr = (ubRa * LCD_VIRTUAL_COLS) + ubCa;
      iLen  = GEN_STRLEN(pcText);
      //
      // Check if the text fits into the virtual screen
      //
      if( (ubRa >= LCD_VIRTUAL_ROWS) || (ubCa >= LCD_VIRTUAL_COLS) ) 
      {
         PRINTF3("cad_Write():BAD COORDS @(%d,%d) [%s]" CRLF, ubRa, ubCa, pcText);
         return;
      }
      //
      // Write text into virtual screen
      //
      iMaxLen = (LCD_VIRTUAL_ROWS * LCD_VIRTUAL_COLS) - iAddr;
      if(iMaxLen < iLen) 
      {
         PRINTF3("cad_Write():Too large string @%d (len=%d, max=%d)" CRLF, iAddr, iLen, iMaxLen);
         iLen = iMaxLen; 
      }
      PRINTF5("cad_Write():New text @(%d,%d) Addr=%d, l=%d [%s]" CRLF, ubRa, ubCa, iAddr, iLen, pcText);
      GEN_MEMCPY(&(pstLcd->pcVirtLcd[iAddr]), pcText, iLen);
   }

   if(pstLcd->iLcdMode == LCD_MODE_TEXT)
   {
      //
      // Fysical screen is in text mode
      //
      #ifdef LCD_USE_WIRINGPI
         if(pstLcd->tLcdHandle != -1)
         {
            if(pcText == NULL)
            {
               //
               // Put out existing virtual screen
               //
               for(ubRt=0; ubRt<LCD_ACTUAL_ROWS; ubRt++)
               {
                  PRINTF("[");
                  lcdPosition(pstLcd->tLcdHandle, ubRt, 0);
                  iAddr = ((ubRt+pstLcd->ubVirtRow) * LCD_VIRTUAL_COLS)+pstLcd->ubVirtCol;
                  pcLcd = &(pstLcd->pcVirtLcd[iAddr]);
                  for(ubCt=0; ubCt<LCD_ACTUAL_COLS; ubCt++)
                  {
                     PRINTF1("%c", *pcLcd);
                     lcdPutchar(pstLcd->tLcdHandle, *pcLcd++);
                  }
                  PRINTF1("] Addr=%d" CRLF, iAddr);
               }
            }
            else
            {
               iAddr = (ubRa * LCD_VIRTUAL_COLS) + ubCa;
               //
               // Put out changed text only
               //
               ubRv = pstLcd->ubVirtRow;
               ubCv = pstLcd->ubVirtCol;
               iCaL = iLen + ubCa;
               //
               ubRe = ubRv + LCD_ACTUAL_ROWS - 1;
               ubCe = ubCv + LCD_ACTUAL_COLS - 1;
               //
               PRINTF6("cad_Write():a=(%d,%d) v=(%d,%d) e=(%d,%d)" CRLF, ubRa, ubCa, ubRv, ubCv, ubRe, ubCe);
               //
               // Check if row is within tft window
               //
               if( (ubRa < ubRv) || (ubRa > ubRe) ) return;
               ubRt = ubRa - ubRv;
               //
               // Check if column is within tft window
               //
               if( (ubCa > ubCe) || (iCaL < (int)ubCv) ) return;
               //
               // We have (some) text within the tft window
               //
               if(ubCa < ubCv)
               {
                  ubCt   = 0;
                  iAddr += (int)(ubCv - ubCa);
                  if(iCaL >= ubCe)  iLen  = (int)(ubCe - ubCv);
                  else              iLen -= (ubCv - ubCa);
                  PRINTF4("cad_Write():t=(%d,%d) Len=%d Addr-->%d" CRLF, ubRt, ubCt, iLen, iAddr);
               }
               else
               {
                  ubCt = ubCa - ubCv;
                  if(iCaL >= (int)ubCe)  
                  {
                     PRINTF2("cad_Write():Ca=%d Len=%d : truncate !" CRLF, ubCa, iLen);
                     iLen = (int)(ubCe - ubCa);
                  }
                  PRINTF4("cad_Write():t=(%d,%d) Len=%d Addr=%d" CRLF, ubRt, ubCt, iLen, iAddr);
               }
               //
               // Put out actual LCD lines
               //
               PRINTF("[");
               lcdPosition(pstLcd->tLcdHandle, ubRt, ubCt);
               pcLcd = &(pstLcd->pcVirtLcd[iAddr]);
               while(iLen--)
               {
                  PRINTF1("%c", *pcLcd);
                  lcdPutchar(pstLcd->tLcdHandle, *pcLcd++);
               }
               PRINTF1("] Addr=%d" CRLF, iAddr);
            }
            //
            // Check if the cursor needs ON/OFF
            //
            if(pstLcd->fLcdCursor)
            {
               if( (pstLcd->ubVirtRow <= pstLcd->ubCursRow) && (pstLcd->ubVirtRow > (pstLcd->ubCursRow-LCD_ACTUAL_ROWS)) )
               {
                  if( (pstLcd->ubVirtCol <= pstLcd->ubCursCol) && (pstLcd->ubVirtCol > (pstLcd->ubCursCol-LCD_ACTUAL_COLS)) )
                  {
                     lcdPosition(pstLcd->tLcdHandle, pstLcd->ubCursRow-pstLcd->ubVirtRow, pstLcd->ubCursCol-pstLcd->ubVirtCol);
                     lcdCursor(pstLcd->tLcdHandle, 1);
                     lcdCursorBlink(pstLcd->tLcdHandle, 1);
                     PRINTF2("cad_Write():Cursor ON (%d, %d)" CRLF, pstLcd->ubCursRow-pstLcd->ubVirtRow, pstLcd->ubCursCol-pstLcd->ubVirtCol);
                  }
                  else
                  {
                     lcdCursor(pstLcd->tLcdHandle, 0);
                     lcdCursorBlink(pstLcd->tLcdHandle, 0);
                     PRINTF("cad_Write():Cursor OFF" CRLF);
                  }
               }
            }
            else 
            {
               lcdCursor(pstLcd->tLcdHandle, 0);
               lcdCursorBlink(pstLcd->tLcdHandle, 0);
               PRINTF("cad_Write():Cursor OFF" CRLF);
            }
         }
      #endif
   }
}

//
//  Function:   min
//  Purpose:    Determine min value
//
//  Parms:      x, y
//  Returns:    Min value
//
static int min(int a, int b)
{
    return(a < b ? a : b);
}

