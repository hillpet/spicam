/*  (c) Copyright:  2017  Patrn, Confidential Data
 *
 *  Workfile:           template.h
 *  Revision:          
 *  Modtime:           
 *
 *  Purpose:            Headerfile for rpi_kodi.c
 *
 *
 *
 *  Compiler/Assembler: Raspbian Linux GNU gcc
 *  Ext Packages:
 *
 *  Author:             Peter Hillen
 *  Date Created:       11 Aug 2017
 *
 *  Revisions:
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
**/

#ifndef _RPI_KODI_H_
#define _RPI_KODI_H_

typedef enum _kodi_cnx_
{
   KCNX_INIT_CONNECT       = 0,
   KCNX_EXEC_CONNECT,
   KCNX_INIT_DISCONNECT,
   KCNX_EXEC_DISCONNECT,
   KCNX_EXIT,
   //
   NUM_KCNX
}  KCNX;
//
int KODI_Init();

#endif /* _RPI_KODI_H_ */
