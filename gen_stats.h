/*  (c) Copyright:  2017  Patrn, Confidential Data
 *
 *  Workfile:           gen_stats.h
 *  Revision:          
 *  Modtime:           
 *
 *  Purpose:            Headerfile for GEN_STATUS_xxx
 *
 *
 *
 *  Compiler/Assembler: Raspbian Linux GNU gcc
 *  Ext Packages:
 *
 *  Author:             Peter Hillen
 *  Date Created:       21 Mar 2017
 *
 *  Revisions:
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
**/

 
EXTRACT_ST( CAM_STATUS_IDLE,           "Cam-Idle"         )
EXTRACT_ST( CAM_STATUS_PARMS,          "Cam-Parms"        )
EXTRACT_ST( CAM_STATUS_ERROR,          "Cam-Error"        )
EXTRACT_ST( CAM_STATUS_REDIRECT,       "Cam-Redirect"     )
EXTRACT_ST( CAM_STATUS_SNAPSHOT,       "Cam-Snapshot"     )
EXTRACT_ST( CAM_STATUS_MOTION,         "Cam-Motion"       )      
EXTRACT_ST( CAM_STATUS_RECORDING,      "Cam-Recording"    )      
EXTRACT_ST( CAM_STATUS_TIMELAPSE,      "Cam-Timelapse"    )      
EXTRACT_ST( CAM_STATUS_STREAMING,      "Cam-Streaming"    )      
EXTRACT_ST( CAM_STATUS_STOPPED,        "Cam-Stopped"      )      
EXTRACT_ST( CAM_STATUS_REJECTED,       "Cam-Rejected"     )      
EXTRACT_ST( CAM_STATUS_KEYPRESS,       "Cam-Keypress"     )      
EXTRACT_ST( CAM_STATUS_KEYRELEASE,     "Cam-Keyrelease"   )
EXTRACT_ST( CAM_STATUS_TIMEOUT,        "Cam-Timeout"      )



