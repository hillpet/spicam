/*  (c) Copyright:  2017..2018  Patrn ESS, Confidential Data 
 *
 *  Workfile:           cmd_main.c
 *  Revision:          
 *  Modtime:           
 *
 *  Purpose:            Main command module
 *
 *
 *
 *  Compiler/Assembler: Raspbian Linux GNU gcc
 *  Ext Packages:
 *
 *  Author:             Peter Hillen
 *  Date Created:       09 Mar 2017
 *
 *  Revisions:
 *  Entry Points:       
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
**/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <fcntl.h>
#include <unistd.h>
#include <sched.h>
#include <time.h>
#include <errno.h>

#include <common.h>
#include "config.h"
#include "globals.h"
#include "cam_main.h"
#include "cam_page.h"
#include "cam_json.h"
#include "cmd_alarm.h"
#include "cmd_cad.h"
#include "cmd_exec.h"
#include "cmd_mail.h"
#include "gen_func.h"
#include "graphics.h"
//
#include "cmd_main.h"

//#define USE_PRINTF
#include <printf.h>

//
// Variables
//
static volatile bool fCmdRunning       = TRUE;
static GLOCMD       *pstCmd            = NULL;
//
static const char *pcMailListTo[] =
{
   "peter.hillen@ziggo.nl",
   NULL
};
//
static const char *pcMailListCc[] =
{
   "peter.hillen@gmail.com",
   "peter@patrn.nl",
   NULL
};
//
static const char *pcMailListBcc[] =
{
   NULL
};
//
static const char *pcMailMotionSymmary    =  RPI_MREP_PATH;
static const char *pcMailListSubjectSlim  =  "PatrnRpi Smart-E data";
static const char *pcMailListSubjectSnap  =  "Patrn Snapshot";
static const char *pcMailListSubjectWan   =  "Patrn WAN change";
static const char *pcMailListSubjectMdet  =  "Patrn Motion Detect summary";
static const char *pcMailListSubjectAlarm =  "Patrn Alarm list";
//    
static const char *pcBodySnap             =  "Notice:" CRLF
                                             "Camera triggered." CRLF CRLF
                                             "-Peter-" CRLF;
//    
static const char *pcBodyWanChanged       =  "Notice:" CRLF
                                             "WAN Address changed from %s to %s !" CRLF CRLF
                                             "-Peter-" CRLF;
//    
// Local prototypes
//
static pid_t   cmd_Deamon                 (void);
static void    cmd_Execute                (void);
static bool    cmd_RunCommand             (void);
static void    cmd_NotifyCam              (int, int);
//
static bool    cmd_SignalRegister         (sigset_t *);
static void    cmd_ReceiveSignalSegmnt    (int);
static void    cmd_ReceiveSignalInt       (int);
static void    cmd_ReceiveSignalTerm      (int);
static void    cmd_ReceiveSignalUser1     (int);
static void    cmd_ReceiveSignalUser2     (int);
//
static void    cmd_InitSemaphore          (void);
static void    cmd_ExitSemaphore          (void);
static void    cmd_PostSemaphore          (void);
static int     cmd_WaitSemaphore          (int);
//
static MAILCT  cmd_MailAlarms             (MAILDT, int, char **, int *);
static MAILCT  cmd_MailSlimData           (MAILDT, int, char **, int *);
static MAILCT  cmd_MailSnapData           (MAILDT, int, char **, int *);
static MAILCT  cmd_MailWanData            (MAILDT, int, char **, int *);
static MAILCT  cmd_MailMotionData         (MAILDT, int, char **, int *);
static bool    cmd_MailCopyBody           (char **, char *, int *);
//
static void    cmd_CheckAlarms            (void);
static bool    cmd_GetWan                 (void);
static bool    cmd_GetHostname            (char *);
static void    cmd_HandleGuard            (void);
//
static int     cmd_HandleNewAlarm         (void);
static int     cmd_HandleSnapshot         (void);
static int     cmd_HandleRecording        (void);
static int     cmd_HandleTimelapse        (void);
static int     cmd_HandleStreaming        (void);
static int     cmd_HandleMotionVectors    (void);
static int     cmd_HandleStop             (void);
//
static void    cmd_HandleSignalFromServer (void);
static void    cmd_HandleSignalFromCam    (void);
static void    cmd_HandleSignalFromHelper (void);
static void    cmd_HandleSignalFromAlarm  (void);
static void    cmd_HandleSignalFromProxy  (void);

/*------  Local functions separator ------------------------------------
__GLOBAL_FUNCTIONS_________(){};
----------------------------------------------------------------------------*/

// 
// Function:   CMD_Init
// Purpose:    Init the command thread
// 
// Parameters: 
// Returns:    Completion
// Note:
// 
int CMD_Init()
{
   int iCc=0;

   if( cmd_Deamon() > 0) iCc = GLOBAL_CMD_INI;

   //
   // The parent cam_main) will return here. The cmd_main deamon will 
   // exit upon completion.
   //
   return(iCc);
}

/*------  Local functions separator ------------------------------------
__LOCAL_FUNCTIONS_________(){};
----------------------------------------------------------------------------*/

//
// Function:   cmd_Deamon
// Purpose:    Instantiate the command thread 
//
// Parms:      
// Returns:    The pid of the new command thread
//
static pid_t cmd_Deamon(void)
{
   pid_t    tPid;

   tPid = fork();
   //
   switch(tPid)
   {
      default:
         // parent 
         GLOBAL_PidPut(PID_CMND, tPid);
         break;

      case 0:
         // child
         PRINTF("cmd_Deamon()" CRLF);
         cmd_Execute();
         _exit(0);
         break;

      case -1:
         // Error
         LOG_printf("cmd_Deamon(): Error!"CRLF);
         LOG_Report(errno, "CMD", "cmd_Deamon(): Error!");
         break;
   }
   return(tPid);
}

// 
// Function:   cmd_Execute 
// Purpose:    Run the command thread
// 
// Parameters:  
// Returns:  
// 
static void cmd_Execute(void)
{
   sigset_t tBlockset;

   if(cmd_SignalRegister(&tBlockset) == FALSE)  _exit(1);
   //
   cmd_InitSemaphore();
   //
   //LOG_Report(0, "CMD", "cmd_Execute():Init Start....");
   pstCmd = GLOBAL_CommandCopy();
   //
   CMD_InitMail();
   //
   pstMap->G_iNumAlarms = ALARM_CheckAlarms();
   //
   GLOBAL_PidSaveGuard(PID_CMND, GLOBAL_CMD_INI);
   GEN_Sleep(200);
   GLOBAL_HostNotification(GLOBAL_CMD_INI);
   //LOG_Report(0, "CMD", "cmd_Execute():Init Ready");
   //
   #ifdef FEATURE_MAIL_ALARM_LIST
   CMD_SendMail(cmd_MailAlarms);
   #endif   //FEATURE_MAIL_ALARM_LIST
   //
   while(fCmdRunning)
   {
      //
      // The cmd mainloop
      // While waiting for a command, a 100 msecs timeout will be used to handle:
      //    o Alarm timekeeping
      //    o Button scan
      //    o Check background processes
      //
      switch( cmd_WaitSemaphore(1000) )
      {
         case 0:
            if( GLOBAL_GetSignalNotification(PID_CMND, GLOBAL_SVR_RUN) ) cmd_HandleSignalFromServer();
            if( GLOBAL_GetSignalNotification(PID_CMND, GLOBAL_CAM_RUN) ) cmd_HandleSignalFromCam();
            if( GLOBAL_GetSignalNotification(PID_CMND, GLOBAL_HLP_RUN) ) cmd_HandleSignalFromHelper();
            if( GLOBAL_GetSignalNotification(PID_CMND, GLOBAL_ALM_RUN) ) cmd_HandleSignalFromAlarm();
            if( GLOBAL_GetSignalNotification(PID_CMND, GLOBAL_PRX_NFY) ) cmd_HandleSignalFromProxy();
            if( GLOBAL_GetSignalNotification(PID_CMND, GLOBAL_GRD_RUN) ) cmd_HandleGuard();
            break;

         default:
            break;

         case -1:
            // Error
            PRINTF("cmd_Execute(): Error cmd_WaitSemaphore"CRLF);
            LOG_Report(errno, "CMD", "cmd_Execute(): Error cmd_WaitSemaphore");
            fCmdRunning = FALSE;
            break;

         case 1:
            //
            // 1 Sec timeout: handle Alarms
            //
            GLOBAL_PidSetGuard(PID_CMND);
            cmd_CheckAlarms();
            break;
      }
   }
   GLOBAL_CommandDestroy(pstCmd);
   cmd_ExitSemaphore();
}

// 
// Function:   cmd_RunCommand
// Purpose:    Handle the HTTP-Server commands
// 
// Parameters: 
// Returns:    TRUE if OKee
// 
static bool cmd_RunCommand(void)
{
   bool     fCc=TRUE;
   int      iStatus;

   iStatus = pstCmd->iStatus;
   //
   switch(pstCmd->iCommand)
   {
      case CAM_CMD_IDLE:
         //
         // Receive NULL command from the CAM thread to ACK command sequence
         // We do not have to do anything here
         //
         PRINTF("cmd_HandleCmd(): CAM_CMD_IDLE" CRLF);
         cmd_NotifyCam(iStatus, GLOBAL_CMD_NFY);
         break;

      case CAM_CMD_FOTOS:
      case CAM_CMD_DEL_FOTOS:
      case CAM_CMD_VIDEOS:
      case CAM_CMD_DEL_VIDEOS:
      case CAM_CMD_PARMS:
      case CAM_CMD_DEFAULTS:
      case CAM_CMD_M_PARMS:
         //
         // Triggered by:
         // HTTP: "cam.json?Command=mparms&...."
         // Receive parms command to reply all parameters
         // We do not have to do anything here
         //
         PRINTF("cmd_HandleCmd(): CAM_CMD_xxx" CRLF);
         cmd_NotifyCam(iStatus, GLOBAL_CMD_NFY);
         break;

      case CAM_CMD_MAN_SNAP:
      case CAM_CMD_M_SNAP:
         //
         // Triggered by:
         // HTTP: "cam.json?Command=msnap&...."
         // Receive Snapshot command to take a picture
         // CMND thread spawns the actual apps and will WAIT HERE for its completion
         //
         PRINTF("cmd_HandleCmd(): CAM_CMD_MAN_SNAP" CRLF);
         iStatus = cmd_HandleSnapshot();
         //
         // Snapshot finished (good or bad): signal completion
         //
         cmd_NotifyCam(iStatus, GLOBAL_CMD_NFY);
         break;

      case CAM_CMD_MAN_RECORD:
         //
         // Receive Recording command to start recording
         // CMD thread spawns the actual apps indefinitely (CHECK MAX REC TIME !!)
         //
         PRINTF("cmd_HandleCmd(): CAM_CMD_MAN_RECORD" CRLF);
         iStatus = cmd_HandleRecording();
         cmd_NotifyCam(iStatus, GLOBAL_CMD_NFY);
         break;

      case CAM_CMD_ALM_RECORD:
         //
         // Receive Akarm Recording command to start recording
         // CMD thread spawns the actual apps indefinitely (CHECK MAX REC TIME !!)
         //
         PRINTF("cmd_HandleCmd(): CAM_CMD_ALM_RECORD" CRLF);
         iStatus = cmd_HandleRecording();
         cmd_NotifyCam(iStatus, GLOBAL_ALM_NFY);
         break;

      case CAM_CMD_TIMELAPSE:
         //
         // Receive Timelapse command to take a series of picture
         // CMD thread spawns the actual apps and awaits its completion
         //
         PRINTF("cmd_HandleCmd(): CAM_CMD_TIMELAPSE" CRLF);
         iStatus = cmd_HandleTimelapse();
         cmd_NotifyCam(iStatus, GLOBAL_CMD_NFY);
         break;

      case CAM_CMD_STREAM:
         //
         // Receive Streaming command to start streaming
         // CMD thread spawns the actual apps indefinitely (CHECK MAX REC TIME !!)
         //
         PRINTF("cmd_HandleCmd(): CAM_CMD_STREAM" CRLF);
         iStatus = cmd_HandleStreaming();
         cmd_NotifyCam(iStatus, GLOBAL_CMD_NFY);
         break;

      case CAM_CMD_M_DETECT:
      case CAM_CMD_M_NORM:
      case CAM_CMD_M_DELTA:
      case CAM_CMD_M_ZOOM:
         //
         // Triggered by:
         // HTTP: "cam.json?Command=m.....&...."
         //
         PRINTF("cmd_HandleCmd(): CAM_CMD_M_xxx" CRLF);
         cmd_NotifyCam(iStatus, GLOBAL_CMD_NFY);
         break;

      case CAM_CMD_M_VECTOR:
         //
         // Triggered by:
         // HTTP: "cam.json?Command=mvector&...."
         //
         PRINTF("cmd_HandleCmd(): CAM_CMD_M_VECTOR" CRLF);
         iStatus = cmd_HandleMotionVectors();
         cmd_NotifyCam(iStatus, GLOBAL_CMD_NFY);
         break;

      case CAM_CMD_ALM_MOTION:
         //
         // Triggered by:
         // Alarm: { "motion", alarm_AlarmMotionDetect }
         //
         PRINTF("cmd_HandleCmd(): CAM_CMD_ALM_MOTION" CRLF);
         iStatus = cmd_HandleMotionVectors();
         cmd_NotifyCam(iStatus, GLOBAL_ALM_NFY);
         break;

      case CAM_CMD_A_VECTOR:
         //
         // Triggered by:
         // Notification from PID_HELP to PID_CMND
         // This keeps one of the MOTION_ xx AUTO modes alive,
         // so do NOT signal this command back to the HTTP server!
         //
         PRINTF("cmd_HandleCmd(): CAM_CMD_A_VECTOR" CRLF);
         iStatus = cmd_HandleMotionVectors();
         break;

      case CAM_CMD_MAN_STOP:
         //pwjh 
         //pwjh // Test mail stuff:
         //PRINTF("cmd_HandleCmd():SEND TEST EMAIL" CRLF);
         //GLOBAL_Notify(PID_PROX, GLOBAL_EML_RUN, SIGUSR1);
         //pwjh //
         //pwjh 
         //CMD_SendMail(cmd_MailSnapData);
         //
         // Receive STOP command to stop background stuff.
         //
         PRINTF("cmd_HandleCmd(): CAM_CMD_MAN_STOP" CRLF);
         iStatus = cmd_HandleStop();
         cmd_NotifyCam(iStatus, GLOBAL_CMD_NFY);
         break;

      case CAM_CMD_ALM_STOP:
         //
         // Receive STOP command to stop background stuff.
         //
         PRINTF("cmd_HandleCmd(): CAM_CMD_ALM_STOP" CRLF);
         iStatus = cmd_HandleStop();
         cmd_NotifyCam(iStatus, GLOBAL_ALM_NFY);
         break;

      case CAM_CMD_ALM:
         //
         // Receive Alarm setting command
         //
         PRINTF("cmd_HandleCmd(): CAM_CMD_ALM" CRLF);
         iStatus = cmd_HandleNewAlarm();
         cmd_NotifyCam(iStatus, GLOBAL_CMD_NFY);
         break;

      case CAM_CMD_ALM_SNAP:
         //
         // Receive Alarm command to take a picture
         // CMND thread spawns the actual apps and will WAIT HERE for its completion
         //
         PRINTF("cmd_HandleCmd(): CAM_CMD_ALM_SNAP" CRLF);
         iStatus = cmd_HandleSnapshot();
         //
         // Snapshot finished (good or bad): signal completion
         //
         cmd_NotifyCam(iStatus, GLOBAL_ALM_NFY);
         if(pstCmd->iData1 & ALARM_EMAIL)
         {
            PRINTF("cmd_HandleCmd():Send pix by email" CRLF);
            CMD_SendMail(cmd_MailSnapData);
         }
         break;

      case CAM_CMD_ALM_WAN:
         PRINTF("cmd_HandleCmd(): CAM_CMD_ALM_WAN" CRLF);
         cmd_GetWan();
         if(GEN_STRCMP(pstMap->G_pcWanAddr, pstCmd->pcArgs) != 0)
         {
            //
            // WAN address changed: send us a mail !
            //
            CMD_SendMail(cmd_MailWanData);
         }
         cmd_NotifyCam(iStatus, GLOBAL_ALM_NFY);
         break;

      case CAM_CMD_ALM_MDET_MAIL:
         PRINTF("cmd_HandleCmd(): CAM_CMD_ALM_MDET_MAIL" CRLF);
         CMD_SendMail(cmd_MailMotionData);
         cmd_NotifyCam(iStatus, GLOBAL_ALM_NFY);
         break;

      default:
         PRINTF("cmd_HandleCmd(): Unknown command" CRLF);
         cmd_NotifyCam(CAM_STATUS_ERROR, GLOBAL_CMD_NFY);
         fCc = FALSE;
         break;
   }
   return(fCc);
}

// 
// Function:   cmd_NotifyCam
// Purpose:    Signal PID_CAMR thread using SIGUSR1
// 
// Parameters: Command completion status, notification 
// Returns:    
// Note:       Uses MMAP shared memory
//
static void cmd_NotifyCam(int iStatus, int iNotification)
{
   // DFD-0-8.1 (CMD Put stCmd)
   pstCmd->iStatus = iStatus;
   GLOBAL_CommandPut(pstCmd);
   GLOBAL_DUMPCOMMAND("cmd_NotifyCam():PID_CMND(Put):Command ready: report to PID_CAMR", pstCmd);
   //
   // DFD-0-8.2 (CMD Xmt SIGUSR1)
   //
   PRINTF5("cmd_NotifyCam():Notify to CAM:Url=%d, Cmd=%d, Status=%d, Args=0x%X, Data=%d" CRLF, 
               pstCmd->tUrl, pstCmd->iCommand, pstCmd->iStatus, pstCmd->iArgs, pstCmd->iData1);
   //
   GLOBAL_Notify(PID_CAMR, iNotification, SIGUSR1);
}

/*------  Local functions separator ------------------------------------
__CMD_HANDLERS(){};
----------------------------------------------------------------------------*/

// 
// Function:   cmd_HandleSignalFromServer
// Purpose:    Handle the signal from the HTTP Server
// 
// Parameters: 
// Returns:    
// 
static void cmd_HandleSignalFromServer()
{
   //
   // A mail parameter has changed: notify the owner (CMD thread)
   //
   PRINTF("cmd_HandleSignalFromServer()" CRLF);
   CMD_ParameterChanged(pstMap->G_iChangedParm);
}

// 
// Function:   cmd_HandleSignalFromCam
// Purpose:    Handle the signal from the Camera thread
// 
// Parameters: 
// Returns:    
// 
static void cmd_HandleSignalFromCam()
{
   // DFD-0-6.1 (CMD Get GLOBAL stCmd)
   // We are signalled by the CAM to do something
   //
   PRINTF("cmd_HandleSignalFromCam():Command from CAM" CRLF);
   GLOBAL_CommandGetAll(pstCmd);
   GLOBAL_DUMPCOMMAND("cmd_HandleSignalFromCam():PID_CMND(GetAll):Execute GLOBAL command from PID_CAMR", pstCmd);
   cmd_RunCommand();
}

// 
// Function:   cmd_HandleSignalFromHelper
// Purpose:    Handle the signal from the helper thread
// 
// Parameters: 
// Returns:    
// 
static void cmd_HandleSignalFromHelper()
{
   //
   // This is a signal from the Helper to respond to the LOCAL command structure
   //
   PRINTF("cmd_HandleSignalFromHelper():Run PID_HELP (CAM_CMD_A_VECTOR)" CRLF);
   //
   // ToDo:Pass new command to PID_CMND
   //
   pstCmd->iCommand = CAM_CMD_A_VECTOR;
   GLOBAL_DUMPCOMMAND("cmd_HandleSignalFromHelper():PID_CMND(ToDo):Execute LOCAL command from PID_HELP", pstCmd);
   cmd_RunCommand();
}

// 
// Function:   cmd_HandleSignalFromAlarm
// Purpose:    Handle the signal from the CMD alarm thread
// 
// Parameters: 
// Returns:    
// 
static void cmd_HandleSignalFromAlarm()
{
   //
   // This is a signal from the Alarm to respond to the LOCAL command structure
   //
   PRINTF("cmd_HandleSignalFromAlarm():Command from ALM" CRLF);
   GLOBAL_CommandGetAll(pstCmd);
   GLOBAL_DUMPCOMMAND("cmd_HandleSignalFromAlarm():PID_CMND(GetAll):Execute LOCAL command from ALARM", pstCmd);
   cmd_RunCommand();
}

// 
// Function:   cmd_HandleSignalFromProxy
// Purpose:    Handle the signal from the proxy server
// 
// Parameters: 
// Returns:    
// 
static void cmd_HandleSignalFromProxy()
{
   //
   // Send RPI#9 smart E-meter results
   //
   CMD_SendMail(cmd_MailSlimData);
}

/*------  Local functions separator ------------------------------------
__MAIL_FUNCTIONS(){};
----------------------------------------------------------------------------*/

// 
// Function:   cmd_MailAlarms
// Purpose:    Send mail with embedded Alarm list
// 
// Parameters: Datatype, index, mail data, mail data size
// Returns:    Content type (MAIL_CT_HTML, ..., MAIL_CT_NONE
// Note:       This is a callback from the mail collector, and it has to free
//             the allocated memory later !
// 
static MAILCT cmd_MailAlarms(MAILDT tDt, int iIdx, char **ppcData, int *piSize)
{
   MAILCT   tNewCt, tRetCt=MAIL_CT_NONE;
   char    *pcData=NULL;
   
   PRINTF2("cmd_MailAlarms():Idx=%d, DT=%d" CRLF, iIdx, tDt);
   //
   switch(tDt)
   {
      default:
         tNewCt = MAIL_CT_NONE;
         break;

      case MAIL_DT_FROM:
      case MAIL_DT_REPLY:
         if(iIdx == 0) pcData = pstMap->G_pcSmtpUser;
         tNewCt = MAIL_CT_TEXT;
         break;

      case MAIL_DT_TO:
         pcData = (char *)pcMailListTo[iIdx];
         tNewCt = MAIL_CT_TEXT;
         break;

      case MAIL_DT_CC:
         pcData = (char *)pcMailListCc[iIdx];
         tNewCt = MAIL_CT_TEXT;
         break;

      case MAIL_DT_BCC:
         pcData = (char *)pcMailListBcc[iIdx];
         tNewCt = MAIL_CT_TEXT;
         break;

      case MAIL_DT_SUBJECT:
         if(iIdx == 0) pcData = (char *)pcMailListSubjectAlarm;
         tNewCt = MAIL_CT_TEXT;
         break;

      case MAIL_DT_ATTM:
         tNewCt = MAIL_CT_NONE;
         break;

      case MAIL_DT_BODY:
         if(iIdx == 0) ALARM_ListAlarms(&pcData);
         tNewCt = MAIL_CT_TEXT;
         break;
   }
   if(pcData)
   {
      if(cmd_MailCopyBody(ppcData, pcData, piSize)) tRetCt = tNewCt;
   }
   return(tRetCt);
}

// 
// Function:   cmd_MailSlimData
// Purpose:    Send mail with embedded Smart-E graph
// 
// Parameters: Datatype, index, mail data, mail data size
// Returns:    Content type (MAIL_CT_HTML, ..., MAIL_CT_NONE
// Note:       This is a callback from the mail collector, and it has to free
//             the allocated memory later !
// 
static MAILCT cmd_MailSlimData(MAILDT tDt, int iIdx, char **ppcData, int *piSize)
{
   MAILCT   tNewCt, tRetCt=MAIL_CT_NONE;
   char    *pcData=NULL;
   
   PRINTF2("cmd_MailSlimData():Idx=%d, DT=%d" CRLF, iIdx, tDt);
   //
   switch(tDt)
   {
      default:
         tNewCt = MAIL_CT_NONE;
         break;

      case MAIL_DT_FROM:
      case MAIL_DT_REPLY:
         if(iIdx == 0) pcData = pstMap->G_pcSmtpUser;
         tNewCt = MAIL_CT_TEXT;
         break;

      case MAIL_DT_TO:
         pcData = (char *)pcMailListTo[iIdx];
         tNewCt = MAIL_CT_TEXT;
         break;

      case MAIL_DT_CC:
         pcData = (char *)pcMailListCc[iIdx];
         tNewCt = MAIL_CT_TEXT;
         break;

      case MAIL_DT_BCC:
         pcData = (char *)pcMailListBcc[iIdx];
         tNewCt = MAIL_CT_TEXT;
         break;

      case MAIL_DT_SUBJECT:
         if(iIdx == 0) pcData = (char *)pcMailListSubjectSlim;
         tNewCt = MAIL_CT_TEXT;
         break;

      case MAIL_DT_ATTM:
         tNewCt = MAIL_CT_NONE;
         break;

      case MAIL_DT_BODY:
         if(iIdx == 0) pcData = pstMap->G_pcLastFile;
         tNewCt = MAIL_CT_HTML;
         break;
   }
   if(pcData)
   {
      if(cmd_MailCopyBody(ppcData, pcData, piSize)) tRetCt = tNewCt;
   }
   return(tRetCt);
}

// 
// Function:   cmd_MailSnapData
// Purpose:    Send mail with embedded snapshot
// 
// Parameters: Datatype, index, mail data, mail data size
// Returns:    Content type (MAIL_CT_HTML, ..., MAIL_CT_NONE
// Note:       This is a callback from the mail collector, and it has to free
//             the allocated memory later !
// 
static MAILCT cmd_MailSnapData(MAILDT tDt, int iIdx, char **ppcData, int *piSize)
{
   MAILCT   tNewCt, tRetCt=MAIL_CT_NONE;
   char    *pcData=NULL;
   
   PRINTF2("cmd_MailSnapData():Idx=%d, DT=%d" CRLF, iIdx, tDt);
   //
   switch(tDt)
   {
      default:
         tNewCt = MAIL_CT_NONE;
         break;

      case MAIL_DT_FROM:
      case MAIL_DT_REPLY:
         if(iIdx == 0) pcData = pstMap->G_pcSmtpUser;
         tNewCt = MAIL_CT_TEXT;
         break;

      case MAIL_DT_TO:
         pcData = (char *)pcMailListTo[iIdx];
         tNewCt = MAIL_CT_TEXT;
         break;

      case MAIL_DT_CC:
         pcData = (char *)pcMailListCc[iIdx];
         tNewCt = MAIL_CT_TEXT;
         break;

      case MAIL_DT_BCC:
         pcData = (char *)pcMailListBcc[iIdx];
         tNewCt = MAIL_CT_TEXT;
         break;

      case MAIL_DT_SUBJECT:
         if(iIdx == 0) pcData = (char *)pcMailListSubjectSnap;
         tNewCt = MAIL_CT_TEXT;
         break;

      case MAIL_DT_ATTM:
         if(iIdx == 0) pcData = pstMap->G_pcLastFile;
         tNewCt = MAIL_CT_ATTM;
         break;

      case MAIL_DT_BODY:
         if(iIdx == 0) pcData = (char *)pcBodySnap;
         tNewCt = MAIL_CT_TEXT;
         break;
   }
   if(pcData)
   {
      if(cmd_MailCopyBody(ppcData, pcData, piSize)) tRetCt = tNewCt;
   }
   return(tRetCt);
}

// 
// Function:   cmd_MailWanData
// Purpose:    Send mail with WAN changes
// 
// Parameters: Datatype, index, mail data, mail data size
// Returns:    Content type (MAIL_CT_HTML, ..., MAIL_CT_NONE
// Note:       This is a callback from the mail collector, and it has to free
//             the allocated memory later !
// 
static MAILCT cmd_MailWanData(MAILDT tDt, int iIdx, char **ppcData, int *piSize)
{
   MAILCT   tNewCt, tRetCt=MAIL_CT_NONE;
   char    *pcData=NULL;
   
   PRINTF2("cmd_MailWanData():Idx=%d, DT=%d" CRLF, iIdx, tDt);
   //
   switch(tDt)
   {
      default:
         tNewCt = MAIL_CT_NONE;
         break;

      case MAIL_DT_FROM:
      case MAIL_DT_REPLY:
         if(iIdx == 0) pcData = pstMap->G_pcSmtpUser;
         tNewCt = MAIL_CT_TEXT;
         break;

      case MAIL_DT_TO:
         pcData = (char *)pcMailListTo[iIdx];
         tNewCt = MAIL_CT_TEXT;
         break;

      case MAIL_DT_CC:
         pcData = (char *)pcMailListCc[iIdx];
         tNewCt = MAIL_CT_TEXT;
         break;

      case MAIL_DT_BCC:
         pcData = (char *)pcMailListBcc[iIdx];
         tNewCt = MAIL_CT_TEXT;
         break;

      case MAIL_DT_SUBJECT:
         if(iIdx == 0) pcData = (char *)pcMailListSubjectWan;
         tNewCt = MAIL_CT_TEXT;
         break;

      case MAIL_DT_ATTM:
         tNewCt = MAIL_CT_NONE;
         break;

      case MAIL_DT_BODY:
         if(iIdx == 0)
         {
            pcData = safemalloc(100);
            GEN_SNPRINTF(pcData, 100, pcBodyWanChanged, pstCmd->pcArgs, pstMap->G_pcWanAddr);
            if(cmd_MailCopyBody(ppcData, pcData, piSize)) tRetCt = MAIL_CT_TEXT;
            pcData = safefree(pcData);
         }
         break;
   }
   if(pcData)
   {
      if(cmd_MailCopyBody(ppcData, pcData, piSize)) tRetCt = tNewCt;
   }
   return(tRetCt);
}

// 
// Function:   cmd_MailMotionData
// Purpose:    Send mail with Motion Detect summary
// 
// Parameters: Datatype, index, mail data, mail data size
// Returns:    Content type (MAIL_CT_HTML, ..., MAIL_CT_NONE
// Note:       This is a callback from the mail collector, and it has to free
//             the allocated memory later !
// 
static MAILCT cmd_MailMotionData(MAILDT tDt, int iIdx, char **ppcData, int *piSize)
{
   MAILCT   tNewCt, tRetCt=MAIL_CT_NONE;
   char    *pcData=NULL;
   
   PRINTF2("cmd-MailMotionData():Idx=%d, DT=%d" CRLF, iIdx, tDt);
   //
   switch(tDt)
   {
      default:
         tNewCt = MAIL_CT_NONE;
         break;

      case MAIL_DT_FROM:
      case MAIL_DT_REPLY:
         if(iIdx == 0) pcData = pstMap->G_pcSmtpUser;
         tNewCt = MAIL_CT_TEXT;
         break;

      case MAIL_DT_TO:
         pcData = (char *)pcMailListTo[iIdx];
         tNewCt = MAIL_CT_TEXT;
         break;

      case MAIL_DT_CC:
         pcData = (char *)pcMailListCc[iIdx];
         tNewCt = MAIL_CT_TEXT;
         break;

      case MAIL_DT_BCC:
         pcData = (char *)pcMailListBcc[iIdx];
         tNewCt = MAIL_CT_TEXT;
         break;

      case MAIL_DT_SUBJECT:
         if(iIdx == 0) pcData = (char *)pcMailListSubjectMdet;
         tNewCt = MAIL_CT_TEXT;
         break;

      case MAIL_DT_ATTM:
         tNewCt = MAIL_CT_NONE;
         break;

      case MAIL_DT_BODY:
         if(iIdx == 0) pcData = (char *)pcMailMotionSymmary;
         tNewCt = MAIL_CT_FILE;
         break;
   }
   if(pcData)
   {
      if(cmd_MailCopyBody(ppcData, pcData, piSize)) tRetCt = tNewCt;
   }
   return(tRetCt);
}

//
// Function:   cmd_MailCopyBody
// Purpose:    Copy mail body to mail handler
//
// Parms:      Data ptr, size ptr
// Returns:    TRUE if OKee
// Note:       
//
static bool cmd_MailCopyBody(char **ppcData, char *pcData, int *piSize)
{
   bool  fCc=FALSE;
   int   iSize;

   if(pcData && ppcData && piSize)
   {
      iSize    = GEN_STRLEN(pcData);
      *ppcData = safemalloc(iSize+1);
      *piSize  = iSize;
      //
      // Copy data to destination
      //
      GEN_STRCPY(*ppcData, pcData);
      fCc = TRUE;
   }
   return(fCc);
}

/*------  Local functions separator ------------------------------------
__SUPPORT_FUNCTIONS(){};
----------------------------------------------------------------------------*/

//
// Function:   cmd_HandleNewAlarm
// Purpose:    Handle the HTTP-Server alarm command
//
// Parms:      Command: Add alarm to the list
// Returns:    CAM_STATUS_xxx
// Note:       G_Delete marks the deletion of a timer rather that adding one !
//                http:\\...\cam?alarm&acmd=xxxx&adat=201309*&delete=yes
//
static int cmd_HandleNewAlarm()
{
   ALARM   *pstNew;
   int      iStatus=CAM_STATUS_ERROR;

   PRINTF2("cmd_HandleNewAlarm():Cmd=%s, Time=%s" CRLF, pstMap->G_pcAlarmCommand, pstMap->G_pcAlarmDandT);
   pstNew = ALARM_FindDuplicate(pstMap->G_pcAlarmDandT, pstMap->G_pcAlarmCommand);
   if(GEN_CheckDelete())
   {
      PRINTF2("cmd_HandleNewAlarm():Cmd=%s, Time=%s **DELETE**" CRLF, pstMap->G_pcAlarmCommand, pstMap->G_pcAlarmDandT);
      //
      // Delete this entry
      //
      if(pstNew) 
      {
         ALARM_DeleteAlarm(pstNew);
      }
      ALARM_ListAlarms(NULL);
      return(CAM_STATUS_IDLE);
   }
   //
   // Add this entry, if it is unique, else copy new settings
   //
   if(pstNew == NULL) 
   {
      //
      // Try to add the alarm
      //
      pstNew = ALARM_AddAlarm();
   }
   //
   // Add/Modify this alarm to the timer list
   //
   if(pstNew)
   {
      // 
      // Still free alarms: use it
      //
      GEN_STRNCPY(pstNew->pcCommand, pstMap->G_pcAlarmCommand, MAX_PARM_LEN);
      GEN_STRNCPY(pstNew->pcDandT,   pstMap->G_pcAlarmDandT,   MAX_PARM_LEN);
      //
      pstNew->iRepTime    = atoi(pstMap->G_pcAlarmRep);
      pstNew->iRepNum     = atoi(pstMap->G_pcAlarmNum);
      pstNew->ulRepNext   = 0;
      pstNew->iRepCount   = 0;
      pstNew->iFlags      = ALARM_ARMED|ALARM_USED;
      iStatus             = CAM_STATUS_PARMS;
      //
      pstMap->G_iNumAlarms++;
   }
   else
   {
      LOG_Report(0, "CMD", "cmd_HandleNewAlarm(): No more Alarm timers!");
   }
   //
   return(iStatus);
}

//
//  Function:   cmd_HandleSnapshot
//  Purpose:    Handle the HTTP-Server snapshot command
//
//  Parms:      Command
//  Returns:    CAM_STATUS_xxx
//
static int cmd_HandleSnapshot()
{
   int      iCc;
   int      iStatus=CAM_STATUS_ERROR;
   pid_t    tPid;
   
   //
   // Run execution
   //
   if(GEN_STRLEN(pstCmd->pcExec))
   {
      LCD_MODE(LCD_MODE_TEXT);
      LCD_BACKLIGHT(100);
      if( CMD_BashProcess(pstCmd) )
      {
         //
         // CMD thread awaits EXEC thread completion !
         //
         PRINTF2("cmd_HandleSnapshot():Waiting to complete: system(%s %s)" CRLF, pstCmd->pcExec, pstCmd->pcArgs);
         //
         while( ((tPid = waitpid(GLOBAL_PidGet(PID_HELP), &iCc, 0)) == -1) && (errno == EINTR) )
             continue;
         //
         //LOG_Report(0, "CMD", "cmd_HandleSnapshot(): HELPER(%d) cc=%d", GLOBAL_PidGet(PID_HELP), iCc);
         PRINTF2("cmd_HandleSnapshot():EXEC=%d, cc=%d" CRLF, tPid, iCc);
         //
         if(tPid == -1)
         {
            LOG_Report(errno, "CMD", "Exec process %s %s ERROR", pstCmd->pcExec, pstCmd->pcArgs);
            PRINTF1("cmd_HandleSnapshot():HELPER=%d Error" CRLF, GLOBAL_PidGet(PID_HELP));
         }
         else
         {
            GLOBAL_PidPut(PID_HELP, 0);
            iStatus = CAM_STATUS_SNAPSHOT;
         }
      }
   }
   else
   {
      PRINTF("cmd_HandleSnapshot():BAD System call !!" CRLF);
      GLOBAL_DUMPVARS("cmd_HandleSnapshot");
      GLOBAL_DUMPCOMMAND("cmd_HandleSnapshot(error):PID_CMND(dump)", pstCmd);
   }
   return(iStatus);
}

//
//  Function:   cmd_HandleRecording
//  Purpose:    Handle the HTTP-Server recording command
//
//  Parms:      Command
// Returns:    CAM_STATUS_xxx
//
static int cmd_HandleRecording()
{
   int   iStatus=CAM_STATUS_ERROR;

   //
   // Run execution, if any
   //
   if(GEN_STRLEN(pstCmd->pcExec))
   {
      PRINTF2("cmd_HandleRecording(): EXEC(%s) ARGS(%s)" CRLF, pstCmd->pcExec, pstCmd->pcArgs);
      //
      // Recording starts in the background: keep monitoring
      //
      if( CMD_ExecProcess(pstCmd) )
      {
         //LOG_Report(0, "CMD", "cmd_HandleRecording():HELPER(%d) FB(%d)", GLOBAL_PidGet(PID_HELP), GLOBAL_PidGet(PID_FBUF));
         iStatus = CAM_STATUS_RECORDING;
         LCD_MODE(LCD_MODE_FB);
         LCD_BACKLIGHT(100);
      }
   }
   else
   {
      PRINTF("cmd_HandleRecording():BAD System call !!" CRLF);
   }
   return(iStatus);
}

//
// Function:   cmd_HandleTimelapse
// Purpose:    Handle the HTTP-Server timelapse command
//
// Parms:      Command
// Returns:    CAM_STATUS_xxx
//
static int cmd_HandleTimelapse()
{
   int      iStatus=CAM_STATUS_ERROR;

   //
   // Run execution, if any
   //
   if(GEN_STRLEN(pstCmd->pcExec))
   {
      //
      // Timelapse starts in the background: keep monitoring
      //
      if( CMD_ExecProcess(pstCmd) )
      {
         //LOG_Report(0, "CMD", "cmd_HandleTimelapse():HELPER(%d)", GLOBAL_PidGet(PID_HELP));
         iStatus = CAM_STATUS_TIMELAPSE;
      }
   }
   else
   {
      PRINTF("cmd_HandleTimelapse():BAD System call !!" CRLF);
   }
   return(iStatus);
}

//
//  Function:   cmd_HandleStreaming
//  Purpose:    Handle the HTTP-Server streaming command
//
//  Parms:      Command
// Returns:    CAM_STATUS_xxx
//
static int cmd_HandleStreaming()
{
   int      iStatus=CAM_STATUS_ERROR;

   //
   // Run execution, if any
   //
   if(GEN_STRLEN(pstCmd->pcExec))
   {
      //
      // Instantiate new thread for streaming video to socket (using vlc)
      // Create a new pipeline to connect raspivid (PID_HELP) to vlc (tStrmPid)
      // Streaming starts in the background: keep monitoring
      //===================================================================
      // Note: if FEATURE_HEXDUMP_VLC then the streaming video from
      //       raspivid goes to the hexdump utility, which will read 
      //       binary data from stdin and rework it to readable ASCII
      //       to stdout.
      //===================================================================
      if( CMD_ExecProcess(pstCmd) )
      {
         //LOG_Report(0, "CMD", "cmd_HandleStreaming():HELPER(%d)", GLOBAL_PidGet(PID_HELP));
         iStatus = CAM_STATUS_STREAMING;
         LCD_MODE(LCD_MODE_FB);
         LCD_BACKLIGHT(100);
      }
   }
   else
   {
      LOG_Report(0, "CMD", "cmd_HandleStreaming():No executable !");
      PRINTF("cmd_HandleStreaming():BAD System call !!" CRLF);
   }
   return(iStatus);
}

//
// Function:   cmd_HandleMotionVectors
// Purpose:    Handle the HTTP-Server motion vector command
//
// Parms:      Command
// Returns:    CAM_STATUS_xxx
//
static int cmd_HandleMotionVectors()
{
   int      iStatus=CAM_STATUS_ERROR;
   MDET    *pstMdet=&pstMap->G_stMotionDetect;

   switch(pstMdet->iMotMode)
   {
      case MOTION_MODE_MANUAL:
      case MOTION_MODE_AUGMENT:
      case MOTION_MODE_PICTURE:
      case MOTION_MODE_VIDEO:
      case MOTION_MODE_STREAM:
      case MOTION_MODE_CYCLE:
         PRINTF("cmd_HandleMotionVectors():Run PID_HELP" CRLF);
         //
         // Instantiate new thread for recording video and enabling the motion vector
         // data to be streamed to a pipe.
         // Create a new pipeline to connect raspivid (PID_HELP) to the motion handler
         //
         if( CMD_ExecProcess(pstCmd) )
         {
            iStatus = CAM_STATUS_MOTION;
            LCD_MODE(LCD_MODE_FB);
            LCD_BACKLIGHT(100);
         }
         break;

      default:
      case MOTION_MODE_OFF:
         PRINTF("cmd_HandleMotionVectors():Turn OFF!" CRLF);
         //
         GEN_KillProcess(GLOBAL_PidGet(PID_UTIL), DO_FRIENDLY, KILL_TIMEOUT_FRIENDLY);
         GEN_WaitProcess(GLOBAL_PidGet(PID_UTIL));
         GLOBAL_PidPut(PID_UTIL, 0);
         //
         GEN_KillProcess(GLOBAL_PidGet(PID_HELP), DO_FRIENDLY, KILL_TIMEOUT_FRIENDLY);
         GEN_WaitProcess(GLOBAL_PidGet(PID_HELP));
         GLOBAL_PidPut(PID_HELP, 0);
         iStatus = CAM_STATUS_STOPPED;
         break;
   }
   return(iStatus);
}

//
// Function:   cmd_HandleStop
// Purpose:    Handle the HTTP-Server STOP command
//
// Parms:      Command
// Returns:    CAM_STATUS_xxx
// Note:       
//
static int cmd_HandleStop()
{
   LCD_MODE(LCD_MODE_TEXT);
   //
   // Kill all helper threads
   //
   GEN_KillProcess(GLOBAL_PidGet(PID_UTIL), NO_FRIENDLY, KILL_TIMEOUT_FORCED);
   GEN_WaitProcess(GLOBAL_PidGet(PID_UTIL));
   //LOG_Report(0, "CMD", "cmd_HandleStop(): Util-Thread (%d) killed", GLOBAL_PidGet(PID_UTIL));
   GLOBAL_PidPut(PID_UTIL, 0);
   //
   GEN_KillProcess(GLOBAL_PidGet(PID_HELP), NO_FRIENDLY, KILL_TIMEOUT_FORCED);
   GEN_WaitProcess(GLOBAL_PidGet(PID_HELP));
   //LOG_Report(0, "CMD", "cmd_HandleStop(): Help-Thread (%d) killed", GLOBAL_PidGet(PID_HELP));
   GLOBAL_PidPut(PID_HELP, 0);
   //
   return(CAM_STATUS_STOPPED);
}

/*------  Local functions separator -----------------------------------------
__MISC_FUNCTIONS_________(){};
----------------------------------------------------------------------------*/

// 
// Function:   cmd_CheckAlarms
// Purpose:    One second Alarm check
// 
// Parameters: 
// Returns:    
// 
static void cmd_CheckAlarms()
{
   GLOBAL_UpdateSecs();
   //===========================================================
   // Timer event: the 1 sec alarm timed out
   //===========================================================
   ALARM_HandleAlarms();
   //
   // Also, check if the helper thread has exited for whatever reason. 
   // If so, kill the utility thread as well
   //
   if(GLOBAL_PidGet(PID_HELP))
   {
      if( GEN_CheckBgndTerminated("Help", GLOBAL_PidGet(PID_HELP)) )
      {
         //
         // Helper has terminated by itself: report
         //
         GLOBAL_PidPut(PID_HELP, 0);
         //
         // Make sure the Utility helper is also terminated
         //
         if( !GEN_CheckBgndTerminated("Util", GLOBAL_PidGet(PID_UTIL)) )
         {
            GEN_KillProcess(GLOBAL_PidGet(PID_UTIL), NO_FRIENDLY, KILL_TIMEOUT_FORCED);
         }
         GLOBAL_PidPut(PID_UTIL, 0);
         //
         cmd_NotifyCam(CAM_STATUS_STOPPED, GLOBAL_CMD_NFY);
         LOG_Report(0, "CMD", "cmd_CheckAlarms():Help thread has stopped unexpectedly!");
         PRINTF("cmd_CheckAlarms():Help thread has stopped unexpectedly!" CRLF);
      }
   }
}

//
// Function:   cmd_GetWan
// Purpose:    Retrieve  the WAN address
//
// Parms:      
// Returns:    
// Note:       
//
static bool cmd_GetWan(void)
{
   bool        fFound;
   const char *pcName = "ipecho.net/plain";
   const char *pcAny  = "";

   if( (fFound = HTTP_GetFromNetwork((char *)pcName, (char *)pcAny, pstMap->G_pcWanAddr, MAX_ADDR_LEN, 20)) )
   {
      PRINTF1("cmd_GetWan():WAN IP-address=%s" CRLF, pstMap->G_pcWanAddr);
   }
   else
   {
      PRINTF("cmd_GetWan():WAN IP-address not resolved" CRLF);
   }
   return(fFound);
}

//
// Function:   cmd_GetHostname
// Purpose:    Retrieve peer hostname
//
// Parms:      Peer IP address
// Returns:    TRUE if found
// Note:       
//
static bool cmd_GetHostname(char *pcIp)
{
   bool        fFound;
   char       *pcName;
   char       *pcData;
   const char *pcUrl  = "%s/parms.json";
   const char *pcHost = "Hostname";

   pcName = safemalloc(NAME_LEN+1);
   pcData = safemalloc(NAME_LEN+1);
   //
   GEN_SNPRINTF(pcName, NAME_LEN, pcUrl, pcIp);
   //
   if( (fFound = HTTP_GetFromNetwork((char *)pcName, (char *)pcHost, pcData, NAME_LEN, 20)) )
   {
      PRINTF2("cmd_GetHostname():Hostname of %s=%s" CRLF, pcIp, pcData);
   }
   else
   {
      PRINTF("cmd_GetHostname():Hostname not resolved" CRLF);
   }
   safefree(pcData);
   safefree(pcName);
   return(fFound);
}

// 
// Function:   cmd_HandleGuard
// Purpose:    Respond to the guard query
// 
// Parameters: 
// Returns:    
// Note:       
//
static void cmd_HandleGuard(void)
{
   //LOG_Report(0, "CMD", "cmd_HandleGuard():Host-notification send");
   GLOBAL_HostNotification(GLOBAL_GRD_NFY);
}

/*------  Local functions separator ------------------------------------
__SIGNAL_FUNCTIONS(){};
----------------------------------------------------------------------------*/

// 
// Function:   cmd_SignalRegister
// Purpose:    Register all SIGxxx
// 
// Parameters: Blockset
// Returns:    TRUE if delivered
// 
static bool cmd_SignalRegister(sigset_t *ptBlockset)
{
  bool fCC = TRUE;

  // SIGSEGV:
  if( signal(SIGSEGV, &cmd_ReceiveSignalSegmnt) == SIG_ERR)
  {
     LOG_Report(errno, "CMD", "cmd_SignalRegister(): SIGSEGV ERROR");
     fCC = FALSE;
  }
  // SIGUSR1:
  if( signal(SIGUSR1, &cmd_ReceiveSignalUser1) == SIG_ERR)
  {
     LOG_Report(errno, "CMD", "cmd_SignalRegister(): SIGUSR1 ERROR");
     fCC = FALSE;
  }
  // SIGUSR2:
  if( signal(SIGUSR2, &cmd_ReceiveSignalUser2) == SIG_ERR)
  {
     LOG_Report(errno, "CMD", "cmd_SignalRegister(): SIGUSR2 ERROR");
     fCC = FALSE;
  }
  // CTL-X handler
  if( signal(SIGTERM, &cmd_ReceiveSignalTerm) == SIG_ERR)
  {
     LOG_Report(errno, "CMD", "cmd_SignalRegister(): SIGTERM ERROR");
     fCC = FALSE;
  }
  // Ctl-C handler
  if( signal(SIGINT, &cmd_ReceiveSignalInt) == SIG_ERR)
  {
     LOG_Report(errno, "CMD", "cmd_SignalRegister(): SIGINT ERROR");
     fCC = FALSE;
  }

  if(fCC)
  {
     //
     // Setup the SIGxxx events
     //
     sigemptyset(ptBlockset);
     sigaddset(ptBlockset, SIGSEGV);
     sigaddset(ptBlockset, SIGTERM);
     sigaddset(ptBlockset, SIGINT);
     sigaddset(ptBlockset, SIGUSR1);
     sigaddset(ptBlockset, SIGUSR2);
  }
  return(fCC);
}

//
// Function:   cmd_ReceiveSignalSegmnt
// Purpose:    Handle the SIGSEGV
//
// Parms:
// Returns:    TRUE if delivered
// Note:       Do NOT use LOG_xxx() here
//
static void cmd_ReceiveSignalSegmnt(int iSignal)
{
   if(fCmdRunning)
   {
      fCmdRunning = FALSE;
      //
      GEN_SegmentationFault(__FILE__, __LINE__);
      LOG_SegmentationFault(__FILE__, __LINE__);
      //
      // (TRY TO) Cleanup background threads:
      //
      GLOBAL_Notify(PID_HOST, GLOBAL_FLT_NFY, SIGTERM);
   }
   cmd_PostSemaphore();
}

// 
// Function:   cmd_ReceiveSignalTerm
// Purpose:    Add the SIGTERM command in the buffer (Ctl-X)
// 
// Parameters: 
// Returns:    TRUE if delivered
// 
static void cmd_ReceiveSignalTerm(int iSignal)
{
   LOG_Report(0, "CMD", "cmd_ReceiveSignalTerm()");
   fCmdRunning = FALSE;
   cmd_PostSemaphore();
}

//
// Function:   cmd_ReceiveSignalInt
// Purpose:    System callback for SIGINT (Ctl-C)
//
// Parms:
// Returns:    TRUE if delivered
//
static void cmd_ReceiveSignalInt(int iSignal)
{
   LOG_Report(0, "CMD", "cmd_ReceiveSignalInt()");
   fCmdRunning = FALSE;
   cmd_PostSemaphore();
}

//
// Function:   cmd_ReceiveSignalUser1
// Purpose:    System callback for SIGUSR1 signal
//
// Parms:
// Returns:    
// Note:       The camera thread will send us a SIGUSR1 to
//             signal a new command
//
static void cmd_ReceiveSignalUser1(int iSignal)
{
   // DFD-0-5.1 (CMD Rcv SIGUSR1)
   //PRINTF("cmd_ReceiveSignalUser1()" CRLF);
   cmd_PostSemaphore();
}

//
// Function:   cmd _ReceiveSignalUser2
// Purpose:    System callback for SIGUSR2 signal
//
// Parms:
// Returns:    
// Note:       Not used
//
static void cmd_ReceiveSignalUser2(int iSignal)
{
}

/*------  Local functions separator ------------------------------------
__SEMAPHORE_FUNCTIONS_________(){};
----------------------------------------------------------------------------*/

//
// Function:   cmd_InitSemaphore
// Purpose:    Init the Semaphore
//
// Parms:      
// Returns:    
// Note:       If pshared has the value 0, then the semaphore is shared between the
//             threads of a process, and should be located at some address that is
//             visible to all threads (e.g., a global variable, or a variable
//             allocated dynamically on the heap).
//             If pshared is nonzero, then the semaphore is shared between
//             processes, and should be located in a region of shared memory.
//
//             The sem value is always >= 0:
//                               0 means LOCKED
//                              >0 means UNLOCKED
//             Init(sem 1, VAL)  VAL=1 unlocked
//                               VAL=0 locked
//             Wait(sem)         LOCK:    if(VAL> 0) { VAL--; return;         } 
//                                        if(VAL==0) { Wait till > 0; return; }
//             Post(sem)         UNLOCK:  VAL++
//
static void cmd_InitSemaphore(void)
{
   //
   // Init the semaphore as LOCKED
   //
   if(sem_init(&pstMap->G_tSemCmd, 1, 0) == -1)
   {
      LOG_Report(errno, "CMD", "cmd_InitSemaphore(): sem_init Error");
   }
}

//
// Function:   cmd_ExitSemaphore
// Purpose:    Exit the Semaphore
//
// Parms:      
// Returns:    
// Note:       
//
static void cmd_ExitSemaphore(void)
{
   if( sem_destroy(&pstMap->G_tSemCmd) == -1) LOG_Report(errno, "CMD", "cmd_ExitSemaphore():sem_destroy");
}

//
// Function:   cad_PostSemaphore
// Purpose:    Unlock the screen Semaphore
//
// Parms:      
// Returns:    
// Note:       sem_post() increments (unlocks) the semaphore pointed to by sem. If the semaphore's 
//             value consequently becomes greater than zero, then another process or thread blocked
//             in a sem_wait(3) call will be woken up and proceed to lock the semaphore. 
//
static void cmd_PostSemaphore()
{
   int      iCc;

   //
   // sem++ (UNLOCK)   
   //
   iCc = sem_post(&pstMap->G_tSemCmd);
   if(iCc == -1) LOG_Report(errno, "CMD", "cmd_PostSemaphore(): sem_post error");
}

//
// Function:   cmd_WaitSemaphore
// Purpose:    Wait till Semaphore has been unlocked, then LOCK and proceed
//
// Parms:      TIMEOUT (or -1 if wait indefinitely)
// Returns:     0 if sem unlocked
//             +1 if sem timeout
//             -1 on error (errno)
// Note:       sem_wait() tries to decrement (lock) the semaphore. 
//             If the semaphore currently has the value zero, then the call blocks until 
//             either it becomes possible to perform the decrement (the semaphore value rises 
//             above zero) or a signal handler interrupts the call. 
//             If the semaphore's value is greater than zero, then the decrement proceeds, 
//             and the function returns immediately. 
//
//             sem_timedwait() is the same as sem_wait(), except that TIMEOUT specifies a 
//             limit on the amount of time that the call should block if the decrement cannot 
//             be immediately performed. The stTime points to a structure that
//             specifies an absolute timeout in seconds and nanoseconds since the Epoch, 
//             1970-01-01 00:00:00 +0000 (UTC). This structure is defined as follows:
//
//             struct timespec 
//             {
//                time_t tv_sec;      /* Seconds */
//                long   tv_nsec;     /* Nanoseconds [0 .. 999999999] */
//             };
//             If the timeout has already expired by the time of the call, and the semaphore 
//             could not be locked immediately, then sem_timedwait() fails with a timeout error 
//             (errno set to ETIMEDOUT). If the operation can be performed immediately, then 
//             sem_timedwait() never fails with a timeout error, regardless of the value of 
//             TIMEOUT.
//
static int cmd_WaitSemaphore(int iMsecs)
{
   //
   // if    sem == 0 : wait
   // else  sem-- (LOCK)   
   //
   return(GEN_WaitSemaphore(&pstMap->G_tSemCmd, iMsecs));
}

