/*  (c) Copyright:  2014  Patrn.nl, Confidential Data
**
**  $Workfile:          rpircu.h
**  $Revision:   
**  $Modtime:    
**
**  Purpose:            rpicad header file
**
 *  Compiler/Assembler: Raspbian Linux
 *  Ext Packages:
**
 *  Author:             Peter Hillen
 *  Date Created:       20 Feb 2014
**
 *  Revisions:
 *    $Log:   $
 *
 *
**/

#ifndef _RPIRCU_H_
#define _RPIRCU_H_

typedef struct RCULUT
{
   const char *pcIrKey;
   int         iRcuKey;
}  RCULUT;


//
// Global prototypes
//
int      RCU_Init          (void);
void     RCU_Exit          (void);

#endif  /*_RPIRCU_H_ */

