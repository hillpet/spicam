/*  (c) Copyright:  2017  Patrn, Confidential Data
 *
 *  Workfile:           rpi_proxy.c
 *  Revision:
 *  Modtime:
 *
 *  Purpose:            Handling dynamic HTTP proxy for a Smart-E-meter
 *
 *  Compiler/Assembler: Raspbian Linux GNU gcc
 *  Ext Packages:
 *
 *  Author:             Peter Hillen
 *  Date Created:       31 Okt 2017
 *
 *  Note:
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 **/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <fcntl.h>
#include <sched.h>
#include <errno.h>
#include <time.h>
#include <termios.h>
#include <unistd.h>
//
#include <common.h>
#include "config.h"
#include "globals.h"
#include "gen_http.h"
#include "gen_func.h"
#include "rpi_page.h"
#include "rpi_proxy.h"

//#define USE_PRINTF
#include <printf.h>

//
// Local prototypes
//
static pid_t proxy_Deamon                 (void);
static void  proxy_Execute                (void);
//
static bool  proxy_DynPageSmartMeter      (NETCL *, int);
//
static bool  proxy_HandleCommandRequest   (void);
static bool  proxy_HandleEmailRequest     (void);
static bool  proxy_HandleServerRequest    (void);
//
static void  proxy_InitDynamicPages       (int);
static int   proxy_SendRequest            (char *);
static int   proxy_SendRemoteIo           (char *);
static void  proxy_HandleGuard            (void);
//
static bool  proxy_SignalRegister         (sigset_t *);
static void  proxy_ReceiveSignalInt       (int);
static void  proxy_ReceiveSignalUser1     (int);
static void  proxy_ReceiveSignalUser2     (int);
static void  proxy_ReceiveSignalSegmnt    (int);
static void  proxy_ReceiveSignalTerm      (int);
static void  proxy_SignalHttpServer       (void);
static void  proxy_InitSemaphore          (void);
static void  proxy_ExitSemaphore          (void);
static void  proxy_PostSemaphore          (void);
static int   proxy_WaitSemaphore          (int);

//
// Local DYN pages
//
#define  EXTRACT_DYN(a,b,c,d,e,f)   {a,b,c,d,e,f},
static const NETDYN stDynamicPages[] =
{
//    tUrl,    tType,   iTimeout,   iFlags,  pcUrl,   pfDynCb;
#include "dyn_proxy.h"
   {  -1,      0,       0,          0,       NULL,    NULL  }
};
#undef EXTRACT_DYN
//
static int              fProxyRunning        = TRUE;
static const char      *pcProxyFile          = RPI_PROX_PATH;
static const char      *pcProxyIp            = "192.168.178.209";
static const int        iProxyPort           =  80;
static const char      *pcRemoteIoUrl        = "io.json?RemIo=%s";
static const int        iIoPort              =  80;
//
// HTTP GET request:
//
static const char       pcGetRequest[] =  
                           "GET /%s HTTP/1.1" CRLF
                           "Host: cvb.patrn.nl" CRLF
//                         "User-Agent: Mozilla/5.0 (Windows; U; WinGecko/20100722 Firefox/3.6.8 (.NET CLR 3.5.30729)" CRLF
//                         "Accept: image/png,image/*;q=0.8,*/*;q=0.5" CRLF
//                         "Accept-Encoding: gzip,deflate" CRLF
//                         "Accept-Charset: ISO-8859-1,utf-8;q=0.7,*;q=0.7" CRLF
                           "Keep-Alive: 115" CRLF
                           "Connection: keep-alive" CRLF CRLF;


/*------  Local functions separator ------------------------------------
__GLOBAL_FUNCTIONS(){};
----------------------------------------------------------------------------*/

//
// Function:   PROXY_Init
// Purpose:    Init the Proxy thread
//
// Parameters:
// Returns:    Startup signal
//
//
int PROXY_Init(void)
{
   int      iCc=0;
   pid_t    tPid;
   
   if( (tPid = proxy_Deamon()) > 0) 
   {
      GLOBAL_PidPut(PID_PROX, tPid);
      iCc = GLOBAL_PRX_INI;
   }
   //
   // The parent will return here. The proxy deamon will
   // exit upon completion.
   //
   PRINTF1("PROXY-Init():Pid=%d" CRLF, tPid);
   return(iCc);
}


//
// Function:   PROXY_DynPageReplyHtml
// Purpose:    Dynamically created system page to show the proxy reply file
// 
// Parameters: Client
// Returns:    TRUE
// Note:       
//
bool PROXY_DynPageReplyHtml(NETCL *pstCl)
{
   // 
   // Put out the HTTP reply from SmartE server
   //         
   PRINTF("PROXY-DynPageReplyHtml()" CRLF);
   HTTP_SendTextFile(pstCl, (char *)pcProxyFile);
   //
   return(TRUE);
}

//
// Function:   PROXY_DynPageReplyJson
// Purpose:    Dynamically created system page to show the proxy reply file
// 
// Parameters: Client
// Returns:    TRUE
// Note:       
//
bool PROXY_DynPageReplyJson(NETCL *pstCl)
{
   return(FALSE);
}

/*------  Local functions separator ------------------------------------
__LOCAL_FUNCTIONS(){};
----------------------------------------------------------------------------*/

//
// Function:   proxy_Deamon
// Purpose:    Instantiate a new thread by the rpi_main thread
//
// Parms:
// Returns:    The pid of the new proxy thread
//
static pid_t proxy_Deamon(void)
{
   pid_t    tPid;
   
   tPid = fork();
   //
   switch(tPid)
   {
      default:
         // rpi_main:parent
         break;
          
      case 0:
         // proxy:child
         sleep(2);
         PRINTF("proxy-Deamon()" CRLF);
         proxy_Execute();
         _exit(0);
         break;
          
      case -1:
         // Error
         GEN_Printf("proxy-Deamon(): Error!"CRLF);
         LOG_Report(errno, "PRX", "proxy-Deamon(): Error!");
         break;
   }
   return(tPid);
}

//
// Function:   proxy_Execute
// Purpose:    Run the proxy thread
//
// Parameters:
// Returns:
// Note:       This is the main loop for the proxy thread.
//             A SIGUSR1 comes from:
//                o the HTTP server to initiate a command
//
static void proxy_Execute(void)
{
   int         iPort;
   sigset_t    tBlockset;
   
   if(proxy_SignalRegister(&tBlockset) == FALSE)  _exit(1);
   //
   proxy_InitSemaphore();
   //
   //LOG_Report(0, "PRX", "proxy-Execute():Init Start....");
   iPort = atoi(pstMap->G_pcSrvrPort);
   proxy_InitDynamicPages(iPort);
   //
   GLOBAL_PidSaveGuard(PID_PROX, GLOBAL_PRX_INI);
   GLOBAL_HostNotification(GLOBAL_PRX_INI);
   //LOG_Report(0, "PRX", "proxy-Execute():Init Ready");
   //
   while(fProxyRunning)
   {
      //
      // The proxy mainloop: 
      //    HTTP  requests come from the rpi_main thread for proxy requests
      //    Email requests come from the cmd_main thread for email results
      //
      PRINTF("proxy-Execute():Wait RPI signal...." CRLF);
      switch( proxy_WaitSemaphore(5000) )
      {
         case 0:
            if( GLOBAL_GetSignalNotification(PID_PROX, GLOBAL_SVR_RUN) ) proxy_HandleServerRequest();
            if( GLOBAL_GetSignalNotification(PID_PROX, GLOBAL_CMD_RUN) ) proxy_HandleCommandRequest();
            if( GLOBAL_GetSignalNotification(PID_PROX, GLOBAL_EML_RUN) ) proxy_HandleEmailRequest();
            if( GLOBAL_GetSignalNotification(PID_PROX, GLOBAL_GRD_RUN) ) proxy_HandleGuard();
            break;
             
         default:
            break;

         case -1:
            LOG_Report(errno, "PRX", "proxy-Execute():ERROR proxy_WaitSemaphore");
            PRINTF("proxy-Execute():ERROR proxy_WaitSemaphore" CRLF);
            break;
             
         case 1:
            //
            // Request timeout
            //
            PRINTF("proxy-Execute(): timeout" CRLF);
            GLOBAL_PidSetGuard(PID_PROX);
            break;
      }
   }
   proxy_ExitSemaphore();
}

//
// Function:   proxy_InitDynamicPages
// Purpose:    Register dynamic webpages
//
// Parameters: Port
// Returns:
// Note:
//
static void proxy_InitDynamicPages(int iPort)
{
   const NETDYN  *pstDyn=stDynamicPages;
   
   PRINTF1("proxy-InitDynamicPages(): port=%d" CRLF, iPort);
   //
   while(pstDyn->pcUrl)
   {
      //PRINTF1("proxy-InitDynamicPages():Url=%s" CRLF, pstDyn->pcUrl);
      if(pstDyn->iFlags & DYN_FLAG_PORT)
      {
         //
         // Register for this specific port
         //
         GEN_RegisterDynamicPage(pstDyn->tUrl, PID_PROX, pstDyn->tType, pstDyn->iTimeout, iPort, pstDyn->pcUrl, (PFVOIDPI)pstDyn->pfDynCb);
      }
      else
      {
         //
         // Register for all ports
         //
         GEN_RegisterDynamicPage(pstDyn->tUrl, PID_PROX, pstDyn->tType, pstDyn->iTimeout,     0, pstDyn->pcUrl, (PFVOIDPI)pstDyn->pfDynCb);
      }
      pstDyn++;
   }
}

//
// Function:   proxy_DynPageSmartMeter
// Purpose:    Handle the Smart-E generic proxy
//
// Parms:       Client, dyn idx
// Returns:
// Note:
//
static bool proxy_DynPageSmartMeter(NETCL *pstCl, int iIdx)
{
    PRINTF("proxy-DynPageSmartMeter()" CRLF);
    return(TRUE);
}

//
// Function:   proxy_HandleCommandRequest
// Purpose:    Handle the PID_CMND RemoteIO request
//             
// Parms:      
// Returns:    
// Note:
//
static bool proxy_HandleCommandRequest(void)
{
   bool     fCc=FALSE;
   int      iLen;
   char    *pcReq;
   char    *pcUrl;

   //
   // Build URL: 192.168.178.205/io.json?RemIo=xxx
   //            G_pcRemoteIp[]                G_pcRemoteIo[]
   //
   iLen  = GEN_STRLEN(pcRemoteIoUrl) + 16;
   pcUrl = safemalloc(iLen);
   GEN_SNPRINTF(pcUrl, iLen, pcRemoteIoUrl, pstMap->G_pcRemoteIo);
   //
   iLen += GEN_STRLEN(pcGetRequest);
   pcReq = safemalloc(iLen);
   GEN_SNPRINTF(pcReq, iLen, pcGetRequest, pcUrl);
   //
   if( proxy_SendRemoteIo(pcReq) > 0)
   {
      //LOG_Report(0, "PRX", "proxy-HandleCommandRequest():Request OKee");
      PRINTF1("proxy-HandleCommandRequest():Notify PID_SRVR:<%s>" CRLF, pcReq);
      proxy_SignalHttpServer();
      fCc = TRUE;
   }
   else
   {
      LOG_Report(errno, "PRX", "proxy-HandleCommandRequest():Request ERROR:");
      PRINTF1("proxy-HandleCommandRequest():Send request error :<%s>" CRLF, pcReq);
   }
   safefree(pcReq);
   safefree(pcUrl);
   return(fCc);
}

//
// Function:   proxy_HandleEmailRequest
// Purpose:    Handle the EMAIL proxy request
//
// Parms:      
// Returns:    
// Note:
//
static bool proxy_HandleEmailRequest(void)
{
   bool     fCc=FALSE;
   int      iLen;
   char    *pcReq=NULL;
   char    *pcUrl;

   PRINTF("proxy-HandleEmailRequest():Have CMD signal" CRLF);
   //===========================================================================
   // Incoming signal from CMD thread
   //===========================================================================
   pcUrl = GEN_GetDynamicPageName(DYN_SME_HTML_DAYSLM);
   PRINTF2("proxy-HandleEmailRequest():Retrieve Idx=%d, %s" CRLF, DYN_SME_HTML_DAYSLM, pcUrl);
   iLen  = GEN_STRLEN(pcGetRequest) + GEN_STRLEN(pcUrl);
   pcReq = safemalloc(iLen);
   //
   GEN_SNPRINTF(pcReq, iLen, pcGetRequest, pcUrl);
   if( proxy_SendRequest(pcReq) > 0)
   {
      //LOG_Report(0, "PRX", "proxy-HandleEmailRequest():Request OKee");
      PRINTF1("proxy-HandleEmailRequest():Notify CMD:<%s>" CRLF, pcReq);
      GLOBAL_Notify(PID_CMND, GLOBAL_PRX_NFY, SIGUSR1);
      fCc = TRUE;
   }
   else
   {
      LOG_Report(errno, "PRX", "proxy-HandleEmailRequest():Request ERROR:");
      PRINTF1("proxy-HandleEmailRequest():Send request error :<%s>" CRLF, pcReq);
   }
   safefree(pcReq);
   return(fCc);
}

//
// Function:   proxy_HandleServerRequest
// Purpose:    Handle the Host proxy request
//
// Parms:      
// Returns:    
// Note:
//
static bool proxy_HandleServerRequest(void)
{
   bool     fCc=FALSE;
   int      iLen;
   char    *pcReq=NULL;
   char    *pcUrl;

   pcUrl = GEN_GetDynamicPageName(pstMap->G_iCurDynPage);
   iLen  = GEN_STRLEN(pcGetRequest) + GEN_STRLEN(pcUrl);
   pcReq = safemalloc(iLen);
   //
   GEN_SNPRINTF(pcReq, iLen, pcGetRequest, pcUrl);
   if( proxy_SendRequest(pcReq) > 0)
   {
      //LOG_Report(0, "PRX", "proxy-HandleServerRequest():Request OKee");
      PRINTF1("proxy-HandleServerRequest():Notify PID_SRVR:<%s>" CRLF, pcReq);
      proxy_SignalHttpServer();
      fCc = TRUE;
   }
   else
   {
      LOG_Report(errno, "PRX", "proxy-HandleServerRequest():Request ERROR:");
      PRINTF1("proxy-HandleServerRequest():Send request error :<%s>" CRLF, pcReq);
   }
   safefree(pcReq);
   return(fCc);
}

//
// Function:   proxy_SendRequest
// Purpose:    Send data to IP stack, read reply
//
// Parms:      Request to send
// Returns:    Number read (-1 is error)
// Note:
//
static int proxy_SendRequest(char *pcReq)
{
   int   iSend, iLen;
   int   iRead, iTotal=-1;
   int   iFdIp, iFdFile;
   char *pcBuffer;
   
   GEN_STRNCPY(pstMap->G_pcLastFile, pcProxyFile, MAX_PATH_LEN);
   //
   iFdIp = NET_ClientConnect(pcProxyIp, iProxyPort, "tcp");
   //
   if(iFdIp > 0)
   {
      pcBuffer = safemalloc(PROXY_LENGTH);
      iLen = GEN_STRLEN(pcReq);
      PRINTF1("proxy-SendRequest():Connect to server OK, send %d bytes !" CRLF, iLen);
      iSend = NET_Write(iFdIp, (char *) pcReq, iLen);
      if(iSend == iLen)
      {
         PRINTF1("proxy-SendRequest():%d bytes send" CRLF, iSend);
         iFdFile = safeopen(pstMap->G_pcLastFile, O_RDWR|O_CREAT|O_TRUNC);
         iTotal  = 0;
         do
         {
            iRead = NET_Read(iFdIp, pcBuffer, PROXY_LENGTH);
            if(iRead < 0)
            {
               PRINTF1("proxy-SendRequest():ERROR %d reading socket" CRLF, iRead);
            }
            else
            {
               //PRINTF1("proxy-SendRequest():%s" CRLF, pcBuffer);
               PRINTF2("proxy-SendRequest():Write %d bytes to %s" CRLF, iRead, pcProxyFile);
               safewrite(iFdFile, pcBuffer, iRead);
               iTotal += iRead;
            }
         }
         while(iRead > 0);
         //
         PRINTF1("proxy-SendRequest():Total=%d bytes" CRLF, iTotal);
         safeclose(iFdFile);
      }
      else
      {
         PRINTF1("proxy-SendRequest():ERROR: %d bytes send" CRLF, iSend);
      }
      NET_ClientDisconnect(iFdIp);
      safefree(pcBuffer);
   }
   else
   {
      PRINTF1("proxy-SendRequest(): Not able to connect to server %s !" CRLF, pcProxyIp);
   }
   return(iTotal);
}   

//
// Function:   proxy_SendRemoteIo
// Purpose:    Send remote IO data to IP stack, read reply
//
// Parms:      Request to send
// Returns:    Number read (-1 is error)
// Note:
//
static int proxy_SendRemoteIo(char *pcReq)
{
   int   iSend, iLen;
   int   iRead, iTotal=0;
   int   iFdIp;
   char *pcBuffer;
   
   #ifdef FEATURE_PRINT_PROXY
   GEN_Printf("proxy-SendRemoteIo():Send %s to %s" CRLF, pcReq, pstMap->G_pcRemoteIp);
   #endif   //FEATURE_PRINT_PROXY

   iFdIp = NET_ClientConnect(pstMap->G_pcRemoteIp, iIoPort, "tcp");
   //
   if(iFdIp > 0)
   {
      pcBuffer = safemalloc(PROXY_LENGTH);
      iLen = GEN_STRLEN(pcReq);
      PRINTF1("proxy-SendRemoteIo():Connect to server OK, send %d bytes !" CRLF, iLen);
      iSend = NET_Write(iFdIp, (char *) pcReq, iLen);
      if(iSend == iLen)
      {
         PRINTF1("proxy-SendRemoteIo():%d bytes send" CRLF, iSend);
         do
         {
            iRead = NET_Read(iFdIp, pcBuffer, PROXY_LENGTH);
            if(iRead < 0)
            {
               #ifdef FEATURE_PRINT_PROXY
               GEN_Printf("proxy-SendRemoteIo():ERROR %d reading socket" CRLF, iRead);
               #endif   //FEATURE_PRINT_PROXY

               PRINTF1("proxy-SendRemoteIo():ERROR %d reading socket" CRLF, iRead);
            }
            else
            {
               #ifdef FEATURE_PRINT_PROXY
               pcBuffer[iRead] = 0;
               GEN_Printf("proxy-SendRemoteIo():Read %d: %s" CRLF, iRead, pcBuffer);
               #endif   //FEATURE_PRINT_PROXY

               iTotal += iRead;
            }
         }
         while(iRead > 0);

         #ifdef FEATURE_PRINT_PROXY
         GEN_Printf("proxy-SendRemoteIo():Read OK: %d bytes" CRLF, iTotal);
         #endif   //FEATURE_PRINT_PROXY

      }
      else
      {
         #ifdef FEATURE_PRINT_PROXY
         GEN_Printf("proxy-SendRemoteIo():ERROR: %d bytes send" CRLF, iSend);
         #endif   //FEATURE_PRINT_PROXY

         PRINTF1("proxy-SendRemoteIo():ERROR: %d bytes send" CRLF, iSend);
      }
      PRINTF1("proxy-SendRemoteIo():%d bytes read" CRLF, iTotal);
      NET_ClientDisconnect(iFdIp);
      safefree(pcBuffer);
   }
   else
   {
      #ifdef FEATURE_PRINT_PROXY
      GEN_Printf("proxy-SendRemoteIo(): Not able to connect to server %s !" CRLF, pstMap->G_pcRemoteIp);
      #endif   //FEATURE_PRINT_PROXY

      PRINTF1("proxy-SendRemoteIo(): Not able to connect to server %s !" CRLF, pstMap->G_pcRemoteIp);
   }
   return(iTotal);
}   

// 
// Function:   proxy_HandleGuard
// Purpose:    Respond to the guard query
// 
// Parameters: 
// Returns:    
// Note:       
//
static void proxy_HandleGuard(void)
{
   //LOG_Report(0, "PRX", "proxy-HandleGuard():Host-notification send");
   GLOBAL_HostNotification(GLOBAL_GRD_NFY);
}


/*------  Local functions separator ------------------------------------
__SIGNAL_FUNCTIONS(){};
----------------------------------------------------------------------------*/

//
// Function:   proxy_SignalRegister
// Purpose:    Register all SIGxxx
//
// Parameters: Blockset
// Returns:    TRUE if delivered
//
static bool proxy_SignalRegister(sigset_t *ptBlockset)
{
   bool fCC = TRUE;
    
   // SIGUSR1:
   if( signal(SIGUSR1, &proxy_ReceiveSignalUser1) == SIG_ERR)
   {
       LOG_Report(errno, "PRX", "proxy-SignalRegister(): SIGUSR1 ERROR");
       fCC = FALSE;
   }
   // SIGUSR2:
   if( signal(SIGUSR2, &proxy_ReceiveSignalUser2) == SIG_ERR)
   {
       LOG_Report(errno, "PRX", "proxy-SignalRegister(): SIGUSR2 ERROR");
       fCC = FALSE;
   }
   // CTL-X handler
   if( signal(SIGTERM, &proxy_ReceiveSignalTerm) == SIG_ERR)
   {
       LOG_Report(errno, "PRX", "proxy-SignalRegister(): SIGTERM ERROR");
       fCC = FALSE;
   }
   // Ctl-C handler
   if( signal(SIGINT, &proxy_ReceiveSignalInt) == SIG_ERR)
   {
       LOG_Report(errno, "PRX", "proxy-SignalRegister(): SIGINT ERROR");
       fCC = FALSE;
   }
   // Segmentation fault
   if( signal(SIGSEGV, &proxy_ReceiveSignalSegmnt) == SIG_ERR)
   {
        LOG_Report(errno, "PRX", "proxy-ReceiveSignalSegmnt(): SIGSEGV ERROR");
        fCC = FALSE;
   }
   
   if(fCC)
   {
       //
       // Setup the SIGxxx events
       //
       sigemptyset(ptBlockset);
       sigaddset(ptBlockset, SIGTERM);
       sigaddset(ptBlockset, SIGINT);
       sigaddset(ptBlockset, SIGUSR1);
       sigaddset(ptBlockset, SIGUSR2);
       sigaddset(ptBlockset, SIGSEGV);
   }
   return(fCC);
}

//
// Function:   proxy_ReceiveSignalTerm
// Purpose:    Add the SIGTERM command in the buffer (Ctl-X)
//
// Parameters:
// Returns:    TRUE if delivered
//
static void proxy_ReceiveSignalTerm(int iSignal)
{
    LOG_Report(0, "PRX", "proxy-ReceiveSignalTerm()");
    fProxyRunning = FALSE;
    proxy_PostSemaphore();
}

//
// Function:   proxy_ReceiveSignalInt
// Purpose:    Add the SIGINT command in the buffer (Ctl-C)
//
// Parms:
// Returns:    TRUE if delivered
//
static void proxy_ReceiveSignalInt(int iSignal)
{
    LOG_Report(0, "PRX", "proxy-ReceiveSignalInt()");
    fProxyRunning = FALSE;
    proxy_PostSemaphore();
}

//
// Function:   proxy_ReceiveSignalUser1
// Purpose:    System callback for SIGUSR1 signal
//
// Parms:
// Returns:
// Note:       Any thread can send us a SIGUSR1 to signal a new action
//             Use GLOBAL_GetSignalNotification(PID_xxx, GLOBAL_xxx_RUN) to specify
//             what needs to be done.
//
static void proxy_ReceiveSignalUser1(int iSignal)
{
    proxy_PostSemaphore();
}

//
// Function:   proxy_ReceiveSignalUser2
// Purpose:    System callback for SIGUSR2 signal
//
// Parms:
// Returns:
// Note:       Not used
//
static void proxy_ReceiveSignalUser2(int iSignal)
{
}

//
// Function:   proxy_ReceiveSignalSegmnt
// Purpose:    Handle the SIGSEGV
//
// Parms:
// Returns:    TRUE if delivered
// Note:       Do NOT use LOG_xxx() here
//
static void proxy_ReceiveSignalSegmnt(int iSignal)
{
   if(fProxyRunning)
   {
      fProxyRunning = FALSE;
      //
      GEN_SegmentationFault(__FILE__, __LINE__);
      LOG_SegmentationFault(__FILE__, __LINE__);
      //
      // (TRY TO) Cleanup background threads:
      //
      GLOBAL_Notify(PID_HOST, GLOBAL_FLT_NFY, SIGTERM);
   }
   proxy_PostSemaphore();
}

//
// Function:   proxy_SignalHttpServer
// Purpose:    Signal the HTTP server
//
// Parameters:
// Returns:
// Note:
//
static void proxy_SignalHttpServer()
{
   GLOBAL_Notify(PID_SRVR, GLOBAL_PRX_NFY, SIGUSR1);
}


/*------  Local functions separator ------------------------------------
__SEMAPHORE_FUNCTIONS(){};
----------------------------------------------------------------------------*/

//
// Function:   proxy_InitSemaphore
// Purpose:    Init the Semaphore
//
// Parms:
// Returns:
// Note:       If pshared has the value 0, then the semaphore is shared between the
//             threads of a process, and should be located at some address that is
//             visible to all threads (e.g., a global variable, or a variable
//             allocated dynamically on the heap).
//             If pshared is nonzero, then the semaphore is shared between
//             processes, and should be located in a region of shared memory.
//
//             The sem value is always >= 0:
//                               0 means LOCKED
//                              >0 means UNLOCKED
//             Init(sem 1, VAL)  VAL=1 unlocked
//                               VAL=0 locked
//             Wait(sem)         LOCK:    if(VAL> 0) { VAL--; return;         } 
//                                        if(VAL==0) { Wait till > 0; return; }
//             Post(sem)         UNLOCK:  VAL++
//
static void proxy_InitSemaphore(void)
{
    //
    // Init the semaphore as LOCKED
    //
    if(sem_init(&pstMap->G_tSemPrx, 1, 0) == -1)
    {
        LOG_Report(errno, "PRX", "proxy-InitSemaphore(): sem_init Error");
    }
}

//
// Function:   proxy_ExitSemaphore
// Purpose:    Exit the Semaphore
//
// Parms:
// Returns:
// Note:
//
static void proxy_ExitSemaphore(void)
{
    if( sem_destroy(&pstMap->G_tSemPrx) == -1) LOG_Report(errno, "PRX", "proxy-ExitSemaphore():sem_destroy");
}

//
// Function:   proxy_PostSemaphore
// Purpose:    Unlock the Semaphore
//
// Parms:      
// Returns:    
// Note:       sem_post() increments (unlocks) the semaphore pointed to by sem. If the semaphore's 
//             value consequently becomes greater than zero, then another process or thread blocked
//             in a sem_wait(3) call will be woken up and proceed to lock the semaphore. 
//
static void proxy_PostSemaphore(void)
{
   //
   // sem++ (UNLOCK)   
   //
   if( (sem_post(&pstMap->G_tSemPrx)) == -1) LOG_Report(errno, "PRX", "proxy-PostSemaphore(): sem_post error");
}

//
// Function:   proxy_WaitSemaphore
// Purpose:    Wait till Semaphore has been unlocked, then LOCK and proceed
//
// Parms:      TIMEOUT (or -1 if wait indefinitely)
// Returns:     0 if sem unlocked
//             +1 if sem timeout
//             -1 on error (errno)
// Note:       sem_wait() tries to decrement (lock) the semaphore. 
//             If the semaphore currently has the value zero, then the call blocks until 
//             either it becomes possible to perform the decrement (the semaphore value rises 
//             above zero) or a signal handler interrupts the call. 
//             If the semaphore's value is greater than zero, then the decrement proceeds, 
//             and the function returns immediately. 
//
//             sem_timedwait() is the same as sem_wait(), except that TIMEOUT specifies a 
//             limit on the amount of time that the call should block if the decrement cannot 
//             be immediately performed. The stTime points to a structure that
//             specifies an absolute timeout in seconds and nanoseconds since the Epoch, 
//             1970-01-01 00:00:00 +0000 (UTC). This structure is defined as follows:
//
//             struct timespec 
//             {
//                time_t tv_sec;      /* Seconds */
//                long   tv_nsec;     /* Nanoseconds [0 .. 999999999] */
//             };
//             If the timeout has already expired by the time of the call, and the semaphore 
//             could not be locked immediately, then sem_timedwait() fails with a timeout error 
//             (errno set to ETIMEDOUT). If the operation can be performed immediately, then 
//             sem_timedwait() never fails with a timeout error, regardless of the value of 
//             TIMEOUT.
//
//
static int proxy_WaitSemaphore(int iMsecs)
{
   //
   // if    sem == 0 : wait
   // else  sem-- (LOCK)   
   //
   return(GEN_WaitSemaphore(&pstMap->G_tSemPrx, iMsecs));
}

