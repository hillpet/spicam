/*  (c) Copyright:  2017  Patrn.nl, Confidential Data
**
**  $Workfile:          rpisupp.h
**  $Revision:   
**  $Modtime:    
**
**  Purpose:            rpisupp.c header file
**
 *  Compiler/Assembler: Raspbian Linux
 *  Ext Packages:
**
 *  Author:             Peter Hillen
 *  Date Created:       26 Jul 2017
**
 *  Revisions:
 *    $Log:   $
 *
 *
**/

#ifndef _RPISUPP_H_
#define _RPISUPP_H_

//
// Seconds support time out
//
#define SUP_BACKGROUND_CHECK_SECS        10
//
// Global prototypes
//
int   SUP_Init          (void);

#endif  /*_RPISUPP_H_ */

