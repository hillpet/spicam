/*  (c) Copyright:  2017  Patrn.nl, Confidential Data
**
**  $Workfile:          gen_usb.c
**  $Revision:   
**  $Modtime:    
**
**  Purpose:            USB HID Interface functions
**
**  Entry Points:       
**
**
**
**
**
 *  Compiler/Assembler: Raspbian Linux
 *  Ext Packages:
**
 *  Author:             Peter Hillen
 *  Date Created:       5 Oct 2017 (from rpiusb.c)
**
 *  Revisions:
 *    $Log:   $
 *
 *
 **/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <time.h>
#include <sys/select.h>
#include <sys/signal.h>
#include <sys/types.h>

#include <common.h>
#include "config.h"
#include "globals.h"
#include "gen_func.h"
#include "gen_usb.h"

//#define USE_PRINTF
#include <printf.h>

#define DUMP_HEX     TRUE
#define DUMP_ASCII   FALSE
//
#ifdef  DEBUG_ASSERT                   // defined in makefile
//
//      USB COMMS Log
//      USB DEBUG Log
//
#define USB_LOG_COMMS(a,b,c,d)         usb_DumpComms(a,b,c,d)
#define USB_LOG_DEBUG(a,b,c,d)         usb_DumpDebug(a,b,c,d)
//
static void    usb_DumpComms           (const char *, char *, int, bool);
static void    usb_DumpDebug           (const char *, char *, int, bool);
#else   //DEBUG_ASSERT                   // defined in makefile
#define USB_LOG_COMMS(a,b,c,d)
#define USB_LOG_DEBUG(a,b,c,d)
#endif  //DEBUG_ASSERT                   // defined in makefile
//
// Local prototypes
//
#ifdef FEATURE_USB
static int     usb_Daemon              (RPIUSB *);
static int     usb_Debug               (RPIUSB *);
static int     usb_Exit                ();
static int     usb_Init                ();
static void    usb_ReceiveSignalInt    (int);
static void    usb_ReceiveSignalTerm   (int);
static void    usb_ReceiveSignalUser1  (int);
static void    usb_ReceiveSignalUser2  (int);
static bool    usb_SignalRegister      (sigset_t *);
static char   *usb_StoreString         (void *, bool);
//
static bool    fUsbRunning = TRUE;
#endif   //FEATURE_USB
//
/*------ functions separator ------------------------------------------------
____Deamon_Threads()  {};
----------------------------------------------------------------------------*/

//
//  Function:  USB_Daamon
//  Purpose:   Deamon communicating with the USB port
//
//  Parms:     HID struct
//  Returns:   Startup code
//
int USB_Daemon(RPIUSB *pstHid)
{
   int      iCc=0;
#ifdef FEATURE_USB
   pid_t    tPid;

   tPid = fork();
   //
   switch(tPid)
   {
      default:
         // Parent (Commandline or service) 
         GLOBAL_PidPut(PID_USBC, tPid);
         iCc = GLOBAL_UDM_INI;
         break;

      case 0:
         // Child:
         if(usb_Daemon(pstHid)) _exit(0);
         //
         // Error
         //
         LOG_printf("USB_Daemon(): Daemon returned error !" CRLF);
         _exit(1);
         break;

      case -1:
         // Error
         LOG_printf("USB_Daemon(): Error!" CRLF);
         _exit(1);
         break;
   }
#endif   //FEATURE_USB
   return(iCc);
}

//
//  Function:  USB_Debug
//  Purpose:   Deamon debug communicating with the USB port
//
//  Parms:     HID struct
//  Returns:   Startup code
//
int USB_Debug(RPIUSB *pstHid)
{
   int      iCc=0;
#ifdef FEATURE_USB
   pid_t    tPid;

   tPid = fork();
   //
   switch(tPid)
   {
      default:
         // Parent (Commandline or service) 
         GLOBAL_PidPut(PID_USBD, tPid);
         iCc = GLOBAL_UDB_INI;
         break;

      case 0:
         // Child:
         if(usb_Debug(pstHid)) _exit(0);
         //
         // Error:
         //
         LOG_printf("USB_Debug(): Debug Daemon returned error !" CRLF);
         _exit(1);
         break;

      case -1:
         // Error
         LOG_printf("USB_Debug(): Error!" CRLF);
         _exit(1);
         break;
   }
#endif   //FEATURE_USB
   return(iCc);
}


/*------ functions separator ------------------------------------------------
____Global_Functions()  {};
----------------------------------------------------------------------------*/

//
//  Function:  USB_Close
//  Purpose:   Close USB devide
//
//  Parms:     Device info struct
//  Returns:   true if OKee 
//
bool USB_Close(RPIUSB *pstUsb)
{
   bool  fCc=FALSE;

#ifdef FEATURE_USB
   if(pstUsb)
   {
      hid_close(pstUsb->ptHandle);
      pstUsb->ptHandle = NULL;
      fCc = TRUE;
   }
#endif   //FEATURE_USB
   return(fCc);
}

//
//  Function:  USB_GetInfo
//  Purpose:   Get info for all connected USB devices
//
//  Parms:     iVid, iPid 
//  Returns:   Linked list of all USB devices
//
RPIUSB *USB_GetInfo(int iVid, int iPid)
{
   RPIUSB                 *pstTop=NULL;
#ifdef FEATURE_USB
   int                     iNr=0;
   RPIUSB                 *pstUsb;
   struct hid_device_info *pstDevs, *pstCurDev;

   //
   // Enumerate all HID devices on the system
   //
   usb_Init();
   pstDevs   = hid_enumerate(iVid, iPid);
   pstCurDev = pstDevs; 
   //
   while(pstCurDev) 
   {
      if(pstTop == NULL)
      {
         pstTop = safemalloc(sizeof(RPIUSB));
         pstUsb = pstTop;
      }
      else
      {
         pstUsb->pstNext = safemalloc(sizeof(RPIUSB));
         pstUsb          = pstUsb->pstNext;
      }
      pstUsb->iIndex         = iNr;
      pstUsb->iVendorId      = (int) pstCurDev->vendor_id;
      pstUsb->iProductId     = (int) pstCurDev->product_id;
      pstUsb->iInterfaceNr   = (int) pstCurDev->interface_number;
      //
      pstUsb->pcDevicePath   = usb_StoreString(pstCurDev->path, FALSE);
      pstUsb->pcManufacturer = usb_StoreString(pstCurDev->manufacturer_string, TRUE);
      pstUsb->pcProduct      = usb_StoreString(pstCurDev->product_string, TRUE);
      pstUsb->pcSerialNr     = usb_StoreString(pstCurDev->serial_number, TRUE);
      //
      PRINTF("USB_GetInfo():Device Found :"  CRLF);
      PRINTF1("   Path          : %s"        CRLF, pstUsb->pcDevicePath);
      PRINTF1("   Vendor  ID    : %04x"      CRLF, pstUsb->iVendorId);
      PRINTF1("   Product ID    : %04x"      CRLF, pstUsb->iProductId);
      PRINTF1("   Manufacturer  : %s"        CRLF, pstUsb->pcManufacturer);
      PRINTF1("   Product       : %s"        CRLF, pstUsb->pcProduct);
      PRINTF1("   Serial nr     : %s"        CRLF, pstUsb->pcSerialNr);
      PRINTF1("   Interface     : %d"        CRLF, pstUsb->iInterfaceNr);
      //
      iNr++;
      pstCurDev = pstCurDev->next;
   }
   hid_free_enumeration(pstDevs);
   usb_Exit();
#endif   //FEATURE_USB
   return(pstTop);
}

//
//  Function:  USB_GetDeviceInfo
//  Purpose:   Get info struct of a USB device in the linked list
//
//  Parms:     Linked list
//             Vendor ID
//             Product ID
//             Interface Number
//  Returns:   Device struct or NULL
//
RPIUSB *USB_GetDeviceInfo(RPIUSB *pstUsb, int iVid, int iPid, int iIntfNr)
{
   while(pstUsb)
   {
      if( (pstUsb->iVendorId == iVid) && (pstUsb->iProductId == iPid) && (pstUsb->iInterfaceNr == iIntfNr) )
      {
         break;
      }
      pstUsb = pstUsb->pstNext;
   }
   return(pstUsb);
}

//
//  Function:  USB_Open
//  Purpose:   Open USB devide
//
//  Parms:     Linked list
//  Returns:   true if OKee 
//
bool USB_Open(RPIUSB *pstUsb)
{
   bool  fCc=FALSE;

#ifdef FEATURE_USB
   if(pstUsb)
   {
      pstUsb->ptHandle = hid_open_path(pstUsb->pcDevicePath);
      fCc = (pstUsb->ptHandle == NULL) ? FALSE : TRUE;
   }
#endif   //FEATURE_USB
   return(fCc);
}

//
//  Function:  USB_Read
//  Purpose:   Read data from USB devide
//
//  Parms:     Device Info struct, Buffer, Size
//  Returns:   Number read, or -1 on error
//
int USB_Read(RPIUSB *pstUsb, char *pcBuffer, int iSize)
{
   int      iNr=-1;
#ifdef FEATURE_USB
   const wchar_t *pwError;

   iNr = hid_read(pstUsb->ptHandle, (unsigned char *)pcBuffer, iSize);
   if(iNr == -1)
   {
      pwError = hid_error(pstUsb->ptHandle);
      if(pwError)
      {
         PRINTF1("USB_Read(): ERROR hid_read(): %ls" CRLF, pwError);
      }
      else
      {
         PRINTF("USB_Read(): ERROR hid_read(): unspecified error !!" CRLF);
      }
   }
#endif   //FEATURE_USB
   return(iNr);
}

//
//  Function:  USB_Write
//  Purpose:   Write data to USB devide
//
//  Parms:     Device Info struct, Buffer, Size
//  Returns:   Number written, or -1 on error
//
int USB_Write(RPIUSB *pstUsb, char *pcBuffer, int iSize)
{
   int      iNr=-1;
#ifdef FEATURE_USB
   const wchar_t *pwError;

   iNr = hid_write(pstUsb->ptHandle, (unsigned char *)pcBuffer, iSize);
   if(iNr == -1)
   {
      pwError = hid_error(pstUsb->ptHandle);
      if(pwError)
      {
         PRINTF1("USB_Write(): ERROR hid_read(): %ls" CRLF, pwError);
      }
      else
      {
         PRINTF("USB_Write(): ERROR hid_read(): unspecified error !!" CRLF);
      }
   }
#endif   //FEATURE_USB
   return(iNr);
}

//
//  Function:  USB_ReleaseInfo
//  Purpose:   Release all linked list info
//
//  Parms:     Linked list
//  Returns:    
//
void USB_ReleaseInfo(RPIUSB *pstUsb)
{
   RPIUSB  *pstCur;

   while(pstUsb)
   {
      pstCur = pstUsb;
      pstUsb = pstUsb->pstNext;
      //
      safefree(pstCur->pcDevicePath);
      safefree(pstCur->pcManufacturer);
      safefree(pstCur->pcProduct);
      safefree(pstCur->pcSerialNr);
      safefree(pstCur);
   }
}

//
//  Function:   USB_PrintInfo
//  Purpose:    Open, enumerate and print USB info
//
//  Parms:      
//  Returns:    Number of devices found
//
int USB_PrintInfo()
{
   int   iNr=0;

#ifdef FEATURE_USB
   usb_Init();
   //
   // Enumerate and print the HID devices on the system
   //
   struct hid_device_info *pstDevs, *pstCurDev;
   
   pstDevs   = hid_enumerate(0x16c0, 0x480);
   pstCurDev = pstDevs; 
   //
   while(pstCurDev) 
   {
      PRINTF ("USB_Open():Device Found :" CRLF);
      PRINTF1("   Path          : %s"    CRLF, pstCurDev->path);
      PRINTF1("   Vendor  ID    : %04x"  CRLF, pstCurDev->vendor_id);
      PRINTF1("   Product ID    : %04x"  CRLF, pstCurDev->product_id);
      PRINTF1("   Manufacturer  : %ls"   CRLF, pstCurDev->manufacturer_string);
      PRINTF1("   Product       : %ls"   CRLF, pstCurDev->product_string);
      PRINTF1("   Serial nr     : %ls"   CRLF, pstCurDev->serial_number);
      PRINTF1("   Interface     : %d"    CRLF, pstCurDev->interface_number);
      //
      iNr++;
      pstCurDev = pstCurDev->next;
   }
   hid_free_enumeration(pstDevs);
   usb_Exit();
#endif   //FEATURE_USB
   return(iNr);
}

/*------ functions separator ------------------------------------------------
____Local_Functions()  {};
----------------------------------------------------------------------------*/

#ifdef FEATURE_USB
//
//  Function:  usb_Daemon
//  Purpose:   Deamon communicating with the USB port
//
//  Parms:     HID struct
//  Returns:   Completion code (0=OK, -1=Error)
//
static int usb_Daemon(RPIUSB *pstTopHid)
{
   int         iNr, iId=1, iOut1=0;
   RPIUSB     *pstRawHid=NULL;
   sigset_t    tBlockset;
   char       *pcBuffer;

   if(!usb_SignalRegister(&tBlockset)) return(-1);
   //
   if(pstTopHid)
   {
      pcBuffer = safemalloc(MAX_USB);
      //
      // Get RAWHID handle
      //
      pstRawHid = USB_GetDeviceInfo(pstTopHid, 0x16c0, 0x480, 0);
      if(!USB_Open(pstRawHid)) return(-1);
      //
      GLOBAL_PidSaveGuard(PID_USBC, GLOBAL_UDM_INI);
      GLOBAL_HostNotification(GLOBAL_UDM_INI);
      LOG_Report(0, "UDM", "usb_Daemon():Init Ready");
      //
      while(fUsbRunning)
      {
         //
         // Read RawHid data
         //
         iNr = USB_Read(pstRawHid, pcBuffer, MAX_USB);
         if(iNr)
         {
            //PRINTF1("RpiUsb():Raw HID has %d bytes" CRLF, iNr);
            USB_LOG_COMMS("USB-Comms", pcBuffer, iNr, DUMP_ASCII);
            //
            // Write back some JSON object
            //
            GEN_SNPRINTF(pcBuffer, 64, "{\"Id\":%d,\"Cmd\":\"Write\",\"Y1\":%d}", iId, iOut1);
            iNr = USB_Write(pstRawHid, pcBuffer, 64);
            if(iNr == -1)
            {
               PRINTF("RpiUsb():Raw HID write error" CRLF);
            }
            else 
            {
               iId++;
               iOut1 ^= 1;
            }
         }
         else
         {
            PRINTF("RpiUsb():Raw HID has no data !!" CRLF);
         }
      }
      safefree(pcBuffer);
   }
   //
   // Cleanup
   //
   USB_Close(pstRawHid);
   return(0);
}

//
//  Function:  usb_Debug
//  Purpose:   Debug communicating with the USB port
//
//  Parms:     HID struct
//  Returns:   Completion code (0=OK, -1=Error)
//
static int usb_Debug(RPIUSB *pstTopHid)
{
   int         iNr;
   RPIUSB     *pstDebHid=NULL;
   sigset_t    tBlockset;
   char       *pcBuffer;

   if(!usb_SignalRegister(&tBlockset)) return(-1);

   if(pstTopHid)
   {
      pcBuffer = safemalloc(MAX_USB);
      //
      // Get Debug HID handle
      //
      pstDebHid = USB_GetDeviceInfo(pstTopHid, 0x16c0, 0x480, 1);
      if(!USB_Open(pstDebHid)) return(-1);
      //
      GLOBAL_PidSaveGuard(PID_USBD, GLOBAL_UDB_INI);
      GLOBAL_HostNotification(GLOBAL_UDB_INI);
      LOG_Report(0, "UDB", "usb_Debug():Init Ready");
      //
      while(fUsbRunning)
      {
         //
         // Read Debug Hid data
         //
         iNr = USB_Read(pstDebHid, pcBuffer, MAX_USB);
         if(iNr)
         {
            //PRINTF1("USB_Debug():Debug HID has %d bytes" CRLF, iNr);
            USB_LOG_DEBUG("USB-Debug", pcBuffer, iNr, DUMP_ASCII);
         }
         else
         {
            PRINTF("USB_Debug():Debug HID has no data" CRLF);
         }
      }
      safefree(pcBuffer);
   }
   //
   // Cleanup
   //
   USB_Close(pstDebHid);
   return(0);
}

#ifdef  DEBUG_ASSERT                   // defined in makefile
//
//  Function:  usb_DumpComms = USB_LOG_COMMS()
//  Purpose:   Dump the Comms buffer
//
//  Parms:     Title, Buffer, size, hexmode
//  Returns:   
//
static void usb_DumpComms(const char *pcTitle, char *pcBuffer, int iSize, bool fHex)
{
   if(fHex)
   {
      int i;

      LOGUSB2("usb_DumpComms():%s:" CRLF, pcTitle);
      //
      for(i=0; i<iSize;i++)
      {
         if( (i % 32) == 0) LOGUSB1(CRLF);
         LOGUSB2("%02x ", pcBuffer[i]);
         LOGUSB1(CRLF);
      }
   }
   else
   {
      LOGUSB3("usb_DumpComms():%s:%s" CRLF, pcTitle, pcBuffer);
   }
}

//
//  Function:  usb_Dump Debug = USB_LOG_DEBUG()
//  Purpose:   Dump the debug buffer
//
//  Parms:     Title, Buffer, size, hexmode
//  Returns:   
//
static void usb_DumpDebug(const char *pcTitle, char *pcBuffer, int iSize, bool fHex)
{
   if(fHex)
   {
      int i;

      LOGUSD2("usb_DumpDebug():%s:" CRLF, pcTitle);
      //
      for(i=0; i<iSize;i++)
      {
         if( (i % 32) == 0) LOGUSD1(CRLF);
         LOGUSD2("%02x ", pcBuffer[i]);
         LOGUSD1(CRLF);
      }
   }
   else
   {
      LOGUSD3("usb_DumpDebug():%s:%s" CRLF, pcTitle, pcBuffer);
   }
}
#endif   //DEBUG_ASSERT                   // defined in makefile

//
//  Function:     usb_Init
//  Description:  Init usb hid api
//
//  Arguments:    
//  Returns:      0 on success, -1 on error
//
//
static int usb_Init()
{
   return( hid_init() );
}

//
//  Function:   usb_ReceiveSignalUser1
//  Purpose:    SIGUSR1 signal
//
//  Parms:
//  Returns:    
//
static void usb_ReceiveSignalUser1(int iSignal)
{
   PRINTF ("usb_ReceiveSignalUser1()" CRLF);
   fUsbRunning = FALSE;
}

//
//  Function:   usb_ReceiveSignalUser2
//  Purpose:    SIGUSR2 signal
//
//  Parms:
//  Returns:    
//  Note:       Not used
//
static void usb_ReceiveSignalUser2(int iSignal)
{
}

//
//  Function:   usb_ReceiveSignalTerm
//  Purpose:    SIGTERM signal (^X)
//
//  Parms:
//  Returns:    
//
static void usb_ReceiveSignalTerm(int iSignal)
{
   PRINTF ("usb_ReceiveSignalTerm()" CRLF);
   fUsbRunning = FALSE;
}

//
//  Function:   usb_ReceiveSignalInt
//  Purpose:    SIGINT signal (^C)
//
//  Parms:
//  Returns:    
//
static void usb_ReceiveSignalInt(int iSignal)
{
   PRINTF ("usb_ReceiveSignalInt()" CRLF);
   fUsbRunning = FALSE;
}

//
//  Function:   usb_SignalRegister
//  Purpose:    Register all SIGxxx
//
//  Parms:      Blockset
//  Returns:    TRUE if delivered
//
static bool usb_SignalRegister(sigset_t *ptBlockset)
{
  bool fCC = TRUE;

  //
  // SIGINT is used to exit
  //
  if( signal(SIGINT, &usb_ReceiveSignalInt) == SIG_ERR)
  {
     LOG_Report(errno, "USB", "usb_SignalRegister(): SIGINT ERROR");
     fCC = FALSE;
  }
  //
  //
  // SIGTERM is used to exit
  //
  if( signal(SIGTERM, &usb_ReceiveSignalTerm) == SIG_ERR)
  {
     LOG_Report(errno, "USB", "usb_SignalRegister(): SIGTERM ERROR");
     fCC = FALSE;
  }
  //
  //
  // SIGUSR1 is used to talk between parent and child 
  //
  if( signal(SIGUSR1, &usb_ReceiveSignalUser1) == SIG_ERR)
  {
     LOG_Report(errno, "USB", "usb_SignalRegister(): SIGUSR1 ERROR");
     fCC = FALSE;
  }
  //
  //
  // SIGUSR2 is not used
  //
  if( signal(SIGUSR2, &usb_ReceiveSignalUser2) == SIG_ERR)
  {
     LOG_Report(errno, "USB", "usb_SignalRegister(): SIGUSR2 ERROR");
     fCC = FALSE;
  }
  //
  if(fCC)
  {
     //
     // Setup the SIGxxx events
     //
     sigemptyset(ptBlockset);
     sigaddset(ptBlockset, SIGINT);
     sigaddset(ptBlockset, SIGTERM);
     sigaddset(ptBlockset, SIGUSR1);
     sigaddset(ptBlockset, SIGUSR2);
  }
  return(fCC);
}

//
//  Function:     usb_StoreString
//  Description:  
// 
//  Arguments:    
//  Returns:      
// 
// 
static char *usb_StoreString(void *pvData, bool fLongString)
{
   int   iLen;
   char *pcDest;

   if(fLongString)
   {
      // Source is a long string
      iLen   = wcslen((wchar_t *) pvData);
      pcDest = safemalloc(iLen+1);
      GEN_SPRINTF(pcDest, "%ls", (wchar_t *)pvData);
   }
   else
   {
      // Source is a normal string
      iLen   = strlen((char *) pvData);
      pcDest = safemalloc(iLen+1);
      GEN_SPRINTF(pcDest, "%s", (char *)pvData);
   }
   return(pcDest);
}

//
//  Function:     usb_Exit
//  Description:  Exit usb hid api
// 
//  Arguments:    
//  Returns:      0 on success, -1 on error
// 
// 
static int usb_Exit()
{
   return( hid_exit() );
}

/*------ functions separator ------------------------------------------------
__COMMENTS__()  {};
----------------------------------------------------------------------------*/

#endif   //FEATURE_USB

